<?php

use Illuminate\Database\Seeder;

use App\BadgeSocialLearning;

class BadgesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $badge = new BadgeSocialLearning;
        $badge->name = 'Make Friend';
        $badge->simple_name = 'badge_make_friend';
        $badge->logo = 'fa-users';
        $badge->save();

        $badge = new BadgeSocialLearning;
        $badge->name = 'Fans';
        $badge->simple_name = 'badge_fans';
        $badge->logo = 'fa-star';
        $badge->save();

        $badge = new BadgeSocialLearning;
        $badge->name = 'Popular';
        $badge->simple_name = 'badge_popular';
        $badge->logo = 'fa-fire';
        $badge->save();

        $badge = new BadgeSocialLearning;
        $badge->name = 'Alumni';
        $badge->simple_name = 'badge_alumni';
        $badge->logo = 'fa-graduation-cap';
        $badge->save();

        $badge = new BadgeSocialLearning;
        $badge->name = 'Welcome';
        $badge->simple_name = 'badge_welcome';
        $badge->logo = 'fa-sign-out-alt';
        $badge->save();

        $badge = new BadgeSocialLearning;
        $badge->name = 'Nocturnal';
        $badge->simple_name = 'badge_nocturnal';
        $badge->logo = 'fa-eye';
        $badge->save();

        $badge = new BadgeSocialLearning;
        $badge->name = 'I-Time';
        $badge->simple_name = 'badge_i_time';
        $badge->logo = 'fa-clock';
        $badge->save();
    }
}
