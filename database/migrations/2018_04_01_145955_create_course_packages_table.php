<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_packages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->boolean('is_publish');
            $table->double('price');
            $table->string('image');
            $table->string('video')->nullable();
            $table->integer('category_id')->foreign('category_id')->references('id')->on('categories')->default(0);
            $table->integer('sub_category_id')->foreign('sub_category_id')->references('id')->on('sub_categories')->default(0);
            $table->integer('tenant_id')->foreign('tenant_id')->references('id')->on('tenants')->default(0);
            $table->integer('user_id')->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_packages');
    }
}
