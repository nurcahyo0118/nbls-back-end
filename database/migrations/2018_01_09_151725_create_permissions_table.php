<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissions', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->boolean('read_role');
            $table->boolean('update_role');

            $table->boolean('read_user');
            $table->boolean('create_user');
            $table->boolean('update_user');
            $table->boolean('delete_user');

            $table->boolean('read_course');
            $table->boolean('create_course');
            $table->boolean('update_course');
            $table->boolean('delete_course');

            $table->boolean('read_category');
            $table->boolean('create_category');
            $table->boolean('update_category');
            $table->boolean('delete_category');

            $table->boolean('read_tenant');
            $table->boolean('create_tenant');
            $table->boolean('update_tenant');
            $table->boolean('delete_tenant');

            $table->boolean('read_tenant_me');
            $table->boolean('create_tenant_me');
            $table->boolean('update_tenant_me');
            $table->boolean('delete_tenant_me');

            $table->boolean('read_student_menu');

            $table->boolean('read_i_time');

            $table->boolean('read_page');

            $table->boolean('read_report_finance');
            $table->boolean('read_report_log');

            $table->integer('role_id')->foreign('role_id')->references('id')->on('roles');
            $table->integer('tenant_id')->foreign('tenant_id')->references('id')->on('tenants');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permissions');
    }
}
