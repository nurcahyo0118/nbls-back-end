<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItimeSocialLearningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itime_social_learnings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('instructor_explanation');
            $table->string('instructor_response');
            $table->integer('best_instructor_id')->foreign('best_instructor_id')->references('id')->on('users');
            $table->integer('worst_instructor_id')->foreign('worst_instructor_id')->references('id')->on('users');
            $table->string('instructor_suggestion');
            $table->string('course_explanation');
            $table->integer('best_course_id')->foreign('best_course_id')->references('id')->on('courses');
            $table->integer('worst_course_id')->foreign('worst_course_id')->references('id')->on('courses');
            $table->string('course_suggestion');
            $table->string('computer_help');
            $table->string('ac_comfortable');
            $table->string('class_cleanliness');
            $table->string('facility_help');
            $table->string('facility_suggestion');
            $table->string('login_problem');
            $table->string('learning_system_suggestion');
            $table->integer('user_id')->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itime_social_learnings');
    }
}
