<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title', 150);
            $table->text('description')->nullable();
            $table->integer('duration');
            $table->string('status', 20);
            $table->double('price');
            $table->text('suitable')->nullable();
            $table->text('requirement')->nullable();
            $table->text('can_be')->nullable();
            $table->string('image')->nullable();
            $table->string('video')->nullable();
            $table->string('wistia_hashed_id')->nullable();
            $table->integer('category_id')->foreign('category_id')->references('id')->on('categories')->default(0);
            $table->integer('sub_category_id')->foreign('sub_category_id')->references('id')->on('sub_categories')->default(0);
            $table->integer('tenant_id')->foreign('tenant_id')->references('id')->on('tenants')->default(0);
            $table->integer('author_id')->foreign('author_id')->references('id')->on('users');
            $table->boolean('is_enable_review')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
