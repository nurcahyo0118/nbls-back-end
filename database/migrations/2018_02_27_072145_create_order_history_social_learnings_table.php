<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderHistorySocialLearningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_history_social_learnings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tXid');
            $table->string('callbackUrl');
            $table->string('description');
            $table->string('transDt');
            $table->string('transTm');
            $table->string('bankVacctNo');
            $table->string('resultCd');
            $table->string('resultMsg');
            $table->string('referenceNo');
            $table->string('payMethod');
            $table->integer('user_id')->foreign('user_id')->references('id')->on('users');
            $table->string('status')->default('unpaid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_history_social_learnings');
    }
}
