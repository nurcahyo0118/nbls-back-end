<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('is_student_open_course')->default(false);
            $table->boolean('is_order_occur')->default(false);
            $table->boolean('is_ask_question')->default(false);
            $table->boolean('is_answer_question')->default(false);
            $table->boolean('is_cancel_subsciption')->default(false);
            $table->boolean('is_email_account_created')->default(true);
            $table->boolean('is_email_weekly')->default(false);
            $table->boolean('is_question_answered')->default(false);
            $table->boolean('is_email_purchase')->default(false);
            $table->integer('user_id')->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification_settings');
    }
}
