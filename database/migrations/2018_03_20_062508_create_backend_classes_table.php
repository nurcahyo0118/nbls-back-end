<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBackendClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('backend_classes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('order_list')->default(0);
            $table->string('file');
            $table->integer('course_id')->foreign('course_id')->references('id')->on('courses');
            $table->integer('user_id')->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('backend_classes');
    }
}
