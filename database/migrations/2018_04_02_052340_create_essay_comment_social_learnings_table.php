<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEssayCommentSocialLearningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('essay_comment_social_learnings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('essay_answer_id')->foreign('essay_answer_id')->references('id')->on('essay_answer_social_learnings');
            $table->integer('user_id')->foreign('user_id')->references('id')->on('users');
            $table->text('body');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('essay_comment_social_learnings');
    }
}
