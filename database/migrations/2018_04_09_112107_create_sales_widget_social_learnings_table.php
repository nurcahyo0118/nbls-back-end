<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesWidgetSocialLearningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_widget_social_learnings', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('is_button_only')->default(false);
            $table->boolean('is_publish')->default(false);
            $table->string('card_background_color')->default('card-default');
            $table->string('button_background_color')->default('button-default');
            $table->string('card_text_color')->default('');
            $table->string('button_text_color')->default('');
            $table->string('button_text');
            $table->string('image')->nullable();
            $table->integer('course_id')->foreign('course_id')->references('id')->on('courses');
            $table->integer('user_id')->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_widget_social_learnings');
    }
}
