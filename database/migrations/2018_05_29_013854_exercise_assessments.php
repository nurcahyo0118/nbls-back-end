<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExerciseAssessments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exercise_assessments', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('exercise_id')->foreign('exercise_id')->references('id')->on('exercises');
            $table->integer('criteria_id')->foreign('criteria_id')->references('id')->on('exercise_criterias');
            $table->integer('user_id')->foreign('user_id')->references('id')->on('users');
            $table->integer('value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exercise_assessments');
    }
}
