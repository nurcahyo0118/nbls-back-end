<?php

namespace App;
Use App\CommentSocialLearning;

use Illuminate\Database\Eloquent\Model;

class PostSocialLearning extends Model
{
  public function user()
  {
      return $this->belongsTo('App\User', 'user_id');
  }

  public function likes()
  {
    return $this->hasMany('App\PostLike', 'post_id');
  }

  public function dislikes()
  {
    return $this->hasMany('App\PostDislike', 'post_id');
  }

  public function postComments()
  {
    return $this->hasMany('App\Comment', 'post_id');
  }

  public function link()
  {
      return $this->hasOne('App\LinkPostSocialLearning', 'post_id');
  }
}
