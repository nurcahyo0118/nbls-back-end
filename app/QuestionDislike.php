<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionDislike extends Model
{
    protected $fillable = ['question_id', 'user_id'];
    
    public function question()
    {
        return $this->hasOne('App\Question', 'question_id');
    }
}
