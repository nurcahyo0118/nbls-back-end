<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class RegisterSocialLearningRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fullname' => 'required|max:255',
            'ktp_id' => 'unique:users',
            'username' => 'required|unique:users',
            'password' => 'required',
            'email' => 'required|unique:users|email',
            'phone' => 'unique:users',
            'mobile' => '',
            'mobile2' => '',
            'pin_bb' => '',
            'zipcode' => '',
            'fb' => '',
            'tw' => '',
            'ig' => '',
            'website' => '',
            'status' => '',
            'hobby' => '',
            'about' => '',
            'skill' => '',
            'hope' => '',
            'reason' => '',
            'birth_date' => 'date'
        ];
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
