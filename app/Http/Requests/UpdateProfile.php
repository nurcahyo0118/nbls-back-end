<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class UpdateProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'numeric',
            'ktp_id' => '',
            // 'username' => '',
            'password' => '',
            // 'email' => 'required',
            'birth_date' => 'date|nullable',
            'phone' => '',
            'mobile' => '',
            'mobile2' => '',
            'pin_bb' => '',
            'notes' => '',
            'photo' => '',
            'fullname' => '',
            'address' => '',
            'sex' => 'in:L,P',
            'zipcode' => '',
            'fb' => '',
            'tw' => '',
            'ig' => '',
            'website' => '',
            'status' => '',
            'hobby' => '',
            'about' => '',
            'skill' => '',
            'hope' => '',
            'reason' => '',
            'is_patnership' => 'boolean',
            'role_id' => 'numeric',
            'point_id' => 'numeric',
            'branch_id' => 'numeric',
            'tenant_id' => 'numeric'
        ];

        // return [
        //     'ktp_id' => '',
        //     'username' => 'required',
        //     'email' => 'required|email',
        //     'birth_date' => 'date|nullable',
        //     'phone' => '',
        //     'mobile' => '',
        //     'mobile2' => '',
        //     'pin_bb' => '',
        //     'notes' => '',
        //     'fullname' => 'required|max:255',
        //     'address' => '',
        //     'sex' => '',
        //     'zipcode' => '',
        //     'fb' => '',
        //     'tw' => '',
        //     'ig' => '',
        //     'website' => '',
        //     'status' => '',
        //     'hobby' => '',
        //     'about' => '',
        //     'skill' => '',
        //     'hope' => '',
        //     'reason' => ''
        // ];
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
