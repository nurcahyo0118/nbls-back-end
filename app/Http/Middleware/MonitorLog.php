<?php

namespace App\Http\Middleware;

use Closure;

use Illuminate\Support\Facades\Route;

use Log;
use Auth;

use App\NotificationSocialLearning;
use App\User;

class MonitorLog
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Log::debug($request->log);
        Log::debug($request->all());

        if (Auth::user() != null) {
            if (Auth::user()->role_id == 5) {
                switch (Route::currentRouteName()) {
                    case 'me':
                    case 'me.edit':
                    case 'dashboard':
                    case 'courses.page.sections':
                    case 'courses.page.quizzes':
                    case 'courses.page.exercises':
                    case 'courses.page.references':
                    case 'reviews.index':
                    case 'social-learning-itime.index':
                    case 'payments':
                    case 'pages.index':

                    case 'tenant.me':
                    case 'tenant.me.edit':
                    case 'tenant.dashboard':
                    case 'tenant.courses.page.sections':
                    case 'tenant.courses.page.quizzes':
                    case 'tenant.courses.page.exercises':
                    case 'tenant.courses.page.references':
                    case 'tenant.reviews.index':
                    case 'tenant.social-learning-itime.index':
                    case 'tenant.payments':
                        return redirect()->route('social-learning-posts.index');
                        break;
                }
            }
        }

        switch ($request->log) {

            // FOLLOW MODULE
            case 'LOG_FOLLOW':

                Log::debug('LOG_FOLLOW');

                $user = Auth::user();

                if($user != null)
                {
                    $notification = new NotificationSocialLearning;
                    $notification->title = $user->fullname . ' mulai mengikuti anda';
                    $notification->content = $user->fullname . ' mulai mengikuti anda';
                    $notification->user_id = $request->following_id;

                    $notification->save();

                    Auth::user()->points()->create([
                        'point' => 2,
                        'user_id' => Auth::id()
                    ]);

                }

                break;

            case 'LOG_UNFOLLOW':

                Log::debug('LOG_UNFOLLOW');

                Auth::user()->points()->create([
                    'point' => -2,
                    'user_id' => Auth::id()
                ]);

                break;

            // BADGE MODULE
            case 'LOG_COLLECT_BADGE':
                Log::debug('LOG_COLLECT_BADGE');
                break;

            // RANK MODULE
            case 'LOG_RANK':
                Log::debug('LOG_RANK');
                break;

            // ALUMNI MODULE
            case 'LOG_MODULE':
                Log::debug('LOG_MODULE');
                break;

            // LOGIN MODULE
            case 'LOG_LOGIN':

                Log::debug('LOG_LOGIN');
                break;

            case 'LOG_LOGOUT':
                Log::debug('LOG_LOGOUT');
                break;

            // QUESTIONS MODULE
            case 'LOG_SHARE_QUESTIONS':

                Log::debug('LOG_SHARE_QUESTIONS');

                Auth::user()->points()->create([
                    'point' => 2,
                    'user_id' => Auth::id()
                ]);

                break;

            // Question Module
            case 'LOG_LIKE_QUESTIONS':

                Log::debug('LOG_LIKE_QUESTIONS');

                if (Auth::id() != $request->user_id) {
                    Auth::user()->points()->create([
                        'point' => 2,
                        'user_id' => Auth::id()
                    ]);

                    $user = Auth::user();

                    if($user != null)
                    {
                        $notification = new NotificationSocialLearning;
                        $notification->title = $user->fullname . ' menyukai pertanyaan anda';
                        $notification->content = $user->fullname . ' menyukai pertanyaan anda';
                        $notification->user_id = $request->user_id;

                        $notification->save();
                    }

                }

                break;

            case 'LOG_DISLIKE_QUESTIONS':

                Log::debug('LOG_DISLIKE_QUESTIONS');

                if (Auth::id() != $request->user_id) {
                    Auth::user()->points()->create([
                        'point' => 2,
                        'user_id' => Auth::id()
                    ]);

                    $user = Auth::user();

                    if($user != null)
                    {
                        $notification = new NotificationSocialLearning;
                        $notification->title = $user->fullname . ' tidak menyukai pertanyaan anda';
                        $notification->content = $user->fullname . ' tidak menyukai pertanyaan anda';
                        $notification->user_id = $request->user_id;

                        $notification->save();
                    }
                    
                }

                break;

            // POSTS MODULE
            case 'LOG_SHARE_POSTS':

                Log::debug('LOG_SHARE_POSTS');

                Auth::user()->points()->create([
                    'point' => 2,
                    'user_id' => Auth::id()
                ]);

                break;

            case 'LOG_LIKE_POSTS':

                Log::debug('LOG_LIKE_POSTS');

                if (Auth::id() != $request->user_id) {
                    Auth::user()->points()->create([
                        'point' => 2,
                        'user_id' => Auth::id()
                    ]);

                    $user = Auth::user();

                    if($user != null)
                    {
                        $notification = new NotificationSocialLearning;
                        $notification->title = $user->fullname . ' menyukai kiriman anda';
                        $notification->content = $user->fullname . ' menyukai kiriman anda';
                        $notification->user_id = $request->user_id;

                        $notification->save();
                    }
                }

                break;

            case 'LOG_DISLIKE_POSTS':

                Log::debug('LOG_DISLIKE_POSTS');

                if (Auth::id() != $request->user_id) {
                    Auth::user()->points()->create([
                        'point' => 2,
                        'user_id' => Auth::id()
                    ]);

                    $user = Auth::user();

                    if($user != null)
                    {
                        $notification = new NotificationSocialLearning;
                        $notification->title = $user->fullname . ' tidak menyukai kiriman anda';
                        $notification->content = $user->fullname . ' tidak menyukai kiriman anda';
                        $notification->user_id = $request->user_id;

                        $notification->save();
                    }
                    
                }

                break;

            case 'LOG_COMMENTS':

                Log::debug('LOG_COMMENTS');

                if (Auth::id() != $request->user_id) {
                    Auth::user()->points()->create([
                        'point' => 2,
                        'user_id' => Auth::id()
                    ]);

                    $user = Auth::user();

                    if($user != null)
                    {
                        $notification = new NotificationSocialLearning;
                        $notification->title = $user->fullname . ' mengomentari kiriman anda';
                        $notification->content = $user->fullname . ' mengomentari kiriman anda';
                        $notification->user_id = $request->user_id;

                        $notification->save();
                    }
                    
                }

                break;

            case 'LOG_LIKE_COMMENTS':

                Log::debug('LOG_LIKE_POSTS');

                Auth::user()->points()->create([
                    'point' => 2,
                    'user_id' => Auth::id()
                ]);

                break;

            case 'LOG_DISLIKE_COMMENTS':

                Log::debug('LOG_DISLIKE_POSTS');

                Auth::user()->points()->create([
                    'point' => 2,
                    'user_id' => Auth::id()
                ]);

                break;

            // EVENTS MODULE
            case 'LOG_CREATE_EVENTS':

                Log::debug('LOG_CREATE_EVENTS');

                Auth::user()->points()->create([
                    'point' => 35,
                    'user_id' => Auth::id()
                ]);

                break;

            case 'LOG_JOIN_EVENTS':

                Log::debug('LOG_JOIN_EVENTS');

                Auth::user()->points()->create([
                    'point' => 30,
                    'user_id' => Auth::id()
                ]);

                break;

            // EXCHANGE POINTS MODULE
            case 'LOG_EXCHANGE_POINTS':

                Log::debug('LOG_EXCHANGE_POINTS');

                Auth::user()->points()->create([
                    'point' => 5,
                    'user_id' => Auth::id()
                ]);

                break;

            // BOOK CLASSES MODULE
            case 'LOG_BOOKING_CLASS':

                Log::debug('LOG_BOOKING_CLASS');

                Auth::user()->points()->create([
                    'point' => 20,
                    'user_id' => Auth::id()
                ]);

                break;

            case 'LOG_CONSULTATION':

                Log::debug('LOG_CONSULTATION');

                Auth::user()->points()->create([
                    'point' => 15,
                    'user_id' => Auth::id()
                ]);

                break;

            // COURSE MODULE
            case 'LOG_BUY_COURSES':

                Log::debug('LOG_BUY_COURSES');

                Auth::user()->points()->create([
                    'point' => 20,
                    'user_id' => Auth::id()
                ]);

                break;

            case 'LOG_END_COURSES':

                Log::debug('LOG_END_COURSES');

                Auth::user()->points()->create([
                    'point' => 50,
                    'user_id' => Auth::id()
                ]);

                break;

            // PROFILE MODULE
            case 'LOG_ITIME':

                Log::debug('LOG_ITIME');

                Auth::user()->points()->create([
                    'point' => 5,
                    'user_id' => Auth::id()
                ]);

                break;

            case 'LOG_COMPLETE_PROFILE':

                Log::debug('LOG_COMPLETE_PROFILE');

                Auth::user()->points()->create([
                    'point' => 10,
                    'user_id' => Auth::id()
                ]);

                break;

            case 'LOG_ASSESSING_PEOPLE':

                Log::debug('LOG_ASSESSING_PEOPLE');

                Auth::user()->points()->create([
                    'point' => 40,
                    'user_id' => Auth::id()
                ]);

                break;

            // STREAMING VIDEO
            case 'LOG_POST_WORK':

                Log::debug('LOG_POST_WORK');

                Auth::user()->points()->create([
                    'point' => 40,
                    'user_id' => Auth::id()
                ]);

                break;

            case 'LOG_ASK_MATERIAL':

                Log::debug('LOG_ASK_MATERIAL');

                Auth::user()->points()->create([
                    'point' => 7,
                    'user_id' => Auth::id()
                ]);

                break;

            case 'LOG_WATCH_VIDEO':

                Log::debug('LOG_WATCH_VIDEO');

                Auth::user()->points()->create([
                    'point' => 7,
                    'user_id' => Auth::id()
                ]);

                break;

            case 'LOG_ANSWER_QUIZ':

                Log::debug('LOG_ANSWER_QUIZ');

                Auth::user()->points()->create([
                    'point' => 2,
                    'user_id' => Auth::id()
                ]);

                break;

            case 'LOG_ANSWER_EXERCISE':

                Log::debug('LOG_ANSWER_EXERCISE');

                Auth::user()->points()->create([
                    'point' => 10,
                    'user_id' => Auth::id()
                ]);

                break;

            case 'LOG_ANSWER_ESSAY':

                Log::debug('LOG_ANSWER_ESSAY');

                Auth::user()->points()->create([
                    'point' => 5,
                    'user_id' => Auth::id()
                ]);

                break;

            case 'LOG_ANSWER_ENCOUNTER':

                Log::debug('LOG_ANSWER_ENCOUNTER');

                Auth::user()->points()->create([
                    'point' => 40,
                    'user_id' => Auth::id()
                ]);

                break;

        }

        if ($request->log != null && $request->log != 'LOG_LOGIN') {
            Auth::user()->logs()->create([
                'reason' => $request->log,
                'user_id' => Auth::id()
            ]);
        }

        return $next($request);
    }
}
