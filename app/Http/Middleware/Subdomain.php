<?php

namespace App\Http\Middleware;

use App\Tenant;
use Closure;
use Illuminate\Support\Facades\Log;

class Subdomain
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $explode = explode('.', $_SERVER['HTTP_HOST']);
//            array_shift for pick first value of array
        $subdomain = array_shift($explode);
        Log::debug($explode);

        $tenant = Tenant::where('subdomain', $subdomain)->first();

        Log::debug($tenant);

        if (!$tenant) {
            return redirect()->route('notfound');
        }

        return $next($request);
    }
}
