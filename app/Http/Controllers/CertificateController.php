<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\Tenant;
use Auth;
use Image;
use File;
use App\ECertificateSocialLearning;

class CertificateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $certificate = ECertificateSocialLearning::where(['user_id' => Auth::id()])->first();

        return view('certificate.index')
            ->withCertificate($certificate);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('certificate.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

//        return $request->all();
        $certificate = ECertificateSocialLearning::where(['user_id' => Auth::id()])->first();
        
        if ($request->hasFile('signature')) {
            $image = $request->file('signature');
            $filename = time() . '.' . $image->getClientOriginalName();
            $path = public_path('images/photo/' . $filename);
            Image::make($image->getRealPath())->save($path);
            File::delete(public_path('images/photo/' . $certificate->signature));
            $certificate->signature = $filename;

        }

        if ($request->hasFile('template')) {
            $image = $request->file('template');
            $filename = time() . '.' . $image->getClientOriginalName();
            $path = public_path('images/photo/' . $filename);
            Image::make($image->getRealPath())->save($path);
            File::delete(public_path('images/photo/' . $certificate->template));
            $certificate->template = $filename;

        }

        $certificate->save();

        return view('certificate.index')->withCertificate($certificate);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $courses = Course::get();
        $certificate = ECertificateSocialLearning::where(['user_id' => Auth::id()])->first();

        return view('social-learnings.e-certificate.show')
            ->withCertificate($certificate);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($domain, $id)
    {
        $certificate = ECertificateSocialLearning::find($id);
        return view('certificate.edit')
            ->withEncounter($certificate);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($domain, Request $request, $id)
    {
        $certificate = ECertificateSocialLearning::find($id);

        if ($request->hasFile('signature')) {
            $image = $request->file('signature');
            $filename = time() . '.' . $image->getClientOriginalName();
            $path = public_path('images/photo/' . $filename);
            Image::make($image->getRealPath())->save($path);
            $certificate->signature = $filename;
        }

        if ($request->hasFile('template')) {
            $image = $request->file('template');
            $filename = time() . '.' . $image->getClientOriginalName();
            $path = public_path('images/photo/' . $filename);
            Image::make($image->getRealPath())->save($path);
            $certificate->template = $filename;

        }

        $certificate->save();

        return view('certificate.index')->withCertificate($certificate);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
