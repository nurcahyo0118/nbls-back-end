<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Course;
use App\Material;
use App\Exercise;
use App\EssaySocialLearning;
use App\Encounter;
use App\MasterpieceSocialLearning;
use App\NotificationSocialLearning;
use Session;

use Auth;
use DB;

class MyCourseSocialLearningController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('social-learnings.my-course.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($domain, $id)
    {
        $current_user = Auth::user();

        $user_courses = [];

        foreach($current_user->courses as $course)
        {
            array_push($user_courses, $course->id);
        }

        $course = Course::where('id', $id)->whereIn('id', $user_courses)->first();
        
        // Validation for own course
        if($course != null)
        {
            $notification = new NotificationSocialLearning;
            $notification->title = $current_user->fullname . ' telah masuk kursus anda';
            $notification->content = $current_user->fullname . ' telah masuk kursus anda';
            $notification->user_id = $course->author_id;

            $notification->save();

            return view('social-learnings.course.vidio')->withCourse($course);
        }
        else
        {
            return redirect()->back();
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function currentMaterial($domain, Request $request, $id, $material_id)
    {
        $current_user = Auth::user();

        $user_courses = [];
        $material_lines = [];
        $section_lines = [];

        foreach($current_user->courses as $course)
        {
            array_push($user_courses, $course->id);
        }

        $course = Course::where('id', $id)->whereIn('id', $user_courses)->first();
        
        // Validation for own course
        if($course != null)
        {
            foreach($course->sections as $section)
            {
                // array_push($section_lines, $section->line);

                foreach($section->materials as $material)
                {
                    array_push($material_lines, $material->line);
                }
            }
            
            sort($material_lines);
            // sort($section_lines);
            
            $material = Material::find($material_id);

            $current_position = array_search($material->line, $material_lines);

            // return $current_position;

            // Is previous video is watched
            if($current_position > 0)
            {
                $previous_material = Material::where('line', $material_lines[$current_position - 1])
                    ->where('section_id', $material->section_id)
                    ->first();

                // return $previous_material->id;

                // return Auth::user()->viewedVideos()->where(['material_id' => $previous_material->id, 'user_id' => Auth::id()])->first();

                if (Auth::user()->viewedVideos()->where([
                    'material_id' => $previous_material->id,
                    'user_id' => Auth::id()])->first() === null)
                {
                    // return 'ddd';
                    Session::flash('failed', 'Tonton video secara runtut');
                    return redirect()->back();
                }
            }

            if (Auth::user()->viewedVideos()->where([
                    'material_id' => $material_id,
                    'user_id' => Auth::id()])->first() === null) {
                Auth::user()->viewedVideos()->attach($material_id);
            }

            Auth::user()->points()->create([
                'point' => 7,
                'user_id' => Auth::id()
            ]);

            Auth::user()->logs()->create([
                'reason' => 'LOG_WATCH_VIDEO',
                'user_id' => Auth::id()
            ]);
            
            return view('social-learnings.course.material-vidio')
                ->withCourse($course)
                ->withMaterial($material);
        }
        else
        {
            return redirect()->back();
        }
        
    }

    public function currentEssay($domain, Request $request, $id, $essay_id)
    {
        $current_user = Auth::user();

        $user_courses = [];

        foreach($current_user->courses as $course)
        {
            array_push($user_courses, $course->id);
        }

        $course = Course::where('id', $id)->whereIn('id', $user_courses)->first();
        
        if($course != null)
        {
            $course_sections = [];

            foreach($course->sections as $section)
            {
                array_push($course_sections, $section->id);
            }

            // return $course;

            if($course != null)
            {
                $essay = EssaySocialLearning::where('id', $essay_id)->whereIn('section_id', $course_sections)->first();

                // return $essay;

                if($essay != null)
                {
                    return view('social-learnings.course.material-essay')
                        ->withCourse($course)
                        ->withEssay($essay);
                }
                else
                {
                    return redirect()->back();
                }

            }
            else
            {
                return redirect()->back();
            }
        }
        else
        {
            return redirect()->back();
        }

    }

    public function currentExercise($domain, Request $request, $id, $exercise_id)
    {
        $current_user = Auth::user();

        $user_courses = [];

        foreach($current_user->courses as $course)
        {
            array_push($user_courses, $course->id);
        }

        $course = Course::where('id', $id)->whereIn('id', $user_courses)->first();
        
        if($course != null)
        {
            $course_sections = [];

            foreach($course->sections as $section)
            {
                array_push($course_sections, $section->id);
            }

            $exercise = Exercise::where('id', $exercise_id)->whereIn('section_id', $course_sections)->first();

            if($exercise != null)
            {
                if (Auth::user()->viewedExercises()->where([
                        'exercise_id' => $exercise_id,
                        'user_id' => Auth::id()])->first() == null) {
                    Auth::user()->viewedExercises()->attach($exercise_id);
                }

                return view('social-learnings.course.material-exercise')
                    ->withCourse($course)
                    ->withExercise($exercise);
            }
            else
            {
                return redirect()->back();
            }
            
        }
        else
        {
            return redirect()->back();
        }

    }

    public function addToCart($domain, Request $request, $id)
    {
        if (Auth::user()->carts()->count() === 0) {
            Auth::user()->carts()->attach($id);
        }

        $course = Course::find($id);

        return view('social-learnings.course.join')->withCourse($course);
    }

    public function addToCartWithoutLogin($domain, Request $request, $id)
    {
        $product = Course::find($id);
        // $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart;
        $cart->add($product, $product->id);
        $request->session()->put('cart', $cart);
        dd(session()->get('cart', $cart));
        return view('social-learnings.course.join');
    }

    // public function deleteCart($domain, $id)
    // {
    //     $course = DB::table('user_carts')->where('id', $id)->first();
    //     $course->delete();
        
    //     return view('social-learning-payment.index');
    // }

    public function storeExercise($domain, Request $request, $id)
    {
        $exercise = Exercise::find($id);

        if ($request->hasFile('file')) {

            // if ($exercise->file != null) {
            //     if (file_exists('submited-exercise-file/' . $exercise->file)) {
            //         unlink(public_path('submited-exercise-file/' . $exercise->file));
            //     }
            // }

            $file = $request->file('file');
            $filename = time() . '.' . $file->getClientOriginalName();
            $path = public_path('submited-exercise-file/');
            $file->move($path, $filename);

            $exercise->submittedExercises()->attach(Auth::id(), [
                'file' => $filename
            ]);

            $master = new MasterpieceSocialLearning;

            $master->content = 'Berhasil mengirim latihan';
            $master->user_id = Auth::id();
            $master->tenant_id = Auth::user()->tenant->id;
            $master->file = $path;
            $master->save();

            Auth::user()->gems()->create([
                'gem' => 1,
                'user_id' => Auth::id()
            ]);

            return redirect()->back();

        } else {
            $exercise->submittedExercises()->attach(Auth::id(), [
                'link' => $request->link
            ]);

            $master = new MasterpieceSocialLearning;

            $master->content = 'Berhasil mengirim latihan';
            $master->user_id = Auth::id();
            $master->tenant_id = Auth::user()->tenant->id;
            $master->link = $request->link;
            $master->save();
            
            return redirect()->back();
        }
    }

    public function storeEncounter($domain, Request $request, $id)
    {
        $encounter = Encounter::find($id);

        if ($request->hasFile('file')) {

            $file = $request->file('file');
            $filename = time() . '.' . $file->getClientOriginalName();
            $path = public_path('submited-encounter-file/');
            $file->move($path, $filename);

            $encounter->submittedEncounters()->attach(Auth::id(), [
                'file' => $filename
            ]);

            $master = new MasterpieceSocialLearning;

            $master->content = 'Berhasil mengirim encounter';
            $master->user_id = Auth::id();
            $master->tenant_id = Auth::user()->tenant->id;
            $master->file = $path;
            $master->save();

            return redirect()->back();

        } else {

            if($request->link === null)
            {
                return redirect()->back();
            }
            $encounter->submittedEncounters()->attach(Auth::id(), [
                'link' => $request->link
            ]);

            $master = new MasterpieceSocialLearning;

            $master->content = 'Berhasil mengirim latihan';
            $master->user_id = Auth::id();
            $master->tenant_id = Auth::user()->tenant->id;
            $master->link = $request->link;
            $master->save();
            
            return redirect()->back();
        }

    }

    public function syncCartsData(Request $request)
    {
        $carts_data = json_decode($request->carts_data, true);
        $package_carts_data = json_decode($request->package_carts_data, true);

        if($carts_data !== null)
        {
            foreach($carts_data as $cart)
            {
                $course = Course::find($cart);

                if($course != null && Auth::user()->carts()->where('course_id', $course->id)->first() === null)
                {
                    
                    if($course->price != 0)
                    {
                        Auth::user()->carts()->attach($cart);
                    }
                    else
                    {
                        Auth::user()->courses()->attach($cart);
                    }
                    
                }
            }
        }

        if($package_carts_data !== null)
        {
            foreach($package_carts_data as $package_cart)
            {
                if(Auth::user()->package_carts()->where('package_course_id', $package_cart)->first() === null)
                {
                    Auth::user()->package_carts()->attach($package_cart);
                }
            }
        }

        return response()->json('Successfully sync data !', 200);
    }

    // Loads all carts
    public function loadAllCarts()
    {
        $tenant = Auth::user()->tenant;

        if($tenant != null)
        {
            $courses = Auth::user()->carts;

            return response()->json([
                'view' => (string)view('social-learnings.course.child.cart')
                    ->withCarts($courses)
            ], 200);
        }
        else
        {
            return response()->json('Merchant Not Found !', 400);
        }

        return response()->json('Oops somethings when wrong!', 500);
    }

    // Loads all package carts
    public function loadAllPackageCarts()
    {
        $tenant = Auth::user()->tenant;

        if($tenant != null)
        {
            $package_carts = Auth::user()->package_carts;

            return response()->json([
                'view' => (string)view('social-learnings.course-package.child.cart')
                    ->withCarts($package_carts)
            ], 200);
        }
        else
        {
            return response()->json('Merchant Not Found !', 400);
        }

        return response()->json('Oops somethings when wrong!', 500);
    }

    public function removeCourseFromCart($course_id)
    {
        $tenant = Auth::user()->tenant;

        if($tenant != null)
        {
            Auth::user()->carts()->detach($course_id);

            $courses = Auth::user()->carts;

            return response()->json([
                'view' => (string)view('social-learnings.course.child.cart')
                    ->withCarts($courses)
            ], 200);
        }
        else
        {
            return response()->json('Merchant Not Found !', 400);
        }

        return response()->json('Oops somethings when wrong!', 500);
    }

    public function removePackageCourseFromCart($package_course_id)
    {
        $tenant = Auth::user()->tenant;

        if($tenant != null)
        {
            Auth::user()->package_carts()->detach($package_course_id);

            $courses = Auth::user()->package_carts;

            return response()->json([
                'view' => (string)view('social-learnings.course-package.child.cart')
                    ->withCarts($courses)
            ], 200);
        }
        else
        {
            return response()->json('Merchant Not Found !', 400);
        }

        return response()->json('Oops somethings when wrong!', 500);
    }
}
