<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\NotificationSocialLearning;
use Auth;

class MyProfileController extends Controller
{
    public function seeNotification()
    {
        $notifications = NotificationSocialLearning::where('is_readed', false)->get();

        foreach($notifications as $notification)
        {
            $notification->is_readed = true;
            $notification->save();
        }

        return response()->json(['message' => 'Success'], 200);
    }
}
