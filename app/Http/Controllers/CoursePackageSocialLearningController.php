<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CoursePackage;
use App\Course;

use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class CoursePackageSocialLearningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $course_packages = CoursePackage::where(['tenant_id' => Auth::user()->tenant_id])->get();

        return view('social-learnings.course-package.index')->withCoursePackages($course_packages);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($domain, $id)
    {
        $course_package = CoursePackage::find($id);

        return view('social-learnings.course-package.show')->withCoursePackage($course_package);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function buyPackage($domain, Request $request, $id)
    {
        $course_package = CoursePackage::find($id);

        // dd($course_package);

        if ($course_package->price < 1) {
            foreach ($course_package->packages as $course) {
                if (DB::table('course_users')->where([
                    'user_id' => Auth::id(),
                    'course_id' => $course->id
                ])->first() != null) {

                } else {
                    $course->users()->attach(Auth::id());
                }
            }

            Session::flash('success', 'Berhasil mengambil paket kursus !');

            return redirect()->to('social-learning-my-course');
        } else {

            if (Auth::user()->package_carts()->where(['package_course_id' => $course_package->id])->count() === 0) {
                Auth::user()->package_carts()->attach($id);
            }

        }

        return redirect()->route('social-learning-payment.index', $_SERVER['HTTP_HOST']);
    }

    public function addToCart($domain, Request $request, $id)
    {
        if (Auth::user()->carts()->count() === 0) {
            Auth::user()->carts()->attach($id);
        }

        $course = Course::find($id);

        return view('social-learnings.course.join')->withCourse($course);
    }
}
