<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EventSocialLearning;
use Session;
use Auth;

class EventSocialLearningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = EventSocialLearning::orderBy('id', 'desc')->paginate(15);
        return view('social-learnings.events.index')->withEvents($events);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $event = new EventSocialLearning;
        $event->user_id = $request->user_id;
        $event->tenant_id = $request->tenant_id;
        $event->title = $request->title;
        $event->body = $request->body;
        $event->date = $request->date;
        $event->save();
        //        refresh the page
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = EventSocialLearning::find($id);
        
        $event->users()->detach();

        $event->delete();

        return redirect()->back();
    }

    public function followEvent(Request $request, $id)
    {
        $event = EventSocialLearning::find($id);
        
        $event->users()->attach(Auth::id());
        
        return redirect()->back();
    }

    public function unFollowEvent(Request $request, $id)
    {
        $event = EventSocialLearning::find($id);

        $event->users()->detach(Auth::id());

        return redirect()->back();
    }
}
