<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use App\Tenant;

use App\User;
use App\ECertificateSocialLearning;
use Illuminate\Support\Facades\Log;
use Hash;
use Mail;
use App\VerifyUser;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $email = 'email';
    protected $guard = 'guest';

    public function getLogin($domain)
    {
        if (Auth::guard('web')->check()) {
            
            return redirect()->route('social-learning-posts.index');
        }

        $tenant = Tenant::where(['domain' => $domain])->first();

        return view('login')->withTenant($tenant);
    }

    public function postLogin($domain, Request $request)
    {   
        $user = User::where('email', $request->email)->first();

        if($user != null)
        {
            if (!$user->verified) {
                auth()->logout();
                return back()->withErrors([
                    'login' => 'You need to confirm your account. We have sent you an activation code, please check your email.',
                ]);
            }
    
            $tenant = Tenant::find($request->tenant_id);
    
            $auth = Auth::guard('web')->attempt([
                'tenant_id' => $tenant->id,
                'email' => $request->email,
                'password' => $request->password
            ]);
    
            if ($auth) {
    
                Auth::user()->points()->create([
                    'point' => 2,
                    'user_id' => Auth::id()
                ]);
    
                Auth::user()->logs()->create([
                    'reason' => $request->log,
                    'user_id' => Auth::id()
                ]);
    
                if (Auth::user()->role['id'] === 5) {
                    return redirect()->route('social-learning-posts.index');
                } else {
                    return redirect()->route('dashboard', $tenant->domain);
                }
    
            } else {
                return redirect()->back()
                    ->withErrors([
                        'login' => 'Username atau Password salah',
                    ]);
            }
            
        }
        else
        {
            return redirect()->back();
        }
    }

    public function getLogout($domain)
    {
        Auth::guard('web')->logout();

        return redirect()->route('/', $domain);
    }


    /* Tenant Authentication */
    public function getTenantLogin($subdomain)
    {

        if (Auth::guard('web')->check()) {
            return redirect()->route('social-learning-posts.index');
            // return redirect()->route('me', $subdomain);
        }

        $tenant = Tenant::where(['subdomain' => $subdomain])->first();

        return view('tenants.login')->withTenant($tenant);

//        if (Auth::guard('web')->check()) {
//            return redirect()->route('me');
//        }
//        $tenant = Tenant::where('subdomain', $subdomain)->first();
//
//        return view('tenants.login')->withTenant($tenant);
    }

    public function postTenantLogin($subdomain, Request $request)
    {
        $user = User::where('email', $request->email)->first();
        if (!$user->verified) {
            auth()->logout();
            return back()->withErrors([
                'login' => 'You need to confirm your account. We have sent you an activation code, please check your email.',
            ]);
        }
        $tenant = Tenant::find($request->tenant_id);

        $auth = Auth::guard('web')->attempt([
            'tenant_id' => $tenant->id,
            'email' => $request->email,
            'password' => $request->password
        ]);

        if ($auth) {

            Auth::user()->points()->create([
                'point' => 2,
                'user_id' => Auth::id()
            ]);

            Auth::user()->logs()->create([
                'reason' => 'LOG_LOGIN',
                'user_id' => Auth::id()
            ]);

            if ($tenant->is_cname) {

                if (Auth::user()->role['id'] === 5) {
                    return redirect()->route('social-learning-posts.index');
                } else {
                    return redirect()->route('tenant.dashboard', $tenant->subdomain);
                }

            } else {

                if (Auth::user()->role['id'] === 5) {
                    return redirect()->route('social-learning-posts.index');
                } else {
                    return redirect()->route('tenant.dashboard', $tenant->subdomain);
                }

            }


        } else {
            Log::debug('=========> null');
        }

        return redirect()->route('tenant.login', $subdomain);
    }

    public function tenantLogout($subdomain)
    {
        Auth::guard('web')->logout();

        return redirect()->route('tenant.landing', $subdomain);
    }

    /* end Tenant Authentication */


    public function postRegister(Request $request)
    {

        $this->validate($request, [
            'tenant_id' => 'required',
            'username' => 'required|unique:users',
            'password' => 'required|confirmed',
            'email' => 'required|unique:users|email',
            'phone' => 'unique:users',
            'mobile' => 'unique:users',
            'mobile2' => 'unique:users',
            'pin_bb' => 'unique:users',
            'zipcode' => '',
            'fb' => '',
            'tw' => '',
            'ig' => '',
            'website' => '',
            'status' => '',
            'hobby' => '',
            'about' => '',
            'skill' => '',
            'hope' => '',
            'reason' => ''
        ]);

        // dd($request->all());
        
        
        $user = new User;

        $user->fullname = $request->username;
        $user->username = $request->username;
        $user->password = Hash::make($request->password);
        $user->email = $request->email;

        $user->ktp_id = $request->ktp_id;

        $user->phone = $request->phone;
        $user->mobile = $request->mobile;
        $user->mobile2 = $request->mobile2;
        $user->pin_bb = $request->pin_bb;
        $user->zipcode = $request->zipcode;
        $user->fb = $request->fb;
        $user->tw = $request->tw;
        $user->ig = $request->ig;
        $user->website = $request->website;
        $user->status = $request->status;
        $user->hobby = $request->hobby;
        $user->about = $request->about;
        $user->skill = $request->skill;
        $user->hope = $request->hope;
        $user->reason = $request->reason;

        $user->role_id = 5;

        $user->branch_id = 0;
        $user->tenant_id = $request->tenant_id;

        $user->save(); 

        $search = User::where('email', $user->email)->first();
        // $confirmationCode = str_random(40);
        $verifyUser = VerifyUser::create([
            'user_id' => $search->id,
            'token' => str_random(40)  
        ]);

        $token = VerifyUser::where(['user_id' => $search->id])->first();

        
        $sendMail = Mail::send([], ['name', 'BabaStudio'], function($message) use($user, $verifyUser) {
            $message->to($user->email, $user->username)
                ->subject('Verify Your Account Please')
                ->setBody((string)view('register-mail')->with('verifyUser', $verifyUser)->withUser($user), 'text/html');
            // $message->from('mr.edwinsetiawan@gmail.com', 'Edwin');
        });

        $certificate1 = new ECertificateSocialLearning;
        $certificate1->signature = 'sertifikat2.jpg';
        $certificate1->template = 'sertifikat2.jpg';
        $certificate1->departement = 'instructor';
        $certificate1->user_id = $user->id;
        $certificate1->save();

        $tenant = Tenant::find($request->tenant_id);

        $auth = Auth::guard('web')->attempt([
            'tenant_id' => $tenant->id,
            'email' => $request->email,
            'password' => $request->password
        ]);

        if ($auth) {

            Auth::user()->points()->create([
                'point' => 2,
                'user_id' => Auth::id()
            ]);

            Auth::user()->logs()->create([
                'reason' => 'LOG_LOGIN',
                'user_id' => Auth::id()
            ]);

            // return $tenant->domain;

            if (Auth::user()->role['id'] === 5) {
                // return redirect()->route('social-learning-posts.index');
                Auth::guard('web')->logout();

                return redirect()->route('login', $tenant->domain)->withErrors([
                    'login' => 'Silahkan cek dan verifikasi email anda',
                ]);;
            } else {
                return redirect()->route('dashboard', $tenant->domain);
            }

        } else {

            return redirect()->route('dashboard', $tenant->domain);
        }

    }

    public function resetPassword()
    {
        # code...
    }
}
