<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\NotificationSetting;
use App\NotificationSocialLearning;

use Auth;
use Session;

class NotificationSettingController extends Controller
{
    public function index()
    {
        $current_user = Auth::user();

        $notification_setting = NotificationSetting::where('user_id', $current_user->id)->first();

        if($notification_setting != null)
        {
            return view('notification.index')->withNotificationSetting($notification_setting);
        }
        else
        {
            $notification_setting = new NotificationSetting;
            $notification_setting->user_id = $current_user->id;
            $notification_setting->save();

            $notification_setting = NotificationSetting::where('user_id', $current_user->id)->first();
            return view('notification.index')->withNotificationSetting($notification_setting);
        }
        
    }

    public function update(Request $request)
    {
        $current_user = Auth::user();

        $notification_setting = NotificationSetting::where('user_id', $current_user->id)->first();

        if($notification_setting != null)
        {
            $notification_setting->is_student_open_course = $request->is_student_open_course != null ? true : false;
            $notification_setting->is_order_occur = $request->is_order_occur != null ? true : false;
            $notification_setting->is_ask_question = $request->is_ask_question != null ? true : false;
            $notification_setting->is_answer_question = $request->is_answer_question != null ? true : false;
            $notification_setting->is_cancel_subsciption = $request->is_cancel_subsciption != null ? true : false;
            $notification_setting->is_email_account_created = $request->is_email_account_created != null ? true : false;
            $notification_setting->is_email_weekly = $request->is_email_weekly != null ? true : false;
            $notification_setting->is_question_answered = $request->is_question_answered != null ? true : false;
            $notification_setting->is_email_purchase = $request->is_email_purchase != null ? true : false;

            $notification_setting->save();

            Session::flash('success', 'Berhasil memperbarui notifikasi !');

            return redirect()->back();
        }
        else
        {
            return redirect()->back();
        }
    }

    public function showAll()
    {
        $current_user = Auth::user();

        $notifications = NotificationSocialLearning::where(['user_id' => Auth::id()])->latest()->paginate(20);
        return view('notification.show-all')->withNotifications($notifications);
    }
}
