<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CoursePackage;

use Auth;
use Image;
use Session;

class CoursePackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $course_packages = CoursePackage::where(['user_id' => Auth::id()])->get();

        return view('course-packages.index')->withCoursePackages($course_packages);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('course-packages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($domain, Request $request)
    {
        $course_package = new CoursePackage;
        $course_package->title = $request->title;
        $course_package->description = $request->description;
        $course_package->is_publish = $request->is_publish == 'on';
        $course_package->price = $request->price;

        $course_package->category_id = $request->category_id;
        $course_package->sub_category_id = $request->sub_category_id;
        $course_package->tenant_id = Auth::user()->tenant_id;
        $course_package->user_id = Auth::id();
        
        // Image
        if ($request->hasFile('image')) {
            if ($course_package->image != null) {
                if (file_exists('/course-package/images/' . $course->image)) {
                    unlink(public_path('/course-package/images/' . $course->image));
                }
            }
            
            $image = $request->file('image');
            $filename = time() . '.' . $image->getClientOriginalName();
            $path = public_path('/course-package/images/' . $filename);
            Image::make($image->getRealPath())->save($path);
            $course_package->image = $filename;

        }

        $course_package->save();

        Session::flash('success', 'Berhasil menambahkan paket kursus !');

        return redirect()
            ->route('course-packages.index', $_SERVER['HTTP_HOST']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($domain, $id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($domain, $id)
    {
        $selected_ids = [];
        $course_package = CoursePackage::find($id);

        $selected_courses = $course_package->packages;

        // return $selected_courses;
        
        foreach($selected_courses as $course) {
            array_push($selected_ids, $course->id);
        }

        $courses = Auth::user()->tenant->courses()->whereNotIn('id', $selected_ids)->get();

        return view('course-packages.edit')
            ->withCoursePackage($course_package)
            ->withSelectedCourses($selected_courses)
            ->withCourses($courses);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($domain, Request $request, $id)
    {
        $course_package = CoursePackage::find($id);
        $course_package->title = $request->title;
        $course_package->description = $request->description;
        $course_package->is_publish = $request->is_publish == 'on';
        $course_package->price = $request->price;

        $course_package->category_id = $request->category_id;
        $course_package->sub_category_id = $request->sub_category_id;
        $course_package->tenant_id = Auth::user()->tenant_id;
        $course_package->user_id = Auth::id();
        
        // Image
        if ($request->hasFile('image')) {
            
            if ($course_package->image != null) {
                if (file_exists('/course-package/images/' . $course_package->image)) {
                    unlink(public_path('/course-package/images/' . $course_package->image));
                }
            }
            
            $image = $request->file('image');
            $filename = time() . '.' . $image->getClientOriginalName();
            $path = public_path('/course-package/images/' . $filename);
            Image::make($image->getRealPath())->save($path);
            $course_package->image = $filename;

        }

        $course_package->save();

        return redirect()
            ->route('course-packages.index', $_SERVER['HTTP_HOST']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($domain, $id)
    {
        $course_package = CoursePackage::find($id);

        if($course_package != null) {
            $course_package->packages()->detach();

            $course_package->delete();

            return response()->json($course_package, 200);
        } else {
            // Data not found
            return response()->json('The data you are referring to is not found!', 400);
        }

        return response()->json('Oops something when wrong!', 500);
    }

    public function select($domain, $id, $course_id)
    {
        $course_package = CoursePackage::find($id);

        $course_package->packages()->attach($course_id);

        return redirect()->back();
    }

    public function unselect($domain, $id, $course_id)
    {
        $course_package = CoursePackage::find($id);

        $course_package->packages()->detach($course_id);

        return redirect()->back();
    }

    public function join($domain, $id, $user_id)
    {
        $course_package = CoursePackage::find($id);

        $course_package->users()->attach($user_id);

        return redirect()->back();
    }

    public function leave($domain, $id, $user_id)
    {
        $course_package = CoursePackage::find($id);

        $course_package->users()->detach($user_id);

        return redirect()->back();
    }
}
