<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\ReviewSocialLearning;

use Auth;

class CourseSocialLearningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::where(['tenant_id' => Auth::user()->tenant_id])->paginate(5);

        return view('social-learnings.course.index')->withCourse($courses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($domain, $id)
    {
        $course = Course::find($id);

        $my_review = $course->reviews()->where('user_id', Auth::id())->first();

        return view('social-learnings.course.show')
            ->withCourse($course)
            ->withMyReview($my_review);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function loadAllCourses()
    {
        // $courses = Auth::user()->tenant->courses()->paginate(12);
        $courses = Auth::user()->tenant->courses;

        return view('social-learnings.course.child.child-all-course')->withCourses($courses);
    }

    public function loadSearchAllCourses($domain, $keyword)
    {
        $courses = Auth::user()->tenant->courses()->where('title', 'like', '%' . $keyword . '%')->paginate(12);

        return response()->json([
            'view' => (string)view('social-learnings.course.child.child-all-course')->withCourses($courses),
            'count' => $courses->count()
        ], 200);
    }

    public function loadAllCategories()
    {
        $categories = Auth::user()->tenant->categories;

        return response()->json($categories, 200);
    }

    public function loadAllCoursesByPrices()
    {
        // $courses = Auth::user()->tenant->courses()->paginate(12);
        $courses = Auth::user()->tenant->courses()->orderBy('price', 'desc')->get();

        return view('social-learnings.course.child.child-all-course')->withCourses($courses);
    }

    public function loadAllCoursesByPopular()
    {
        // $courses = Auth::user()->tenant->courses()->paginate(12);
        $courses = Auth::user()->tenant->courses->sortByDesc(function($course){
            return $course->users()->count();
        });

        // $courses = Auth::user()->tenant->courses;

        return view('social-learnings.course.child.child-all-course')->withCourses($courses);
    }

    public function rating(Request $request)
    {
        $review = new ReviewSocialLearning;
        $review->rate = $request->star;
        $review->suggestion = $request->suggestion;
        $review->course_id = $request->course_id;
        $review->user_id = Auth::id();

        $review->save();

        return redirect()->back();
    }

    public function updateRating(Request $request)
    {
        $review = ReviewSocialLearning::where([
            'user_id' => Auth::id(),
            'course_id' => $request->course_id
        ])->first();
        
        $review->rate = $request->star;
        $review->suggestion = $request->suggestion;

        $review->save();

        return redirect()->back();
    }

    public function deleteRating(Request $request)
    {
        $review = ReviewSocialLearning::where([
            'user_id' => Auth::id(),
            'course_id' => $request->course_id
        ])->first();
        
        $review->delete();

        return redirect()->back();
    }
    

}
