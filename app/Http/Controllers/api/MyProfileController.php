<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;

use Auth;
use Image;
use App\User;
use Hash;
use Session;

use App\ExchangePointHistory;
use App\Course;
use App\BadgeSocialLearning;
use DB;

use App\Http\Requests\UpdateProfile;
use App\Http\Requests\api\ChangePhotoRequestValidation;

/**
 * @resource My Profile
 *
 * This module requires full authentication
 *  Headers : 
 *    - Authorization : "Bearer ACCESS_TOKEN"
 * 
 */
class MyProfileController extends Controller
{

    /**
     * My Profile
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $current_user = Auth::guard('api')->user();

        // return $current_user;

        if($current_user != null)
        {
            $user = User::with('role', 'courses', 'followers', 'followings', 'orderHistories')->find($id);

            if($user->photo != null)
            {
                $user->photo = env('APP_URL') . '/images/photo/' . $user->photo;
            }

            return response()->json($user, 200);

        } else {
            // Unauthorize
            return response()->json('Unauthorize', 401);
        }
        
        return response()->json('Oops something when wrong!', 500);
    }

    /**
     * I - Friend
     *
     * @return \Illuminate\Http\Response
     */
    public function iFriend($id)
    {
        $current_user = Auth::guard('api')->user();

        if($current_user != null)
        {
            $users = User::paginate(5);
            foreach($users as $user)
            {
                if($user->photo != null)
                {
                    $user->photo = env('APP_URL') . '/images/photo/' . $user->photo;
                }
            }

            // Response Oke
            return response()->json($users, 200);

        } else {
            // Unauthorize
            return response()->json('Unauthorize', 401);
        }
        
        return response()->json('Oops something when wrong!', 500);
    }

    /**
     * My Courses
     * 
     * @response [
     * {
     *   "id": 2,
     *   "title": "Intro",
     *   "description": "nnn",
     *   "duration": 0,
     *   "status": "POSTED",
     *   "price": 0,
     *   "suitable": null,
     *   "requirement": null,
     *   "can_be": null,
     *   "image": null,
     *   "video": null,
     *   "wistia_hashed_id": "ubxlnyti20",
     *   "category_id": 1,
     *   "sub_category_id": 0,
     *   "tenant_id": 1,
     *   "author_id": 1,
     *   "is_enable_review": 1,
     *   "created_at": "2018-04-13 11:55:21",
     *   "updated_at": "2018-04-13 11:55:21",
     *   "pivot": {
     *       "user_id": 1,
     *       "course_id": 2
     *   }
     *  }
     * ]
     * 
     */
    public function myCourses($id)
    {
        $current_user = Auth::guard('api')->user();

        if($current_user != null)
        {
            $courses = $current_user->courses()->with('users')->paginate(5);
            
            foreach($courses as $course)
            {
                if($course->image != null)
                {
                    // $course->image = base64_encode(Image::make(public_path('xxcourses/images/' . $course->image))->encode());
                    $course->image = env('APP_URL') . '/xxcourses/images/' . $course->image;
                }
            }

            return response()->json($courses, 200);

        } else {
            // Unauthorize
            return response()->json('Unauthorize', 401);
        }
        
        return response()->json('Oops something when wrong!', 500);
    }

    public function changePhoto($id, Request $request)
    {
        // return $request->photo;
        $current_user = Auth::guard('api')->user();

        // return $current_user;
        if($current_user != null)
        {
            $user = User::find($id);

            $photo = Image::make($request->photo);

            // return $photo;
            $filename = time() . '.' . $user->fullname . '.png';
            $path = public_path('images/photo/' . $filename);
            Image::make($photo)->save($path);
            $user->photo = $filename;
            
            $user->save();

            if($user->photo != null)
            {
                $user->photo = env('APP_URL') . '/images/photo/' . $user->photo;

                return response()->json([
                    'photo' => $user->photo
                ], 200);
            } else {
                return response()->json('Oops something when wrong!', 500);
            }

            
        } else {
            // Unauthorize
            return response()->json('Unauthorize', 401);
        }
        
        return response()->json('Oops something when wrong!', 500);
    }

    public function updateProfile($id, Request $request)
    {
        $user = Auth::guard('api')->user();        

        // If User Not Found
        if($user == null)
        {
            return response()->json('Unauthorized', 401);
        }

        if($request->fullname != null)
        {
            $user->fullname = $request->fullname;
        }

        if($request->ktp_id != null)
        {
            $user->ktp_id = $request->ktp_id;
        }

        // $user->username = $request->username;

        if($request->password != null)
        {
            $user->password = Hash::make($request->password);
        }
        
        // $user->email = $request->email;
        $user->birth_date = $request->birth_date;
        $user->phone = $request->phone;
        $user->mobile = $request->mobile;
        $user->mobile2 = $request->mobile2;
        $user->pin_bb = $request->pin_bb;
        $user->notes = $request->notes;

        $user->address = $request->address;
        $user->zipcode = $request->zipcode;
        $user->fb = $request->fb;
        $user->tw = $request->tw;
        $user->website = $request->website;
        $user->status = $request->status;
        $user->hobby = $request->hobby;
        $user->reason = $request->reason;
        $user->city = $request->city;
        if($request->sex != null)
        {
            $user->sex = $request->sex;
        }
        
        $user->is_partnership = $request->is_partnership;

        $user->save();

        $user->photo = env('APP_URL') . '/images/photo/' . $user->photo;

        return response()->json($user, 200);
    }

    public function exchangePointList($id)
    {
        $current_user = Auth::guard('api')->user();

        if($current_user != null)
        {
            $all_courses = Course::where(['tenant_id' => User::find($current_user->id)->tenant_id, 'status' => 'POSTED'])->with('users')->paginate(5);
            
            foreach($all_courses as $course)
            {
                if($course->image != null)
                {
                    $course->image = env('APP_URL') . '/xxcourses/images/' . $course->image;
                }

            }

            return response()->json($all_courses, 200);
            
        } else {
            // Unauthorize
            return response()->json('Unauthorize', 401);
        }

        return response()->json('Oops something when wrong!', 500);
    }

    public function sampleExchange($user_id, $id)
    {
        $current_user = Auth::guard('api')->user();
        $course = Course::find($id);

        if($current_user != null)
        {
            if($course != null)
            {
                if ($current_user->points()->sum('point') > 1000) 
                {
                    $course->users()->attach(Auth::id());

                    $current_user->gems()->create([
                        'gem' => 100,
                        'user_id' => $current_user->id
                    ]);
    
                    $history = new ExchangePointHistory;
                    $history->exchange_date = '2000-01-21';
                    $history->course_id = $course->id;
                    $history->user_id = $current_user->id;
    
                    $history->save();

                    $current_user->points()->create([
                        'point' => -10000,
                        'user_id' => $current_user->id
                    ]);

                    if($course->image != null)
                    {
                        $course->image = base64_encode(Image::make(public_path('xxcourses/images/' . $course->image))->encode());
                    }

                    return response()->json($course, 200);

                } else {
                    // Point is not enough
                    return response()->json('point is not enough', 400);
                }
            } else {
                // Data not found
                return response()->json('The data you are referring to is not found!', 400);
            }
            
        } else {
            // Unauthorize
            return response()->json('Unauthorize', 401);
        }

        return response()->json('Oops something when wrong!', 500);
    }

    public function points($id)
    {
        $user = User::find($id);
        if($user != null) {
            $points = $user->points->sum('point');
            $gems = $user->points->sum('gems');

            return response()->json([
                'points' => $points,
                'gems' => $gems
            ], 200);
        } else {
            // Unauthorize
            return response()->json('Unauthorize', 401);
        }
        
        return response()->json('Oops something when wrong!', 500);
    }

    public function badges($id)
    {
        $user = User::find($id);
        if($user != null) {
            $badges = BadgeSocialLearning::all();

            foreach($badges as $badge)
            {
                if($badge->simple_name == 'badge_make_friend') {

                    $badge->logo = 'users';

                    if($user->followers()->count() >= 10) {
                        $badge->is_owned = true;
                    } else {
                        $badge->is_owned = false;
                    }

                } else if($badge->simple_name == 'badge_fans') {

                    $badge->logo = 'star';

                    if($user->followers()->count() >= 50) {
                        $badge->is_owned = true;
                    } else {
                        $badge->is_owned = false;
                    }

                } else if($badge->simple_name == 'badge_popular') {

                    $badge->logo = 'fire';
                    
                    if($user->followers()->count() >= 100) {
                        $badge->is_owned = true;
                    } else {
                        $badge->is_owned = false;
                    }

                } else if($badge->simple_name == 'badge_alumni') {

                    $badge->logo = 'graduation-cap';
                    
                    $submitted_encounter = DB::table('user_encounters')->where([
                        'user_id' => $user->id
                    ])->first();
                    
                    if($submitted_encounter != null) {
                        $badge->is_owned = true;
                    } else {
                        $badge->is_owned = false;
                    }
                    
                } else if($badge->simple_name == 'badge_welcome') {

                    $badge->logo = 'sign-out';

                    $login_hour = DB::table('logs')->where([
                        'reason' => 'LOG_LOGIN',
                        'user_id' => $user->id
                    ])->count();

                    if($login_hour > 7) {
                        $badge->is_owned = true;
                    } else {
                        $badge->is_owned = false;
                    }

                } else if($badge->simple_name == 'badge_nocturnal') {

                    $badge->logo = 'eye';

                    $login_hour = DB::table('logs')->where([
                        'reason' => 'LOG_LOGIN',
                        'user_id' => $user->id
                    ])->count();

                    if($login_hour > 7) {
                        $badge->is_owned = true;
                    } else {
                        $badge->is_owned = false;
                    }

                } else if($badge->simple_name == 'badge_i_time') {

                    $badge->logo = 'clock-o';
                    $i_time = DB::table('itime_social_learnings')->where('user_id', $user->id)->first();

                    if($i_time != null) {
                        $badge->is_owned = true;
                    } else {
                        $badge->is_owned = false;
                    }

                }
                
            }

            return response()->json([
                'badges' => $badges
            ], 200);
        } else {
            // Unauthorize
            return response()->json('Unauthorize', 401);
        }
        
        return response()->json('Oops something when wrong!', 500);
    }

    public function userSuggested($id)
    {
        $current_user = User::find($id);

        if($current_user != null) {
            $users = User::where('role_id', '=', '5')
                ->where('id', '<>', $current_user->id)
                ->whereNotIn('id', $current_user->followings)
                ->inRandomOrder()
                ->take(5)
                ->get();

            foreach($users as $user) {
                $user->photo = $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null;
            }

            return response()->json($users, 200);
        } else {
            // Unauthorize
            return response()->json('Unauthorize', 401);
        }

        return response()->json('Oops something when wrong!', 500);
        
    }
}
