<?php

namespace App\Http\Controllers\api;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;
use App\PostSocialLearning;
use App\Comment;
use App\CommentLike;
use App\CommentDislike;
use App\PostLike;
use App\PostDislike;
use App\LinkPostSocialLearning;
use App\User;

use Image;
use File;
use Carbon\Carbon;

use App\Http\Requests\StorePostSocialLearningRequest;
use App\Http\Requests\UpdatePostSocialLearningRequest;
use App\Http\Requests\StoreCommentPostSocialLearningRequest;

use Illuminate\Support\Facades\Session;

class PostSocialLearningController extends Controller
{

    public function index()
    {
      $posts = PostSocialLearning::with(
        'likes', 
        'dislikes', 
        'postComments', 
        'postComments.likes', 
        'postComments.dislikes',
        'link'
      )
            ->where('user_id', Auth::id())
            ->orWhereIn('user_id', Auth::user()->followings)
            ->latest()
            ->paginate(5);

      foreach($posts as $post)
      {
        $user = User::find($post->user_id);
        
        if($user != null) {
          $post->user = [
            'fullname' => $user->fullname,
            'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
          ];

        }
        
        if($post->photo != null)
        {
          $post->photo = env('APP_URL') . '/images/photo/' . $post->photo;
        }
        
        // Likes
        foreach($post->likes as $like)
        {
          $user = User::find($like->user_id);
        
          if($user != null) {
            $like->user = [
              'fullname' => $user->fullname,
              'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
            ];

          }
          
        }

        // Dislikes
        foreach($post->dislikes as $dislike)
        {
          $user = User::find($dislike->user_id);
        
          if($user != null) {
            $dislike->user = [
              'fullname' => $user->fullname,
              'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
            ];

          }
          
        }

        // Comments
        foreach($post->postComments as $comment)
        {
          $user = User::find($comment->user_id);
        
          if($user != null) {
            $comment->user = [
              'fullname' => $user->fullname,
              'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
            ];

          }
          
        }

      }

      return response()->json($posts, 200);
    }


    public function create()
    {
        //
    }

    public function store(Request $request)
    {
      $current_user = Auth::guard('api')->user();

      if($current_user != null)
      {
        // Oke
        $post = new PostSocialLearning;

        if($request->photo != null)
        {
          $photo = Image::make($request->photo);

          $filename = time() . '.' . $current_user->id . str_replace('image/', '.', $photo->mime());
          $path = public_path('images/photo/' . $filename);
          Image::make($photo)->save($path);
          $post->photo = $filename;
          
        }

        $post->content = $request->content;
        $post->user_id = $current_user->id;
        $post->tenant_id = $current_user->id;
        $post->save();

        if($request->link != null)
        {
            $link = new LinkPostSocialLearning;
            $link->link = $request->link;
            $link->post_id = $post->id;
            $link->user_id = $current_user->id;

            $link->save();
        }

        $post = PostSocialLearning::with(
          'likes', 
          'dislikes', 
          'postComments', 
          'postComments.likes', 
          'postComments.dislikes',
          'link'
        )->find($post->id);

        $user = User::find($post->user_id);
    
        if($user != null) {
          $post->user = [
            'fullname' => $user->fullname,
            'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
          ];

        }

        if($post->photo != null)
        {
          // $post->photo = base64_encode(Image::make(public_path('images/photo/' . $post->photo))->encode());
          $post->photo = env('APP_URL') . '/images/photo/' . $post->photo;
        }

        // Likes
        foreach($post->likes as $like)
        {
          $user = User::find($like->user_id);
        
          if($user != null) {
            $like->user = [
              'fullname' => $user->fullname,
              'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
            ];

          }
          
        }

        // Dislikes
        foreach($post->dislikes as $dislike)
        {
          $user = User::find($dislike->user_id);
        
          if($user != null) {
            $dislike->user = [
              'fullname' => $user->fullname,
              'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
            ];

          }
          
        }

        // Comments
        foreach($post->postComments as $comment)
        {
          $user = User::find($comment->user_id);
        
          if($user != null) {
            $comment->user = [
              'fullname' => $user->fullname,
              'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
            ];

          }
          
        }

        return response()->json($post, 200);
          
      } else {
        // Unauthorize
        return response()->json('Unauthorize', 401);
      }
      
      return response()->json('Oops something when wrong!', 500);

      // -------------------------------------------------------
    }


    public function show($id)
    {
      $current_user = Auth::guard('api')->user();

      if($current_user != null)
      {
        $post = PostSocialLearning::with(
          'likes', 
          'dislikes', 
          'postComments', 
          'postComments.likes', 
          'postComments.dislikes',
          'link'
          )->find($id);

        $user = User::find($post->user_id);
      
        if($user != null) {
          $post->user = [
            'fullname' => $user->fullname,
            'photo' => env('APP_URL') . '/images/photo/' . $post->photo
          ];

        }

        if($post != null)
        {
          if($post->photo != null)
          {
            // $post->photo = base64_encode(Image::make(public_path('images/photo/' . $post->photo))->encode());
            $post->photo = env('APP_URL') . '/images/photo/' . $post->photo;
          }

          return response()->json($post, 200); 
        } else {
          // Data not found
          return response()->json('The data you are referring to is not found!', 400);
        }
          
      } else {
          // Unauthorize
          return response()->json('Unauthorize', 401);
      }
      
      return response()->json('Oops something when wrong!', 500);
      
      // -----------------------------------------------------------
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
      $current_user = Auth::guard('api')->user();

      if($current_user != null)
      {
        // Oke
        $post = PostSocialLearning::find($id);

        if($post != null)
        {

          if($current_user->id == $post->user_id)
          {
            if($request->photo != null)
            {
              $photo = Image::make($request->photo);

              $filename = time() . '.' . $current_user->id . '.png';
              $path = public_path('images/photo/' . $filename);
              Image::make($photo)->save($path);
              File::delete(public_path('images/photo/' . $post->photo));
              $post->photo = $filename;
            }

            $post->content = $request->content;
            $post->save();

            if($request->link != null)
            {
              $link = LinkPostSocialLearning::where('post_id', $post->id);

              if($link != null)
              {

                $link->link = $request->link;
                $link->post_id = $post->id;
                $link->user_id = $current_user->id;

                $link->save();
              } else {
                // Data not found
                return response()->json('The data you are referring to is not found!', 400);
              }
            }

            $post = PostSocialLearning::with(
              'likes', 
              'dislikes', 
              'postComments', 
              'postComments.likes', 
              'postComments.dislikes',
              'link'
            )->find($post->id);

            $user = User::find($post->user_id);
        
            if($user != null) {
              $post->user = [
                'fullname' => $user->fullname,
                'photo' => env('APP_URL') . '/images/photo/' . $user->photo
              ];

            }
              
            if($post->photo != null)
            {
              // $post->photo = base64_encode(Image::make(public_path('images/photo/' . $post->photo))->encode());
              $post->photo = env('APP_URL') . '/images/photo/' . $post->photo;
            }

            return response()->json($post, 200);

          } else {
            // Forbidden
            return response()->json('This data not belongs to you!', 403);    
          }

        } else {
          // Data not found
          return response()->json('The data you are referring to is not found!', 400);
        }

      } else {
        // Unauthorize
        return response()->json('Unauthorize', 401);
      }
      
      return response()->json('Oops something when wrong!', 500);

      // -------------------------------------------------------
    }

    /**
     * api/v1/social-learning-posts/{post_id}
     *
     */
    public function destroy($id)
    {
      $current_user = Auth::guard('api')->user();

      if($current_user != null)
      {
        
        $post = PostSocialLearning::with(
          'likes', 
          'dislikes', 
          'postComments', 
          'postComments.likes', 
          'postComments.dislikes',
          'link'
          )->find($id);

        if($post != null)
        {
          if($current_user->id == $post->user_id)
          {
            // Oke
            $post->likes()->delete();
            $post->dislikes()->delete();
            foreach($post->postComments as $comment)
            {
              $comment->likes()->delete();
              $comment->dislikes()->delete();
            }
            $post->postComments()->delete();
            $post->link()->delete();
            File::delete(public_path('images/photo/' . $post->photo));
            $post->delete();

            return response()->json($post, 200);

          } else {
            // Forbidden
            return response()->json('This data not belongs to you!', 403);    
          }

        } else {
          // Data not found
          return response()->json('The data you are referring to is not found!', 400);
        }

      } else {
        // Unauthorize
        return response()->json('Unauthorize', 401);
      }

      return response()->json('Oops something when wrong!', 500);

    }

    public function like(Request $request, $id)
    {

      // return $id;
      $current_user = Auth::guard('api')->user();

      if($current_user != null)
      {
        
        $post = PostSocialLearning::with(
          'likes', 
          'dislikes', 
          'postComments', 
          'postComments.likes', 
          'postComments.dislikes',
          'link'
          )->find($id);

        if($post != null)
        {
          // Oke
          $like = PostLike::where([
            'post_id' => $post->id,
            'user_id' => $current_user->id
            ])->first();
    
          $dislike = PostDislike::where([
            'post_id' => $post->id,
            'user_id' => $current_user->id
            ])->first();
    
          if($like != null)
          {
            $like->delete();
    
          } else {
            $post->likes()->save(new PostLike([
              'post_id' => $post->id,
              'user_id' => $current_user->id
            ]));
    
          }
    
          if($dislike != null)
          {
            $dislike->delete();
          }

          $post = PostSocialLearning::with(
            'likes', 
            'dislikes', 
            'postComments', 
            'postComments.likes', 
            'postComments.dislikes',
            'link'
          )->find($id);

          $user = User::find($post->user_id);
        
          if($user != null) {
            $post->user = [
              'fullname' => $user->fullname,
              'photo' => env('APP_URL') . '/images/photo/' . $user->photo
            ];

          }

          if($post->photo != null)
          {
            $post->photo = env('APP_URL') . '/images/photo/' . $post->photo;
          }
    
          return response()->json($post, 200);

        } else {
          // Data not found
          return response()->json('The data you are referring to is not found!', 400);
        }

      } else {
        // Unauthorize
        return response()->json('Unauthorize', 401);
      }

      return response()->json('Oops something when wrong!', 500);

      // --------------------------------------------------------

    }

    public function dislike(Request $request, $id)
    {
      $current_user = Auth::guard('api')->user();

      if($current_user != null)
      {
        
        $post = PostSocialLearning::with(
          'likes', 
          'dislikes', 
          'postComments', 
          'postComments.likes', 
          'postComments.dislikes',
          'link'
          )->find($id);

        if($post != null)
        {
          // Oke
          $like = PostLike::where([
            'post_id' => $post->id,
            'user_id' => $current_user->id
            ])->first();
    
          $dislike = PostDislike::where([
            'post_id' => $post->id,
            'user_id' => $current_user->id
            ])->first();
    
          if($dislike != null)
          {
            $dislike->delete();
    
          } else {
            $post->likes()->save(new PostDislike([
              'post_id' => $post->id,
              'user_id' => $current_user->id
            ]));
    
          }
    
          if($like != null)
          {
            $like->delete();
          }
    
          $post = PostSocialLearning::with(
            'likes', 
            'dislikes', 
            'postComments', 
            'postComments.likes', 
            'postComments.dislikes',
            'link'
          )->find($id);

          $user = User::find($post->user_id);
        
          if($user != null) {
            $post->user = [
              'fullname' => $user->fullname,
              'photo' => env('APP_URL') . '/images/photo/' . $user->photo
            ];

          }
                
          if($post->photo != null)
          {
            $post->photo = env('APP_URL') . '/images/photo/' . $post->photo;
          }
    
          return response()->json($post, 200);

        } else {
          // Data not found
          return response()->json('The data you are referring to is not found!', 400);
        }

      } else {
        // Unauthorize
        return response()->json('Unauthorize', 401);
      }

      return response()->json('Oops something when wrong!', 500);

      // --------------------------------------------------------
    }

    public function postComment(Request $request, $id)
    {
      $current_user = Auth::guard('api')->user();

      if($current_user != null)
      {
        
        $post = PostSocialLearning::with(
          'likes', 
          'dislikes', 
          'postComments', 
          'postComments.likes', 
          'postComments.dislikes',
          'link'
        )->find($id);

        if($post != null)
        {
          // Oke
          $comment = $post->postComments()->save(new Comment([
            'question_id' => 0,
            'post_id' => $post->id,
            'user_id' => $current_user->id,
            'body' => $request->content,
          ]));

          $post = PostSocialLearning::with(
            'likes', 
            'dislikes', 
            'postComments', 
            'postComments.likes', 
            'postComments.dislikes',
            'link'
          )->find($id);

          $user = User::find($post->user_id);
        
          if($user != null) {
            $post->user = [
              'fullname' => $user->fullname,
              'photo' => env('APP_URL') . '/images/photo/' . $user->photo
            ];

          }
                
          if($post->photo != null)
          {
            $post->photo = env('APP_URL') . '/images/photo/' . $post->photo;
          }
    
          return response()->json($post, 200);

        } else {
          // Data not found
          return response()->json('The data you are referring to is not found!', 400);
        }

      } else {
        // Unauthorize
        return response()->json('Unauthorize', 401);
      }

      return response()->json('Oops something when wrong!', 500);

      // --------------------------------------------------------
      
    }

    public function likeComment($id)
    {
      $comment = Comment::find($id);
      $post = PostSocialLearning::with('likes', 'dislikes', 'postComments')->find($comment->post_id);

      $like = CommentLike::where([
        'comment_id' => $comment->id,
        'user_id' => Auth::id()
        ])->first();

      $dislike = CommentDislike::where([
        'comment_id' => $comment->id,
        'user_id' => Auth::id()
        ])->first();

      if($like != null)
      {
        $like->delete();

      } else {
        $comment->likes()->save(new CommentLike([
          'comment_id' => $comment->id,
          'user_id' => Auth::id()
        ]));

      }

      if($dislike != null)
      {
        $dislike->delete();
      }

      $post = PostSocialLearning::with(
        'likes', 
        'dislikes', 
        'postComments', 
        'postComments.likes', 
        'postComments.dislikes',
        'link'
      )->find($id);

      $user = User::find($post->user_id);
    
      if($user != null) {
        $post->user = [
          'fullname' => $user->fullname,
          'photo' => env('APP_URL') . '/images/photo/' . $user->photo
        ];

      }
            
      if($post->photo != null)
      {
        $post->photo = env('APP_URL') . '/images/photo/' . $post->photo;
      }

      return response()->json($post, 200);

    }

    public function dislikeComment($id)
    {
      $comment = Comment::find($id);
      $post = PostSocialLearning::with('likes', 'dislikes', 'postComments')->find($comment->post_id);

      $like = CommentLike::where([
        'comment_id' => $comment->id,
        'user_id' => Auth::id()
        ])->first();

      $dislike = CommentDislike::where([
        'comment_id' => $comment->id,
        'user_id' => Auth::id()
        ])->first();

      if($dislike != null)
      {
        $dislike->delete();

      } else {
        $comment->likes()->save(new CommentDislike([
          'comment_id' => $comment->id,
          'user_id' => Auth::id()
        ]));

      }

      if($like != null)
      {
        $like->delete();
      }

      $post = PostSocialLearning::with(
        'likes', 
        'dislikes', 
        'postComments', 
        'postComments.likes', 
        'postComments.dislikes',
        'link'
      )->find($id);

      $user = User::find($post->user_id);
    
      if($user != null) {
        $post->user = [
          'fullname' => $user->fullname,
          'photo' => env('APP_URL') . '/images/photo/' . $user->photo
        ];

      }
            
      if($post->photo != null)
      {
        $post->photo = env('APP_URL') . '/images/photo/' . $post->photo;
      }

      return response()->json($post, 200);
    }

    /**
     * api/v1/social-learning-posts/{post_id}/image
     * 
     * This request will return image as string Base64
     * 
     */
    public function getImage($id)
    {
      $post = PostSocialLearning::find($id);

      // return public_path('images/photo/' . $post->photo);

      $image = Image::make(public_path('images/photo/' . $post->photo));
      if($post->photo != null)
      {
        $post->photo = base64_encode(Image::make(public_path('images/photo/' . $post->photo))->encode());
        // $post->photo = env('HTTP_HOST') . '/social-learning-posts/' . $post->id . '/image';
      }

      return response()->json(['photo' => $post->photo], 200);
    }

    public function getByUser($id)
    {
      $user = User::find($id);

      if($user != null)
      {
        $posts = PostSocialLearning::with(
          'likes', 
          'dislikes', 
          'postComments', 
          'postComments.likes', 
          'postComments.dislikes',
          'link'
          )
              ->where('user_id', $user->id)
              ->latest()
              ->paginate(5);

        foreach($posts as $post)
        {
          $user = User::find($post->user_id);
        
          if($user != null) {
            $post->user = [
              'fullname' => $user->fullname,
              'photo' => env('APP_URL') . '/images/photo/' . $post->photo
            ];

          }
          
          if($post->photo != null)
          {
            // $post->photo = base64_encode(Image::make(public_path('images/photo/' . $post->photo))->encode());
            $post->photo = env('APP_URL') . '/images/photo/' . $post->photo;
          }
        }

        return response()->json($posts, 200);
      } else {
        // Data not found
        return response()->json('The data you are referring to is not found!', 400);
      }

      return response()->json('Oops something when wrong!', 500);
    }

}
