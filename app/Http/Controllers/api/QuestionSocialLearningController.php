<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\QuestionSocialLearning;
use App\Comment;
use App\QuestionLike;
use App\QuestionDislike;
use App\CommentLike;
use App\CommentDislike;
use App\User;

use Auth;

class QuestionSocialLearningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $current_user = Auth::guard('api')->user();

        if($current_user != null)
        {
            // Oke
            $questions = QuestionSocialLearning::with(
                'likes', 
                'dislikes', 
                'comments', 
                'comments.likes', 
                'comments.dislikes'
                )->where('user_id', $current_user->id)
                    ->orWhereIn('user_id', $current_user->followings)
                    ->latest()
                    ->paginate(5);

            foreach($questions as $question) {
                $user = User::find($question->user_id);
        
                if($user != null) {
                    $question->user = [
                        'fullname' => $user->fullname,
                        'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                    ];

                }
                
                if($question->photo != null)
                {
                    $question->photo = env('APP_URL') . '/images/photo/' . $question->photo;
                }
                
                // Likes
                foreach($question->likes as $like)
                {
                    $user = User::find($like->user_id);
                    
                    if($user != null) {
                        $like->user = [
                            'fullname' => $user->fullname,
                            'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                
                }

                // Dislikes
                foreach($question->dislikes as $dislike)
                {
                    $user = User::find($dislike->user_id);
                    
                    if($user != null) {
                        $dislike->user = [
                            'fullname' => $user->fullname,
                            'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                
                }

                // Comments
                foreach($question->comments as $comment)
                {
                    $user = User::find($comment->user_id);
                    
                    if($user != null) {
                        $comment->user = [
                            'fullname' => $user->fullname,
                            'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                
                }
            }

            return response()->json($questions, 200);
        } else {
            // Unauthorize
            return response()->json('Unauthorize', 401);
        }

        return response()->json('Oops something when wrong!', 500);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $current_user = Auth::guard('api')->user();

        if($current_user != null)
        {
            // Oke
            $question = new QuestionSocialLearning;

            $question->content = $request->content;
            $question->user_id = $current_user->id;
            $question->tenant_id = $current_user->tenant_id;
            $question->material_id = 0;
            $question->save();

            $question = QuestionSocialLearning::with(
                'likes', 
                'dislikes', 
                'comments', 
                'comments.likes', 
                'comments.dislikes'
                )->find($question->id);

            $user = User::find($question->user_id);
    
            if($user != null) {
                $question->user = [
                    'fullname' => $user->fullname,
                    'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                ];

            }
            
            if($question->photo != null)
            {
                $question->photo = env('APP_URL') . '/images/photo/' . $question->photo;
            }
            
            // Likes
            foreach($question->likes as $like)
            {
                $user = User::find($like->user_id);
                
                if($user != null) {
                    $like->user = [
                        'fullname' => $user->fullname,
                        'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                    ];

                }
            
            }

            // Dislikes
            foreach($question->dislikes as $dislike)
            {
                $user = User::find($dislike->user_id);
                
                if($user != null) {
                    $dislike->user = [
                        'fullname' => $user->fullname,
                        'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                    ];

                }
            
            }

            // Comments
            foreach($question->comments as $comment)
            {
                $user = User::find($comment->user_id);
                
                if($user != null) {
                    $comment->user = [
                    'fullname' => $user->fullname,
                    'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                    ];

                }
            
            }

            return response()->json($question, 200);
        } else {
            // Unauthorize
            return response()->json('Unauthorize', 401);
        }
        
        return response()->json('Oops something when wrong!', 500);

        // ----------------------------------------
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $current_user = Auth::guard('api')->user();

        if($current_user != null)
        {
            $question = QuestionSocialLearning::with(
                'likes', 
                'dislikes', 
                'comments', 
                'comments.likes', 
                'comments.dislikes'
                )->find($id);

            if($question != null)
            {
                // Ok
                $user = User::find($question->user_id);
    
                if($user != null) {
                    $question->user = [
                        'fullname' => $user->fullname,
                        'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                    ];

                }
                
                if($question->photo != null)
                {
                    $question->photo = env('APP_URL') . '/images/photo/' . $question->photo;
                }
                
                // Likes
                foreach($question->likes as $like)
                {
                    $user = User::find($like->user_id);
                    
                    if($user != null) {
                        $like->user = [
                            'fullname' => $user->fullname,
                            'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                
                }

                // Dislikes
                foreach($question->dislikes as $dislike)
                {
                    $user = User::find($dislike->user_id);
                    
                    if($user != null) {
                        $dislike->user = [
                            'fullname' => $user->fullname,
                            'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                
                }

                // Comments
                foreach($question->comments as $comment)
                {
                    $user = User::find($comment->user_id);
                    
                    if($user != null) {
                        $comment->user = [
                        'fullname' => $user->fullname,
                        'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                
                }
                
                return response()->json($question, 200);

            } else {
                // Data not found
                return response()->json('The data you are referring to is not found!', 400);
            }
            
        } else {
            // Unauthorize
            return response()->json('Unauthorize', 401);
        }
        
        return response()->json('Oops something when wrong!', 500);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $current_user = Auth::guard('api')->user();

        if($current_user != null)
        {
            $question = QuestionSocialLearning::with(
                'likes', 
                'dislikes', 
                'comments', 
                'comments.likes', 
                'comments.dislikes'
                )->find($id);

            if($question != null)
            {
                if($current_user->id == $question->user_id)
                {
                    // Ok
                    $question->content = $request->content;
                    $question->save();

                    $question = QuestionSocialLearning::with(
                        'likes', 
                        'dislikes', 
                        'comments', 
                        'comments.likes', 
                        'comments.dislikes'
                        )->find($id);

                    $user = User::find($question->user_id);
    
                    if($user != null) {
                        $question->user = [
                            'fullname' => $user->fullname,
                            'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                    
                    if($question->photo != null)
                    {
                        $question->photo = env('APP_URL') . '/images/photo/' . $question->photo;
                    }
                    
                    // Likes
                    foreach($question->likes as $like)
                    {
                        $user = User::find($like->user_id);
                        
                        if($user != null) {
                            $like->user = [
                                'fullname' => $user->fullname,
                                'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                            ];

                        }
                    
                    }

                    // Dislikes
                    foreach($question->dislikes as $dislike)
                    {
                        $user = User::find($dislike->user_id);
                        
                        if($user != null) {
                            $dislike->user = [
                                'fullname' => $user->fullname,
                                'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                            ];

                        }
                    
                    }

                    // Comments
                    foreach($question->comments as $comment)
                    {
                        $user = User::find($comment->user_id);
                        
                        if($user != null) {
                            $comment->user = [
                            'fullname' => $user->fullname,
                            'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                            ];

                        }
                    
                    }

                    return response()->json($question, 200);
                } else {
                    // Forbidden
                    return response()->json('This data not belongs to you!', 403);    
                }

            } else {
                // Data not found
                return response()->json('The data you are referring to is not found!', 400);
            }
            
        } else {
            // Unauthorize
            return response()->json('Unauthorize', 401);
        }
        
        return response()->json('Oops something when wrong!', 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $current_user = Auth::guard('api')->user();

        if($current_user != null)
        {
            $question = QuestionSocialLearning::with(
                'likes', 
                'dislikes', 
                'comments', 
                'comments.likes', 
                'comments.dislikes'
                )->find($id);

            if($question != null)
            {
                if($current_user->id == $question->user_id)
                {
                    // Ok
                    $question->delete();

                    return response()->json($question, 200);
                } else {
                    // Forbidden
                    return response()->json('This data not belongs to you!', 403);    
                }

            } else {
                // Data not found
                return response()->json('The data you are referring to is not found!', 400);
            }
            
        } else {
            // Unauthorize
            return response()->json('Unauthorize', 401);
        }
        
        return response()->json('Oops something when wrong!', 500);
    }

    public function like(Request $request, $id)
    {
        $current_user = Auth::guard('api')->user();

        if($current_user != null)
        {
            $question = QuestionSocialLearning::find($id);

            if($question != null)
            {
                // Ok
                $like = QuestionLike::where([
                    'question_id' => $question->id,
                    'user_id' => $current_user->id
                ])->first();
        
                $dislike = QuestionDislike::where([
                    'question_id' => $question->id,
                    'user_id' => $current_user->id
                ])->first();
        
                if ($like != null) {

                    $like->delete();
        
                } else {
                    $question->likes()->save(new QuestionLike([
                        'question_id' => $question->id,
                        'user_id' => $current_user->id
                    ]));
        
                }
        
                if ($dislike != null) {
                    $dislike->delete();
                }
        
                $question = QuestionSocialLearning::with(
                    'likes', 
                    'dislikes', 
                    'comments', 
                    'comments.likes', 
                    'comments.dislikes'
                    )->find($id);

                $user = User::find($question->user_id);

                if($user != null) {
                    $question->user = [
                        'fullname' => $user->fullname,
                        'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                    ];

                }
                
                if($question->photo != null)
                {
                    $question->photo = env('APP_URL') . '/images/photo/' . $question->photo;
                }
                
                // Likes
                foreach($question->likes as $like)
                {
                    $user = User::find($like->user_id);
                    
                    if($user != null) {
                        $like->user = [
                            'fullname' => $user->fullname,
                            'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                
                }

                // Dislikes
                foreach($question->dislikes as $dislike)
                {
                    $user = User::find($dislike->user_id);
                    
                    if($user != null) {
                        $dislike->user = [
                            'fullname' => $user->fullname,
                            'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                
                }

                // Comments
                foreach($question->comments as $comment)
                {
                    $user = User::find($comment->user_id);
                    
                    if($user != null) {
                        $comment->user = [
                        'fullname' => $user->fullname,
                        'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                
                }
        
                return response()->json($question, 200);

            } else {
                // Data not found
                return response()->json('The data you are referring to is not found!', 400);
            }
            
        } else {
            // Unauthorize
            return response()->json('Unauthorize', 401);
        }
        
        return response()->json('Oops something when wrong!', 500);

        // -------------------------------------------------------
    }

    public function dislike(Request $request, $id)
    {
        $current_user = Auth::guard('api')->user();

        if($current_user != null)
        {
            $question = QuestionSocialLearning::find($id);

            if($question != null)
            {
                // Ok
                $like = QuestionLike::where([
                    'question_id' => $question->id,
                    'user_id' => $current_user->id
                ])->first();
        
                $dislike = QuestionDislike::where([
                    'question_id' => $question->id,
                    'user_id' => $current_user->id
                ])->first();
        
                if ($dislike != null) {

                    $dislike->delete();
        
                } else {
                    $question->dislikes()->save(new QuestionDislike([
                        'question_id' => $question->id,
                        'user_id' => $current_user->id
                    ]));
        
                }
        
                if ($like != null) {
                    $like->delete();
                }
        
                $question = QuestionSocialLearning::with(
                    'likes', 
                    'dislikes', 
                    'comments', 
                    'comments.likes', 
                    'comments.dislikes'
                    )->find($id);

                $user = User::find($question->user_id);

                if($user != null) {
                    $question->user = [
                        'fullname' => $user->fullname,
                        'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                    ];

                }
                
                if($question->photo != null)
                {
                    $question->photo = env('APP_URL') . '/images/photo/' . $question->photo;
                }
                
                // Likes
                foreach($question->likes as $like)
                {
                    $user = User::find($like->user_id);
                    
                    if($user != null) {
                        $like->user = [
                            'fullname' => $user->fullname,
                            'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                
                }

                // Dislikes
                foreach($question->dislikes as $dislike)
                {
                    $user = User::find($dislike->user_id);
                    
                    if($user != null) {
                        $dislike->user = [
                            'fullname' => $user->fullname,
                            'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                
                }

                // Comments
                foreach($question->comments as $comment)
                {
                    $user = User::find($comment->user_id);
                    
                    if($user != null) {
                        $comment->user = [
                        'fullname' => $user->fullname,
                        'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                
                }
        
                return response()->json($question, 200);

            } else {
                // Data not found
                return response()->json('The data you are referring to is not found!', 400);
            }
            
        } else {
            // Unauthorize
            return response()->json('Unauthorize', 401);
        }
        
        return response()->json('Oops something when wrong!', 500);

        // -------------------------------------------------------
    }

    public function comment(Request $request, $id)
    {
        $current_user = Auth::guard('api')->user();

        if($current_user != null)
        {
            $question = QuestionSocialLearning::with(
                'likes', 
                'dislikes', 
                'comments', 
                'comments.likes', 
                'comments.dislikes'
                )->find($id);

            if($question != null)
            {
                // Ok
                $question->comments()->save(new Comment([
                    'post_id' => 0,
                    'question_id' => $question->id,
                    'user_id' => $current_user->id,
                    'body' => $request->content,
                ]));
        
                $question = QuestionSocialLearning::with(
                    'likes', 
                    'dislikes', 
                    'comments', 
                    'comments.likes', 
                    'comments.dislikes'
                    )->find($id);

                $user = User::find($question->user_id);

                if($user != null) {
                    $question->user = [
                        'fullname' => $user->fullname,
                        'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                    ];

                }
                
                if($question->photo != null)
                {
                    $question->photo = env('APP_URL') . '/images/photo/' . $question->photo;
                }
                
                // Likes
                foreach($question->likes as $like)
                {
                    $user = User::find($like->user_id);
                    
                    if($user != null) {
                        $like->user = [
                            'fullname' => $user->fullname,
                            'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                
                }

                // Dislikes
                foreach($question->dislikes as $dislike)
                {
                    $user = User::find($dislike->user_id);
                    
                    if($user != null) {
                        $dislike->user = [
                            'fullname' => $user->fullname,
                            'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                
                }

                // Comments
                foreach($question->comments as $comment)
                {
                    $user = User::find($comment->user_id);
                    
                    if($user != null) {
                        $comment->user = [
                        'fullname' => $user->fullname,
                        'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                
                }
                
                return response()->json($question, 200);

            } else {
                // Data not found
                return response()->json('The data you are referring to is not found!', 400);
            }
            
        } else {
            // Unauthorize
            return response()->json('Unauthorize', 401);
        }
        
        return response()->json('Oops something when wrong!', 500);
    }

    public function likeComment($question_id, $id)
    {
        $current_user = Auth::guard('api')->user();

        if($current_user != null)
        {
            $comment = Comment::find($id);
            $question = QuestionSocialLearning::with(
                'likes', 
                'dislikes', 
                'comments', 
                'comments.likes', 
                'comments.dislikes'
                )->find($question_id);

            if($question != null || $comment != null)
            {
                if($current_user->id == $question->user_id)
                {
                    // Ok
                    $like = CommentLike::where([
                        'comment_id' => $comment->id,
                        'user_id' => $current_user->id
                    ])->first();

                    $dislike = CommentDislike::where([
                        'comment_id' => $comment->id,
                        'user_id' => $current_user->id
                    ])->first();

                    if ($like != null) {
                        $like->delete();

                    } else {
                        $comment->likes()->save(new CommentLike([
                            'comment_id' => $comment->id,
                            'user_id' => $current_user->id
                        ]));

                    }

                    if ($dislike != null) {
                        $dislike->delete();
                    }
            
                    $question = QuestionSocialLearning::with(
                        'likes', 
                        'dislikes', 
                        'comments', 
                        'comments.likes', 
                        'comments.dislikes'
                        )->find($question_id);
            
                    return response()->json($question, 200);

                } else {
                    // Forbidden
                    return response()->json('This data not belongs to you!', 403);    
                }

            } else {
                // Data not found
                return response()->json('The data you are referring to is not found!', 400);
            }
            
        } else {
            // Unauthorize
            return response()->json('Unauthorize', 401);
        }
        
        return response()->json('Oops something when wrong!', 500);

        // -----------------------------------------------------------

        

    }

    public function dislikeComment($question_id, $id)
    {
        $current_user = Auth::guard('api')->user();

        if($current_user != null)
        {
            $comment = Comment::find($id);
            $question = QuestionSocialLearning::with(
                'likes', 
                'dislikes', 
                'comments', 
                'comments.likes', 
                'comments.dislikes'
                )->find($question_id);

            if($question != null || $comment != null)
            {
                if($current_user->id == $question->user_id)
                {
                    // Ok
                    $like = CommentLike::where([
                        'comment_id' => $comment->id,
                        'user_id' => $current_user->id
                    ])->first();
            
                    $dislike = CommentDislike::where([
                        'comment_id' => $comment->id,
                        'user_id' => $current_user->id
                    ])->first();
            
                    if ($dislike != null) {
                        $dislike->delete();
            
                    } else {
                        $comment->dislikes()->save(new CommentDislike([
                            'comment_id' => $comment->id,
                            'user_id' => $current_user->id
                        ]));
            
                    }
            
                    if ($like != null) {
                        $like->delete();
                    }
            
                    $question = QuestionSocialLearning::with(
                        'likes', 
                        'dislikes', 
                        'comments', 
                        'comments.likes', 
                        'comments.dislikes'
                        )->find($question_id);
            
                    return response()->json($question, 200);

                } else {
                    // Forbidden
                    return response()->json('This data not belongs to you!', 403);    
                }

            } else {
                // Data not found
                return response()->json('The data you are referring to is not found!', 400);
            }
            
        } else {
            // Unauthorize
            return response()->json('Unauthorize', 401);
        }
        
        return response()->json('Oops something when wrong!', 500);
    }
}
