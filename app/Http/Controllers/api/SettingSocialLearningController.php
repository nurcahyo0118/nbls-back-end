<?php

namespace App\Http\Controllers\api;
use App\Http\Controllers\Controller;

use Auth;
use Session;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

/**
 * @resource Setting Social Learning
 *
 * Longer description
 */

class SettingSocialLearningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('social-learnings.setting.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $user = Auth::user();
      $user->fullname = $request->fullname;
//        $user->email = $request->email;
      $user->username = $request->username;
      // $user->password = $request->newPassword;
      $user->ktp_id = $request->ktp_id;
      $user->birth_date = $request->birth_date;
      $user->sex = $request->sex;
      $user->phone = $request->phone;
      $user->mobile = $request->mobile;
      $user->notes = $request->notes;
      $user->pin_bb = $request->pin_bb;
      $user->fb = $request->fb;
      $user->tw = $request->tw;
      $user->ig = $request->ig;
      $user->about = $request->about;
      $user->hobby = $request->hobby;
      $user->skill = $request->skill;
      $user->hope = $request->hope;
      $user->website = $request->website;
      $user->address = $request->address;
      $user->zipcode = $request->zipcode;
      $user->hobby = $request->hobby;
      $user->reason = $request->reason;
      $user->is_patnership = $request->is_patnership;
      $user->save();


      return response()->json($user, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function changePassword($id, Request $request)
    {
        $current_user = User::find($id);

        if($current_user != null) {
            
            if($request->old_password != null)
            {
                if(Hash::check($request->old_password, $current_user->password))
                {
                    if($request->password != null)
                    {
                        if($request->password === $request->password_confirmation) {
                            $current_user->password = Hash::make($request->password);
                            $current_user->save();

                            return response()->json('Success update password !', 200);
                        } else {
                            return response()->json('Password confirmation does not match !', 400);
                        }
                        
                    } else {
                        return response()->json('Password cannot null !', 400);
                    }
                    
                } else {
                    return response()->json('Old password wrong !', 400);
                }
                
            } else {
                
                return response()->json('Old password cannot null !', 400);
            }

        } else {
            // Unauthorize
            return response()->json('Unauthorize', 401);
        }

        return response()->json('Oops something when wrong!', 500);
        
    }
}
