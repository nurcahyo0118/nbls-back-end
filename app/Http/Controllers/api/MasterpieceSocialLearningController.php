<?php

namespace App\Http\Controllers\api;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\MasterpieceSocialLearning;
use App\PostLikeMasterpiece;
use App\PostDislikeMasterpiece;

use App\CommentMasterpieceLike;
use App\CommentMasterpieceDislike;
use App\CommentMasterpiece;

use App\User;

use Auth;

/**
 * @resource Master Piece Social Learning
 *
 * Longer description
 */

class MasterpieceSocialLearningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $current_user = Auth::guard('api')->user();

        if($current_user != null)
        {
            // Response Oke
            $master_pieces = MasterpieceSocialLearning::with(
                'likesMasterpieces', 
                'dislikesMasterpieces', 
                'postCommentsMasterpieces', 
                'postCommentsMasterpieces.likesMasterpieces', 
                'postCommentsMasterpieces.dislikesMasterpieces'
                )->where('user_id', $current_user->id)
                    ->orWhereIn('user_id', $current_user->followings)
                    ->latest()
                    ->paginate(5);

            foreach($master_pieces as $master_piece)
            {
                // add user model
                $user = User::find($master_piece->user_id);
                
                if($user != null) {
                    $master_piece->user = [
                        'fullname' => $user->fullname,
                        'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                    ];
        
                }
        
                // add masterpiece file
                if($master_piece->file != null)
                {
                    $master_piece->file = env('APP_URL') . '/images/file/' . $master_piece->file;
                }
                
                if($master_piece->photo != null)
                {
                    $master_piece->photo = env('APP_URL') . '/images/photo/' . $master_piece->photo;
                }
                
                // Likes
                foreach($master_piece->likesMasterpieces as $like)
                {
                    $user = User::find($like->user_id);
                    
                    if($user != null) {
                        $like->user = [
                            'fullname' => $user->fullname,
                            'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                
                }

                // Dislikes
                foreach($master_piece->dislikesMasterpieces as $dislike)
                {
                    $user = User::find($dislike->user_id);
                    
                    if($user != null) {
                        $dislike->user = [
                            'fullname' => $user->fullname,
                            'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                
                }

                // Comments
                foreach($master_piece->postCommentsMasterpieces as $comment)
                {
                    $user = User::find($comment->user_id);
                    
                    if($user != null) {
                        $comment->user = [
                        'fullname' => $user->fullname,
                        'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                
                }
            }

            return response()->json($master_pieces, 200);

        } else {
            // Unauthorize
            return response()->json('Unauthorize', 401);
        }

        return response()->json('Oops something when wrong!', 500);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $current_user = Auth::guard('api')->user();

        if($current_user != null)
        {
            $master_piece = new MasterpieceSocialLearning;

            $master_piece->content = $request->content;
            $master_piece->user_id = $current_user->id;
            $master_piece->tenant_id = $current_user->tenant_id;
            $master_piece->save();

            $master_piece = MasterpieceSocialLearning::with(
                'likesMasterpieces', 
                'dislikesMasterpieces', 
                'postCommentsMasterpieces', 
                'postCommentsMasterpieces.likesMasterpieces', 
                'postCommentsMasterpieces.dislikesMasterpieces'
                )->find($master_piece->id);

            // add user model
            $user = User::find($master_piece->user_id);
                
            if($user != null) {
                $master_piece->user = [
                    'fullname' => $user->fullname,
                    'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                ];
    
            }
    
            // add masterpiece file
            if($master_piece->file != null)
            {
                $master_piece->file = env('APP_URL') . '/images/file/' . $master_piece->file;
            }
            
            if($master_piece->photo != null)
            {
                $master_piece->photo = env('APP_URL') . '/images/photo/' . $master_piece->photo;
            }
            
            // Likes
            foreach($master_piece->likesMasterpieces as $like)
            {
                $user = User::find($like->user_id);
                
                if($user != null) {
                    $like->user = [
                        'fullname' => $user->fullname,
                        'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                    ];

                }
            
            }

            // Dislikes
            foreach($master_piece->dislikesMasterpieces as $dislike)
            {
                $user = User::find($dislike->user_id);
                
                if($user != null) {
                    $dislike->user = [
                        'fullname' => $user->fullname,
                        'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                    ];

                }
            
            }

            // Comments
            foreach($master_piece->postCommentsMasterpieces as $comment)
            {
                $user = User::find($comment->user_id);
                
                if($user != null) {
                    $comment->user = [
                    'fullname' => $user->fullname,
                    'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                    ];

                }
            
            }

            return response()->json($master_piece, 200);
        } else {
            // Unauthorize
            return response()->json('Unauthorize', 401);
        }
        
        return response()->json('Oops something when wrong!', 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $current_user = Auth::guard('api')->user();

        if($current_user != null)
        {
            $master_piece = MasterpieceSocialLearning::with(
                'likesMasterpieces', 
                'dislikesMasterpieces', 
                'postCommentsMasterpieces', 
                'postCommentsMasterpieces.likesMasterpieces', 
                'postCommentsMasterpieces.dislikesMasterpieces'
                )->find($id);

            if($master_piece != null)
            {
                // Ok
                // add user model
                $user = User::find($master_piece->user_id);
                
                if($user != null) {
                    $master_piece->user = [
                        'fullname' => $user->fullname,
                        'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                    ];
        
                }
        
                // add masterpiece file
                if($master_piece->file != null)
                {
                    $master_piece->file = env('APP_URL') . '/images/file/' . $master_piece->file;
                }
                
                if($master_piece->photo != null)
                {
                    $master_piece->photo = env('APP_URL') . '/images/photo/' . $master_piece->photo;
                }
                
                // Likes
                foreach($master_piece->likesMasterpieces as $like)
                {
                    $user = User::find($like->user_id);
                    
                    if($user != null) {
                        $like->user = [
                            'fullname' => $user->fullname,
                            'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                
                }

                // Dislikes
                foreach($master_piece->dislikesMasterpieces as $dislike)
                {
                    $user = User::find($dislike->user_id);
                    
                    if($user != null) {
                        $dislike->user = [
                            'fullname' => $user->fullname,
                            'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                
                }

                // Comments
                foreach($master_piece->postCommentsMasterpieces as $comment)
                {
                    $user = User::find($comment->user_id);
                    
                    if($user != null) {
                        $comment->user = [
                        'fullname' => $user->fullname,
                        'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                
                }

                return response()->json($master_piece, 200);
            } else {
                // Data not found
                return response()->json('The data you are referring to is not found!', 400);
            }
            
        } else {
            // Unauthorize
            return response()->json('Unauthorize', 401);
        }
        
        return response()->json('Oops something when wrong!', 500);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $current_user = Auth::guard('api')->user();

        if($current_user != null)
        {
            $master_piece = MasterpieceSocialLearning::with(
                'likesMasterpieces', 
                'dislikesMasterpieces', 
                'postCommentsMasterpieces', 
                'postCommentsMasterpieces.likesMasterpieces', 
                'postCommentsMasterpieces.dislikesMasterpieces'
                )->find($id);

            if($master_piece != null)
            {
                if($current_user->id == $master_piece->user_id)
                {
                    // Ok
                    $master_piece->content = $request->content;
                    $master_piece->save();

                    return response()->json($master_piece, 200);
                } else {
                    // Forbidden
                    return response()->json('This data not belongs to you!', 403);    
                }

            } else {
                // Data not found
                return response()->json('The data you are referring to is not found!', 400);
            }
            
        } else {
            // Unauthorize
            return response()->json('Unauthorize', 401);
        }
        
        return response()->json('Oops something when wrong!', 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $current_user = Auth::guard('api')->user();

        if($current_user != null)
        {
            $master_piece = MasterpieceSocialLearning::find($id);

            if($master_piece != null)
            {
                if($current_user->id == $master_piece->user_id)
                {
                    // Ok
                    $master_piece->delete();

                    return response()->json($master_piece, 200);
                } else {
                    // Forbidden
                    return response()->json('This data not belongs to you!', 403);    
                }

            } else {
                // Data not found
                return response()->json('The data you are referring to is not found!', 400);
            }
            
        } else {
            // Unauthorize
            return response()->json('Unauthorize', 401);
        }
        
        return response()->json('Oops something when wrong!', 500);
    }
    
    public function like(Request $request, $id)
    {
        $current_user = Auth::guard('api')->user();

        if($current_user != null)
        {
            $master_piece = MasterpieceSocialLearning::find($id);

            if($master_piece != null)
            {
                // Ok
                $like = PostLikeMasterpiece::where([
                    'post_id' => $master_piece->id,
                    'user_id' => $current_user->id
                ])->first();
        
                $dislike = PostDislikeMasterpiece::where([
                    'post_id' => $master_piece->id,
                    'user_id' => $current_user->id
                ])->first();
        
                if ($like != null) {

                    $like->delete();
        
                } else {
                    $master_piece->likesMasterpieces()->save(new PostLikeMasterpiece([
                        'post_id' => $master_piece->id,
                        'user_id' => $current_user->id
                    ]));
        
                }
        
                if ($dislike != null) {
                    $dislike->delete();
                }
        
                $master_piece = MasterpieceSocialLearning::with(
                    'likesMasterpieces', 
                    'dislikesMasterpieces', 
                    'postCommentsMasterpieces', 
                    'postCommentsMasterpieces.likesMasterpieces', 
                    'postCommentsMasterpieces.dislikesMasterpieces'
                    )->find($id);

                // add user model
                $user = User::find($master_piece->user_id);
                
                if($user != null) {
                    $master_piece->user = [
                        'fullname' => $user->fullname,
                        'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                    ];
        
                }
        
                // add masterpiece file
                if($master_piece->file != null)
                {
                    $master_piece->file = env('APP_URL') . '/images/file/' . $master_piece->file;
                }
                
                if($master_piece->photo != null)
                {
                    $master_piece->photo = env('APP_URL') . '/images/photo/' . $master_piece->photo;
                }
                
                // Likes
                foreach($master_piece->likesMasterpieces as $like)
                {
                    $user = User::find($like->user_id);
                    
                    if($user != null) {
                        $like->user = [
                            'fullname' => $user->fullname,
                            'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                
                }

                // Dislikes
                foreach($master_piece->dislikesMasterpieces as $dislike)
                {
                    $user = User::find($dislike->user_id);
                    
                    if($user != null) {
                        $dislike->user = [
                            'fullname' => $user->fullname,
                            'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                
                }

                // Comments
                foreach($master_piece->postCommentsMasterpieces as $comment)
                {
                    $user = User::find($comment->user_id);
                    
                    if($user != null) {
                        $comment->user = [
                        'fullname' => $user->fullname,
                        'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                
                }
        
                return response()->json($master_piece, 200);

            } else {
                // Data not found
                return response()->json('The data you are referring to is not found!', 400);
            }
            
        } else {
            // Unauthorize
            return response()->json('Unauthorize', 401);
        }
        
        return response()->json('Oops something when wrong!', 500);
    }

    public function dislike(Request $request, $id)
    {
        $current_user = Auth::guard('api')->user();

        if($current_user != null)
        {
            $master_piece = MasterpieceSocialLearning::find($id);

            if($master_piece != null)
            {
                // Ok
                $like = PostLikeMasterpiece::where([
                    'post_id' => $master_piece->id,
                    'user_id' => $current_user->id
                ])->first();
        
                $dislike = PostDislikeMasterpiece::where([
                    'post_id' => $master_piece->id,
                    'user_id' => $current_user->id
                ])->first();
        
                if ($dislike != null) {

                    $dislike->delete();
        
                } else {

                    $master_piece->dislikesMasterpieces()->save(new PostDislikeMasterpiece([
                        'post_id' => $master_piece->id,
                        'user_id' => $current_user->id
                    ]));
        
                }
        
                if ($like != null) {
                    $like->delete();
                }
        
                $master_piece = MasterpieceSocialLearning::with(
                    'likesMasterpieces', 
                    'dislikesMasterpieces', 
                    'postCommentsMasterpieces', 
                    'postCommentsMasterpieces.likesMasterpieces', 
                    'postCommentsMasterpieces.dislikesMasterpieces'
                    )->find($id);

                // add user model
                $user = User::find($master_piece->user_id);
                
                if($user != null) {
                    $master_piece->user = [
                        'fullname' => $user->fullname,
                        'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                    ];
        
                }
        
                // add masterpiece file
                if($master_piece->file != null)
                {
                    $master_piece->file = env('APP_URL') . '/images/file/' . $master_piece->file;
                }
                
                if($master_piece->photo != null)
                {
                    $master_piece->photo = env('APP_URL') . '/images/photo/' . $master_piece->photo;
                }
                
                // Likes
                foreach($master_piece->likesMasterpieces as $like)
                {
                    $user = User::find($like->user_id);
                    
                    if($user != null) {
                        $like->user = [
                            'fullname' => $user->fullname,
                            'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                
                }

                // Dislikes
                foreach($master_piece->dislikesMasterpieces as $dislike)
                {
                    $user = User::find($dislike->user_id);
                    
                    if($user != null) {
                        $dislike->user = [
                            'fullname' => $user->fullname,
                            'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                
                }

                // Comments
                foreach($master_piece->postCommentsMasterpieces as $comment)
                {
                    $user = User::find($comment->user_id);
                    
                    if($user != null) {
                        $comment->user = [
                        'fullname' => $user->fullname,
                        'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                
                }
        
                return response()->json($master_piece, 200);

            } else {
                // Data not found
                return response()->json('The data you are referring to is not found!', 400);
            }
            
        } else {
            // Unauthorize
            return response()->json('Unauthorize', 401);
        }
        
        return response()->json('Oops something when wrong!', 500);
    }

    public function postComment(Request $request, $id)
    {
        $current_user = Auth::guard('api')->user();

        if($current_user != null)
        {
            $master_piece = MasterpieceSocialLearning::with(
                'likesMasterpieces', 
                'dislikesMasterpieces', 
                'postCommentsMasterpieces', 
                'postCommentsMasterpieces.likesMasterpieces', 
                'postCommentsMasterpieces.dislikesMasterpieces'
                )->find($id);

            if($master_piece != null)
            {
                // Ok
                $master_piece->postCommentsMasterpieces()->save(new CommentMasterpiece([
                    'question_id' => 0,
                    'post_id' => $master_piece->id,
                    'user_id' => $current_user->id,
                    'body' => $request->content,
                ]));
        
                $master_piece = MasterpieceSocialLearning::with(
                    'likesMasterpieces', 
                    'dislikesMasterpieces', 
                    'postCommentsMasterpieces', 
                    'postCommentsMasterpieces.likesMasterpieces', 
                    'postCommentsMasterpieces.dislikesMasterpieces'
                    )->find($id);

                // add user model
                $user = User::find($master_piece->user_id);
                
                if($user != null) {
                    $master_piece->user = [
                        'fullname' => $user->fullname,
                        'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                    ];
        
                }
        
                // add masterpiece file
                if($master_piece->file != null)
                {
                    $master_piece->file = env('APP_URL') . '/images/file/' . $master_piece->file;
                }
                
                if($master_piece->photo != null)
                {
                    $master_piece->photo = env('APP_URL') . '/images/photo/' . $master_piece->photo;
                }
                
                // Likes
                foreach($master_piece->likesMasterpieces as $like)
                {
                    $user = User::find($like->user_id);
                    
                    if($user != null) {
                        $like->user = [
                            'fullname' => $user->fullname,
                            'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                
                }

                // Dislikes
                foreach($master_piece->dislikesMasterpieces as $dislike)
                {
                    $user = User::find($dislike->user_id);
                    
                    if($user != null) {
                        $dislike->user = [
                            'fullname' => $user->fullname,
                            'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                
                }

                // Comments
                foreach($master_piece->postCommentsMasterpieces as $comment)
                {
                    $user = User::find($comment->user_id);
                    
                    if($user != null) {
                        $comment->user = [
                        'fullname' => $user->fullname,
                        'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                
                }
                
                return response()->json($master_piece, 200);

            } else {
                // Data not found
                return response()->json('The data you are referring to is not found!', 400);
            }
            
        } else {
            // Unauthorize
            return response()->json('Unauthorize', 401);
        }
        
        return response()->json('Oops something when wrong!', 500);
    }

    public function likeComment($master_piece_id, $id)
    {
        $current_user = Auth::guard('api')->user();

        if($current_user != null)
        {
            $comment = CommentMasterpiece::find($id);
            $master_piece = MasterpieceSocialLearning::with(
                'likesMasterpieces', 
                'dislikesMasterpieces', 
                'postCommentsMasterpieces', 
                'postCommentsMasterpieces.likesMasterpieces', 
                'postCommentsMasterpieces.dislikesMasterpieces'
                )->find($master_piece_id);

            if($master_piece != null || $comment != null)
            {
                // Ok
                $like = CommentMasterpieceLike::where([
                    'comment_id' => $comment->id,
                    'user_id' => $current_user->id
                ])->first();
        
                $dislike = CommentMasterpieceDislike::where([
                    'comment_id' => $comment->id,
                    'user_id' => $current_user->id
                ])->first();
        
                if ($like != null) {
                    $like->delete();
        
                } else {
                    $comment->likesMasterpieces()->save(new CommentMasterpieceLike([
                        'comment_id' => $comment->id,
                        'user_id' => $current_user->id
                    ]));
        
                }
        
                if ($dislike != null) {
                    $dislike->delete();
                }
        
                $master_piece = MasterpieceSocialLearning::with(
                    'likesMasterpieces', 
                    'dislikesMasterpieces', 
                    'postCommentsMasterpieces', 
                    'postCommentsMasterpieces.likesMasterpieces', 
                    'postCommentsMasterpieces.dislikesMasterpieces'
                    )->find($master_piece_id);

                // add user model
                $user = User::find($master_piece->user_id);
                
                if($user != null) {
                    $master_piece->user = [
                        'fullname' => $user->fullname,
                        'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                    ];
        
                }
        
                // add masterpiece file
                if($master_piece->file != null)
                {
                    $master_piece->file = env('APP_URL') . '/images/file/' . $master_piece->file;
                }
                
                if($master_piece->photo != null)
                {
                    $master_piece->photo = env('APP_URL') . '/images/photo/' . $master_piece->photo;
                }
                
                // Likes
                foreach($master_piece->likesMasterpieces as $like)
                {
                    $user = User::find($like->user_id);
                    
                    if($user != null) {
                        $like->user = [
                            'fullname' => $user->fullname,
                            'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                
                }

                // Dislikes
                foreach($master_piece->dislikesMasterpieces as $dislike)
                {
                    $user = User::find($dislike->user_id);
                    
                    if($user != null) {
                        $dislike->user = [
                            'fullname' => $user->fullname,
                            'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                
                }

                // Comments
                foreach($master_piece->postCommentsMasterpieces as $comment)
                {
                    $user = User::find($comment->user_id);
                    
                    if($user != null) {
                        $comment->user = [
                        'fullname' => $user->fullname,
                        'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                
                }
        
                return response()->json($master_piece, 200);

            } else {
                // Data not found
                return response()->json('The data you are referring to is not found!', 400);
            }
            
        } else {
            // Unauthorize
            return response()->json('Unauthorize', 401);
        }
        
        return response()->json('Oops something when wrong!', 500);

    }

    public function dislikeComment($master_piece_id, $id)
    {
        $current_user = Auth::guard('api')->user();

        if($current_user != null)
        {
            $comment = CommentMasterpiece::find($id);
            $master_piece = MasterpieceSocialLearning::with(
                'likesMasterpieces', 
                'dislikesMasterpieces', 
                'postCommentsMasterpieces', 
                'postCommentsMasterpieces.likesMasterpieces', 
                'postCommentsMasterpieces.dislikesMasterpieces'
                )->find($master_piece_id);

            if($master_piece != null || $comment != null)
            {
                // Ok
                $like = CommentMasterpieceLike::where([
                    'comment_id' => $comment->id,
                    'user_id' => $current_user->id
                ])->first();
        
                $dislike = CommentMasterpieceDislike::where([
                    'comment_id' => $comment->id,
                    'user_id' => $current_user->id
                ])->first();
        
                if ($dislike != null) {
                    $dislike->delete();
        
                } else {
                    $comment->dislikesMasterpieces()->save(new CommentMasterpieceDislike([
                        'comment_id' => $comment->id,
                        'user_id' => $current_user->id
                    ]));
        
                }
        
                if ($like != null) {
                    $like->delete();
                }
        
                $master_piece = MasterpieceSocialLearning::with(
                    'likesMasterpieces', 
                    'dislikesMasterpieces', 
                    'postCommentsMasterpieces', 
                    'postCommentsMasterpieces.likesMasterpieces', 
                    'postCommentsMasterpieces.dislikesMasterpieces'
                    )->find($master_piece_id);

                // add user model
                $user = User::find($master_piece->user_id);
                
                if($user != null) {
                    $master_piece->user = [
                        'fullname' => $user->fullname,
                        'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                    ];
        
                }
        
                // add masterpiece file
                if($master_piece->file != null)
                {
                    $master_piece->file = env('APP_URL') . '/images/file/' . $master_piece->file;
                }
                
                if($master_piece->photo != null)
                {
                    $master_piece->photo = env('APP_URL') . '/images/photo/' . $master_piece->photo;
                }
                
                // Likes
                foreach($master_piece->likesMasterpieces as $like)
                {
                    $user = User::find($like->user_id);
                    
                    if($user != null) {
                        $like->user = [
                            'fullname' => $user->fullname,
                            'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                
                }

                // Dislikes
                foreach($master_piece->dislikesMasterpieces as $dislike)
                {
                    $user = User::find($dislike->user_id);
                    
                    if($user != null) {
                        $dislike->user = [
                            'fullname' => $user->fullname,
                            'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                
                }

                // Comments
                foreach($master_piece->postCommentsMasterpieces as $comment)
                {
                    $user = User::find($comment->user_id);
                    
                    if($user != null) {
                        $comment->user = [
                        'fullname' => $user->fullname,
                        'photo' => $user->photo != null ? env('APP_URL') . '/images/photo/' . $user->photo : null
                        ];

                    }
                
                }
        
                return response()->json($master_piece, 200);

            } else {
                // Data not found
                return response()->json('The data you are referring to is not found!', 400);
            }
            
        } else {
            // Unauthorize
            return response()->json('Unauthorize', 401);
        }
        
        return response()->json('Oops something when wrong!', 500);
    }
}
