<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\MasterpieceSocialLearning;
use App\CommentMasterpiece;
use App\CommentMasterpieceLike;
use App\CommentMasterpieceDislike;
use App\PostLikeMasterpiece;
use App\PostDislikeMasterpiece;
use App\User;


/**
 * @resource Master Piece
 *
 * This module requires full authentication
 *  Headers : 
 *    - Authorization : "Bearer ACCESS_TOKEN"
 * 
 */
class MasterPieceController extends Controller
{
    /**
     * Get All
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $masters = MasterpieceSocialLearning::with(
            'likesMasterpieces', 
            'dislikesMasterpieces', 
            'postCommentsMasterpieces',
            'postCommentsMasterpieces.likesMasterpieces',
            'postCommentsMasterpieces.dislikesMasterpieces'
        )->latest()->paginate(10);

        return response()->json($masters, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Insert.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $master = new MasterpieceSocialLearning;

        $master->content = $request->content;
        $master->user_id = Auth::id();
        $master->tenant_id = Auth::user()->tenant->id;
        $master->save();

        return response()->json($master, 200);
    }

    /**
     * Get One
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $master = MasterpieceSocialLearning::with('likesMasterpieces', 'dislikesMasterpieces', 'postCommentsMasterpieces')->find($id);

        return response()->json($master, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $master = MasterpieceSocialLearning::find($id);

        $master->content = $request->content;
        $master->user_id = Auth::id();
        $master->tenant_id = Auth::user()->tenant->id;
        $master->save();
    }

    /**
     * Delete
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $master = MasterpieceSocialLearning::find($id);

        $master->likesMasterpieces()->delete();
        $master->dislikesMasterpieces()->delete();
        $master->postCommentsMasterpieces()->delete();
        $master->delete();

        return response()->json(['message' => 'Success'], 200);
    }

    /**
     * Like
     *
     * @return \Illuminate\Http\Response
     */
    public function like(Request $request, $id)
    {
        $master = MasterpieceSocialLearning::with('likesMasterpieces', 'dislikesMasterpieces', 'postCommentsMasterpieces')->find($id);

        $like = PostLikeMasterpiece::where([
            'post_id' => $master->id,
            'user_id' => Auth::id()
        ])->first();

        $dislike = PostDislikeMasterpiece::where([
            'post_id' => $master->id,
            'user_id' => Auth::id()
        ])->first();

        if ($like != null) {
            $like->delete();

        } else {
            $master->likesMasterpieces()->save(new PostLikeMasterpiece([
                'post_id' => $master->id,
                'user_id' => Auth::id()
            ]));

        }

        if ($dislike != null) {
            $dislike->delete();
        }

        $master = MasterpieceSocialLearning::with('likesMasterpieces', 'dislikesMasterpieces', 'postCommentsMasterpieces')->find($id);

        return response()->json($master, 200);
    }

    /**
     * Dislike
     *
     * @return \Illuminate\Http\Response
     */
    public function dislike(Request $request, $id)
    {
        $master = MasterpieceSocialLearning::with('likesMasterpieces', 'dislikesMasterpieces', 'postCommentsMasterpieces')->find($id);

        $like = PostLikeMasterpiece::where([
            'post_id' => $master->id,
            'user_id' => Auth::id()
        ])->first();

        $dislike = PostDislikeMasterpiece::where([
            'post_id' => $master->id,
            'user_id' => Auth::id()
        ])->first();

        if ($dislike != null) {
            $dislike->delete();

        } else {
            $master->likesMasterpieces()->save(new PostDislikeMasterpiece([
                'post_id' => $master->id,
                'user_id' => Auth::id()
            ]));

        }

        if ($like != null) {
            $like->delete();
        }

        $master = MasterpieceSocialLearning::with('likesMasterpieces', 'dislikesMasterpieces', 'postCommentsMasterpieces')->find($id);

        return response()->json($master, 200);
    }

    /**
     * Comment.
     *
     * @return \Illuminate\Http\Response
     */
    public function postComment(Request $request, $id)
    {
        $master = MasterpieceSocialLearning::with('likesMasterpieces', 'dislikesMasterpieces', 'postCommentsMasterpieces')->find($id);

        $master->postCommentsMasterpieces()->save(new CommentMasterpiece([
            'question_id' => 0,
            'post_id' => $master->id,
            'user_id' => Auth::id(),
            'body' => $request->content,
        ]));

        $master = MasterpieceSocialLearning::with('likesMasterpieces', 'dislikesMasterpieces', 'postCommentsMasterpieces')->find($id);
        
        return response()->json($master, 200);
    }

    /**
     * Like Comment
     *
     * @return \Illuminate\Http\Response
     */
    public function likeComment($id)
    {
        $master = MasterpieceSocialLearning::with('likesMasterpieces', 'dislikesMasterpieces', 'postCommentsMasterpieces')->find($id);

        $like = CommentMasterpieceLike::where([
            'comment_id' => $comment->id,
            'user_id' => Auth::id()
        ])->first();

        $dislike = CommentMasterpieceDislike::where([
            'comment_id' => $comment->id,
            'user_id' => Auth::id()
        ])->first();

        if ($like != null) {
            $like->delete();

        } else {
            $comment->likesMasterpieces()->save(new CommentMasterpieceLike([
                'comment_id' => $comment->id,
                'user_id' => Auth::id()
            ]));

        }

        if ($dislike != null) {
            $dislike->delete();
        }

        $master = MasterpieceSocialLearning::with('likesMasterpieces', 'dislikesMasterpieces', 'postCommentsMasterpieces')->find($id);

        return response()->json($master, 200);

    }

    /**
     * Dislike Comment
     *
     * @return \Illuminate\Http\Response
     */
    public function dislikeComment($id)
    {
        $master = MasterpieceSocialLearning::with('likesMasterpieces', 'dislikesMasterpieces', 'postCommentsMasterpieces')->find($id);

        $like = CommentMasterpieceLike::where([
            'comment_id' => $comment->id,
            'user_id' => Auth::id()
        ])->first();

        $dislike = CommentMasterpieceDislike::where([
            'comment_id' => $comment->id,
            'user_id' => Auth::id()
        ])->first();

        if ($dislike != null) {
            $dislike->delete();

        } else {
            $comment->likesMasterpieces()->save(new CommentMasterpieceDislike([
                'comment_id' => $comment->id,
                'user_id' => Auth::id()
            ]));

        }

        if ($like != null) {
            $like->delete();
        }

        $master = MasterpieceSocialLearning::with('likesMasterpieces', 'dislikesMasterpieces', 'postCommentsMasterpieces')->find($id);

        return response()->json($master, 200);
    }
}
