<?php

namespace App\Http\Controllers\api;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Course;
use App\Material;

use Auth;
/**
 * @resource My course Social Learning
 *
 * Longer description
 */

class MyCourseSocialLearningController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view ('social-learnings.my-course.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($domain, $id)
    {
        $course = Course::find($id);

        // return 'meeeee';

        return response()->json($course, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function currentMaterial($domain, Request $request, $id, $material_id)
    {
        
        $course = Course::find($id);
        $material = Material::find($material_id);

        Auth::user()->points()->create([
            'point' => 7,
            'user_id' => Auth::id()
        ]);

        Auth::user()->logs()->create([
            'reason' => 'LOG_WATCH_VIDEO',
            'user_id' => Auth::id()
        ]);

        return response()->json($course, 200);
    }

    public function addToCart($domain, Request $request, $id)
    {
        if(Auth::user()->carts()->count() === 0) {
            Auth::user()->carts()->attach($id);
        }

        $course = Course::find($id);

        return response()->json($course, 200);
    }
}
