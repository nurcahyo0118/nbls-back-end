<?php

namespace App\Http\Controllers\api;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\EventSocialLearning;
use Session;
use Auth;

/**
 * @resource Event Social Learning
 *
 * Longer description
 */

class EventSocialLearningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $events = EventSocialLearning::orderBy("id")->paginate(15);
      return response()->json($events, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $event = new EventSocialLearning;
        $event->user_id = $request->user_id;
        $event->tenant_id = $request->tenant_id;
        $event->title = $request->title;
        $event->body = $request->body;
        $event->date = $request->date;
        $event->save();
        //        refresh the page
        return response()->json($event, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($domain, $id)
    {
      // $post = PostSocialLearning::find($id);
      $event = EventSocialLearning::find($id);
      $event->delete();

      // $post->likes()->detach();
      // $post->dislikes()->detach();
      // $post->postComments()->detach();
      // $post->delete();
    }

    public function followEvent(Request $request, $id)
    {
      $event = EventSocialLearning::find($id);
      // $event->title = $event->$request;
      // $event->save();
      $event->users()->attach(Auth::id());
      return response()->json($event, 200);
    }

    public function unFollowEvent(Request $request, $id)
    {
      $event = EventSocialLearning::find($id);
      // $event->title = $event->$request;
      // $event->save();
      $event->users()->detach(Auth::id());
      return response()->json($event, 200);
    }
}
