<?php

namespace App\Http\Controllers\api;
use App\Http\Controllers\Controller;


use Illuminate\Http\Request;
use App\Quiz;

use Log;


/**
 * @resource Quiz
 *
 * Longer description
 */

class QuizController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $quizzes = Quiz::all();

      return response()->json($quizzes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($domain, Request $request)
    {
        Log::debug($request);
        $quizzes = Quiz::where(['material_id' => $request->material_id])->get();

        $quiz = new Quiz;
        $quiz->question = $request->question;
        $quiz->option_a = $request->option_a;
        $quiz->option_b = $request->option_b;
        $quiz->option_c = $request->option_c;
        $quiz->option_d = $request->option_d;
        $quiz->option_true = $request->option_true;
        $quiz->line = $quizzes->count() + 1;
        $quiz->material_id = $request->material_id;

        $quiz->save();

        return response()->json([
         'material_id' => $quiz->material['id'],
         'view' => (string)view('courses.childquiz')->withQuiz($quiz)
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($domain, Request $request, $id)
    {
      Log::debug($request->question);
      $quiz = Quiz::find($id);
      Log::debug($quiz);

      $quiz->question = $request->question;
      $quiz->option_a = $request->option_a;
      $quiz->option_b = $request->option_b;
      $quiz->option_c = $request->option_c;
      $quiz->option_d = $request->option_d;
      $quiz->option_true = $request->option_true;

      $quiz->save();

      return response()->json($quiz);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($domain, $id)
    {
      $quiz = Quiz::find($id);
      $quiz->delete();
    }
}
