<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\QuestionSocialLearning;
use App\Comment;
use App\QuestionLike;
use App\QuestionDislike;
use App\CommentLike;
use App\CommentDislike;

use Auth;

/**
 * @resource Question
 *
 * This module requires full authentication
 *  Headers : 
 *    - Authorization : "Bearer ACCESS_TOKEN"
 * 
 */
class QuestionController extends Controller
{
    /**
     * Get All.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Insert.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $question = new QuestionSocialLearning;

        $question->content = $request->content;
        $question->user_id = Auth::id();
        $question->tenant_id = Auth::user()->tenant->id;
        $question->material_id = $request->material_id == null ? 0 : $request->material_id;
        $question->save();

        return response()->json($question, 200);
    }

    /**
     * Get One.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Delete.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        QuestionSocialLearning::find($id)->delete();
    }

    /**
     * Comment.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function postComment(Request $request, $id)
    {
        $question = QuestionSocialLearning::find($id);

        $question->comments()->save(new Comment([
            'question_id' => $question->id,
            'post_id' => 0,
            'user_id' => Auth::id(),
            'body' => $request->content,
        ]));

        return response()->json($question->with('likes', 'dislikes', 'comments', 'user'), 200);
    }

    /**
     * Like.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function like(Request $request, $id)
    {
        $question = QuestionSocialLearning::find($id);

        $like = QuestionLike::where([
            'question_id' => $question->id,
            'user_id' => Auth::id()
        ])->first();

        $dislike = QuestionDislike::where([
            'question_id' => $question->id,
            'user_id' => Auth::id()
        ])->first();

        if ($like != null) {
            $like->delete();

        } else {
            $question->likes()->save(new QuestionLike([
                'question_id' => $question->id,
                'user_id' => Auth::id()
            ]));
        }

        if ($dislike != null) {
            $dislike->delete();
        }

        return view('social-learnings.course.child.question-child')->withQuestion($question);
    }

    /**
     * Dislike.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function dislike(Request $request, $id)
    {
        $question = QuestionSocialLearning::find($id);

        $like = QuestionLike::where([
            'question_id' => $question->id,
            'user_id' => Auth::id()
        ])->first();

        $dislike = QuestionDislike::where([
            'question_id' => $question->id,
            'user_id' => Auth::id()
        ])->first();

        if ($dislike != null) {
            $dislike->delete();

        } else {
            $question->likes()->save(new QuestionDislike([
                'question_id' => $question->id,
                'user_id' => Auth::id()
            ]));

        }

        if ($like != null) {
            $like->delete();
        }

        return view('social-learnings.course.child.question-child')->withQuestion($question);
    }

    /**
     * Like Comment.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function likeComment($id)
    {
        $comment = Comment::find($id);

        $like = CommentLike::where([
            'comment_id' => $comment->id,
            'user_id' => Auth::id()
        ])->first();

        $dislike = CommentDislike::where([
            'comment_id' => $comment->id,
            'user_id' => Auth::id()
        ])->first();

        if ($like != null) {
            $like->delete();

        } else {
            $comment->likes()->save(new CommentLike([
                'comment_id' => $comment->id,
                'user_id' => Auth::id()
            ]));

        }

        if ($dislike != null) {
            $dislike->delete();
        }

        return view('social-learnings.course.child.question-child')->withQuestion($comment->question);

    }

    /**
     * Dislike Comment.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function dislikeComment($id)
    {
        $comment = Comment::find($id);

        // return $comment;

        $like = CommentLike::where([
            'comment_id' => $comment->id,
            'user_id' => Auth::id()
        ])->first();

        $dislike = CommentDislike::where([
            'comment_id' => $comment->id,
            'user_id' => Auth::id()
        ])->first();

        // return $like;

        if ($dislike != null) {
            $dislike->delete();

        } else {
            $comment->likes()->save(new CommentDislike([
                'comment_id' => $comment->id,
                'user_id' => Auth::id()
            ]));

            // return $comment->likes;

        }

        if ($like != null) {
            $like->delete();
        }

        return view('social-learnings.course.child.question-child')->withQuestion($comment->question);
    }

}
