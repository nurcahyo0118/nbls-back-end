<?php

namespace App\Http\Controllers\api;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Tenant;
use App\ExchangePointSocialLearning;

use Auth;
use Illuminate\Support\Facades\Hash;
use Log;
use DB;
/**
 * @resource User Social Learning
 *
 * Longer description
 */

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function checkAuth($domain, Request $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
        ];

        if (!Auth::attempt($credentials)) {
            return response('Username or password is wrong!', 403);
        }

        return response(Auth::user(), 201);
    }

    public function index($domain)
    {
      $tenant = Auth::user()->tenant;
      $users = User::where('tenant_id', $tenant->id)->get();
      $roles = Role::all();

      return view('users.index')
          ->withUsers($users)
          ->withRoles($roles);
    }

    public function usersByRole($domain, $id)
    {
        $tenant = Auth::user()->tenant;

        $users = User::where([
          'role_id' => $id,
          'tenant_id' => $tenant->id
          ])->get();

        $roles = Role::all();

        return response()->json($users, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($domain)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($domain, Request $request)
    {

        $tenant = Tenant::where(['domain' => $domain])->first();

        $this->validate($request, [
            'fullname' => 'required|max:255',
            'ktp_id' => 'required|unique:users',
            'username' => 'required|unique:users',
            'password' => 'required',
            'email' => 'required|unique:users',
            'phone' => 'required|unique:users',
        ]);

        $user = new User;

        $user->fullname = $request->fullname;
        $user->ktp_id = $request->ktp_id;
        $user->username = $request->username;
        $user->password = Hash::make($request->password);
        $user->email = $request->email;
        $user->birth_date = $request->birth_date;
        $user->phone = $request->phone;
        $user->mobile = $request->mobile;
        $user->mobile2 = $request->mobile2;
        $user->pin_bb = $request->pin_bb;
        $user->notes = $request->notes;

        $user->photo = $request->photo;

        $user->address = $request->address;
        $user->zipcode = $request->zipcode;
        $user->fb = $request->fb;
        $user->tw = $request->tw;
        $user->website = $request->website;
        $user->status = $request->status;
        $user->hobby = $request->hobby;
        $user->reason = $request->reason;

//        Default User Role
        $user->role_id = 5;

//        Default Tenant and Branch
        $user->branch_id = 0;
        $user->tenant_id = $tenant->id;

        $user->save();

        return response()->json($user, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($domain, $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($domain, $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($domain, Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($domain, $id)
    {
        //
    }

    public function changeRole($domain, Request $request, $id)
    {
        $user = User::find($id);
        $user->role_id = $request->role_id;

        $user->save();

    return response()->json($user, 200);    }

    public function addRole($domain, Request $request)
    {
        $user = User::find($request->user_id);
        $user->roles()->attach($request->role_id);

        return response()->json($user->roles);
    }

    public function deleteRole($domain, Request $request)
    {
        $user = User::find($request->user_id);
        $user->roles()->detach($request->role_id);

        return response()->json($user->roles);
    }

    public function selectExpert($domain, Request $request)
    {
        $user = User::find($request->user_id);
        $user->tenant_id = $request->tenant_id;

        $user->save();

        return redirect()->back();
    }

    public function follow(Request $request, $id)
    {
        $current_user = Auth::guard('api')->user();

        $user_follow = DB::table('user_follows')->where([
            'following_id' => $id,
            'follower_id' => $current_user->id
        ])->first();

        if($current_user != null)
        {
            if($user_follow == null)
            {
                $current_user->followings()->attach($id);
            } else {
                $current_user->followings()->detach($id);
            }

            $current_user->points()->create([
                'point' => 2,
                'user_id' => $current_user->id
            ]);

            $user = User::with('followings', 'followers')->find($current_user->id);
    
            return response()->json($user, 200);

        } else {
            // Unauthorize
            return response()->json('Unauthorize', 401);
        }
        
        return response()->json('Oops something when wrong!', 500);

        // -----------------------------------------------------------
    }

    public function unfollow(Request $request, $id)
    {
        $current_user = Auth::guard('api')->user();

        $user_follow = DB::table('user_follows')->where([
            'following_id' => $id,
            'follower_id' => $current_user->id
        ])->first();

        if($current_user != null)
        {
            if($user_follow != null)
            {
                $current_user->followings()->detach($id);
            } else {
                $current_user->followings()->attach($id);
            }

            $current_user->points()->create([
                'point' => -2,
                'user_id' => $current_user
            ]);

            $current_user->followings;

            $user = User::with('followings', 'followers')->find($current_user->id);
    
            return response()->json($user, 200);

        } else {
            // Unauthorize
            return response()->json('Unauthorize', 401);
        }
        
        return response()->json('Oops something when wrong!', 500);

        // -----------------------------------------------------------
    }

    public function getUsersByName($name)
    {
        $users = User::where('fullname', 'like', '%' . $name . '%')->get();

        return response()->json($users, 200);
    }

}
