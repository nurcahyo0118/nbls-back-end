<?php

namespace App\Http\Controllers\api;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterSocialLearningRequest;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use GuzzleHttp\Client;

use App\User;
use Auth;
use DB;

use App\Http\Requests\LoginRequest;

/**
 * @resource Login & Register
 *
 * Longer description
 */

class LoginController extends Controller
{
  /**
   * /api/login
   *
   * 
   *
   */
    public function postLogin(Request $request)
    {
      $http = new Client(['http_errors' => false]);

      $response = $http->request('POST', 'http://' . env('HOST_NAME') . '/oauth/token', [
          'form_params' => [
            'grant_type'=> 'password',
            'client_id'=> env('CLIENT_ID'),
            'client_secret'=> env('CLIENT_SECRET'),
            'username'=> $request->username,
            'password'=> $request->password
          ]
      ]);

      $return_response = json_decode((string) $response->getBody(), true);

      $user = User::where('email', $request->username)->first();

      if($user != null)
      {
        if($response->getStatusCode() === 200) {
          $return_response['user_id'] = $user->id;
        }
        

        // return response()->json($return_response['user_id'], 200);
        // return response()->json(json_decode((string) $response->getBody(), true), $response->getStatusCode());
        return response($return_response, $response->getStatusCode())->header('Content-Type', 'application/javascript');
      } else {
        return response()->json('The data you are referring to is not found!', 400);
      }

      return response()->json('Oops something when wrong!', 500);
    }

    public function postRegister(RegisterSocialLearningRequest $request)
    {
      $user = new User;

      $user->fullname = $request->fullname;
      $user->username = $request->username;
      $user->password = Hash::make($request->password);
      $user->email = $request->email;

      $user->ktp_id = $request->ktp_id;

      $user->phone = $request->phone;
      $user->mobile = $request->mobile;
      $user->mobile2 = $request->mobile2;
      $user->pin_bb = $request->pin_bb;
      $user->zipcode = $request->zipcode;
      $user->fb = $request->fb;
      $user->tw = $request->tw;
      $user->ig = $request->ig;
      $user->website = $request->website;
      $user->status = $request->status;
      $user->hobby = $request->hobby;
      $user->about = $request->about;
      $user->skill = $request->skill;
      $user->hope = $request->hope;
      $user->reason = $request->reason;

      $user->role_id = 5;

      $user->branch_id = 0;
      // $user->tenant_id = $request->tenant_id;
      $user->tenant_id = 1;

      $user->save();

      return response()->json($user);
    }

    /**
     * Logout
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
      $accessToken = Auth::user()->token();
        DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update([
                'revoked' => true
            ]);

        $accessToken->revoke();
        return response()->json(null, 204);
    }
}
