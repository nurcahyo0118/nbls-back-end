<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Log;
use App\User;

class LogController extends Controller
{
    public function index()
    {
        $users = User::all();

        return view('logs.index')->withUsers($users);
    }

    public function show($domain, $id)
    {
        $logs = Log::where('user_id', User::find($id)->id)->latest()->get();

        return view('logs.show')->withLogs($logs);
    }
}
