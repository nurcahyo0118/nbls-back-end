<?php

namespace App\Http\Controllers\tenant;

use App\Category;
use App\Http\Controllers\Controller;
use App\SubCategory;
use App\Tenant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CategoryController extends Controller
{
    public function index($subdomain)
    {
        $tenant = Tenant::where(['subdomain' => $subdomain])->first();

        $categories = Category::where(['tenant_id' => $tenant->id])->paginate(5);

        $sub_categories = SubCategory::all();

        return view('categories.index')
            ->withCategories($categories)
            ->withSubCategories($sub_categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($subdomain, Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:categories|max:50'
        ]);

        $tenant = Tenant::where(['subdomain' => $subdomain])->first();

        $category = new Category;
        $category->name = $request->name;
        $category->description = $request->description;
        $category->tenant_id = $tenant->id;

        $category->save();

//        add success message with session flash (hanya untuk 1 page)
        Session::flash('success', 'Kategori berhasil di tambahkan !');

//        refresh the page
        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($subdomain, Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|unique:categories|max:50'
        ]);

        $category = Category::find($id);
        $category->name = $request->name;
        $category->description = $request->description;

        $category->save();

//        add success message with session flash (hanya untuk 1 page)
        Session::flash('success', 'Kategori berhasil di edit !');

//        refresh the page
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($subdomain, $id)
    {
        $category = Category::find($id);

        $category->delete();

        return redirect()->back();
    }
}
