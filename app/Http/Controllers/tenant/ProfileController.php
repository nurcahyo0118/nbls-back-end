<?php

namespace App\Http\Controllers\tenant;

use Illuminate\Http\Request;
use Auth;
use App\Research;
use App\User;
use App\Comment;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Image;

class ProfileController extends Controller
{

    public function index($subdomain)
    {
        $user = Auth::user();

        return view('profile.index')->withUser($user);

    }

    public function edit()
    {
        $user = Auth::user();

        return view('profile.edit')->withUser($user);

    }

    public function update(Request $request)
    {
        $user = Auth::user();
        $user->fullname = $request->fullname;
//        $user->email = $request->email;
        $user->ktp_id = $request->ktp_id;
        $user->birth_date = $request->birth_date;
        $user->sex = $request->sex;
        $user->phone = $request->phone;
        $user->pin_bb = $request->pin_bb;
        $user->fb = $request->fb;
        $user->tw = $request->tw;
        $user->website = $request->website;
        $user->address = $request->address;
        $user->zipcode = $request->zipcode;
        $user->hobby = $request->hobby;
        $user->reason = $request->reason;
        $user->save();

        Session::flash('success', 'Berhasil memperbarui profil !');

        return redirect()->route('me', $_SERVER['HTTP_HOST']);
    }

    public function dashboard()
    {
        $research = Research::where('author', Auth::id())->first();
        $author = User::find($research->author);
        $comments = Comment::where('research_id', $research->id)->get();
        $comment_id = Comment::where('author', $research->author)->get();


        return view('profile.dashboard')
            ->withResearch($research)
            ->withAuthor($author);
    }

    public function changeImage(Request $request)
    {
        $user = Auth::user();
        if ($request->hasFile('photo')) {
            if ($user->photo != null) {
                if (file_exists('images/photo/' . $user->photo)) {
                    unlink(public_path('images/photo/' . $user->photo));
                }
            }
            $image = $request->file('photo');
            $filename = time() . '.' . $image->getClientOriginalName();
            $path = public_path('images/photo/' . $filename);
            Image::make($image->getRealPath())->save($path);
            $user->photo = $filename;
//            $user->photo = 'asdasdas';
//            dd($request);
        }

//        $user->photo = 'asdasdas';
        $user->save();

        return redirect()->back();
    }

    public function changePassword(Request $request)
    {
        $user = User::find(Auth::user()->id);

        if (Hash::check($request->old_password, $user->password)) {
            $user->password = bcrypt($request->new_password);
            $user->save();

            Session::flash('success', 'Kata sandi berhasil di ubah !');

            return redirect()->back();
        } else {
            Session::flash('failed', 'Kata sandi salah !');
            return redirect()->back();

        }
    }
}
