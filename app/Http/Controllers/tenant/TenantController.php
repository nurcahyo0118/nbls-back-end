<?php

namespace App\Http\Controllers\tenant;

use App\Permission;
use Illuminate\Http\Request;

use Auth;
use App\Tenant;
use App\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Session;
use Image;

class TenantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($subdomain)
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($subdomain)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($subdomain, Request $request)
    {

    }

    public function show($subdomain, $id)
    {

    }

    public function me($subdomain)
    {
        $tenant = Tenant::where(['subdomain' => $subdomain])->first();

        $users = User::where(['tenant_id' => $tenant->id])->get();

        $permission = Auth::user()->role->permissions->where('tenant_id', $tenant->id)->first();

        return view('tenants.me')
            ->withTenant($tenant)
            ->withUsers($users)
            ->withPermission($permission);
    }

    public function tenantMe($subdomain)
    {
        $tenant = Tenant::where(['subdomain' => $subdomain])->first();

        $users = User::where(['tenant_id' => $tenant->id])->get();

        return view('tenants.me')
            ->withTenant($tenant)
            ->withUsers($users);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($subdomain, $id)
    {
        $tenant = Tenant::find($id);

        return view('tenants.edit')
            ->withTenant($tenant);
    }

    public function meEdit($subdomain)
    {
        $tenant = Tenant::where(['subdomain' => $subdomain])->first();

        return view('tenants.meedit')
            ->withTenant($tenant);
    }

    public function tenantMeEdit($subdomain)
    {
        $tenant = Tenant::where(['subdomain' => $subdomain])->first();

        return view('tenants.meedit')
            ->withTenant($tenant);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($subdomain, Request $request, $id)
    {
        //        Validate request
        if ((int)$request->is_cname === 1) {
            /* subdomain */
            $this->validate($request, [
                'name' => 'required|max:150|unique:tenants,name,' . $id,
                'subdomain' => 'max:50|unique:tenants,subdomain,' . $id
            ]);
        } else {
            /* SUBDOMAIN */
            $this->validate($request, [
                'name' => 'required|max:150|unique:tenants,name,' . $id,
                'subdomain' => 'max:10|unique:tenants,subdomain,' . $id
            ]);
        }

//        Save Tenant to database
        $tenant = Tenant::find($id);
        Log::debug($tenant);
        $tenant->name = $request->name;
        $tenant->description = $request->description;

        if ((int)$request->is_cname === 1) {
            $tenant->subdomain = $request->subdomain;
            $tenant->subdomain = null;
        } else {
            $tenant->subdomain = null;
            $tenant->subdomain = $request->subdomain;
        }

        $tenant->is_cname = (int)$request->is_cname;

        if ($request->hasFile('company_logo')) {

            if ($tenant->photo != null) {
                if (file_exists('images/photo/tenant/' . $tenant->photo)) {
                    unlink(public_path('images/photo/tenant/' . $tenant->photo));
                }
            }

            $image = $request->file('company_logo');
            $filename = time() . '.' . $image->getClientOriginalName();
            $path = public_path('images/photo/tenant/' . $filename);
            Image::make($image->getRealPath())->save($path);
            $tenant->company_logo = $filename;

        }

        $tenant->theme = $request->theme;
        $tenant->tawk_to = $request->tawk_to;
        $tenant->author_id = Auth::id();

        $tenant->save();

        if ($subdomain != env('HOST_NAME')) {
            Auth::guard('web')->logout();

            if ($tenant->is_cname) {
                return Redirect::to('http://' . $tenant->subdomain);
            } else {
                return redirect()->route('tenant.landing', $tenant->subdomain);
            }

        } else {
            return redirect()->route('tenants.index', $subdomain);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($subdomain, $id)
    {
        $tenant = Tenant::find($id);

//        if ($tenant->file != null) {
//            unlink('file/research/'.$tenant->file);
//        }

        $tenant->delete();

        return redirect()->back();
    }
}
