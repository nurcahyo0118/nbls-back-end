<?php

namespace App\Http\Controllers\tenant;

use App\User;
use Illuminate\Http\Request;

class PartialController extends Controller
{
    public function section($subdomain)
    {
        $categories = Category::all();
        $courses = Course::all();

        return view('courses.index')
            ->withCategories($categories)
            ->withCourses($courses);
    }
}
