<?php

namespace App\Http\Controllers\tenant;

use App\Permission;
use App\Tenant;
use Illuminate\Http\Request;
use App\Role;

class RoleController extends Controller
{
    public function index($subdomain)
    {
        $roles = Role::all();

        return view('roles.index')->withRoles($roles);
    }

    public function show($subdomain, $id)
    {
        $tenant = Tenant::where(['subdomain' => $subdomain])->first();
        $role = Role::find($id);
        $permission = Permission::where(['role_id' => $role->id, 'tenant_id' => $tenant->id])->first();

        return view('roles.show')
            ->withPermission($permission)
            ->withRole($role);
    }

    public function update($subdomain, Request $request, $id)
    {
//        dd($request);
        $tenant = Tenant::where(['subdomain' => $subdomain])->first();

        $role = Role::find($id);

        $permission = Permission::where(['role_id' => $role->id, 'tenant_id' => $tenant->id])->first();

        if ($permission === null) {
            $permission = new Permission;
        }

        $permission->read_role = $request->read_role != null;
        $permission->update_role = $request->update_role != null;

        $permission->read_user = $request->read_user != null;
        $permission->create_user = $request->create_user != null;
        $permission->update_user = $request->update_user != null;
        $permission->delete_user = $request->delete_user != null;

        $permission->read_course = $request->read_course != null;
        $permission->create_course = $request->create_course != null;
        $permission->update_course = $request->update_course != null;
        $permission->delete_course = $request->delete_course != null;

        $permission->read_category = $request->read_category != null;
        $permission->create_category = $request->create_category != null;
        $permission->update_category = $request->update_category != null;
        $permission->delete_category = $request->delete_category != null;

        $permission->read_tenant = $request->read_tenant != null;
        $permission->create_tenant = $request->create_tenant != null;
        $permission->update_tenant = $request->update_tenant != null;
        $permission->delete_tenant = $request->delete_tenant != null;

        $permission->read_tenant_me = $request->read_tenant_me != null;
        $permission->create_tenant_me = $request->create_tenant_me != null;
        $permission->update_tenant_me = $request->update_tenant_me != null;
        $permission->delete_tenant_me = $request->delete_tenant_me != null;

        $permission->role_id = $role->id;
        $permission->tenant_id = $tenant->id;

        $permission->save();

        return redirect()->route('me', $subdomain);
    }

}
