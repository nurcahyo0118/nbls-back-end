<?php

namespace App\Http\Controllers\tenant;

use App\Section;
use App\Material;
use App\Quiz;
use Illuminate\Http\Request;
use App\Http\Controllers\CourseController;

use GuzzleHttp\Client;

class SectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($subdomain, Request $request)
    {
      $req = json_decode($request->getContent());

      $sections = Section::where(['course_id' => $req->course_id])->get();

      $section = new Section;
      $section->title = $req->title;
      $section->description = $req->description;
      $section->line = $sections->count() + 1;
      $section->course_id = (int) $req->course_id;
      $section->wistia_project_id = $req->wistia_hashed_id;

      $section->save();

      return view('courses.childsection')->withSection($section);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($subdomain, Request $request, $id)
    {

      return $request->all();
      $section = Section::find($id);
      $section->title = $request->title;
      $section->description = $request->description;

      $section->save();

      return response()->json([
       'section_id' => $section->id,
       'view' => (string)view('courses.childsection')->withSection($section)
      ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($subdomain, $id)
    {
        $http = new Client;

        $section = Section::find($id);

        $materials = $section->materials;

        foreach ($materials as $material) {

          $response = $http->request('DELETE', 'https://api.wistia.com/v1/medias/' . $material->wistia_media_url . '.json', [
            'form_params' => [
              'api_password' => env('WISTIA_TOKEN')
            ]
          ]);

          if ($response->getStatusCode() === 200) {
            $material->delete();
            Quiz::where(['material_id' => $material->id])->delete();
            $section->delete();
          }
          else
          {
            return response()->json(['message' => 'Failed delete media from wistia'], 404);
          }

        }

    }

    public function deleteAll($subdomain, $id)
    {
        $sections = Section::where(['course_id' => $id])->get();

        foreach ($sections as $section) {

          $http = new Client;

          $materials = $section->materials;

          foreach ($materials as $material) {

            $response = $http->request('DELETE', 'https://api.wistia.com/v1/medias/' . $material->wistia_media_url . '.json', [
              'form_params' => [
                'api_password' => env('WISTIA_TOKEN')
              ]
            ]);

            if ($response->getStatusCode() === 200) {
              $material->delete();
              Quiz::where(['material_id' => $material->id])->delete();
              $section->delete();
            }
            else
            {
              return response()->json(['message' => 'Failed delete media from wistia'], 404);
            }

          }
        }
    }
}
