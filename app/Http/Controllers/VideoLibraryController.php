<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Material;

use Auth;
use Log;

use GuzzleHttp\Client;

class VideoLibraryController extends Controller
{
    public function index()
    {
        $courses = Auth::user()->mycourses;

        return view('video-libraries.index');
    }

    public function materialVideos()
    {
        $http = new Client;

        $courses = Auth::user()->mycourses;
        $materials = [];

        foreach ($courses as $course) {

            foreach ($course->sections as $section) {

                foreach ($section->materials as $material) {
                    Log::debug('https://api.wistia.com/v1/medias/' . $material->wistia_media_hashed_id . '.json');
                    $response = $http->request('GET', 'https://api.wistia.com/v1/medias/' . $material->wistia_media_hashed_id . '.json', [
                        'form_params' => [
                            'api_password' => env('WISTIA_TOKEN')
                        ]
                    ]);

                    if ($response->getStatusCode() === 200) {
                        // Jika Request berhasil
                        Log::debug($response->getBody());
                        array_push($materials, json_decode((string)$response->getBody()));
                    } else {
                        // Jika gagal
                        // return response()->json(['message' => 'Failed delete media from wistia'], 404);
                    }

                    // array_push($materials, $material);
                }

            }

        }


        // return response()->json($materials, 200);
        Log::debug($materials);
        return view('video-libraries.child.child-video')->withMaterials($materials);
    }
}
