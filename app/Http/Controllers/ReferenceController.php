<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Reference;

class ReferenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reference = new Reference;

        $reference->section_id = $request->section_id;
        $reference->title = $request->title;

        if ($request->hasFile('file')) {

            if ($reference->file != null) {
                if (file_exists('reference-file/' . $reference->file)) {
                    unlink(public_path('reference-file/' . $reference->file));
                }
            }

            $file = $request->file('file');
            $filename = time() . '.' . $file->getClientOriginalName();
            $path = public_path('reference-file/');
            $file->move($path, $filename);
            $reference->file = $filename;

            $reference->save();

        } else {
            return response()->json('File not found', 404);
        }

        return view('courses.child-reference')->withReference($reference);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($domain, $id)
    {
        $reference = Reference::find($id);
        $reference->delete();
    }

}
