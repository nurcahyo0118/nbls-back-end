<?php

namespace App\Http\Controllers;

use App\Category;
use App\Course;
use App\Section;
use App\SubCategory;
use App\Tenant;
use App\Material;
use App\Quiz;
use App\NotificationSocialLearning;
use App\SalesWidgetSocialLearning;
use App\BackendClass;
use App\Encounter;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Image;
use Session;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($domain)
    {
        $categories = Category::where(['tenant_id' => Auth::user()->tenant['id']])->get();
        $courses = Course::where(['tenant_id' => Auth::user()->tenant['id']])->get();

        return view('courses.index')
            ->withCategories($categories)
            ->withCourses($courses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($domain)
    {
        $tenant = Tenant::where(['domain' => $domain])->first();

        $categories = Category::where(['tenant_id' => $tenant->id])->get();

        $sub_categories = SubCategory::all();

        return view('courses.create')
            ->withCategories($categories)
            ->withSubCategories($sub_categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($domain, Request $request)
    {
        $tenant = Auth::user()->tenant;

        $http = new Client;

        $response = $http->request('POST', 'https://api.wistia.com/v1/projects.json', [
            'form_params' => [
                'name' => $request->title,
                'adminEmail' => Auth::user()->email,
                'api_password' => env('WISTIA_TOKEN')
            ]
        ]);

        Log::debug($response->getBody());

        $wistia_project = json_decode($response->getBody(), true);

        Log::debug('Hashed ID = ' . $wistia_project['hashedId']);

        $course = new Course;

        /* General */
        $course->title = $request->title;
        $course->description = $request->description;
        $course->category_id = $request->category_id;
        $course->sub_category_id = 0;

        if($request->is_free)
        {
            $course->price = 0;
        }
        else
        {
            $course->price = $request->price;
        }

        /* Detail */
        $course->suitable = $request->suitable;
        $course->requirement = $request->requirement;
        $course->can_be = $request->can_be;

        /* Image */
        if ($request->hasFile('image')) {
            if ($course->image != null) {
                if (file_exists('xxcourses/images/' . $course->image)) {
                    unlink(public_path('xxcourses/images/' . $course->image));
                }
            }
            $image = $request->file('image');
            $filename = time() . '.' . $image->getClientOriginalName();
            $path = public_path('/xxcourses/images/' . $filename);
            Image::make($image->getRealPath())->save($path);
            $course->image = $filename;
        }

        /* Video */
        $course->video = $request->video;

        $course->wistia_hashed_id = $wistia_project['hashedId'];

        $course->duration = 0;
        $course->status = 'POSTED';
        $course->tenant_id = $tenant->id;
        $course->author_id = Auth::id();

        $course->save();

        return redirect()->route('courses.edit', [$_SERVER['HTTP_HOST'], $course->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($domain, $id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($domain, $id)
    {
        $tenant = Auth::user()->tenant;

        $course = Course::find($id);

        $categories = Category::where(['tenant_id' => $tenant->id])->get();

        $sub_categories = SubCategory::where(['category_id' => $course->category_id])->get();

        return view('courses.edit')
            ->withCourse($course)
            ->withCategories($categories)
            ->withSubCategories($sub_categories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($domain, Request $request, $id)
    {
        $tenant = Tenant::where(['domain' => $domain])->first();

        $course = Course::find($id);

        /* General */
        $course->title = $request->title;
        $course->description = $request->description;
        $course->category_id = $request->category_id;
        $course->sub_category_id = $request->sub_category_id;
        
        if($request->is_free)
        {
            $course->price = 0;
        }
        else
        {
            $course->price = $request->price;
        }

        /* Detail */
        $course->suitable = $request->suitable;
        $course->requirement = $request->requirement;
        $course->can_be = $request->can_be;

//        dd($request);
        /* Image */
        if ($request->hasFile('image')) {
            if ($course->image != null) {
                if (file_exists('xxcourses/images/' . $course->image)) {
                    unlink(public_path('xxcourses/images/' . $course->image));
                }
            }
            $image = $request->file('image');
            $filename = time() . '.' . $image->getClientOriginalName();
            $path = public_path('/xxcourses/images/' . $filename);
            Image::make($image->getRealPath())->save($path);
            $course->image = $filename;
        }

        /* Video */
        $course->video = $request->video;

        $course->duration = 0;
        $course->status = 'POSTED';
        $course->tenant_id = $tenant->id;
        $course->author_id = Auth::id();

        $course->save();

        return redirect()->route('courses.edit', [$_SERVER['HTTP_HOST'], $course->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($domain, $id)
    {
        $http = new Client(['http_errors' => false]);

        $course = Course::find($id);

        if($course) {
            $response = $http->request(
                'DELETE', 
                'https://api.wistia.com/v1/projects/' . $course->wistia_hashed_id . '.json', 
                [
                    'form_params' => [
                        'api_password' => env('WISTIA_TOKEN')
                    ]
                ]
            );
    
            // Related data
            foreach($course->sections as $section) {
                $section->materials()->delete();
                $section->references()->delete();
                $section->exercises()->delete();
                $section->essays()->delete();
            }
    
            $course->sections()->delete();

            $widgets = SalesWidgetSocialLearning::where('course_id', $course->id)->get();
            $encounters = Encounter::where('course_id', $course->id)->get();
            $classes = BackendClass::where('course_id', $course->id)->get();

            if($widgets != null)
            {
                foreach($widgets as $widget)
                {
                    $widget->delete();
                }
            }

            if($encounters != null)
            {
                foreach($encounters as $encounter)
                {
                    $encounter->delete();
                }
            }

            if($classes != null)
            {
                foreach($classes as $class)
                {
                    $class->delete();
                }
            }
    
            $course->delete();
            
            Session::flash('success', 'Berhasil menghapus kursus !');

        } else {
            Session::flash('failed', 'Gagal menghapus kursus, kursus tidak ditemukan !');
        }

        return redirect()->back();
    }

    public function join(Request $request, $id)
    {
        $current_user = Auth::user();
        $course = Course::find($id);

        if($course != null)
        {
            $notification = new NotificationSocialLearning;
            $notification->title = $current_user->fullname . ' telah bergabung dengan kursus anda';
            $notification->content = $current_user->fullname . ' telah bergabung dengan kursus anda';
            $notification->user_id = $course->author_id;

            $notification->save();

            $course->users()->attach(Auth::id());

            return view('social-learnings.course.join')->withCourse($course);
        }
        else
        {
            return response()->json('Oops somethings when wrong !', 500);
        }
        
    }

    public function leave(Request $request, $id)
    {
        $course = Course::find($id);

        // if($course->users->count() > 0) {
        $course->users()->detach(Auth::id());
        // }

        return view('social-learnings.course.join')->withCourse($course);
    }

    public function sectionsPage($id)
    {
        $course = Course::find($id);

        return view('courses.pages.sections-page')->withCourse($course);
    }

    public function quizzesPage($id)
    {
        $course = Course::find($id);

        return view('courses.pages.quizzes-page')->withCourse($course);
    }

    public function exercisesPage($id)
    {
        $course = Course::find($id);

        return view('courses.pages.exercises-page')->withCourse($course);
    }

    public function referencesPage($id)
    {
        $course = Course::find($id);

        return view('courses.pages.references-page')->withCourse($course);
    }


}
