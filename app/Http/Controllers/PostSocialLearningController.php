<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\PostSocialLearning;
use App\Comment;
use App\CommentLike;
use App\CommentDislike;
use App\PostLike;
use App\PostDislike;
use App\User;
use App\Course;
use App\BadgeSocialLearning;

use Image;
use DB;

use App\QuestionSocialLearning;
use App\LinkPostSocialLearning;

use Illuminate\Support\Facades\Session;

class PostSocialLearningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $instructors = User::where([
            'tenant_id' => Auth::user()->tenant_id,
            'role_id' => 4])->get();

        $allusers = \App\User::with('points')->get()->sortByDesc(function ($item) {
            return $item->points()->sum('point');
        })->take(5);

        $courses = Auth::user()->courses;

        $posts = PostSocialLearning::where('user_id', Auth::id())
            ->orWhereIn('user_id', Auth::user()->followings)
            ->latest()
            ->get();

        // $questions = QuestionSocialLearning::where(['material_id' => 0])->latest()->get();

        $questions = QuestionSocialLearning::where('user_id', Auth::id())
            ->orWhereIn('user_id', Auth::user()->followings)
            ->latest()
            ->get();

        return view('social-learnings.posts.index')
            ->withPosts($posts)
            ->withQuestions($questions)
            ->withInstructors($instructors)
            ->withCourses($courses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $post = new PostSocialLearning;

        if ($request->hasFile('photo')) {
            if ($post->photo != null) {
                if (file_exists('images/photo/' . $post->photo)) {
                    unlink(public_path('images/photo/' . $post->photo));
                }
            }
            $image = $request->file('photo');
            $filename = time() . '.' . $image->getClientOriginalName();
            $path = public_path('images/photo/' . $filename);
            Image::make($image->getRealPath())->save($path);
            $post->photo = $filename;
        } else {
            $post->photo = $request->photo;
        }

        $post->content = $request->content;
        $post->user_id = Auth::id();
        $post->tenant_id = Auth::user()->tenant->id;
        $post->save();

        if ($request->link != null) {
            $link = new LinkPostSocialLearning;
            $link->link = $request->link;
            $link->post_id = $post->id;
            $link->user_id = Auth::id();

            $link->save();
        }

        return view('social-learnings.posts.child.post-child')->withPost($post);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::where(['username' => $id])->first();

        $profiles = PostSocialLearning::where('user_id', $user->id)->latest()->get()->sortByDesc("id");
        $badges = BadgeSocialLearning::all();
        $my_badges = [];

        foreach($badges as $badge)
        {
            if($badge->simple_name == 'badge_make_friend') {

                if($user->followers()->count() >= 10) {
                    array_push($my_badges, $badge);
                }

            } else if($badge->simple_name == 'badge_fans') {

                if($user->followers()->count() >= 50) {
                    array_push($my_badges, $badge);
                }

            } else if($badge->simple_name == 'badge_popular') {

                if($user->followers()->count() >= 100) {
                    array_push($my_badges, $badge);
                }

            } else if($badge->simple_name == 'badge_alumni') {

                $submitted_encounter = DB::table('user_encounters')->where([
                    'user_id' => $user->id
                ])->first();
                
                if($submitted_encounter != null) {
                    array_push($my_badges, $badge);
                }
                
            } else if($badge->simple_name == 'badge_welcome') {

                $badge->logo = 'fa-sign-out';

                $login_hour = DB::table('logs')->where([
                    'reason' => 'LOG_LOGIN',
                    'user_id' => $user->id
                ])->count();

                if($login_hour > 7) {
                    array_push($my_badges, $badge);
                }

            } else if($badge->simple_name == 'badge_nocturnal') {

                $login_hour = DB::table('logs')->where([
                    'reason' => 'LOG_LOGIN',
                    'user_id' => $user->id
                ])->count();

                if($login_hour > 7) {
                    array_push($my_badges, $badge);
                }

            } else if($badge->simple_name == 'badge_i_time') {

                $i_time = DB::table('itime_social_learnings')->where('user_id', $user->id)->first();

                if($i_time != null) {
                    array_push($my_badges, $badge);
                }

            }
            
        }

        if ($user->id == Auth::id()) {
            return redirect()->route('social-learning-profile.index');
        }

        return view('social-learnings.profile.show')
            ->withUser($user)
            ->withMyBadges($my_badges);
    }

    /**register
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        dd($request);
//        return $request->all();
        $post = PostSocialLearning::find($id);

        if ($request->hasFile('photo')) {
            if ($post->photo != null) {
                if (file_exists('images/photo/' . $post->photo)) {
                    unlink(public_path('images/photo/' . $post->photo));
                }
            }
            $image = $request->file('photo');
            $filename = time() . '.' . $image->getClientOriginalName();
            $path = public_path('images/photo/' . $filename);
            Image::make($image->getRealPath())->save($path);
            $post->photo = $filename;
        }
        //  else {
        //     $post->photo = $request->photo;
        // }

        $post->content = $request->content;
        $post->user_id = Auth::id();
        $post->tenant_id = Auth::user()->tenant->id;
        $post->save();

        if ($request->link != null) {
            $link = LinkPostSocialLearning::where('post_id', $post->id)->first();

            if ($link != null) {
                $link->link = $request->link;

                $link->save();
            } else {
                $link = new LinkPostSocialLearning;
                $link->link = $request->link;
                $link->post_id = $post->id;
                $link->user_id = Auth::id();

                $link->save();
            }
        }

        return redirect()->back();
        return view('social-learnings.posts.child.post-child')->withPost($post);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = PostSocialLearning::find($id);

        $post->likes()->delete();
        $post->dislikes()->delete();
        $post->postComments()->delete();
        $post->delete();

        $link = LinkPostSocialLearning::where('post_id', $post->id)->first();
        if ($link != null) {
            $link->delete();
        }

        return response()->json(['message' => 'Success'], 200);
    }

    public function like(Request $request, $id)
    {
        $post = PostSocialLearning::find($id);

        $like = PostLike::where([
            'post_id' => $post->id,
            'user_id' => Auth::id()
        ])->first();

        $dislike = PostDislike::where([
            'post_id' => $post->id,
            'user_id' => Auth::id()
        ])->first();

        if ($like != null) {
            $like->delete();

        } else {
            $post->likes()->save(new PostLike([
                'post_id' => $post->id,
                'user_id' => Auth::id()
            ]));

        }

        if ($dislike != null) {
            $dislike->delete();
        }

        return view('social-learnings.posts.child.post-child')->withPost($post);
    }

    public function dislike(Request $request, $id)
    {
        $post = PostSocialLearning::find($id);

        $like = PostLike::where([
            'post_id' => $post->id,
            'user_id' => Auth::id()
        ])->first();

        $dislike = PostDislike::where([
            'post_id' => $post->id,
            'user_id' => Auth::id()
        ])->first();

        if ($dislike != null) {
            $dislike->delete();

        } else {
            $post->likes()->save(new PostDislike([
                'post_id' => $post->id,
                'user_id' => Auth::id()
            ]));

        }

        if ($like != null) {
            $like->delete();
        }

        return view('social-learnings.posts.child.post-child')->withPost($post);
    }

    public function postComment(Request $request, $id)
    {
        $post = PostSocialLearning::find($id);

        $post->postComments()->save(new Comment([
            'question_id' => 0,
            'post_id' => $post->id,
            'user_id' => Auth::id(),
            'body' => $request->content,
        ]));
        return view('social-learnings.posts.child.post-child')->withPost($post);
    }

    public function likeComment($id)
    {
        $comment = Comment::find($id);

        $like = CommentLike::where([
            'comment_id' => $comment->id,
            'user_id' => Auth::id()
        ])->first();

        $dislike = CommentDislike::where([
            'comment_id' => $comment->id,
            'user_id' => Auth::id()
        ])->first();

        if ($like != null) {
            $like->delete();

        } else {
            $comment->likes()->save(new CommentLike([
                'comment_id' => $comment->id,
                'user_id' => Auth::id()
            ]));

        }

        if ($dislike != null) {
            $dislike->delete();
        }

        return view('social-learnings.posts.child.post-child')->withPost($comment->post);

    }

    public function dislikeComment($id)
    {
        $comment = Comment::find($id);

        $like = CommentLike::where([
            'comment_id' => $comment->id,
            'user_id' => Auth::id()
        ])->first();

        $dislike = CommentDislike::where([
            'comment_id' => $comment->id,
            'user_id' => Auth::id()
        ])->first();

        if ($dislike != null) {
            $dislike->delete();

        } else {
            $comment->likes()->save(new CommentDislike([
                'comment_id' => $comment->id,
                'user_id' => Auth::id()
            ]));

        }

        if ($like != null) {
            $like->delete();
        }

        return view('social-learnings.posts.child.post-child')->withPost($comment->post);
    }

}
