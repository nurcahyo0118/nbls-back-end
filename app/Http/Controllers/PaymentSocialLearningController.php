<?php

namespace App\Http\Controllers;

// require_once base_path('public/payment-method/lib').'/NicepayLib.php';

use Illuminate\Http\Request;

use App\OrderHistorySocialLearning;
use App\NotificationSocialLearning;

use Auth;
use Log;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

class PaymentSocialLearningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nicepay_carts = [];
        $carts = Auth::user()->carts;

        foreach ($carts as $cart) {
            $data = [
                "img_url" => env('HOST'),
                "goods_name" => $cart->title,
                "goods_detail" => $cart->description,
                "goods_amt" => $cart->price
            ];

            array_push($nicepay_carts, $data);
        }

        // return json_encode($nicepay_carts);

        return view('social-learnings.payments.index')
            ->withCarts($carts)
            ->withNicepayCarts(json_encode($nicepay_carts));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function createTransaction(Request $request)
    {
        $http = new Client(['http_errors' => false]);

        $goodsNm = str_random(10);

        if($request->payMethod === '02')
        {
            $response = $http->post(env('NICEPAY_ENVIRONMENT') . 'nicepay/api/onePass.do', 
                array(
                    'form_params' => array(
                        'iMid' => env('IMID'),
                        'payMethod' => '02',
                        'currency' => 'IDR',
                        'amt' => $request->amount,
                        'referenceNo' => '99997',
                        'goodsNm' => $goodsNm,
                        'billingNm' => 'Customer Name',
                        'billingPhone' => '012345678912',
                        'billingEmail' => 'email@customer.com',
                        'callBackUrl' => env('APP_URL') . '/payments/confirm/' . $goodsNm,
                        'dbProcessUrl' => env('APP_URL') . '/payments/confirm/' . $goodsNm,
                        'description' => 'Payment of referenceNo 99997',
                        'merchantToken' => env('MERCHANT_TOKEN'),
                        'cartData' => json_encode('{}'),
                        'bankCd' => $request->bankCd
                    ),
                    'headers' => [
                        'Content-Type' => 'application/x-www-form-urlencoded'
                    ]
                )
            );

            $data = json_decode((string) $response->getBody(), true);

            if($data['resultMsg'] === "SUCCESS")
            {
                // Success
                $order_history = new OrderHistorySocialLearning;
                $order_history->tXid = $data['tXid'];
                $order_history->callbackUrl = $data['callbackUrl'];
                $order_history->description = $data['description'];
                $order_history->transDt = $data['transDt'];
                $order_history->transTm = $data['transTm'];
                $order_history->bankVacctNo = $data['bankVacctNo'];
                $order_history->resultCd = $data['resultCd'];
                $order_history->resultMsg = $data['resultMsg'];
                $order_history->referenceNo = $data['referenceNo'];
                $order_history->payMethod = $data['payMethod'];
                $order_history->user_id = Auth::id();
                $order_history->goodsNm = $goodsNm;

                $order_history->save();

                return redirect()->route('social-learning-order-history.index', $_SERVER['HTTP_HOST']);
            }
            else
            {
                // Session::flash('failed', 'Oops, Payment is failed to register');
                return redirect()->back();
            }
        }
        
        if($request->payMethod === '01')
        {
            // return redirect()->back();
            $response = $http->post(env('NICEPAY_ENVIRONMENT') . 'nicepay/api/orderRegist.do', 
                array(
                    'form_params' => array(
                        'iMid' => env('IMID'),
                        'payMethod' => '01',
                        'currency' => 'IDR',
                        'amt' => 10000,
                        'instmntType' => 1,
                        'instmntMon' => 1,
                        'referenceNo' => '99997',
                        'goodsNm' => $goodsNm,
                        'billingNm' => 'Customer Name',
                        'billingPhone' => '012345678912',
                        'billingEmail' => 'email@customer.com',
                        'billingCity' => 'Jakarta',
                        'billingState' => 'Jakarta',
                        'billingPostCd' => '51355',
                        'billingCountry' => 'Indonesia',
                        'callBackUrl' => env('APP_URL') . '/payments/confirm/' . $goodsNm,
                        'dbProcessUrl' => env('APP_URL') . '/payments/confirm/' . $goodsNm,
                        'description' => 'Payment of referenceNo 99997',
                        'merchantToken' => env('MERCHANT_TOKEN'),
                        'cartData' => json_encode('{}'),
                        'userIP' => '127.0.0.1'
                    ),
                    'headers' => [
                        'Content-Type' => 'application/x-www-form-urlencoded'
                    ]
                )
            );

            $data = json_decode((string) str_replace('0277', '', $response->getBody()), true);

            // return str_replace('0277', '', $response->getBody());

            // return response()->json($response->getBody(), 200);
            // return $data;
            // return $data['data'];

            if($data['data']['resultMsg'] === "SUCCESS")
            {
                // Success
                $order_history = new OrderHistorySocialLearning;
                $order_history->tXid = $data['data']['tXid'];
                $order_history->callbackUrl = env('APP_URL') . '/payments/confirm/' . $goodsNm;
                $order_history->description = '';
                $order_history->transDt = '';
                $order_history->transTm = '';
                $order_history->bankVacctNo = '';
                $order_history->resultCd = $data['data']['resultCd'];
                $order_history->resultMsg = $data['data']['resultMsg'];
                $order_history->referenceNo = '';
                $order_history->payMethod = $request->payMethod;
                $order_history->user_id = Auth::id();
                $order_history->goodsNm = $goodsNm;

                $order_history->save();

                return redirect($data['data']['requestURL'] . "?tXid=" . $data['data']['tXid']);
            }
            else
            {
                // Session::flash('failed', 'Oops, Payment is failed to register');
                return redirect()->back();
            }
            // return $response->getBody();
            // return $data;
        }

        return response()->json('Oops something when wrong!', 500);
    }
    
    public function confirmPayment($id)
    {
        $order_history = OrderHistorySocialLearning::where('goodsNm', $id)->first();
        $order_history->status = 'paid';
        $order_history->save();

        $notification = new NotificationSocialLearning;
        $notification->title = Auth::user()->fullname . ' pembayaran telah di konfirmasi';
        $notification->content = Auth::user()->fullname . ' pembayaran telah di konfirmasi';
        $notification->user_id = Auth::id();

        $notification->save();

        return response()->json('Payment confirmed !', 200);
    }
}
