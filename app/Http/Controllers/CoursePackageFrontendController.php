<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CoursePackage;
use App\Tenant;
use App\SubCategory;
use Auth;

use Illuminate\Pagination\Paginator;

class CoursePackageFrontendController extends Controller
{

    public function index()
    {
        $domain = $_SERVER['HTTP_HOST'];
        $tenant = Tenant::where('domain', $domain)->first();

        if($tenant != null)
        {
            return view('social-learnings.course-package-front-end.index');

        } else {
            $tenant = Tenant::where('domain', $domain)->first();

            $explode = explode('.', $_SERVER['HTTP_HOST']);

            if ($explode != null) {
                $subdomain = array_shift($explode);

                $tenant = Tenant::where('subdomain', $subdomain)->first();

                return view('social-learnings.course-package-front-end.index');
            }
        }

        return redirect()->back();

    }

    public function show($id)
    {
        $tenant = null;

        $explode = explode('.', $_SERVER['HTTP_HOST']);

        if ($explode != null) {
            $subdomain = array_shift($explode);

            if($subdomain !== env('SUBDOMAIN'))
            {
                $tenant = Tenant::where('subdomain', $subdomain)->first();
            }
            else
            {
                $tenant = Tenant::where('domain', $_SERVER['HTTP_HOST'])->first();
            }

        }
        else
        {
            $tenant = Tenant::where('domain', $_SERVER['HTTP_HOST'])->first();
        }

        if($tenant != null)
        {
            $course_package = CoursePackage::find($id);

            if($course_package != null)
            {
                return view('social-learnings.course-package-front-end.show')->withCoursePackage($course_package);
            }
            else
            {
                return redirect()->back();
            }
            
        }
        else
        {
            return response()->json('Merchant Not Found !', 400);
        }

        $course_package = CoursePackage::find($id);
        // $my_review = $course_package->reviews()->where('user_id', Auth::id())->first();

        return view('social-learnings.course-package-front-end.show')
            ->withCoursePackage($course_package);
            // ->withMyReview($my_review);
    }

    public function loadAllCourses()
    {
        $itemPerPage = 3;
        $currentPage = 1;

        if(request()->has('page'))
        {
            $currentPage = request()->page;
        }

        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });

        $tenant = null;

        $explode = explode('.', $_SERVER['HTTP_HOST']);

        if ($explode != null) {
            $subdomain = array_shift($explode);

            if($subdomain !== env('SUBDOMAIN'))
            {
                $tenant = Tenant::where('subdomain', $subdomain)->first();
            }
            else
            {
                $tenant = Tenant::where('domain', $_SERVER['HTTP_HOST'])->first();
            }

        }
        else
        {
            $tenant = Tenant::where('domain', $_SERVER['HTTP_HOST'])->first();
        }

        if($tenant != null)
        {
            $course_packages = CoursePackage::where(['tenant_id' => $tenant->id])->paginate($itemPerPage);

            return response()->json([
                'view' => (string)view('social-learnings.course-package-front-end.child.child-all-course')
                    ->with('filter', 'all')
                    ->withCoursePackages($course_packages)
            ], 200);
        }
        else
        {
            return response()->json('Merchant Not Found !', 400);
        }

        return response()->json('Oops somethings when wrong!', 500);
    }

    public function loadSearchAllCourses($keyword)
    {
        $itemPerPage = 3;
        $currentPage = 1;

        if(request()->has('page'))
        {
            $currentPage = request()->page;
        }

        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });

        $tenant = null;

        $explode = explode('.', $_SERVER['HTTP_HOST']);

        if ($explode != null) {
            $subdomain = array_shift($explode);

            if($subdomain !== env('SUBDOMAIN'))
            {
                $tenant = Tenant::where('subdomain', $subdomain)->first();
            }
            else
            {
                $tenant = Tenant::where('domain', $_SERVER['HTTP_HOST'])->first();
            }

        }
        else
        {
            $tenant = Tenant::where('domain', $_SERVER['HTTP_HOST'])->first();
        }

        if($tenant != null)
        {
            $courses = $tenant->courses()->where('title', 'like', '%' . $keyword . '%')->paginate($itemPerPage);

            return response()->json([
                'view' => (string)view('social-learnings.course-front-end.child.child-all-course')
                            ->with('filter', 'search')
                            ->withCourses($courses)
            ], 200);
        }
        else
        {
            return response()->json('Merchant Not Found !', 400);
        }
    }

    public function loadAllCategories()
    {
        $tenant = null;

        $explode = explode('.', $_SERVER['HTTP_HOST']);

        if ($explode != null) {
            $subdomain = array_shift($explode);

            if($subdomain !== env('SUBDOMAIN'))
            {
                $tenant = Tenant::where('subdomain', $subdomain)->first();
            }
            else
            {
                $tenant = Tenant::where('domain', $_SERVER['HTTP_HOST'])->first();
            }
        }
        else
        {
            $tenant = Tenant::where('domain', $_SERVER['HTTP_HOST'])->first();
        }

        if($tenant != null)
        {
            $categories = $tenant->categories;

            return response()->json($categories, 200);
        }
        else
        {
            return response()->json('Merchant Not Found !', 400);
        }

    }

    public function loadAllCoursesByPrices()
    {
        $itemPerPage = 3;
        $currentPage = 1;

        if(request()->has('page'))
        {
            $currentPage = request()->page;
        }

        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });

        $tenant = null;

        $explode = explode('.', $_SERVER['HTTP_HOST']);

        if ($explode != null) {
            $subdomain = array_shift($explode);

            if($subdomain !== env('SUBDOMAIN'))
            {
                $tenant = Tenant::where('subdomain', $subdomain)->first();
            }
            else
            {
                $tenant = Tenant::where('domain', $_SERVER['HTTP_HOST'])->first();
            }

        }
        else
        {
            $tenant = Tenant::where('domain', $_SERVER['HTTP_HOST'])->first();
        }

        if($tenant != null)
        {
            $courses = $tenant->courses()->orderBy('price', 'desc')->paginate($itemPerPage);

            return view('social-learnings.course-front-end.child.child-all-course')
                ->with('filter', 'price')
                ->withCourses($courses);
        }
        else
        {
            return response()->json('Merchant Not Found !', 400);
        }
    }

    public function loadAllCoursesByPopular()
    {
        $itemPerPage = 3;
        $currentPage = 1;

        if(request()->has('page'))
        {
            $currentPage = request()->page;
        }

        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });

        $tenant = null;

        $explode = explode('.', $_SERVER['HTTP_HOST']);

        if ($explode != null) {
            $subdomain = array_shift($explode);

            if($subdomain !== env('SUBDOMAIN'))
            {
                $tenant = Tenant::where('subdomain', $subdomain)->first();
            }
            else
            {
                $tenant = Tenant::where('domain', $_SERVER['HTTP_HOST'])->first();
            }

        }
        else
        {
            $tenant = Tenant::where('domain', $_SERVER['HTTP_HOST'])->first();
        }

        if($tenant != null)
        {
            // $courses = $tenant->courses()->sortByDesc(function($course){
            //     return $course->users()->count();
            // })->paginate(3);

            $courses = $tenant->courses->sortByDesc(function($course){
                return $course->users()->count();
            })->forPage($currentPage, $itemPerPage);

            // return $courses;

            return view('social-learnings.course-front-end.child.child-all-course')
                ->with('filter', 'popular')
                ->with('currentPage', $currentPage)
                ->withCourses($courses);
        }
        else
        {
            return response()->json('Merchant Not Found !', 400);
        }

        return response()->json('Oops somethings when wrong!', 500);
    }

    public function loadSubCategory($id)
    {
        $tenant = null;

        $explode = explode('.', $_SERVER['HTTP_HOST']);

        if ($explode != null) {
            $subdomain = array_shift($explode);

            if($subdomain !== env('SUBDOMAIN'))
            {
                $tenant = Tenant::where('subdomain', $subdomain)->first();
            }
            else
            {
                $tenant = Tenant::where('domain', $_SERVER['HTTP_HOST'])->first();
            }

        }
        else
        {
            $tenant = Tenant::where('domain', $_SERVER['HTTP_HOST'])->first();
        }

        if($tenant != null)
        {
            $sub_categories = SubCategory::where(['category_id' => $id])->get();

            return response()->json($sub_categories, 200);
        }
        else
        {
            return response()->json('Merchant Not Found !', 400);
        }

        return response()->json('Oops somethings when wrong!', 500);
    }

    public function loadCategoryCourses($id)
    {
        $itemPerPage = 3;
        $currentPage = 1;

        if(request()->has('page'))
        {
            $currentPage = request()->page;
        }

        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });

        $tenant = null;

        $explode = explode('.', $_SERVER['HTTP_HOST']);

        if ($explode != null) {
            $subdomain = array_shift($explode);

            if($subdomain !== env('SUBDOMAIN'))
            {
                $tenant = Tenant::where('subdomain', $subdomain)->first();
            }
            else
            {
                $tenant = Tenant::where('domain', $_SERVER['HTTP_HOST'])->first();
            }

        }
        else
        {
            $tenant = Tenant::where('domain', $_SERVER['HTTP_HOST'])->first();
        }

        if($tenant != null)
        {
            $courses = $tenant->courses()->where('category_id', $id)->paginate($itemPerPage);

            return view('social-learnings.course-front-end.child.child-all-course')
                ->with('filter', 'category')
                ->withCourses($courses);
        }
        else
        {
            return response()->json('Merchant Not Found !', 400);
        }
    }

    public function addCourseToCart(Request $request, $course_id)
    {

    }

    public function loadAllCarts()
    {
        $itemPerPage = 3;
        $data = [];

        if(request()->has('carts'))
        {
            $data = request()->carts;
        }

        // return $data;


        $tenant = null;

        $explode = explode('.', $_SERVER['HTTP_HOST']);

        if ($explode != null) {
            $subdomain = array_shift($explode);

            if($subdomain !== env('SUBDOMAIN'))
            {
                $tenant = Tenant::where('subdomain', $subdomain)->first();
            }
            else
            {
                $tenant = Tenant::where('domain', $_SERVER['HTTP_HOST'])->first();
            }

        }
        else
        {
            $tenant = Tenant::where('domain', $_SERVER['HTTP_HOST'])->first();
        }

        if($tenant != null)
        {
            $courses = CoursePackage::whereIn('id', $data)->get();

            return response()->json([
                'view' => (string)view('social-learnings.course-front-end.child.cart')
                    ->withCarts($courses)
            ], 200);
        }
        else
        {
            return response()->json('Merchant Not Found !', 400);
        }

        return response()->json('Oops somethings when wrong!', 500);
    }
}
