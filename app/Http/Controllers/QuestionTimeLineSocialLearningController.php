<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\QuestionTimeLineSocialLearning;
use App\Comment;
use App\CommentLike;
use App\CommentDislike;
use App\PostLike;
use App\PostDislike;
use App\User;

class QuestionTimeLineSocialLearningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = QuestionTimeLineSocialLearning::latest()->get()->sortByDesc("id");

        return view('social-learnings.posts.index')->withPosts($posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($domain, Request $request)
    {
        $post = new QuestionTimeLineSocialLearning;

        $post->content = $request->content;
        $post->user_id = Auth::id();
        $post->tenant_id = Auth::user()->tenant->id;
        $post->save();

        return view('social-learnings.posts.child.post-child')->withPost($post);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($domain, $id)
    {
        $user = User::where(['username' => $id])->first();
        return view('social-learnings.profile.show')->withUser($user);
    }

    /**register
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($domain, Request $request, $id)
    {
        $req = json_decode($request->getContent());

        $post = QuestionTimeLineSocialLearning::find($id);

        $post->content = $req->content;
        $post->user_id = Auth::id();
        $post->tenant_id = Auth::user()->tenant->id;
        $post->save();

        return view('social-learnings.posts.child.post-child')->withPost($post);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($domain, $id)
    {
        $post = QuestionTimeLineSocialLearning::find($id);

        $post->likes()->delete();
        $post->dislikes()->delete();
        $post->postComments()->delete();
        $post->delete();

        return response()->json(['message' => 'Success'], 200);
    }

    public function like(Request $request, $id)
    {
        $post = QuestionTimeLineSocialLearning::find($id);

        $like = PostLike::where([
            'post_id' => $post->id,
            'user_id' => Auth::id()
        ])->first();

        $dislike = PostDislike::where([
            'post_id' => $post->id,
            'user_id' => Auth::id()
        ])->first();

        if ($like != null) {
            $like->delete();

        } else {
            $post->likes()->save(new PostLike([
                'post_id' => $post->id,
                'user_id' => Auth::id()
            ]));

        }

        if ($dislike != null) {
            $dislike->delete();
        }

        return view('social-learnings.posts.child.post-child')->withPost($post);
    }

    public function dislike(Request $request, $id)
    {
        $post = QuestionTimeLineSocialLearning::find($id);

        $like = PostLike::where([
            'post_id' => $post->id,
            'user_id' => Auth::id()
        ])->first();

        $dislike = PostDislike::where([
            'post_id' => $post->id,
            'user_id' => Auth::id()
        ])->first();

        if ($dislike != null) {
            $dislike->delete();

        } else {
            $post->likes()->save(new PostDislike([
                'post_id' => $post->id,
                'user_id' => Auth::id()
            ]));

        }

        if ($like != null) {
            $like->delete();
        }

        return view('social-learnings.posts.child.post-child')->withPost($post);
    }

    public function postComment(Request $request, $id)
    {
        $req = json_decode($request->getContent());

        $post = PostSocialLearning::find($id);

        $post->postComments()->save(new Comment([
            'question_id' => 0,
            'post_id' => $post->id,
            'user_id' => Auth::id(),
            'body' => $req->content,
        ]));
        return view('social-learnings.posts.child.post-child')->withPost($post);
    }

    public function likeComment($id)
    {
        $comment = Comment::find($id);

        $like = CommentLike::where([
            'comment_id' => $comment->id,
            'user_id' => Auth::id()
        ])->first();

        $dislike = CommentDislike::where([
            'comment_id' => $comment->id,
            'user_id' => Auth::id()
        ])->first();

        if ($like != null) {
            $like->delete();

        } else {
            $comment->likes()->save(new CommentLike([
                'comment_id' => $comment->id,
                'user_id' => Auth::id()
            ]));

        }

        if ($dislike != null) {
            $dislike->delete();
        }

        return view('social-learnings.posts.child.post-child')->withPost($comment->post);

    }

    public function dislikeComment($id)
    {
        $comment = Comment::find($id);

        $like = CommentLike::where([
            'comment_id' => $comment->id,
            'user_id' => Auth::id()
        ])->first();

        $dislike = CommentDislike::where([
            'comment_id' => $comment->id,
            'user_id' => Auth::id()
        ])->first();

        if ($dislike != null) {
            $dislike->delete();

        } else {
            $comment->likes()->save(new CommentDislike([
                'comment_id' => $comment->id,
                'user_id' => Auth::id()
            ]));

        }

        if ($like != null) {
            $like->delete();
        }

        return view('social-learnings.posts.child.post-child')->withPost($comment->post);
    }

}
