<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\MasterpieceSocialLearning;
use App\CommentMasterpiece;
use App\CommentMasterpieceLike;
use App\CommentMasterpieceDislike;
use App\PostLikeMasterpiece;
use App\PostDislikeMasterpiece;
use App\User;
use Illuminate\Support\Facades\Session;

class MasterpieceSocialLearningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $masters = MasterpieceSocialLearning::all();
        //
        // return view ('social-learnings.masterpiece.index')->withMasters($masters);

        $masters = MasterpieceSocialLearning::latest()->get()->sortByDesc("id");

        return view('social-learnings.masterpiece.index')->withMasters($masters);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $master = new MasterpieceSocialLearning;

        $master->content = $request->content;
        $master->user_id = Auth::id();
        $master->tenant_id = Auth::user()->tenant->id;
        $master->save();

        return view('social-learnings.masterpiece.child.post-child')->withMaster($master);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($domain, $id)
    {
        $user = User::where(['username' => $id])->first();
        return view('social-learnings.profile.show')->withUser($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($domain, Request $request, $id)
    {
        $req = json_decode($request->getContent());

        $master = MasterpieceSocialLearning::find($id);

        $master->content = $req->content;
        $master->user_id = Auth::id();
        $master->tenant_id = Auth::user()->tenant->id;
        $master->save();

        return view('social-learnings.masterpiece.child.post-child')->withMaster($master);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($domain, $id)
    {
        $master = MasterpieceSocialLearning::find($id);

        $master->likesMasterpieces()->delete();
        $master->dislikesMasterpieces()->delete();
        $master->postCommentsMasterpieces()->delete();
        $master->delete();

        return response()->json(['message' => 'Success'], 200);
    }


    public function like(Request $request, $id)
    {
        $master = MasterpieceSocialLearning::find($id);

        $like = PostLikeMasterpiece::where([
            'post_id' => $master->id,
            'user_id' => Auth::id()
        ])->first();

        $dislike = PostDislikeMasterpiece::where([
            'post_id' => $master->id,
            'user_id' => Auth::id()
        ])->first();

        if ($like != null) {
            $like->delete();

        } else {
            $master->likesMasterpieces()->save(new PostLikeMasterpiece([
                'post_id' => $master->id,
                'user_id' => Auth::id()
            ]));

        }

        if ($dislike != null) {
            $dislike->delete();
        }

        return view('social-learnings.masterpiece.child.post-child')->withMaster($master);
    }

    public function dislike(Request $request, $id)
    {
        $master = MasterpieceSocialLearning::find($id);

        $like = PostLikeMasterpiece::where([
            'post_id' => $master->id,
            'user_id' => Auth::id()
        ])->first();

        $dislike = PostDislikeMasterpiece::where([
            'post_id' => $master->id,
            'user_id' => Auth::id()
        ])->first();

        if ($dislike != null) {
            $dislike->delete();

        } else {
            $master->likesMasterpieces()->save(new PostDislikeMasterpiece([
                'post_id' => $master->id,
                'user_id' => Auth::id()
            ]));

        }

        if ($like != null) {
            $like->delete();
        }

        return view('social-learnings.masterpiece.child.post-child')->withMaster($master);
    }

    public function postComment(Request $request, $id)
    {
        $req = json_decode($request->getContent());

        $master = MasterpieceSocialLearning::find($id);

        $master->postCommentsMasterpieces()->save(new CommentMasterpiece([
            'question_id' => 0,
            'post_id' => $master->id,
            'user_id' => Auth::id(),
            'body' => $req->content,
        ]));
        return view('social-learnings.masterpiece.child.post-child')->withMaster($master);
    }

    public function likeComment($id)
    {
        $comment = CommentMasterpiece::find($id);

        $like = CommentMasterpieceLike::where([
            'comment_id' => $comment->id,
            'user_id' => Auth::id()
        ])->first();

        $dislike = CommentMasterpieceDislike::where([
            'comment_id' => $comment->id,
            'user_id' => Auth::id()
        ])->first();

        if ($like != null) {
            $like->delete();

        } else {
            $comment->likesMasterpieces()->save(new CommentMasterpieceLike([
                'comment_id' => $comment->id,
                'user_id' => Auth::id()
            ]));

        }

        if ($dislike != null) {
            $dislike->delete();
        }

        return view('social-learnings.masterpiece.child.post-child')->withMaster($comment->Masterpieces);

    }

    public function dislikeComment($id)
    {
        $comment = CommentMasterpiece::find($id);

        $like = CommentMasterpieceLike::where([
            'comment_id' => $comment->id,
            'user_id' => Auth::id()
        ])->first();

        $dislike = CommentMasterpieceDislike::where([
            'comment_id' => $comment->id,
            'user_id' => Auth::id()
        ])->first();

        if ($dislike != null) {
            $dislike->delete();

        } else {
            $comment->likesMasterpieces()->save(new CommentMasterpieceDislike([
                'comment_id' => $comment->id,
                'user_id' => Auth::id()
            ]));

        }

        if ($like != null) {
            $like->delete();
        }

        return view('social-learnings.masterpiece.child.post-child')->withMaster($comment->Masterpieces);
    }
}
