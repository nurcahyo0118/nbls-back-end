<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use Auth;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::where('user_id', Auth::id())->get();

        return view('pages.index')->withPages($pages);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->is_published ? 1 : 0;
        // return $request->all();

        $page = new Page;
        $page->name = $request->name;
        $page->content = $request->content;
        $page->subtitle = $request->subtitle;
        $page->url = $request->url;
        if((boolean)$request->is_private)
        {
            $page->is_private = true;
            $page->is_published = false;
        }
        else
        {
            $page->is_private = false;
            $page->is_published = true;
        }
        
        $page->user_id = Auth::id();

        $page->save();

        return redirect()->route('pages.index', $_SERVER['HTTP_HOST']);

        // return view('pages.child.child-page')->withPage($page);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('packet.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($domain, $id)
    {
        $page = Page::find($id);
        return view('pages.edit')->withPage($page);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($domain, Request $request, $id)
    {
        // return $request->all();

        $page = Page::find($id);
        $page->name = $request->name;
        $page->content = $request->content;
        $page->subtitle = $request->subtitle;
        $page->url = $request->url;
        
        if((boolean)$request->is_private)
        {
            $page->is_private = true;
            $page->is_published = false;
        }
        else
        {
            $page->is_private = false;
            $page->is_published = true;
        }

        $page->save();

        return redirect()->route('pages.index', $_SERVER['HTTP_HOST']);
        // return view('pages.child.child-page')->withPage($page);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($domain, $id)
    {
        Page::find($id)->delete();
    }

    public function showPage($url)
    {
        $page = Page::where([
            'url' => $url,
            'is_published' => 1
        ])->first();

        if ($page != null) {
            return view('social-learnings.pages.index')->withPage($page);
        } else {
            return redirect()->route('notfound');
        }

        return redirect()->back();

    }
}
