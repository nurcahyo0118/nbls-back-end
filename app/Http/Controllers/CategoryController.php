<?php

namespace App\Http\Controllers;

use App\Category;
use App\SubCategory;
use App\Tenant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use Auth;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($domain)
    {
        $tenant = Auth::user()->tenant;

        $categories = Category::where(['tenant_id' => $tenant->id])->paginate(5);

        $sub_categories = SubCategory::all();

        return view('categories.index')
            ->withCategories($categories)
            ->withSubCategories($sub_categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($domain, Request $request)
    {
        $tenant = Auth::user()->tenant;
        
        $this->validate($request, [
            'name' => 'required|max:50'
        ]);

        $category = Category::where('name', $request->name)
            ->where('tenant_id', $tenant->id)
            ->first();

        if($category == null)
        {
            $category = new Category;
            $category->name = $request->name;
            $category->description = $request->description;
            $category->tenant_id = $tenant->id;

            $category->save();

    //        add success message with session flash (hanya untuk 1 page)
            Session::flash('success', 'Kategori berhasil di tambahkan !');

    //        refresh the page
            return redirect()->back();
        } else {
    //        add error message with session flash (hanya untuk 1 page)
            Session::flash('failed', 'Kategori gagal di tambahkan !, nama sudah diambil');

    //        refresh the page
            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($domain, Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|unique:categories|max:50'
        ]);

        $category = Category::find($id);
        $category->name = $request->name;
        $category->description = $request->description;

        $category->save();

//        add success message with session flash (hanya untuk 1 page)
        // Session::flash('success', 'Kategori berhasil di edit !');

//        refresh the page
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($domain, $id)
    {
        $category = Category::find($id);

        $category->subcategories()->delete();
        $category->delete();

        return redirect()->back();
    }

    public function loadSubCategory($domain, $id)
    {
        $sub_categories = SubCategory::where(['category_id' => $id])->get();

        return response()->json($sub_categories, 200);
    }
}
