<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ItimeSocialLearning;
use Auth;

class ITimeController extends Controller
{
    public function index()
    {
        $students = [];
        $courses = Auth::user()->mycourses;

        foreach ($courses as $course) {
            foreach ($course->users as $user) {
                array_push($students, $user->id);
            }
        }

        $i_times = ItimeSocialLearning::whereIn('user_id', $students)->get();

        return view('i-times.index')->withITimes($i_times);
    }

    public function show($domain, $id)
    {
        $i_time = ItimeSocialLearning::find($id);
        return view('i-times.show')->withITime($i_time);
    }
}
