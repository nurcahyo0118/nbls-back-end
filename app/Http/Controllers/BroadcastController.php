<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Broadcast;

class BroadcastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return Auth::user()->tenant->users;
        // $users = [];
        // foreach(Auth::user()->tenant->users as $user)
        // {
        //     array_push($users, $user->id);
        // }

        // return view('broadcast.index')->withSalesWidgets(SalesWidgetSocialLearning::whereIn('user_id', $users)->get());
       
        $broadcast = Broadcast::all();
        return view('broadcast.index')->withBroadcast($broadcast);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('broadcast.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($domain, Request $request)
    {
        $broadcast = new Broadcast;
        $broadcast->tenant_id = $request->tenant_id;
        $broadcast->user_id = $request->user_id;
        $broadcast->sender = $request->sender;
        $broadcast->message = $request->message;
        $broadcast->viewer = $request->viewer;
        $broadcast->view_date = $request->view_date;
        $broadcast->close_date = $request->close_date;
        $broadcast->save();

        return redirect()->route('broadcast.index', $_SERVER['HTTP_HOST']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($domain, $id)
    {
        $broadcast = Broadcast::find($id);
        return view('broadcast.edit')->withBroadcast($broadcast);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($domain, Request $request, $id)
    {
        $broadcast = Broadcast::find($id);
        $broadcast->sender = $request->sender;
        $broadcast->message = $request->message;
        $broadcast->viewer = $request->viewer;
        $broadcast->view_date = $request->view_date;
        $broadcast->close_date = $request->close_date;
        $broadcast->save();

        return redirect()->route('broadcast.index', $_SERVER['HTTP_HOST']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($domain, $id)
    {
        Broadcast::find($id)->delete();
    }
}
