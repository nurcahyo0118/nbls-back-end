<?php

namespace App\Http\Controllers;
use Auth;
use Session;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Image;

class SettingSocialLearningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('social-learnings.setting.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return $request->all();
        $this->validate($request, [
            'password' => 'confirmed'            
        ]);

        $user = Auth::user();

        $user->fullname = $request->fullname;
        
        $user->username = $request->username;

        if($request->old_password != null)
        {
            if(Hash::check($request->old_password, $user->password))
            {
                if($request->password != null)
                {
                    $user->password = Hash::make($request->password);
                } else {
                    Session::flash('failed', 'Password tidak boleh kosong !');
                    return redirect()->back();
                }
                
            } else {
                Session::flash('failed', 'Password lama salah !');

                return redirect()->back();
            }
            
        }
        
        $user->ktp_id = $request->ktp_id;
        $user->birth_date = $request->birth_date;
        $user->sex = $request->sex;
        $user->phone = $request->phone;
        $user->mobile = $request->mobile;
        $user->city = $request->city;
        $user->notes = $request->notes;
        $user->pin_bb = $request->pin_bb;
        $user->fb = $request->fb;
        $user->tw = $request->tw;
        $user->ig = $request->ig;
        $user->about = $request->about;
        $user->hobby = $request->hobby;
        $user->skill = $request->skill;
        $user->hope = $request->hope;
        $user->website = $request->website;
        $user->address = $request->address;
        $user->zipcode = $request->zipcode;
        $user->hobby = $request->hobby;
        $user->reason = $request->reason;
        $user->is_partnership = $request->is_partnership != null ? true : false;

        if ($request->hasFile('photo')) {
            if ($user->photo != null) {
                if (file_exists('images/photo/' . $user->photo)) {
                    unlink(public_path('images/photo/' . $user->photo));
                }
            }
            $image = $request->file('photo');
            $filename = time() . '.' . $image->getClientOriginalName();
            $path = public_path('images/photo/' . $filename);
            Image::make($image->getRealPath())->save($path);
            $user->photo = $filename;

        }

        $user->save();

        Session::flash('success', 'Berhasil memperbarui profile !');

        return redirect()->route('social-learning-setting.index', $_SERVER['HTTP_HOST']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
