<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Auth;
use App\User;

class DashboardController extends Controller
{

    public function __construct()
    {
        return $this->middleware('web');
    }

    public function dashboard()
    {
      $tenant = Auth::user()->tenant;
      $users = User::where('tenant_id', $tenant->id)->get();
      return view('dashboard.index')
          ->withUsers($users);
    }

    public function noPermission()
    {
        return redirect()->back();
    }

    /* Tenant */
    public function tenantDashboard($subdomain)
    {

        return view('layouts.main');
    }
    /* End Tenant */
}
