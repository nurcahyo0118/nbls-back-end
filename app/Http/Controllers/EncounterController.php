<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Encounter;
use App\Course;
use App\Criteria;
use App\User;

use DB;
use Auth;
use Session;
use Illuminate\Support\Facades\Storage;

class EncounterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $encounters = Encounter::where(['user_id' => Auth::id()])->get();

        return view('encounters.index')
            ->withEncounters($encounters);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $courses = Course::where(['author_id' => Auth::id()])->get();

        return view('encounters.create')
            ->withCourses($courses);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $base_encounter = Encounter::where('course_id', $request->course_id)->first();

        if($base_encounter == null)
        {
            $encounter = new Encounter;
            $encounter->name = $request->name;
            $encounter->is_active = $request->is_active;

            if ($request->hasFile('file')) {

                if ($encounter->file != null) {
                    if (file_exists('encounter-file/' . $encounter->file)) {
                        // unlink(public_path('encounter-file/' . $encounter->file));
                    }
                }

                $file = $request->file('file');
                $filename = time() . '.' . $file->getClientOriginalName();
                $path = public_path('encounter-file/');
                $file->move($path, $filename);
                
                $encounter->file = $filename;

            }

            $encounter->course_id = $request->course_id;
            $encounter->user_id = Auth::id();

            $encounter->save();

            Session::flash('success', 'Berhasil menambahkan ujian !');
        } else {
            Session::flash('failed', 'Gagal menambahkan ujian !, data ujian sudah ada');
        }

        return redirect()->route('encounters.index', $_SERVER['HTTP_HOST']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($domain, $id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($domain, $id)
    {
        $encounter = Encounter::find($id);
        $courses = Course::where(['author_id' => Auth::id()])->get();
        return view('encounters.edit')
            ->withEncounter($encounter)
            ->withCourses($courses);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($domain, Request $request, $id)
    {
        // return redirect()->route('encounters.index', $_SERVER['HTTP_HOST']);
        $encounter = Encounter::find($id);
        $encounter->name = $request->name;
        $encounter->is_active = $request->is_active;

        if ($request->hasFile('file')) {

            if ($encounter->file != null) {
                if (file_exists('encounter-file/' . $encounter->file)) {
                    // unlink(public_path('encounter-file/' . $encounter->file));
                }
            }

            $file = $request->file('file');
            $filename = time() . '.' . $file->getClientOriginalName();
            $path = public_path('encounter-file/');
            $file->move($path, $filename);            
            $encounter->file = $filename;

        }

        $encounter->course_id = $request->course_id;
        $encounter->user_id = Auth::id();

        $encounter->save();

        return redirect()->route('encounters.index', $_SERVER['HTTP_HOST']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($domain, $id)
    {
        Encounter::find($id)->delete();
    }

    public function instructorEncounter()
    {
        $encounters = [];
        $courses = Auth::user()->mycourses;

        foreach ($courses as $course) {
            foreach ($course->encounters as $encounter) {
                array_push($encounters, $encounter);
            }

        }

        // return $encounters;
        return view('instructor.encounters')->withEncounters($encounters);
    }

    public function answers($domain, $id)
    {
        $encounter = Encounter::find($id);

        // return $encounter;
        return view('instructor.encounter.index')->withEncounter($encounter);
    }

    // public function criteriaList($domain, $id)
    // {
    //     $encounter = Encounter::find($id);

    //     return view('encounters.criteria')
    //         ->withEncounter($encounter);
    // }

    public function getCriterias($domain, $encounter_id)
    {
        $criterias = Criteria::where('encounter_id', $encounter_id)->get();
        $encounter = Encounter::find($encounter_id);

        return view('encounters.criterias.index')
            ->withCriterias($criterias)
            ->withEncounter($encounter);
    }

    public function storeCriteria($domain, $encounter_id, Request $request)
    {
        $encounter = Encounter::find($encounter_id);
        
        if($encounter != null)
        {
            $criteria = new Criteria;

            $criteria->content = $request->content;
            $criteria->weight = $encounter->criterias->count() === 0 ? 100 : 0;
            $criteria->encounter_id = $encounter_id;

            $criteria->save();

            Session::flash('success', 'Berhasil menambah kriteria !');
        }
        else
        {
            Session::flash('failed', 'Oops somethings when wrong !');
        }

        return redirect()->back();
    }

    public function updateCriteria($domain, $encounter_id, $criteria_id, Request $request)
    {
        // dd($request->all());
        $encounter = Encounter::find($encounter_id);
        
        if($encounter != null)
        {
            $criteria = Criteria::find($criteria_id);

            $criteria->content = $request->content;

            $criteria->save();

            Session::flash('success', 'Berhasil mengupdate kriteria !');
        }
        else
        {
            Session::flash('failed', 'Oops somethings when wrong !');
        }

        return redirect()->back();
    }

    public function updateCriteriaWeight($domain, $encounter_id, Request $request)
    {
        // dd($request->weight[2]);
        
        $encounter = Encounter::find($encounter_id);
        $encounter_criteria_percentage = 0;

        // dd($encounter->criterias);
        
        if($encounter != null)
        {
            foreach($encounter->criterias as $key => $c)
            {
                $encounter_criteria_percentage += $request->weight[$key];
            }

            if($encounter_criteria_percentage === 100)
            {
                foreach($encounter->criterias as $index => $criteria)
                {
                    $criteria->weight = $request->weight[$index];

                    $criteria->save();

                }

                Session::flash('success', 'Berhasil merubah bobot kriteria !');
            }
            else
            {
                Session::flash('failed', 'Total bobot harus 100 !');
            }

        }
        else
        {
            Session::flash('failed', 'Oops somethings when wrong !');
        }

        return redirect()->back();
    }

    public function deleteCriteria($domain, $encounter_id, $criteria_id)
    {
        $criteria = Criteria::find($criteria_id);
        
        $encounter = Encounter::find($encounter_id);

        $criteria->delete();

        if($encounter != null)
        {
            $assessments = DB::table('encounter_assessments')
                ->where('encounter_id', $encounter->id)
                ->where('criteria_id', $criteria_id)
                ->delete();

            $first_criteria = $encounter->criterias->first();
            $first_criteria->weight += $criteria->weight;
            $first_criteria->save();

            Session::flash('success', 'Berhasil menghapus kriteria !');
        }

        return redirect()->back();
    }

    // Assessments

    public function getAssessments($domain, $encounter_id)
    {
        $encounter = Encounter::find($encounter_id);

        if($encounter != null)
        {
            return view('encounters.assessments.index')
                ->withAssessments($encounter->submittedEncounters)
                ->withEncounter($encounter);
        }
        
        return redirect()->back();
        
    }

    public function storeAssessment($domain, $encounter_id, $user_id, Request $request)
    {
        $encounter = Encounter::find($encounter_id);
        $criterias = Criteria::where('encounter_id', $encounter_id)->get();
        
        if($encounter != null && $criterias != null)
        {

            foreach($criterias as $index => $criteria)
            {
                $is_assessment_exist = DB::table('encounter_assessments')
                    ->where('encounter_id', $encounter_id)
                    ->where('criteria_id', $criteria->id)
                    ->where('user_id', $user_id);
                
                if($is_assessment_exist->first() === null)
                {
                    DB::table('encounter_assessments')->insert([
                        'encounter_id' => $encounter_id,
                        'criteria_id' => $criteria->id,
                        'user_id' => $user_id,
                        'value' => $request->value[$index]
                    ]);
                }
                else
                {
                    $is_assessment_exist->update(['value' => $request->value[$index]]);
                }
                
            }

            Session::flash('success', 'Berhasil menilai !');
        }
        else
        {
            Session::flash('failed', 'Oops somethings when wrong !');
        }

        return redirect()->back();
    }
}