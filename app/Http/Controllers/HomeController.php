<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tenant;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('web');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('layouts.frontend');
    }

    public function portal()
    {
        // \Illuminate\Support\Facades\Password::sendResetLink(['email' => 'lawlawliet98@gmail.com']);
        return view('social-learnings.portal.index');
    }

    public function howItWorks()
    {
        return view('social-learnings.how-it-works.index');
    }

    public function notfound()
    {
        return view('notfound');
    }

    public function register($domain)
    {
        $tenant = Tenant::where(['domain' => $domain])->first();

        return view('auth.register')->withTenant($tenant);
    }

    public function tenantRegister($subdomain)
    {
        $tenant = Tenant::where(['subdomain' => $subdomain])->first();

        return view('auth.register')->withTenant($tenant);
    }

    public function payments()
    {
        return view('payment-example');
    }
}
