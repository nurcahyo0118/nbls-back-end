<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ItimeSocialLearning;

use Auth;

class ItimeSocialLearningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $itime = new ItimeSocialLearning;
        $itime->instructor_explanation = $request->instructor_explanation;
        $itime->instructor_response = $request->instructor_response;
        $itime->best_instructor_id = $request->best_instructor;
        $itime->worst_instructor_id = $request->worst_instructor;
        $itime->instructor_suggestion = $request->instructor_suggestion;
        $itime->course_explanation = $request->course_explanation;
        $itime->best_course_id = $request->best_course;
        $itime->worst_course_id = $request->worst_course;
        $itime->course_suggestion = $request->course_suggestion;
        $itime->computer_help = $request->computer_help;
        $itime->ac_comfortable = $request->ac_comfortable;
        $itime->class_cleanliness = $request->class_cleanliness;
        $itime->facility_help = $request->facility_help;
        $itime->facility_suggestion = $request->facility_suggestion;
        $itime->login_problem = $request->login_problem;
        $itime->learning_system_suggestion = $request->learning_system_suggestion;
        $itime->user_id = Auth::id();

        $itime->save();

        return response()->json($itime, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
