<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\NotificationSocialLearning;

use Auth;

class NotificationSocialLearningController extends Controller
{
    public function postNotification(Request $request)
    {
        $notification = new NotificationSocialLearning;
        $notification->title = $request->title;
        $notification->content = $request->content;
        $notification->user_id = Auth::id();

        $notification->save();
    }
}
