<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Post;

use Illuminate\Support\Facades\Session;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = Post::all()->sortByDesc("id");

        return view('post.index')->withPost($post);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($domain, Request $request)
    {
        // $tenant = Tenant::where(['domain' => $domain])->first();

        $post = new Post;
        $post->status = $request->status;
        $post->save();
        Session::flash('success', 'Kategori berhasil di tambahkan !');
        //        refresh the page
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($domain, Request $request, $id)
    {
        // $this->validate($request, [
        //     'name' => 'required|unique:categories|max:50'
        // ]);

        $post = Post::find($id);
        $post->status = $request->status;

        $post->save();

//        add success message with session flash (hanya untuk 1 page)
        Session::flash('success', 'Kategori berhasil di edit !');

//        refresh the page
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($domain, $id)
    {
        $post = Post::find($id);

        $post->delete();

        return redirect()->back();
    }
}
