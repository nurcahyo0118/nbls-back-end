<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BackendClass;

use Auth;

class BackendClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $classes = BackendClass::all();
        return view('classes.index')->withClasses($classes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $courses = Auth::user()->mycourses;

        return view('classes.create')
            ->withCourses($courses);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $class = new BackendClass;
        $class->name = $request->name;
        $class->order_list = $request->order_list;

        if ($request->hasFile('file')) {

            if ($class->file != null) {
                if (file_exists('class-file/' . $class->file)) {
                    unlink(public_path('class-file/' . $class->file));
                }
            }

            $file = $request->file('file');
            $filename = time() . '.' . $file->getClientOriginalName();
            $path = public_path('class-file/' . $filename);
            $file->move($path);
            $class->file = $filename;

        }

        $class->course_id = $request->course_id;
        $class->user_id = Auth::id();

        $class->save();

        return redirect()->route('classes.index', $_SERVER['HTTP_HOST']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($domain, $id)
    {
        $class = BackendClass::find($id);
        $courses = Auth::user()->mycourses;

        return view('classes.edit')
            ->withClass($class)
            ->withCourses($courses);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($domain, Request $request, $id)
    {
        $class = BackendClass::find($id);
        $class->name = $request->name;
        $class->order_list = $request->order_list;

        if ($request->hasFile('file')) {

            if ($class->file != null) {
                if (file_exists('class-file/' . $class->file)) {
                    unlink(public_path('class-file/' . $class->file));
                }
            }

            $file = $request->file('file');
            $filename = time() . '.' . $file->getClientOriginalName();
            $path = public_path('class-file/' . $filename);
            $file->move($path);
            $class->file = $filename;

        }

        $class->course_id = $request->course_id;
        $class->user_id = Auth::id();

        $class->save();

        return redirect()->route('classes.index', $_SERVER['HTTP_HOST']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($domain, $id)
    {
        BackendClass::find($id)->delete();
    }
}
