<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Course;

use Auth;

class PerformanceController extends Controller
{
    public function index()
    {
        return view('performance.index');
    }

    public function show($domain, $id)
    {
        $course = Course::find($id);
        $students = [];
        $courses = Auth::user()->mycourses;

        // foreach ($courses as $course) {
            foreach ($course->users as $user) {
                array_push($students, $user);
            }
        // }

        return view('performance.show')
            ->withStudents($students)
            ->withCourse($course);
    }
}
