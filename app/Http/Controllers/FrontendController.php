<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class FrontendController extends Controller
{
    public function index()
    {
        return view('post.index');
    }
    
    public function coba(){
        Mail::send(['text'=>'mail'],['name','Edwin'], function($message){
            $message->to('mr.edwinsetiawan@gmail.com', 'To Edwin')->subject('Test Email');
            $message->from('mr.edwinsetiawan@gmail.com', 'Edwin');
        });
    }
}
