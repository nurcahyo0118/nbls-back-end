<?php

namespace App\Http\Controllers;

use App\Section;
use App\Material;
use App\Quiz;
use Illuminate\Http\Request;
use App\Http\Controllers\CourseController;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7;

use Log;

class SectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($domain)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($domain)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($domain, Request $request)
    {
        $req = json_decode($request->getContent());

        $sections = Section::where(['course_id' => $req->course_id])->get();

        $section = new Section;
        $section->title = $req->title;
        $section->description = $req->description;
        $section->line = $sections->count() + 1;
        $section->course_id = (int)$req->course_id;
        $section->wistia_project_id = $req->wistia_hashed_id;

        $section->save();

        return view('courses.childsection')->withSection($section);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($domain, Request $request, $id)
    {

        return $request->all();
        $section = Section::find($id);
        $section->title = $request->title;
        $section->description = $request->description;

        $section->save();

        return response()->json([
            'section_id' => $section->id,
            'view' => (string)view('courses.childsection')->withSection($section)
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($domain, $id)
    {
        $http = new Client;

        $section = Section::find($id);

        $materials = $section->materials;

        foreach ($materials as $material) {

            $response = $http->request('DELETE', 'https://api.wistia.com/v1/medias/' . $material->wistia_media_hashed_id . '.json', [
                'form_params' => [
                    'api_password' => env('WISTIA_TOKEN')
                ]
            ]);

            if ($response->getStatusCode() === 200) {
                $material->delete();
                Quiz::where(['material_id' => $material->id])->delete();
                $section->delete();
            } else {
                return response()->json(['message' => 'Failed delete media from wistia'], 404);
            }

        }

    }

    public function deleteAll($domain, $id)
    {

        $sections = Section::where(['course_id' => $id])->get();

        foreach ($sections as $section) {

            $http = new Client;

            if ($section->materials->count() > 0) {

                // Loop
                foreach ($section->materials as $material) {

                    try {
                        $response = $http->request('DELETE', 'https://api.wistia.com/v1/medias/' . $material->wistia_media_hashed_id . '.json', [
                            'form_params' => [
                                'api_password' => env('WISTIA_TOKEN')
                            ]
                        ]);

                        if ($response->getStatusCode() === 200) {

                            Log::debug('Success Delete Wistia Media');
                            $material->delete();
                            Quiz::where(['material_id' => $material->id])->delete();
                            $section->delete();
                        }

                    } catch (ClientException $exception) {

                        Log::debug($exception->getResponse()->getStatusCode());
                        if ($exception->getResponse()->getStatusCode() === 404) {
                            $section->delete();
                        }

                    }

                }

                // End Loop

            } else {
                $section->delete();

            }

        }

        return response()->json(['message' => 'Success delete all section'], 200);
    }
}
