<?php

namespace App\Http\Controllers;

use App\Category;
use App\SubCategory;
use Illuminate\Http\Request;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($domain, Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:50'
        ]);

        $category = Category::find($request->category_id);
        $sub_category = SubCategory::where([
            'name' => $request->name,
            'category_id' => $request->category_id
        ])->first();

        if($sub_category == null)
        {
            $sub_category = new SubCategory;
            $sub_category->name = $request->name;
            $sub_category->description = $request->description;
            $sub_category->category_id = $request->category_id;

            $sub_category->save();

            return response()->json($sub_category, 200);
        } else {
            // Data not found
            return response()->json('The data you are referring to is not found!', 400);
        }

        return response()->json('Oops something when wrong!', 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($domain, $id)
    {
        $sub_category = SubCategory::find($id);

        $sub_category->delete();

        return response()->json(null, 200);
    }
}
