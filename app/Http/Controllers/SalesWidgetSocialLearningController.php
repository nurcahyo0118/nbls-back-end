<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\SalesWidgetSocialLearning;

use Auth;
use Image;

class SalesWidgetSocialLearningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return Auth::user()->tenant->users;
        $users = [];
        foreach(Auth::user()->tenant->users as $user)
        {
            array_push($users, $user->id);
        }

        return view('sales-widget.index')->withSalesWidgets(SalesWidgetSocialLearning::whereIn('user_id', $users)->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sales-widget.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $widget = new SalesWidgetSocialLearning;
        $widget->is_button_only = $request->is_button_only != null;
        $widget->card_background_color = $request->card_background_color;
        $widget->button_background_color = $request->button_background_color;
        $widget->card_text_color = $request->card_text_color;
        $widget->button_text_color = $request->button_text_color;
        $widget->button_text = $request->button_text;
        $widget->course_id = $request->course_id;
        $widget->user_id = Auth::id();
        $widget->is_publish = $request->is_publish != null;

        // Image
        if ($request->hasFile('image')) {
            
            if ($widget->image != null) {
                if (file_exists('/sales-widget-images/' . $widget->image)) {
                    unlink(public_path('/sales-widget-images/' . $widget->image));
                }
            }
            
            $image = $request->file('image');
            $filename = time() . '.' . $image->getClientOriginalName();
            $path = public_path('/sales-widget-images/' . $filename);
            Image::make($image->getRealPath())->save($path);
            $widget->image = $filename;

        }

        $widget->save();

        return redirect()->route('social-learning-sales-widget.index', $_SERVER['HTTP_HOST']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($domain, $id)
    {
        $widget = SalesWidgetSocialLearning::find($id);
        return view('sales-widget.edit')->withWidget($widget);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($domain, Request $request, $id)
    {
        $widget = SalesWidgetSocialLearning::find($id);
        $widget->is_button_only = $request->is_button_only != null;
        $widget->card_background_color = $request->card_background_color;
        $widget->button_background_color = $request->button_background_color;
        $widget->card_text_color = $request->card_text_color;
        $widget->button_text_color = $request->button_text_color;
        $widget->button_text = $request->button_text;
        $widget->course_id = $request->course_id;
        $widget->user_id = Auth::id();
        $widget->is_publish = $request->is_publish != null;

        // Image
        if ($request->hasFile('image')) {
            
            if ($widget->image != null) {
                if (file_exists('/sales-widget-images/' . $widget->image)) {
                    unlink(public_path('/sales-widget-images/' . $widget->image));
                }
            }
            
            $image = $request->file('image');
            $filename = time() . '.' . $image->getClientOriginalName();
            $path = public_path('/sales-widget-images/' . $filename);
            Image::make($image->getRealPath())->save($path);
            $widget->image = $filename;

        }

        $widget->save();

        return redirect()->route('social-learning-sales-widget.index', $_SERVER['HTTP_HOST']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($domain, $id)
    {
        SalesWidgetSocialLearning::find($id)->delete();
    }

    public function publish($domain, $id)
    {
        $widget = SalesWidgetSocialLearning::find($id);
        $widget->is_publish = true;

        $widget->save();

        return redirect()->back();
    }

    public function unpublish($domain, $id)
    {
        $widget = SalesWidgetSocialLearning::find($id);
        $widget->is_publish = false;

        $widget->save();

        return redirect()->back();
    }
}
