<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Voucher;

use Auth;
use Session;

class VoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $current_user = Auth::user();
        $vouchers = Voucher::where([
            'tenant_id' => $current_user->tenant_id
        ])->get();

        return view('vouchers.index')->withVouchers($vouchers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vouchers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($domain, Request $request)
    {
        $this->validate($request, [
            'code' => 'required|max:10|unique:vouchers'
        ]);

        $current_user = Auth::user();

        $voucher = new Voucher;

        $voucher->code = $request->code;
        $voucher->discount = $request->discount;
        $voucher->expired_date = $request->expired_date;
        $voucher->user_id = $current_user->id;
        $voucher->tenant_id = $current_user->tenant_id;

        $voucher->save();

        Session::flash('success', 'Voucher berhasil di tambahkan !');

        return redirect()->route('vouchers.index', $_SERVER['HTTP_HOST']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($domain, $id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($domain, $id)
    {
        $voucher = Voucher::find($id);

        return view('vouchers.edit')->withVoucher($voucher);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($domain, Request $request, $id)
    {
        $this->validate($request, [
            'code' => 'required|max:10'
        ]);

        $current_user = Auth::user();

        $voucher = Voucher::find($id);

        $voucher->discount = $request->discount;
        $voucher->expired_date = $request->expired_date;

        $voucher->save();

        Session::flash('success', 'Voucher berhasil di perbarui !');

        return redirect()->route('vouchers.index', $_SERVER['HTTP_HOST']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($domain, $id)
    {
        $voucher = Voucher::find($id);

        $voucher->delete();

        return response()->json(null, 204);
    }

    public function check(Request $request)
    {
        // return $request->all();
        $current_user = Auth::user();

        $voucher = Voucher::where(['code' => $request->voucher_code])->first();

        if($voucher != null && $voucher->expired_date >= \Carbon\Carbon::now()->toDateString()) {
            $voucher->discount_price = $request->subtotal * ($voucher->discount/100);
            $voucher->total = $request->subtotal - $voucher->discount_price;
            return response()->json(
                (string)view('social-learnings.payments.child.section-discount')->withVoucher($voucher), 
                200
            );
        } else {
            return response()->json('Voucher code wrong or expired', 400);
        }

        return response()->json('Oops somethings when wrong !', 500);
    }
}
