<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\QuestionSocialLearning;
use App\Comment;
use App\QuestionLike;
use App\QuestionDislike;
use App\CommentLike;
use App\CommentDislike;
use App\NotificationSocialLearning;


use Auth;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function postComment(Request $request, $id)
    {
        $current_user = Auth::user();
        $question = QuestionSocialLearning::find($id);

        $question->comments()->save(new Comment([
            'question_id' => $question->id,
            'post_id' => 0,
            'user_id' => Auth::id(),
            'body' => $request->content,
        ]));

        $notification = new NotificationSocialLearning;
        $notification->title = $current_user->fullname . ' menjawab pertanyaan ' . $question->user->fullname;
        $notification->content = $current_user->fullname . ' menjawab pertanyaan ' . $question->user->fullname;
        $notification->user_id = $question->user_id;

        $notification->save();

        return view('social-learnings.course.child.question-child')
            ->withQuestion($question)
            ->withMaterial($question->material);
    }

    public function like(Request $request, $id)
    {
        $question = QuestionSocialLearning::find($id);

        $like = QuestionLike::where([
            'question_id' => $question->id,
            'user_id' => Auth::id()
        ])->first();

        $dislike = QuestionDislike::where([
            'question_id' => $question->id,
            'user_id' => Auth::id()
        ])->first();

        if ($like != null) {
            $like->delete();

        } else {
            $question->likes()->save(new QuestionLike([
                'question_id' => $question->id,
                'user_id' => Auth::id()
            ]));
        }

        if ($dislike != null) {
            $dislike->delete();
        }

        return view('social-learnings.course.child.question-child')
            ->withQuestion($question)
            ->withMaterial($question->material);;
    }

    public function dislike(Request $request, $id)
    {
        $question = QuestionSocialLearning::find($id);

        $like = QuestionLike::where([
            'question_id' => $question->id,
            'user_id' => Auth::id()
        ])->first();

        $dislike = QuestionDislike::where([
            'question_id' => $question->id,
            'user_id' => Auth::id()
        ])->first();

        if ($dislike != null) {
            $dislike->delete();

        } else {
            $question->likes()->save(new QuestionDislike([
                'question_id' => $question->id,
                'user_id' => Auth::id()
            ]));

        }

        if ($like != null) {
            $like->delete();
        }

        return view('social-learnings.course.child.question-child')
            ->withQuestion($question)
            ->withMaterial($question->material);;
    }

    public function questionComment(Request $request, $id)
    {
        $req = json_decode($request->getContent());

        $question = QuestionSocialLearning::find($id);

        $question->questionComments()->save(new Comment([
            'question_id' => 0,
            'question_id' => $question->id,
            'user_id' => Auth::id(),
            'body' => $req->content,
        ]));
        return view('social-learnings.course.child.question-child')
            ->withQuestion($question)
            ->withMaterial($question->material);;
    }

    public function likeComment($id)
    {
        $comment = Comment::find($id);

        $like = CommentLike::where([
            'comment_id' => $comment->id,
            'user_id' => Auth::id()
        ])->first();

        $dislike = CommentDislike::where([
            'comment_id' => $comment->id,
            'user_id' => Auth::id()
        ])->first();

        if ($like != null) {
            $like->delete();

        } else {
            $comment->likes()->save(new CommentLike([
                'comment_id' => $comment->id,
                'user_id' => Auth::id()
            ]));

        }

        if ($dislike != null) {
            $dislike->delete();
        }

        return view('social-learnings.course.child.question-child')
            ->withQuestion($comment->question)
            ->withMaterial($comment->question->material);

    }

    public function dislikeComment($id)
    {
        $comment = Comment::find($id);

        // return $comment;

        $like = CommentLike::where([
            'comment_id' => $comment->id,
            'user_id' => Auth::id()
        ])->first();

        $dislike = CommentDislike::where([
            'comment_id' => $comment->id,
            'user_id' => Auth::id()
        ])->first();

        // return $like;

        if ($dislike != null) {
            $dislike->delete();

        } else {
            $comment->likes()->save(new CommentDislike([
                'comment_id' => $comment->id,
                'user_id' => Auth::id()
            ]));

            // return $comment->likes;

        }

        if ($like != null) {
            $like->delete();
        }

        return view('social-learnings.course.child.question-child')
            ->withQuestion($comment->question)
            ->withMaterial($comment->question->material);
    }

    public function encounterVideoComments()
    {
        $comments = [];
        $courses = Auth::user()->mycourses;

        foreach ($courses as $course) {
            foreach ($course->sections as $section) {
                foreach ($section->materials as $material) {
                    foreach ($material->questions as $question) {
                        array_push($comments, $question);
                    }

                }

            }

        }

        return view('instructor.video-comments')->withComments($comments);
    }
}
