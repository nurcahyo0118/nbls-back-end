<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OrderHistorySocialLearning;

class FinanceController extends Controller
{
    public function index()
    {
        $finances = OrderHistorySocialLearning::all();

        return view('finances.index')->withFinances($finances);
    }
}
