<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\EssaySocialLearning;
use App\EssayAnswerSocialLearning;
use App\EssayAnswerLike;
use App\EssayAnswerDislike;
use App\EssayAnswerComment;

use Auth;

class EssaySocialLearningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $essay = new EssaySocialLearning;
        $essay->title = $request->title;
        $essay->essay = $request->essay;
        $essay->section_id = $request->section_id;

        $essay->save();

        return (string)view('courses.child-essay')->withEssay($essay);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($domain, $id)
    {
        $essay = EssaySocialLearning::find($id);
        $essay->delete();
    }

    public function postAnswer($domain, Request $request, $id)
    {
        $answer = new EssayAnswerSocialLearning;
        $answer->essay_id = $id;
        $answer->user_id = Auth::id();
        $answer->body= $request->body;

        $answer->save();

        return redirect()->back();
    }

    public function like($domain, Request $request, $id)
    {
        $answer = EssayAnswerSocialLearning::find($id);

        $like = EssayAnswerLike::where([
            'essay_answer_id' => $answer->id,
            'user_id' => Auth::id()
        ])->first();

        $dislike = EssayAnswerDislike::where([
            'essay_answer_id' => $answer->id,
            'user_id' => Auth::id()
        ])->first();

        if ($like != null) {
            $like->delete();

        } else {
            $answer->likes()->save(new EssayAnswerLike([
                'essay_answer_id' => $answer->id,
                'user_id' => Auth::id()
            ]));
        }

        if ($dislike != null) {
            $dislike->delete();
        }

        return view('social-learnings.course.essay.answer-child')->withAnswer($answer);
    }

    public function dislike($domain, Request $request, $id)
    {
        $answer = EssayAnswerSocialLearning::find($id);

        $like = EssayAnswerLike::where([
            'essay_answer_id' => $answer->id,
            'user_id' => Auth::id()
        ])->first();

        $dislike = EssayAnswerDislike::where([
            'essay_answer_id' => $answer->id,
            'user_id' => Auth::id()
        ])->first();

        if ($dislike != null) {
            $dislike->delete();

        } else {
            $answer->likes()->save(new EssayAnswerDislike([
                'essay_answer_id' => $answer->id,
                'user_id' => Auth::id()
            ]));

        }

        if ($like != null) {
            $like->delete();
        }

        return view('social-learnings.course.essay.answer-child')->withAnswer($answer);
    }

    public function comment($domain, Request $request, $id)
    {
        // return $request->all();
        $answer = EssayAnswerSocialLearning::find($id);

        $answer->comments()->save(new EssayAnswerComment([
            'essay_answer_id' => $answer->id,
            'user_id' => Auth::id(),
            'body' => $request->body,
        ]));

        return view('social-learnings.course.essay.answer-child')->withAnswer($answer);
    }

}
