<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\QuestionSocialLearning;
use App\Comment;
use App\QuestionLike;
use App\QuestionDislike;
use App\CommentLike;
use App\CommentDislike;
use App\NotificationSocialLearning;

use Auth;
use Image;

class QuestionSocialLearningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $current_user = Auth::user();
        $question = new QuestionSocialLearning;

        $question->content = $request->content;
        $question->user_id = Auth::id();
        $question->tenant_id = Auth::user()->tenant->id;
        $question->material_id = $request->material_id;

        if ($request->hasFile('photo')) {
            if ($question->photo != null) {
                if (file_exists('images/photo/' . $question->photo)) {
                    unlink(public_path('images/photo/' . $question->photo));
                }
            }
            $image = $request->file('photo');
            $filename = time() . '.' . $image->getClientOriginalName();
            $path = public_path('images/photo/' . $filename);
            Image::make($image->getRealPath())->save($path);
            $question->photo = $filename;
        }

        $question->save();

        $notification = new NotificationSocialLearning;
        $notification->title = $current_user->fullname . ' mengirim pertanyaan';
        $notification->content = $current_user->fullname . ' mengirim pertanyaan';
        $notification->user_id = $question->user_id;

        $notification->save();

        // if($request->material_id != null)
        // {
        //     return view('social-learnings.course.child.question-child')->withQuestion($question);
        // }
        // else
        // {
            return view('social-learnings.posts.child.question-child')->withQuestion($question);
        // }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $question = QuestionSocialLearning::find($id);

        $question->content = $request->content;
        $question->user_id = Auth::id();
        $question->tenant_id = Auth::user()->tenant->id;
        $question->material_id = $request->material_id;

        if ($request->hasFile('photo')) {
            if ($question->photo != null) {
                if (file_exists('images/photo/' . $question->photo)) {
                    unlink(public_path('images/photo/' . $question->photo));
                }
            }
            $image = $request->file('photo');
            $filename = time() . '.' . $image->getClientOriginalName();
            $path = public_path('images/photo/' . $filename);
            Image::make($image->getRealPath())->save($path);
            $question->photo = $filename;
        }

        $question->save();

        return redirect()->back();
        return view('social-learnings.posts.child.question-child')->withQuestion($question);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question = QuestionSocialLearning::find($id);

        $question->likes()->delete();
        $question->dislikes()->delete();
        $question->comments()->delete();
        $question->delete();

//        $link = LinkPostSocialLearning::where('post_id', $question->id)->first();
//        if ($link != null) {
//            $link->delete();
//        }

        return redirect()->back();
        return response()->json(['message' => 'Success'], 200);
    }

    public function postComment(Request $request, $id)
    {
        //     return $request->all();
        $current_user = Auth::user();
        
        $question = QuestionSocialLearning::find($id);

        $question->comments()->save(new Comment([
            'question_id' => $question->id,
            'post_id' => 0,
            'user_id' => Auth::id(),
            'body' => $request->content,
        ]));

        $notification = new NotificationSocialLearning;
        $notification->title = $current_user->fullname . ' menjawab pertanyaan ' . $question->user->fullname;
        $notification->content = $current_user->fullname . ' menjawab pertanyaan ' . $question->user->fullname;
        $notification->user_id = $question->user_id;

        $notification->save();

        return view('social-learnings.posts.child.question-child')->withQuestion($question);
    }

    public function like(Request $request, $id)
    {
        $question = QuestionSocialLearning::find($id);

        $like = QuestionLike::where([
            'question_id' => $question->id,
            'user_id' => Auth::id()
        ])->first();

        $dislike = QuestionDislike::where([
            'question_id' => $question->id,
            'user_id' => Auth::id()
        ])->first();

        if ($like != null) {
            $like->delete();

        } else {
            $question->likes()->save(new QuestionLike([
                'question_id' => $question->id,
                'user_id' => Auth::id()
            ]));
        }

        if ($dislike != null) {
            $dislike->delete();
        }

        return view('social-learnings.posts.child.question-child')->withQuestion($question);
    }

    public function dislike(Request $request, $id)
    {
        $question = QuestionSocialLearning::find($id);

        $like = QuestionLike::where([
            'question_id' => $question->id,
            'user_id' => Auth::id()
        ])->first();

        $dislike = QuestionDislike::where([
            'question_id' => $question->id,
            'user_id' => Auth::id()
        ])->first();

        if ($dislike != null) {
            $dislike->delete();

        } else {
            $question->likes()->save(new QuestionDislike([
                'question_id' => $question->id,
                'user_id' => Auth::id()
            ]));

        }

        if ($like != null) {
            $like->delete();
        }

        return view('social-learnings.posts.child.question-child')->withQuestion($question);
    }

    public function questionComment(Request $request, $id)
    {
        $req = json_decode($request->getContent());

        $question = QuestionSocialLearning::find($id);

        $question->questionComments()->save(new Comment([
            'question_id' => 0,
            'question_id' => $question->id,
            'user_id' => Auth::id(),
            'body' => $req->content,
        ]));
        return view('social-learnings.posts.child.question-child')->withQuestion($question);
    }

    public function likeComment($id)
    {
        $comment = Comment::find($id);

        $like = CommentLike::where([
            'comment_id' => $comment->id,
            'user_id' => Auth::id()
        ])->first();

        $dislike = CommentDislike::where([
            'comment_id' => $comment->id,
            'user_id' => Auth::id()
        ])->first();

        if ($like != null) {
            $like->delete();

        } else {
            $comment->likes()->save(new CommentLike([
                'comment_id' => $comment->id,
                'user_id' => Auth::id()
            ]));

        }

        if ($dislike != null) {
            $dislike->delete();
        }

        return view('social-learnings.posts.child.question-child')->withQuestion($comment->question);

    }

    public function dislikeComment($id)
    {
        $comment = Comment::find($id);

        // return $comment;

        $like = CommentLike::where([
            'comment_id' => $comment->id,
            'user_id' => Auth::id()
        ])->first();

        $dislike = CommentDislike::where([
            'comment_id' => $comment->id,
            'user_id' => Auth::id()
        ])->first();

        // return $like;

        if ($dislike != null) {
            $dislike->delete();

        } else {
            $comment->likes()->save(new CommentDislike([
                'comment_id' => $comment->id,
                'user_id' => Auth::id()
            ]));

            // return $comment->likes;

        }

        if ($like != null) {
            $like->delete();
        }

        return view('social-learnings.posts.child.question-child')->withQuestion($comment->question);
    }
}
