<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Tenant;
use App\ExchangePointSocialLearning;
use App\Course;
use App\ECertificateSocialLearning;

use Auth;
use Illuminate\Support\Facades\Hash;
use Log;
use Session;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function checkAuth($domain, Request $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
        ];

        if (!Auth::attempt($credentials)) {
            return response('Username or password is wrong!', 403);
        }

        return response(Auth::user(), 201);
    }

    public function index($domain)
    {
        $tenant = Auth::user()->tenant;
        $users = User::where('tenant_id', $tenant->id)->orderBy('created_at', 'desc')->get();
        $roles = Role::all();

        return view('users.index')
            ->withUsers($users)
            ->withRoles($roles);
    }

    public function usersByRole($domain, $id)
    {
        $tenant = Auth::user()->tenant;

        $users = User::where([
            'role_id' => $id,
            'tenant_id' => $tenant->id
        ])->get();

        $roles = Role::all();

        return view('users.index')
            ->withUsers($users)
            ->withRoles($roles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($domain)
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($domain, Request $request)
    {

        $tenant = Auth::user()->tenant;

        $this->validate($request, [
            'fullname' => 'required',
            'ktp_id' => '',
            'username' => 'required|unique:users',
            'email' => 'required|unique:users|email',
            'password' => 'required',
            'phone' => 'unique:users',
            'mobile' => 'unique:users',
            'mobile2' => 'unique:users',
            'pin_bb' => 'unique:users'
        ]);

        $user = new User;

        $user->fullname = $request->fullname;
        $user->ktp_id = $request->ktp_id;
        $user->username = $request->username;
        $user->password = Hash::make($request->password);
        $user->email = $request->email;
        $user->birth_date = $request->birth_date;
        $user->phone = $request->phone;
        $user->mobile = $request->mobile;
        $user->mobile2 = $request->mobile2;
        $user->pin_bb = $request->pin_bb;
        $user->notes = $request->notes;

        $user->photo = $request->photo;

        $user->address = $request->address;
        $user->zipcode = $request->zipcode;
        $user->fb = $request->fb;
        $user->tw = $request->tw;
        $user->website = $request->website;
        $user->status = $request->status;
        $user->hobby = $request->hobby;
        $user->reason = $request->reason;

        $user->verified = 1;
        $user->tenant_id = Auth::user()->tenant_id;

//        Default User Role
        $user->role_id = 5;

//        Default Tenant and Branch
        $user->branch_id = 0;
        $user->tenant_id = $tenant->id;

        $user->save();

        $certificate = new ECertificateSocialLearning;
        $certificate->user_id = $user->id;
        $certificate->departement = Auth::user()->tenant->name;
        $certificate->signature = 'sertifikat2.jpg';
        $certificate->template = 'sertifikat2.jpg';

        $certificate->save();

        Session::flash('success', 'User berhasil di tambahkan !');

        return redirect()->route('users.index', $_SERVER['HTTP_HOST']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($domain, $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($domain, $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($domain, Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($domain, $id)
    {
        $user = User::find($id);
        $user->delete();
    }

    public function changeRole($domain, Request $request, $id)
    {
        $user = User::find($id);
        $user->role_id = $request->role_id;

        $user->save();

        return redirect()->back();
    }

    public function addRole($domain, Request $request)
    {
        $user = User::find($request->user_id);
        $user->roles()->attach($request->role_id);

        return response()->json($user->roles);
    }

    public function deleteRole($domain, Request $request)
    {
        $user = User::find($request->user_id);
        $user->roles()->detach($request->role_id);

        return response()->json($user->roles);
    }

    public function selectExpert($domain, Request $request)
    {
        $user = User::find($request->user_id);
        $user->tenant_id = $request->tenant_id;

        $user->save();

        return redirect()->back();
    }

    public function follow(Request $request, $id)
    {
        $following_user = User::find($id);

        if($following_user != null)
        {
            $following_user->followers()->attach(Auth::id());

            Auth::user()->points()->create([
                'point' => 2,
                'user_id' => Auth::id()
            ]);

            return view('social-learnings.profile.follow')->withUser($following_user);

        } else {
            return response('Data not found!', 400);
        }

        return response('Oops somethings when wrong!', 500);
    }

    public function unfollow(Request $request, $id)
    {
        $following_user = User::find($id);
        
        if($following_user != null)
        {
            $following_user->followers()->detach(Auth::id());

            Auth::user()->points()->create([
                'point' => -2,
                'user_id' => Auth::id()
            ]);

            return view('social-learnings.profile.follow')->withUser($following_user);

        } else {
            return response('Data not found!', 400);
        }

        return response('Oops somethings when wrong!', 500);
    }

    public function videoComments()
    {
        $comments = [];
        $courses = Auth::user()->mycourses;

        foreach ($courses as $course) {

            foreach ($course->sections as $section) {

                foreach ($section->materials as $material) {

                    foreach ($material->questions as $question) {
                        array_push($comments, $question);
                    }

                }

            }

        }

        return view('students.index')->withComments($comments);

    }

    public function videoQuizzes()
    {
        $quiz_answers = [];
        $courses = Auth::user()->mycourses;

        foreach ($courses as $course) {

            foreach ($course->sections as $section) {

                foreach ($section->materials as $material) {

                    foreach ($material->quizzes as $quiz) {
                        foreach ($quiz->quizAnswers as $answer)
                        {
                            array_push($quiz_answers, $answer);
                        }
                    }

                }

            }

        }

        return view('students.quizzes')->withQuizAnswers($quiz_answers);
    }

    public function essays()
    {
        $discussion = [];
        $courses = Auth::user()->mycourses;

        foreach ($courses as $course) {

            foreach ($course->sections as $section) {

                foreach ($section->essays as $essay)
                {
                    foreach($essay->answers as $discuss)
                    {
                        array_push($discussion, $discuss);
                    }
                }

            }

        }

        return view('students.discuss')->withDiscussion($discussion);

    }

    public function students()
    {
        $students = [];
        $courses = Auth::user()->mycourses;

        foreach ($courses as $course) {
            foreach ($course->users as $user) {
                array_push($students, $user);
            }
        }

        return view('instructor.index')->withStudents($students);

    }

}
