<?php

namespace App\Http\Controllers;

use App\PostSocialLearning;
use Illuminate\Http\Request;
use App\BadgeSocialLearning;

use Auth;
use DB;

class ProfileSocialLearningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $profiles = PostSocialLearning::latest()->get()->sortByDesc("id");
        $badges = BadgeSocialLearning::all();
        $my_badges = [];

        foreach($badges as $badge)
        {
            if($badge->simple_name == 'badge_make_friend') {

                if($user->followers()->count() >= 10) {
                    array_push($my_badges, $badge);
                }

            } else if($badge->simple_name == 'badge_fans') {

                if($user->followers()->count() >= 50) {
                    array_push($my_badges, $badge);
                }

            } else if($badge->simple_name == 'badge_popular') {

                if($user->followers()->count() >= 100) {
                    array_push($my_badges, $badge);
                }

            } else if($badge->simple_name == 'badge_alumni') {

                $submitted_encounter = DB::table('user_encounters')->where([
                    'user_id' => $user->id
                ])->first();
                
                if($submitted_encounter != null) {
                    array_push($my_badges, $badge);
                }
                
            } else if($badge->simple_name == 'badge_welcome') {

                $badge->logo = 'fa-sign-out';

                $login_hour = DB::table('logs')->where([
                    'reason' => 'LOG_LOGIN',
                    'user_id' => $user->id
                ])->count();

                if($login_hour > 7) {
                    array_push($my_badges, $badge);
                }

            } else if($badge->simple_name == 'badge_nocturnal') {

                $login_hour = DB::table('logs')->where([
                    'reason' => 'LOG_LOGIN',
                    'user_id' => $user->id
                ])->count();

                if($login_hour > 7) {
                    array_push($my_badges, $badge);
                }

            } else if($badge->simple_name == 'badge_i_time') {

                $i_time = DB::table('itime_social_learnings')->where('user_id', $user->id)->first();

                if($i_time != null) {
                    array_push($my_badges, $badge);
                }

            }
            
        }

        return view('social-learnings.profile.index')
            ->withProfiles($profiles)
            ->withMyBadges($my_badges);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $user = \App\User::find($id);
        // $user->id = \App\UserController::where('user_id', $id)->get();
        //
        // return view('social-learnings.profile.index')->with('user', $user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
