<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Exercise;
use App\User;
use App\Role;
use App\Tenant;
use App\ExchangePointSocialLearning;
use App\Course;
use App\ExerciseCriteria;

use Auth;
use Session;
use Illuminate\Support\Facades\Hash;
use Log;
use DB;

class ExerciseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Auth::user()->mycourses;
        return view('instructor.exercises')->withCourses($courses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $exercise = new Exercise;

        $exercise->section_id = $request->section_id;
        $exercise->title = $request->title;

        if ($request->hasFile('file')) {

            if ($exercise->file != null) {
                if (file_exists('exercise-file/' . $exercise->file)) {
                    unlink(public_path('exercise-file/' . $exercise->file));
                }
            }

            $file = $request->file('file');
            $filename = time() . '.' . $file->getClientOriginalName();
            $path = public_path('exercise-file/');
            $file->move($path, $filename);
            $exercise->file = $filename;

            $exercise->save();

        } else {
            return response()->json('File not found', 404);
        }

        return view('courses.child-exercise')->withExercise($exercise);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($domain, $id)
    {
        $exercises = [];
        $course = Course::find($id);

        foreach ($course->sections as $section) {
            foreach ($section->exercises as $exercise) {
                array_push($exercises, $exercise);
            }
        }

        return view('instructor.exercise')->withExercises($exercises);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function submittedExercise($domain, $id)
    {
        $exercise = Exercise::find($id);

        $students = $exercise->submittedExercises;

        return view('instructor.submitted-exercise')->withStudents($students);
    }

    public function getCriterias($domain, $exercise_id)
    {
        $criterias = ExerciseCriteria::where('exercise_id', $exercise_id)->get();

        $exercise = Exercise::find($exercise_id);

        return view('exercises.criterias.index')
            ->withCriterias($criterias)
            ->withExercise($exercise);
    }

    public function storeCriteria($domain, $exercise_id, Request $request)
    {
        $exercise = Exercise::find($exercise_id);
        
        if($exercise != null)
        {
            $criteria = new ExerciseCriteria;

            $criteria->content = $request->content;
            $criteria->weight = $exercise->criterias->count() === 0 ? 100 : 0;
            $criteria->exercise_id = $exercise_id;

            $criteria->save();

            Session::flash('success', 'Berhasil menambah kriteria !');
        }
        else
        {
            Session::flash('failed', 'Oops somethings when wrong !');
        }

        return redirect()->back();
    }

    public function updateCriteria($domain, $exercise_id, $criteria_id, Request $request)
    {
        $exercise = Exercise::find($exercise_id);
        
        if($exercise != null)
        {
            $criteria = ExerciseCriteria::find($criteria_id);

            $criteria->content = $request->content;

            $criteria->save();

            Session::flash('success', 'Berhasil mengupdate kriteria !');
        }
        else
        {
            Session::flash('failed', 'Oops somethings when wrong !');
        }

        return redirect()->back();
    }

    public function updateCriteriaWeight($domain, $exercise_id, Request $request)
    {
        // dd($request->weight[2]);
        
        $exercise = Exercise::find($exercise_id);
        $exercise_criteria_percentage = 0;

        // dd($exercise->criterias);
        
        if($exercise != null)
        {
            foreach($exercise->criterias as $key => $c)
            {
                $exercise_criteria_percentage += $request->weight[$key];
            }

            if($exercise_criteria_percentage === 100)
            {
                foreach($exercise->criterias as $index => $criteria)
                {
                    $criteria->weight = $request->weight[$index];

                    $criteria->save();

                }

                Session::flash('success', 'Berhasil merubah bobot kriteria !');
            }
            else
            {
                Session::flash('failed', 'Total bobot harus 100 !');
            }

        }
        else
        {
            Session::flash('failed', 'Oops somethings when wrong !');
        }

        return redirect()->back();
    }

    public function deleteCriteria($domain, $exercise_id, $criteria_id)
    {
        $criteria = ExerciseCriteria::find($criteria_id);
        
        $exercise = Exercise::find($exercise_id);

        $criteria->delete();
        
        if($exercise != null)
        {
            $first_criteria = $exercise->criterias->first();
            $first_criteria->weight += $criteria->weight;
            
            $assessments = DB::table('exercise_assessments')
                ->where('exercise_id', $exercise->id)
                ->where('criteria_id', $criteria->id)
                ->delete();

            $first_criteria->save();

            Session::flash('success', 'Berhasil menghapus kriteria !');
        }

        return redirect()->back();
    }

    // Assessments

    public function getAssessments($domain, $exercise_id)
    {
        $exercise = Exercise::find($exercise_id);

        if($exercise != null)
        {
            return view('exercises.assessments.index')
                ->withAssessments($exercise->submittedExercises)
                ->withExercise($exercise);
        }
        
        return redirect()->back();
        
    }

    public function storeAssessment($domain, $exercise_id, $user_id, Request $request)
    {
        $exercise = Exercise::find($exercise_id);
        $criterias = ExerciseCriteria::where('exercise_id', $exercise_id)->get();
        
        if($exercise != null && $criterias != null)
        {

            foreach($criterias as $index => $criteria)
            {
                $is_assessment_exist = DB::table('exercise_assessments')
                    ->where('exercise_id', $exercise_id)
                    ->where('criteria_id', $criteria->id)
                    ->where('user_id', $user_id);
                
                if($is_assessment_exist->first() === null)
                {
                    DB::table('exercise_assessments')->insert([
                        'exercise_id' => $exercise_id,
                        'criteria_id' => $criteria->id,
                        'user_id' => $user_id,
                        'value' => $request->value[$index]
                    ]);
                }
                else
                {
                    $is_assessment_exist->update(['value' => $request->value[$index]]);
                }
                
            }

            Session::flash('success', 'Berhasil menilai !');
        }
        else
        {
            Session::flash('failed', 'Oops somethings when wrong !');
        }

        return redirect()->back();
    }
}
