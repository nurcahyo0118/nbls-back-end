<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;

use Auth;
use Session;
use DB;

class BookingSocialLearningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('social-learnings.booking.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $course = Course::find($request->course_id);

        // return $course->bookings()->get(['username']);

        // return response()->json(DB::table('course_bookings')->where('user_id', Auth::id())->whereNotIn('course_id', $course->bookings)->first(), 200);

        if(DB::table('course_bookings')->where('user_id', Auth::id())->where('course_id', '<>', $course->id)->first() === null)
        {
            $course->bookings()->attach(Auth::id(), [
                'class_id' => $request->class_id,
                'date' => $request->date
            ]);

            Session::flash('success', 'Berhasil melakukan booking !');
        } else {
            Session::flash('failed', 'Gagal melakukan booking, coba hapus booking sebelumnya !');
        }

        return redirect()->route('social-learning-booking.index', $_SERVER['HTTP_HOST'])->withCourse($course);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($domain, $id)
    {
        // $booking = DB::table('course_bookings')->where('id', $id)->first();
        $booking = Auth::user()->bookings()->where('course_id', $id)->first();

        if($booking != null)
        {
            Auth::user()->bookings()->detach($id);
            Session::flash('success', 'Berhasil membatalkan booking !');
        }
        else
        {
            Session::flash('failed', 'Gagal menghapus booking, data tidak ditemukan !');
        }

        return redirect()->back();
    }
}
