<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class PartialController extends Controller
{
    public function section($domain)
    {
        $categories = Category::all();
        $courses = Course::all();

        return view('courses.index')
            ->withCategories($categories)
            ->withCourses($courses);
    }
}
