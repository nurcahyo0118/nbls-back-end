<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Course;
use App\ExchangePointHistory;
use Auth;
use Image;
use Session;

class ExchangePointSocialLearningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $current_user = Auth::user();

        if($current_user != null)
        {
            $all_courses = Course::where(['tenant_id' => $current_user->tenant_id, 'status' => 'POSTED'])
                ->whereNotIn('id', $current_user->courses)
                ->get();
                
            $courses = [];

            foreach($all_courses as $course)
            {
                if($course->image != null)
                {
                    $course->image = base64_encode(Image::make(public_path('xxcourses/images/' . $course->image))->encode());
                }

                array_push($courses, $course);
            }

            $exchange_histories = ExchangePointHistory::where(['user_id' => $current_user->id])->get();

            return view('social-learnings.exchange-point.index')
                ->withCourses($courses)
                ->withExchangeHistories($exchange_histories);
            
        } else {
            // Unauthorize
            return response()->json('Unauthorize', 401);
        }

        return response()->json('Oops something when wrong!', 500);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function sampleExchange($id)
    {
        $current_user = Auth::user();
        $course = Course::find($id);

        if($current_user != null)
        {
            if($course != null)
            {
                if ($current_user->points()->sum('point') > 1000) 
                {
                    $course->users()->attach(Auth::id());

                    $current_user->gems()->create([
                        'gem' => 100,
                        'user_id' => $current_user->id
                    ]);
    
                    $history = new ExchangePointHistory;
                    $history->exchange_date = '2000-01-21';
                    $history->course_id = $course->id;
                    $history->user_id = $current_user->id;
    
                    $history->save();

                    $current_user->points()->create([
                        'point' => -10000,
                        'user_id' => $current_user->id
                    ]);

                    if($course->image != null)
                    {
                        $course->image = base64_encode(Image::make(public_path('xxcourses/images/' . $course->image))->encode());
                    }

                    Session::flash('success', 'Berhasil menukar poin !');

                    return redirect()->back();

                } else {
                    // Point is not enough
                    Session::flash('failed', 'Point tidak cukup !');
                    return redirect()->back();
                    // return response()->json('point is not enough', 400);
                }
            } else {
                // Data not found
                return response()->json('The data you are referring to is not found!', 400);
            }
            
        } else {
            // Unauthorize
            return response()->json('Unauthorize', 401);
        }

        return response()->json('Oops something when wrong!', 500);
    }
}
