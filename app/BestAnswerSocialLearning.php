<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BestAnswerSocialLearning extends Model
{
    protected $table = 'best_answer_social_learnings';
}
