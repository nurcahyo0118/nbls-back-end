<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterpieceSocialLearning extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function likesMasterpieces()
    {
      return $this->hasMany('App\PostLikeMasterpiece', 'post_id');
    }

    public function dislikesMasterpieces()
    {
      return $this->hasMany('App\PostDislikeMasterpiece', 'post_id');
    }

    public function postCommentsMasterpieces()
    {
      return $this->hasMany('App\CommentMasterpiece', 'post_id');
    }
}
