<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BadgeSocialLearning extends Model
{
    public function users()
    {
        return $this->belongsToMany('App\User', 'user_badges', 'badge_id', 'user_id');
    }
}
