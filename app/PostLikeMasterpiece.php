<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostLikeMasterpiece extends Model
{
  protected $fillable = ['post_id', 'user_id'];

  public function CommentMasterpiece()
  {
      return $this->hasOne('App\MasterpieceSocialLearning', 'post_id');
  }
}
