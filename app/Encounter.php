<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Encounter extends Model
{
    public function course()
    {
        return $this->hasOne('App\Course', 'id', 'course_id');
    }

    public function submittedEncounters()
    {
        return $this->belongsToMany('App\User', 'user_encounters', 'encounter_id', 'user_id')->withPivot('file', 'link');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function criterias()
    {
        return $this->hasMany('App\Criteria', 'encounter_id');
    }

}
