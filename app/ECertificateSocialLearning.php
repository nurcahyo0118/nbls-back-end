<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ECertificateSocialLearning extends Model
{
    public function user()
    {
        return $this->belongsToMany('App\User', 'user_id');
    }
    public function course()
    {
        return $this->belongsToMany('App\Course', 'course_id');
    }
}
