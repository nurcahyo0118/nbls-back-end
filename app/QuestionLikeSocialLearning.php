<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionLikeSocialLearning extends Model
{
    protected $fillable = ['question_id', 'user_id'];
    
    public function question()
    {
        return $this->hasOne('App\QuestionSocialLearning', 'question_id');
    }
}
