<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Criteria extends Model
{
    public function encounter()
    {
        return $this->belongsTo('App\Encounter', 'encounter_id');
    }

    public function assessedEncounters()
    {
        return $this->belongsToMany('App\User', 'encounter_assessments', 'criteria_id', 'user_id')->withPivot('value');
    }
}
