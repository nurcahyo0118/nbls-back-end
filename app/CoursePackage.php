<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoursePackage extends Model
{
    public function packages()
    {
        return $this->belongsToMany('App\Course', 'course_package_courses', 'course_package_id', 'course_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id');
    }

    public function package_carts()
    {
        return $this->belongsToMany('App\User', 'user_package_carts', 'package_course_id', 'user_id');
    }

    // public function users()
    // {
    //     return $this->belongsToMany('App\User', 'course_package_users', 'course_package_id', 'user_id');
    // }
}
