<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GemSocialLearning extends Model
{
  protected $fillable = ['gem', 'user_id'];

  public function user()
  {
      return $this->hasOne('App\User', 'id', 'user_id');
  }
}
