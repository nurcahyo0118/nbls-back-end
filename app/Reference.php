<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reference extends Model
{
    public function section()
    {
        return $this->hasOne('App\Section', 'id', 'section_id');
    }
}
