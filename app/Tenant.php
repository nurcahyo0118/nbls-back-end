<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tenant extends Model
{
    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function courses()
    {
        return $this->hasMany('App\Course');
    }

    public function categories()
    {
        return $this->hasMany('App\Category');
    }
}
