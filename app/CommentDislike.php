<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentDislike extends Model
{
    protected $fillable = ['comment_id', 'user_id'];

    public function comment()
    {
        return $this->hasOne('App\Comment', 'comment_id');
    }
}
