<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentMasterpieceDislike extends Model
{
  protected $fillable = ['comment_id', 'user_id'];

  public function CommentMasterpiece()
  {
      return $this->hasOne('App\CommentMasterpiece', 'comment_id');
  }
}
