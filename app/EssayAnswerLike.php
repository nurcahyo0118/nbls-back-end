<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EssayAnswerLike extends Model
{
    protected $fillable = ['essay_answer_id', 'user_id'];
    
    public function answer()
    {
        return $this->hasOne('App\EssayAnswerSocialLearning', 'essay_answer_id');
    }
}
