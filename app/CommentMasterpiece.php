<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentMasterpiece extends Model
{
  protected $fillable = ['question_id', 'post_id', 'user_id', 'body'];

  public function Masterpieces()
  {
    return $this->hasOne('App\MasterpieceSocialLearning', 'id', 'post_id');
  }

  public function user()
  {
    return $this->hasOne('App\User', 'id', 'user_id');
  }

  public function likesMasterpieces()
  {
    return $this->hasMany('App\CommentMasterpieceLike', 'comment_id');
  }

  public function dislikesMasterpieces()
  {
    return $this->hasMany('App\CommentMasterpieceDislike', 'comment_id');
  }
}
