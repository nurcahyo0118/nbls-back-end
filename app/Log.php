<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $fillable = ['reason', 'user_id'];

    public function user() 
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
