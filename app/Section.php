<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    public function course()
    {
        return $this->hasOne('App\Course', 'id', 'course_id');
    }

    public function materials()
    {
        return $this->hasMany('App\Material', 'section_id');
    }

    public function references()
    {
        return $this->hasMany('App\Reference', 'section_id');
    }

    public function exercises()
    {
        return $this->hasMany('App\Exercise', 'section_id');
    }

    public function essays()
    {
        return $this->hasMany('App\EssaySocialLearning', 'section_id');
    }

}
