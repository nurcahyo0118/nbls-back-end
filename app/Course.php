<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    public function author()
    {
        return $this->belongsTo('App\User', 'author_id');
    }

    public function sections()
    {
        return $this->hasMany('App\Section', 'course_id')->orderBy('line');
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'course_users', 'course_id', 'user_id');
    }

    public function carts()
    {
        return $this->belongsToMany('App\User', 'user_carts', 'course_id', 'user_id');
    }

    public function questions()
    {
        return $this->hasMany('App\Question', 'course_id');
    }

    public function bookings()
    {
        return $this->belongsToMany('App\User', 'course_bookings', 'course_id', 'user_id')
            ->withPivot('class_id', 'date');
    }

    public function encounters()
    {
        return $this->hasMany('App\Encounter', 'course_id');
    }

    public function reviews()
    {
      return $this->hasMany('App\ReviewSocialLearning', 'course_id');
    }

    public function classes()
    {
      return $this->hasMany('App\BackendClass', 'course_id');
    }

    public function packages()
    {
        return $this->belongsToMany('App\CoursePackage', 'course_package_courses', 'course_id', 'course_package_id');
    }

    public function widgets()
    {
      return $this->hasMany('App\SalesWidgetSocialLearning', 'course_id');
    }

    public function exchangeHistories()
    {
        return $this->hasMany('App\ExchangePointHistory', 'course_id');
    }

}
