<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public function course()
    {
      return $this->hasOne('App\Course', 'id', 'course_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
  
    public function likes()
    {
      return $this->hasMany('App\QuestionLike', 'question_id');
    }
  
    public function dislikes()
    {
      return $this->hasMany('App\QuestionDislike', 'question_id');
    }
  
    public function comments()
    {
      return $this->hasMany('App\Comment', 'question_id');
    }

}
