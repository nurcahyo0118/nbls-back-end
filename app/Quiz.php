<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    public function material()
    {
        return $this->hasOne('App\Material', 'id', 'material_id');
    }

    public function quizAnswers()
    {
        return $this->belongsToMany('App\User', 'user_quizzes', 'quiz_id', 'user_id')->withPivot('answer');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
