<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EssayCommentSocialLearning extends Model
{
    public function answer()
    {
        return $this->hasOne('App\EssayAnswerSocialLearning', 'id', 'essay_answer_id');
    }
}
