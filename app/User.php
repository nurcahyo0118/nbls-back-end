<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $hidden = [
        'password', 'remember_token',
    ];

    public $timestamps = true;

    public function roles()
    {
        return $this->belongsToMany('App\Role', 'user_roles');
    }

    public function role()
    {
        return $this->hasOne('App\Role', 'id', 'role_id');
    }

    public function tenant()
    {
        return $this->hasOne('App\Tenant', 'id', 'tenant_id');
    }

    public function certificate()
    {
        return $this->hasOne('App\ECertificateSocialLearning', 'id', 'user_id');
    }

    public function likes()
    {
        return $this->belongsToMany('App\PostSocialLearning', 'user_likes');
    }

    public function dislikes()
    {
        return $this->belongsToMany('App\PostSocialLearning', 'user_dislikes');
    }

    public function postComments()
    {
        return $this->belongsToMany('App\PostSocialLearning', 'user_comments');
    }

    private function checkIfUserHasRole($need_role)
    {
        // return (strtolower($need_role)==strtolower($this->role->name)) ? true : null;
    }

    public function hasRole($roles)
    {
        if (is_array($roles)) {
            foreach ($roles as $need_role) {
                if ($this->checkIfUserHasRole($need_role)) {
                    return true;
                }
            }
        } else {
            return $this->checkIfUserHasRole($roles);
        }
        return false;
    }

    public function myevents()
    {
        return $this->hasMany('App\EventSocialLearning', 'user_id');
    }

    public function mycourses()
    {
        return $this->hasMany('App\Course', 'author_id');
    }

    public function events()
    {
        return $this->belongsToMany('App\EventSocialLearning', 'event_follow_social_learnings', 'user_id', 'event_id');
    }

    public function courses()
    {
        return $this->belongsToMany('App\Course', 'course_users', 'user_id', 'course_id');
    }

    public function carts()
    {
        return $this->belongsToMany('App\Course', 'user_carts', 'user_id', 'course_id');
    }

    public function package_carts()
    {
        return $this->belongsToMany('App\CoursePackage', 'user_package_carts', 'user_id', 'package_course_id');
    }

    public function followers()
    {
        return $this->belongsToMany('App\User', 'user_follows', 'following_id', 'follower_id');
    }

    public function followings()
    {
        return $this->belongsToMany('App\User', 'user_follows', 'follower_id', 'following_id');
    }

    public function points()
    {
        return $this->hasMany('App\ExchangePointSocialLearning', 'user_id');
    }

    public function gems()
    {
        return $this->hasMany('App\GemSocialLearning', 'user_id');
    }

    public function bookings()
    {
        return $this->belongsToMany('App\Course', 'course_bookings', 'user_id', 'course_id')
            ->withPivot('class_id', 'date');
    }

    public function posts()
    {
        return $this->hasMany('App\PostSocialLearning', 'user_id');
    }

    // masterpiece
    public function masterpieces()
    {
        return $this->belongsTo('App\MasterpieceSocialLearning', 'user_id');
    }

    public function likesMasterpieces()
    {
        return $this->belongsToMany('App\MasterpieceSocialLearning', 'user_likes');
    }

    public function dislikesMasterpieces()
    {
        return $this->belongsToMany('App\MasterpieceSocialLearning', 'user_dislikes');
    }

    public function postCommentsMasterpieces()
    {
        return $this->belongsToMany('App\MasterpieceSocialLearning', 'user_comments');
    }

    // end masterpieces

    public function logs()
    {
        return $this->hasMany('App\Log', 'user_id');
    }

    public function orderHistories()
    {
        return $this->hasMany('App\OrderHistorySocialLearning', 'user_id');
    }

    public function reviews()
    {
        return $this->hasMany('App\ReviewSocialLearning', 'user_id');
    }

    public function viewedVideos()
    {
        return $this->belongsToMany('App\Material', 'viewed_videos', 'user_id', 'material_id');
    }

    public function viewedExercises()
    {
        return $this->belongsToMany('App\Exercise', 'viewed_exercises', 'user_id', 'exercise_id');
    }

    public function submittedExercises()
    {
        return $this->belongsToMany('App\Exercise', 'user_exercises', 'user_id', 'exercise_id');
    }

    public function coursePackages()
    {
        return $this->belongsToMany('App\User', 'course_package_users', 'user_id', 'course_package_id');
    }

    public function quizAnswers()
    {
        return $this->belongsToMany('App\Quiz', 'user_quizzes', 'user_id', 'quiz_id');
    }

    public function badges()
    {
        return $this->belongsToMany('App\BadgeSocialLearning', 'user_badges', 'user_id', 'badge_id');
    }

//    Pages
    public function pages()
    {
        return $this->hasMany('App\Page', 'user_id');
    }

    // Encounter Assessments
    public function assessedEncounters()
    {
        return $this->belongsToMany('App\Criteria', 'encounter_assessments', 'user_id', 'criteria_id')->withPivot('value');
    }
    
    public function verifyUser()
    {
        return $this->hasOne('App\VerifyUser');
    }
}
