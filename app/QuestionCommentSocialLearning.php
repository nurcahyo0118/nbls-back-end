<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionCommentSocialLearning extends Model
{
    public function question()
    {
        return $this->hasOne('App\Question', 'id', 'question_id');
    }
}
