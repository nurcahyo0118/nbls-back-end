<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExerciseCriteria extends Model
{
    public function exercise()
    {
        return $this->belongsTo('App\Exercise', 'exercise_id');
    }
}
