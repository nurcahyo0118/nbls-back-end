<?php

namespace App;

use App\User;
use App\EventSocialLearning;
use Illuminate\Database\Eloquent\Model;

class EventSocialLearning extends Model
{

  public function user()
  {
    return $this->hasOne('App\User', 'id', 'user_id');
  }


  public function users()
  {
    return $this->belongsToMany('App\User', 'event_follow_social_learnings', 'event_id', 'user_id');
  }

}
