<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
  protected $fillable = ['question_id', 'post_id', 'user_id', 'body'];

  public function post()
  {
    return $this->hasOne('App\PostSocialLearning', 'id', 'post_id');
  }

  public function question()
  {
    return $this->hasOne('App\QuestionSocialLearning', 'id', 'question_id');
  }

  public function user()
  {
    return $this->hasOne('App\User', 'id', 'user_id');
  }

  public function likes()
  {
    return $this->hasMany('App\CommentLike', 'comment_id');
  }

  public function dislikes()
  {
    return $this->hasMany('App\CommentDislike', 'comment_id');
  }

  public function masterpiecelikes()
  {
    return $this->hasMany('App\CommentMasterpieceLike', 'comment_id');
  }

  public function masterpiecedislikes()
  {
    return $this->hasMany('App\CommentMasterpieceDislike', 'comment_id');
  }

}
