<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EssayAnswerComment extends Model
{
    protected $fillable = ['essay_answer_id', 'user_id', 'body'];

    public function answer()
    {
        return $this->belongsTo('App\EssayAnswerSocialLearning', 'essay_answer_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
