<?php

namespace App;
Use App\PostSocialLearning;

use Illuminate\Database\Eloquent\Model;

class CommentSocialLearning extends Model
{
    public function post()
    {
        return $this->belongsTo('App\PostSocialLearning');
    }
}
