<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LinkPostSocialLearning extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User', 'post_id');
    }
}
