<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EssaySocialLearning extends Model
{

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    
    public function section()
    {
        return $this->hasOne('App\Section', 'id', 'section_id');
    }

    public function answers()
    {
        return $this->hasMany('App\EssayAnswerSocialLearning', 'essay_id');
    }
}
