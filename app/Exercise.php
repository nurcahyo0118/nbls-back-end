<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exercise extends Model
{
    public function section()
    {
        return $this->hasOne('App\Section', 'id', 'section_id');
    }

    public function viewers()
    {
        return $this->belongsToMany('App\User', 'viewed_exercises', 'exercise_id', 'user_id');
    }

    public function submittedExercises()
    {
        return $this->belongsToMany('App\User', 'user_exercises', 'exercise_id', 'user_id')->withPivot('file', 'link');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function criterias()
    {
        return $this->hasMany('App\ExerciseCriteria', 'exercise_id');
    }
}
