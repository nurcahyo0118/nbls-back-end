<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EssayAnswerSocialLearning extends Model
{
    protected $fillable = ['essay_id', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function essay()
    {
        return $this->belongsTo('App\EssaySocialLearning', 'essay_id');
    }

    public function likes()
    {
        return $this->hasMany('App\EssayAnswerLike', 'essay_answer_id');
    }

    public function dislikes()
    {
        return $this->hasMany('App\EssayAnswerDislike', 'essay_answer_id');
    }

    public function comments()
    {
        return $this->hasMany('App\EssayAnswerComment', 'essay_answer_id');
    }
}
