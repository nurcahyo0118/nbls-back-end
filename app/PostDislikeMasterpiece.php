<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostDislikeMasterpiece extends Model
{
  protected $fillable = ['post_id', 'user_id'];

  public function CommentMasterpiece()
  {
      return $this->hasOne('App\MasterpieceSocialLearning', 'post_id');
  }
}
