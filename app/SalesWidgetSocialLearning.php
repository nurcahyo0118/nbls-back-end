<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesWidgetSocialLearning extends Model
{
    public function course()
    {
        return $this->hasOne('App\Course', 'id', 'course_id');
    }
}
