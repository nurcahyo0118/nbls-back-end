<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $casts = [
        'is_private' => 'boolean',
        'is_published' => 'boolean'
    ];

    //

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
