<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostDislike extends Model
{
    protected $fillable = ['post_id', 'user_id'];

    public function comment()
    {
        return $this->hasOne('App\PostSocialLearning', 'post_id');
    }
}
