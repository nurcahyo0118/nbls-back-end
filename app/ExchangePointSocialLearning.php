<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExchangePointSocialLearning extends Model
{
    protected $fillable = ['point', 'user_id'];

    public function user() 
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
