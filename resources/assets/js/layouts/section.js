function onSubmit(id) {
    var title = $('#title').val();
    var description = $('#description').val();

    $.ajax({
        type: "POST",
        url: '/sections',
        data: {
            title: title,
            description: description,
            course_id: id
        },
        success: function (response) {
            console.log(response);
            $("#modalAddSection").modal('toggle');

            $("#sectionsList").append(
                "<div id=\"section{{ $section->id }}\">\n" +
                "                                            <div class=\"box box-primary box-solid collapsed-box\">\n" +
                "                                                <div class=\"box-header with-border\">\n" +
                "                                                    <h3 class=\"box-title\"><b>Modul \n" +
                "                                                                             : </b>" + response.title + "</h3>\n" +
                "\n" +
                "                                                    <div class=\"box-tools pull-right\">\n" +
                "                                                        <button type=\"button\" class=\"btn btn-box-tool\"\n" +
                "                                                                data-widget=\"collapse\">\n" +
                "                                                            <i class=\"fa fa-plus\"></i>\n" +
                "                                                        </button>\n" +
                "                                                    </div>\n" +
                "\n" +
                "                                                </div>\n" +
                "\n" +
                "                                                <div class=\"box-body\">\n" +
                "                                                    <div class=\"col-md-12\">\n" +
                "\n" +
                "                                                        <div class=\"form-group\">\n" +
                "                                                            <label>Deskripsi : </label>\n" +
                "                                                            <p></p>\n" +
                "                                                            " + response.description + "\n" +
                "                                                        </div>\n" +
                "                                                    </div>\n" +
                "\n" +
                "                                                    <div class=\"col-md-12\">\n" +
                "                                                        <button class=\"btn btn-info\">\n" +
                "                                                            <i class=\"fa fa-plus\"></i>\n" +
                "                                                            Tambah Materi\n" +
                "                                                        </button>\n" +
                "                                                        <br>\n" +
                "                                                        <br>\n" +
                "                                                    </div>\n" +
                "\n" +
                "                                                    \n" +
                "                                                        <div class=\"col-md-12\">\n" +
                "\n" +
                "                                                            <div class=\"box box-info collapsed-box\">\n" +
                "                                                                <div class=\"box-header with-border\">\n" +
                "\n" +
                "                                                                    <h3 class=\"box-title\"><b>Materi  : </b></h3>\n" +
                "                                                                    <input type=\"text\" value=" + response.title + "\n" +
                "                                                                           class=\"form-control\">\n" +
                "\n" +
                "                                                                    <div class=\"box-tools pull-right\">\n" +
                "                                                                        <button type=\"button\" class=\"btn btn-box-tool\"\n" +
                "                                                                                data-widget=\"collapse\">\n" +
                "                                                                            <i class=\"fa fa-plus\"></i>\n" +
                "                                                                        </button>\n" +
                "\n" +
                "                                                                        <button type=\"button\" class=\"btn btn-box-tool\">\n" +
                "                                                                            <i class=\"fa fa-trash\"></i>\n" +
                "                                                                        </button>\n" +
                "                                                                    </div>\n" +
                "\n" +
                "                                                                </div>\n" +
                "\n" +
                "                                                                <div class=\"box-body\">\n" +
                "\n" +
                "                                                                </div>\n" +
                "\n" +
                "                                                            </div>\n" +
                "\n" +
                "                                                        </div>\n" +
                "                                                    \n" +
                "\n" +
                "                                                </div>\n" +
                "\n" +
                "                                                <div class=\"box-footer\">\n" +
                "                                                    <div class=\"col-md-12 text-right\">\n" +
                "                                                        <button class=\"btn btn-danger\" type=\"button\" onclick=\"onDeleteSection(1)\">\n" +
                "                                                            <i class=\"fa fa-trash\"></i>\n" +
                "                                                            Hapus\n" +
                "                                                        </button>\n" +
                "                                                    </div>\n" +
                "                                                </div>\n" +
                "\n" +
                "                                            </div>\n" +
                "                                        </div>"
            );
            swal("Berhasil !", "Modul berhasil ditambahkan", "success");
        },
        error: function (error) {
            swal("Gagal !", "Section gagal ditambahkan", "error");
        }
    });
}