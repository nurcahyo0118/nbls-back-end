@extends('layouts.main')

@section('content')

    @php
        $tenant = Auth::user()->tenant;
        $permission = App\Permission::where(['role_id' => Auth::user()->role['id'], 'tenant_id' => $tenant->id])->first();
    @endphp

    <div class="row">

        <div class="col-xs-12">

            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Kinerja Murid</h3>

                </div>

                <div class="box-body">
                    <table class="table table-export">
                        <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Progres</th>
                        </tr>
                        </thead>
                        <tbody id="encounterList">

                        @foreach($students as $student)
                            @php
                                $answer = null;
                                $encounterAssessments = [];
                                $materials = [];
                                $exercises = [];
                                $quizzes = [];
                                $encounter = App\Encounter::where('course_id', $course->id)->first();
                        
                                if($encounter != null)
                                {
                                    $answer = $encounter->submittedEncounters()->where(['user_id' => $student->id])->first();
                                }
                        
                                foreach($course->sections as $section) {
                                    foreach($section->materials as $material)
                                    {
                                        array_push($materials, $material->id);
                        
                                        foreach($material->quizzes as $quiz)
                                        {
                                            array_push($quizzes, $quiz->id);
                                        }
                        
                                    }
                                }
                        
                                foreach($course->sections as $section) {
                                    foreach($section->exercises as $exercise)
                                    {
                                        array_push($exercises, $exercise->id);
                                    }
                                }
                        
                                $videoPerformance = DB::table('viewed_videos')
                                    ->where('user_id', $student->id)
                                    ->whereIn('material_id', $materials)
                                    ->get();
                        
                        
                                $exercisePerformance = DB::table('user_exercises')
                                    ->where('user_id', $student->id)
                                    ->whereIn('exercise_id', $exercises)
                                    ->get();
                        
                                $quizPerformance = DB::table('user_quizzes')
                                    ->where('user_id', $student->id)
                                    ->whereIn('quiz_id', $quizzes)
                                    ->get();
                        
                                if(collect($materials)->count() < 1)
                                {
                                    $videoProgress = 0;
                                }
                                else
                                {
                                    $videoProgress = $videoPerformance->count() / collect($materials)->count() * 100;
                                }
                        
                                if(collect($exercises)->count() < 1 && collect($quizzes)->count() < 1)
                                {
                                    $exerciseProgress = 0;
                                }
                                else
                                {
                                    $exerciseProgress = ($exercisePerformance->count() + $quizPerformance->count()) / (collect($exercises)->count() + collect($quizzes)->count()) * 100;
                                }
                        
                                $encounterProgress = $answer != null ? 1 * 100 : 0;
                        
                                $overallProgress = ($videoProgress * (30 / 100)) + ($exerciseProgress * (20 / 100)) + ($encounterProgress * (50 / 100));       
                            @endphp

                            <tr>
                                <td>{{ $student->fullname }}</td>
                                <td>{{ number_format($overallProgress, 2, '.', '') }}%</td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div>

            </div>

        </div>
    </div>

@endsection