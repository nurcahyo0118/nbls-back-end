<li class="nav-item dropdown notification-list float-right d-none d-md-none d-lg-block">
    <a class="nav-link waves-effect waves-light nav-user"
       data-toggle="dropdown"
       href="#"
       role="button"
       aria-haspopup="false"
       aria-expanded="false">
        @if( Auth::user()->photo === null)
            <img class="rounded-circle"
                 src="/images/default.png" alt="Generic placeholder image">
        @else
            <img class="rounded-circle"
                 src="/images/photo/{{ Auth::user()->photo }}" alt="Generic placeholder image">
        @endif
    </a>

    <div class="dropdown-menu dropdown-menu-right profile-dropdown " aria-labelledby="Preview">
        <!-- item-->
        <a href="/social-learning-profile" class="dropdown-item notify-item">
            <i class="zmdi zmdi-account-circle"></i>
            <span>My Profile</span>
        </a>
        <!-- item-->
        <a href="/social-learning-setting" class="dropdown-item notify-item">
            <i class="zmdi zmdi-account-circle"></i>
            <span>Setting</span>
        </a>


        <a class="dropdown-item notify-item" data-toggle="modal" data-target="#logoutModal">
            <i class="zmdi zmdi-power"></i>
            <span>Logout</span>
        </a>


    </div>
</li>
