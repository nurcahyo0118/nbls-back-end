<div class="col-md-4 col-lg-3 footer-space">
    <!-- point -->
    <div class="card-box">
        <h2 class="m-t-0 m-b-20 header-title">
            <b>
                Point Saya
                <span class="text-muted">({{ Auth::user()->points->sum('point') }})</span>
            </b>
        </h2>

        <hr>

        <!-- Start Badge -->
        @php
            $badge = Auth::user()->points->sum('point');
        @endphp

        @if($badge < 399)

            <a class="row" href="/social-learning-my-badge">
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <img src="/images/brand/TOSCA.png" class="img-rounded center-block w-75 h-75" alt="point-saya">
                </div>

                <div class="col-md-6 col-md-6 col-xs-6 p-l-0 p-r-0">
                    <strong><p class="text-info m-0">NEWBIE</p></strong>
                    <div class="progress mt-2">
                        <div class="progress-bar" role="progressbar" style="width: 16%" aria-valuenow="25"
                             aria-valuemin="0"
                             aria-valuemax="100"></div>
                    </div>
                </div>
            </a>
        @elseif($badge >= 899 and $badge <= 2999)
            <a class="row" href="/social-learning-my-badge">
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <img src="/images/brand/MERAH.png" class="img-rounded center-block w-75 h-75" alt="point-saya">
                </div>

                <div class="col-md-6 col-md-6 col-xs-6 p-l-0 p-r-0">
                    <strong><p class="text-info m-0">TRAINED</p></strong>
                    <div class="progress mt-2">
                        <div class="progress-bar" role="progressbar" style="width: 32%" aria-valuenow="25"
                             aria-valuemin="0"
                             aria-valuemax="100"></div>
                    </div>
                </div>
            </a>
        @elseif($badge >= 2999 and $badge <= 6999)
            <a class="row" href="/social-learning-my-badge">
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <img src="/images/brand/BLUE.png" class="img-rounded center-block w-75 h-75" alt="point-saya">
                </div>

                <div class="col-md-6 col-md-6 col-xs-6 p-l-0 p-r-0">
                    <strong><p class="text-info m-0">SUPER</p></strong>
                    <div class="progress mt-2">
                        <div class="progress-bar" role="progressbar" style="width: 48%" aria-valuenow="25"
                             aria-valuemin="0"
                             aria-valuemax="100"></div>
                    </div>
                </div>
            </a>
        @elseif($badge > 6999 and $badge <= 10000)
            <a class="row" href="/social-learning-my-badge">
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <img src="/images/brand/ORANGE.png" class="img-rounded center-block w-75 h-75" alt="point-saya">
                </div>

                <div class="col-md-6 col-md-6 col-xs-6 p-l-0 p-r-0">
                    <strong><p class="text-info m-0">GEEK</p></strong>
                    <div class="progress mt-2">
                        <div class="progress-bar" role="progressbar" style="width: 66%" aria-valuenow="25"
                             aria-valuemin="0"
                             aria-valuemax="100"></div>
                    </div>
                </div>
            </a>
        @elseif($badge > 10000 and $badge <= 18000)
            <a class="row" href="/social-learning-my-badge">
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <img src="/images/brand/HIJAU.png" class="img-rounded center-block w-75 h-75" alt="point-saya">
                </div>

                <div class="col-md-6 col-md-6 col-xs-6 p-l-0 p-r-0">
                    <strong><p class="text-info m-0">FREAK</p></strong>
                    <div class="progress mt-2">
                        <div class="progress-bar" role="progressbar" style="width: 82%" aria-valuenow="25"
                             aria-valuemin="0"
                             aria-valuemax="100"></div>
                    </div>
                </div>
            </a>
        @elseif($badge >= 18000)
            <a class="row" href="/social-learning-my-badge">
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <img src="/images/brand/HIJAU.png" class="img-rounded center-block w-75 h-75" alt="point-saya">
                </div>

                <div class="col-md-6 col-md-6 col-xs-6 p-l-0 p-r-0">
                    <strong><p class="text-info m-0">MASTER</p></strong>
                    <div class="progress mt-2">
                        <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="25"
                             aria-valuemin="0"
                             aria-valuemax="100"></div>
                    </div>
                </div>
            </a>
    @endif
    <!-- End Badge -->

    </div>
    <!-- end point -->


    <!-- peringkat -->
    <div class="card-box m-t-0">
        <h2 class="m-t-0 m-b-20 header-title"><b>Peringkat</b></h2>
        <hr>
        <div class="dropdown text-muted">
            <div class="search-result-box">
                <ul class="nav nav-tabs">

                    <li class="nav-item col-md-6 col-sm-6 p-0 ">
                        <h5>
                            <a href="#pil1" data-toggle="tab" aria-expanded="true" class="nav-link active">
                                minggu ini
                            </a>
                        </h5>
                    </li>

                    <li class="nav-item col-md-6 col-sm-6 p-0">
                        <h5>
                            <a href="#pil2" data-toggle="tab" aria-expanded="false" class="nav-link">
                                bulan ini
                            </a>
                        </h5>
                    </li>

                </ul>
                <div class="tab-content pt-0">
                    <div class="tab-pane active" id="pil1">
                        <div class="row">
                            <div class="panel-collapse collapse show">

                            @php
                                $allusers = \App\User::with('points')->get()->sortByDesc(function($item){
                                    return $item->points()->sum('point');
                                })->take(5);

                                $no = 0;
                                $no2 = 0;
                            @endphp

                            @foreach($allusers as $user)
                                <!-- daftar nama -->
                                    <div class="row pt-1">
                                        <div class="col-md-1 col-sm-1 col-xs-1 p-0">
                                            {{ $no += 1 }}
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-3 p-0">
                                            <a href="social-learning-posts/{{ $user->username }}">
                                                @if($user->photo === null)
                                                    <img src="/images/default.png" class="thumb-md" alt="friend">
                                                @else
                                                    <img src="/images/photo/{{ $user->photo }}" class="thumb-md"
                                                         alt="friend">
                                                @endif
                                            </a>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <a href="social-learning-posts/{{ $user->username }}">
                                                        <small class="p-0 text-secondary">
                                                            <b>{{ str_limit($user->fullname, 10) }}</b>
                                                        </small>
                                                    </a>
                                                    <br>
                                                    <small>update today</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-2 col-xs-2 p-0">
                                            <!-- <i class=""></i> -->
                                            <img src="/images/brand/HIJAU.png" class="img-rounded center-block"
                                                 height="25px" alt="point-saya">
                                            <small>{{ $user->points->sum('point') }}</small>
                                        </div>
                                    </div>
                                    <!-- end daftar nama -->
                                @endforeach

                            </div>
                        </div>
                    </div>

                    <!-- end All results tab -->


                    <!-- Users tab -->
                    <div class="tab-pane" id="pil2">
                        <div class="row">
                            <div id="pilih2" class="panel-collapse collapse show">
                            @foreach($allusers as $user)
                                <!-- daftar nama -->
                                    <div class="row pt-1">
                                        <div class="col-md-1 col-sm-1 col-xs-1 p-0">
                                            {{ $no2 += 1 }}
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-3 p-0">
                                            <a href="social-learning-posts/{{ $user->username }}">
                                                @if($user->photo === null)
                                                    <img src="/images/default.png" class="thumb-md" alt="friend">
                                                @else
                                                    <img src="/images/photo/{{ $user->photo }}" class="thumb-md"
                                                         alt="friend">
                                                @endif
                                            </a>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <a href="social-learning-posts/{{ $user->username }}">
                                                        <small class="p-0 text-secondary">
                                                            <b>{{ str_limit($user->fullname, 10) }}</b>
                                                        </small>
                                                    </a>
                                                    <br>
                                                    <small>update today</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-2 col-xs-2 p-0">
                                            <!-- <i class=""></i> -->
                                            <img src="/images/brand/HIJAU.png" class="img-rounded center-block"
                                                 height="25px" alt="point-saya">
                                            <small>{{ $user->points->sum('point') }}</small>
                                        </div>
                                    </div>
                                    <!-- end daftar nama -->
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!-- end Users tab -->

                </div>
            </div>

        </div>


    </div>

    <!-- end peringkta -->

    <div class="card">
        <div class="card-body">
            <h2 class="header-title"><b>Up Coming Event</b></h2>
        </div>
        <ul class="sortable-list taskList list-unstyled" id="completed">
            @foreach(App\EventSocialLearning::orderBy('id','DESC')->take(5)->get() as $eventIndex => $event)
                <li class="task-warning m-0" id="task14">
                    <div class="row">
                        <div class="col-md-3">
                            {{ $eventIndex+1 }}
                        </div>
                        <div class="col-md-9">
                            <small class="text-muted">{{ $event->title }}</small>
                        </div>
                    </div>
                </li>
            @endforeach

        </ul>

        <div class="card-body text-center">
            <a href="{{ url('/social-learning-events') }}" class="btn btn-primary">
                ADD EVENT
            </a>
        </div>

    </div>

    @php
        $point = Auth::user();
        $users = App\User::all()->take(7);
    @endphp

    <div class="card-box mt-4">
        <h4 class="m-t-0 m-b-20 header-title">
            <b>
                <a data-toggle="modal" data-target="#followersModal" href="" class="text-muted">Followers
                    <span class="text-muted">
                        ({{ $point->followers->count() }})
                    </span>
                </a>
            </b>
        </h4>
        <!-- Modal -->
        <div class="modal fade" id="followersModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-body mt-0">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Pengikut</h4>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Foto</th>
                                <th>Full Name</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $no = 1;?>

                            @foreach(Auth::user()->followers->take(8) as $search)
                                <tr>
                                    <td>{{ $no ++ }}</td>

                                    @if( $search->photo === null)
                                        <td><img src="/images/default.png" alt="" class="img-rounded center-block"
                                                 height="50px" width="50px"></td>
                                    @else
                                        <td><img src="/images/photo/{{ $search->photo }}" alt=""
                                                 class="img-rounded center-block" height="50px" width="50px"></td>
                                    @endif

                                    <td>{{ substr($search->fullname,0,16) }}</td>
                                    <td>
                                        <a href="social-learning-posts/{{$search->username}}">
                                            <button type="button" name="button" class="btn btn-primary">Profile</button>
                                        </a>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>


        <hr>

        <div class="friend-list">
            @foreach(Auth::user()->followers->take(8) as $user)
                <a href="social-learning-posts/{{ $user->username }}" style="color:black">
                    @if($user->photo === null)
                        {{-- <a href="/social-learning-posts/{{ $user->username }}"> --}}
                        <img src="/images/default.png" class="rounded-circle thumb-md" alt="friend">
                        <p style="color:black">{{ str_limit($user->username, 6) }}</p>
                        {{-- </a> --}}
                    @else
                        {{-- <a href="/social-learning-posts/{{ $user->username }}"> --}}
                        <img src="/images/photo/{{ $user->photo }}" class="rounded-circle thumb-md" alt="friend">
                        <p style="color:black">{{ str_limit($user->username, 6) }}</p>
                        {{-- </a> --}}
                    @endif
                </a>
            @endforeach

            @if(Auth::user()->followers->count() > 8)
                <a class="text-center">
                    <span class="extra-number">+{{ $point->followers->count() }}</span>
                </a>
            @endif
        </div>


    </div>

</div>
