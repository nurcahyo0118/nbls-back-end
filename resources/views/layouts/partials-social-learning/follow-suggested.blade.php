<div class="card-box bg-muted">
    <h5>People You May Follow</h5>
    <div class="row text-center">

        @php
            $users = App\User::where('role_id', '=', '5')->where('id', '<>', Auth::id())->whereNotIn('id', Auth::user()->followings)->inRandomOrder()->take(3)->get();
        @endphp

        @if($users != null)
            @foreach($users as $user)
                <div id="suggestion{{ $user->id }}" class="col-md-4 border card-box text-center">
                    @if($user->photo == null)
                        <img class="rounded-circle thumb-md" src="/images/users/avatar-1.jpg"
                             alt="Generic placeholder image">
                    @else
                        <img class="rounded-circle thumb-md" src="/images/photo/{{ $user->photo }}"
                             alt="Generic placeholder image">
                    @endif

                    <h6 class="text-primary">{{ str_limit($user->fullname, 15) }}</h6>
                    @if($user->skill == null)
                        <p>Belum Ada</p>
                    @else
                        <p>{{ str_limit($user->skill, 40) }}</p>
                    @endif

                    <div class="row text-center">
                        <div class="col-md-12 p-0">

                            @if($user->hobby ==null )
                                <small class="p-0 text-muted">Hobby : Belum ada.</small>
                            @else
                                <small class="p-0 text-muted">Hobby : {{ str_limit($user->hobby, 40) }}</small>
                            @endif

                            @include('layouts.partials-social-learning.follow')

                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
</div>
