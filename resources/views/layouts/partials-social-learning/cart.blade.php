{{--<li class="nav-item dropdown notification-list d-none d-md-none d-lg-block">--}}
    {{--<a class="nav-link dropdown-toggle arrow-none waves-light waves-effect" data-toggle="dropdown" href="#"--}}
       {{--role="button" aria-haspopup="false" aria-expanded="false">--}}
        {{--<i class="md-shopping-cart noti-icon"></i>--}}
        {{--<span class="badge badge-pink noti-icon-badge">{{ Auth::user()->carts()->count() + Auth::user()->package_carts->count() }}</span>--}}
    {{--</a>--}}
    {{--<div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-lg" aria-labelledby="Preview">--}}
        {{--<!-- item-->--}}
        {{--<div class="dropdown-item noti-title">--}}
            {{--<h5>--}}
                {{--<span class="badge badge-danger float-right">{{ Auth::user()->carts()->count() + Auth::user()->package_carts->count() }}</span>--}}
                {{--Keranjang--}}
            {{--</h5>--}}
        {{--</div>--}}

    {{--@foreach(Auth::user()->carts()->get() as $cart)--}}
        {{--<!-- item-->--}}
            {{--<a href="javascript:void(0);" class="dropdown-item notify-item">--}}
                {{--<div class="notify-icon bg-warning">--}}
                    {{--<i class="fa fa-book"></i>--}}
                {{--</div>--}}
                {{--<p class="notify-details">{{ $cart->title }}--}}
                    {{--<small class="text-muted">{{ $cart->author->fullname }}--}}
                    {{--</small>--}}
                {{--</p>--}}
            {{--</a>--}}
    {{--@endforeach--}}

    {{--@foreach(Auth::user()->package_carts as $package_cart)--}}
        {{--<!-- item-->--}}
            {{--<a href="javascript:void(0);" class="dropdown-item notify-item">--}}
                {{--<div class="notify-icon bg-warning">--}}
                    {{--<i class="fa fa-book"></i>--}}
                {{--</div>--}}
                {{--<p class="notify-details">{{ $package_cart->title }}--}}
                    {{--<small class="text-muted">{{ $package_cart->user->fullname }}</small>--}}
                {{--</p>--}}
            {{--</a>--}}
    {{--@endforeach--}}

    {{--<!-- All-->--}}
        {{--<a href="{{ route('social-learning-payment.index', $_SERVER['HTTP_HOST']) }}"--}}
           {{--class="dropdown-item notify-item notify-all">--}}
            {{--Checkout--}}
        {{--</a>--}}

    {{--</div>--}}
{{--</li>--}}

<li class="nav-item dropdown notification-list d-none d-md-none d-lg-block">
    <a class="nav-link dropdown-toggle arrow-none waves-light waves-effect" 
        onclick="loadCartData()"
        data-toggle="modal"
        data-target="#cartsModal">

        <i class="md-shopping-cart noti-icon"></i>
        <span class="badge badge-pink noti-icon-badge">{{ Auth::user()->carts()->count() + Auth::user()->package_carts->count() }}</span>
        
    </a>
</li>
