@php
    {{-- $broadcast = App\Broadcast::all(); --}}
    $broadcast = App\Broadcast::where('viewer', '=', Auth::user()->role->name)->get();

@endphp

@foreach($broadcast as $broadcast)
    <br>
    @if(date("Y-m-d") >= $broadcast->view_date && date("Y-m-d") <= $broadcast->close_date)

        @if(Auth::user()->role_id = '5' && $broadcast->viewer === 'Murid')
            <div class="alert alert-warning" role="alert">
                <div class="row">
                    <div class="col col-md-12">
                        <center>
                            <i class="fa fa-bullhorn fa-2x" aria-hidden="true"></i>
                        </center>
                        <br>
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Pemberitahuan !</strong> {{ $broadcast->message }} <br>
                        <i>
                            <small class="float-right"> Broadcast ini akan hilang pada {{ $broadcast->close_date }}..
                            </small>
                        </i>
                    </div>
                </div>
            </div>
        @endif

        @if(Auth::user()->role_id = '4' && $broadcast->viewer === 'Instruktur')
            <div class="alert alert-warning" role="alert">
                <div class="row">
                    <div class="col col-md-12">
                        <center>
                            <i class="fa fa-bullhorn fa-2x" aria-hidden="true"></i>
                        </center>
                        <br>
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Pemberitahuan !</strong> {{ $broadcast->message }} <br>
                        <i>
                            <small class="float-right"> Broadcast ini akan hilang pada {{ $broadcast->close_date }}..
                            </small>
                        </i>
                    </div>
                </div>
            </div>
        @endif

        @if(Auth::user()->role_id = '3' && $broadcast->viewer === 'Expert')
            <div class="alert alert-warning" role="alert">
                <div class="row">
                    <div class="col col-md-12">
                        <center>
                            <i class="fa fa-bullhorn fa-2x" aria-hidden="true"></i>
                        </center>
                        <br>
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Pemberitahuan !</strong> {{ $broadcast->message }} <br>
                        <i>
                            <small class="float-right"> Broadcast ini akan hilang pada {{ $broadcast->close_date }}..
                            </small>
                        </i>
                    </div>
                </div>
            </div>
        @endif

        @if(Auth::user()->role_id = '2' && $broadcast->viewer === 'Admin')
            <div class="alert alert-warning" role="alert">
                <div class="row">
                    <div class="col col-md-12">
                        <center>
                            <i class="fa fa-bullhorn fa-2x" aria-hidden="true"></i>
                        </center>
                        <br>
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Pemberitahuan !</strong> {{ $broadcast->message }} <br>
                        <i>
                            <small class="float-right"> Broadcast ini akan hilang pada {{ $broadcast->close_date }}..
                            </small>
                        </i>
                    </div>
                </div>
            </div>
        @endif

        @if(Auth::user()->role_id = '1' && $broadcast->viewer === 'SuperAdmin')
            <div class="alert alert-warning" role="alert">
                <div class="row">
                    <div class="col col-md-12">
                        <center>
                            <i class="fa fa-bullhorn" aria-hidden="true"></i>
                        </center>
                        <br>
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Pemberitahuan !</strong> {{ $broadcast->message }} <br>
                        <i>
                            <small class="float-right"> Broadcast ini akan hilang pada {{ $broadcast->close_date }}..
                            </small>
                        </i>
                    </div>
                </div>
            </div>
        @endif

    @endif
@endforeach
