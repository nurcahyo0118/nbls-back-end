<div class="modal fade" id="cartsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" style="color: darkslategray">Keranjang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div id="cartsList" class="col-sm-12 col-md-12 col-md-12 text-center">
                    <i style="color: darkslategray" class="fa fa-spinner fa-spin fa-5x"></i>
                </div>

                <div id="packageCartsList" class="col-sm-12 col-md-12 col-md-12 text-center"></div>
            </div>

            <div class="modal-footer">
                <a href="{{ route('social-learning-payment.index', $_SERVER['HTTP_HOST']) }}" class="btn btn-primary">Checkout</a>
            </div>
        </div>
    </div>
</div>