@php
    $birthday = Auth::user()->birth_date;
@endphp
@if(date('m-d') == substr($birthday,5,5))
    <div class="alert alert-default" role="alert">
        <div class="row">
            <div class="col col-md-12">
                <center>
                    <i class="fa fa-birthday-cake fa-2x" aria-hidden="true"></i>
                </center>
                <br>
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Happy birth Day!!</strong> Selamat ulang tahun <b>{{ Auth::user()->fullname }}</b>, semoga kamu
                                                   menjadi pribadi yang lebih baik
                                                   dan terus belajar lebih giat, Salam dari Team Kami. <br>
                <i>
                    <small class="float-right text-muted"> Pesan ini akan hilang 1x24 jam.</small>
                </i>
                                                   .
            </div>
        </div>
    </div>
@else
@endif
