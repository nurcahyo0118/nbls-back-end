<!-- Navigation Bar-->
<header id="topnav">
    <div class="topbar-main {{ Auth::user()->tenant->theme }}">
        <div class="container">

            <nav class="navbar navbar-expand-md navbar-dark clearfix">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                        data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
                        aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <a class="navbar-brand" href="/social-learning-posts">
                    @if(Auth::user()->tenant->company_logo === 0 )
                        <img src="/images/nbls-logo.png" alt="" height="30" class="logo-lg">
                        <img src="/images/nbls-logo.png" alt="" height="34" class="logo-sm">
                    @else
                        <img src="/images/photo/tenant/{{ Auth::user()->tenant->company_logo }}" alt="" height="30"
                             class="logo-lg">
                        <img src="/images/photo/tenant/{{ Auth::user()->tenant->company_logo }}" alt="" height="34"
                             class="logo-sm">
                    @endif
                </a>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                            <a class="nav-link" href="https://www.babastudio.com/kursus-online/how-it-works">Cara
                                                                                                             Penggunaan
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item active d-block d-none d-sm-block d-md-none d-sm-none">
                            <a class="nav-link" href="">Logout
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="/" id="navbarDropdownMenuLink"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Kursus
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="/social-learning-my-course">Kursus Saya</a>
                                <a class="dropdown-item" href="/social-learning-course">Semua Kursus</a>
                                <a class="dropdown-item" href="/social-learning-course-packages">Paket Kursus</a>
                            </div>

                        </li>
                        <li class="nav-item">
                            <div class="input-group">
                                <form action="/social-learning-search-friend/update" method="post" class="form-inline"
                                      enctype="multipart/form-data" data-parsley-validate>

                                    {{ csrf_field() }}

                                    <input type="hidden" name="_method" value="put">
                                    <input type="text" class="form-control" name="name" placeholder="cari teman ..."
                                           required>
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-default form-control" type="button">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </form>
                            </div>
                        </li>
                    </ul>

                </div>
                <!--  End collapse-->

                <ul class="navbar-nav">
                    @include('layouts.partials-social-learning.add-friend-popup')
                    @include('layouts.partials-social-learning.cart')
                    @include('layouts.partials-social-learning.notification')
                    @include('layouts.partials-social-learning.icon')
                </ul>

            </nav>


        </div>
        <!-- end container -->
    </div>
    <!-- end topbar-main -->
</header>
<!-- End Navigation Bar -->


<!-- The Modal Logout -->
<div class="modal fade" id="logoutModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h6 class="modal-title">Share hasil pembelajaranmu hari ini yuk?</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <!-- FB -->
                <div class="fb-share-button" data-href="https://www.babastudio.com/blog" data-layout="button_count"
                     data-size="large" data-mobile-iframe="true">
                    <a target="_blank"
                       href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse"
                       class="fb-xfbml-parse-ignore">Bagikan
                    </a>
                </div>
                <!-- End FB -->
                <div class="btn btn-primary">
                    <span class="fa fa-twitter"></span>
                    <a class="text-white"
                       href="https://twitter.com/intent/tweet?text=Mau%20lihat%20hasil%20belajarku%20di%20babastudio,,%20ayo%20lihat%20dan%20gabung%20di%20www.babastudio.com%20."
                       data-size="large">
                        Tweet
                    </a>
                </div>

            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <form id="logout"
                      action="{{ !Auth::user()->tenant->is_cname ? route('tenant.logout', Auth::user()->tenant->subdomain) : route('logout', $_SERVER['HTTP_HOST']) }}"
                      method="post">
                    {{ csrf_field() }}

                    <input type="hidden" name="log" value="LOG_LOGOUT">

                </form>

                <a href="#!" onclick="event.preventDefault(); document.getElementById('logout').submit();"
                   class="btn btn-primary ">
                    <i class="zmdi zmdi-power"></i>
                    <span>Lain Kali</span>
                </a>
            </div>

        </div>
    </div>
</div>
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.12';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
