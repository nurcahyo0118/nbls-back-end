<div class="row">
    <div class="col-md-4 col-lg-3">
        <div class="profile-detail card-box">
            <div>
                @if(Auth::user()->photo === null)
                    <a href="/social-learning-profile">
                        <img src="/images/default.png" class="rounded-circle" alt="profile-image">
                    </a>
                @else
                    <a href="/social-learning-profile">
                        <img src="/images/photo/{{ Auth::user()->photo }}" class="rounded-circle" alt="profile-image">
                    </a>
                @endif

            <!-- Start Badge -->
                @php
                    $badge = Auth::user()->points->sum('point');
                @endphp
                @if($badge < 399)
                    <h5 class="text-info mb-1"><strong>NEWBIE</strong></h5>
                @elseif($badge >= 899 and $badge <= 2999)
                    <h5 class="text-info mb-1"><strong>TRAINED</strong></h5>
                @elseif($badge >= 2999 and $badge <= 6999)
                    <h5 class="text-info mb-1"><strong>SUPER</strong></h5>
                @elseif($badge > 6999 and $badge <= 10000)
                    <h5 class="text-info mb-1"><strong>GEEK</strong></h5>
                @elseif($badge > 10000 and $badge <= 18000)
                    <h5 class="text-info mb-1"><strong>FREAK</strong></h5>
                @elseif($badge >= 18000)
                    <h5 class="text-info mb-1"><strong>MASTER</strong></h5>
                @endif

                <a style="color:black;" href="/social-learning-profile">
                    <strong class="mb-0">{{ Auth::user()->fullname }}</strong><br>
                </a>
                <small class="text-muted mt-0"><p>{{ Auth::user()->skill }}</p></small>
                <hr>
            </div>

            <div id="accordion1" class="">
                <div class="container">
                    <div class="panel-heading">
                        <h6 class="panel-title" style="padding: 0;">
                            <a class="collapsed " href="{{ url('/posts') }}" data-toggle="collapse"
                               data-parent="#accordion1">

                                <h4 class="text-muted header-title text-left">
                                    <i class="text-primary fa fa-tachometer"></i>
                                    <a href="{{ url('/social-learning-posts') }}" class="text-muted"> My Dashboard</a>
                                </h4>

                            </a>

                        </h6>
                    </div>
                    <div class="panel-heading">
                        <h6 class="panel-title" style="padding: 0;">
                            <a class="collapsed" href="#course" data-toggle="collapse" data-parent="#accordion1">

                                <h4 class="text-muted header-title text-left">
                                    <i class="fa fa-book text-primary"></i>
                                    Course
                                </h4>

                            </a>

                        </h6>
                    </div>
                    <div id="course" class="panel-collapse collapse" style="height: 0px;">

                        <div class="timeline-content ml-2 text-left">
                            <div class="timeline-2">
                                <div class="time-item">
                                    <div class="item-info">
                                        <div class="text-primary">
                                            <small>
                                                <a href="/social-learning-my-course" class="text-muted">My Course
                                                </a>
                                            </small>
                                        </div>
                                    </div>
                                </div>

                                <div class="time-item">
                                    <div class="item-info">
                                        <div class="text-primary">
                                            <small>
                                                <a href="/social-learning-my-performance" class="text-muted">My
                                                                                                             Performance
                                                </a>
                                            </small>
                                        </div>
                                    </div>
                                </div>

                                <div class="time-item">
                                    <div class="item-info">
                                        <div class="text-primary">
                                            <small>
                                                <a href="/social-learning-exam" class="text-muted">Ujian</a>
                                            </small>
                                        </div>
                                    </div>
                                </div>

                                <div class="time-item">
                                    <div class="item-info">
                                        <div class="text-primary">
                                            <small>
                                                <a href="/social-learning-e-certificate" class="text-muted">
                                                    E-Certificate
                                                </a>
                                            </small>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="">
                        <div class="panel-heading">
                            <h6 class="panel-title" style="padding: 0;">
                                <a class="collapsed " href="#hasil" data-toggle="collapse" data-parent="#accordion1">

                                    <h4 class="text-muted header-title text-left">
                                        <i class="fa fa-certificate text-primary"></i>
                                        <a href="/social-learning-masterpiece" class="text-muted"> Hasil Karya</a>
                                    </h4>
                                    <!-- <span class="media-meta pull-right text-primary mt-2"> <i class="fa fa-chevron-right"></i> </span> -->
                                </a>

                            </h6>
                        </div>
                        <div id="hasil" class="panel-collapse collapse text-left" style="height: 0px;">
                            <span></span>
                        </div>
                    </div>

                    <div class="">
                        <div class="panel-heading">
                            <h6 class="panel-title" style="padding: 0;">
                                <a class="collapsed " href="#order" data-toggle="collapse" data-parent="#accordion1">

                                    <h4 class="text-muted header-title text-left">
                                        <i class="fa fa-undo text-primary"></i>
                                        <a href="/social-learning-order-history" class="text-muted"> Order History</a>
                                    </h4>
                                    <!-- <span class="media-meta pull-right text-primary mt-2"> <i class="fa fa-chevron-right"></i> </span> -->
                                </a>

                            </h6>
                        </div>
                        <div id="order" class="panel-collapse collapse text-left" style="height: 0px;">
                            <span></span>
                        </div>
                    </div>

                    <div class="">
                        <div class="panel-heading">
                            <h6 class="panel-title" style="padding: 0;">
                                <a class="collapsed " href="#point" data-toggle="collapse" data-parent="#accordion1">

                                    <h4 class="text-muted header-title text-left">
                                        <i class="fa fa-credit-card text-primary"></i>
                                        <a href="{{ url('/social-learning-exchange-point') }}" class="text-muted"> Tukar
                                                                                                                   Point
                                        </a>
                                    </h4>
                                    <!-- <span class="media-meta pull-right text-primary mt-2"> <i class="fa fa-chevron-right"></i> </span> -->
                                </a>

                            </h6>
                        </div>
                        <div id="point" class="panel-collapse collapse text-left" style="height: 0px;">
                            <span></span>
                        </div>
                    </div>

                    <div class="">
                        <div class="panel-heading">
                            <h6 class="panel-title" style="padding: 0;">
                                <a class="collapsed " href="#badge" data-toggle="collapse" data-parent="#accordion1">

                                    <h4 class="text-muted header-title text-left">
                                        <i class="fa fa-cog text-primary"></i>
                                        <a href="{{ url('/social-learning-my-badge') }}" class="text-muted">My Badge</a>
                                    </h4>
                                    <!-- <span class="media-meta pull-right text-primary mt-2"> <i class="fa fa-chevron-right"></i> </span> -->
                                </a>

                            </h6>
                        </div>
                        <div id="badge" class="panel-collapse collapse text-left" style="height: 0px;">
                            <span></span>
                        </div>
                    </div>

                    <div class="">
                        <div class="panel-heading">
                            <h6 class="panel-title" style="padding: 0;">
                                <a class="collapsed " href="#event" data-toggle="collapse" data-parent="#accordion1">

                                    <h4 class="text-muted header-title text-left">
                                        <i class="fa fa-calendar text-primary"></i>
                                        <a href="{{ url('/social-learning-events') }}" class="text-muted"> Events</a>
                                    </h4>
                                    <!-- <span class="media-meta pull-right text-primary mt-2"> <i class="fa fa-chevron-right"></i> </span> -->
                                </a>

                            </h6>
                        </div>
                        <div id="event" class="panel-collapse collapse text-left" style="height: 0px;">
                            <span></span>
                        </div>
                    </div>

                    <div>
                        <div class="panel-heading">
                            <h6 class="panel-title" style="padding: 0;">
                                <a class="collapsed " href="#booking" data-toggle="collapse" data-parent="#accordion1">

                                    <h4 class="text-muted header-title text-left">
                                        <i class="fa fa-list-ul text-primary"></i>
                                        <a href="/social-learning-booking" class="text-muted"> Booking</a>
                                    </h4>
                                    <!-- <span class="media-meta pull-right text-primary mt-2"> <i class="fa fa-chevron-right"></i> </span> -->
                                </a>

                            </h6>
                        </div>
                        <div id="booking" class="panel-collapse collapse text-left" style="height: 0px;">
                            <span></span>
                        </div>
                    </div>

                </div>

                <hr>

                @php
                    $tenant_users = [];
                    foreach(Auth::user()->tenant->users as $user)
                    {
                        array_push($tenant_users, $user->id);
                    }

                    $widget = App\SalesWidgetSocialLearning::whereIn('user_id', $tenant_users)->inRandomOrder()->first();

                @endphp

                @if($widget != null)

                    <h5>Sales Widget</h5>

                    <div class="card {{ $widget->card_background_color }}">

                        @if($widget->image != null)
                            <div href="{{ route('social-learning-course.show', [$_SERVER['HTTP_HOST'], $widget->course_id]) }}"
                                 style="overflow:hidden;">
                                <img src="/sales-widget-images/{{ $widget->image }}" style="width:100%; height:100%"
                                     alt="profile-image">
                            </div>
                        @endif
                        <div class="card-body">
                            {{--  {{ $widget->card_background_color }}  --}}
                            {{--  {{ $widget->course_id }}  --}}
                            <h5 class="text-card {{ $widget->card_text_color }}">{{ $widget->course->title }}</h5>

                            <a href="{{ route('social-learning-course.show', [$_SERVER['HTTP_HOST'], $widget->course_id]) }}"
                               class="btn {{ $widget->button_background_color }} {{ $widget->button_text_color }}">Lihat
                            </a>
                        </div>
                    </div>

                @endif


            </div>
        </div>
    </div>

    

    