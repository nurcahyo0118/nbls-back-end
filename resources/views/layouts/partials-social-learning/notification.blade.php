@php
    $notifications = \App\NotificationSocialLearning::where(['user_id' => Auth::id(), 'is_readed' => false])->latest()->get();
@endphp

<li class="nav-item dropdown notification-list d-none d-md-none d-lg-block" onclick="seeNotification()">
    <a class="nav-link dropdown-toggle arrow-none waves-light waves-effect" data-toggle="dropdown" href="#"
       role="button" aria-haspopup="false" aria-expanded="false">
        <i class="md-notifications noti-icon"></i>
        <span id="notificationCount" class="badge badge-pink noti-icon-badge">{{ $notifications->count() }}</span>
    </a>
    <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-lg" aria-labelledby="Preview">
        <!-- item-->
        <div class="dropdown-item noti-title">
            <h5>
                Notification
                <a href="javascript:void(0);">
                    <i class="fa fa-gear pull-right"></i>
                </a>
            </h5>
        </div>

    @foreach($notifications as $notification)
        <!-- item-->
            <a href="javascript:void(0);" class="dropdown-item notify-item">
                <div class="notify-icon bg-success">
                    <i class="icon-bubble"></i>
                </div>
                <p class="notify-details">
                    {{ $notification->title }}
                    <small class="text-muted">{{ $notification->created_at->diffForHumans() }}</small>
                </p>
            </a>
        @endforeach


    </div>
</li>
