<div id="followSuggested{{ $user->id }}" class="m-1 align-bottom">
    @if(Auth::user()->followings()->where(['following_id' => $user->id])->count() == 0)
        <a id="followUser" onclick="onFollow({{ $user->id }})">
            <button type="button" class="btn btn-primary" name="button">Follow</button>
        </a>
    @else
        <a id="followUser" onclick="onUnfollow({{ $user->id }})">
            <button type="button" class="btn btn-primary" name="button">Unfollow</button>
        </a>
    @endif
    <a href="/social-learning-posts/{{ $user->username }}">
        <button type="button" class="btn btn-muted" name="button">Prolife</button>
    </a>
</div>
