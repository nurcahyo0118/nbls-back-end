<footer class="footer {{Auth::user()->tenant->theme}} m-t-50">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h5 class="text-white">NBLS</h5>
                @if(Auth::user()->tenant->company_logo === 0 )
                    <img src="/images/nbls-logo.png" alt="" height="34" class="img-responsive">
                @else
                    <img src="/images/photo/tenant/{{ Auth::user()->tenant->company_logo }}" alt="" height="34"
                         class="img-responsive">
                @endif
                <br>
                <p class="mt-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                <span class="fa fa-phone mt-1 text-muted"> 021 121392 2424</span>
                <br>
                <span class="fa fa-envelope mt-1 text-muted"> admin@babastudio.com</span>

            </div>

            <div class="col-md-3">
                <h5 class="text-white">LINK MAP</h5>
                <ul>
                    <li class="mt-2">
                        <a href="/">Beranda</a>
                    </li>
                    <li class="mt-2">
                        <a href="/">Tentang Kami</a>
                    </li>
                    <li class="mt-2">
                        <a href="/">Cara Pengunaan</a>
                    </li>
                    <li class="mt-2">
                        <a href="/">Kursus</a>
                    </li>
                    <li class="mt-2">
                        <a href="/">Persyaratan Pengguna</a>
                    </li>
                    <li class="mt-2">
                        <a href="/">Kebijakan Privasi</a>
                    </li>
                    <li class="mt-2">
                        <a href="/">Blog</a>
                    </li>
                </ul>
            </div>

            <div class="col-md-3">
                <h5 class="text-white">GET NEWSLETTER</h5>
                <p>Above the law sunny days sweeping the clouds away lateful trip where.</p>
                <!-- <input type="text" name="" class="form-control" value="" placeholder="Your email address"><button class="btn btn-primary"><span class="fa fa-send"></span></button> -->
                <div class="input-group">

                    <input type="text" class="form-control">
                    <span class="input-group-btn">
                        <button class="btn btn-default form-control" type="button">
                            <i class="fa fa-send"></i>
                        </button>
                    </span>
                </div>

                <p>
                    Your information are safe with us.
                </p>
                <button type="button" class="btn btn-facebook waves-effect waves-light">
                    <i class="fa fa-facebook"></i>
                </button>

                <button type="button" class="btn btn-twitter waves-effect waves-light">
                    <i class="fa fa-twitter"></i>
                </button>

                <button type="button" class="btn btn-danger waves-effect waves-light">
                    <i class="fa fa-google-plus"></i>
                </button>


            </div>
            <div class="col-md-3">
                <h5 class="text-white">OUR INSTRUCTOR</h5>
                @php

                    $users = App\User::where('role_id', '4')->inRandomOrder()->take(9)->get();

                @endphp

                @foreach($users as $user)
                    @if( $user->photo === null)
                        <img src="/images/users/avatar-1.jpg" alt="user" class="img-responsive m-1" width="70px"
                             height="70">
                    @else
                        <img src="/images/photo/{{ $user->photo }}" alt="user" class="img-responsive m-1" width="70px"
                             height="70">
                    @endif
                @endforeach
            </div>
        </div>
    </div>
    <!-- End Of container -->

    <hr>
    <div class="container">
        <div class="row">
            <p>Copyright 2019. NBLS Hybirrd Learning All Right Reserverd.</p>
        </div>
    </div>
</footer>
<!-- End Footer -->
