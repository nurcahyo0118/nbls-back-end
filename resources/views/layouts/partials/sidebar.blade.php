<aside class="main-sidebar">

    <section class="sidebar">

        <div class="user-panel">
            <div class="pull-left image">
                @if( Auth::user()->photo === null)
                    <img src="/images/default.png" class="img-circle" alt="User Image">
                @else
                    <img src="/images/photo/{{ Auth::user()->photo }}" class="img-circle" alt="User Image">
                @endif
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->fullname }}</p>
                <a>
                    <i class="fa fa-circle text-success"></i>
                    Online
                </a>
            </div>
        </div>

        @if($permission != null)
            <ul class="sidebar-menu">
                <li>
                    <a href="{{ route('dashboard', '') }}">
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('social-learning-posts.index') }}" target="_blank">
                        <i class="fa fa-globe"></i>
                        <span>Social Learning</span>
                    </a>
                </li>

                <li>
                    <a href="/broadcast">
                        <i class="fa fa-bullhorn"></i>
                        <span>Broadcast</span>
                    </a>
                </li>

                <li class="header"> Kelola User</li>

                @if($permission->read_student_menu)
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-users"></i>
                            <span>Murid</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li>
                                <a href="{{ route('instructor.students', $_SERVER['HTTP_HOST']) }}">
                                    <i class="fa fa-circle-o"></i>
                                    Daftar Murid
                                </a>
                            </li>

                            {{--  <li>
                                <a href="{{ route('users.index', '') }}">
                                    <i class="fa fa-circle-o"></i>
                                    Daftar Murid Terbaik
                                </a>
                            </li>  --}}

                            <li>
                                <a href="{{ route('instructor.essays', $_SERVER['HTTP_HOST']) }}">
                                    <i class="fa fa-circle-o"></i>
                                    Jawaban Essay (Diskusi)
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('instructor.quizzes', $_SERVER['HTTP_HOST']) }}">
                                    <i class="fa fa-circle-o"></i>
                                    Jawaban Kuis
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('instructor.exercises', $_SERVER['HTTP_HOST']) }}">
                                    <i class="fa fa-circle-o"></i>
                                    Daftar Latihan
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('instructor.encounters', $_SERVER['HTTP_HOST']) }}">
                                    <i class="fa fa-circle-o"></i>
                                    Daftar Encounter
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('instructor.video-comments', $_SERVER['HTTP_HOST']) }}">
                                    <i class="fa fa-circle-o"></i>
                                    Komentar Video
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('instructor.students.performance', $_SERVER['HTTP_HOST']) }}">
                                    <i class="fa fa-circle-o"></i>
                                    Kinerja Murid
                                </a>
                            </li>

                        </ul>
                    </li>
                @endif

                @if($permission->read_user)
                    <li class="header"> Kelola User</li>

                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-users"></i>
                            <span>User</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li>
                                <a href="{{ route('users.index', '') }}">
                                    <i class="fa fa-circle-o"></i>
                                    Semua
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('users.role', [$_SERVER['HTTP_HOST'], 1]) }}">
                                    <i class="fa fa-circle-o"></i>
                                    SuperAdmin
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('users.role', [$_SERVER['HTTP_HOST'], 2]) }}">
                                    <i class="fa fa-circle-o"></i>
                                    Management
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('users.role', [$_SERVER['HTTP_HOST'], 3]) }}">
                                    <i class="fa fa-circle-o"></i>
                                    Expert
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('users.role', [$_SERVER['HTTP_HOST'], 4]) }}">
                                    <i class="fa fa-circle-o"></i>
                                    Instructor
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('users.role', [$_SERVER['HTTP_HOST'], 5]) }}">
                                    <i class="fa fa-circle-o"></i>
                                    Student
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif

                @if($permission->read_role)
                    <li>
                        <a href="{{ route('roles.index', '') }}">
                            <i class="fa fa-key"></i>
                            <span>Roles</span>
                        </a>
                    </li>
                @endif

                @if($permission->read_course || $permission->read_category)
                    <li class="header"> Kursus</li>
                @endif

                @if($permission->read_course)
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-book"></i>
                            <span>Kursus</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li>
                                <a href="{{ route('courses.index', $_SERVER['HTTP_HOST']) }}">
                                    <i class="fa fa-circle-o"></i>
                                    <span>Kursus</span>
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('course-packages.index', $_SERVER['HTTP_HOST']) }}">
                                    <i class="fa fa-circle-o"></i>
                                    <span>Paket Kursus</span>
                                </a>
                            </li>

                            @if($permission->read_category)
                                <li>
                                    <a href="{{ route('categories.index', $_SERVER['HTTP_HOST']) }}">
                                        <i class="fa fa-circle-o"></i>
                                        <span>Kategori</span>
                                    </a>
                                </li>
                            @endif

                            <li>
                                <a href="{{ route('encounters.index', $_SERVER['HTTP_HOST']) }}">
                                    <i class="fa fa-circle-o"></i>
                                    <span>Encounter</span>
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('video-libraries.index', $_SERVER['HTTP_HOST']) }}">
                                    <i class="fa fa-circle-o"></i>
                                    <span>Video Library</span>
                                </a>
                            </li>

                            {{--  <li>
                                <a href="/certificate">
                                    <i class="fa fa-circle-o"></i>
                                    <span>Certificate</span>
                                </a>
                            </li>  --}}

                            <li>
                                <a href="{{ route('classes.index', $_SERVER['HTTP_HOST']) }}">
                                    <i class="fa fa-circle-o"></i>
                                    <span> Daftar Kelas</span>
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('reviews.index') }}">
                                    <i class="fa fa-circle-o"></i>
                                    <span>Ulasan</span>
                                </a>
                            </li>

                        </ul>
                    </li>

                @endif

                @if($permission->read_tenant_me)
                    @if(Auth::user()->role_id === 1)
                        <li class="header"> Kelola Tenant</li>
                        <li>
                            <a href="{{ route('tenants.me', $_SERVER['HTTP_HOST']) }}">
                                <i class="fa fa-building"></i>
                                <span> Kelola Tenant</span>
                            </a>
                        </li>
                    @endif
                @endif

                @if($_SERVER['HTTP_HOST'] == env('HOST_NAME') && Auth::user()->role_id == 1)
                    <li class="header"> Tenant</li>
                    <li>
                        <a href="{{ route('tenants.index', $_SERVER['HTTP_HOST']) }}">
                            <i class="fa fa-building"></i>
                            <span> Daftar Tenant</span>
                        </a>
                    </li>
                @endif

                <li class="header"> Halaman</li>
                <li>
                    <a href="{{ route('pages.index', $_SERVER['HTTP_HOST']) }}">
                        <i class="fa fa-file"></i>
                        <span> Daftar Halaman</span>
                    </a>
                </li>

                <li class="header"> I-Time</li>
                <li>
                    <a href="{{ route('i-times.index', $_SERVER['HTTP_HOST']) }}">
                        <i class="fa fa-comments"></i>
                        <span> I-Time</span>
                    </a>
                </li>

                <li class="header"> Keuangan</li>
                <li>
                    <a href="{{ route('finances.index', $_SERVER['HTTP_HOST']) }}">
                        <i class="fa fa-money"></i>
                        <span> Laporan Keuangan</span>
                    </a>
                </li>

                <li class="header"> Log</li>
                <li>
                    <a href="{{ route('logs.index', $_SERVER['HTTP_HOST']) }}">
                        <i class="fa fa-list"></i>
                        <span> Log</span>
                    </a>
                </li>

                <li class="header"> Widget</li>
                <li>
                    <a href="{{ route('social-learning-sales-widget.index', $_SERVER['HTTP_HOST']) }}">
                        <i class="fa fa-list"></i>
                        <span> Sales Widget</span>
                    </a>
                </li>

                {{-- <li class="header"> Voucher</li>
                <li>
                    <a href="{{ route('vouchers.index', $_SERVER['HTTP_HOST']) }}">
                        <i class="fa fa-ticket"></i>
                        <span> Voucher</span>
                    </a>
                </li> --}}

                <li class="header"> Pengaturan</li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-gear"></i>
                        <span> Pengaturan</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="{{ route('me', $_SERVER['HTTP_HOST']) }}">
                                <i class="fa fa-circle-o"></i>
                                Profil
                            </a>
                        </li>

                        {{-- <li>
                            <a href="{{ route('notification.setting') }}">
                                <i class="fa fa-circle-o"></i>
                                Setting
                            </a>
                        </li> --}}
                    </ul>
                </li>

                <form id="logout"
                      action="{{ Auth::user()->tenant->is_cname ? route('logout', $_SERVER['HTTP_HOST']) : route('tenant.logout', Auth::user()->tenant->subdomain) }}"
                      method="post">

                    {{ csrf_field() }}

                    <input type="hidden" name="log" value="LOG_LOGOUT">

                </form>

                {{--  @if(Auth::user()->tenant->is_cname)  --}}
                <li>
                    <a href="#" onclick="event.preventDefault(); document.getElementById('logout').submit();">
                        <i class="fa fa-sign-out"></i>
                        <span> Keluar</span>
                    </a>
                </li>
                {{--  @endif  --}}
            </ul>
    @endif
    {{-- Multi Domain --}}

</aside>
