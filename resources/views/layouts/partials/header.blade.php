<header class="main-header">
@php
    $header_notifications = \App\NotificationSocialLearning::where(['user_id' => Auth::id(), 'is_readed' => false])->latest()->get();
@endphp
<!-- Logo -->
    <a href="#!" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>BS</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Baba Studio</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown notifications-menu" onclick="seeNotification()">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                        <span id="notificationCount"
                              class="label label-warning">{{ $header_notifications->count() }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu">
                                @foreach($header_notifications as $notification)
                                    <li>
                                        <a href="#!">
                                            <i class="fa fa-users text-aqua"></i> {{ $notification->title }}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                        <li class="footer">
                            <a href="{{ route('notification.show.all') }}">View all</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
