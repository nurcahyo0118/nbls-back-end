<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8"/>
    <title>Babastudio</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description"/>
    <meta content="Coderthemes" name="author"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- App css -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/ css /bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous"> -->
    <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/css/icons.css" rel="stylesheet" type="text/css"/>
    <link href="/css/style.css" rel="stylesheet" type="text/css"/>

    <!-- <script src="/js/modernizr.min.js"></script> -->

    <base href="/">

    <script type="text/javascript">
        var baseUrl = "{{ url('/') }}";
    </script>

    <style>
        .post-icon {
            margin-left: 6px;
            margin-right: 6px;
        }

        .bottom-align-text {
            position: absolute;
            bottom: 0;
            right: 0;
        }

        a {
            cursor: pointer;
        }

        button {
            cursor: pointer;
        }

        {{--  Card Color  --}}

        .card.card-default {
            background-color: #CFD8DC !important;
        }

        .card.card-primary {
            background-color: #BBDEFB !important;
        }

        .card.card-info {
            background-color: #B3E5FC !important;
        }

        .card.card-warning {
            background-color: #FFCCBC !important;
        }

        .card.card-danger {
            background-color: #ffcdd2 !important;
        }

        {{--  Button Color  --}}

        .btn.text-default {
            color: #FFF !important;
        }

        .btn.text-primary {
            color: #1565C0 !important;
        }

        .btn.text-info {
            color: #0277BD !important;
        }

        .btn.text-warning {
            color: #D84315 !important;
        }

        .btn.text-danger {
            color: #c62828 !important;
        }

        {{--  Card Text Color  --}}

        .text-card.text-default {
            color: #FFF !important;
        }

        .text-card.text-primary {
            color: #1565C0 !important;
        }

        .text-card.text-info {
            color: #0277BD !important;
        }

        .text-card.text-warning {
            color: #D84315 !important;
        }

        .text-card.text-danger {
            color: #c62828 !important;
        }

    </style>

    @yield('style')

</head>


<body>

@include('layouts.partials-social-learning.header')

@include('layouts.partials-social-learning.carts')


<div class="wrapper">
    <div class="container">
        <div class="row">

            @include('layouts.partials-social-learning.left-sidebar')

            @yield('content')

            <div class="footer-space" style="margin-top:1000px;">

            </div>

            @include('layouts.partials-social-learning.right-sidebar')

        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
</div>
<!-- end wrapper -->


@include('layouts.partials-social-learning.footer')





<!-- jQuery  -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

<script src="/js/jquery.min.js"></script>
<script src="/js/popper.min.js"></script>
<!-- Popper for Bootstrap -->
<script src="/js/bootstrap.min.js"></script>
<script src="/js/waves.js"></script>
<script src="/js/jquery.slimscroll.js"></script>
<script src="/js/jquery.scrollTo.min.js"></script>

<!-- App js -->
<!-- <script src="/js/jquery.core.js"></script>
<script src="/js/jquery.app.js"></script> -->

<script type="text/javascript">

    var baseUrl = "{{ url('/') }}";

</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.8.0/parsley.min.js"></script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<script>
    function loadCartData()
    {
        var url = '/loads/carts';

        $.ajax({
            type: 'GET',
            url: url,
            success: function (response) {
                $('#cartsList').replaceWith(response.view);
            },
            error: function (error) {
                console.log('ddddd');
                $('#cartsList').replaceWith('<div id="cartsList" class="col-sm-12 col-md-12 col-md-12 text-center" style="color: #424242">Kosong !</div>');
            }
        });

        var packageUrl = '/loads/package-carts';

        $.ajax({
            type: 'GET',
            url: packageUrl,
            success: function (response) {
                $('#packageCartsList').replaceWith(response.view);
            },
            error: function (error) {
                console.log('ddddd');
                $('#cartsList').replaceWith('<div id="cartsList" class="col-sm-12 col-md-12 col-md-12 text-center" style="color: #424242">Kosong !</div>');
            }
        });

    }

    function removeCartItem(courseId)
    {
        var url = '/carts/' + courseId + '/remove';

        $.ajax({
            type: 'DELETE',
            url: url,
            success: function (response) {
                $('#cartsList').replaceWith(response.view);
            },
            error: function (error) {
                $('#cartsList').replaceWith('<div id="cartsList" class="col-sm-12 col-md-12 col-md-12 text-center" style="color: #424242">Kosong !</div>');
            }
        });

    }

    function removePackageCartItem(courseId)
    {
        var url = '/package-carts/' + courseId + '/remove';

        $.ajax({
            type: 'DELETE',
            url: url,
            success: function (response) {
                $('#packageCartsList').replaceWith(response.view);
            },
            error: function (error) {
                $('#cartsList').replaceWith('<div id="cartsList" class="col-sm-12 col-md-12 col-md-12 text-center" style="color: #424242">Kosong !</div>');
            }
        });

    }
</script>

{{--  <script>
    $(function()
    {
        console.log('Heeeeee');
    })
</script>  --}}

<script>
    function seeNotification() {
        console.log('Notification Seen');
        $.ajax({
            url: '/notifications/see',
            type: 'POST',
            data: null,
            success: function (data) {
                console.log(data);
                $('#notificationCount').replaceWith('<span id="notificationCount" class="badge badge-pink noti-icon-badge">0</span>');
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
</script>

@yield('script')

@include('layouts.partials-social-learning.message')


</body>

</html>
