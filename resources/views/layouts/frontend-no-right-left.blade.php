<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8"/>
    <title>Babastudio</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description"/>
    <meta content="Coderthemes" name="author"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- App css -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous"> -->
    <link rel="stylesheet" href="/css/stars.css">

    <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/css/icons.css" rel="stylesheet" type="text/css"/>
    <link href="/css/style.css" rel="stylesheet" type="text/css"/>

    <!-- <script src="/js/modernizr.min.js"></script> -->

    <base href="/">

    <script type="text/javascript">
        var baseUrl = "{{ url('/') }}";
    </script>

    <style>
        button {
            cursor: pointer !important;
        }
    </style>

    @yield('style')

</head>


<body>

@include('layouts.partials-social-learning.header')

@include('layouts.partials-social-learning.carts')


<div class="wrapper">
    <div class="container footer-space">


    @yield('content')


    <!-- end row -->
    </div>
    <!-- end container -->
</div>
<!-- end wrapper -->


@include('layouts.partials-social-learning.footer')

<!-- jQuery  -->
<script src="/js/jquery.min.js"></script>
<script src="/js/popper.min.js"></script>
<!-- Popper for Bootstrap -->
<script src="/js/bootstrap.min.js"></script>
<script src="/js/waves.js"></script>
<script src="/js/jquery.slimscroll.js"></script>
<script src="/js/jquery.scrollTo.min.js"></script>

<!-- App js -->
<script src="/js/jquery.core.js"></script>
<script src="/js/jquery.app.js"></script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<script>
    function seeNotification() {
        console.log('Notification Seen');
        $.ajax({
            url: '/notifications/see',
            type: 'POST',
            data: null,
            success: function (data) {
                console.log(data);
                $('#notificationCount').replaceWith('<span id="notificationCount" class="badge badge-pink noti-icon-badge">0</span>');
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
</script>

@yield('script')

@include('layouts.partials-social-learning.message')

</body>

</html>
