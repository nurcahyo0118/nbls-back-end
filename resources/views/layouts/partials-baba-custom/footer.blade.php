<!-- Footer -->
<section class="section-footer" id="footer">
    <div class="container">
        <div class="row text-center text-xs-center text-sm-left text-md-left">
            <div class="col-xs-12 col-sm-4 col-md-3">
                <img src="/baba-custom/images/logo.jpg" class="img-fluid">
                <p class="f-white">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                   Lorem Ipsum has been the industry's
                                   standard dummy text ever since the 1500s,
                                   when an unknown printer took a galley of type and scrambled
                                   it to make a type specimen book.
                </p>
                <ul class="list-unstyled quick-links">
                    <li>
                        <a href="javascript:void();">
                            <i class="fa fa-phone"></i>
                            (021) 5366 4008
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void();">
                            <i class="fa fa-envelope"></i>
                            info@babastudio.com
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3">
                <h5>LINK MAP</h5>
                <ul class="list-unstyled quick-links">
                    <li>
                        <a href="javascript:void();">
                            <i class="fa fa-circle"></i>
                            Home
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void();">
                            <i class="fa fa-circle"></i>
                            About
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void();">
                            <i class="fa fa-circle"></i>
                            FAQ
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void();">
                            <i class="fa fa-circle"></i>
                            Get Started
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void();">
                            <i class="fa fa-circle"></i>
                            Videos
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3">
                <h5>GET NEWSLETTER</h5>
                <p class="f-white">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                   Lorem Ipsum has been the industry's
                </p>
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="" aria-describedby="basic-addon2">
                    <span class="input-group-addon" id="basic-addon2">
                        <i class="fa fa-paper-plane" aria-hidden="true"></i>
                    </span>
                    <br/>
                    <!-- <h6 class="f-white f-slim">Yout Information are safe with us </h6> -->
                </div>
                <p class="f-white">Yout Information are safe with us
                </p>
                <div id="social">
                    <a class="facebookBtn smGlobalBtn" href="#"></a>
                    <a class="twitterBtn smGlobalBtn" href="#"></a>
                    <a class="googleplusBtn smGlobalBtn" href="#"></a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3">
                <h5>OUR INSTRUCTOR</h5>
                @php

                    $users = App\User::where('role_id', '4')->inRandomOrder()->take(9)->get();

                @endphp

                @foreach($users as $user)
                    @if( $user->photo === null)
                        <div class="col-xs-3 float-left mr-1 mt-1"><img src="/images/users/avatar-1.jpg" width="70em">
                        </div>
                    @else
                        <div class="col-xs-3 float-left mr-1 mt-1"><img src="/images/photo/{{ $user->photo }}"
                                                                        width="70em"></div>
                    @endif
                @endforeach
            </div>
        </div>
        <hr
        / style="border:1px solid #152e48;">
        <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <p class="h6 float-left">&copy All right Reversed.
                    <a class="text-green ml-2" href="" target="_blank">BABASTUDIO</a>
                </p>
            </div>
            <div class="credits" style="font-size:5px; color:#ccc; opacity: 0.2; float:right;">
                Best
                <a href="https://bootstrapmade.com/">Bootstrap Templates</a>
                by BootstrapMade
            </div>
        </div>
        </hr>
    </div>
</section>
<!-- end Footer -->

<!-- Bootstrap core JavaScript -->
<script src="/baba-custom/vendor/jquery/jquery.min.js"></script>
<script src="/baba-custom/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- custom Javascript -->
<script src="/baba-custom/js/custom.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>