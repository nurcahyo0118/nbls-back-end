@php
    $tenant = NULL;

    $tenant = App\Tenant::where(['domain' => $_SERVER['HTTP_HOST']])->first();

    if($tenant === null)
    {
      $explode = explode('.', $_SERVER['HTTP_HOST']);
      $subdomain = array_shift($explode);

      $tenant = App\Tenant::where('subdomain', $subdomain)->first();
    }

@endphp
        <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>NBLS - Babastudio</title>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link href="/baba-custom/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/baba-custom/css/custom.css" rel="stylesheet">
    <link href="/baba-custom/lib/animate/animate.min.css" rel="stylesheet">
    <link href="/baba-custom/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="/baba-custom/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="/baba-custom/lib/lightbox/css/lightbox.min.css" rel="stylesheet">
    <link href="/baba-custom/css/style.css" rel="stylesheet">

    @yield('style')

</head>
<body>
<!-- topbar -->
<div class="container-fluid bg-blue top-bar f-white pl-5 pb-2">
    <div class="row">
        <div class="col-md-8 col-sm-12">
            <div class="ml-4">
                <i class="fa fa-phone ml-5 float-sm-left mt-1"></i>
                + Call Us : 021 - 5366 4008
            </div>
        </div>
        <div class="col-md-4 col-sm-12">
            <div class="ml-2 tentang">
                <a href="#" class="f-white mr-2">Tentang Kami</a>
                <span>|
                    <span>
                        <i class="fa fa-user ml-2"></i>
                        <a href="/login" class="mr-1 link">Login</a>
                        or
                        <a href="/register" class="mr-1 link">Register</a>
                        <span>
                            <i class="fa fa-shopping-cart"></i>
                        </span>
            </div>
        </div>
    </div>
</div>
<!-- end topbar -->
<!-- navbar -->
<nav class="navbar  navbar-expand-lg navbar-dark bg-black navbar-bootbites" data-toggle="sticky-onscroll">
    <div class="container">
        <a class="navbar-brand" href="/"><img src="/baba-custom/images/babastudio.png" width="200em" class="img-fluid">
        </a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/">BERANDA</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/social-learning-course-front-end">KURSUS ONLINE</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="http://techfor.id/baba-custom/how-it-works.php">CARA PENGGUNAAN</a>
                </li>
                {{-- <li class="nav-item">
                  <a class="nav-link" href="#">KUPON DISKON</a>
                </li> --}}
                <li class="nav-item">
                    <a class="nav-link" href="/register">MULAI BELAJAR</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- end navbar -->


@yield('content')



<!-- Footer -->
<section class="section-footer" id="footer">
    <div class="container">
        <div class="row text-center text-xs-center text-sm-left text-md-left">
            <div class="col-xs-12 col-sm-4 col-md-3">
                <img src="/baba-custom/images/logo.jpg" class="img-fluid">
                <p class="f-white">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                   Lorem Ipsum has been the industry's
                                   standard dummy text ever since the 1500s,
                                   when an unknown printer took a galley of type and scrambled
                                   it to make a type specimen book.
                </p>
                <ul class="list-unstyled quick-links">
                    <li>
                        <a href="javascript:void();">
                            <i class="fa fa-phone"></i>
                            (021) 5366 4008
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void();">
                            <i class="fa fa-envelope"></i>
                            info@babastudio.com
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3">
                <h5>LINK MAP</h5>
                <ul class="list-unstyled quick-links">
                    <li>
                        <a href="javascript:void();">
                            <i class="fa fa-circle"></i>
                            Home
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void();">
                            <i class="fa fa-circle"></i>
                            About
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void();">
                            <i class="fa fa-circle"></i>
                            FAQ
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void();">
                            <i class="fa fa-circle"></i>
                            Get Started
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void();">
                            <i class="fa fa-circle"></i>
                            Videos
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3">
                <h5>GET NEWSLETTER</h5>
                <p class="f-white">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                   Lorem Ipsum has been the industry's
                </p>
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="" aria-describedby="basic-addon2">
                    <span class="input-group-addon" id="basic-addon2">
                        <i class="fa fa-paper-plane" aria-hidden="true"></i>
                    </span>
                    <br/>
                    <!-- <h6 class="f-white f-slim">Yout Information are safe with us </h6> -->
                </div>
                <p class="f-white">Yout Information are safe with us
                </p>
                <div id="social">
                    <a class="facebookBtn smGlobalBtn" href="#"></a>
                    <a class="twitterBtn smGlobalBtn" href="#"></a>
                    <a class="googleplusBtn smGlobalBtn" href="#"></a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3">
                <h5>OUR INSTRUCTOR</h5>
                @php

                    $users = App\User::where('role_id', '4')->inRandomOrder()->take(9)->get();

                @endphp

                @foreach($users as $user)
                    @if( $user->photo === null)
                        <div class="col-xs-3 float-left mr-1 mt-1"><img src="/images/users/avatar-1.jpg" width="70em">
                        </div>
                    @else
                        <div class="col-xs-3 float-left mr-1 mt-1"><img src="/images/photo/{{ $user->photo }}"
                                                                        width="70em"></div>
                    @endif
                @endforeach
            </div>
        </div>
        <hr
        / style="border:1px solid #152e48;">
        <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <p class="h6 float-left">&copy All right Reversed.
                    <a class="text-green ml-2" href="" target="_blank">BABASTUDIO</a>
                </p>
            </div>
            <div class="credits" style="font-size:5px; color:#ccc; opacity: 0.2; float:right;">
                Best
                <a href="https://bootstrapmade.com/">Bootstrap Templates</a>
                by BootstrapMade
            </div>
        </div>
        </hr>
    </div>
</section>
<!-- end Footer -->
<script src="/baba-custom/vendor/jquery/jquery.min.js"></script>
<script src="/baba-custom/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/baba-custom/js/custom.js"></script>
<script src="/baba-custom/lib/jquery/jquery-migrate.min.js"></script>
<script src="/baba-custom/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/baba-custom/lib/easing/easing.min.js"></script>
<script src="/baba-custom/lib/superfish/hoverIntent.js"></script>
<script src="/baba-custom/lib/superfish/superfish.min.js"></script>
<script src="/baba-custom/lib/wow/wow.min.js"></script>
<script src="/baba-custom/lib/waypoints/waypoints.min.js"></script>
<script src="/baba-custom/lib/counterup/counterup.min.js"></script>
<script src="/baba-custom/lib/owlcarousel/owl.carousel.min.js"></script>
<script src="/baba-custom/lib/isotope/isotope.pkgd.min.js"></script>
<script src="/baba-custom/lib/lightbox/js/lightbox.min.js"></script>
<script src="/baba-custom/lib/touchSwipe/jquery.touchSwipe.min.js"></script>
<script src="/baba-custom/js/main.js"></script>
@yield('script')
</body>
</html>
        