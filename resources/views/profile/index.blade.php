@extends('layouts.main')

@section('content')

    <div class="row">

        <div class="col-md-12">
            @include('layouts.partials.message')
        </div>

        <div class="col-md-4">


            <div class="box box-primary">

                <div class="box-header">

                </div>

                <div class="box-body">
                    <div class="col-md-12 text-center">
                        @if( Auth::user()->photo === null)
                            <img src="/images/default.png" class="img-responsive" alt="">
                        @else
                            <img src="/images/photo/{{ Auth::user()->photo }}" class="img-responsive" width="400"
                                alt="">
                        @endif
                    </div>
                    <div class="col-md-12">
                        <h2>{{ Auth::user()->name }}</h2>
                    </div>

                </div>

                <div class="modal fade" id="modalAdd">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="defaultModalLabel">Edit Foto Profil</h4>
                            </div>
                            <div class="modal-body">
                                <form action="{{ route('me.update.image', $_SERVER['HTTP_HOST']) }}"
                                    method="post"
                                    enctype="multipart/form-data" data-parsley-validate>

                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    <input type="hidden" name="_method" value="put"/>

                                    <fieldset>
                                        <div class="col-md-12">
                                            <label>Berkas ( Gambar ) : </label>
                                            <div class="form-group">
                                                <div class="btn btn-default btn-file">
                                                    <i class="fa fa-paperclip"></i>
                                                    Upload Gambar
                                                    <input type="file" name="photo" accept="image/*">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <button class="btn btn-success" type="submit">Simpan</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Batal
                                                </button>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="modalPassword">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="defaultModalLabel">Ubah kata sandi</h4>
                            </div>
                            <div class="modal-body">
                                <form action="{{ route('me.update.password', $_SERVER['HTTP_HOST']) }}"
                                    method="POST"
                                    enctype="multipart/form-data" data-parsley-validate>
                                    <fieldset>

                                        {{ csrf_field() }}

                                        <input type="hidden" name="_method" value="put">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Kata sandi lama : </label>
                                                <input class="form-control" type="password" name="old_password"
                                                    required>
                                            </div>

                                            <div class="form-group">
                                                <label>Kata sandi baru : </label>
                                                <input class="form-control" type="password" name="new_password"
                                                    required>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <button class="btn btn-success" type="submit">Simpan</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Batal
                                                </button>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="box-footer">
                    <div class="col-md-12">
                        <a href="#!" data-toggle="modal" data-target="#modalPassword" class="btn btn-default">
                            <i class="fa fa-edit"></i>
                            Ubah Kata Sandi
                        </a>
                        <a href="#!" data-toggle="modal" data-target="#modalAdd" class="btn btn-default pull-right">
                            <i class="fa fa-image"></i>
                            Ubah Foto
                        </a>
                        <!-- <a href="#" class="btn btn-danger pull-right">Blokir</a> -->
                    </div>

                </div>

            </div>

        </div>

        <div class="col-md-8">


            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Detail Profil</h3>
                </div>


                <div class="box-body">
                    <div class="col-md-12 table-responsive">
                        <table class="table">
                            <tr>
                                <td>Nama Lengkap</td>
                                <td>:</td>
                                <td>{{ $user->fullname }}</td>
                            </tr>
                            <tr>
                                <td>Username</td>
                                <td>:</td>
                                <td>{{ $user->username }}</td>
                            </tr>
                            <tr>
                                <td>E-mail</td>
                                <td>:</td>
                                <td>{{ $user->email }}</td>
                            </tr>
                            <tr>
                                <td>Tanggal Lahir</td>
                                <td>:</td>
                                <td>{{ date('d MM Y', strtotime($user->dob)) }}</td>
                            </tr>
                            <tr>
                                <td>Jenis Kelamin</td>
                                <td>:</td>
                                <td>{{ $user->sex != null && $user->sex === 'L' ? 'Laki-laki' : 'Perempuan' }}</td>
                            </tr>
                            <tr>
                                <td>Alamat</td>
                                <td>:</td>
                                <td>{{ $user->address }}</td>
                            </tr>
                            <tr>
                                <td>Kode POS</td>
                                <td>:</td>
                                <td>{{ $user->zipcode }}</td>
                            </tr>
                            <tr>
                                <td>Telepon</td>
                                <td>:</td>
                                <td>{{ $user->phone }}</td>
                            </tr>
                            <tr>
                                <td>Hobi</td>
                                <td>:</td>
                                <td>{{ $user->hobby }}</td>
                            </tr>
                            <tr>
                                <td>Alasan</td>
                                <td>:</td>
                                <td>{{ $user->reason }}</td>
                            </tr>

                        </table>
                    </div>
                </div>

                <div class="box-footer">
                    <div class="col-md-12">
                        <a href="{{ url('me/edit') }}" class="btn btn-default">
                            <i class="fa fa-edit"></i>
                            Edit Profile
                        </a>
                    </div>
                </div>

            </div>

        </div>

    </div>

@endsection


