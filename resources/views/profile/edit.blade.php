@extends('layouts.main')

@section('content')

    <div class="row">


        <div class="col-xs-12">


            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Edit Profil</h3>
                </div>

                <form action="{{ route('me.update', $_SERVER['HTTP_HOST']) }}" method="post"
                      enctype="multipart/form-data" data-parsley-validate>

                    <div class="box-body">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <input type="hidden" name="_method" value="put"/>

                        <fieldset>
                            <div class="col">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Nama Lengkap : </label>
                                        <input class="form-control" name="fullname" value="{{ $user->fullname }}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>E-mail : </label>
                                        <input class="form-control" type="email" name="email"
                                               value="{{ $user->email }}" disabled required>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>No. KTP : </label>
                                        <input class="form-control" name="ktp_id" value="{{ $user->ktp_id }}">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tanggal Lahir : </label>
                                        <input type="text" class="form-control datepicker" name="birth_date"
                                               value="{{ $user->birth_date }}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Jenis Kelamin : </label>
                                        <select class="form-control" name="sex">
                                            <option value="L" {{ $user->sex === 'L' ? 'selected' : ''}}>Laki-laki
                                            </option>
                                            <option value="P" {{ $user->sex === 'P' ? 'selected' : ''}}>Perempuan
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12"></div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Telepon : </label>
                                        <input class="form-control" type="number" name="phone"
                                               value="{{ $user->phone }}" required>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>PIN BB : </label>
                                        <input class="form-control" name="pin_bb" value="{{ $user->pin_bb }}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Link Facebook : </label>
                                        <input class="form-control" name="fb" value="{{ $user->fb }}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Link Twitter : </label>
                                        <input class="form-control" name="tw" value="{{ $user->tw }}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Link Website : </label>
                                        <input class="form-control" name="website" value="{{ $user->website }}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Alamat : </label>
                                        <textarea class="form-control" name="address"
                                                  rows="3">{{ $user->address }}</textarea>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Kode POS : </label>
                                        <input class="form-control" type="number" name="zipcode"
                                               value="{{ $user->zipcode }}" required>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Hobi : </label>
                                        <textarea class="form-control" name="hobby"
                                                  rows="3">{{ $user->hobby }}</textarea>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Alasan : </label>
                                        <textarea class="form-control" name="reason"
                                                  rows="3">{{ $user->reason }}</textarea>
                                    </div>
                                </div>

                            </div>

                        </fieldset>

                    </div>

                    <div class="box-footer text-right">
                        <div class="col-md-12">
                            <div class="form-group">
                                <a href="{{ url('me') }}" class="btn btn-default">
                                    <i class="fa fa-close"></i>
                                    Batal
                                </a>
                                <button class="btn btn-success">
                                    <i class="fa fa-save"></i>
                                    Simpan
                                </button>
                            </div>
                        </div>
                    </div>

                </form>

            </div>

        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        $('.select2-multi').select2();

    </script>
@endsection
