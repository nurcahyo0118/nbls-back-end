@extends('layouts.main')

@section('content')

    @php
        $tenant = Auth::user()->tenant;
        $permission = App\Permission::where(['role_id' => Auth::user()->role['id'], 'tenant_id' => $tenant->id])->first();
    @endphp

    <div class="row">

        <div class="col-xs-12">

            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Buat Paket Kursus</h3>

                </div>

                <form 
                    action="{{ route('course-packages.store', $_SERVER['HTTP_HOST']) }}"
                    method="POST"
                    enctype="multipart/form-data"
                    data-parsley-validate>

                    {{ csrf_field() }}

                    <div class="box-body">
                        
                        <fieldset>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Judul : </label>
                                    <input class="form-control" type="text" name="title" required>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Deskripsi : </label>
                                    <textarea class="form-control" name="description" rows="4" required></textarea>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Harga(Rp) : </label>
                                    <input class="form-control" type="number" name="price" required>
                                </div>
                            </div>

                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Gambar : </label>
                                    <input class="form-control" type="file" name="image" accept="image/*" required>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Kategori : </label>
                                    <select class="form-control" type="text" name="category_id" onchange="onSelectCategory(value)" required>
                                        <option value="">Pilih Kategori</option>
                                        @foreach(Auth::user()->tenant->categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Sub Kategori : </label>
                                    <select id="course_sub_category_id" class="form-control" type="text" name="sub_category_id" required>
                                        <option value="">Pilih Sub Kategori</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>
                                        <input type="checkbox" name="is_publish">
                                        Publikasikan
                                    </label>
                                </div>
                            </div>

                        </fieldset>
                        
                    </div>

                    <div class="box-footer">
                        <div class="col-md-12 text-right">
                            <a 
                                href="{{ route('course-packages.index', $_SERVER['HTTP_HOST']) }}"
                                class="btn btn-default">
                                <i class="fa fa-close"></i>
                                Batal
                            </a>
                            <button type="submit" class="btn btn-success">
                                <i class="fa fa-save"></i>
                                Simpan
                            </button>
                        </div>
                    </div>

                </form>

            </div>

        </div>
    </div>

@endsection

@section('script')
    <script>
        function onSelectCategory(value) {
            var courseSubCategoryId = $('#course_sub_category_id');

            $.ajax({
                type: 'GET',
                url: '/loads/subcategories/' + value,
                success: function (response) {
                    console.log(response);

                    courseSubCategoryId.empty();

                    courseSubCategoryId.append('<option value="">Pilih Sub Kategori</option>');

                    for (let subcategory of response) {
                        console.log(subcategory);
                        courseSubCategoryId.append('<option value="' + subcategory.id + '">' + subcategory.name + '</option>');
                    }

                },
                error: function (error) {
                    console.log(error);
                }
            });

        }
    </script>
@endsection