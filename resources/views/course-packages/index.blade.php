@extends('layouts.main')

@section('content')

    @php
        $tenant = Auth::user()->tenant;
        $permission = App\Permission::where(['role_id' => Auth::user()->role['id'], 'tenant_id' => $tenant->id])->first();
    @endphp

    <div class="row">

        <div class="col-xs-12">

            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Paket Kursus</h3>
                    <br>
                    <a class="btn btn-primary pull-left col-md-1" href="{{ route('course-packages.create', $_SERVER['HTTP_HOST']) }}">
                        <span class="fa fa-plus"></span>
                    </a>
                </div>

                <div class="box-body">
                    <table class="table js-basic-example dataTable">
                        <thead>
                        <tr>
                            <th>Judul</th>
                            <th>Harga (Rp)</th>
                            <th>Instruktur</th>
                            <th>Diterbitkan</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody id="coursePackagesList">

                        @foreach($course_packages as $course_package)
                            <tr>
                                <td>{{ $course_package->title }}</td>
                                <td>Rp. {{ number_format($course_package->price, 0, ',', '.') }}</td>
                                <td>{{ $course_package->user->fullname }}</td>
                                <td>
                                    {{ $course_package->is_publish ? 'TRUE' : 'FALSE' }}
                                    {{--  @if($course_package->is_publish)
                                        <button class="btn btn-danger btn-xs" onclick="hide()">
                                            <i class="fa fa-close"></i>
                                            Sembunyikan
                                        </button>
                                    @else
                                        <button class="btn btn-success btn-xs" onclick="publish()">
                                            <i class="fa fa-paper-plane"></i>
                                            Terbitkan
                                        </button>
                                    @endif  --}}
                                </td>
                                <td class="text-right">
                                    <a
                                        href="{{ route('course-packages.edit', [$_SERVER['HTTP_HOST'], $course_package->id]) }}"
                                        class="btn btn-default btn-xs">
                                        <i class="fa fa-gear"></i>
                                        Kelola
                                    </a>
                                    {{-- <a
                                        href="{{ route('logs.show', [$_SERVER['HTTP_HOST'], $course_package->id]) }}"
                                        class="btn btn-info btn-xs">
                                        <i class="fa fa-eye"></i>
                                        Detail
                                    </a> --}}
                                    <a
                                        href="#!"
                                        class="btn btn-danger btn-xs"
                                        onclick="deleteCoursePackage({{ $course_package->id }})">
                                        <i class="fa fa-trash"></i>
                                        Hapus
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div>

            </div>

        </div>
    </div>

@endsection

@section('script')
    <script>
        function publish() {
            swal("Berhasil !", "Paket kursus berhasil diterbitkan", "success");
        }

        function hide() {
            swal("Berhasil !", "Paket kursus berhasil disembunyikan", "success");
        }

        function deleteCoursePackage(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "Data yang telah di hapus tidak dapat dikembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {

                    $.ajax({
                        url: '/course-packages/' + id,
                        type: 'DELETE',
                        data: null,
                        success: function (data) {
                            window.location.reload();
                        },
                        error: function (error) {
                            swal("Gagal !", "Gagal menghapus kursus", "error");
                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    });

                }
            });
        }
    </script>
@endsection