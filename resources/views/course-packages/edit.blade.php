@extends('layouts.main')

@section('content')

    @php
        $tenant = Auth::user()->tenant;
        $permission = App\Permission::where(['role_id' => Auth::user()->role['id'], 'tenant_id' => $tenant->id])->first();
    @endphp

    <div class="row">

        <div class="col-xs-12">

            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Kelola Paket Kursus</h3>

                </div>

                <form 
                    action="{{ route('course-packages.update', [$_SERVER['HTTP_HOST'], $course_package->id]) }}"
                    method="POST"
                    enctype="multipart/form-data"
                    data-parsley-validate>

                    {{ csrf_field() }}

                    <input type="hidden" name="_method" value="PUT">

                    <div class="box-body">
                        
                        <fieldset>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Judul : </label>
                                    <input class="form-control" type="text" name="title" value="{{ $course_package->title }}" required>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Deskripsi : </label>
                                    <textarea class="form-control" name="description" rows="4" required>{{ $course_package->description }}</textarea>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Harga(Rp) : </label>
                                    <input class="form-control" type="number" name="price" value="{{ $course_package->price }}" required>
                                </div>
                            </div>

                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Gambar : </label>
                                    <input class="form-control" type="file" name="image" accept="image/*">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Kategori : </label>
                                    <select class="form-control" type="text" name="category_id" onchange="onSelectCategory(value)" required>
                                        <option value="">Pilih Kategori</option>
                                        @foreach(Auth::user()->tenant->categories as $category)
                                            <option value="{{ $category->id }}" {{ $category->id == $course_package->category_id ? 'selected' : '' }}>{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Sub Kategori : </label>
                                    <select id="course_sub_category_id" class="form-control" type="text" name="sub_category_id" required>
                                        <option value="">Pilih Sub Kategori</option>
                                        @foreach($course_package->category->subcategories as $subcategory)
                                            <option value="{{ $subcategory->id }}" {{ $subcategory->id == $course_package->sub_category_id ? 'selected' : '' }}>{{ $subcategory->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>
                                        <input type="checkbox" name="is_publish" {{ $course_package->is_publish ? 'checked' : '' }}>
                                        Publikasikan
                                    </label>
                                </div>
                            </div>

                        </fieldset>
                        
                    </div>

                    <div class="box-footer">
                        <div class="col-md-12 text-right">
                            <a 
                                href="{{ route('course-packages.index', $_SERVER['HTTP_HOST']) }}"
                                class="btn btn-default">
                                <i class="fa fa-close"></i>
                                Batal
                            </a>
                            <button type="submit" class="btn btn-success">
                                <i class="fa fa-save"></i>
                                Simpan
                            </button>
                        </div>
                    </div>

                </form>

            </div>

            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Daftar Kursus Dalam Paket</h3>

                </div>

                <div class="box-body">
                    
                    <table class="table js-basic-example dataTable">
                        <thead>
                        <tr>
                            <th>Judul</th>
                            <th>Harga (Rp)</th>
                            <th>Instruktur</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                            {{--  {{ $selected_courses }}  --}}
                        @foreach($selected_courses as $selected_course)
                            <tr>
                                <td><b>{{ substr($selected_course->title, 0, 20) }}</b></td>
                                <td>Rp. {{ number_format($selected_course->price, 0, ',', '.') }}</td>
                                <td>{{ \App\User::find($selected_course->author_id)->fullname }}</td>
                                <td class="text-green">{{ $selected_course->status }}</td>
                                <td class="text-right">
                                    <form 
                                        action="{{ route('course-packages.unselect-course', [$_SERVER['HTTP_HOST'], $course_package->id, $selected_course->id]) }}" 
                                        method="POST">

                                        {{ csrf_field() }}

                                        <button 
                                            type="submit"
                                            class="btn btn-danger btn-xs">
                                            Batal Pilih
                                        </button>

                                    </form>
                                </td>
                            </tr>

                        @endforeach
                        </tbody>
                    </table>

                </div>

            </div>

            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Pilih Kursus</h3>

                </div>

                <div class="box-body">
                    
                    <table class="table js-basic-example dataTable">
                        <thead>
                        <tr>
                            <th>Judul</th>
                            <th>Harga (Rp)</th>
                            <th>Instruktur</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($courses as $course)
                            <tr>
                                <td><b>{{ substr($course->title, 0, 20) }}</b></td>
                                <td>Rp. {{ number_format($course->price, 0, ',', '.') }}</td>
                                <td>{{ \App\User::find($course->author_id)->fullname }}</td>
                                <td class="text-green">{{ $course->status }}</td>
                                <td class="text-right">
                                    <form 
                                        action="{{ route('course-packages.select-course', [$_SERVER['HTTP_HOST'], $course_package->id, $course->id]) }}" 
                                        method="POST">

                                        {{ csrf_field() }}

                                        <button 
                                            type="submit"
                                            class="btn btn-success btn-xs">
                                            Pilih
                                        </button>

                                    </form>
                                </td>
                            </tr>

                            <div class="modal fade" id="modalDelete{{ $course->id }}">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="defaultModalLabel">Hapus {{ $course->name }}
                                                                                            ?</h4>
                                        </div>
                                        <div class="modal-body">
                                            Anda yakin ingin menghapus data ini?
                                        </div>
                                        <div class="modal-footer">
                                            {!! Form::open(['route' => ['courses.destroy', $_SERVER['HTTP_HOST'], $course->id], 'method' => 'DELETE']) !!}
                                            <button type="button" class="btn btn-default" data-dismiss="modal"> Batal
                                            </button>
                                            <button type="submit" class="btn btn-danger"> Hapus</button>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        </tbody>
                    </table>

                </div>

            </div>

        </div>
    </div>

@endsection

@section('script')
    <script>
        function selectCourse(id) {
            $.ajax({
                url: '/course-packages/' + {{ $course_package->id }} + '/select/' + id,
                type: 'POST',
                data: null,
                success: function (data) {
                    
                },
                error: function (error) {
                    swal("Gagal !", "Gagal memilih kursus", "error");
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function unselectCourse(id) {
            $.ajax({
                url: '/course-packages/' + {{ $course_package->id }} + '/unselect/' + id,
                type: 'POST',
                data: null,
                success: function (data) {
                    
                },
                error: function (error) {
                    swal("Gagal !", "Gagal membatalkan kursus", "error");
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    </script>
@endsection