@extends('layouts.main')

@section('content')

    @php
        $tenant = Auth::user()->tenant;
        $permission = App\Permission::where(['role_id' => Auth::user()->role['id'], 'tenant_id' => $tenant->id])->first();
    @endphp

    <div class="row">

        <div class="modal fade" id="modalAdd">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel">Tambah Kategori</h4>
                    </div>

                    <form class="" action="{{ route('categories.store', $_SERVER['HTTP_HOST']) }}" method="post"
                          data-parsley-validate>

                        <div class="modal-body">
                            <fieldset>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label> Nama : </label>
                                        <input type="text" name="name" class="form-control" required>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label> Deskripsi : </label>
                                        <textarea type="text" name="description" class="form-control"
                                                  rows="4"></textarea>
                                    </div>
                                </div>

                            </fieldset>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                <i class="fa fa-close"></i>
                                Tutup
                            </button>
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-save"></i>
                                Simpan
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <div class="col-xs-12">

            @include('layouts.partials.message')

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Daftar Kategori</h3>
                    <br>
                    <a class="btn btn-primary pull-left col-md-1" href="#!" data-toggle="modal" data-target="#modalAdd">
                        <span class="fa fa-plus"></span>
                    </a>

                </div>
                <div class="box-body">

                    @foreach($categories as $category)
                        <div class="col-md-12">
                            <div class="box box-primary box-solid collapsed-box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">{{ $category->name }}</h3>

                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </div>

                                </div>

                                <div class="box-body">

                                    <div class="col-md-12">
                                        <button onclick="toggleForm({{ $category->id }})" class="btn btn-info">
                                            <i class="fa fa-plus"></i>
                                            Tambah Sub Kategori
                                        </button>
                                    </div>

                                    <div id="sub{{ $category->id }}" class="subcategory">
                                        <form>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label> Sub Kategori : </label>
                                                    <input id="name{{ $category->id }}" class="form-control" type="text"
                                                           name="name">

                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label> Deskripsi : </label>
                                                    <input id="description{{ $category->id }}" class="form-control"
                                                           type="text"
                                                           name="description">

                                                </div>
                                            </div>

                                            <div class="col-md-12 text-right">

                                                <button onclick="onSubmitSubCategory({{ $category->id }})" type="button"
                                                        class="btn btn-success">
                                                    <i class="fa fa-save"></i>
                                                    Simpan
                                                </button>
                                            </div>

                                        </form>
                                    </div>

                                    <br>
                                    <br>

                                    <div class="col-md-12">

                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>Sub Kategori</th>
                                                <th>Deskripsi</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody id="tableSubCategory{{ $category->id }}">

                                            @foreach($category->subcategories as $sub_category)
                                                <tr id="subcategory{{ $sub_category->id }}">
                                                    <td><b>{{ $sub_category->name }}</b></td>
                                                    <td>{{ $sub_category->description }}</td>
                                                    <td class="text-right">

                                                        <button onclick="deleteSubCategory({{ $sub_category->id }})"
                                                                class="btn btn-danger btn-xs">
                                                            <i class="fa fa-trash"></i>
                                                            Hapus
                                                        </button>

                                                    </td>
                                                </tr>
                                            @endforeach

                                            </tbody>
                                        </table>

                                    </div>

                                </div>

                                <div class="box-footer text-right">

                                    @if($permission->update_category)
                                        <button class="btn btn-default" data-toggle="modal"
                                                data-target="#modalEdit{{ $category->id }}">
                                            <i class="fa fa-edit"></i>
                                            Edit
                                        </button>
                                    @endif

                                    @if($permission->delete_category)
                                        <button class="btn btn-danger" data-toggle="modal"
                                                data-target="#modalDelete{{ $category->id }}">
                                            <i class="fa fa-trash"></i>
                                            Hapus
                                        </button>
                                    @endif
                                </div>

                            </div>
                        </div>

                        <div class="modal fade" id="modalEdit{{ $category->id }}">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="defaultModalLabel">Edit Kategori</h4>
                                    </div>

                                    <form class=""
                                          action="{{ route('categories.update', [$_SERVER['HTTP_HOST'], $category->id]) }}"
                                          method="post" data-parsley-validate>

                                        <div class="modal-body">
                                            <fieldset>
                                                <input type="hidden" name="_token"
                                                       value="{{ csrf_token() }}">
                                                <input name="_method" type="hidden" value="PUT">

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label> Nama : </label>
                                                        <input type="text" name="name"
                                                               class="form-control"
                                                               value="{{ $category->name }}"
                                                               required>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label> Deskripsi : </label>
                                                        <textarea type="text" name="description"
                                                                  class="form-control"
                                                                  rows="4">{{ $category->description }}</textarea>
                                                    </div>
                                                </div>

                                            </fieldset>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default"
                                                    data-dismiss="modal">
                                                <i class="fa fa-close"></i>
                                                Tutup
                                            </button>


                                            <button type="submit" class="btn btn-primary">
                                                <i class="fa fa-save"></i>
                                                Simpan
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="modalDelete{{ $category->id }}">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="defaultModalLabel">
                                            Hapus {{ $category->name }}
                                            ?</h4>
                                    </div>
                                    <div class="modal-body">
                                        Anda yakin ingin menghapus data ini?
                                    </div>
                                    <div class="modal-footer">
                                        {!! Form::open(['route' => ['categories.destroy', $_SERVER['HTTP_HOST'], $category->id], 'method' => 'DELETE']) !!}
                                        <button type="button" class="btn btn-default"
                                                data-dismiss="modal"> Batal
                                        </button>
                                        <button type="submit" class="btn btn-danger"> Hapus</button>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                    @endforeach

                </div>

                <div class="box-footer text-right">
                    <div class="col-md-12">
                        {{ $categories->links() }}
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('script')
    <script>
        $(function () {
            $('.subcategory').hide();
        });

        function toggleForm(id) {
            var form = $('#sub' + id).toggle();
        }

        function onSubmitSubCategory(id) {

            var name = $('#name' + id).val();
            var description = $('#description' + id).val();

            $.ajax({
                type: "POST",
                url: '/sub_categories',
                data: {
                    name: name,
                    description: description,
                    category_id: id
                },
                success: function (response) {
                    console.log(response);

                    name = "";
                    description = "";

                    $("#tableSubCategory" + response.category_id).append(
                        "<tr id='subcategory" + response.id + "'>\n" +
                        "<td><b>" + response.name + "</b></td>\n" +
                        "<td>" + response.description + "</td>\n" +
                        "<td class=\"text-right\">\n" +
                        "\n" +
                        "                                                        <button onclick=\"deleteSubCategory(" + response.id + ")\"" +
                        "                                                           class=\"btn btn-danger btn-xs\">" +
                        "                                                            <i class=\"fa fa-trash\"></i>" +
                        "                                                           Hapus" +
                        "                                                        </button>" +
                        "\n" +
                        "                                                    </td>\n" +
                        "                                                </tr>"
                    );

                    swal("Berhasil !", "Sub Kategori berhasil ditambahkan", "success");

                },
                error: function(error) {
                    swal("Gagal !", "Sub Kategori gagal ditambahkan, Sub Kategori sudah diambil", "error");
                }
            });
        }

        function deleteSubCategory(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "Data yang telah di hapus tidak dapat dikembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    serviceDeleteSubCategory(id);
                } else {
                    swal("Your imaginary file is safe!");
                }
            });

        }

        /* Service */
        function serviceDeleteSubCategory(id) {
            $.ajax({
                type: "DELETE",
                url: '/sub_categories/' + id,
                success: function (response) {
                    console.log(response);
                    $("#subcategory" + id).remove();

                    swal("Berhasil !", "Sub Kategori berhasil dihapus", "success");
                }
            });
        }

        /* End Service */
    </script>
@endsection
