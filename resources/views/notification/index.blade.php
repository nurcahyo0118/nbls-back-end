@extends('layouts.main')

@section('content')

    @php
        $tenant = Auth::user()->tenant;
        $permission = App\Permission::where(['role_id' => Auth::user()->role['id'], 'tenant_id' => $tenant->id])->first();
    @endphp

    <section class="content-header">
        <h1>
            Terhubung dengan murid anda
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="#">
                    <i class="fa fa-dashboard"></i>
                    Dashboard
                </a>
            </li>
            <li class="active">
                <a href="{{ route('tenants.index', $_SERVER['HTTP_HOST']) }}">
                    <i class="fa fa-gears"></i>
                    Pengaturan Notifikasi
                </a>
            </li>
        </ol>
    </section>

    <section class="content">

        <div class="row">

            <div class="col-xs-12">

                @include('layouts.partials.message')
                
                <div class="box box-primary">
                    <form action="{{ route('notification.setting.update') }}" method="POST">
                        <div class="box-body">
                            
                            {{ csrf_field() }}

                            <input type="hidden" name="_method" value="put">

                            <div class="col-md-12">
                                <h3>Beri tahu saya jika</h3>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>
                                        <input type="checkbox" name="is_student_open_course" value="1" {{ $notification_setting->is_student_open_course ? 'checked' : '' }}>
                                        Murid membuka kursus
                                    </label>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>
                                        <input type="checkbox" name="is_order_occur" value="1" {{ $notification_setting->is_order_occur ? 'checked' : '' }}>
                                        Ada pesanan masuk
                                    </label>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>
                                        <input type="checkbox" name="is_ask_question" value="1" {{ $notification_setting->is_ask_question ? 'checked' : '' }}>
                                        Murid mengajukan pertanyaan
                                    </label>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>
                                        <input type="checkbox" name="is_answer_question" value="1" {{ $notification_setting->is_answer_question ? 'checked' : '' }}>
                                        Murid menjawab pertanyaan
                                    </label>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>
                                        <input type="checkbox" name="is_cancel_subsciption" value="1" {{ $notification_setting->is_cancel_subsciption ? 'checked' : '' }}>
                                        Murid membatalkan langganan
                                    </label>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <h3>Notifikasi email murid</h3>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>
                                        <input type="checkbox" name="is_email_account_created" value="1" {{ $notification_setting->is_email_account_created ? 'checked' : '' }}>
                                        Kirim murid email saat akun di buat
                                    </label>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>
                                        <input type="checkbox" name="is_email_weekly" value="1" {{ $notification_setting->is_email_weekly ? 'checked' : '' }}>
                                        Izinkan murid menerima email mingguan
                                    </label>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>
                                        <input type="checkbox" name="is_question_answered" value="1" {{ $notification_setting->is_question_answered ? 'checked' : '' }}>
                                        Izinkan murid menerima email saat pertanyaan nya di jawab
                                    </label>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>
                                        <input type="checkbox" name="is_email_purchase" value="1" {{ $notification_setting->is_email_purchase ? 'checked' : '' }}>
                                        Kirim murid sebuah email jika mereka melakukan pembelian
                                    </label>
                                </div>
                            </div>
                            
                        </div>

                        <div class="box box-footer text-right">
                            <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                        </div>

                    </form>
                </div>

            </div>
        </div>

    </section>

@endsection