@extends('layouts.main')

@section('content')

    @php
        $tenant = Auth::user()->tenant;
        $permission = App\Permission::where(['role_id' => Auth::user()->role['id'], 'tenant_id' => $tenant->id])->first();
    @endphp

    <div class="row">

        <div class="col-xs-12">

            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Notifikasi</h3>

                </div>

                <div class="box-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Judul</th>
                            <th>Waktu</th>
                        </tr>
                        </thead>
                        <tbody id="encounterList">

                        @foreach($notifications as $notification)
                            <tr>
                                <td>
                                    {{--  @if()
                                        <a href="">  --}}
                                            {{ $notification->title }}
                                        {{--  </a>
                                    @else

                                    @endif  --}}
                                </td>
                                <td>
                                    {{ $notification->created_at->diffForHumans() }}
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                    {{ $notifications->links() }}
                </div>

            </div>

        </div>
    </div>

@endsection