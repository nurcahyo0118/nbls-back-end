@extends('layouts.main')

@section('content')

    @php
        $tenant = Auth::user()->tenant;
        $permission = App\Permission::where(['role_id' => Auth::user()->role['id'], 'tenant_id' => $tenant->id])->first();
    @endphp

    <div class="row">

        <div class="col-xs-12">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            

            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Buat User</h3>
                </div>

                <div class="box-body">
                    <form action="{{ Auth::user()->tenant->is_cname ? route('users.store', $_SERVER['HTTP_HOST']) : route('tenant.users.store', Auth::user()->tenant->subdomain) }}"
                            method="post" data-parsley-validate>
        
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
    
                        <fieldset>
    
                            {{--  <div class="col-md-6">
                                <div class="form-group">
                                    <label> Foto : </label>
                                    <input type="file" name="company_logo">
                                </div>
                            </div>  --}}
    
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label> Nama Lengkap : </label>
                                    <input type="text" name="fullname" class="form-control" required>
                                </div>
                            </div>
    
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label> Nomot KTP : </label>
                                    <input type="number" name="ktp_id" class="form-control">
                                </div>
                            </div>
    
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label> Username : </label>
                                    <input type="text" name="username" class="form-control" required>
                                </div>
                            </div>
    
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label> E-mail : </label>
                                    <input type="email" name="email" class="form-control" required>
                                </div>
                            </div>
    
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label> Kata Sandi: </label>
                                    <input type="password" name="password" class="form-control" required>
                                </div>
                            </div>
    
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label> Telepon : </label>
                                    <input type="number" name="phone" class="form-control">
                                </div>
                            </div>
    
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label> Nomor Handphone 1 : </label>
                                    <input type="number" name="mobile" class="form-control">
                                </div>
                            </div>
    
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label> Nomor Handphone 2 : </label>
                                    <input type="number" name="mobile2" class="form-control">
                                </div>
                            </div>
    
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label> PIN BBM : </label>
                                    <input type="text" name="pin_bb" class="form-control">
                                </div>
                            </div>
    
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label> Kode Pos : </label>
                                    <input type="text" name="zipcode" class="form-control">
                                </div>
                            </div>
    
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label> Alamat : </label>
                                    <textarea class="form-control" name="address" rows="3"></textarea>
                                </div>
                            </div>
    
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label> Catatan : </label>
                                    <textarea class="form-control" name="notes" rows="3"></textarea>
                                </div>
                            </div>
    
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label> Hobi : </label>
                                    <textarea class="form-control" name="hobby" rows="3"></textarea>
                                </div>
                            </div>
    
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label> Alasan : </label>
                                    <textarea class="form-control" name="reason" rows="3"></textarea>
                                </div>
                            </div>
    
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label> Facebook : </label>
                                    <input type="text" name="fb" class="form-control">
                                </div>
                            </div>
    
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label> Twitter : </label>
                                    <input type="text" name="tw" class="form-control">
                                </div>
                            </div>
    
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label> Website : </label>
                                    <input type="text" name="website" class="form-control">
                                </div>
                            </div>
    
                        </fieldset>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                <i
                                        class="fa fa-close"></i>
                                Tutup
                            </button>
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-save"></i>
                                Simpan
                            </button>
                        </div>
                    </form>

                </div>

            </div>

        </div>
    </div>

@endsection

@section('script')
    <script>
        function onSelectCategory(value) {
            var courseSubCategoryId = $('#course_sub_category_id');

            $.ajax({
                type: 'GET',
                url: '/loads/subcategories/' + value,
                success: function (response) {
                    console.log(response);

                    courseSubCategoryId.empty();

                    courseSubCategoryId.append('<option value="">Pilih Sub Kategori</option>');

                    for (let subcategory of response) {
                        console.log(subcategory);
                        courseSubCategoryId.append('<option value="' + subcategory.id + '">' + subcategory.name + '</option>');
                    }

                },
                error: function (error) {
                    console.log(error);
                }
            });

        }
    </script>
@endsection