@extends('layouts.main')

@section('content')

    @php
        $tenant = Auth::user()->tenant;
        $permission = App\Permission::where(['role_id' => Auth::user()->role['id'], 'tenant_id' => $tenant->id])->first();
    @endphp

    <div class="row">

        <div class="col-xs-12">

            @include('layouts.partials.message')

            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Daftar User</h3>
                    <br>
                <a class="btn btn-info pull-left col-md-1" href="{{ route('users.create', $_SERVER['HTTP_HOST']) }}">
                        <span class="fa fa-plus"></span>
                    </a>

                </div>

                <div class="box-body">
                    <table class="table table-export">
                        <thead>
                        <tr>
                            <th>Nama</th>
                            <th>E-Mail</th>
                            <th>Peran</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody id="userList">

                        @foreach($users as $user)
                            <tr id="userId{{ $user->id }}">
                                <td>{{ $user->fullname }}</td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    {!! Form::open(['route' => ['changeRole', $_SERVER['HTTP_HOST'], $user->id], 'method' => 'PUT', 'data-parsley-validate' => '', 'files' => true]) !!}
                                    <fieldset>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <select class="form-control" name="role_id">
                                                    @foreach($roles as $role)
                                                        <option value="{{ $role->id }}" {{ $user->role->id === $role->id ? 'selected' : ''}}>{{ $role->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <button type="submit" class="btn btn-info">
                                                <i class="fa fa-refresh"></i>
                                            </button>
                                        </div>

                                    </fieldset>
                                    {!! Form::close() !!}
                                </td>

                                @if($permission->delete_user)
                                    <td class="text-right">
                                        <a onclick="onDeleteUser({{ $user->id }})" class="btn btn-danger btn-xs">
                                            <i class="fa fa-trash"></i>
                                            Hapus
                                        </a>
                                    </td>
                                @endif
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div>

            </div>

        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        function addRole(user_id, role_id) {

            $.ajax({
                method: 'POST',
                url: '/users/add-role/',
                data: {
                    user_id: user_id,
                    role_id: role_id
                },
                async: true,
                success: function (response) {
                    var data = response;
                    console.log(data.length);
                    $("a#user" + user_id).append('sss');
                },
                error: function (data) {
                    console.log(data);
                    alert("fail" + ' ' + this.data)
                },
            });

        }

        function onDeleteUser(userId) {
            swal({
                title: "Apa kamu yakin?",
                text: "Data yang telah di hapus tidak dapat dikembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {

                    $.ajax({
                        type: "DELETE",
                        url: '/users/' + userId,
                        success: function (response) {
                            console.log(response);
                            $("#userId" + userId).remove();
                            swal("Berhasil !", "User berhasil dihapus", "success");
                        }
                    });

                }
            });
        }

        function deleteRole(user_id, role_id) {

            // $.ajax({
            //   method: 'POST',
            //   url: '/users/delete-role/',
            //   data: {
            //     user_id: user_id,
            //     role_id: role_id
            //   },
            //   async: true,
            //   success: function(response){
            //       $("#" + role_id).remove();
            //   },
            //   error: function(data){
            //       console.log(data);
            //       alert("fail" + ' ' + this.data)
            //   },
            // });

        }
    </script>
@endsection