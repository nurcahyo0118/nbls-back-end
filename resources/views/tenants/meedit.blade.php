@extends('layouts.main')

@section('content')

    <section class="content-header">
        <h1>
            Site Details
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="#">
                    <i class="fa fa-dashboard"></i>
                    Dashboard
                </a>
            </li>
            <li class="active">
                <i class="fa fa-building"></i>
                Kelola Tenant
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-edit"></i>
                    Edit
                </a>
            </li>
        </ol>
    </section>

    @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach

    <section class="content">

        <div class="row">

            <div class="modal fade" id="modalAdd">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Tambah Tenant</h4>
                        </div>


                    </div>
                </div>
            </div>


            <div class="col-xs-12">

                @include('layouts.partials.message')

                <div class="box box-primary">
                    <form class="" action="{{ route('tenants.update', [$_SERVER['HTTP_HOST'], $tenant->id]) }}"
                          enctype="multipart/form-data"
                          method="post"
                          data-parsley-validate>

                        <div class="box-body">
                            <fieldset>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <input type="hidden" name="_method" value="put"/>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label> Nama : </label>
                                        <input type="text" name="name" class="form-control" value="{{ $tenant->name }}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label> Banner 1 : </label>
                                        <input type="file" name="banner">
                                    </div>
                                <img src="/images/photo/tenant/{{ $tenant->banner }}" height="100px" class="img-fluid" alt="">
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label> Banner 2 : </label>
                                        <input type="file" name="banner2">
                                    </div>
                                      <img src="/images/photo/tenant/{{ $tenant->banner2 }}" height="100px" class="img-fluid" alt="">
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label> Banner 3 : </label>
                                        <input type="file" name="banner3">
                                    </div>
                                      <img src="/images/photo/tenant/{{ $tenant->banner3 }}" height="100px" class="img-fluid" alt="">
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label> Deskripsi : </label>
                                        <textarea type="text" name="description" class="form-control"
                                                  rows="4" required>{{ $tenant->description }}</textarea>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label> Tawk.to : </label>
                                        <textarea type="text" name="tawk_to" class="form-control"
                                                  placeholder="Masukkan Script Tawk.to anda disini"
                                                  rows="4" required>{{ $tenant->tawk_to }}</textarea>
                                    </div>
                                </div>

                                @if(Auth::user()->tenant->domain != env('HOST_NAME'))
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label> Tipe Host : </label>
                                            <br>
                                            <input type="radio" name="is_cname" value="1" onclick="customDomain()"
                                                    {{ $tenant->is_cname ? 'checked' : '' }}>
                                            Domain
                                            <input type="radio" name="is_cname" value="0" onclick="customSubdomain()"
                                                    {{ !$tenant->is_cname ? 'checked' : '' }}>
                                            Subdomain
                                        </div>
                                    </div>

                                    <div id="domain" class="col-md-6">
                                        <div class="form-group">
                                            <label> Domain : </label>
                                            <input id="inputDomain" type="text" name="domain" class="form-control" value="{{ $tenant->domain }}">
                                        </div>
                                    </div>

                                    <div id="subdomain" class="col-md-6">
                                        <div class="form-group">
                                            <label> Subdomain : </label>
                                            <input id="inputSubdomain" type="text" name="subdomain" class="form-control"
                                                maxlength="10" value="{{ $tenant->subdomain }}">
                                        </div>
                                    </div>
                                @endif

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label> Tema Front-End : </label>
                                        <select name="theme" class="form-control">
                                            <option value="bg-dark" {{ $tenant->theme == 'bg-dark' ? 'selected' : '' }}>
                                                Default Babastudio - Biru Dark
                                            </option>
                                            <option value="bg-primary" {{ $tenant->theme == 'bg-primary' ? 'selected' : '' }}>
                                                bg-primary - Biru
                                            </option>
                                            <option value="bg-success" {{ $tenant->theme == 'bg-success' ? 'selected' : '' }}>
                                                bg-success - Hijau
                                            </option>
                                            <option value="bg-danger" {{ $tenant->theme == 'bg-danger' ? 'selected' : '' }}>
                                                bg-danger - Merah
                                            </option>
                                            <option value="bg-muted" {{ $tenant->theme == 'bg-muted' ? 'selected' : '' }}>
                                                bg-muted - Abu-abu
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label> Logo Perusahaan : </label>
                                        <input type="file" name="company_logo">
                                    </div>
                                </div>

                            </fieldset>

                        </div>

                        <div class="box-footer">
                            <div class="col-md-12 text-right">
                                <a class="btn btn-default" href="{{ route('tenants.me', $_SERVER['HTTP_HOST']) }}">
                                    <i class="fa fa-close"></i>
                                    Tutup
                                </a>
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-save"></i>
                                    Simpan
                                </button>
                            </div>
                        </div>

                    </form>
                </div>

            </div>
        </div>

    </section>

@endsection

@section('script')
    <script>
        @if ($tenant->is_cname)
        customDomain();
        @else
        customSubdomain()

        @endif

        function customDomain() {
            document.getElementById('subdomain').style.display = 'none';
            document.getElementById('domain').style.display = 'block';

            document.getElementById('inputSubdomain').required = false;
            document.getElementById('inputDomain').required = true;

        }

        function customSubdomain() {
            document.getElementById('subdomain').style.display = 'block';
            document.getElementById('domain').style.display = 'none';

            document.getElementById('inputSubdomain').required = true;
            document.getElementById('inputDomain').required = false;
        }
    </script>
@endsection
