<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>NBLS - Babastudio</title>
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
      <link href="/baba-custom/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="/baba-custom/css/custom.css" rel="stylesheet">
      <link href="/baba-custom/lib/animate/animate.min.css" rel="stylesheet">
      <link href="/baba-custom/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
      <link href="/baba-custom/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
      <link href="/baba-custom/lib/lightbox/css/lightbox.min.css" rel="stylesheet">
      <link href="/baba-custom/css/style.css" rel="stylesheet">
  </head>
  <body style="background-image: url('/images/big/img1b.jpg'); background-repeat: repeat-x;">
  

  @include('layouts.partials-baba-custom.header')

<br>
<br>
<br>
<center>
<div class="col-md-4">
<section>
    <!-- form login -->
    <div class="wrapper-page">
        <div class="card bg-primary" style="opacity: 0.8;">
            <div class="panel-heading mt-4">
                <h4 class="text-center">
                    <a href="/" class="text-white"><b>{{ $tenant->name }}</b></a>
                </h4>
            </div>

            <div class="p-20">
                   @foreach($errors->all() as $error)
                        <p class="text-center">{{ $error }}</p>
                    @endforeach
                <form class="form-horizontal mt-1" action="{{ route('tenant.login', $tenant->subdomain) }}"
                      method="POST">

                    {{ csrf_field() }}

                    <input type="hidden" name="tenant_id" value="{{ $tenant->id }}">

                    <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn  form-control" type="button">
                                <i class="fa fa-user"></i>
                            </button>
                        </span>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        <input class="form-control" type="text" id="user-password" name="email" required="required"
                               placeholder="Username"/>
                    </div>

                    <br>

                    <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn  form-control" type="button">
                                <i class="fa fa-lock"></i>
                            </button>
                        </span>
                        <input class="form-control" type="password" id="user-password" name="password"
                               required="required" placeholder="Password"/>
                    </div>

                    <div class="form-group m-t-20">
                        <div class="col-12 text-center">
                            <div class="checkbox checkbox-primary text-white">
                                <input id="checkbox-signup" type="checkbox">
                                <label for="checkbox-signup"> Remember me </label>
                            </div>

                        </div>
                    </div>

                    <div class="form-group text-center m-t-20">
                        <div class="col-12">
                            <button class="btn btn-dark btn-block text-uppercase waves-effect waves-light"
                                    type="submit">
                                Login
                            </button>
                        </div>
                    </div>

                    <div class="form-group m-t-20 m-b-0">
                        <div class="row">
                            <div class="col-6">
                                <a href="/register" class="text-white">Dont have an Account?</a>
                            </div>
                            <div class="col-6">
                                <a href="/password/reset" class="text-white float-right">Forgotten Password?</a>
                            </div>

                            <div class="col-md-12 text-center m-t-20">
                                <a href="/" class="text-white">Return to Homepage <strong
                                            class="ti-arrow-left icon-zize"></strong></a>
                            </div>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
    <!-- end form login -->
    </section>
</div>
</center>
<br>
<br>
<br>
<br>




@include('layouts.partials-baba-custom.footer')

<script src="/baba-custom/vendor/jquery/jquery.min.js"></script>
<script src="/baba-custom/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/baba-custom/js/custom.js"></script>
<script src="/baba-custom/lib/jquery/jquery-migrate.min.js"></script>
<script src="/baba-custom/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/baba-custom/lib/easing/easing.min.js"></script>
<script src="/baba-custom/lib/superfish/hoverIntent.js"></script>
<script src="/baba-custom/lib/superfish/superfish.min.js"></script>
<script src="/baba-custom/lib/wow/wow.min.js"></script>
<script src="/baba-custom/lib/waypoints/waypoints.min.js"></script>
<script src="/baba-custom/lib/counterup/counterup.min.js"></script>
<script src="/baba-custom/lib/owlcarousel/owl.carousel.min.js"></script>
<script src="/baba-custom/lib/isotope/isotope.pkgd.min.js"></script>
<script src="/baba-custom/lib/lightbox/js/lightbox.min.js"></script>
<script src="/baba-custom/lib/touchSwipe/jquery.touchSwipe.min.js"></script>
<script src="/baba-custom/js/main.js"></script>
</body>
</html>