@extends('layouts.main')

@section('content')

    <section class="content-header">
        <h1>
            Detail Tenant
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="#">
                    <i class="fa fa-dashboard"></i>
                    Dashboard
                </a>
            </li>
            <li class="active">
                <i class="fa fa-building"></i>
                Kelola Tenant
            </li>
        </ol>
    </section>

    <section class="content">

        <div class="row">
            <div class="col-md-3">

                <!-- Profile Image -->
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        @if($tenant->company_logo === null)
                            {{--  <img src="/images/default.png" class="profile-user-img img-responsive">  --}}
                        @else
                            <img src="/images/photo/tenant/{{ $tenant->company_logo }}"
                                 class="profile-user-img img-responsive">
                        @endif

                        <h3 class="profile-username text-center"> {{ $tenant->name }}</h3>

                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b> Kursus</b>
                                <a class="pull-right">{{ $tenant->courses->count() }}</a>
                            </li>
                            <li class="list-group-item">
                                <b> Pengguna</b>
                                <a class="pull-right">{{ $tenant->users->count() }}</a>
                            </li>
                        </ul>

                        <a href="{{ route('tenants.me.edit', $_SERVER['HTTP_HOST']) }}"
                           class="btn btn-default btn-block">
                            <b>
                                <i class="fa fa-gears"></i>
                                Basic Settings
                            </b>
                        </a>

                    </div>

                </div>

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"> Tentang</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <strong>
                            <i class="fa fa-globe margin-r-5"></i>
                            Laman
                        </strong>

                        <p>
                            @if($tenant->is_cname)
                                <i>{{ $tenant->domain }}</i>
                                <a href="http://{{ $tenant->domain }}" target="_blank">
                                    <i class="fa fa-link"></i>
                                </a>
                            @else
                                <i>{{ $tenant->subdomain . '.' . env('HOST_NAME') }}</i>
                                <a href="http://{{ $tenant->subdomain . '.' . env('HOST_NAME') }}"
                                   target="_blank">
                                    <i class="fa fa-link"></i>
                                </a>
                            @endif
                        </p>

                        <hr>

                        <strong>
                            <i class="fa fa-file-text-o margin-r-5"></i>
                            Deskripsi</strong>

                        <p>{{ $tenant->description }}</p>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->

            <div class="col-md-9">

                <div class="box box-primary">

                    <div class="box-header">
                        <h3 class="box-title">Daftar User</h3>
                    </div>

                    <div class="box-body">
                        <table class="table js-basic-example dataTable">
                            <thead>
                            <tr>
                                <th>Nama</th>
                                <th>E-Mail</th>
                                <th>Peran</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="userList">
                            @foreach($tenant->users as $user)

                                @include('tenants.child.child-user')

                            @endforeach

                            </tbody>
                        </table>

                    </div>

                </div>

                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Daftar Kursus</h3>
                    </div>

                    <div class="box-body">

                        <table class="table js-basic-example dataTable">
                            <thead>
                            <tr>
                                <th>Judul</th>
                                <th>Durasi</th>
                                <th>Harga (Rp)</th>
                                <th>Instruktur</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($tenant->courses as $course)
                                <tr>
                                    <td><b>{{ substr($course->title, 0, 20) }}</b></td>
                                    <td class="text-green">{{ $course->duration }} Menit</td>
                                    <td>Rp. {{ number_format($course->price, 0, ',', '.') }}</td>
                                    <td>{{ \App\User::find($course->author_id)->fullname }}</td>
                                    <td class="text-green">{{ $course->status }}</td>
                                    <td class="text-right">

                                        {{--  @if($permission->update_course)
                                            <a href="{{ route('courses.edit', [$_SERVER['HTTP_HOST'], $course->id]) }}"
                                                class="btn btn-default btn-xs">
                                                <i class="fa fa-gear"></i>
                                                Kelola
                                            </a>
                                        @endif  --}}

                                        {{--  @if($permission->delete_course)
                                            <a href="#!" data-toggle="modal" data-target="#modalDelete{{ $course->id }}"
                                                class="btn btn-danger btn-xs">
                                                <i class="fa fa-trash"></i>
                                                Hapus
                                            </a>
                                        @endif  --}}
                                    </td>
                                </tr>

                                <div class="modal fade" id="modalDelete{{ $course->id }}">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="defaultModalLabel">Hapus {{ $course->name }}
                                                                                               ?</h4>
                                            </div>
                                            <div class="modal-body">
                                                Anda yakin ingin menghapus data ini?
                                            </div>
                                            <div class="modal-footer">
                                                {!! Form::open(['route' => ['courses.destroy', $_SERVER['HTTP_HOST'], $course->id], 'method' => 'DELETE']) !!}
                                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                                    Batal
                                                </button>
                                                <button type="submit" class="btn btn-danger"> Hapus</button>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            </tbody>
                        </table>

                    </div>

                </div>

            </div>
            <!-- /.row -->

    </section>
@endsection

@section('script')
    <script>
        function onDeleteUser(userId) {
            swal({
                title: "Apa kamu yakin?",
                text: "Data yang telah di hapus tidak dapat dikembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {

                    $.ajax({
                        type: "DELETE",
                        url: '/users/' + userId,
                        success: function (response) {
                            console.log(response);
                            $("#userId" + userId).remove();
                            swal("Berhasil !", "User berhasil dihapus", "success");
                        }
                    });

                }
            });
        }
    </script>
@endsection