@extends('layouts.main')

@section('content')

    @php
        $tenant = Auth::user()->tenant;
        $permission = App\Permission::where(['role_id' => Auth::user()->role['id'], 'tenant_id' => $tenant->id])->first();
    @endphp

    <section class="content-header">
        <h1>
            Daftar Tenant
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="#">
                    <i class="fa fa-dashboard"></i>
                    Dashboard
                </a>
            </li>
            <li class="active">
                <a href="{{ route('tenants.index', $_SERVER['HTTP_HOST']) }}">
                    <i class="fa fa-building"></i>
                    Tenants
                </a>
            </li>
        </ol>
    </section>

    <section class="content">

        <div class="row">

            <div class="modal fade" id="modalAdd">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Tambah Tenant</h4>
                        </div>

                        <form class="" action="{{ route('tenants.store', $_SERVER['HTTP_HOST']) }}" method="post"
                              enctype='multipart/form-data' data-parsley-validate>

                            <div class="modal-body">
                                <fieldset>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label> Nama : </label>
                                            <input type="text" name="name" class="form-control" required>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label> Deskripsi : </label>
                                            <textarea type="text" name="description" class="form-control"
                                                      rows="4" required></textarea>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label> Tipe Host : </label>
                                            <input type="radio" name="is_cname" value="1" onclick="customDomain()"
                                                   checked>
                                            Domain
                                            <input type="radio" name="is_cname" value="0" onclick="customSubdomain()">
                                            Subdomain
                                        </div>
                                    </div>

                                    <div id="domain" class="col-md-6">
                                        <div class="form-group">
                                            <label> Domain : </label>
                                            <input type="text" name="domain" class="form-control" maxlength="25">
                                        </div>
                                    </div>

                                    <div id="subdomain" class="col-md-6">
                                        <div class="form-group">
                                            <label> Subdomain : </label>
                                            <input type="text" name="subdomain" class="form-control"
                                                   maxlength="10">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label> Logo Perusahaan : </label>
                                            <input type="file" name="company_logo" required>
                                        </div>
                                    </div>

                                </fieldset>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                    <i class="fa fa-close"></i>
                                    Tutup
                                </button>
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-save"></i>
                                    Simpan
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


            <div class="col-xs-12">

                @foreach ($errors->all() as $error)
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span
                                    aria-hidden="true">&times;
                            </span>
                        </button>
                        {{ $error }}
                    </div>
                @endforeach

                @include('layouts.partials.message')

                <div class="box box-primary">
                    <div class="box-header">
                        <a class="btn btn-info pull-left col-md-1" href="#!" data-toggle="modal"
                           data-target="#modalAdd">
                            <span class="fa fa-plus"></span>
                        </a>

                    </div>
                    <div class="box-body">
                        <table class="table js-basic-example dataTable">
                            <thead>
                            <tr>
                                <th>Nama Tenant</th>
                                <th>DNS</th>
                                <th>Deskripsi</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($tenants as $tenant)
                                @if($tenant->id != 1)
                                    <tr>
                                        <td><b>{{ $tenant->name }}</b></td>

                                        <td>
                                            @if($tenant->is_cname)
                                                <i>{{ $tenant->domain }}</i>
                                                <a href="http://{{ $tenant->domain }}" target="_blank">
                                                    <i class="fa fa-link"></i>
                                                </a>
                                            @else
                                                <i>{{ $tenant->subdomain . '.' . env('HOST_NAME') }}</i>
                                                <a href="http://{{ $tenant->subdomain . '.' . env('HOST_NAME') }}"
                                                   target="_blank">
                                                    <i class="fa fa-link"></i>
                                                </a>
                                            @endif
                                        </td>

                                        <td>{{ $tenant->description }}</td>

                                        <td class="text-right">
                                            <a href="{{ route('tenants.show', [$_SERVER['HTTP_HOST'], $tenant->id]) }}"
                                               class="btn btn-info btn-xs">
                                                <i class="fa fa-search"></i>
                                                Detail
                                            </a>

                                            @if($permission->update_tenant)
                                                <a href="{{ route('tenants.edit', [$_SERVER['HTTP_HOST'], $tenant->id]) }}"
                                                   class="btn btn-default btn-xs">
                                                    <i class="fa fa-edit"></i>
                                                    Edit
                                                </a>
                                            @endif

                                            @if($permission->delete_tenant)
                                                <a href="#!" data-toggle="modal"
                                                   data-target="#modalDelete{{ $tenant->id }}"
                                                   class="btn btn-danger btn-xs">
                                                    <i class="fa fa-trash"></i>
                                                    Hapus
                                                </a>
                                            @endif
                                        </td>

                                    </tr>

                                    <div class="modal fade" id="modalDelete{{ $tenant->id }}">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="defaultModalLabel">
                                                        Hapus {{ $tenant->name }}
                                                        ?</h4>
                                                </div>
                                                <div class="modal-body">
                                                    Anda yakin ingin menghapus data ini?
                                                </div>
                                                <div class="modal-footer">
                                                    {!! Form::open(['route' => ['tenants.destroy', $_SERVER['HTTP_HOST'], $tenant->id], 'method' => 'DELETE']) !!}
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">
                                                        Batal
                                                    </button>
                                                    <button type="submit" class="btn btn-danger"> Hapus</button>
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                @endif
                            @endforeach
                            </tbody>
                        </table>

                    </div>

                </div>

            </div>
        </div>

    </section>

@endsection

@section('script')
    <script>
        customDomain();

        function customDomain() {
            document.getElementById('subdomain').style.display = 'none';
            document.getElementById('domain').style.display = 'block';
        }

        function customSubdomain() {
            document.getElementById('subdomain').style.display = 'block';
            document.getElementById('domain').style.display = 'none';
        }
    </script>
@endsection
