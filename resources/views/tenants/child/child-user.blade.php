<tr id="userId{{ $user->id }}">
    <td>{{ $user->fullname }}</td>
    <td>{{ $user->email }}</td>
    <td>{{ $user->role->name }}</td>

    <td class="pull-right">
        <a onclick="onDeleteUser({{ $user->id }})" class="btn btn-danger btn-xs">
            <i class="fa fa-trash"></i>
            Hapus
        </a>
    </td>
</tr>