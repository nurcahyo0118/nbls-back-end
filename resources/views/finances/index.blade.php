@extends('layouts.main')

@section('content')

    @php
        $tenant = Auth::user()->tenant;
        $permission = App\Permission::where(['role_id' => Auth::user()->role['id'], 'tenant_id' => $tenant->id])->first();
    @endphp

    <div class="row">

        <div class="col-xs-12">

            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Laporan Keuangan</h3>

                </div>

                <div class="box-body">
                    <table class="table table-export">
                        <thead>
                        <tr>
                            <th>Reference Number</th>
                            <th>Metode</th>
                            <th>Nama User</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody id="encounterList">

                        @foreach($finances as $finance)
                            <tr>
                                <td>{{ $finance->referenceNo }}</td>
                                <td>{{ $finance->payMethod == 02 ? 'Bank Transfer' : '' }}</td>
                                <td>{{ $finance->user->fullname }}</td>
                                <td>{{ $finance->resultMsg }}</td>
                                <td></td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div>

            </div>

        </div>
    </div>

@endsection