<div class="modal fade" id="modalEditSection{{ $section->id }}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Tambah Modul</h4>
            </div>

            <form id="editsection" enctype="multipart/form-data" data-parsley-validate>

                <div class="modal-body">
                    <fieldset>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="put">

                        <input id="section_id" type="hidden" name="section_id" value="{{ $section->id }}">

                        <div class="col-md-12">
                            <div class="form-group">
                                <label> Judul : </label>
                                <input id="title" type="text" name="title" class="form-control"
                                       value="{{ $section->title }}" required>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label> Deskripsi : </label>
                                <textarea id="description" type="text" name="description" class="form-control"
                                          rows="4">{{ $section->description }}</textarea>
                            </div>
                        </div>

                    </fieldset>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <i class="fa fa-close"></i>
                        Tutup
                    </button>
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-save"></i>
                        Simpan
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
