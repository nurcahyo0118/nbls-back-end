<div class="modal fade" id="modalAddQuiz{{ $material->id }}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Tambah Kuis</h4>
            </div>

            <form id="formaddquiz" onsubmit="onSubmitQuiz({{ $material->id }}); return false;"
                  enctype="multipart/form-data" data-parsley-validate>
                <div class="modal-body">
                    <fieldset>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <input id="inputMaterialId{{ $material->id }}" type="hidden" name="material_id"
                               value="{{ $material->id }}">

                        <div class="col-md-12">
                            <div class="form-group">
                                <label> Soal Kuis : </label>
                                <textarea id="inputQuizQuestion{{ $material->id }}" name="question" class="form-control"
                                          required></textarea>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label> A : </label>
                                <input id="inputQuizA{{ $material->id }}" type="text" name="option_a"
                                       class="form-control" required>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label> B : </label>
                                <input id="inputQuizB{{ $material->id }}" type="text" name="option_b"
                                       class="form-control" required>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label> C : </label>
                                <input id="inputQuizC{{ $material->id }}" type="text" name="option_c"
                                       class="form-control" required>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label> D : </label>
                                <input id="inputQuizD{{ $material->id }}" type="text" name="option_d"
                                       class="form-control" required>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label> Jawaban : </label>
                                <select id="inputQuizTrue{{ $material->id }}" class="form-control" name="option_true">
                                    <option value="">Pilih jawaban</option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                    <option value="C">C</option>
                                    <option value="D">D</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label> Waktu kuis : </label>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <input id="inputQuizMinute{{ $material->id }}" type="number" maxlength="2"
                                       name="minute{{ $material->id }}" class="form-control" placeholder="Menit"
                                       required>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <input id="inputQuizSecond{{ $material->id }}" type="text" maxlength="2"
                                       name="second{{ $material->id }}" class="form-control" placeholder="Detik"
                                       required>
                            </div>
                        </div>

                    </fieldset>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <i class="fa fa-close"></i>
                        Tutup
                    </button>
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-save"></i>
                        Simpan
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
