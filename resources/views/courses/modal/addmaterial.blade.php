<div class="modal fade" id="modalAddMaterial{{ $section->id }}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Tambah Materi {{ $section->id }}</h4>
            </div>

            <form id="addmaterial{{ $section->id }}" onsubmit="onSubmitMaterial({{ $section->id }}); return false;" method="POST"
                  enctype="multipart/form-data" data-parsley-validate>
                <div class="modal-body">
                    <fieldset>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <input type="hidden" name="_method" value="post">

                        <input id="inputCourseId{{ $section->id }}" type="hidden" name="course_id"
                               value="{{ $section->course['id'] }}">

                        <input id="inputSectionId{{ $section->id }}" type="hidden" name="section_id"
                               value="{{ $section->id }}">

                        <input id="inputType{{ $section->id }}" type="hidden" name="type" value="VIDEO">

                        <div class="col-md-12">
                            <div class="form-group">
                                <label> Judul : </label>
                                <input id="materialTitle{{ $section->id }}" type="text" name="title"
                                       class="form-control" required>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label> Berkas (Video) : </label>
                                <input id="materialFile{{ $section->id }}" type="file" name="file" accept="video/*" required>
                            </div>
                        </div>

                    </fieldset>

                    <div id="materialprogress{{ $section->id }}" class="col-md-12" style="display: none;">
                        <h3>Mengunggah media...</h3>

                        <div class="progress progress-xs">
                            <div id="myprog{{ $section->id }}" class="progress-bar progress-bar-primary progress-bar-striped"
                                 role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="0"
                                 style="width: 0%">
                                <span class="sr-only">40% Complete (success)</span>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <i class="fa fa-close"></i>
                        Tutup
                    </button>
                    <button id="buttonSubmitMaterial" type="submit" class="btn btn-primary">
                        <i class="fa fa-save"></i>
                        Simpan
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
