<div class="modal fade" id="modalAddReference{{ $section->id }}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Tambah Referensi</h4>
            </div>

            <form id="formaddreference" onsubmit="onSubmitReference({{ $section->id }}); return false;"
                  enctype="multipart/form-data" method="POST" data-parsley-validate>
                <div class="modal-body">
                    <fieldset>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <input type="hidden" name="_method" value="post">

                        <input id="referenceSectionId{{ $section->id }}" type="hidden" name="section_id" value="{{ $section->id }}">

                        <div class="col-md-12">
                            <div class="form-group">
                                <label> Judul : </label>
                                <input type="text" id="referenceTitle{{ $section->id }}" name="title" class="form-control" required>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label> Berkas : </label>
                                <input id="referenceFile{{ $section->id }}" type="file" name="file" required>
                            </div>
                        </div>

                    </fieldset>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <i class="fa fa-close"></i>
                        Tutup
                    </button>
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-save"></i>
                        Simpan
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
