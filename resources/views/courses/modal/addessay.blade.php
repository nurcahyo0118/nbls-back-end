<div class="modal fade" id="modalAddEssay{{ $section->id }}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Tambah Essay</h4>
            </div>

            <form id="formaddessay" onsubmit="onSubmitEssay({{ $section->id }}); return false;"
                  enctype="multipart/form-data" method="POST" data-parsley-validate>
                <div class="modal-body">
                    <fieldset>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <input type="hidden" name="_method" value="post">

                        <input id="essaySectionId{{ $section->id }}" type="hidden" name="section_id" value="{{ $section->id }}">

                        <div class="col-md-12">
                            <div class="form-group">
                                <label> Judul : </label>
                                <input type="text" id="essayTitle{{ $section->id }}" name="title" class="form-control" required>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label> Essay : </label>
                                <textarea id="essayEssay{{ $section->id }}" name="essay" class="form-control" rows="10"></textarea>
                            </div>
                        </div>

                    </fieldset>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <i class="fa fa-close"></i>
                        Tutup
                    </button>
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-save"></i>
                        Simpan
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
