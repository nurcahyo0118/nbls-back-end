<tr id="exercise{{ $exercise->id }}">
    <td>{{ $exercise->title }}</td>
    <td>{{ $exercise->file }}</td>
    <td class="pull-right">
        <a target="_blank" href="{{ route('exercises.assessments.index', [$_SERVER['HTTP_HOST'], $exercise->id]) }}"
            class="btn btn-default btn-xs">
            <i class="fa fa-sort-numeric-asc"></i>
            Penilaian
        </a>
        <a target="_blank" href="{{ route('exercises.criterias.index', [$_SERVER['HTTP_HOST'], $exercise->id]) }}"
            class="btn btn-default btn-xs">
            <i class="fa fa-list"></i>
            Kriteria
        </a>
        <button class="btn btn-danger btn-xs" type="button"
                onclick="onDeleteExercise({{ $exercise->id }})">
            <i class="fa fa-trash"></i>
            Hapus
        </button>
    </td>
</tr>