@extends('layouts.main')

@section('content')

    <div class="row">

        @include('courses.modal.addsection')

        <div class="col-xs-12">

            @include('layouts.partials.message')

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Kelola Kursus</h3>
                    <br>
                </div>
                <div class="box-body">

                    <div class="board">

                        <div class="board-inner">
                            <ul class="nav nav-tabs">
                                <div class="liner"></div>
                                <li class="steps active">
                                    <a href="#generals" data-toggle="tab" onclick="renderHtml('generals')" title="Umum">
                                        <span class="round-tabs one">
                                            <i class="fa fa-home"></i>
                                        </span>
                                    </a>
                                </li>
                                <li class="steps">
                                    <a href="#sections" data-toggle="tab" onclick="renderHtml('sections')"
                                       title="Kursus">
                                        <span class="round-tabs two">
                                            <i class="fa fa-list-ol"></i>
                                        </span>
                                    </a>
                                </li>

                                <li class="steps">
                                    <a href="#quizzes" data-toggle="tab" onclick="renderHtml('quizzes')" title="Kuis">
                                        <span class="round-tabs three">
                                            <i class="fa fa-question"></i>
                                        </span>
                                    </a>
                                </li>

                                <li class="steps">
                                    <a href="#exercises" data-toggle="tab" onclick="renderHtml('exercises')"
                                       title="Latihan">
                                        <span class="round-tabs four">
                                            <i class="fa fa-edit"></i>
                                        </span>
                                    </a>
                                </li>
                                <li class="steps">
                                    <a href="#references" data-toggle="tab" onclick="renderHtml('references')"
                                       title="Referensi">
                                        <span class="round-tabs five">
                                            <i class="fa fa-link"></i>
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="generals">

                                <form id="course"
                                      action="{{ route('courses.update', [$_SERVER['HTTP_HOST'], $course->id]) }}"
                                      method="post"
                                      enctype="multipart/form-data"
                                      data-parsley-validate>

                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    <input type="hidden" name="_method" value="put">

                                    <div class="row">
                                        <div class="col-md-3">
                                            <ul class="nav nav-pills nav-stacked admin-menu">
                                                <li class="active">
                                                    <a href="#" data-target-id="general">
                                                        <i class="fa fa-file"></i>
                                                        Umum
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" data-target-id="detail">
                                                        <i class="fa fa-archive"></i>
                                                        Detail
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" data-target-id="image">
                                                        <i class="fa fa-file-o fa-fw"></i>
                                                        Gambar
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" data-target-id="video">
                                                        <i class="fa fa-bar-chart-o fa-fw"></i>
                                                        Video Promo
                                                    </a>
                                                </li>

                                            </ul>
                                        </div>

                                        {{-- General --}}
                                        <div class="col-md-9 admin-content" id="general">

                                            <fieldset>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label> Judul : </label>
                                                        <input class="form-control" type="text" name="title"
                                                               value="{{ $course->title }}"
                                                               placeholder="Judul kursus" required>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label> Deskripsi : </label>
                                                        <textarea class="textarea" name="description"
                                                                  placeholder="Tuliskan deskripsi kursus anda disini">{{ $course->description }}</textarea>
                                                    </div>
                                                </div>

                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label> Kategori : </label>
                                                        <select id="course_category_id"
                                                                onchange="onSelectCategory(value)" class="form-control"
                                                                name="category_id" required>
                                                            @foreach($categories as $category)
                                                                <option value="{{ $category->id }}" {{ $category->id === $course->category_id ? 'selected' : '' }}> {{ $category->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label> Sub Kategori : </label>
                                                        <select id="course_sub_category_id" class="form-control"
                                                                name="sub_category_id" required>
                                                            @foreach($sub_categories as $sub_category)
                                                                <option value="{{ $sub_category->id }}" {{ $sub_category->id === $course->sub_category_id ? 'selected' : '' }} > {{ $sub_category->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label> Tipe Kursus : </label>
                                                        <input type="radio" name="is_free" value="1" onclick="onSelectFree()" {{ $course->price != 0 ? '' : 'checked' }}>
                                                        Gratis
                                                        <input type="radio" name="is_free" value="0" onclick="onSelectPaid()" {{ $course->price != 0 ? 'checked' : '' }}>
                                                        Berbayar
                                                    </div>
                                                </div>

                                                <div id="coursePriceInput" class="col-md-5">
                                                    <div class="form-group">
                                                        <label> Harga (Rp) : </label>
                                                        <input class="form-control" type="number" name="price"
                                                               value="{{ $course->price }}">
                                                    </div>
                                                </div>

                                            </fieldset>

                                        </div>
                                        {{-- General --}}

                                        {{-- Detail --}}
                                        <div class="col-md-9 admin-content" id="detail">

                                            <fieldset>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label> Siapa yang cocok : </label>
                                                        <textarea class="textarea" name="suitable"
                                                                  placeholder="Tuliskan siapa yang cocok dengan kursus anda disini">
                                                            {{ $course->suitable }}
                                                        </textarea>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label> Syarat : </label>
                                                        <textarea class="textarea" name="requirement"
                                                                  placeholder="Tuliskan syarat untuk mengikuti kursus anda disini">
                                                            {{ $course->requirement }}
                                                        </textarea>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label> Yang akan didapat murid : </label>
                                                        <textarea class="textarea" name="can_be"
                                                                  placeholder="Tuliskan apa yang akan murid dapat setelah mengikuti kursus anda disini">
                                                            {{ $course->can_be }}
                                                        </textarea>
                                                    </div>
                                                </div>

                                            </fieldset>

                                        </div>
                                        {{-- Detail --}}

                                        {{-- Images --}}
                                        <div class="col-md-9 admin-content" id="image">

                                            <fieldset>
                                                <div class="col-md-12 text-center">
                                                    @if( $course->image === null)
                                                        <img id="courseImage" src="/xxcourses/images/default.png" class="img-responsive"
                                                             alt="">
                                                    @else
                                                        <img id="courseImage" src="/xxcourses/images/{{ $course->image }}"
                                                             class="img-responsive" width="400" alt="">
                                                    @endif
                                                </div>

                                                <div class="col-md-12">
                                                    <label>Berkas ( Gambar ) : </label>
                                                    <div class="form-group">
                                                        <div class="btn btn-default btn-file">
                                                            <i class="fa fa-paperclip"></i>
                                                            Upload Gambar
                                                            <input id="inputCourseImage" type="file" name="image" accept="image/*">
                                                        </div>
                                                        <button id="saveCourseImage" type="submit" class="btn btn-primary">
                                                            <i class="fa fa-save"></i>
                                                            Simpan Perubahan
                                                        </button>
                                                    </div>
                                                </div>

                                            </fieldset>

                                        </div>
                                        {{-- Images --}}

                                        {{-- Video --}}
                                        <div class="col-md-9 admin-content" id="video">
                                            <fieldset>
                                                <div class="col-md-12 text-center">
                                                    @if($course->video != null)
                                                        <iframe width="800" height="500" src="{{ str_replace('watch?v=', 'embed/', $course->video) }}" frameborder="0" allowfullscreen></iframe>
                                                    @endif
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Video ( URL Video ) : </label>
                                                        <input type="url" class="form-control" name="video" value="{{ $course->video }}">
                                                    </div>
                                                </div>

                                            </fieldset>
                                        </div>
                                        {{-- Video --}}

                                    </div>

                                    <hr>

                                    <div class="col-md-12 text-right">
                                        <a href="{{ route('courses.index', $_SERVER['HTTP_HOST']) }}"
                                           class="btn btn-default">
                                            <i class="fa fa-close"></i>
                                            Tutup
                                        </a>
                                        <button type="submit" class="btn btn-primary" onclick="submit()">
                                            <i class="fa fa-save"></i>
                                            Simpan
                                        </button>
                                    </div>

                                </form>

                            </div>

                            <div class="tab-pane fade" id="sections">

                                <div id="sectionsBody"></div>

                            </div>

                            <div class="tab-pane fade" id="quizzes">

                                <div id="quizzesBody"></div>

                            </div>

                            <div class="tab-pane fade" id="exercises">

                                <div id="exercisesBody"></div>

                            </div>

                            <div class="tab-pane fade" id="references">

                                <div id="referencesBody"></div>

                            </div>

                            <div class="clearfix"></div>

                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection

@section('script')
    <script>
        $(function () {

            {{--  $('#materialprogress').hide();  --}}

            $('#saveCourseImage').hide();

            $("form#editquiz").submit(function (e) {
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('question', 'pertanyaan');

                $.ajax({
                    url: '/quizzes/' + formData.get('id'),
                    type: 'PUT',
                    data: formData,
                    success: function (data) {
                        swal("Berhasil !", "Kuis berhasil ditambahkan", "success");
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });

            });

            // On Image Change
            $('#inputCourseImage').change(function() {
            if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#courseImage').attr('src', e.target.result);
                }

                reader.readAsDataURL(this.files[0]);

                $('#saveCourseImage').show();
            }
        });

        });
    </script>

    <script>
        @if($course->price != 0)
            onSelectPaid();
        @else
            onSelectFree();
        @endif

        function onSelectFree() {
            document.getElementById('coursePriceInput').style.display = 'none';
        }

        function onSelectPaid() {
            document.getElementById('coursePriceInput').style.display = 'block';
        }
    </script>

    <script>

        function onSelectCategory(value) {

            var courseCategoryId = $('#course_category_id');
            var courseSubCategoryId = $('#course_sub_category_id');

            $.ajax({
                type: 'GET',
                url: '/loads/subcategories/' + value,
                success: function (response) {
                    console.log(response);

                    courseSubCategoryId.empty();

                    for (let subcategory of response) {
                        console.log(subcategory);
                        courseSubCategoryId.append('<option value="' + subcategory.id + '">' + subcategory.name + '</option>');
                    }

                },
                error: function (error) {
                    console.log(error);
                    {{--  $('#allCourseList').replaceWith('<div id="allCourseList" class="text-center">Course Not Found.</div>');  --}}
                }
            });

        }

        function onSubmitEssay(sectionId) {
            var formData = new FormData();

            var title = $('#essayTitle' + sectionId).val();
            var essay = $('#essayEssay' + sectionId).val();

            formData.append('_token', '{{ csrf_token() }}');
            formData.append('section_id', sectionId);
            formData.append('title', title);
            formData.append('essay', essay);

            $.ajax({
                url: '/essays',
                type: 'POST',
                data: formData,
                success: function (data) {

                    console.log(data);

                    $('#essayList' + sectionId).append(data);
                    $('form#formaddessay')[0].reset();
                    $('#modalAddEssay' + sectionId).modal('toggle');

                    swal("Berhasil !", "Latihan berhasil ditambahkan", "success");
                },
                error: function (error) {
                    swal("Gagal !", "Latihan gagal ditambahkan", "error");
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onSubmitExcercise(sectionId) {
            var formData = new FormData();

            var title = $('#exerciseTitle' + sectionId).val();
            var file = $('#exerciseFile' + sectionId)[0].files[0];

            formData.append('_token', '{{ csrf_token() }}');
            formData.append('section_id', sectionId);
            formData.append('title', title);
            formData.append('file', file);

            $.ajax({
                url: '/exercises',
                type: 'POST',
                data: formData,
                success: function (data) {

                    console.log(data);

                    $('#sectionExercisesList' + sectionId).append(data);
                    $('form#formaddexercise')[0].reset();
                    $('#modalAddExercise' + sectionId).modal('toggle');
                    swal("Berhasil !", "Latihan berhasil ditambahkan", "success");
                },
                error: function (error) {
                    swal("Gagal !", "Latihan gagal ditambahkan", "error");
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onSubmitReference(sectionId) {
            var formData = new FormData();

            var title = $('#referenceTitle' + sectionId).val();
            var file = $('#referenceFile' + sectionId)[0].files[0];

            formData.append('_token', '{{ csrf_token() }}');
            formData.append('section_id', sectionId);
            formData.append('title', title);
            formData.append('file', file);

            $.ajax({
                url: '/references',
                type: 'POST',
                data: formData,
                success: function (data) {

                    console.log(data);

                    $('#sectionReferencesList' + sectionId).append(data);
                    $('form#formaddreference')[0].reset();
                    $('#modalAddReference' + sectionId).modal('toggle');

                    swal("Berhasil !", "Referensi berhasil ditambahkan", "success");
                },
                error: function (error) {
                    swal("Gagal !", "Referensi gagal ditambahkan", "error");
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onSubmitQuiz(materialId) {
            console.log('Your form submitted');

            var formData = new FormData();

            var materialId = $('#inputMaterialId' + materialId).val();
            var quizQuestion = $('#inputQuizQuestion' + materialId).val();
            var quizA = $('#inputQuizA' + materialId).val();
            var quizB = $('#inputQuizB' + materialId).val();
            var quizC = $('#inputQuizC' + materialId).val();
            var quizD = $('#inputQuizD' + materialId).val();
            var quizOptionTrue = $('#inputQuizTrue' + materialId + ' option:selected').val();
            var quizMinute = $('#inputQuizMinute' + materialId).val();
            var quizSecond = $('#inputQuizSecond' + materialId).val();

            formData.append('_token', '{{ csrf_token() }}');
            formData.append('material_id', materialId);
            formData.append('question', quizQuestion);
            formData.append('option_a', quizA);
            formData.append('option_b', quizB);
            formData.append('option_c', quizC);
            formData.append('option_d', quizD);
            formData.append('option_true', quizOptionTrue);
            formData.append('minute', quizMinute);
            formData.append('second', quizSecond);

            $.ajax({
                url: '/quizzes',
                type: 'POST',
                data: formData,
                success: function (data) {
                    console.log(data.view);
                    $('#quizzesList' + materialId).append(data.view);
                    $('form#formaddquiz')[0].reset();
                    $('#modalAddQuiz' + materialId).modal('toggle');
                    swal("Berhasil !", "Kuis berhasil ditambahkan", "success");
                },
                error: function (error) {
                    swal("Gagal !", "Kuis gagal ditambahkan", "error");
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onSubmitMaterial(sectionId) {

            $('#materialprogress' + sectionId).show();
            var formData = new FormData();

            $('#buttonSubmitMaterial').attr('disabled', 'disabled');

            // Prepare data
            var courseId = $('#inputCourseId' + sectionId).val();
            console.log(courseId);

            var sectionId = $('#inputSectionId' + sectionId).val();
            console.log(sectionId);

            var type = $('#inputType' + sectionId).val();
            console.log(type);

            var materialTitle = $('#materialTitle' + sectionId).val();
            console.log(materialTitle);

            var materialFile = $('#materialFile' + sectionId)[0].files[0];
            console.log(materialFile);

            formData.append('_token', '{{ csrf_token() }}');
            formData.append('course_id', courseId);
            formData.append('section_id', sectionId);
            formData.append('type', type);
            formData.append('title', materialTitle);
            formData.append('file', materialFile);

            $.ajax({
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();

                    // Upload progress
                    xhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            //Do something with upload progress
                            console.log('percentComplete = ' + percentComplete);
                            $('#myprog' + sectionId).width(percentComplete + '%');
                        }
                    }, false);

                    // Download progress
                    xhr.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            // Do something with download progress
                            console.log(percentComplete);
                        }
                    }, false);

                    return xhr;
                },
                url: '/material/store',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#myprog' + sectionId).width('100%');

                    $('#materialsList' + data.section_id).append(data.view);
                    $('#modalAddMaterial' + data.section_id).modal('hide');
                    $('#buttonSubmitMaterial').removeAttr("disabled")
                    $('form#addmaterial' + sectionId)[0].reset();
                    $('#materialprogress' + sectionId).hide();

                    swal("Berhasil !", "Materi berhasil ditambahkan", "success");
                },
                error: function (error) {
                    $('#materialprogress' + sectionId).hide();

                    swal("Gagal !", "Materi gagal ditambahkan", "error");
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onSubmit(id) {

            console.log('Clicked');
            var title = $('#title');
            var description = $('#description');
            var course_id = $('#course_id');
            var wistia_project_id = $('#wistia_project_id');

            $.ajax({
                url: '/sections',
                type: 'POST',
                data: JSON.stringify({
                    _token: '{{ csrf_token() }}',
                    title: title.val(),
                    description: description.val(),
                    course_id: course_id.val(),
                    wistia_hashed_id: wistia_project_id.val()
                }),
                success: function (data) {
                    $("#sectionsList").append(data);
                    $('#modalAddSection').modal('toggle');
                    $('form#addsection')[0].reset();
                    swal("Berhasil !", "Modul berhasil ditambahkan", "success");
                },
                error: function (error) {
                    swal("Gagal !", "Modul gagal ditambahkan", "error");
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function submit() {
            $("#course").submit();
        }

        function onDeleteSection(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "Data yang telah di hapus tidak dapat dikembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    deleteSection(id)
                }
            });
        }

        function onDeleteMaterial(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "Data yang telah di hapus tidak dapat dikembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    deleteMaterial(id);
                }
            });
        }

        function onDeleteQuiz(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "Data yang telah di hapus tidak dapat dikembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    deleteQuiz(id);
                }
            });
        }

        function onDeleteEssay(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "Data yang telah di hapus tidak dapat dikembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {

                    $.ajax({
                        url: '/essays/' + id,
                        type: 'DELETE',
                        data: null,
                        success: function (data) {
                            $("#essay" + id).remove();

                            swal("Berhasil !", "Essay berhasil dihapus", "success");
                        },
                        error: function (error) {
                            swal("Gagal !", "Essay gagal dihapus", "error");
                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    });

                }
            });
        }

        function onDeleteAllSection(courseId) {
            swal({
                title: "Apa kamu yakin?",
                text: "Data yang telah di hapus tidak dapat dikembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    deleteAllSection(courseId);
                }
            });
        }

        /* Service */
        function deleteSection(id) {
            $.ajax({
                type: "DELETE",
                url: '/sections/' + id,
                success: function (response) {
                    $("#section" + id).remove();
                    swal("Berhasil !", "Modul berhasil dihapus", "success");
                },
                error: function(error) {
                    swal("Gagal !", "Modul gagal dihapus", "error");
                }
            });
        }

        function deleteMaterial(id) {
            $.ajax({
                type: "DELETE",
                url: '/material/delete/' + id,
                success: function (response) {
                    $("#material" + id).remove();
                    swal("Berhasil !", "Materi berhasil dihapus", "success");
                }
            });
        }

        function onDeleteReference(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "Data yang telah di hapus tidak dapat dikembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type: "DELETE",
                        url: '/references/' + id,
                        success: function (response) {
                            $("#reference" + id).remove();
                            swal("Berhasil !", "Referensi berhasil dihapus", "success");
                        }
                    });
                }
            });
        }

        function onDeleteExercise(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "Data yang telah di hapus tidak dapat dikembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type: "DELETE",
                        url: '/exercises/' + id,
                        success: function (response) {
                            $("#exercise" + id).remove();
                            swal("Berhasil !", "Latihan berhasil dihapus", "success");
                        }
                    });
                }
            });
        }

        function deleteAllSection(courseId) {
            $.ajax({
                type: "DELETE",
                url: '/sections/delete-all/' + courseId,
                success: function (response) {
                    $("#sectionsList").empty();
                    swal("Berhasil !", "Semua Modul berhasil dihapus", "success");
                },
                error: function (error) {
                    swal("Gagal !", "Gagal menghapus semua modul", "error");
                }
            });
        }

        function deleteQuiz(id) {
            $.ajax({
                type: "DELETE",
                url: '/quizzes/' + id,
                success: function (response) {
                    console.log(response);
                    $("#quiz" + id).remove();
                    swal("Berhasil !", "Modul berhasil dihapus", "success");
                }
            });
        }

        function renderHtml(htmlTitle) {
            console.log(htmlTitle);

            switch (htmlTitle) {
                case 'generals':
                    $('#generalsBody').replaceWith('<div id="generalsBody" class="text-center"><i class="fa fa-spinner fa-spin fa-5x"></i></div>');

                    $.ajax({
                        type: 'GET',
                        url: '/courses/{{ $course->id }}/sections-page',
                        success: function (response) {
                            console.log(response);
                            $('#generalsBody').replaceWith(response);
                        }
                    });
                    break;

                case 'sections':
                    $('#sectionsBody').replaceWith('<div id="sectionsBody" class="text-center"><i class="fa fa-spinner fa-spin fa-5x"></i></div>');

                    $.ajax({
                        type: 'GET',
                        url: '/courses/{{ $course->id }}/sections-page',
                        success: function (response) {
                            console.log(response);
                            $('#sectionsBody').replaceWith(response);
                        }
                    });
                    break;

                case 'quizzes':
                    $('#quizzesBody').replaceWith('<div id="quizzesBody" class="text-center"><i class="fa fa-spinner fa-spin fa-5x"></i></div>');

                    $.ajax({
                        type: 'GET',
                        url: '/courses/{{ $course->id }}/quizzes-page',
                        success: function (response) {
                            console.log(response);
                            $('#quizzesBody').replaceWith(response);
                        }
                    });
                    break;

                case 'exercises':
                    $('#exercisesBody').replaceWith('<div id="exercisesBody" class="text-center"><i class="fa fa-spinner fa-spin fa-5x"></i></div>');

                    $.ajax({
                        type: 'GET',
                        url: '/courses/{{ $course->id }}/exercises-page',
                        success: function (response) {
                            console.log(response);
                            $('#exercisesBody').replaceWith(response);
                        }
                    });
                    break;

                case 'references':
                    $('#referencesBody').replaceWith('<div id="referencesBody" class="text-center"><i class="fa fa-spinner fa-spin fa-5x"></i></div>');

                    $.ajax({
                        type: 'GET',
                        url: '/courses/{{ $course->id }}/references-page',
                        success: function (response) {
                            console.log(response);
                            $('#referencesBody').replaceWith(response);
                        }
                    });
                    break;
            }

        }
    </script>

    <script>
        $(function () {
            $('.textarea').wysihtml5();
        })
    </script>

    <script>
        $(document).ready(function () {
            var navItems = $('.admin-menu li > a');
            var navListItems = $('.admin-menu li');
            var allWells = $('.admin-content');
            var allWellsExceptFirst = $('.admin-content:not(:first)');

            allWellsExceptFirst.hide();
            navItems.click(function (e) {
                e.preventDefault();
                navListItems.removeClass('active');
                $(this).closest('li').addClass('active');


                allWells.fadeOut();
                var target = $(this).attr('data-target-id');
                $('#' + target).fadeIn();
            });

        });
    </script>

@endsection
