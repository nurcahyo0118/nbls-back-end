<tr id="reference{{ $reference->id }}">
    <td>{{ $reference->title }}</td>
    <td>{{ $reference->file }}</td>
    <td class="pull-right">
        <button class="btn btn-danger btn-xs" type="button"
                onclick="onDeleteReference({{ $reference->id }})">
            <i class="fa fa-trash"></i>
            Hapus
        </button>
    </td>
</tr>