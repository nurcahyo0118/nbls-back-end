<div class="col-md-12">

    <button onclick="onDeleteAllSection({{ $course->id }})"
            class="btn btn-danger pull-right">
        <i class="fa fa-trash"></i>
        Hapus Semua Kuis
    </button>
</div>

<br>
<br>

<div id="sectionsList" class="col-md-12">
    @foreach($course->sections as $index => $section)
        <div id="section{{ $section->id }}">
            <div class="box box-primary box-solid collapsed-box">
                <div class="box-header with-border">
                    <h3 class="box-title"><b>Modul {{ $section->line }}
                                             : </b>{{ $section->title }}</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool"
                                data-widget="collapse">
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>

                </div>

                <div class="box-body">

                    <div class="col-md-12">
                        <button data-toggle="modal" data-target="#modalAddQuiz{{ $section->id }}"
                                class="btn btn-info">
                            <i class="fa fa-plus"></i>
                            Tambah Kuis
                        </button>
                        <br>
                        <br>
                    </div>

                    <div class="materialList">
                        @foreach($section->materials as $material)
                            @include('courses.quiz.childmaterial')
                        @endforeach
                    </div>

                </div>

            </div>
        </div>

        @include('courses.modal.addquiz')

    @endforeach
</div>