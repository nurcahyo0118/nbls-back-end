<div id="exercisesBody">
    <div id="exerciseSectionsList" class="col-md-12">

        @foreach($course->sections as $index => $section)

            @include('courses.exercise.childsection')

            @include('courses.modal.addexercise')

            @include('courses.modal.addessay')

        @endforeach
    </div>
</div>