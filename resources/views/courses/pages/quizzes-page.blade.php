<div id="quizzesBody">
    <div id="sectionsList" class="col-md-12">
        @foreach($course->sections as $index => $section)

            @include('courses.quiz.childsection')

        @endforeach
    </div>
</div>