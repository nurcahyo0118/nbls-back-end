<div id="referencesBody">
    <div id="referenceSectionsList" class="col-md-12">
        @foreach($course->sections as $index => $section)

            @include('courses.reference.childsection')

            @include('courses.modal.addreference')

        @endforeach
    </div>
</div>