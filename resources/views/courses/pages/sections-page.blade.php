<div id="sectionsBody">
    <div class="col-md-12">
        <button data-toggle="modal" data-target="#modalAddSection" class="btn btn-primary">
            <i class="fa fa-plus"></i>
            Tambah Modul
        </button>

        <button onclick="onDeleteAllSection({{ $course->id }})"
                class="btn btn-danger pull-right">
            <i class="fa fa-trash"></i>
            Hapus Semua Modul
        </button>
    </div>

    <br>
    <br>

    <div id="sectionsList" class="col-md-12">
        @foreach($course->sections as $index => $section)
            @include('courses.childsection')
        @endforeach
    </div>

</div>