<div id="section{{ $section->id }}">
    <div class="box box-primary box-solid collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title"><b>{{ $section->title }}</b></h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool"
                        data-widget="collapse">
                    <i class="fa fa-plus"></i>
                </button>
            </div>

        </div>

        <div class="box-body">
            <div class="col-md-12">

                <div class="form-group">
                    <label>Deskripsi : </label>
                    <p></p>
                    {{ $section->description }}
                </div>
            </div>

            <div class="col-md-12">
                <button data-backdrop="static" data-keyboard="false" data-toggle="modal" data-id="{{ $section->id }}"
                        data-target="#modalAddReference{{ $section->id }}"
                        class="btn btn-info">
                    <i class="fa fa-plus"></i>
                    Tambah Referensi
                </button>
                <br>
                <br>

                <table class="table">
                    <thead>
                    <tr>
                        <th>Judul</th>
                        <th>Berkas</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody id="sectionReferencesList{{ $section->id }}">
                    @foreach($section->references as $reference)
                        @include('courses.child-reference')
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

    </div>
</div>
