<div id="material{{ $material->id }}" class="col-md-12">

    <div class="box box-info box-solid collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title"><b>{{ $material->title }}</b></h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool"
                        data-widget="collapse">
                    <i class="fa fa-plus"></i>
                </button>
            </div>

        </div>

        <div class="box-body">

            <label>Daftar Referensi</label>

            <br>

            <button id="modaladdquiz" data-toggle="modal" data-id="{{ $material->id }}" data-target="#modalAddQuiz"
                    class="btn btn-info">
                <i class="fa fa-plus"></i>
                Tambah Referensi
            </button>

            @include('courses.modal.addquiz')

            <br>
            <br>

            <div class="referencesList{{ $material->id }}">

            </div>

        </div>

        <div class="box-footer">
            <div class="col-md-12 text-right">
                <button class="btn btn-danger" type="button"
                        onclick="onDeleteMaterial({{ $material->id }})">
                    <i class="fa fa-trash"></i>
                    Hapus
                </button>
            </div>
        </div>

    </div>

</div>