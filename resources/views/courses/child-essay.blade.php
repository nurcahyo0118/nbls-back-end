<div id="essay{{ $essay->id }}">
    <div class="box box-info box-solid collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title"><b>Essay : {{ $essay->title }}</b></h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool"
                        data-widget="collapse">
                    <i class="fa fa-plus"></i>
                </button>

            </div>

        </div>

        <div class="box-body">
            <label>Essay : </label>
            <p>{{ $essay->essay }}</p>
        </div>

        <div class="box-footer text-right">
            {{--  <button data-toggle="modal" data-target="#modalEditEssay{{ $essay->id }}" class="btn btn-default">Edit</button>  --}}
            <button onclick="onDeleteEssay({{ $essay->id }})" type="button" class="btn btn-danger">Hapus</button>
        </div>

    </div>

</div>
    