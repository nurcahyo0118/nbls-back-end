<div id="section{{ $section->id }}">
    <div class="box box-primary box-solid collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title"><b>{{ $section->title }}</b></h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool"
                        data-widget="collapse">
                    <i class="fa fa-plus"></i>
                </button>
            </div>

        </div>

        <div class="box-body">
            <div class="col-md-12">

                <div class="form-group">
                    <label>Deskripsi : </label>
                    <p></p>
                    {{ $section->description }}
                </div>
            </div>

            <div id="materialsList{{ $section->id }}">
                @foreach($section->materials as $index => $material)

                    @include('courses.quiz.childmaterial')

                @endforeach

            </div>

        </div>

    </div>
</div>