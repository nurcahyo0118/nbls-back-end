<div id="quiz{{ $quiz->id }}" class="col-md-12">
    <div class="box box-info box-solid collapsed-box">
        <div class="box-header with-border">

            <h3 class="box-title"><b>Kuis</b></h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool"
                        data-widget="collapse">
                    <i class="fa fa-plus"></i>
                </button>
            </div>

        </div>

        <div class="box-body">
            <label>pertanyaan</label>
            <p>{{ $quiz->question }}</p>

            <hr>

            <label>pilihan</label>
            <p>A : {{ $quiz->option_a }}</p>
            <p>B : {{ $quiz->option_b }}</p>
            <p>C : {{ $quiz->option_c }}</p>
            <p>D : {{ $quiz->option_d }}</p>

            <hr>

            <label>Jawaban</label>
            <p>{{ $quiz->option_true }}</p>

            <hr>

            <label>Waktu ditampilkan</label>
            <p>{{ $quiz->minute }}:{{ $quiz->second }}</p>
        </div>

        <div class="box-footer">
            <div class="col-md-12 text-right">
                <button class="btn btn-danger" type="button"
                        onclick="onDeleteQuiz({{ $quiz->id }})">
                    <i class="fa fa-trash"></i>
                    Hapus
                </button>
            </div>
        </div>

    </div>
</div>

@include('courses.modal.editquiz')