<div id="material{{ $material->id }}" class="col-md-12">

    <div class="box box-info box-solid collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title"><b>{{ $material->title }}</b></h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool"
                        data-widget="collapse">
                    <i class="fa fa-plus"></i>
                </button>
            </div>

        </div>

        <div class="box-body">

            <label>Daftar Kuis</label>

            <br>

            <button data-backdrop="static" data-keyboard="false" data-toggle="modal" data-id="{{ $material->id }}"
                    data-target="#modalAddQuiz{{ $material->id }}"
                    class="btn btn-info">
                <i class="fa fa-plus"></i>
                Tambah Kuis
            </button>

            <br>
            <br>

            <div id="quizzesList{{ $material->id }}">
                @foreach($material->quizzes as $quizzesindex => $quiz)

                    @include('courses.quiz.childquiz')

                @endforeach

            </div>

        </div>

    </div>

</div>

@include('courses.modal.addquiz')