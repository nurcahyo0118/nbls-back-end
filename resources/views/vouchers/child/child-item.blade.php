<tr id="voucher{{ $voucher->id }}">
    <td>{{ $voucher->code }}</td>
    <td>{{ $voucher->discount }}%</td>
    <td>{{ $voucher->expired_date }}</td>
    <td class="text-right">
        <a href="{{ route('vouchers.edit', [$_SERVER['HTTP_HOST'], $voucher->id]) }}"
           class="btn btn-default btn-xs">
            <i class="fa fa-edit"></i>
            Edit
        </a>
        <a onclick="onDeleteVoucher({{ $voucher->id }})" class="btn btn-danger btn-xs">
            <i class="fa fa-trash"></i>
            Hapus
        </a>
    </td>
</tr>