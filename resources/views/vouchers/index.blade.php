@extends('layouts.main')

@section('content')

    @php
        $tenant = Auth::user()->tenant;
        $permission = App\Permission::where(['role_id' => Auth::user()->role['id'], 'tenant_id' => $tenant->id])->first();
    @endphp

    <div class="row">
        <div class="col-xs-12">

            @include('layouts.partials.message')

            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Daftar Voucher</h3>
                    <br>

                    <a class="btn btn-info pull-left col-md-1"
                        href="{{ route('vouchers.create', $_SERVER['HTTP_HOST']) }}">
                        <span class="fa fa-plus"></span>
                    </a>

                </div>

                <div class="box-body">
                    <table class="table js-basic-example dataTable">
                        <thead>
                        <tr>
                            <th>Kode Voucher</th>
                            <th>Diskon</th>
                            <th>Tanggal Kadaluarsa</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($vouchers as $voucher)

                            @include('vouchers.child.child-item')

                        @endforeach
                        </tbody>
                    </table>

                </div>

            </div>

        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        function onDeleteVoucher(voucherId) {
            swal({
                title: "Apa kamu yakin?",
                text: "Data yang telah di hapus tidak dapat dikembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {

                    $.ajax({
                        url: '/vouchers/' + voucherId,
                        type: 'DELETE',
                        success: function (response) {
                            $('#voucher' + voucherId).remove();
                            swal("Berhasil !", "Voucher berhasil dihapus", "success");
                        },
                        error: function (error) {
                            swal("Berhasil !", "Voucher gagal dihapus", "error");
                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    });

                }
            });
        }
    </script>
@endsection