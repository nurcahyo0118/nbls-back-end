@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-xs-12">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            

            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Tambah Voucher</h3>

                </div>

                <div class="box-body">

                    <div class="col-md-12">
                        <form action="{{ route('vouchers.store', $_SERVER['HTTP_HOST']) }}" method="post"
                              class="form-horizontal" enctype="multipart/form-data">

                            {{ csrf_field() }}

                            @php
                                $code = Illuminate\Support\Str::random(10);
                            @endphp

                            <fieldset>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label> Kode Voucher : </label>
                                        <input type="text" class="form-control" value="{{ $code }}" disabled required>
                                        <input type="hidden" name="code" value="{{ $code }}" required>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label> Tanggal Kadaluarsa : </label>
                                        <input type="date" name="expired_date" class="form-control" id="datepicker" required>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label> Diskon : </label>
                                        <div class="input-group">
                                            <input type="number" name="discount" max="99" class="form-control" required>
                                            <span class="input-group-addon">
                                                %
                                            </span>
                                        </div>
                                    </div>
                                </div>

                            </fieldset>

                            <div class="modal-footer">
                                <a href="{{ route('vouchers.index', $_SERVER['HTTP_HOST']) }}"
                                   class="btn btn-default">
                                    <i class="fa fa-close"></i>
                                    Tutup
                                </a>
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-save"></i>
                                    Simpan
                                </button>
                            </div>
                        </form>
                    </div>

                </div>

            </div>

        </div>
    </div>
@endsection

@section('script')
$(function() {
    $('#datepicker').datepicker({
        autoclose: true
    });
});
@endsection