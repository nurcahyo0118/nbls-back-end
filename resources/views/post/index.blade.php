@extends('layouts.frontend')

@section('content')


    <div class="col-md-8 col-lg-6">
        <img src="/images/nbls-profile-header.JPG" class="img-fluid" alt="" height="50px">

        <div class="row">
            <div class="col-lg-12">
                <div class="search-result-box">
                    <ul class="nav nav-tabs">
                        <li class="nav-item col-md-4 p-0 ">
                            <a href="#home" data-toggle="tab" aria-expanded="true" class="nav-link active">
                                <p class="fa fa-comments-o fa-2x"></p>
                                <b>Status</b>
                            </a>
                        </li>
                        <li class="nav-item col-md-4 p-0">
                            <a href="#users" data-toggle="tab" aria-expanded="false" class="nav-link">
                                <p class="fa fa-question fa-2x"></p>
                                <b>Tanya</b>
                            </a>
                        </li>
                        <li class="nav-item col-md-4 p-0">
                            <a href="#other-tab" data-toggle="tab" aria-expanded="false" class="nav-link">
                                <p class="fa fa-sitemap fa-2x"></p>
                                <b>Hasil Karya</b>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="home">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>What's Happening?</h4>
                                    <hr>
                                </div>
                                <div class="col-md-2 m-b-20">
                                    <img class="d-flex mr-3 rounded-circle" src="/images/users/avatar-1.jpg"
                                         alt="Generic placeholder image" height="54">
                                </div>
                                <div class="col-md-10 m-b-20">
                                <!-- <form class="" action="{{ route('post.store', $_SERVER['HTTP_HOST']) }}" method="post">
                                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                          <textarea name="name" class="item-fluid" placeholder="Share something"></textarea>
                                          <button type="button" class="btn btn-primary float-right" name="button">SEND</button>
                                          </form> -->

                                    <form class="" action="{{ route('post.store', $_SERVER['HTTP_HOST']) }}"
                                          method="post"
                                          data-parsley-validate>

                                        <fieldset>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <textarea type="text" name="status" class="form-control" rows="4"
                                                              placeholder="Share something" required></textarea>
                                                </div>
                                            </div>


                                </div>

                                <div class="col-md-12">
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <ul class="nav nav-pills profile-pills m-t-10">
                                                <li>
                                                    <a data-toggle="tooltip" data-placement="top" title="choose photos"
                                                       href="#">
                                                        <i class="fa fa-camera"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a data-toggle="tooltip" data-placement="top" title="flip max 50 kb"
                                                       href="#">
                                                        <i class="fa fa-paperclip"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a data-toggle="tooltip" data-placement="top" title="link" href="#">
                                                        <i class="fa fa-link"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-2">
                                            <button type="submit" class="btn btn-primary float-right" name="button">
                                                SEND
                                            </button>
                                        </div>
                                        </fieldset>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <!-- end All results tab -->


                        <!-- Users tab -->
                        <div class="tab-pane" id="users">
                            <div class="search-item">
                                <div class="media">
                                    <img class="d-flex mr-3 rounded-circle" src="/images/users/avatar-1.jpg"
                                         alt="Generic placeholder image" height="54">
                                    <div class="media-body">
                                        <h5 class="media-heading">
                                            <a href="#" class="text-dark">Chadengle</a>
                                        </h5>
                                        <p class="font-13">
                                            <b>Email:</b>
                                            <span>
                                                <a href="#" class="text-muted">mediaheader@mail.com</a>
                                            </span>
                                        </p>
                                        <p class="m-b-0 font-13">
                                            <b>Bio:</b>
                                            <br/>
                                            <span class="text-muted">Cras sit amet nibh libero, in gravida nulla. Nulla
                                                                     vel metus scelerisque ante sollicitudin commodo.
                                                                     Cras purus odio, vestibulum in vulputate at, tempus
                                                                     viverra turpis. Fusce condimentum nunc ac nisi
                                                                     vulputate fringilla. Donec lacinia congue felis in
                                                                     faucibus.
                                            </span>
                                        </p>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- end Users tab -->

                        <div class="tab-pane" id="other-tab">
                            <p>
                                Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo
                                rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis
                                pretium.
                                Integer tincidunt.Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend
                                tellus.
                                Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.
                            </p>

                        </div>

                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-12">
                <div class="search-result-box">
                    <ul class="nav nav-tabs">
                        <li class="nav-item col-md-6 p-0">
                            <a href="#timeline" data-toggle="tab" aria-expanded="true" class="nav-link active">
                                <b>TimeLine</b>
                            </a>
                        </li>
                        <li class="nav-item col-md-6 p-0">
                            <a href="#newquestion" data-toggle="tab" aria-expanded="false" class="nav-link">
                                <b>NewQuestion</b>
                            </a>
                        </li>

                    </ul>

                    <!-- timeline -->

                    <div class="tab-content">
                        <div class="tab-pane active" id="timeline">
                            <div class="row">
                                @foreach($post as $post)
                                    <div class="col-lg-12">
                                        <div class=" m-t-20">

                                            <div class="media m-b-30">
                                                <img class="d-flex mr-3 rounded-circle thumb-sm"
                                                     src="/images/users/avatar-2.jpg" alt="Generic placeholder image">
                                                <div class="media-body">
                                                    <span class="media-meta pull-right text-primary" href="#"
                                                          role="button" id="dropdownMenuLink" data-toggle="dropdown"
                                                          aria-haspopup="true" aria-expanded="false">
                                                        <i class="fa fa-bookmark"></i>
                                                    </span>
                                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                        <a class="dropdown-item" data-toggle="modal"
                                                           data-target="#modalEdit{{$post->id}}">Edit
                                                        </a>
                                                        <a class="dropdown-item" data-toggle="modal"
                                                           data-target="#modalDelete{{ $post->id }}">Delete
                                                        </a>
                                                    </div>
                                                    <h4 class="text-primary font-16 m-0">
                                                        Chris Greyson
                                                        <small class="text-muted">
                                                            <i class="ion-clock"></i>
                                                            {{$post->created_at}}
                                                        </small>
                                                    </h4>

                                                </div>
                                            </div>
                                            <p>{{$post->status}}</p>

                                            <hr/>

                                            <div class="row">
                                                <div class="col-md-2">
                                                    <i class="fa fa-thumbs-o-up "> 10</i>
                                                </div>

                                                <div class="col-md-5">
                                                    <p class="fa fa-comments-o"> 15 </p>
                                                </div>
                                            </div>
                                            <hr>

                                            <div>
                                                <div class="media m-b-30">
                                                    <img class="d-flex mr-3 rounded-circle thumb-sm"
                                                         src="/images/users/avatar-3.jpg"
                                                         alt="Generic placeholder image">
                                                    <div class="media-body">
                                                        <input type="text" class="form-control" id=""
                                                               placeholder="Jadilah yang pertama menjawab...">

                                                    </div>
                                                </div>
                                                <hr>
                                            </div>

                                        </div>
                                    </div>


                                    <!-- Modal -->
                                    <div class="modal fade" id="modalEdit{{$post->id}}" tabindex="-1" role="dialog"
                                         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLongTitle">Edit Status</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form class=""
                                                          action="{{ route('post.update', [$_SERVER['HTTP_HOST'], $post->id]) }}"
                                                          method="post" data-parsley-validate>

                                                        <div class="modal-body">
                                                            <fieldset>
                                                                <input type="hidden" name="_token"
                                                                       value="{{ csrf_token() }}">
                                                                <input name="_method" type="hidden" value="PUT">

                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                      <textarea type="text" name="status"
                                                                class="form-control"
                                                                rows="4">{{ $post->status }}</textarea>
                                                                    </div>
                                                                </div>

                                                            </fieldset>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default"
                                                                    data-dismiss="modal">
                                                                <i class="fa fa-close"></i>
                                                                Tutup
                                                            </button>
                                                            <button type="submit" class="btn btn-primary">
                                                                <i class="fa fa-save"></i>
                                                                Simpan
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="modal fade" id="modalDelete{{ $post->id }}">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="defaultModalLabel">
                                                        Hapus {{ $post->status }}
                                                        ?</h4>
                                                </div>
                                                <div class="modal-body">
                                                    Anda yakin ingin menghapus data ini?
                                                </div>
                                                <div class="modal-footer">
                                                    {!! Form::open(['route' => ['post.destroy', $_SERVER['HTTP_HOST'], $post->id], 'method' => 'DELETE']) !!}
                                                    <button type="button" class="btn btn-default"
                                                            data-dismiss="modal"> Batal
                                                    </button>
                                                    <button type="submit" class="btn btn-danger"> Hapus</button>
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Modal -->
                                @endforeach


                            </div>

                        </div>

                        <!-- end timelines tab -->


                        <!-- question tab -->
                        <div class="tab-pane" id="newquestion">

                        </div>
                        <!-- end question tab -->

                    </div>
                </div>
            </div>
        </div>


        <div class="card-box bg-muted">
            <h5>People You May Follow</h5>
            <div class="row text-center">
                <div class="col-md-4 border card-box text-center">
                    <img class="rounded-circle thumb-md" src="/images/users/avatar-1.jpg"
                         alt="Generic placeholder image">
                    <h6 class="text-primary">Jhon Smith</h6>
                    <p>Graphic Designer</p>

                    <div class="row text-center">
                        <div class="col-md-12 p-0">
                            <small class="p-0 text-muted">UX Designer, Photoshop, Web Development</small>
                        </div>
                        <div class="m-1">
                            <button type="button" class="btn btn-primary" name="button">Follow</button>
                            <button type="button" class="btn btn-muted" name="button">Prolife</button>
                        </div>

                    </div>
                </div>
                <div class="col-md-4 border card-box text-center">
                    <img class="rounded-circle thumb-md" src="/images/users/avatar-1.jpg"
                         alt="Generic placeholder image">
                    <h6 class="text-primary">Jhon Smith</h6>
                    <p>Graphic Designer</p>

                    <div class="row text-center">
                        <div class="col-md-12 p-0">
                            <small class="p-0 text-muted">UX Designer, Photoshop, Web Development</small>
                        </div>
                        <div class="m-1">
                            <button type="button" class="btn btn-primary" name="button">Follow</button>
                            <button type="button" class="btn btn-muted" name="button">Prolife</button>
                        </div>

                    </div>
                </div>
                <div class="col-md-4 border card-box text-center">
                    <img class="rounded-circle thumb-md" src="/images/users/avatar-1.jpg"
                         alt="Generic placeholder image">
                    <h6 class="text-primary">Jhon Smith</h6>
                    <p>Graphic Designer</p>

                    <div class="row text-center">
                        <div class="col-md-12 p-0">
                            <small class="p-0 text-muted">UX Designer, Photoshop, Web Development</small>
                        </div>
                        <div class="m-1">
                            <button type="button" class="btn btn-primary" name="button">Follow</button>
                            <button type="button" class="btn btn-muted" name="button">Prolife</button>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>





    <div class="footer-space">
        <!-- <button type="button" class="tombol btn waves-effect waves-light w-lg float-left"><i class="ti-arrow-left icon-zize"></i>&nbsp;BACK REVIEW</button>
        <button type="button" class="tombol btn btn-primary waves-effect waves-light w-lg float-right">PAY NOW&nbsp;<i class="ti-arrow-right icon-zize"></i></button> -->
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>





@endsection
@section('script')
@endsection
