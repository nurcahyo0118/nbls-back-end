<div class="col-lg-12">
    <div class=" m-t-20">
        <div class="media m-b-30">
            <img class="d-flex mr-3 rounded-circle thumb-sm" src="/images/users/avatar-2.jpg"
                 alt="Generic placeholder image">
            <div class="media-body">
                <span class="media-meta pull-right text-primary" href="#" role="button" id="dropdownMenuLink"
                      data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-bookmark"></i>
                </span>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <a class="dropdown-item" data-toggle="modal" data-target="#modalEdit{{$post->id}}">Edit</a>
                    <a class="dropdown-item" data-toggle="modal" data-target="#modalDelete{{ $post->id }}">Delete</a>
                </div>
                <h4 class="text-primary font-16 m-0">
                    Chris Greyson
                    <small class="text-muted">
                        <i class="ion-clock"></i>
                        {{ $post->created_at }}
                    </small>
                </h4>

            </div>
        </div>
        <p>{{$post->status}}</p>

        <hr>

        <div class="row">
            <div class="col-md-2">
                <i class="fa fa-thumbs-o-up "> 10</i>
            </div>

            <div class="col-md-5">
                <p class="fa fa-comments-o"> 15 </p>
            </div>
        </div>
        <hr>

        <div>
            <div class="media m-b-30">
                <img class="d-flex mr-3 rounded-circle thumb-sm" src="/images/users/avatar-3.jpg"
                     alt="Generic placeholder image">
                <div class="media-body">
                    <input type="text" class="form-control" id="" placeholder="Jadilah yang pertama menjawab...">

                </div>
            </div>
            <hr>
        </div>

    </div>
</div>
