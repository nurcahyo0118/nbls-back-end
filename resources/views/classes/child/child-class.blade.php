<tr id="classId{{ $class->id }}">
    <td>{{ $class->course->title }}</td>
    <td>{{ $class->name }}</td>
    {{--  <td>
        <a href="#!" class="btn btn-info btn-xs">
            <i class="fa fa-plus"></i>
            Tambah
        </a>
    </td>  --}}
    <td class="text-right">
        <a href="{{ route('classes.edit', [$_SERVER['HTTP_HOST'], $class->id]) }}" class="btn btn-default btn-xs">
            <i class="fa fa-edit"></i>
            Edit
        </a>
        <a onclick="onDeleteClass({{ $class->id }})" class="btn btn-danger btn-xs">
            <i class="fa fa-trash"></i>
            Hapus
        </a>
    </td>
</tr>

{{--  @include('classes.modal.edit-class')  --}}