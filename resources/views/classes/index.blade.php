@extends('layouts.main')

@section('content')

    @php
        $tenant = Auth::user()->tenant;
        $permission = App\Permission::where(['role_id' => Auth::user()->role['id'], 'tenant_id' => $tenant->id])->first();
    @endphp

    <div class="row">

        <div class="col-xs-12">

            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Daftar Off Class</h3>
                    <br>

                    <a class="btn btn-info pull-left col-md-1"
                       href="{{ route('classes.create', $_SERVER['HTTP_HOST']) }}">
                        <span class="fa fa-plus"></span>
                    </a>

                </div>

                <div class="box-body">
                    <table class="table js-basic-example dataTable">
                        <thead>
                        <tr>
                            <th>Nama Kursus</th>
                            <th>Nama Soal</th>
                            {{--  <th>Quiz</th>  --}}
                            <th></th>
                        </tr>
                        </thead>
                        <tbody id="classList">

                        @foreach($classes as $class)

                            @include('classes.child.child-class')

                        @endforeach

                        </tbody>
                    </table>

                </div>

            </div>

        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        function onDeleteClass(classId) {
            swal({
                title: "Apa kamu yakin?",
                text: "Data yang telah di hapus tidak dapat dikembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {

                    $.ajax({
                        url: '/classes/' + classId,
                        type: 'DELETE',
                        success: function (response) {
                            $('#classId' + classId).remove();
                            swal("Berhasil !", "Off Class berhasil dihapus", "success");
                        },
                        error: function (error) {
                            swal("Berhasil !", "Off Class gagal dihapus", "error");
                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    });

                }
            });
        }
    </script>
@endsection