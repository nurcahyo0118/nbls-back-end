@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-xs-12">

            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Tambah Pertemuan</h3>

                </div>

                <div class="box-body">

                    <div class="col-md-12">
                        <form action="{{ route('classes.update', [$_SERVER['HTTP_HOST'], $class->id]) }}" method="post"
                              class="form-horizontal" enctype="multipart/form-data" data-parsley-validate>

                            {{ csrf_field() }}

                            <input type="hidden" name="_method" value="put">

                            <fieldset>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label> Kursus : </label>
                                        <select type="text" name="course_id" class="form-control" required>
                                            @foreach($courses as $course)
                                                <option value="{{ $course->id }}" {{ $class->course_id == $course->id ? 'selected' : ''  }}>{{ $course->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label> Nama Soal : </label>
                                        <input type="text" name="name" class="form-control" value="{{ $class->name }}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label> Order List : </label>
                                        <input type="number" maxlength="3" name="order_list" class="form-control"
                                               value="{{ $class->order_list }}" required>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label> File Soal : </label>
                                        <input type="file" name="file">
                                    </div>
                                </div>

                            </fieldset>

                            <div class="modal-footer">
                                <a href="{{ route('classes.index', $_SERVER['HTTP_HOST']) }}" class="btn btn-default">
                                    <i class="fa fa-close"></i>
                                    Tutup
                                </a>
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-save"></i>
                                    Simpan
                                </button>
                            </div>
                        </form>
                    </div>

                </div>

            </div>

        </div>
    </div>
@endsection