<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>NBLS - Babastudio</title>
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
      <link href="/baba-custom/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="/baba-custom/css/custom.css" rel="stylesheet">
      <link href="/baba-custom/lib/animate/animate.min.css" rel="stylesheet">
      <link href="/baba-custom/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
      <link href="/baba-custom/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
      <link href="/baba-custom/lib/lightbox/css/lightbox.min.css" rel="stylesheet">
      <link href="/baba-custom/css/style.css" rel="stylesheet">
  </head>
  <body style="background-image: url('/images/big/img1b.jpg'); background-repeat: repeat-x;">
  

  @include('layouts.partials-baba-custom.header')

<br>
<br>
<br>
<center>
<div class="col-md-4">
<section>
    <!-- form login -->
    <div class="wrapper-page">
        <div class="card bg-primary" style="opacity: 0.8;">
            <div class="panel-heading mt-4">
                <h4 class="text-center">
                    <a href="/" class="text-white"><b>{{ $tenant->name }}</b></a>
                </h4>
            </div>

            <div class="p-2">
                <form class="form-horizontal mt-1" action="{{ route('login', $tenant->domain) }}" method="POST">

                    {{ csrf_field() }}

                    <input type="hidden" name="tenant_id" value="{{ $tenant->id }}">

                    <input type="hidden" name="log" value="LOG_LOGIN">

                    <p class="text-center">{{ $errors->first('login') }}</p>

                    <div class="input-group">
                        <span class="input-group-btn">
                            <a class="btn form-control">
                                <i class="fa fa-user"></i>
                            </a>
                        </span>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        <input class="form-control" type="text" id="user-password" name="email" required="required"
                               placeholder="E-mail"/>
                    </div>

                    <br>

                    <div class="input-group">
                        <span class="input-group-btn">
                            <a class="btn form-control" type="button">
                                <i class="fa fa-lock"></i>
                            </a>
                        </span>
                        <input class="form-control" type="password" id="user-password" name="password"
                               required="required" placeholder="Password"/>
                    </div>

                    <div class="form-group m-t-20">
                        <div class="col-12 text-center">
                            <div class="checkbox checkbox-primary text-white">
                                <input id="checkbox-signup" type="checkbox">
                                <label for="checkbox-signup"> Remember me </label>
                            </div>

                        </div>
                    </div>

                    <div class="form-group text-center m-t-20">
                        <div class="col-12">
                            <button class="btn btn-dark btn-block text-uppercase waves-effect waves-light"
                                    type="submit">
                                Login
                            </button>
                        </div>
                    </div>

                    <div class="form-group m-t-20 m-b-0">
                        <div class="row">
                            <div class="col-6">
                                <a href="{{ route('register', $_SERVER['HTTP_HOST']) }}" class="text-white">Dont have an
                                                                                                           Account?
                                </a>
                            </div>
                            <div class="col-6">
                                <a target="_blank" href="{{ route('password.request') }}" class="text-white float-right">Forgotten Password?</a>
                            </div>

                            <div class="col-md-12 text-center m-t-20">
                                <a href="/" class="text-white">Return to Homepage <strong
                                            class="ti-arrow-left icon-zize"></strong></a>
                            </div>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
    <!-- end form login -->
</section>
</div>
</center>
<br>
<br>
<br>
<br>


 @include('layouts.partials-baba-custom.footer')

<script src="/baba-custom/vendor/jquery/jquery.min.js"></script>
<script src="/baba-custom/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/baba-custom/js/custom.js"></script>
<script src="/baba-custom/lib/jquery/jquery-migrate.min.js"></script>
<script src="/baba-custom/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/baba-custom/lib/easing/easing.min.js"></script>
<script src="/baba-custom/lib/superfish/hoverIntent.js"></script>
<script src="/baba-custom/lib/superfish/superfish.min.js"></script>
<script src="/baba-custom/lib/wow/wow.min.js"></script>
<script src="/baba-custom/lib/waypoints/waypoints.min.js"></script>
<script src="/baba-custom/lib/counterup/counterup.min.js"></script>
<script src="/baba-custom/lib/owlcarousel/owl.carousel.min.js"></script>
<script src="/baba-custom/lib/isotope/isotope.pkgd.min.js"></script>
<script src="/baba-custom/lib/lightbox/js/lightbox.min.js"></script>
<script src="/baba-custom/lib/touchSwipe/jquery.touchSwipe.min.js"></script>
<script src="/baba-custom/js/main.js"></script>

<script>
        function addToCart(course_id)
        {
            var carts = [];

            console.log('Masuk Pak Eko');

            var carts_from_local = JSON.parse(window.localStorage.getItem('carts'));

            if(carts_from_local === null)
            {
                carts.push(course_id);
                window.localStorage.setItem('carts', JSON.stringify(carts));
            }
            else
            {
                if(carts_from_local.indexOf(course_id) < 0)
                {
                    carts_from_local.push(course_id);
                    window.localStorage.setItem('carts', JSON.stringify(carts_from_local));
                }

            }
        }

        function removeFromCart(course_id)
        {
            console.log('Keluar Pak Eko');

            var carts_from_local = JSON.parse(window.localStorage.getItem('carts'));
            var index = carts_from_local.indexOf(course_id);
            if(carts_from_local !== null && index > -1)
            {
                carts_from_local.splice(index, 1);
                window.localStorage.setItem('carts', JSON.stringify(carts_from_local));

                // remove item cart
                $('#cartsItem' + course_id).remove();
                $('#cartsItemDivider' + course_id).remove();

                var carts_from_local_latest = JSON.parse(window.localStorage.getItem('carts'));
                if(carts_from_local_latest.length === 0)
                {
                    $('#cartsList').replaceWith('<div id="cartsList" class="col-sm-12 col-md-12 col-md-12 text-center" style="color: #424242">Kosong !</div>');
                }
            }
        }

        function loadCartData(carts)
        {
            console.log(carts.length);
            var url = '/loads/frontend/carts?';

            if(carts !== null && carts.length !== 0)
            {
                for(i = 0; i < carts.length; i++)
                {
                    url = url + '&carts[]=' + carts[i];
                    console.log(url);
                }

                $.ajax({
                    type: 'GET',
                    url: url,
                    success: function (response) {
                        $('#cartsList').replaceWith(response.view);
                    },
                    error: function (error) {
                        console.log('ddddd');
                        $('#cartsList').replaceWith('<div id="cartsList" class="col-sm-12 col-md-12 col-md-12 text-center" style="color: #424242">Kosong !</div>');
                    }
                });
            }
            else
            {
                $('#cartsList').replaceWith('<div id="cartsList" class="col-sm-12 col-md-12 col-md-12 text-center" style="color: #424242">Kosong !</div>');
            }

        }
</script>

</body>
</html>