@extends('layouts.main')

@section('content')

    <div class="row">

        <div class="modal fade" id="modalAdd">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel">Tambah Peran</h4>
                    </div>
                    <div class="modal-body">

                    </div>
                </div>
            </div>
        </div>


        <div class="col-xs-12">


            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Manage and view your videos</h3>
                    <br>
                </div>
                <div class="box-body">
                    <table class="table js-basic-example dataTable">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Nama</th>
                            <th>Size</th>
                            {{--  <th>Length</th>  --}}
                            <th>Uploaded</th>
                            <th>Modified</th>
                            <th>Encoding</th>
                            {{--  <th></th>  --}}
                        </tr>
                        </thead>
                        <tbody id="videoList"></tbody>

                    </table>

                </div>

            </div>

        </div>
    </div>

@endsection

@section('script')
    <script>
        $(function () {

            $('#videoList').replaceWith('<div id="videoList" class="text-center"><i class="fa fa-spinner fa-spin fa-5x"></i></div>');

            $.ajax({
                url: '/video-libraries/videos',
                type: 'GET',
                data: {
                    api_password: '{{ env("WISTIA_TOKEN") }}'
                },
                success: function (response) {
                    $('#videoList').replaceWith(response);

                },
                error: function (error) {
                    console.log(error);
                }
            });

            {{--  @foreach($materials as $material)
                $.ajax({
                    url: 'https://api.wistia.com/v1/medias/{{ $material->wistia_media_hashed_id }}.json',
                    type: 'GET',
                    data: {
                        api_password: '{{ env("WISTIA_TOKEN") }}'
                    },
                    success: function(response) {
                        console.log(response);
                        $('#videoList').replaceWith("");

                    },
                    error: function(error) {
                        console.log(error);
                    }
                });
            @endforeach  --}}
        });
    </script>
@endsection