<tbody id="videoList">
    {{--  {{ $materials }}  --}}
    @foreach($materials as $material)
        <tr>
            <td><img class="img-responsive" src="{{ $material->thumbnail->url }}"></img></td>
            <td>{{ $material->name }}</td>
            <td>{{ $material->assets[0]->fileSize }}</td>
            {{--  <td>{{ $material->duration }}</td>  --}}
            <td>{{ $material->created }}</td>
            <td>{{ $material->updated }}</td>
            <td>Finished</td>
            {{--  <td class="text-right">
                <a href="{{ route('video-libraries.index', [$_SERVER['HTTP_HOST'], $material->id]) }}"
                class="btn btn-default btn-xs">
                    <i class="fa fa-gear"></i>
                    Kelola
                </a>
            </td>  --}}
        </tr>
    @endforeach
    
    </tbody>