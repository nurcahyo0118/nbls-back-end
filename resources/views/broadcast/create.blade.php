@extends('layouts.main')

@section('content')

    @php
        $tenant = Auth::user()->tenant;
        $permission = App\Permission::where(['role_id' => Auth::user()->role['id'], 'tenant_id' => $tenant->id])->first();
    @endphp

    <div class="row">

        <div class="col-xs-12">

            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Kirim Broadcast</h3>

                </div>

                <form 
                    action="{{ route('broadcast.store', $_SERVER['HTTP_HOST']) }}"
                    method="POST"
                    enctype="multipart/form-data"
                    data-parsley-validate>

                    {{ csrf_field() }}

                    <div class="box-body">
                        
                        <fieldset>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Pengirim : </label>
                                    <input class="form-control" type="hidden" name="tenant_id" value="{{ Auth::user()->tenant->id }}" >
                                    <input class="form-control" type="hidden" name="user_id" value="{{ Auth::user()->id }}" >
                                    <input class="form-control" type="text" name="sender" value="{{ Auth::user()->username }}" required>
                                
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Pesan : </label>
                                    <textarea class="form-control" name="message" required>
                                    </textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Viewer : </label>
                                    <select class="form-control" type="text" name="viewer" required>
                                        
                                        <option class="text-primary" value="Murid">Murid</option>
                                        <option class="text-info" value="Instruktur">Instruktur</option>
                                        <option class="text-default" value="Expert">Expert</option>
                                        <option class="text-danger" value="Admin">Admin</option>
                                        <option class="text-warning" value="SuperAdmin">Superadmin</option>
                                        <option class="text-danger" value="all">Semua</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Tanggal Broadcast : </label>
                                    <input class="form-control" type="date" name="view_date" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Tanggal Selesai Broadcast : </label>
                                    <input class="form-control" type="date" name="close_date" required>
                                </div>
                            </div>


                            

                        </fieldset>
                        
                    </div>

                    <div class="box-footer">
                        <div class="col-md-12 text-right">
                            <a 
                                href="{{ route('broadcast.index', $_SERVER['HTTP_HOST']) }}"
                                class="btn btn-default">
                                <i class="fa fa-close"></i>
                                Batal
                            </a>
                            <button type="submit" class="btn btn-success">
                                <i class="fa fa-save"></i>
                                Simpan
                            </button>
                        </div>
                    </div>

                </form>

            </div>

        </div>
    </div>

@endsection
