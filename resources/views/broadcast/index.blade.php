@extends('layouts.main')

@section('content')

    @php
        $tenant = Auth::user()->tenant;
        $permission = App\Permission::where(['role_id' => Auth::user()->role['id'], 'tenant_id' => $tenant->id])->first();
        
    @endphp

    <div class="row">
        <div class="col-xs-12">

            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Daftar Broadcast</h3>
                    <br>
                    <a class="btn btn-primary pull-left col-md-1" href="{{ route('broadcast.create', $_SERVER['HTTP_HOST']) }}">
                        <span class="fa fa-plus"></span>
                    </a>
                </div>

                <div class="box-body">
                    <table class="table js-basic-example dataTable">
                        <thead>
                        <tr>
                            <th>Pengirim</th>
                            <th>Pesan</th>
                            <th>Viewer</th>
                            <th>Tanggal Mulai Broadcast</th>
                            <th>Tanggal Selesai Broadcast</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody id="coursePackagesList">

                        @foreach($broadcast as $broadcast)
                            @if($broadcast->tenant_id === Auth::user()->tenant->id)
                                <tr>
                                    <td>{{ $broadcast->sender }}</td>
                                    <td>{{ $broadcast->message }}</td>
                                    <td>{{ $broadcast->viewer }}</td>
                                    <td>{{ $broadcast->view_date }}</td>
                                    <td>{{ $broadcast->close_date }}</td>
                                    <td class="text-right">
                                            <a
                                                href="{{ route('broadcast.edit', [$_SERVER['HTTP_HOST'], $broadcast->id]) }}"
                                                class="btn btn-default btn-xs">
                                                <i class="fa fa-edit"></i>
                                                Edit
                                            </a>
                                            <a
                                                href="#!"
                                                class="btn btn-danger btn-xs"
                                                onclick="onDelete({{ $broadcast->id }})">
                                                <i class="fa fa-trash"></i>
                                                Hapus
                                            </a>
                                        </td>
                                </tr>
                            @endif
                        @endforeach

                        </tbody>
                    </table>

                </div>

            </div>

        </div>
    </div>

@endsection

@section('script')
    <script>
        function publish() {
            swal("Berhasil !", "Paket kursus berhasil diterbitkan", "success");
        }

        function hide() {
            swal("Berhasil !", "Paket kursus berhasil disembunyikan", "success");
        }

        function onDelete(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "Data yang telah di hapus tidak dapat dikembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {

                    $.ajax({
                        url: '/broadcast/' + id,
                        type: 'DELETE',
                        data: null,
                        success: function (data) {
                            window.location.reload();
                        },
                        error: function (error) {
                            swal("Gagal !", "Gagal menghapus", "error");
                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    });

                }
            });
        }
    </script>
@endsection