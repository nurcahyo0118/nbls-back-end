@extends('layouts.main')

@section('content')

    @php
        $tenant = Auth::user()->tenant;
        $permission = App\Permission::where(['role_id' => Auth::user()->role['id'], 'tenant_id' => $tenant->id])->first();
    @endphp

    <div class="row">

        <div class="col-xs-12">

            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Edit Widget</h3>

                </div>

                <form 
                    action="{{ route('broadcast.update', [$_SERVER['HTTP_HOST'], $broadcast->id]) }}"
                    method="POST"
                    enctype="multipart/form-data"
                    data-parsley-validate>

                    {{ csrf_field() }}

                    <input type="hidden" name="_method" value="PUT">

                    <div class="box-body">
                        
                        <fieldset>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Pengirim : </label>
                                    <input class="form-control" type="text" name="sender" value="{{ Auth::user()->username }}" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Pesan : </label>
                                    <textarea class="form-control" name="message" required>
                                        {{ $broadcast->message }}
                                    </textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Viewer : </label>
                                    <select class="form-control" type="text" name="viewer" required>
                                        <option class="text-primary" value="{{ $broadcast->viewer }}">{{ $broadcast->viewer }}</option>
                                        <option class="text-primary" value="murid">Murid</option>
                                        <option class="text-info" value="instruktur">Instruktur</option>
                                        <option class="text-default" value="expert">Expert</option>
                                        <option class="text-danger" value="admin">Admin</option>
                                        <option class="text-warning" value="superdamin">Superadmin</option>
                                        <option class="text-danger" value="all">Semua</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Tanggal Broadcast : </label>
                                    <input class="form-control" type="date" name="view_date" value="{{ $broadcast->view_date }}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Tanggal Selesai Broadcast : </label>
                                    <input class="form-control" type="date" name="close_date" value="{{ $broadcast->close_date }}" required>
                                </div>
                            </div>


                            

                        </fieldset>
                        
                    </div>

                    <div class="box-footer">
                        <div class="col-md-12 text-right">
                            <a 
                                href="{{ route('social-learning-sales-widget.index', $_SERVER['HTTP_HOST']) }}"
                                class="btn btn-default">
                                <i class="fa fa-close"></i>
                                Batal
                            </a>
                            <button type="submit" class="btn btn-success">
                                <i class="fa fa-save"></i>
                                Simpan
                            </button>
                        </div>
                    </div>

                </form>

            </div>

        </div>
    </div>

@endsection

@section('script')
    <script>
        function onSelectCategory(value) {
            var courseSubCategoryId = $('#course_sub_category_id');

            $.ajax({
                type: 'GET',
                url: '/loads/subcategories/' + value,
                success: function (response) {
                    console.log(response);

                    courseSubCategoryId.empty();

                    courseSubCategoryId.append('<option value="">Pilih Sub Kategori</option>');

                    for (let subcategory of response) {
                        console.log(subcategory);
                        courseSubCategoryId.append('<option value="' + subcategory.id + '">' + subcategory.name + '</option>');
                    }

                },
                error: function (error) {
                    console.log(error);
                }
            });

        }
    </script>
@endsection