<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>NBLS - Babastudio</title>
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
      <link href="/baba-custom/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="/baba-custom/css/custom.css" rel="stylesheet">
      <link href="/baba-custom/lib/animate/animate.min.css" rel="stylesheet">
      <link href="/baba-custom/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
      <link href="/baba-custom/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
      <link href="/baba-custom/lib/lightbox/css/lightbox.min.css" rel="stylesheet">
      <link href="/baba-custom/css/style.css" rel="stylesheet">
  </head>
  <body style="background-image: url('/images/big/img1b.jpg'); background-repeat: repeat-x;">
  
@include('layouts.partials-baba-custom.header')
<br>
<br>
<br>
<center>
<div class="col-md-4">
<section>
    <!-- form Register -->
    <div class="wrapper-page m-b-20">
        <div class="card bg-primary"  style="opacity: 0.8;">
            <div class="panel-head pt-3">
                <h4 class="text-center text-white"> REGISTRATION</h4>
            </div>

            <div class="p-3">
                <form class="form-horizontal m-t-20" action="{{ route('register', $_SERVER['HTTP_HOST']) }}"
                      method="POST">

                    {{ csrf_field() }}

                    <input type="hidden" name="tenant_id" value="{{ $tenant->id }}">

                    @foreach($errors->all() as $error)
                        <p class="text-center">{{ $error }}</p>
                    @endforeach

                    <div class="input-group">
                        <span class="input-group-btn">
                            <a class="btn  form-control" type="button">
                                <i class="fa fa-user"></i>
                            </a>
                        </span>
                        <input class="form-control" type="text" name="username" required="required"
                               placeholder="Username"/>
                    </div>

                    <br>

                    <div class="input-group">
                        <span class="input-group-btn">
                            <a class="btn form-control" type="button">
                                <i class="fa fa-envelope-square"></i>
                            </a>
                        </span>
                        <input class="form-control" type="text" name="email" required="required"
                               placeholder="Email Address"/>
                    </div>

                    <br>

                    <div class="input-group">
                        <span class="input-group-btn">
                            <a class="btn  form-control" type="button">
                                <i class="fa fa-lock"></i>
                            </a>
                        </span>
                        <input class="form-control" type="password" name="password" required="required"
                               placeholder="Password"/>
                    </div>

                    <br>

                    <div class="input-group">
                        <span class="input-group-btn">
                            <a class="btn  form-control" type="button">
                                <i class="fa fa-lock"></i>
                            </a>
                        </span>
                        <input class="form-control" type="password" required="required" name="password_confirmation" placeholder="re-Type Password"/>
                    </div>

                    <hr>

                    <div class="g-recaptcha col-md-12" data-callback="captchaCallback"
                         data-sitekey="{{ env('CAPTHA_SITEKEY') }}"></div>

                    <div class="form-group m-t-20">
                        <div class="col-12 text-center">
                            <div class="checkbox checkbox-primary required">
                                <input id="checkbox-signup" type="checkbox" required>
                                <label for="checkbox-signup text-white"> I accept all the Term of Service </label>
                            </div>

                        </div>
                    </div>

                    <div class="form-group text-center m-t-20">
                        <div class="col-12">
                            <button id="btnRegister"
                                    class="btn btn-dark btn-block text-uppercase waves-effect waves-light"
                                    type="submit">
                                Register
                            </button>
                        </div>
                    </div>

                    <div class="form-group m-t-20 m-b-0">
                        <div class="row">
                            <div class="col-6">
                                <a href="/login" class="text-white">Have an Account & Login?</a>
                            </div>
                            <div class="col-6">
                                <a target="_blank" href="{{ route('password.request') }}" class="text-white float-right">Forgotten Password?</a>
                            </div>

                            <div class="col-md-12 text-center m-t-20">
                                <a href="/" class="text-white">Return to Homepage <strong
                                            class="ti-arrow-left icon-zize"></strong></a>
                            </div>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
    <!-- end form Register -->
</section>
</div>
</center>
<br>
<br>
<br>
<br>
@include('layouts.partials-baba-custom.footer')


<!-- jQuery  -->
<script src="/js/jquery.min.js"></script>
<script src="/js/popper.min.js"></script>
<!-- Popper for Bootstrap -->
<script src="/js/bootstrap.min.js"></script>
<script src="/js/waves.js"></script>
<script src="/js/jquery.slimscroll.js"></script>
<script src="/js/jquery.scrollTo.min.js"></script>

<!-- App js -->
<script src="/js/jquery.core.js"></script>
<script src="/js/jquery.app.js"></script>

<script src='https://www.google.com/recaptcha/api.js'></script>

<script>
    $(function () {
        $('#btnRegister').hide();
    });
</script>

<script>
    function captchaCallback(response) {
        $('#btnRegister').show();
    }
</script>

<script>
    function verifyCaptcha(response) {

        $.ajax({
            url: 'https://www.google.com/recaptcha/api/siteverify',
            type: 'POST',
            data: null,
            success: function (data) {
                alert(JSON.stringify(data));
            },
            error: function (error) {
                alert(JSON.stringify(error));
            },
            cache: false,
            contentType: false,
            processData: false
        });


    }

    function addToCart(course_id)
        {
            var carts = [];

            console.log('Masuk Pak Eko');

            var carts_from_local = JSON.parse(window.localStorage.getItem('carts'));

            if(carts_from_local === null)
            {
                carts.push(course_id);
                window.localStorage.setItem('carts', JSON.stringify(carts));
            }
            else
            {
                if(carts_from_local.indexOf(course_id) < 0)
                {
                    carts_from_local.push(course_id);
                    window.localStorage.setItem('carts', JSON.stringify(carts_from_local));
                }

            }
        }

        function removeFromCart(course_id)
        {
            console.log('Keluar Pak Eko');

            var carts_from_local = JSON.parse(window.localStorage.getItem('carts'));
            var index = carts_from_local.indexOf(course_id);
            if(carts_from_local !== null && index > -1)
            {
                carts_from_local.splice(index, 1);
                window.localStorage.setItem('carts', JSON.stringify(carts_from_local));

                // remove item cart
                $('#cartsItem' + course_id).remove();
                $('#cartsItemDivider' + course_id).remove();

                var carts_from_local_latest = JSON.parse(window.localStorage.getItem('carts'));
                if(carts_from_local_latest.length === 0)
                {
                    $('#cartsList').replaceWith('<div id="cartsList" class="col-sm-12 col-md-12 col-md-12 text-center" style="color: #424242">Kosong !</div>');
                }
            }
        }

        function loadCartData(carts)
        {
            console.log(carts.length);
            var url = '/loads/frontend/carts?';

            if(carts !== null && carts.length !== 0)
            {
                for(i = 0; i < carts.length; i++)
                {
                    url = url + '&carts[]=' + carts[i];
                    console.log(url);
                }

                $.ajax({
                    type: 'GET',
                    url: url,
                    success: function (response) {
                        $('#cartsList').replaceWith(response.view);
                    },
                    error: function (error) {
                        console.log('ddddd');
                        $('#cartsList').replaceWith('<div id="cartsList" class="col-sm-12 col-md-12 col-md-12 text-center" style="color: #424242">Kosong !</div>');
                    }
                });
            }
            else
            {
                $('#cartsList').replaceWith('<div id="cartsList" class="col-sm-12 col-md-12 col-md-12 text-center" style="color: #424242">Kosong !</div>');
            }

        }
</script>

</body>

</html>
