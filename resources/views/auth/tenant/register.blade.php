<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>NBLS - Babastudio</title>
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
      <link href="/baba-custom/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="/baba-custom/css/custom.css" rel="stylesheet">
      <link href="/baba-custom/lib/animate/animate.min.css" rel="stylesheet">
      <link href="/baba-custom/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
      <link href="/baba-custom/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
      <link href="/baba-custom/lib/lightbox/css/lightbox.min.css" rel="stylesheet">
      <link href="/baba-custom/css/style.css" rel="stylesheet">
  </head>
  <body style="background-image: url('/images/big/img1b.jpg'); background-repeat: repeat-x;">
  
@include('layouts.partials-baba-custom.header')
<br>
<br>
<br>
<center>
<div class="col-md-4">
<section>
    <!-- form Register -->
    <div class="wrapper-page m-b-20">
        <div class="card bg-primary"  style="opacity: 0.8;">
            <div class="panel-head pt-3">
                <h4 class="text-center text-white"> REGISTRATION</h4>
            </div>

            <div class="p-3">
                    @foreach($errors->all() as $error)
                        <p class="text-center">{{ $error }}</p>
                    @endforeach
                    
                <form class="form-horizontal m-t-20" action="{{ route('register', $tenant->subdomain) }}" method="POST">

                    {{ csrf_field() }}

                    <input type="hidden" name="tenant_id" value="{{ $tenant->id }}">

                    <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn  form-control" type="button">
                                <i class="fa fa-user"></i>
                            </button>
                        </span>
                        <input class="form-control" type="text" id="user-password" name="username" required="required"
                               placeholder="Username"/>
                    </div>

                    <br>

                    <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn  form-control" type="button">
                                <i class="fa fa-envelope-square"></i>
                            </button>
                        </span>
                        <input class="form-control" type="text" id="user-password" name="email" required="required"
                               placeholder="Email Address"/>
                    </div>

                    <br>

                    <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn  form-control" type="button">
                                <i class="fa fa-lock"></i>
                            </button>
                        </span>
                        <input class="form-control" type="password" id="user-password" name="password"
                               required="required" placeholder="Password"/>
                    </div>

                    <br>

                    <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn  form-control" type="button">
                                <i class="fa fa-lock"></i>
                            </button>
                        </span>
                        <input class="form-control" type="password" id="user-password" required="required"
                               placeholder="re-Type Password"/>
                    </div>


                    <div class="form-group m-t-20">
                        <div class="col-12 text-center">
                            <div class="checkbox checkbox-primary">
                                <input id="checkbox-signup" type="checkbox">
                                <label for="checkbox-signup"> I accept all the Term of Service </label>
                            </div>

                        </div>
                    </div>

                    <div class="form-group text-center m-t-20">
                        <div class="col-12">
                            <button class="btn btn-dark btn-block text-uppercase waves-effect waves-light"
                                    type="submit">
                                Register
                            </button>
                        </div>
                    </div>

                    <div class="form-group m-t-20 m-b-0">
                        <div class="row">
                            <div class="col-6">
                                <a href="/" class="text-dark">Have an Account & Login?</a>
                            </div>
                            <div class="col-6">
                                <a href="/" class="text-dark float-right">Forgotten Password?</a>
                            </div>

                            <div class="col-md-12 text-center m-t-20">
                                <a href="/" class="text-dark">Return to Homepage <strong
                                            class="ti-arrow-left icon-zize"></strong></a>
                            </div>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
    <!-- end form Register -->
</section>
</section>
</div>
</center>
<br>
<br>
<br>
<br>
@include('layouts.partials-baba-custom.footer')

<!-- jQuery  -->
<script src="/js/jquery.min.js"></script>
<script src="/js/popper.min.js"></script>
<!-- Popper for Bootstrap -->
<script src="/js/bootstrap.min.js"></script>
<script src="/js/waves.js"></script>
<script src="/js/jquery.slimscroll.js"></script>
<script src="/js/jquery.scrollTo.min.js"></script>

<!-- App js -->
<script src="/js/jquery.core.js"></script>
<script src="/js/jquery.app.js"></script>

</body>

</html>
