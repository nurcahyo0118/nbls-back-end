@extends('layouts.frontend-no-right-left')

@section('content')

    <div class="row">

        <!-- start Carousel -->
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" style="height: 500px;" src="/images/big/img3.jpg" alt="First slide">

                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" style="height: 500px;" src="/images/big/img3.jpg" alt="Second slide">

                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" style="height: 500px;" src="/images/big/img3.jpg" alt="Third slide">

                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <!-- end Caraousel -->


        <div class="container">

          <br>
          <br>
          <br>
          <br>
          <center>
            <h3>Kursus Paket <br> <h6 class="text-muted">Harga Lebih Murah</h6> </h3>
            <h1>Rp. 4,5 Juta</h1>
            <h3>Membuat Website Sampai Mahir</h3>
          </center>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

            <div class="row">

              <!-- card Kursus -->
              <div class="col-md-4">
                        <div class="card">
                                <img class="card-img-top" src="/images/order/web_design.jpg" alt="Card image cap">

                            <div class="card-body">
                                <p class="text-muted">
                                    <small>By Edwin
                                        <i class="ion-clock"></i> 3 Days Ago
                                    </small>
                                </p>
                                <h6><strong>Pemgrogaman Dasar PHP</strong></h6>
                                <p>
                                    <i class="fa fa-users"> 100 Siswa</i>
                                </p>
                                <hr>
                                {{--  <p>Sisa Waktu Belajar : 300 hari</p>  --}}
                                <div class="progress">
                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="95"
                                         aria-valuemin="0" aria-valuemax="100" style="width: %;">
                                        <span class="sr-only">95% Complete</span>
                                    </div>
                                </div>
                            </div>

                            <div class="card-footer text-center btn-primary btn-block">
                                <a class="text-white"
                                   href="">
                                    VIEW DETAILS
                                    <i class="md-arrow-forward"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- end card Kursus -->
              <!-- card Kursus -->
              <div class="col-md-4">
                        <div class="card">
                                <img class="card-img-top" src="/images/order/web_design.jpg" alt="Card image cap">

                            <div class="card-body">
                                <p class="text-muted">
                                    <small>By Edwin
                                        <i class="ion-clock"></i> 3 Days Ago
                                    </small>
                                </p>
                                <h6><strong>Pemgrogaman Dasar PHP</strong></h6>
                                <p>
                                    <i class="fa fa-users"> 100 Siswa</i>
                                </p>
                                <hr>
                                {{--  <p>Sisa Waktu Belajar : 300 hari</p>  --}}
                                <div class="progress">
                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="95"
                                         aria-valuemin="0" aria-valuemax="100" style="width: %;">
                                        <span class="sr-only">95% Complete</span>
                                    </div>
                                </div>
                            </div>

                            <div class="card-footer text-center btn-primary btn-block">
                                <a class="text-white"
                                   href="">
                                    VIEW DETAILS
                                    <i class="md-arrow-forward"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- end card Kursus -->
              <!-- card Kursus -->
              <div class="col-md-4">
                        <div class="card">
                                <img class="card-img-top" src="/images/order/web_design.jpg" alt="Card image cap">

                            <div class="card-body">
                                <p class="text-muted">
                                    <small>By Edwin
                                        <i class="ion-clock"></i> 3 Days Ago
                                    </small>
                                </p>
                                <h6><strong>Pemgrogaman Dasar PHP</strong></h6>
                                <p>
                                    <i class="fa fa-users"> 100 Siswa</i>
                                </p>
                                <hr>
                                {{--  <p>Sisa Waktu Belajar : 300 hari</p>  --}}
                                <div class="progress">
                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="95"
                                         aria-valuemin="0" aria-valuemax="100" style="width: %;">
                                        <span class="sr-only">95% Complete</span>
                                    </div>
                                </div>
                            </div>

                            <div class="card-footer text-center btn-primary btn-block">
                                <a class="text-white"
                                   href="">
                                    VIEW DETAILS
                                    <i class="md-arrow-forward"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- end card Kursus -->

            </div>
            <!-- Pagination -->
            <div class="m-t-15">

                <div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" data-size="large" data-mobile-iframe="true"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Bagikan</a></div>
            </div>
            <!-- end Pagination -->
        </div>
        <!-- end row -->



        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.12';
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
@endsection
