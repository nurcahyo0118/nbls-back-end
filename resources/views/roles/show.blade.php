@extends('layouts.main')

@section('content')

    {{-- @php
    $permission = new App\Permission;
    @endphp --}}
    <section class="content-header">
        <h1>
            Kelola {{ $role->name }}
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="#">
                    <i class="fa fa-dashboard"></i>
                    Dashboard
                </a>
            </li>
            <li>
                <a href="{{ route('tenants.index', $_SERVER['HTTP_HOST']) }}">
                    <i class="fa fa-building"></i>
                    Tenants
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-key"></i>
                    {{ $role->name }}
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-gear"></i>
                    Kelola
                </a>
            </li>
        </ol>
    </section>

    @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach

    <section class="content">

        <div class="row">

            <div class="col-xs-12">

                @include('layouts.partials.message')

                {{--                @if($permission != null)--}}
                <div class="box box-primary">
                    <form class="" action="{{ route('roles.update', [$_SERVER['HTTP_HOST'], $role->id]) }}"
                          method="post"
                          data-parsley-validate>

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input name="_method" type="hidden" value="PUT">

                        <div class="box-body">
                            <div class="col-md-12">

                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Menu</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td></td>
                                        <td class="text-right"><label><input id="all" name="all" type="checkbox">
                                                Semua</label></td>
                                    </tr>
                                    <tr>
                                        <td><b>Role</b></td>
                                        <td class="text-right">
                                            <label><input name="read_role"
                                                          type="checkbox" {{ $permission->read_role ? 'checked' : '' }}>
                                                Read</label>
                                            &nbsp;
                                            <label><input name="update_role"
                                                          type="checkbox" {{ $permission->update_role ? 'checked' : '' }}>
                                                Update</label>

                                        </td>
                                    </tr>

                                    <tr>
                                        <td><b>User</b></td>
                                        <td class="text-right">
                                            <label><input name="read_user"
                                                          type="checkbox" {{ $permission->read_user ? 'checked' : '' }}>
                                                Read</label>
                                            &nbsp;
                                            <label><input name="create_user"
                                                          type="checkbox" {{ $permission->create_user ? 'checked' : '' }}>
                                                Create</label>
                                            &nbsp;
                                            <label><input name="update_user"
                                                          type="checkbox" {{ $permission->update_user ? 'checked' : '' }}>
                                                Update</label>
                                            &nbsp;
                                            <label><input name="delete_user"
                                                          type="checkbox" {{ $permission->delete_user ? 'checked' : '' }}>
                                                Delete</label>

                                        </td>
                                    </tr>

                                    <tr>
                                        <td><b>Murid</b></td>
                                        <td class="text-right">
                                            <label><input name="read_student_menu"
                                                            type="checkbox" {{ $permission->read_student_menu ? 'checked' : '' }}>
                                                Read</label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td><b>I-Time</b></td>
                                        <td class="text-right">
                                            <label><input name="read_i_time"
                                                            type="checkbox" {{ $permission->read_i_time ? 'checked' : '' }}>
                                                Read</label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td><b>Halaman</b></td>
                                        <td class="text-right">
                                            <label><input name="read_page"
                                                            type="checkbox" {{ $permission->read_course ? 'checked' : '' }}>
                                                Read</label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td><b>Laporan Keuangan</b></td>
                                        <td class="text-right">
                                            <label><input name="read_report_finance"
                                                            type="checkbox" {{ $permission->read_report_finance ? 'checked' : '' }}>
                                                Read</label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td><b>Laporan Log</b></td>
                                        <td class="text-right">
                                            <label><input name="read_report_log"
                                                            type="checkbox" {{ $permission->read_report_log ? 'checked' : '' }}>
                                                Read</label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td><b>Kursus</b></td>
                                        <td class="text-right">
                                            <label><input name="read_course"
                                                          type="checkbox" {{ $permission->read_course ? 'checked' : '' }}>
                                                Read</label>
                                            &nbsp;
                                            <label><input name="create_course"
                                                          type="checkbox" {{ $permission->create_course ? 'checked' : '' }}>
                                                Create</label>
                                            &nbsp;
                                            <label><input name="update_course"
                                                          type="checkbox" {{ $permission->update_course ? 'checked' : '' }}>
                                                Update</label>
                                            &nbsp;
                                            <label><input name="delete_course"
                                                          type="checkbox" {{ $permission->delete_course ? 'checked' : '' }}>
                                                Delete</label>

                                        </td>
                                    </tr>

                                    <tr>
                                        <td><b>Kategori</b></td>
                                        <td class="text-right">
                                            <label><input name="read_category"
                                                          type="checkbox" {{ $permission->read_category ? 'checked' : '' }}>
                                                Read</label>
                                            &nbsp;
                                            <label><input name="create_category"
                                                          type="checkbox" {{ $permission->create_category ? 'checked' : '' }}>
                                                Create</label>
                                            &nbsp;
                                            <label><input name="update_category"
                                                          type="checkbox" {{ $permission->update_category ? 'checked' : '' }}>
                                                Update</label>
                                            &nbsp;
                                            <label><input name="delete_category"
                                                          type="checkbox" {{ $permission->delete_category ? 'checked' : '' }}>
                                                Delete</label>

                                        </td>
                                    </tr>

                                    <tr>
                                        <td><b>Daftar Tenant</b></td>
                                        <td class="text-right">
                                            <label><input name="read_tenant" type="checkbox" {{ $permission->read_tenant ? 'checked' : '' }}>
                                                Read</label>
                                            &nbsp;
                                            <label><input name="create_tenant" type="checkbox" {{ $permission->create_tenant ? 'checked' : '' }}>
                                                Create</label>
                                            &nbsp;
                                            <label><input name="update_tenant" type="checkbox" {{ $permission->update_tenant ? 'checked' : '' }}>
                                                Update</label>
                                            &nbsp;
                                            <label><input name="delete_tenant" type="checkbox" {{ $permission->delete_tenant ? 'checked' : '' }}>
                                                Delete</label>

                                        </td>
                                    </tr>

                                    <tr>
                                        <td><b>Kelola Tenant</b></td>
                                        <td class="text-right">
                                            <label><input name="read_tenant_me"
                                                          type="checkbox" {{ $permission->read_tenant_me ? 'checked' : '' }}>
                                                Read</label>
                                            &nbsp;
                                            <label><input name="create_tenant_me"
                                                          type="checkbox" {{ $permission->create_tenant_me ? 'checked' : '' }}>
                                                Create</label>
                                            &nbsp;
                                            <label><input name="update_tenant_me"
                                                          type="checkbox" {{ $permission->update_tenant_me ? 'checked' : '' }}>
                                                Update</label>
                                            &nbsp;
                                            <label><input name="delete_tenant_me"
                                                          type="checkbox" {{ $permission->delete_tenant_me ? 'checked' : '' }}>
                                                Delete</label>

                                        </td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>

                        </div>

                        <div class="box-footer">
                            <div class="col-md-12 text-right">
                                <a class="btn btn-default"
                                   href="{{ route('roles.index', $_SERVER['HTTP_HOST']) }}">
                                    <i class="fa fa-close"></i>
                                    Tutup
                                </a>

                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-save"></i>
                                    Simpan
                                </button>
                            </div>
                        </div>

                    </form>
                </div>

                {{--@endif--}}

            </div>
        </div>

    </section>

@endsection

@section('script')

    <script>
        $(function () {
            /* Select All */
            $('#all').change(function () {
                $("input:checkbox").prop('checked', this.checked);
            });
        });
    </script>
    <script>
        @if ($role->is_cname)
        customDomain();
        @else
        customSubdomain();

        @endif

        function customDomain() {
            document.getElementById('subdomain').style.display = 'none';
            document.getElementById('domain').style.display = 'block';
        }

        function customSubdomain() {
            document.getElementById('subdomain').style.display = 'block';
            document.getElementById('domain').style.display = 'none';
        }

    </script>
@endsection
