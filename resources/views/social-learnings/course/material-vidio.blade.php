@extends('layouts.frontend-no-right-left')

@section('content')

    @foreach($material->quizzes as $quiz)
        @include('social-learnings.course.modal.quiz')
    @endforeach

    <div class="row">
        @include('layouts.partials.dialog-message')

        @include('social-learnings.course.child.sidebar')

        <div class="col-md-8">

            <div class="wistia_embed wistia_async_{{ $material->wistia_media_hashed_id }}"
                style="height:430px;width:100%">&nbsp;
            </div>

            <br>
            <div class="card container">

                <div class="card-body">
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#home-2"
                                role="tab" aria-controls="konten-home" aria-selected="true">ASK
                            </a>

                            <a class="nav-item nav-link" id="nav-5-tab" data-toggle="tab" href="#messages-2" role="tab"
                                aria-controls="konten-5" aria-selected="false">Referensi
                            </a>

                            {{--  <a class="nav-item nav-link" id="nav-10-tab" data-toggle="tab" href="#settings-2" role="tab"
                                aria-controls="konten-10" aria-selected="false">Notes
                            </a>  --}}
                        </div>
                    </nav>


                    <div class="tab-content p-2">

                        <div class="tab-pane active" id="home-2">

                            <form id="addquestion" method="POST" data-parsley-validate>

                                <div class="row">

                                    <div class="col-md-12">
                                        <h4>Ada pertanyaan ?</h4>
                                        <hr>
                                    </div>

                                    <div class="col-md-1">
                                        @if( Auth::user()->photo === null)
                                            <img class="d-flex rounded-circle"
                                                    src="/images/default.png" alt="Generic placeholder image" height="55" width="55">
                                        @else
                                            <img class="d-flex rounded-circle"
                                                    src="/images/photo/{{ Auth::user()->photo }}"
                                                    alt="Generic placeholder image" height="45" width="45">
                                        @endif
                                    </div>


                                    <div class="col-md-11">

                                        <fieldset>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                            <input type="hidden" name="material_id" value="{{ $material->id }}">

                                            <input type="hidden" name="log" value="LOG_ASK_MATERIAL">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <textarea type="text" name="content" class="form-control" rows="4"
                                                                placeholder="Tanyakan sesuatu..." required></textarea>
                                                </div>
                                            </div>
                                        </fieldset>

                                    </div>


                                    <div class="col-md-12">
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-10">
                                            </div>

                                            <div class="col-md-2">
                                                <button type="submit" class="btn btn-primary float-right" name="button">
                                                    Kirim
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </form>

                            <div id="questionList">
                                @foreach($material->questions as $question)

                                    @include('social-learnings.course.child.question-child')

                                @endforeach
                            </div>

                        </div>

                        <div class="tab-pane" id="messages-2">

                            <hr>

                            @foreach($material->section->references as $reference)
                                <a href="/reference-file/{{ $reference->file }}" class="p-2" download>{{ $reference->title }}</a>
                                <hr>
                            @endforeach
                        </div>

                        <div class="tab-pane" id="settings-2">
                            <div class="row">
                                <div class="col-md-5">
                                    <button type="button" class="btn btn-primary mt-4 mb-4">Download All Notes</button>
                                    <form>
                                        <textarea name="notes" class="form-control mb-4" rows="8" cols="80"
                                                placeholder="silahkan masukkan note"></textarea>
                                        <button type="button" class="btn btn-primary">Save Notes</button>
                                    </form>
                                </div>
                                <div class="col-md-7">
                                    <button type="button" class="btn btn-primary mt-4">Download Notes</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- end row -->
    </div>

@endsection
@section('script')
<script src="//fast.wistia.com/embed/medias/{{ $material->wistia_media_hashed_id }}.jsonp" async></script>
<script src="//fast.wistia.com/assets/external/E-v1.js" async></script>

<script>
    
</script>
<script>
    
    $(function () {
        window._wq = window._wq || [];
        _wq.push({ id: "{{ $material->wistia_media_hashed_id }}", onReady: function(video) {
            console.log("I got a handle to the video!", video);
            video.play();
        }});

        @foreach($material->quizzes as $quiz)
            @if(DB::table('user_quizzes')->where(['user_id' => Auth::id(), 'quiz_id' => $quiz->id])->count() == 0)
                window.setTimeout('quiz({{ $quiz->id }})', Math.floor({{ ($quiz->minute * 60 + $quiz->second) * 1000 }}));
            @endif
        @endforeach

        $("form#addquestion").submit(function (e) {
            e.preventDefault();
            var formData = new FormData(this);

            $.ajax({
                url: '/social-learning-questions',
                type: 'POST',
                data: formData,
                success: function (data) {
                    window.location.reload();
                    {{-- $('#questionList').prepend(data); --}}
                    $("form#addquestion")[0].reset();
                    swal("Berhasil !", "Modul berhasil ditambahkan", "success");
                },
                cache: false,
                contentType: false,
                processData: false
            });

            return false;
        });

    });
</script>

<script>
    function quiz(id) {
        window._wq = window._wq || [];
        _wq.push({ id: "{{ $material->wistia_media_hashed_id }}", onReady: function(video) {
            console.log("I got a handle to the video!", video);
            video.pause();
        }});
        $('#quiz' + id).modal({
            backdrop: 'static',
            keyboard: false
        });
        $('#quiz' + id).modal('show');
    }

    function postAnswerQuiz(id) {
        console.log('Try Submit');

        var answer = $('input[name=answer' + id + ']:checked').val();;

        var formData = new FormData();
        formData.append('_token', '{{ csrf_token() }}');
        formData.append('answer', answer);
        formData.append('log', 'LOG_ANSWER_QUIZ');

        $.ajax({
            url: '/social-learning-my-course/quiz/' + id + '/store',
            type: 'POST',
            data: formData,
            success: function (data) {
                $('#quiz' + id).modal('hide');
                window._wq = window._wq || [];
                _wq.push({ id: "{{ $material->wistia_media_hashed_id }}", onReady: function(video) {
                    console.log("I got a handle to the video!", video);
                    video.play();
                }});
                swal("Berhasil !", "Jawaban Benar", "success");
            },
            error: function (data) {
                console.log(data);
                $('#quiz' + id).modal('hide');
                window._wq = window._wq || [];
                _wq.push({ id: "{{ $material->wistia_media_hashed_id }}", onReady: function(video) {
                    console.log("I got a handle to the video!", video);
                    video.play();
                }});
                swal("Salah !", "Jawaban Salah", "error");
            },
            cache: false,
            contentType: false,
            processData: false
        });

    }

    function onQuestionComment(questionId, userId) {
        console.log('Try Submit');
        var content = $('#questioncomment' + questionId).val();

        var formData = new FormData();
        formData.append('_token', '{{ csrf_token() }}');
        formData.append('content', content);
        formData.append('log', 'LOG_COMMENTS');
        formData.append('user_id', userId);

        $.ajax({
            url: '/questions/' + questionId + '/comments',
            type: 'POST',
            data: formData,
            success: function (data) {
                $('#question' + questionId).replaceWith(data);
            },
            error: function (data) {
                console.log(data);
            },
            cache: false,
            contentType: false,
            processData: false
        });

    }

    function onQuestionLike(questionId, userId) {
        var formData = new FormData();
        formData.append('log', 'LOG_LIKE_QUESTIONS');
        formData.append('user_id', userId);

        $.ajax({
            url: '/questions/' + questionId + '/likes',
            type: 'POST',
            data: formData,
            success: function (data) {
                $('#question' + questionId).replaceWith(data);
            },
            cache: false,
            contentType: false,
            processData: false
        });

    }

    function onQuestionDislike(questionId, userId) {
        var formData = new FormData();
        formData.append('log', 'LOG_DISLIKE_QUESTIONS');
        formData.append('user_id', userId);

        $.ajax({
            url: '/questions/' + questionId + '/dislikes',
            type: 'POST',
            data: formData,
            success: function (data) {
                $('#question' + questionId).replaceWith(data);
            },
            cache: false,
            contentType: false,
            processData: false
        });

    }

    function onQuestionLikeComment(commentId, questionId, userId) {
        var formData = new FormData();
        formData.append('log', 'LOG_LIKE_COMMENTS');
        formData.append('user_id', userId);

        $.ajax({
            url: '/questions/comments/' + commentId + '/likes',
            type: 'POST',
            data: formData,
            success: function (data) {
                $('#question' + questionId).replaceWith(data);
            },
            cache: false,
            contentType: false,
            processData: false
        });

    }

    function onQuestionDislikeComment(commentId, questionId, userId) {
        var formData = new FormData();
        formData.append('log', 'LOG_DISLIKE_COMMENTS');
        formData.append('user_id', userId);

        $.ajax({
            url: '/questions/comments/' + commentId + '/dislikes',
            type: 'POST',
            data: formData,
            success: function (data) {
                $('#question' + questionId).replaceWith(data);
            },
            cache: false,
            contentType: false,
            processData: false
        });

    }

    function onEditPost(postId, materialId) {
        $('#modalEdit' + postId).modal('hide');

        var content = $('#inputPostContent').val();
        {{-- var link = $('#inputPostLink').val();
        var photo = $('input#inputPostPhoto')[0].files[0]; --}}

        var formData = new FormData();

        console.log('{{ csrf_token() }}');
        
        formData.append('_token', '{{ csrf_token() }}');
        formData.append('content', content);
        formData.append('material_id', materialId);

        console.log(formData.get('content'));
        console.log(formData.get('material_id'));

        $.ajax({
            url: '/social-learning-questions/' + postId,
            type: 'PUT',
            data: formData,
            success: function (data) {
                $('#question' + postId).replaceWith(data);
            },
            cache: false,
            contentType: false,
            processData: false
        });

        return false;

    }

    function onDeleteQuestion(postId) {

        swal({
            title: "Apa kamu yakin?",
            text: "Data yang telah di hapus tidak dapat dikembalikan",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {

            if (willDelete) {

                $.ajax({
                    url: '/social-learning-questions/' + postId,
                    type: 'DELETE',
                    data: null,
                    success: function (data) {
                        $('#question' + postId).remove();
                        swal("Berhasil !", "Pertanyaan berhasil dihapus", "success");
                    },
                    error: function (data) {
                        $('#question' + postId).remove();
                        swal("Berhasil !", "Pertanyaan berhasil dihapus", "success");
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });

            }

        });
    }
</script>
@endsection
