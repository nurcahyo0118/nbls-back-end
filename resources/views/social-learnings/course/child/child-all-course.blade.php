<div id="allCourseList" class="row">

    @foreach($courses as $course)

        <div class="col-md-4 m-t-20">
            <div class="card">
                @if( $course->image === null)
                    <img class="card-img-top" src="/images/order/web_design.jpg" style="height: 200px"
                         alt="Card image cap">
                @else
                    <img class="card-img-top" src="/xxcourses/images/{{ $course->image }}" style="height: 200px"
                         alt="Card image cap">
                @endif
                <div class="card-body">
                    <p class="text-muted">
                        <small>By {{ $course->author->fullname }} |
                            <i class="ion-clock"></i> {{ $course->updated_at->diffForHumans() }}
                        </small>
                    </p>
                    <h6 style="height: 40px"><strong>{{ $course->title }}</strong></h6>
                    <p>
                        <i class="fa fa-users"> {{ $course->users->count() }} Siswa</i>
                    </p>
                </div>

                <div class="card-footer text-center btn-primary btn-block">
                    <a class="text-white"
                       href="{{ route('social-learning-course.show', [$_SERVER['HTTP_HOST'], $course->id]) }}">
                        VIEW DETAILS
                        <i class="md-arrow-forward"></i>
                    </a>
                </div>
            </div>
        </div>

    @endforeach

</div>