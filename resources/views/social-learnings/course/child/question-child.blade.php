<div id="question{{ $question->id }}" class="col-lg-12">
    <div class=" m-t-20">
        <div class="card-box">
            <div class="media m-b-30">
                <a>
                    @if($question->user->photo === null)
                        <img class="d-flex mr-3 rounded-circle thumb-sm"
                             src="/images/default.png" alt="Generic placeholder image">
                    @else
                        <img class="d-flex mr-3 rounded-circle thumb-sm"
                             src="/images/photo/{{ $question->user->photo }}" alt="Generic placeholder image">
                    @endif
                </a>

                <div class="media-body">

                    @if($question->user->id === Auth::id())
                        <a style="color: black" class="media-meta pull-right" href="#"
                           role="button" id="dropdownMenuLink" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            &nbsp;
                            <i class="fa fa-ellipsis-v"></i>
                            &nbsp;
                        </a>

                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <a class="dropdown-item" data-toggle="modal"
                               data-target="#modalEditQuestion{{ $question->id }}">Edit
                            </a>
                            <a class="dropdown-item" onclick="onDeleteQuestion({{ $question->id }})">Delete
                            </a>
                        </div>

                    @endif

                    <h4 class="text-primary font-16 m-0">
                        <a href="{{ route('social-learning-posts.show', $question->user->username) }}">
                            {{ $question->user->fullname }}
                        </a>
                        <small class="text-muted">
                            <i class="ion-clock"></i>
                            {{ $question->created_at->diffForHumans() }}
                        </small>
                    </h4>


                </div>

            </div>

            <p>{{ $question->content }}</p>

            <hr>

            <div class="row">
                <div class="col-md-12">
                    <a class="post-icon" onclick="onQuestionLike({{ $question->id }}, {{ Auth::id() }})">
                        <i class="fa fa-thumbs-o-up {{ $question->likes()->where('user_id', Auth::id())->count() ? 'like' : '' }}">
                            {{ $question->likes()->count() }}
                        </i>
                    </a>

                    <a class="post-icon" onclick="onQuestionDislike({{ $question->id }}, {{ Auth::id() }})">
                        <i class="fa fa-thumbs-o-down {{ $question->dislikes()->where('user_id', Auth::id())->count() ? 'dislike' : '' }}">
                            {{ $question->dislikes()->count() }}
                        </i>
                    </a>

                    <a class="pull-right post-icon">
                        <p class="fa fa-comments-o"> {{ $question->comments()->count() }} </p>
                    </a>
                </div>

            </div>
            <hr>

            <div>

                @foreach($question->comments as $comment)
                    <div class="comment" style="background-colot:p ">
                        @if( Auth::user()->photo === null)
                            <img src="/images/default.png" alt="" class="comment-avatar">
                        @else
                            <img src="/images/photo/{{ Auth::user()->photo }}" alt="" class="comment-avatar">
                        @endif

                        <div class="comment-body">
                            <div class="comment-text">
                                <div class="comment-header">
                                    <a href="#" title="">{{ $comment->user->fullname }}</a>
                                    <span>about {{ $comment->created_at->diffForHumans() }}</span>
                                    @if(App\BestAnswerSocialLearning::where(['question_id' => $question->id])->first() == null)
                                        <span class="float-right label label-primary text-white">
                                            <form method="POST" action="/social-learning-best-answers">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="user_id" value="{{ $comment->user->id }}">
                                                <button class="label label-primary m-0" type="submit" name="question_id"
                                                        value="{{ $question->id }}">
                                                    <small class="text-white">Pilih Best Answer</small>
                                                </button>
                                            </form>
                                        </span>
                                    @endif
                                </div>
                                {{ $comment->body }}
                            </div>
                            <div class="comment-footer">
                                <a onclick="onQuestionLikeComment({{ $comment->id }}, {{ $question->id }}, {{ Auth::id() }})">
                                    <i class="fa fa-thumbs-o-up {{ $comment->likes()->where('user_id', Auth::id())->count() ? 'like' : '' }}"></i>
                                    {{ $comment->likes()->count() }}
                                </a>
                                &nbsp;
                                <a onclick="onQuestionDislikeComment({{ $comment->id }}, {{ $question->id }}, {{ Auth::id() }})">
                                    <i class="fa fa-thumbs-o-down {{ $comment->dislikes()->where('user_id', Auth::id())->count() ? 'dislike' : '' }}"></i>
                                    {{ $comment->dislikes()->count() }}
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach

                <div class="media m-b-30">

                    @if(Auth::user()->photo === null)
                        <img class="d-flex mr-3 rounded-circle thumb-sm"
                             src="/images/default.png" alt="Generic placeholder image">
                    @else
                        <img class="d-flex mr-3 rounded-circle thumb-sm"
                             src="/images/photo/{{ Auth::user()->photo }}" alt="Generic placeholder image">
                    @endif

                    <div class="media-body">
                        <form id="formaddcomment"
                              onsubmit="onQuestionComment({{ $question->id }}, {{ Auth::id() }}); return false;"
                              method="post" data-parsley-validate>

                            <input id="questioncomment{{ $question->id }}" type="text" name="content"
                                   class="form-control"
                                   placeholder="Tulis komentar...">

                        </form>

                    </div>
                </div>
                <hr>
            </div>
        </div>

    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalEditQuestion{{ $question->id }}" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Status</h5>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">

                <form id="formeditpost" action="{{ route('social-learning-questions.update', $question->id) }}" method="post"
                      data-parsley-validate>

                    <div class="modal-body">
                        <fieldset>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <input name="_method" type="hidden" value="PUT">

                            <input type="hidden" name="material_id" value="{{ $material->id }}">

                            <div class="col-md-12">
                                <div class="form-group">
                                  <textarea id="inputPostContent" type="text" name="content"
                                            class="form-control"
                                            rows="4">{{ $question->content }}</textarea>
                                </div>
                            </div>

                        </fieldset>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal">
                            <i class="fa fa-close"></i>
                            Tutup
                        </button>
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-save"></i>
                            Simpan
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
