<div id="post{{ $question->id }}" class="col-lg-12">
    <div class=" m-t-20">

        <div class="media m-b-30">
            @if(Auth::user()->photo === null)
                <img class="d-flex mr-3 rounded-circle thumb-sm"
                     src="/images/default.png" alt="Generic placeholder image">
            @else
                <img class="d-flex mr-3 rounded-circle thumb-sm"
                     src="/images/photo/{{ Auth::user()->photo }}" alt="Generic placeholder image">
            @endif
            <div class="media-body">

                @if($question->user->id === Auth::id())
                    <span class="media-meta pull-right text-primary" href="#"
                          role="button" id="dropdownMenuLink" data-toggle="dropdown"
                          aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-bookmark"></i>
                    </span>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item" data-toggle="modal"
                           data-target="#modalEdit{{ $question->id }}">Edit
                        </a>
                        <a class="dropdown-item" onclick="onDeletePost({{ $question->id }})">Delete
                        </a>
                    </div>
                @endif
                <h4 class="text-primary font-16 m-0">
                    {{ $question->user->fullname }}
                    <small class="text-muted">
                        <i class="ion-clock"></i>
                        {{ $question->created_at->diffForHumans() }}
                    </small>
                </h4>

            </div>
        </div>

        <p>{{ $question->content }}</p>

        <hr>

        <div class="row">
            <div class="col-md-2">
                <a onclick="onLike({{ $question->id }})">
                    <i class="fa fa-thumbs-o-up {{ $question->likes()->where('user_id', Auth::id())->count() ? 'like' : '' }}">
                        {{ $question->likes()->count() }}
                    </i>
                </a>
            </div>

            <div class="col-md-2">
                <a onclick="onDislike({{ $question->id }})">
                    <i class="fa fa-thumbs-o-down {{ $question->dislikes()->where('user_id', Auth::id())->count() ? 'dislike' : '' }}">
                        {{ $question->dislikes()->count() }}
                    </i>
                </a>
            </div>

            <div class="col-md-5">
                <a>
                    <p class="fa fa-comments-o"> {{ $question->postComments()->count() }} </p>
                </a>
            </div>
        </div>
        <hr>

        <div>

            @foreach($question->postComments as $comment)
                <div class="comment">
                    @if( Auth::user()->photo === null)
                        <img src="/images/default.png" alt="" class="comment-avatar">
                    @else
                        <img src="/images/photo/{{ Auth::user()->photo }}" alt="" class="comment-avatar">
                    @endif

                    <div class="comment-body">
                        <div class="comment-text">
                            <div class="comment-header">
                                <a href="#" title="">{{ $comment->user->fullname }}</a>
                                <span>about {{ $comment->created_at->diffForHumans() }}</span>
                            </div>
                            {{ $comment->body }}
                        </div>
                        <div class="comment-footer">
                            <a onclick="onLikeComment({{ $comment->id }}, {{ $question->id }})">
                                <i class="fa fa-thumbs-o-up {{ $comment->likes()->where('user_id', Auth::id())->count() ? 'like' : '' }}"></i>
                                {{ $comment->likes()->count() }}
                            </a>
                            &nbsp;
                            <a onclick="onDislikeComment({{ $comment->id }}, {{ $question->id }})">
                                <i class="fa fa-thumbs-o-down {{ $comment->dislikes()->where('user_id', Auth::id())->count() ? 'dislike' : '' }}"></i>
                                {{ $comment->dislikes()->count() }}
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach

            <div class="media m-b-30">

                <img class="d-flex mr-3 rounded-circle thumb-sm"
                     src="/images/users/avatar-3.jpg"
                     alt="Generic placeholder image">

                <div class="media-body">
                    <form id="formaddcomment" onsubmit="onComment({{ $question->id }});return false;"
                          method="post" data-parsley-validate>

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <input id="postcomment{{ $question->id }}" type="text" name="content" class="form-control"
                               placeholder="Tulis komentar...">

                    </form>

                </div>
            </div>
            <hr>
        </div>

    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalEdit{{ $question->id }}" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Status</h5>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">

                <form id="formeditpost" onsubmit="onEditPost({{ $question->id }});return false;" method="post"
                      data-parsley-validate>

                    <div class="modal-body">
                        <fieldset>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <input name="_method" type="hidden" value="PUT">

                            <input type="hidden" name="post_id" value="{{ $question->id }}">

                            <div class="col-md-12">
                                <div class="form-group">
                                      <textarea type="text" name="content"
                                                class="form-control"
                                                rows="4">{{ $question->content }}</textarea>
                                </div>
                            </div>

                        </fieldset>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal">
                            <i class="fa fa-close"></i>
                            Tutup
                        </button>
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-save"></i>
                            Simpan
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
    