<div class="col-md-12" style="margin-bottom: 10px;">
    <div class="card card-default">
        <div class="card-body">
            <div class="media">
                <img class="d-flex mr-3 rounded-circle thumb-sm" src="/images/default.png"
                     alt="Generic placeholder image" width="30px" height="30px"> 
                <div class="media-body">
                    <h6 class="mt-0"><strong>{{ $review->user->fullname }}</strong></h6>
                    <h6>
                        <i class="fa fa-star{{ $review->rate >= 1 ? '' : '-o' }}"></i>
                        <i class="fa fa-star{{ $review->rate >= 2 ? '' : '-o' }}"></i>
                        <i class="fa fa-star{{ $review->rate >= 3 ? '' : '-o' }}"></i>
                        <i class="fa fa-star{{ $review->rate >= 4 ? '' : '-o' }}"></i>
                        <i class="fa fa-star{{ $review->rate >= 5 ? '' : '-o' }}"></i>
                    </h6>
                    {{ $review->suggestion }}
                </div>
            </div>
        </div>
    </div>

</div>