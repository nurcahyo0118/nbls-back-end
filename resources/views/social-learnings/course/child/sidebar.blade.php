<div class="col-md-4">
    <div class="card">
        <div class="card-header bg-info p-4">

        </div>
        <div class="card-body">
            <div class="slimscroller p-l-r-10" style="max-height: 400px;">

                @foreach($course->sections as $index => $section)
                    <div class="pb-2">
                        <h4 class="text-primary">
                            <strong>Modul {{ $index + 1 }} : {{ $section->title }}</strong>
                        </h4>

                        @foreach($section->materials as $materialindex => $materialSidebar)
                            <h5>
                                <i class="fa fa-chevron-circle-right text-secondary"></i> &nbsp;
                                <a 
                                    href="{{ route('social-learning-my-course.material', [$_SERVER['HTTP_HOST'], $course->id, $materialSidebar->id]) }}" 
                                    style="color: {{ Auth::user()->viewedVideos()->where(['material_id' => $materialSidebar->id, 'user_id' => Auth::id()])->first() != null ? '':'black !important'}};">
                                    {{ $materialSidebar->title }}
                                </a>
                            </h5>
                            <hr>
                        @endforeach

                        @foreach($section->essays as $essayindex => $essaySidebar)
                            <h5>
                                <i class="fa fa-comments text-secondary"></i> &nbsp;
                                <a 
                                    href="{{ route('social-learning-my-course.essay', [$_SERVER['HTTP_HOST'], $course->id, $essaySidebar->id]) }}">
                                    {{ $essaySidebar->title }}
                                </a>
                            </h5>
                            <hr>
                        @endforeach

                        @foreach($section->exercises as $exerciseindex => $exerciseSidebar)
                            <h5>
                                <i class="fa fa-star text-secondary"></i> &nbsp;
                                <a 
                                    href="{{ route('social-learning-my-course.exercise', [$_SERVER['HTTP_HOST'], $course->id, $exerciseSidebar->id]) }}"
                                    style="color: {{ Auth::user()->viewedExercises()->where(['exercise_id' => $exerciseSidebar->id, 'user_id' => Auth::id()])->first() != null ? '':'black !important'}};">
                                    {{ $exerciseSidebar->title }}
                                </a> 
                            </h5>
                            <hr>
                        @endforeach

                    </div>
                    <hr>
                @endforeach
            </div>
        </div>

        <div class="card-footer bg-info p-4">
        </div>
    </div>
</div>