<div id="answer{{ $answer->id }}" class="col-lg-12">
    <div class=" m-t-20">
        <div class="card-box">
            <div class="media m-b-30">
                <a>
                    @if($answer->user->photo === null)
                        <img class="d-flex mr-3 rounded-circle thumb-sm"
                             src="/images/default.png" alt="Generic placeholder image">
                    @else
                        <img class="d-flex mr-3 rounded-circle thumb-sm"
                             src="/images/photo/{{ $answer->user->photo }}" alt="Generic placeholder image">
                    @endif
                </a>

                <div class="media-body">

                    <a href="{{ route('social-learning-posts.show', [$_SERVER['HTTP_HOST'], $answer->user->username]) }}">
                        <h4 class="text-primary font-16 m-0">
                            {{ $answer->user->fullname }}
                            <small class="text-muted">
                                <i class="ion-clock"></i>
                                {{ $answer->created_at->diffForHumans() }}
                            </small>
                        </h4>
                    </a>

                </div>

            </div>

            <p>{{ $answer->body }}</p>

            <hr>

            <div class="row">
                <div class="col-md-12">
                    <a class="post-icon" onclick="onAnswerLike({{ $answer->id }})">
                        <i class="fa fa-thumbs-o-up {{ $answer->likes()->where('user_id', Auth::id())->count() ? 'like' : '' }}">
                            {{ $answer->likes()->count() }}
                        </i>
                    </a>

                    <a class="post-icon" onclick="onAnswerDislike({{ $answer->id }})">
                        <i class="fa fa-thumbs-o-down {{ $answer->dislikes()->where('user_id', Auth::id())->count() ? 'dislike' : '' }}">
                            {{ $answer->dislikes()->count() }}
                        </i>
                    </a>

                    <a class="pull-right post-icon">
                        <p class="fa fa-comments-o"> {{ $answer->comments()->count() }} </p>
                    </a>
                </div>

            </div>
            <hr>

            <div>

                @foreach($answer->comments as $comment)
                    <div class="comment" style="background-colot:p ">
                        @if( Auth::user()->photo === null)
                            <img src="/images/default.png" alt="" class="comment-avatar">
                        @else
                            <img src="/images/photo/{{ Auth::user()->photo }}" alt="" class="comment-avatar">
                        @endif

                        <div class="comment-body">
                            <div class="comment-text">
                                <div class="comment-header">
                                    <a href="#" title="">{{ $comment->user->fullname }}</a>
                                    <span>about {{ $comment->created_at->diffForHumans() }}</span>
                                    <span class="float-right label label-primary text-white">
                                        <small>Pilih Best Answer</small>
                                    </span>
                                </div>
                                {{ $comment->body }}
                            </div>
                            
                        </div>
                    </div>
                @endforeach

                <div class="media m-b-30">

                    @if(Auth::user()->photo === null)
                        <img class="d-flex mr-3 rounded-circle thumb-sm"
                             src="/images/default.png" alt="Generic placeholder image">
                    @else
                        <img class="d-flex mr-3 rounded-circle thumb-sm"
                             src="/images/photo/{{ Auth::user()->photo }}" alt="Generic placeholder image">
                    @endif

                    <div class="media-body">
                        <form id="formaddcomment" onsubmit="onAnswerComment({{ $answer->id }}); return false;"
                              method="post" data-parsley-validate>

                            <input id="answerBody{{ $answer->id }}" type="text" name="body"
                                   class="form-control"
                                   placeholder="Tulis komentar...">

                        </form>

                    </div>
                </div>
                <hr>
            </div>
        </div>

    </div>
</div>
