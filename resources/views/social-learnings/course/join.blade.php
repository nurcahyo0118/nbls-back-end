<div id="join{{ $course->id }}" class="col-md-4">

    @if($course->users()->where(['user_id' => Auth::id()])->count() === 0)
        @if($course->price == 0)
            <button type="button" onclick="onTakeFreeCourse({{ $course->id }})" class="btn btn-primary btn-block">
                Gabung
            </button>
        @else
            <button type="button" onclick="onTakeCourse({{ $course->id }})" class="btn btn-primary btn-block">Gabung
            </button>
        @endif
    @else
        <button type="button" onclick="onLeaveCourse({{ $course->id }})" class="btn btn-primary btn-block">Telah
                                                                                                           Bergabung
        </button>
    @endif
</div>
