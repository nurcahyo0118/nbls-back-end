@extends('layouts.frontend-no-right-left')

@section('content')

    @php

    @endphp
    <div class="row">

        

        <div class="container">
            <h6 class="text-muted">
                <span class="fa fa-home"></span>
                Home
                <span class="fa fa-long-arrow-right"></span>
                Course
            </h6>

            <h4 class="text-center mt-5"><strong>{{ $course->title }}</strong></h4>
            <hr width="35%">

            <div class="row mt-5 mb-5">
                @if($course->video != null)
                    <div class="col-md-6">
                        <iframe width="560" height="315" src="{{ str_replace('watch?v=', 'embed/', $course->video) }}"
                                frameborder="0" allowfullscreen></iframe>
                    </div>
                @else
                    <div class="col-md-3"></div>
                @endif
                <div class="col-md-6 mt-5 pl-5">
                    <div class="row">

                        @include('social-learnings.course.join')

                        <div class="col-md-8 p-2">
                            <span class="label label-success p-2">
                                @if($course->price != 0)
                                    Rp. {{ number_format($course->price, 0, ',', '.') }}
                                @else
                                    Gratis
                                @endif
                            </span>
                        </div>
                    </div>
                    <div class="mt-3">
                        <div class="card p-1 mb-1">
                            <div class="card-title">
                                <strong>Siapa Yang Cocok mengambiil kursus Ini !</strong>
                                <hr>
                                <p>{!! $course->suitable !!}</p>
                            </div>
                        </div>
                        <div class="card p-1 mb-1">
                            <div class="card-title">
                                <strong>Apa Yang Akan Anda Peroleh!</strong>
                                <hr>
                                <p>{!! $course->can_be !!}</p>
                            </div>

                        </div>
                        <div class="card p-1 mb-1">
                            <div class="card-title">
                                <strong>Apa Yang Harus Anda Persiapkan!</strong>
                                <hr>
                                <p>{!! $course->requirement !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <h4 class="text-center"><strong>Fitur Apa Yang Anda Dapatkan?</strong></h4>
            <hr width="35%">
            <!-- row -->
            <div class="row mt-5 ">
                <div class="col-md-3  text-center">
                    <div class="col-md-12">
                        <img src="/images/icon/sertifikat.png" alt="">
                    </div>
                    <div class="col-md-12 mt-2">
                        <strong class="mt-2">SERTIFIKAT RESMI</strong>
                    </div>
                </div>

                <div class="col-md-3  text-center">
                    <div class="col-md-12">
                        <img src="/images/icon/ternama.png" alt="">
                    </div>
                    <div class="col-md-12 mt-2">
                        <strong class="mt-2">PRAKTIS TERNAMA</strong>
                    </div>
                </div>

                <div class="col-md-3 text-center">
                    <div class="col-md-12">
                        <img src="/images/icon/pengajar.png" alt="">
                    </div>
                    <div class="col-md-12 mt-2">
                        <strong class="mt-2">INSTRUKTUR SELALU ONLINE</strong>
                    </div>
                </div>

                <div class="col-md-3 text-center">
                    <div class="row">
                        <div class="col-md-12">
                            <img src="/images/icon/kampus.png" alt="">
                        </div>
                        <div class="col-md-12 mt-2">
                            <strong class="mt-2">KOMUNITAS ESKLUSIF</strong>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->

            <br>
            <br>
            <br>

            <h4 class="text-center"><strong>Ulasan</strong></h4>
            <hr width="35%">

            @if($course->reviews()->count() < 1)
                <div class="text-center">
                    <h4>Belum ada ulasan.</h4>
                </div>
            @endif

            @if($my_review != null)
                <h5><strong>Ulasan saya</strong></h5>
                <hr>

                <div class="row mt-5">
                    <div class="col-md-12">
                        <div class="card card-default">
                            <div class="card-body">
                                <div class="media">
                                    <img class="d-flex mr-3 rounded-circle thumb-sm" src="/images/default.png"
                                         alt="Generic placeholder image">
                                    <div class="media-body">
                                        <h5 class="mt-0"><strong>{{ Auth::user()->fullname }}</strong></h5>
                                        <h5>
                                            <i class="fa fa-star{{ $my_review->rate >= 1 ? '' : '-o' }}"></i>
                                            <i class="fa fa-star{{ $my_review->rate >= 2 ? '' : '-o' }}"></i>
                                            <i class="fa fa-star{{ $my_review->rate >= 3 ? '' : '-o' }}"></i>
                                            <i class="fa fa-star{{ $my_review->rate >= 4 ? '' : '-o' }}"></i>
                                            <i class="fa fa-star{{ $my_review->rate >= 5 ? '' : '-o' }}"></i>
                                        </h5>
                                        {{ $my_review->suggestion }}
                                        <hr>

                                        <button type="button" onclick="onDeleteReview({{ $my_review->id }})"
                                                class="btn btn-danger pull-right">
                                            <i class="fa fa-trash"></i>
                                            Hapus
                                        </button>

                                        <button type="button" class="btn btn-default pull-right"
                                                style="margin-right: 10px" data-toggle="modal"
                                                data-target="#reviewEditForm">
                                            <i class="fa fa-edit"></i>
                                            Edit
                                        </button>


                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            @else
                <button type="button" class="btn btn-primary text-right" data-toggle="modal" data-target="#reviewForm">
                    Tulis Ulasan
                </button>
            @endif

            <h5><strong>Daftar Ulasan</strong></h5>
            <hr>
            <div class="row mt-5">
                @foreach($course->reviews as $review)
                    @include('social-learnings.course.child.review-child')
                @endforeach
            </div>
            <!-- end row -->

            @php
                $related_courses = App\Course::where('category_id', $course->category_id)->where('id', '<>', $course->id)->take(4)->get()
            @endphp

            @if($related_courses->count() != 0)
            <!-- jumbotron -->
                <div class="jumbotron mt-5 footer-space">
                    <h4 class="text-center mt-5"><strong>KURSUS TERKAIT</strong></h4>
                    <hr width="35%">

                    <div class="container">
                        <div class="row">

                        @foreach($related_courses as $related_course)
                            <!-- card Kursus -->
                                <div class="col-md-3 m-t-20">
                                    <div class="card">
                                        @if($related_course->image === null)
                                            <img class="card-img-top" src="/images/order/web_design.jpg"
                                                 alt="Card image cap">
                                        @else
                                            <img class="card-img-top"
                                                 src="/xxcourses/images/{{ $related_course->image }}"
                                                 style="height: 150px"
                                                 alt="Card image cap">
                                        @endif
                                        <div class="card-body">
                                            <p class="text-muted">
                                                <small>By {{ $related_course->author->fullname }} |
                                                    <i class="ion-clock"></i> {{ $related_course->updated_at->diffForHumans() }}
                                                </small>
                                            </p>

                                            <h6 style="height:40px;">
                                                <strong>{{ str_limit($related_course->title, 30) }}</strong></h6>
                                            <p>
                                                <i class="fa fa-users"> {{ $related_course->users->count() }} Siswa</i>
                                            </p>
                                        </div>

                                        <div class="card-footer text-center btn-primary btn-block">
                                            <a class="text-white"
                                               href="{{ route('social-learning-course.show', [$_SERVER['HTTP_HOST'], $related_course->id]) }}">
                                                VIEW DETAILS
                                                <i class="md-arrow-forward"></i>
                                            </a>
                                        </div>

                                    </div>
                                </div>
                                <!-- end card Kursus -->
                            @endforeach
                        </div>
                    </div>

                </div>
            <!-- end jumbotrol -->
            @endif

        </div>

    </div>


    @if($my_review != null)
        {{--  MODAL EDIT  --}}
        <div class="modal fade" id="reviewEditForm" tabindex="-1" role="dialog" aria-labelledby="reviewEditForm"
             aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tulis Ulasan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <form id="rating" action="{{ route('social-learning-course.update-rating') }}" method="POST">
                        <div class="modal-body">

                            {{ csrf_field() }}

                            <input type="hidden" name="_method" value="put">

                            <input type="hidden" name="course_id" value="{{ $course->id }}">

                            <fieldset>
                                <div class="col-md-12">
                                    <div class="stars">
                                        <input class="star star-5" id="star-5" type="radio" name="star" value="5"
                                               {{ $review->rate == 5 ? 'checked' : '' }} required>
                                        <label class="star star-5" for="star-5"></label>
                                        <input class="star star-4" id="star-4" type="radio" name="star" value="4"
                                               {{ $review->rate == 4 ? 'checked' : '' }} required>
                                        <label class="star star-4" for="star-4"></label>
                                        <input class="star star-3" id="star-3" type="radio" name="star" value="3"
                                               {{ $review->rate == 3 ? 'checked' : '' }} required>
                                        <label class="star star-3" for="star-3"></label>
                                        <input class="star star-2" id="star-2" type="radio" name="star" value="2"
                                               {{ $review->rate == 2 ? 'checked' : '' }} required>
                                        <label class="star star-2" for="star-2"></label>
                                        <input class="star star-1" id="star-1" type="radio" name="star" value="1"
                                               {{ $review->rate == 1 ? 'checked' : '' }} required>
                                        <label class="star star-1" for="star-1"></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Saran : </label>
                                    <textarea class="form-control" name="suggestion"
                                              required>{{ $my_review->suggestion }}</textarea>
                                </div>
                            </fieldset>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Kirim</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @else
        {{--  MODAL INPUT  --}}
        <div class="modal fade" id="reviewForm" tabindex="-1" role="dialog" aria-labelledby="reviewForm"
             aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tulis Ulasan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <form id="rating" action="{{ route('social-learning-course.rate') }}" method="POST">
                        <div class="modal-body">
                            {{ csrf_field() }}

                            <input type="hidden" name="course_id" value="{{ $course->id }}">

                            <fieldset>
                                <div class="col-md-12">
                                    <div class="stars">
                                        <input class="star star-5" id="star-5" type="radio" name="star" value="5"
                                               required>
                                        <label class="star star-5" for="star-5"></label>
                                        <input class="star star-4" id="star-4" type="radio" name="star" value="4"
                                               required>
                                        <label class="star star-4" for="star-4"></label>
                                        <input class="star star-3" id="star-3" type="radio" name="star" value="3"
                                               required>
                                        <label class="star star-3" for="star-3"></label>
                                        <input class="star star-2" id="star-2" type="radio" name="star" value="2"
                                               required>
                                        <label class="star star-2" for="star-2"></label>
                                        <input class="star star-1" id="star-1" type="radio" name="star" value="1"
                                               required>
                                        <label class="star star-1" for="star-1"></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Saran : </label>
                                    <textarea class="form-control" name="suggestion" required></textarea>
                                </div>
                            </fieldset>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Kirim</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endif

@endsection

@section('script')
    <script>
        $(function () {
            {{--  $("form#rating").submit(function(e) {
                e.preventDefault();
                var formData = new FormData(this);

                $.ajax({
                    url: '/course/rate',
                    type: 'POST',
                    data: formData,
                    success: function (data) {
                        swal("Berhasil !", "Berhasil mengirim ulasan", "success");
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });

            });  --}}

        });
    </script>
    <script>
        function onTakeCourse(courseId) {
            $.ajax({
                url: '/social-learning-my-course/' + courseId + '/add-to-cart',
                type: 'POST',
                data: null,
                success: function (data) {
                    console.log(data);
                    $('#join' + courseId).replaceWith(data);
                    swal("Berhasil !", "Berhasil menambahkan kursus kedalam keranjang", "success");
                },
                error: function (error) {
                    swal("Gagal !", "Gagal bergabung", "error");
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }

        function onTakeFreeCourse(courseId) {

            $.ajax({
                url: '/courses/' + courseId + '/join',
                type: 'POST',
                data: null,
                success: function (data) {
                    console.log(data);
                    $('#join' + courseId).replaceWith(data);
                    swal("Berhasil !", "Berhasil bergabung", "success");
                },
                error: function (error) {
                    swal("Gagal !", "Gagal bergabung", "error");
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }

        function onLeaveCourse(courseId) {
            $.ajax({
                url: '/courses/' + courseId + '/leave',
                type: 'POST',
                data: null,
                success: function (data) {
                    console.log(data);
                    $('#join' + courseId).replaceWith(data);
                    swal("Berhasil !", "Berhasil meninggalkan kursus", "success");
                },
                error: function (error) {
                    swal("Gagal !", "Gagal meninggalkan kursus", "error");
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }

        function onDeleteReview(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "Data yang telah di hapus tidak dapat dikembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {

                    $.ajax({
                        url: '/course/{{ $course->id }}/rate',
                        type: 'DELETE',
                        data: null,
                        success: function (data) {
                            window.location.reload();
                        },
                        error: function (error) {
                            swal("Gagal !", "Gagal meninggalkan kursus", "error");
                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    });

                }
            });
        }

        function addToCart(course_id)
        {
            var carts = [];

            console.log('Masuk Pak Eko');

            var carts_from_local = JSON.parse(window.localStorage.getItem('carts'));

            if(carts_from_local === null)
            {
                carts.push(course_id);
                window.localStorage.setItem('carts', JSON.stringify(carts));
            }
            else
            {
                if(carts_from_local.indexOf(course_id) < 0)
                {
                    carts_from_local.push(course_id);
                    window.localStorage.setItem('carts', JSON.stringify(carts_from_local));
                }

            }
        }

        function removeFromCart(course_id)
        {
            console.log('Keluar Pak Eko');

            var carts_from_local = JSON.parse(window.localStorage.getItem('carts'));
            var index = carts_from_local.indexOf(course_id);
            if(carts_from_local !== null && index > -1)
            {
                carts_from_local.splice(index, 1);
                window.localStorage.setItem('carts', JSON.stringify(carts_from_local));

                // remove item cart
                $('#cartsItem' + course_id).remove();
                $('#cartsItemDivider' + course_id).remove();

                var carts_from_local_latest = JSON.parse(window.localStorage.getItem('carts'));
                if(carts_from_local_latest.length === 0)
                {
                    $('#cartsList').replaceWith('<div id="cartsList" class="col-sm-12 col-md-12 col-md-12 text-center" style="color: #424242">Kosong !</div>');
                }
            }
        }

        function loadCartData(carts)
        {
            console.log(carts.length);
            var url = '/loads/frontend/carts?';

            if(carts !== null && carts.length !== 0)
            {
                for(i = 0; i < carts.length; i++)
                {
                    url = url + '&carts[]=' + carts[i];
                    console.log(url);
                }

                $.ajax({
                    type: 'GET',
                    url: url,
                    success: function (response) {
                        $('#cartsList').replaceWith(response.view);
                    },
                    error: function (error) {
                        console.log('ddddd');
                        $('#cartsList').replaceWith('<div id="cartsList" class="col-sm-12 col-md-12 col-md-12 text-center" style="color: #424242">Kosong !</div>');
                    }
                });
            }
            else
            {
                $('#cartsList').replaceWith('<div id="cartsList" class="col-sm-12 col-md-12 col-md-12 text-center" style="color: #424242">Kosong !</div>');
            }

        }
    </script>
@endsection