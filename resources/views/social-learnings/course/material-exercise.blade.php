@extends('layouts.frontend-no-right-left')

@section('content')
    <div class="row">
        
        @include('social-learnings.course.child.sidebar')

        <div class="col-md-8">

            <div class="card">

                @if(DB::table('user_exercises')->where(['exercise_id' => $exercise->id, 'user_id' => Auth::id()])->first() != null)
                    <div class="card-body">
                        <h3 class="text-center">Anda sudah mengirim jawaban !</h3>
                    </div>
                @else
                    <div class="card-body">

                        <label>Soal Latihan : </label>
                        <br>
                        <p>{{ $exercise->title }}</p>
                        <a href="/exercise-file/{{ $exercise->file }}" class="btn btn-default" download="/exercise-file/{{ $exercise->file }}">
                            <i class="fa fa-download"></i>
                            Unduh Soal Latihan
                        </a>

                        <hr>

                        <form 
                            action="{{ route('social-learning-my-course.exercise.store', [$_SERVER['HTTP_HOST'], $exercise->id]) }}" 
                            method="POST"
                            enctype="multipart/form-data">

                            <label>Unggah Jawaban : </label>
                            <br>

                            {{ csrf_field() }}

                            <input type="hidden" name="log" value="LOG_ANSWER_EXERCISE">

                            <div class="form-group">
                                <label> Tipe Kursus : </label>
                                <input type="radio" name="is_file" value="1" onclick="onSelectFile()" checked>
                                File
                                <input type="radio" name="is_file" value="0" onclick="onSelectLink()">
                                Link
                            </div>

                            <input id="exerciseFileInput" type="file" name="file">
                            
                            <div id="linkFormGroup" class="from-group">
                                <label>Link Jawaban : </label>
                                <input id="exerciseLinkInput" type="url" name="link" class="form-control">
                            </div>
                            
                            <br>
                            <br>

                            <button type="submit" class="btn btn-primary pull-right">
                                <i class="fa fa-paper-plane"></i>
                                Kirim
                            </button>
                        </form>

                    </div>
                @endif
                
            </div>
        </div>
        <!-- end row -->
    </div>

@endsection

@section('script')
    <script>
        $(function () {

            onSelectFile();

            $("form#addquestion").submit(function (e) {
                e.preventDefault();
                var formData = new FormData(this);

                $.ajax({
                    url: '/social-learning-questions',
                    type: 'POST',
                    data: formData,
                    success: function (data) {
                        $('#questionList').prepend(data);
                        $("form#addquestion")[0].reset();
                        swal("Berhasil !", "Modul berhasil ditambahkan", "success");
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });

                return false;
            });

        });
    </script>

    <script>
        function onSelectFile() {
            document.getElementById('linkFormGroup').style.display = 'none';
            document.getElementById('exerciseLinkInput').style.display = 'none';
            document.getElementById('exerciseFileInput').style.display = 'block';
            document.getElementById('exerciseFileInput').required = true;
            document.getElementById('exerciseLinkInput').required = false;
        }

        function onSelectLink() {
            document.getElementById('linkFormGroup').style.display = 'block';
            document.getElementById('exerciseFileInput').style.display = 'none';
            document.getElementById('exerciseLinkInput').style.display = 'block';
            document.getElementById('exerciseLinkInput').required = true;
            document.getElementById('exerciseFileInput').required = false;
        }

        function onQuestionComment(questionId) {
            console.log('Try Submit');
            var content = $('#questioncomment' + questionId).val();

            var formData = new FormData();
            formData.append('_token', '{{ csrf_token() }}');
            formData.append('content', content);
            formData.append('log', 'LOG_COMMENTS');

            $.ajax({
                url: '/questions/' + questionId + '/comments',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#question' + questionId).replaceWith(data);
                },
                error: function (data) {
                    console.log(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onQuestionLike(questionId) {
            var formData = new FormData();
            formData.append('log', 'LOG_LIKE_POSTS');

            $.ajax({
                url: '/questions/' + questionId + '/likes',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#question' + questionId).replaceWith(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onQuestionDislike(questionId) {
            var formData = new FormData();
            formData.append('log', 'LOG_DISLIKE_POSTS');

            $.ajax({
                url: '/questions/' + questionId + '/dislikes',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#question' + questionId).replaceWith(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onQuestionLikeComment(commentId, questionId) {
            var formData = new FormData();
            formData.append('log', 'LOG_LIKE_COMMENTS');

            $.ajax({
                url: '/questions/comments/' + commentId + '/likes',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#question' + questionId).replaceWith(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onQuestionDislikeComment(commentId, questionId) {
            var formData = new FormData();
            formData.append('log', 'LOG_DISLIKE_COMMENTS');

            $.ajax({
                url: '/questions/comments/' + commentId + '/dislikes',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#question' + questionId).replaceWith(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }
    </script>
@endsection