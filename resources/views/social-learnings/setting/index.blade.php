@extends('layouts.frontend')

@section('content')

    <div class="col-md-8 col-lg-6 footer-space">
        <h3 class="m-2">Pengaturan</h3>

        <br>

        @include('layouts.partials.message')

        @foreach($errors->all() as $error)
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;
                    </span>
                </button>
                {{ $error }}
            </div>
        @endforeach

        <div class="card-box">

            <div class="">
                <ul class="nav nav-tabs navtab-bg nav-justified">
                    <li class="nav-item">
                        <a href="#dasar" data-toggle="tab" aria-expanded="true" class="nav-link active">
                            Dasar
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#kontak" data-toggle="tab" aria-expanded="false" class="nav-link">
                            Kontak
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#pribadi" data-toggle="tab" aria-expanded="false" class="nav-link">
                            Pribadi
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#mediasosial" data-toggle="tab" aria-expanded="false" class="nav-link">
                            MediaSosial
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#ubahsandi" data-toggle="tab" aria-expanded="false" class="nav-link">
                            UbahSandi
                        </a>
                    </li>
                </ul>

                <!-- start dasar -->

                <form id="imageForm"
                      enctype="multipart/form-data"
                      method="post">

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <input type="hidden" name="_method" value="put">

                    {{-- <input id="inputPhotoProfile" type="file" name="photo" accept="image/*" style="display:none !important;"> --}}

                </form>

                <form action="social-learning-setting/update" method="post"
                      enctype="multipart/form-data" data-parsley-validate>

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="_method" value="put">

                    <div class="tab-content">
                        <div class="tab-pane active" id="dasar" aria-expanded="true">
                            <span class="media-meta pull-right text-primary">
                                <i class="fa fa-edit fa-3x"></i>
                            </span>
                            <h3 class="m-2">Informasi Dasar</h3>

                            <p class="text-muted m-2">Lengkapi profile anda agar orang lain bisa mengenali anda.</p>

                            <hr>

                            <div class="row m-2">
                                <div class="col-sm-4">
                                    <form id="imageForm"
                                        enctype="multipart/form-data"
                                        method="post">
                  
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    
                                        <input type="hidden" name="_method" value="put">

                                        @include('social-learnings.setting.child.image')

                                        <label class="btn btn-primary">
                                            <input id="inputPhotoProfile" type="file" name="photo" accept="image/*" style="display:none !important;">
                                                + Foto Image
                                        </label>

                                        <button id="saveCourseImage" 
                                            type="submit" 
                                            class="btn btn-success"
                                            style="display: none;">
                                            Simpan
                                        </button>
                                    </form>

                                    <br>

                                    <div id="progress" class="progress" style="display:none;">
                                        <div id="uploadprogress" class="progress-bar" role="progressbar"
                                             style="width: 0%;" aria-valuenow="25" aria-valuemin="0"
                                             aria-valuemax="100"></div>
                                    </div>

                                </div>

                                <div class="col-sm-8">

                                    <div class="bform-group border p-2 border-primary mb-3">
                                        <h6>Nama Lengkap</h6>
                                        <input type="text" name="fullname" value="{{ Auth::user()->fullname }}"
                                               class="form-control border-0"
                                               placeholder="Fullname">
                                    </div>

                                    <div class="form-group border p-2 border-success mb-3">
                                        <h6>Username</h6>
                                        <input type="text" name="username" value="{{ Auth::user()->username }}"
                                               class="form-control border-0"
                                               placeholder="Username">
                                    </div>

                                    <div class="form-group border p-2 border-danger mb-3">
                                        <h6>No KTP</h6>
                                        <input type="number"
                                                name="ktp_id" value="{{ Auth::user()->ktp_id }}"
                                                class="form-control border-0"
                                                maxlength="16"
                                                minlength="16"
                                                placeholder="13241xxxxxxxxx">
                                    </div>

                                    <div class=" form-group border p-2 border-primary mb-3">
                                        <h6>Jenis Kelamin</h6>
                                        <select class="form-control" name="sex">
                                            @if(Auth::user()->sex === null )
                                                <option value="{{ Auth::user()->sex }}">-SELECT-</option>
                                            @else
                                                <option value="{{ Auth::user()->sex }}">{{ Auth::user()->sex }}</option>
                                            @endif
                                            <option value="L">Laki-laki</option>
                                            <option value="P">Perempuan</option>
                                        </select>
                                    </div>

                                    <div class="form-group border p-2 border-primary mb-5">
                                        <h6>Tanggal Lahir</h6>
                                        <input type="date" name="birth_date" value="{{ Auth::user()->birth_date }}"
                                               class="form-control border-0">
                                    </div>

                                    <div class="">
                                        <div class="col-xs-8">
                                           
                                            <a class="text-muted">
                                                <input type="checkbox" class="form-check" name="is_partnership" value="1" {{ Auth::user()->is_partnership ? 'checked' : '' }}> Available to partnership?
                                            </a>
                                            
                                            
                                        </div>
                                        <div class="col-xs-1">

                                        </div>
                                    </div>

                                </div>

                            </div>  <!-- end row -->
                        </div>

                        <!-- end dasar -->

                        <!-- start kontak -->

                        <div class="tab-pane " id="kontak" aria-expanded="true">
                            <span class="media-meta pull-right text-primary">
                                <i class="fa fa-edit fa-3x"></i>
                            </span>
                            <h3 class="m-2">Informasi Kontak</h3>
                            <p class="text-muted m-2">Lengkapi profile anda agar orang lain bisa mengenali anda.</p>
                            <hr>

                            <div class="row m-2">
                                <div class="col-sm-6">
                                    <div class="form-group border p-2 border-secondary mb-3">
                                        <h6>Email Address</h6>
                                        <input type="email" name="" value="{{ Auth::user()->email }}"
                                               class="form-control border-0"
                                               placeholder="ex. some@example.com" disabled>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class=" form-group border p-2 border-secondary mb-3">
                                        <h6>Website</h6>
                                        <input type="text" name="website" value="{{ Auth::user()->website }}"
                                               class="form-control border-0"
                                               placeholder="ex.www.example.com">
                                    </div>
                                </div>
                            </div> <!-- end row -->

                            <div class="row m-2">
                                <div class="col-sm-6">
                                    <div class="form-group border p-2 border-secondary mb-3">
                                        <h6>Phone Number</h6>
                                        <input type="text" name="phone" value="{{ Auth::user()->phone }}"
                                               class="form-control border-0"
                                               placeholder="ex. +021xxxx">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group border p-2 border-secondary mb-3">
                                        <h6>Telephone Number</h6>
                                        <input type="text" name="mobile" value="{{ Auth::user()->mobile }}"
                                               class="form-control border-0"
                                               placeholder="ex. +62821xxxx">
                                    </div>
                                </div>
                            </div> <!-- end row -->

                            <div class="row m-2">
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-group mb-3">
                                <label>Kota</label>
                                <input class="form-control border-secondary" id="exampleTextarea" rows="3"
                                          name="city"
                                          placeholder="Kota" value="{{ Auth::user()->city }}">
                                    </div>
                                </div>
                            </div> <!-- end row -->

                            <div class="row m-2">
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-group mb-3">
                                <label>Notes</label>
                                <textarea class="form-control border-secondary" id="exampleTextarea" rows="3"
                                          name="notes"
                                          placeholder="Tulis notes kamu disini">{{ Auth::user()->notes }}</textarea>
                                    </div>
                                </div>
                            </div> <!-- end row -->

                            <div class="row m-2">
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-group mb-3">
                                      <label>Alamat Lengkap</label>
                                <textarea class="form-control border-secondary" id="exampleTextarea" rows="3"
                                          name="address"
                                          placeholder="Jl. Angrek Barat No.66 Jakarta Pusat, Indonesia">{{ Auth::user()->address }}</textarea>
                                    </div>
                                </div>
                            </div> <!-- end row -->

                            <div class="row m-2">
                                <div class="col-sm-6">
                                    <div class=" form-group border p-2 border-secondary mb-3">
                                        <h6>Pin BB</h6>
                                        <input type="text" name="pin_bb" value="{{ Auth::user()->pin_bb }}"
                                               class="form-control border-0"
                                               placeholder="ex. 42JIUI35">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group border p-2 border-secondary mb-3">
                                        <h6>Kode Pos</h6>
                                        <input type="text" name="zipcode" value="{{ Auth::user()->zipcode }}"
                                               class="form-control border-0"
                                               placeholder="ex. 2131xxxx">
                                    </div>
                                </div>
                            </div> <!-- end row -->
                        </div><!-- end tab-pane -->

                        <!-- end kontak -->

                        <!-- start pribadi -->

                        <div class="tab-pane " id="pribadi" aria-expanded="true">
                            <span class="media-meta pull-right text-primary">
                                <i class="fa fa-edit fa-3x"></i>
                            </span>
                            <h3 class="m-2">Informasi Pribadi</h3>
                            <p class="text-muted m-2">Lengkapi profile anda agar orang lain bisa mengenali anda.</p>
                            <hr>

                            <div class="row">
                                <div class="col-sm-4">
                                    Tentang Saya
                                </div>
                                <div class="col-sm-8">
                                    <div class="border p-2">
                                        <small>Tulis sesuatu tentang anda</small>
                                        <textarea class="form-control border-secondary border-0" id="exampleTextarea"
                                                  name="about"
                                                  rows="3">{{ Auth::user()->about }}</textarea>
                                    </div>
                                </div>
                            </div>  <!-- end row -->

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    Skill
                                </div>
                                <div class="col-sm-8">
                                    <div class="border p-2">
                                        <small>Masukkan 3 keahlian Anda</small>
                                        <textarea class="form-control border-secondary border-0" id="exampleTextarea"
                                                  name="skill"
                                                  rows="3">{{ Auth::user()->skill }}</textarea>
                                    </div>
                                </div>
                            </div>  <!-- end row -->

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    Hobi
                                </div>
                                <div class="col-sm-8">
                                    <div class="border p-2">
                                        <small>Masukkan 3 Hobi Anda</small>
                                        <textarea class="form-control border-secondary border-0" id="exampleTextarea"
                                                  name="hobby"
                                                  rows="3">{{ Auth::user()->hobby }}</textarea>
                                    </div>
                                </div>
                            </div>  <!-- end row -->

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    Alasan masuk Babastudio
                                </div>
                                <div class="col-sm-8">
                                    <div class="border p-2">
                                        <small>Berikan alasan Anda</small>
                                        <textarea class="form-control border-secondary border-0" id="exampleTextarea"
                                                  rows="3" name="reason">{{ Auth::user()->reason }}</textarea>
                                    </div>
                                </div>
                            </div>  <!-- end row -->

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    Harapan setelah masuk Babastudio
                                </div>
                                <div class="col-sm-8">
                                    <div class="border p-2">
                                        <small>Berikan alasan Anda</small>
                                        <textarea class="form-control border-secondary border-0" id="exampleTextarea"
                                                  name="hope"
                                                  rows="3">{{ Auth::user()->hope }}</textarea>
                                    </div>
                                </div>
                            </div>  <!-- end row -->
                        </div>

                        <!-- end pribadi -->

                        <!-- start sosialmedia -->

                        <div class="tab-pane " id="mediasosial" aria-expanded="true">
                            <span class="media-meta pull-right text-primary">
                                <i class="fa fa-edit fa-3x"></i>
                            </span>
                            <h3 class="m-2">Media Sosial</h3>
                            <p class="text-muted m-2">Lengkapi profile anda agar orang lain bisa mengenali anda.</p>
                            <hr>

                            <div class="container">
                                <p class="text-primary mb-0">Facebook</p>
                                <div class="row m-2">
                                    <div class="col-sm-2 border border-primary">
                                        <i class="text-primary fa fa-facebook-f fa-2x mt-3 ml-2"></i>

                                    </div>
                                    <div class="col-sm-10 border border-primary">
                                        <small>Your facebook account</small>
                                        <input type="text" name="fb" value="{{ Auth::user()->fb }}"
                                               class="form-control border-0"
                                               placeholder="ex. www.facebook/some example">
                                    </div>
                                </div><!-- end row -->

                                <p class="text-primary mb-0 mt-3">Twitter</p>
                                <div class="row m-2">
                                    <div class="col-sm-2 border border-secondary">
                                        <i class="text-primary fa fa-twitter fa-2x mt-3 ml-2"></i>

                                    </div>
                                    <div class="col-sm-10 border border-secondary">
                                        <small>Your Twitter account</small>
                                        <input type="text" name="tw" value="{{ Auth::user()->tw }}"
                                               class="form-control border-0"
                                               placeholder="ex. www.twitter.com/some example">
                                    </div>
                                </div><!-- end row -->

                                <p class="text-primary mb-0 mt-3">Instagram</p>
                                <div class="row m-2">
                                    <div class="col-sm-2 border border-secondary">
                                        <i class="text-primary fa fa-instagram fa-2x mt-3 ml-2"></i>

                                    </div>
                                    <div class="col-sm-10 border border-secondary">
                                        <small>Your Instagram account</small>
                                        <input type="text" name="ig" value="{{ Auth::user()->ig }}"
                                               class="form-control border-0"
                                               placeholder="ex. www.instagram.com/some example">
                                    </div>
                                </div><!-- end row -->

                            </div>  <!-- end container -->
                        </div>

                        <!-- end sosialmedia -->

                        <!-- start ubahsandi -->

                        <div class="tab-pane " id="ubahsandi" aria-expanded="true">
                            <span class="media-meta pull-right text-primary">
                                <i class="fa fa-edit fa-3x"></i>
                            </span>
                            <h3 class="m-2">Ubah Kata Sandi</h3>
                            <p class="text-muted m-2">Lengkapi profile anda agar orang lain bisa mengenali anda.</p>
                            <hr>

                            <div class="container">
                                <p class="text-primary mb-0">Password lama</p>
                                <div class="row m-2">
                                    <div class="col-sm-2 border border-success">
                                        <i class="text-success fa fa-lock fa-2x mt-3 ml-2"></i>

                                    </div>
                                    <div class="col-sm-10 border border-success">
                                        <small>Kata Sandi lama</small>
                                        <input type="password" name="old_password" class="form-control border-0"
                                               placeholder="**************">
                                    </div>
                                </div><!-- end row -->

                                <p class="text-primary mb-0 mt-3">Password baru</p>
                                <div class="row m-2">
                                    <div class="col-sm-2 border border-secondary">
                                        <i class="text-secondary fa fa-unlock fa-2x mt-3 ml-2"></i>

                                    </div>
                                    <div class="col-sm-10 border border-secondary">
                                        <small>Buat Kata sandi baru</small>
                                        <input type="password" name="password" class="form-control border-0"
                                               placeholder="**************">
                                    </div>
                                </div><!-- end row -->

                                <p class="text-primary mb-0 mt-3">Ulangi Password baru</p>
                                <div class="row m-2">
                                    <div class="col-sm-2 border border-secondary">
                                        <i class="text-secondary fa fa-unlock-alt fa-2x mt-3 ml-2"></i>

                                    </div>
                                    <div class="col-sm-10 border border-secondary">
                                        <small>Ulangi Kata sandi baru</small>
                                        <input type="password" name="password_confirmation" class="form-control border-0"
                                               placeholder="**************">
                                    </div>
                                </div><!-- end row -->

                            </div>  <!-- end container -->
                        </div>
                        <div class="row m-2">
                            <div class="col">
                                <button class="btn btn-info float-right m-5">
                                    <i class="fa fa-save"></i>
                                    Simpan
                                </button>
                            </div>
                        </div><!-- end row -->

                        <!-- end ubahsandi -->
                    </div>
                </form>
            </div>
        </div>
        <!-- end card -->
    </div>

@endsection

@section('script')
    <script>

        $(function () {
            // On Image Change
            $('#inputPhotoProfile').change(function() {
                console.log('ccccc');
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    
                    reader.onload = function (e) {
                        $('#photoProfile').attr('src', e.target.result);
                    }
                    
                    reader.readAsDataURL(this.files[0]);

                    $('#saveCourseImage').show();
                }
            });

            $('form#imageForm').submit(function (e) {
                e.preventDefault();

                $('#progress').show();

                var formData = new FormData(this);

                console.log(formData.get('photo'));

                $.ajax({
                    xhr: function () {
                        var xhr = new window.XMLHttpRequest();

                        // Upload progress
                        xhr.upload.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                //Do something with upload progress
                                console.log('percentComplete = ' + percentComplete);
                                $('#uploadprogress').width(percentComplete + '%');
                            }
                        }, false);

                        // Download progress
                        xhr.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                // Do something with download progress
                                console.log(percentComplete);
                            }
                        }, false);

                        return xhr;
                    },
                    url: '/me/update-image/social',
                    type: 'PUT',
                    data: formData,
                    success: function (data) {
                        $('#uploadprogress').width('100%');
                        $('#progress').hide();

                        swal("Berhasil !", "Berhasil mengganti gambar", "success");

                    },
                    error: function (error) {
                        $('#progress').hide();

                        swal("Gagal !", "Gagal mengganti gambar", "error");
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });

            });

        });

    </script>
@endsection
