@if(Auth::user()->photo === null)
    <img id="photoProfile" src="/images/default.png" alt="profile-photo"
         class="rounded mb-3 img-fluid">
@else
    <img id="photoProfile" src="/images/photo/{{ Auth::user()->photo }}" alt="profile-photo"
         class="rounded mb-3 img-fluid">
@endif