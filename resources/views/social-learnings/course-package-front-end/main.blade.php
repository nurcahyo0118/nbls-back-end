<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css"
          integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- App css -->
    <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/css/icons.css" rel="stylesheet" type="text/css"/>
    <link href="/css/style.css" rel="stylesheet" type="text/css"/>
    <title>Babastudio</title>
</head>
<body>
<!-- Navigation Bar-->
<header id="topnav">
    <div class="topbar-main bg-dark">
        <div class="container">

            <nav class="navbar navbar-expand-md navbar-dark clearfix">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                        data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
                        aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <a class="navbar-brand" href="/social-learning-posts">
                    <img src="/images/nbls-logo.png" alt="" height="30" class="logo-lg">
                    <img src="/images/nbls-logo.png" alt="" height="34" class="logo-sm">
                </a>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                            <a class="nav-link" href="https://www.babastudio.com/kursus-online/how-it-works">Cara
                                                                                                             Penggunaan
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item dropdown active">
                            <a class="nav-link dropdown-toggle" href="/" id="navbarDropdownMenuLink"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Paket Kursus
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="/social-learning-course">Semua Kursus</a>
                            </div>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="https://www.babastudio.com/kursus-online/how-it-works">Blog
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="https://www.babastudio.com/kursus-online/how-it-works">Galeri
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="https://www.babastudio.com/kursus-online/how-it-works">Kontak
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="https://www.babastudio.com/kursus-online/how-it-works">Video
                                                                                                             Tutorial
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                    </ul>

                </div>
                <ul class="navbar-nav">
                    <li class="nav-item active float-right mx-2">
                        <a href="#" class="text-muted">
                            <small>Free Online Learning?</small>
                        </a>
                    </li>
                    <li class="nav-item active float-right">
                        <a href="/login">
                            <button type="button" class="btn text-white" style="background-color: #C87F1F;"
                                    name="button">Login / Register
                            </button>
                        </a>
                    </li>

                    @include('layouts.partials-social-learning.cart-without-login')

                </ul>
            </nav>


        </div>
        <!-- end container -->
    </div>
    <!-- end topbar-main -->
</header>
<!-- End Navigation Bar -->

<div class="row">

    <!-- start Carousel -->
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" style="height: 500px;" width="100%" src="/images/big/banner.jpg"
                     alt="Second slide">

            </div>
            <div class="carousel-item">
                <img class="d-block w-100" style="height: 500px;" width="100%" src="/images/big/banner.jpg"
                     alt="Second slide">

            </div>
            <div class="carousel-item">
                <img class="d-block w-100" style="height: 500px;" src="/images/big/banner.jpg" alt="Third slide">

            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!-- end Caraousel -->
</div>

@yield('content')


<!-- Footer -->
<footer class="footer bg-dark mt-50">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h5 class="text-white">NBLS</h5>
                <img src="/images/nbls-logo.png" class="img-responsive" alt="">
                <br>
                <p class="mt-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                <span class="fa fa-phone mt-1 text-muted"> 021 121392 2424</span>
                <br>
                <span class="fa fa-envelope mt-1 text-muted"> admin@babastudio.com</span>

            </div>

            <div class="col-md-3">
                <h5 class="text-white">LINK MAP</h5>
                <ul>
                    <li class="mt-2">
                        <a href="/">Beranda</a>
                    </li>
                    <li class="mt-2">
                        <a href="/">Tentang Kami</a>
                    </li>
                    <li class="mt-2">
                        <a href="/">Cara Pengunaan</a>
                    </li>
                    <li class="mt-2">
                        <a href="/">Kursus</a>
                    </li>
                    <li class="mt-2">
                        <a href="/">Persyaratan Pengguna</a>
                    </li>
                    <li class="mt-2">
                        <a href="/">Kebijakan Privasi</a>
                    </li>
                    <li class="mt-2">
                        <a href="/">Blog</a>
                    </li>
                </ul>
            </div>

            <div class="col-md-3">
                <h5 class="text-white">GET NEWSLETTER</h5>
                <p>Above the law sunny days sweeping the clouds away lateful trip where.</p>
                <!-- <input type="text" name="" class="form-control" value="" placeholder="Your email address"><button class="btn btn-primary"><span class="fa fa-send"></span></button> -->
                <div class="input-group">

                    <input type="text" class="form-control">
                    <span class="input-group-btn">
                        <button class="btn btn-default form-control" type="button">
                            <i class="fa fa-send"></i>
                        </button>
                    </span>
                </div>

                <p>
                    Your information are safe with us.
                </p>
                <button type="button" class="btn btn-facebook waves-effect waves-light">
                    <i class="fa fa-facebook"></i>
                </button>

                <button type="button" class="btn btn-twitter waves-effect waves-light">
                    <i class="fa fa-twitter"></i>
                </button>

                <button type="button" class="btn btn-danger waves-effect waves-light">
                    <i class="fa fa-google-plus"></i>
                </button>


            </div>
            <div class="col-md-3">
                <h5 class="text-white">OUR INSTRUCTOR</h5>
                @php

                    $users = App\User::where('role_id', '4')->inRandomOrder()->take(9)->get();

                @endphp

                @foreach($users as $user)
                    @if( $user->photo === null)
                        <img src="/images/users/avatar-1.jpg" alt="user" class="img-responsive m-1" width="70px"
                             height="70">
                    @else
                        <img src="/images/photo/{{ $user->photo }}" alt="user" class="img-responsive m-1" width="70px"
                             height="70">
                    @endif
                @endforeach
            </div>
        </div>
    </div>
    <!-- End Of container -->

    <hr>
    <div class="container">
        <div class="row">
            <p>Copyright 2019. NBLS Hybirrd Learning All Right Reserverd.</p>
        </div>
    </div>
</footer>
<!-- End Footer -->


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<!-- jQuery  -->
<script src="/js/jquery.min.js"></script>
<script src="/js/popper.min.js"></script>
<!-- Popper for Bootstrap -->
<script src="/js/bootstrap.min.js"></script>
<script src="/js/waves.js"></script>
<script src="/js/jquery.slimscroll.js"></script>
<script src="/js/jquery.scrollTo.min.js"></script>

<!-- App js -->
<script src="/js/jquery.core.js"></script>
<script src="/js/jquery.app.js"></script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

@yield('script')

</body>
</html>
    