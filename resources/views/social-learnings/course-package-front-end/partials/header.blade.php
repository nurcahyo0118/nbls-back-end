<!-- topbar -->
<div class="container-fluid bg-blue top-bar f-white pl-5 pb-2">
    <div class="row">
        <div class="col-md-8 col-sm-12">
            <div class="ml-4">
                <i class="fa fa-phone ml-5 float-sm-left mt-1"></i>
                + Call Us : 021 - 5366 4008
            </div>
        </div>
        <div class="col-md-4 col-sm-12">
            <div class="ml-2 tentang">
                <a href="#" class="f-white mr-2">Tentang Kami</a>
                <span>|
                    <span>
                        <i class="fa fa-user ml-2"></i>
                        <a href="/login" class="mr-1 link">Login</a>
                        or
                        <a href="/register" class="mr-1 link">Register</a>

                        <a href="#!" onclick="loadCartData(JSON.parse(window.localStorage.getItem('carts')))"
                           data-toggle="modal" data-target="#exampleModalCenter">
                            <span>
                                <i class="fa fa-shopping-cart"></i>
                            </span>
                        </a>

                @include('social-learnings.course-front-end.modal.carts')

            </div>
        </div>
    </div>
</div>
<!-- end topbar -->
<!-- navbar -->
<nav class="navbar  navbar-expand-lg navbar-dark bg-black navbar-bootbites" data-toggle="sticky-onscroll">
    <div class="container">
        <a class="navbar-brand" href="/"><img src="/baba-custom/images/babastudio.png" width="200em" class="img-fluid">
        </a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/">BERANDA</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/social-learning-course-front-end">KURSUS ONLINE</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="http://techfor.id/baba-custom/how-it-works.php">CARA PENGGUNAAN</a>
                </li>
                {{-- <li class="nav-item">
                  <a class="nav-link" href="#">KUPON DISKON</a>
                </li> --}}
                <li class="nav-item">
                    <a class="nav-link" href="/register">MULAI BELAJAR</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- end navbar -->
