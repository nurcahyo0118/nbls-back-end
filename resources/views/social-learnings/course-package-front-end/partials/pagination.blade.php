<div id="coursesPagination" class="offset-5">
    <nav aria-label="Page navigation example">
        <ul class="pagination">
            <li class="page-item">
                <a class="page-link" href="#" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">Previous</span>
                </a>
            </li>

            @foreach($courses->data as $page)
                @if($filter === 'ALL')
                    <li class="page-item {{ $page != $currentPage ? '' : 'active' }}">
                        <a class="page-link" href="#!" onclick="onSelectFilter('all')">{{ $page }}</a>
                    </li>
                @elseif($filter === 'CATEGORY')
                    <li class="page-item {{ $page != $currentPage ? '' : 'active' }}">
                        <a class="page-link" href="#!" onclick="onSearchCourse('category')">{{ $page }}</a>
                    </li>
                @elseif($filter === 'PRICE')
                    <li class="page-item {{ $page != $currentPage ? '' : 'active' }}">
                        <a class="page-link" href="#!" onclick="onSearchCourse('price')">{{ $page }}</a>
                    </li>
                @elseif($filter === 'POPULAR')
                    <li class="page-item {{ $page != $currentPage ? '' : 'active' }}">
                        <a class="page-link" href="#!" onclick="onSearchCourse('popular')">{{ $page }}</a>
                    </li>
                @elseif($filter === 'SEARCH')
                    <li class="page-item {{ $page != $currentPage ? '' : 'active' }}">
                        <a class="page-link" href="#!" onclick="onSearchCourse('popular')">{{ $page }}</a>
                    </li>
                @endif
            @endforeach

            <li class="page-item">
                <a class="page-link" href="#" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Next</span>
                </a>
            </li>
        </ul>
    </nav>
</div>