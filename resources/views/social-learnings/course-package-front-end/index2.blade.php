@extends('social-learnings.course-front-end.main')

@section('content')
    <div class="container footer-space">

        <h3>Kursus</h3>

        <!-- form input -->
        <div class="row">
            <div class="col-md-1">
                Filter by
            </div>

            <div class="col-md-5">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <select class="form-control" onchange="onSelectFilter(value);">
                                <option>All</option>
                                <option value="category">Kategory</option>
                                <option value="price">Harga</option>
                                <option value="popular">Populer</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <select id="selectChildFilter" class="form-control" onchange="onSelectCategory(value);">
                                <option>All</option>
                            </select>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-md-6">
                <form id="inputSearchForm" onsubmit="onSearchCourse(); return false;" method="POST">
                    <div class="input-group">

                        {{ csrf_field() }}

                        <input id="inputSearch" type="text" class="form-control" placeholder="Search" required>

                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </form>
            </div>

            <div class="container">
                <div id="allCourseList" class="text-center">
                    <i class="fa fa-spinner fa-spin fa-5x"></i>
                </div>
            </div>

        </div>
        <!-- end form input -->

        <!-- Pagination -->
        <div class="m-t-15">

            <nav>
                <!-- <ul class="pagination justify-content-center">
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </li>
                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">4</a></li>
                    <li class="page-item"><a class="page-link" href="#">5</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Next</span>
                        </a>
                    </li>
                </ul> -->
            </nav>
        </div>
        <!-- end Pagination -->
    </div>
@endsection

@section('script')
    <script>
        $(function () {

            $.ajax({
                type: 'GET',
                url: '/loads/social-learning-course-front-end/all',
                success: function (response) {
                    $('#allCourseList').replaceWith(response);
                },
                error: function (error) {
                    $('#allCourseList').replaceWith('<div id="allCourseList" class="text-center">Course Not Found.</div>');
                }
            });

        });
    </script>
@endsection
