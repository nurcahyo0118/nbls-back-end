<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>NBLS - Babastudio</title>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link href="/baba-custom/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/baba-custom/css/custom.css" rel="stylesheet">
    <link href="/baba-custom/lib/animate/animate.min.css" rel="stylesheet">
    <link href="/baba-custom/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="/baba-custom/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="/baba-custom/lib/lightbox/css/lightbox.min.css" rel="stylesheet">
    <link href="/baba-custom/css/style.css" rel="stylesheet">
</head>
<body>

@include('layouts.partials-baba-custom.header')

<div class="row">

    <div class="container footer-space">
        <h6 class="text-muted">
            <span class="fa fa-home"></span>
            Home
            <span class="fa fa-long-arrow-right"></span>
            Paket Kursus
        </h6>

        <h4 class="text-center mt-5">
            <strong>{{ $course_package->title }}</strong>
        </h4>

        <hr width="35%">

        <div class="row">
            <div class="col-md-8">
                <div class="row mt-5 mb-5">
                    <div class="col-md-12">
                        <img src="/course-package/images/{{ $course_package->image }}" class="img-fluid">
                    </div>

                    <div class="col-md-12">
                        <div class="card h-100" style="margin-top: 40px">

                            <div class="card-body">
                                <h3>
                                    Deskripsi
                                </h3>
                                {!! $course_package->description !!}
                            </div>
                        </div>
                    </div>

                    {{--  <div class="col-md-6 mt-5 pl-5">
                        <div class="row">

                            <div class="col-md-12 p-2">
                                <span class="label label-success p-2">
                                    @if($course_package->price != 0)
                                        Rp. {{ number_format($course_package->price, 0, ',', '.') }}
                                    @else
                                        Gratis
                                    @endif
                                </span>

                            </div>

                            <br>
                            <br>

                            @include('social-learnings.course-package-front-end.join')


                        </div>

                    </div>  --}}
                </div>
            </div>

            <div class="col-md-4">
                <div class="row mt-5 mb-5">
                    <div class="col-md-12">
                        <div class="card h-100">

                            <div class="card-body">
                                <div class="row mt-5 ">
                                    <div class="col-md-6 text-center">
                                        <div class="col-md-12">
                                            <img src="/images/icon/sertifikat.png" alt="">
                                        </div>
                                        <div class="col-md-12 mt-2">
                                            <strong class="mt-2">SERTIFIKAT RESMI</strong>
                                        </div>
                                    </div>

                                    <div class="col-md-6 text-center">
                                        <div class="col-md-12">
                                            <img src="/images/icon/ternama.png" alt="">
                                        </div>
                                        <div class="col-md-12 mt-2">
                                            <strong class="mt-2">PRAKTIS TERNAMA</strong>
                                        </div>
                                    </div>

                                    <div class="col-md-6 text-center">
                                        <div class="col-md-12">
                                            <img src="/images/icon/pengajar.png" alt="">
                                        </div>
                                        <div class="col-md-12 mt-2">
                                            <strong class="mt-2">INSTRUKTUR SELALU ONLINE</strong>
                                        </div>
                                    </div>

                                    <div class="col-md-6 text-center">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <img src="/images/icon/kampus.png" alt="">
                                            </div>
                                            <div class="col-md-12 mt-2">
                                                <strong class="mt-2">KOMUNITAS ESKLUSIF</strong>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                @if($course_package->price != 0)
                                    Rp. {{ number_format($course_package->price, 0, ',', '.') }}
                                @else
                                    Gratis
                                @endif
                            </div>

                            <div class="card-body">
                                <button type="button" onclick="addPackageToCart({{ $course_package->id }})"
                                        class="btn btn-primary col-md-12">Beli
                                </button>

                                <hr>

                                <p>Metode Pembayaran bisa dilakukan dengan cara :</p>
                                <p>1. Transfer ATM</p>
                                <p>2. Mobile Banking</p>
                                <p>3. Internet Banking</p>
                                <p>4. SMS Banking</p>
                            </div>

                        </div>
                    </div>

                    {{--  <div class="col-md-6 mt-5 pl-5">
                        <div class="row">

                            <div class="col-md-12 p-2">
                                <span class="label label-success p-2">
                                    @if($course_package->price != 0)
                                        Rp. {{ number_format($course_package->price, 0, ',', '.') }}
                                    @else
                                        Gratis
                                    @endif
                                </span>

                            </div>

                            <br>
                            <br>

                            @include('social-learnings.course-package-front-end.join')


                        </div>

                    </div>  --}}
                </div>
            </div>

            <div class="col-md-12">
                <div class="card h-100" style="margin-top: 40px">

                    <div class="card-body">
                        <h3>
                            Kurikulum
                        </h3>
                        <div class="accordion" id="accordionExample">
                            @foreach($course_package->packages as $course)
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link" type="button" data-toggle="collapse"
                                                    data-target="#collapseOne" aria-expanded="true"
                                                    aria-controls="collapseOne">
                                                {{ $course->title }}
                                            </button>
                                        </h5>
                                    </div>

                                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne"
                                         data-parent="#accordionExample">
                                        <div class="card-body">

                                            @foreach($course->sections as $section)
                                                <div class="card">
                                                    <div class="card-body">
                                                        <h5>{{ $section->title }}</h5>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <hr>

                        <h3>
                            Ulasan
                        </h3>

                        @foreach($course_package->packages as $course)

                            @foreach($course->reviews as $review)
                                @include('social-learnings.course.child.review-child')
                            @endforeach

                        @endforeach

                    </div>

                </div>
            </div>
        </div>


        {{--  <h4 class="text-center"><strong>Kursus Apa Yang Anda Dapatkan?</strong></h4>
        <hr width="35%">

        <div class="row mt-5 ">

            @foreach($course_package->packages as $course)
                <div class="col-lg-4 mb-4">
                    <div class="card h-100">
                        <a href="#">
                            @if($course->image === null)
                                <img class="card-img-top" src="/images/order/web_design.jpg" style="height: 200px"
                                        alt="Card image cap">
                            @else
                                <img class="card-img-top" src="/xxcourses/images/{{ $course->image }}" style="height: 200px"
                                        alt="Card image cap">
                            @endif
                        </a>
                        <div class="card-body">
                            <small class="text-muted"><i class="fa fa-clock-o" aria-hidden="true"></i> {{ $course->updated_at->diffForHumans() }}</small>
                            <h3 class="card-title">{{ $course->title }}</h3>
                            <div class="row">
                                <div class="col-md-12">
                                    <small class="text-muted"><i class="fa fa-users" aria-hidden="true"></i> {{ $course->users->count() }} Siswa</small>
                                    <h4 class="text-warning float-right">
                                        @if($course->price != 0)
                                            IDR {{ number_format($course->price, 0, ',', '.') }}
                                        @else
                                            Gratis
                                        @endif
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                        <a href="{{ route('social-learning-course-front-end.show', [$_SERVER['HTTP_HOST'], $course->id]) }}">View Details <i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        <br>
        <br>
        <br>  --}}

        <br>
        <br>
        <br>
        <br>

    </div>
</div>

@include('layouts.partials-baba-custom.footer')

<script>
    $(function () {
        {{--  $("form#rating").submit(function(e) {
            e.preventDefault();
            var formData = new FormData(this);

            $.ajax({
                url: '/course/rate',
                type: 'POST',
                data: formData,
                success: function (data) {
                    swal("Berhasil !", "Berhasil mengirim ulasan", "success");
                },
                cache: false,
                contentType: false,
                processData: false
            });

        });  --}}

    });
</script>
<script>
    function onTakeCourse(courseId) {
        var formData = new FormData();
        formData.append('_token', '{{ csrf_token() }}')
        $.ajax({
            url: '/social-learning-my-course/' + courseId + '/add-to-cart-without-login',
            type: 'POST',
            data: formData,
            success: function (data) {
                console.log(data);
                $('#join' + courseId).replaceWith(data);
                swal("Berhasil !", "Berhasil menambahkan kursus kedalam keranjang", "success");
            },
            error: function (error) {
                swal("Gagal !", "Gagal bergabung", "error");
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }

    function onTakeFreeCourse(courseId) {

        $.ajax({
            url: '/courses/' + courseId + '/join',
            type: 'POST',
            data: null,
            success: function (data) {
                console.log(data);
                $('#join' + courseId).replaceWith(data);
                swal("Berhasil !", "Berhasil bergabung", "success");
            },
            error: function (error) {
                swal("Gagal !", "Gagal bergabung", "error");
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }

    function onLeaveCourse(courseId) {
        $.ajax({
            url: '/courses/' + courseId + '/leave',
            type: 'POST',
            data: null,
            success: function (data) {
                console.log(data);
                $('#join' + courseId).replaceWith(data);
                swal("Berhasil !", "Berhasil meninggalkan kursus", "success");
            },
            error: function (error) {
                swal("Gagal !", "Gagal meninggalkan kursus", "error");
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }

    function onDeleteReview(id) {
        swal({
            title: "Apa kamu yakin?",
            text: "Data yang telah di hapus tidak dapat dikembalikan",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {

                $.ajax({
                    url: '/course/rate',
                    type: 'DELETE',
                    data: null,
                    success: function (data) {
                        window.location.reload();
                    },
                    error: function (error) {
                        swal("Gagal !", "Gagal meninggalkan kursus", "error");
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });

            }
        });
    }

    function addPackageToCart(course_package_id) {
        var carts = [];

        var carts_from_local = JSON.parse(window.localStorage.getItem('package_carts'));

        if (carts_from_local === null) {
            carts.push(course_package_id);
            window.localStorage.setItem('package_carts', JSON.stringify(carts));

            swal("Berhasil memasukkan paket kursus ke dalam keranjang");
        }
        else {
            if (carts_from_local.indexOf(course_package_id) < 0) {
                carts_from_local.push(course_package_id);
                window.localStorage.setItem('package_carts', JSON.stringify(carts_from_local));
            }

            swal("Berhasil memasukkan paket kursus ke dalam keranjang");

        }
    }

    function loadCartPackageData(carts) {
        var url = '/loads/frontend/package-carts?';

        if (carts !== null && carts.length !== 0) {
            for (i = 0; i < carts.length; i++) {
                url = url + '&carts[]=' + carts[i];
                console.log(url);
            }

            $.ajax({
                type: 'GET',
                url: url,
                success: function (response) {
                    $('#cartsList').replaceWith(response.view);
                },
                error: function (error) {
                    console.log('ddddd');
                    $('#cartsList').replaceWith('<div id="cartsList" class="col-sm-12 col-md-12 col-md-12 text-center" style="color: #424242">Kosong !</div>');
                }
            });
        }
        else {
            $('#cartsList').replaceWith('<div id="cartsList" class="col-sm-12 col-md-12 col-md-12 text-center" style="color: #424242">Kosong !</div>');
        }

    }

    function addToCart(course_id) {
        var carts = [];

        var carts_from_local = JSON.parse(window.localStorage.getItem('carts'));

        if (carts_from_local === null) {
            carts.push(course_id);
            window.localStorage.setItem('carts', JSON.stringify(carts));
        }
        else {
            if (carts_from_local.indexOf(course_id) < 0) {
                carts_from_local.push(course_id);
                window.localStorage.setItem('carts', JSON.stringify(carts_from_local));
            }

        }
    }

    function removeFromCart(course_id) {
        var carts_from_local = JSON.parse(window.localStorage.getItem('carts'));
        var index = carts_from_local.indexOf(course_id);
        if (carts_from_local !== null && index > -1) {
            carts_from_local.splice(index, 1);
            window.localStorage.setItem('carts', JSON.stringify(carts_from_local));

            // remove item cart
            $('#cartsItem' + course_id).remove();
            $('#cartsItemDivider' + course_id).remove();

            var carts_from_local_latest = JSON.parse(window.localStorage.getItem('carts'));
            if (carts_from_local_latest.length === 0) {
                $('#cartsList').replaceWith('<div id="cartsList" class="col-sm-12 col-md-12 col-md-12 text-center" style="color: #424242">Kosong !</div>');
            }
        }
    }

    function loadCartData(carts) {
        var url = '/loads/frontend/carts?';
        var packageUrl = '/loads/frontend/package-carts?';

        var packageCarts = JSON.parse(window.localStorage.getItem('package_carts'));

        if (carts !== null && carts.length !== 0) {
            for (i = 0; i < carts.length; i++) {
                url = url + '&carts[]=' + carts[i];
                console.log(url);
            }

            $.ajax({
                type: 'GET',
                url: url,
                success: function (response) {
                    $('#cartsList').replaceWith(response.view);
                },
                error: function (error) {
                    console.log('ddddd');
                    $('#cartsList').replaceWith('<div id="cartsList" class="col-sm-12 col-md-12 col-md-12 text-center" style="color: #424242">Kosong !</div>');
                }
            });
        }
        else {
            $('#cartsList').replaceWith('<div id="cartsList" class="col-sm-12 col-md-12 col-md-12 text-center" style="color: #424242">Kosong !</div>');
        }

        if (packageCarts !== null && packageCarts.length !== 0) {
            for (i = 0; i < packageCarts.length; i++) {
                packageUrl = packageUrl + '&packageCarts[]=' + packageCarts[i];
                console.log(packageUrl);
            }

            $.ajax({
                type: 'GET',
                url: packageUrl,
                success: function (response) {
                    $('#packageCartsList').replaceWith(response.view);
                },
                error: function (error) {
                    console.log('ddddd');
                    {{--  $('#packageCartsList').replaceWith('<div id="cartsList" class="col-sm-12 col-md-12 col-md-12 text-center" style="color: #424242">Kosong !</div>');  --}}
                }
            });
        }
        else {
            $('#cartsList').replaceWith('<div id="cartsList" class="col-sm-12 col-md-12 col-md-12 text-center" style="color: #424242">Kosong !</div>');
        }

    }
</script>
