<div id="courseContent">
    <div id="allCourseList" class="row">

        @foreach($course_packages as $course_package)

            <div class="col-lg-4 mb-4">
                <div class="card h-100">

                    <a href="#">
                        @if($course_package->image === null)
                            <img class="card-img-top" src="/images/order/web_design.jpg" style="height: 200px"
                                 alt="Card image cap">
                        @else
                            <img class="card-img-top" src="/course-package/images/{{ $course_package->image }}"
                                 style="height: 200px"
                                 alt="Card image cap">
                        @endif
                    </a>
                    <div class="card-body">
                        <small class="text-muted">
                            <i class="fa fa-clock-o"
                               aria-hidden="true"></i> {{ $course_package->updated_at->diffForHumans() }}</small>
                        <h3 class="card-title">{{ $course_package->title }}</h3>
                        <div class="row">
                            <div class="col-md-12">
                                {{-- <small class="text-muted"><i class="fa fa-users" aria-hidden="true"></i> {{ $course_package->users->count() }} Siswa</small> --}}
                                <h4 class="text-warning float-right">
                                    @if($course_package->price != 0)
                                        IDR {{ number_format($course_package->price, 0, ',', '.') }}
                                    @else
                                        Gratis
                                    @endif
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="{{ route('course-package-frontend.show', $course_package->id) }}">View Details
                            <i class="fa fa-long-arrow-right"></i>
                        </a>
                    </div>
                </div>
            </div>

        @endforeach

    </div>

    {{-- @if($filter === 'popular')
        <div id="coursesPagination" class="offset-5">
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <li class="page-item">
                        <a class="page-link" href="#!" onclick="selectPage({{ $currentPage - 1 }}, '{{ $filter }}')" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </li>

                    @for($i = 0; $i < $course_packages->count(); $i++)

                        <li class="page-item {{ $currentPage === $i + 1 ? 'active' : '' }}">
                            <a class="page-link" href="#!" onclick="selectPage({{ $i + 1 }}, '{{ $filter }}')">{{ $i + 1 }} </a>
                        </li>

                    @endfor

                    <li class="page-item">
                        <a class="page-link" href="#!" onclick="selectPage({{ $currentPage + 1 }}, '{{ $filter }}')" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Next</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    @else
        <div id="coursesPagination" class="offset-5">
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <li class="page-item">
                        <a class="page-link" href="#!" onclick="selectPage({{ $course_packages->currentPage() - 1 }}, '{{ $filter }}')" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </li>

                    @for($i = 0; $i < $course_packages->count(); $i++)

                        <li class="page-item {{ $course_packages->currentPage() === $i + 1 ? 'active' : '' }}">
                            <a class="page-link" href="#!" onclick="selectPage({{ $i + 1 }}, '{{ $filter }}')">{{ $i + 1 }} </a>
                        </li>

                    @endfor

                    <li class="page-item">
                        <a class="page-link" href="#!" onclick="selectPage({{ $course_packages->currentPage() + 1 }}, '{{ $filter }}')" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Next</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    @endif --}}
</div>