<div id="packageCartsList" class="col-sm-12 col-md-12 col-md-12">

    @foreach($carts as $cart)
        <div id="cartsItem{{ $cart->id }}" class="media">

            @if($cart->image === null)
                <img class="mr-3"
                     src="/images/default2.jpg"
                     alt="Generic placeholder image"
                     height="64"
                     width="64">
            @else
                <img class="mr-3"
                     src="/course-package/images/{{ $cart->image }}"
                     alt="Generic placeholder image"
                     height="64"
                     width="64">
            @endif

            <div class="media-body" style="color: #757575">
                <h5 class="mt-0" style="color: #424242">{{ str_limit($cart->title, 20) }}</h5>
                {{ str_limit($cart->description, 30)  }}
            </div>

            <button type="button" class="close" onclick="removeFromCart({{ $cart->id }})">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
        <hr id="cartsItemDivider{{ $cart->id }}">
    @endforeach
</div>