@extends('layouts.frontend-no-right-left')

@section('content')
    <div class="row">

        @include('social-learnings.course.child.sidebar')

        <div class="col-md-8">

            {{-- <div class="wistia_embed wistia_async_{{ $exercise->wistia_media_hashed_id }}"
                style="height:430px;width:100%">&nbsp;
            </div>

            <br> --}}
            <div class="card">

                @if(DB::table('user_exercises')->where(['exercise_id' => $exercise->id, 'user_id' => Auth::id()])->first() != null)
                    <div class="card-body">
                        <h3 class="text-center">Anda sudah mengirim jawaban !</h3>
                    </div>
                @else
                    <div class="card-body">

                        <label>Soal Latihan : </label>
                        <br>
                        <p>{{ $exercise->title }}</p>
                        <a href="/exercise-file/{{ $exercise->file }}" class="btn btn-default"
                           download="/exercise-file/{{ $exercise->file }}">
                            <i class="fa fa-download"></i>
                            Unduh Soal Latihan
                        </a>

                        <hr>

                        <form
                                action="{{ route('social-learning-my-course.exercise.store', [$_SERVER['HTTP_HOST'], $exercise->id]) }}"
                                method="POST"
                                enctype="multipart/form-data">

                            <label>Unggah Jawaban : </label>
                            <br>

                            {{ csrf_field() }}

                            <input type="hidden" name="log" value="LOG_ANSWER_EXERCISE">

                            <input type="file" name="file">

                            <br>
                            <br>

                            <button type="submit" class="btn btn-primary pull-right">
                                <i class="fa fa-paper-plane"></i>
                                Kirim
                            </button>
                        </form>

                    </div>
                @endif

            </div>
        </div>
        <!-- end row -->
    </div>

@endsection

@section('script')
    <script src="//fast.wistia.com/embed/medias/{{ $exercise->wistia_media_hashed_id }}.jsonp" async></script>
    <script src="//fast.wistia.com/assets/external/E-v1.js" async></script>

    <script>
        $(function () {

            $("form#addquestion").submit(function (e) {
                e.preventDefault();
                var formData = new FormData(this);

                $.ajax({
                    url: '/social-learning-questions',
                    type: 'POST',
                    data: formData,
                    success: function (data) {
                        $('#questionList').prepend(data);
                        $("form#addquestion")[0].reset();
                        swal("Berhasil !", "Modul berhasil ditambahkan", "success");
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });

                return false;
            });

        });
    </script>

    <script>
        function onQuestionComment(questionId) {
            console.log('Try Submit');
            var content = $('#questioncomment' + questionId).val();

            var formData = new FormData();
            formData.append('_token', '{{ csrf_token() }}');
            formData.append('content', content);
            formData.append('log', 'LOG_COMMENTS');

            $.ajax({
                url: '/questions/' + questionId + '/comments',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#question' + questionId).replaceWith(data);
                },
                error: function (data) {
                    console.log(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onQuestionLike(questionId) {
            var formData = new FormData();
            formData.append('log', 'LOG_LIKE_POSTS');

            $.ajax({
                url: '/questions/' + questionId + '/likes',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#question' + questionId).replaceWith(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onQuestionDislike(questionId) {
            var formData = new FormData();
            formData.append('log', 'LOG_DISLIKE_POSTS');

            $.ajax({
                url: '/questions/' + questionId + '/dislikes',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#question' + questionId).replaceWith(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onQuestionLikeComment(commentId, questionId) {
            var formData = new FormData();
            formData.append('log', 'LOG_LIKE_COMMENTS');

            $.ajax({
                url: '/questions/comments/' + commentId + '/likes',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#question' + questionId).replaceWith(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onQuestionDislikeComment(commentId, questionId) {
            var formData = new FormData();
            formData.append('log', 'LOG_DISLIKE_COMMENTS');

            $.ajax({
                url: '/questions/comments/' + commentId + '/dislikes',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#question' + questionId).replaceWith(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }
    </script>
@endsection
