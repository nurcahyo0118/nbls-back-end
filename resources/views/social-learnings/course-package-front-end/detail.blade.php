@extends('layouts.frontend-no-right-left')

@section('content')

    <h6 class="text-muted mb-4 mt-4">
        <span class="fa fa-home"></span>
        Courses
        <span class="fa fa-long-arrow-right"></span>
        My Courses
    </h6>

    <div class="card-box">
        <div class="row">
            <div class="col-md-5">
                <h3 class="text-primary"><strong>Modul 1 : Pengenalan PHP</strong></h3>
                <h5 class="text-secondary"><strong>Belajar Konsep - Konsep Dasar PHP dan Fungsi PHP Pada Sebuah
                                                   Website</strong></h5>
            </div> <!-- end col md 6 -->
            <div class="col-md-7">
                <h5>
                    <i class="fa fa-chevron-circle-right text-secondary"></i> &nbsp;
                    <a href="#">1.1 Apa Itu PHP</a>
                </h5>
                <hr>
                <h5>
                    <i class="fa fa-chevron-circle-right text-secondary"></i> &nbsp;
                    <a href="#">1.2 Definisi dan Konsep PHP</a>
                </h5>
                <hr>
                <h5>
                    <i class="fa fa-chevron-circle-right text-secondary"></i> &nbsp;
                    <a href="#">1.3 Penerapan PHP Pada HTML</a>
                </h5>
                <hr>
                <h5>
                    <i class="fa fa-chevron-circle-right text-secondary"></i> &nbsp;
                    <a href="#">Latihan Essay PHP 1</a>
                </h5>
            </div> <!-- end col md 6 -->
        </div>
        <!-- end row -->
        <hr>
        <div class="row">
            <div class="col-md-5">
                <h3 class="text-primary"><strong>Modul 2 : Dasar - Dasar PHP</strong></h3>
                <h5 class="text-secondary"><strong>
                        Dasar Dasar Penggunaan Variable, Fungsi Cetak Echo & Print Jenis Jenis Penulisan PHP Pada HTML.
                        Dan Tipe Tipe Data Pada PHP
                    </strong></h5>
            </div> <!-- end col md 6 -->
            <div class="col-md-7">
                <h5>
                    <i class="fa fa-chevron-circle-right text-secondary"></i> &nbsp;
                    <a href="#">2.1 Variable & Fungsi Cetak Pada PHP</a>
                </h5>
                <hr>
                <h5>
                    <i class="fa fa-chevron-circle-right text-secondary"></i> &nbsp;
                    <a href="#">2.2 Tipe - Tipe Data Pada PHP</a>
                </h5>
                <hr>
                <h5>
                    <i class="fa fa-chevron-circle-right text-secondary"></i> &nbsp;
                    <a href="#">2.3 Fungsi Statement Dan Operators Pada PHP Part 1</a>
                </h5>
                <hr>
                <h5>
                    <i class="fa fa-chevron-circle-right text-secondary"></i> &nbsp;
                    <a href="#">2.4 Fungsi Starement Dan Operators Pada PHP Part 211</a>
                </h5>
                <hr>
                <h5>
                    <i class="fa fa-chevron-circle-right text-secondary"></i> &nbsp;
                    <a href="#">2.5 Form Handing Part 1</a>
                </h5>
                <hr>
                <h5>
                    <i class="fa fa-chevron-circle-right text-secondary"></i> &nbsp;
                    <a href="#">2.6 Form Handing Part 2</a>
                </h5>
                <hr>
                <h5>
                    <i class="fa fa-chevron-circle-right text-secondary"></i> &nbsp;
                    <a href="#">2.7 Form Handing Part 3</a>
                </h5>
                <hr>
                <h5>
                    <i class="fa fa-chevron-circle-right text-secondary"></i> &nbsp;
                    <a href="#">2.8 Form Validation Part 1 (Contoh : Program Kalkulator)</a>
                </h5>
                <hr>
                <h5>
                    <i class="fa fa-chevron-circle-right text-secondary"></i> &nbsp;
                    <a href="#">2.9 Form Validation Part 2 (Contoh : Form Pencarian Makanan)</a>
                </h5>
                <hr>
                <h5>
                    <i class="fa fa-chevron-circle-right text-secondary"></i> &nbsp;
                    <a href="#">2.10 Form Validation Part 3 (Contoh : Form Looping / Pengulangan)</a>
                </h5>
                <hr>
                <h5>
                    <i class="fa fa-chevron-circle-right text-secondary"></i> &nbsp;
                    <a href="#">Soal Exercise 1 - Dasar Dasar PHP</a>
                </h5>
                <hr>
                <h5>
                    <i class="fa fa-chevron-circle-right text-secondary"></i> &nbsp;
                    <a href="#">Latihan Essay PHP 2</a>
                </h5>
            </div> <!-- end col md 6 -->
        </div>
        <!-- end row -->
    </div>
    <!-- end card-box -->

@endsection
@section('script')
@endsection
