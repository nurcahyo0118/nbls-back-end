<div class="modal fade" id="quiz{{ $quiz->id }}" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Kuis</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form onsubmit="postAnswerQuiz({{ $quiz->id }}); return false;" method="POST">
                <div class="modal-body">

                    <div class="col-md-12">
                        <p>{{ $quiz->question }}</p>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group text-left">
                            <label>Jawaban : </label>
                            <br>
                            <label><input type="radio" name="answer{{ $quiz->id }}"
                                          value="option_a"> {{ $quiz->option_a }}</label>
                            <br>
                            <label><input type="radio" name="answer{{ $quiz->id }}"
                                          value="option_b"> {{ $quiz->option_b }}</label>
                            <br>
                            <label><input type="radio" name="answer{{ $quiz->id }}"
                                          value="option_c"> {{ $quiz->option_c }}</label>
                            <br>
                            <label><input type="radio" name="answer{{ $quiz->id }}"
                                          value="option_d"> {{ $quiz->option_d }}</label>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Kirim</button>
                </div>
            </form>

        </div>
    </div>
</div>