@extends('layouts.frontend')

@section('content')

    @php
        $user = Auth::user();
    @endphp

    <div class="col-md-8 col-lg-6 footer-space">

        <div class="card-box">

            <a href="/social-learning-setting" class="pull-right" style="margin-left:20px;">
                <span class="fa fa-gear fa-2x"></span>
            </a>

            <p></p>

            <div class="text-center m-t-40">
                @if($user->photo === null)
                    <img src="/images/default.png" alt="profile-photo"
                         class="img-thumbnail img-fluid" width="150px" height="150px">
                @else
                    <img src="/images/photo/{{ $user->photo }}" alt="profile-photo"
                         class="img-thumbnail img-fluid" width="150px" height="150px">
                @endif

                @php
                    $badge = $user->points->sum('point');
                @endphp

                @if($badge < 399)
                    <h5 class="text-primary">NEWBIE</h5>

                @elseif($badge >= 899 and $badge <= 2999)
                    <h5 class="text-primary">TRAINED</h5>

                @elseif($badge >= 2999 and $badge <= 6999)
                    <h5 class="text-primary">SUPER</h5>

                @elseif($badge > 6999 and $badge <= 10000)
                    <h5 class="text-primary">GEEK</h5>

                @elseif($badge > 10000 and $badge <= 18000)
                    <h5 class="text-primary">SUPER</h5>

                @elseif($badge >= 18000)
                    <h5 class="text-primary">MASTER</h5>

                @endif

                <h4>{{ $user->fullname }}</h4>
                <h6 class="text-muted">
                    Paket Kursus :
                    @foreach($user->courses as $course)
                        {{ $course->title }}
                    @endforeach
                </h6>
                <h6 class="text-muted">Skill : {{ $user->skill }}</h6>

            </div>

            <hr>

            <div class="row item-inline">

                <div class="col-sm-3">
                    <a target="_blank" href="{{ $user->fb != null ? $user->fb : 'javascript:void(0)' }}">
                        <img src="/images/social/facebook.png" width="18" alt="">
                        &nbsp;
                    </a>
                    <a target="_blank" href="{{ $user->tw != null ? $user->tw : 'javascript:void(0)' }}">
                        <img src="/images/social/twitter.png" width="18" alt="">
                        &nbsp;
                    </a>
                    <a target="_blank" href="{{ $user->ig != null ? $user->ig : 'javascript:void(0)' }}">
                        <img src="/images/social/instagram.png" width="18" alt="">
                        &nbsp;
                    </a>
                </div>

                <div class="col-sm-4 text-center">
                    <a href="{{ strpos($user->website, 'http://') !== false || strpos($user->website, 'https://') !== false ? $user->website : 'http://' . $user->website }}"
                       target="_blank">{{ $user->website != null ? substr($user->website, 0, 30) . '...' : '' }}</a>
                </div>

                <div class="col-sm-5 text-right">
                    @if($user->is_partnership)
                        <p>
                            <i class="fa fa-handshake-o fa-1x"></i>
                            Open for Partnership
                        </p>
                    @endif
                </div>

            </div>

        </div>


        <div class="card-box">
            <div class="media m-b-30">
                <i class="fa fa-certificate fa-2x"></i>
                <div class="media-body">
                    <span class="media-meta pull-right text-muted">
                        <small>{{ count($my_badges) }}/7 badges</small>
                    </span>
                    <h4 class=" font-16 m-0">
                        &nbsp; Your Badges
                    </h4>

                </div>
            </div>
            <hr>
            <div class="row item-inline">
                @foreach($my_badges as $badge)
                    <div class="col-sm-2">
                        <i class="fa {{ $badge->logo }} fa-2x text-primary p-2" title="{{ $badge->name }}"></i>
                    </div>
                @endforeach
            </div>
            {{--  <hr>
            <div type="hidden" class=" waves-effect waves-light btn-block" data-toggle="modal"
                 data-target="#modal-badges">
                <div class="text-center text-primary">
                    <i class="fa fa-plus-circle"></i>
                    <span class="">&nbsp; SEE MORE</span>
                </div>
            </div>  --}}

        </div> <!-- end card box -->

        <div class="card-box">
            <div class="media m-b-30">
                <i class="fa fa-graduation-cap fa-2x"></i>
                <div class="media-body">

                    <span class="media-meta pull-right text-muted">
                        <small>{{ $user->courses()->count() }} materi</small>
                    </span>

                    <h4 class=" font-16 m-0">
                        &nbsp;
                        Materials
                    </h4>

                </div>
            </div>
            <hr>

            @php
                $courses = $user->courses;
            @endphp

            @if($courses->count() != 0)
                <div>
                    @foreach($courses as $course)
                        <span class="label label-primary">{{ $course->title }}</span>
                    @endforeach
                </div>
            @else
                <div class="text-center">
                    Belum ada materi
                </div>
            @endif

            <hr>

            @if($courses->count() > 20)
                <div type="hidden" class="waves-effect waves-light btn-block" data-toggle="modal"
                     data-target="">
                    <div class="text-center text-primary">
                        <i class="fa fa-plus-circle"></i>
                        <span>&nbsp; SEE MORE</span>
                    </div>
                </div>
            @endif

        </div>

        <div class="card-box" style="margin-bottom: 0px !important;">
            <div class="media">
                <i class="fa fa-rss fa-2x"></i>
                <div class="media-body">
                    <h4 class=" font-16 m-0">
                        &nbsp; Newsfeed
                    </h4>

                </div>
            </div>

        </div>

        <div>
            @foreach($user->posts as $post)
                @include('social-learnings.posts.child.post-child')
            @endforeach
        </div>

    </div>


@endsection

@section('script')
    <script>
        function onFollow(userId) {

            var formData = new FormData();
            formData.append('_token', '{{ csrf_token() }}');
            formData.append('log', 'LOG_FOLLOW');

            $.ajax({
                url: '/users/' + userId + '/follow',
                type: 'POST',
                data: formData,
                success: function (data) {
                    console.log(data);
                    $('#followUser').replaceWith(data);
                    {{-- swal("Berhasil !", "Berhasil mengikuti", "success"); --}}
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }

        function onUnfollow(userId) {

            var formData = new FormData();
            formData.append('_token', '{{ csrf_token() }}');
            formData.append('log', 'LOG_UNFOLLOW');

            $.ajax({
                url: '/users/' + userId + '/unfollow',
                type: 'POST',
                data: formData,
                success: function (data) {
                    console.log(data);
                    $('#followUser').replaceWith(data);
                    {{-- swal("Berhasil !", "Batal mengikuti", "success"); --}}
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }

        function onLike(postId, userId) {
            var formData = new FormData();
            formData.append('log', 'LOG_LIKE_POSTS');
            formData.append('user_id', userId);

            $.ajax({
                url: '/posts/' + postId + '/likes',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#post' + postId).replaceWith(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onDislike(postId, userId) {
            var formData = new FormData();
            formData.append('log', 'LOG_DISLIKE_POSTS');
            formData.append('user_id', userId);

            $.ajax({
                url: '/posts/' + postId + '/dislikes',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#post' + postId).replaceWith(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onComment(postId, userId) {
            var content = $('#postcomment' + postId).val();
            console.log(content);

            var formData = new FormData();
            formData.append('_token', '{{ csrf_token() }}');
            formData.append('content', content);
            formData.append('log', 'LOG_COMMENTS');
            formData.append('user_id', userId);

            $.ajax({
                url: '/posts/' + postId + '/comments',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#post' + postId).replaceWith(data);
                },
                error: function (data) {
                    console.log(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

            return false;

        }

        function onLikeComment(commentId, postId) {
            var formData = new FormData();
            formData.append('log', 'LOG_LIKE_COMMENTS');

            $.ajax({
                url: '/comments/' + commentId + '/likes',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#post' + postId).replaceWith(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onDislikeComment(commentId, postId) {
            var formData = new FormData();
            formData.append('log', 'LOG_DISLIKE_COMMENTS');

            $.ajax({
                url: '/comments/' + commentId + '/dislikes',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#post' + postId).replaceWith(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }
    </script>
@endsection