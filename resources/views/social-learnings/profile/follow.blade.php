@if($user->id == Auth::user()->id)

@else
<div id="followUser">
    @if(Auth::user()->followings()->where(['following_id' => $user->id])->count() == 0)
        <button type="button" class="btn btn-primary pull-right" onclick="onFollow({{ $user->id }})">Ikuti</button>
    @else
        <button type="button" class="btn btn-danger pull-right" onclick="onUnfollow({{ $user->id }})">Batal Ikuti</button>
    @endif
</div>
@endif
