@extends('layouts.frontend')

@section('content')
    <div class="col-md-8 col-lg-6 footer-space">
        <div class="row">
            <div class="col-lg-12">
                <div class="search-result-box">
                    <ul class="nav nav-tabs">
                        <li class="nav-item col-md-4 p-0">
                            <a href="#events" data-toggle="tab" aria-expanded="true" class="nav-link active">
                                <b>Events</b>
                            </a>
                        </li>
                        <li class="nav-item col-md-4 p-0">
                            <a href="#myevents" data-toggle="tab" aria-expanded="false" class="nav-link">
                                <b>My Event</b>
                            </a>
                        </li>
                        <li class="nav-item col-md-4 p-0">
                            <a href="#followed" data-toggle="tab" aria-expanded="false" class="nav-link">
                                <b>Event Followed</b>
                            </a>
                        </li>

                    </ul>

                    <!-- events -->

                    <div class="tab-content">
                        <div class="tab-pane active" id="events">
                            @foreach($events as $indexEvent => $event)
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class=" m-t-20">
                                            <hr>
                                            <div class="media m-b-30">
                                                
                                                @if($event->user->photo === null)
                                                    <img class="d-flex rounded-circle"
                                                         src="/images/default.png" alt="Generic placeholder image" height="35" width="35">
                                                @else
                                                    <img class="d-flex rounded-circle"
                                                         src="/images/photo/{{ $event->user->photo }}"
                                                         alt="Generic placeholder image" height="35" width="35">
                                                @endif

                                                &nbsp;&nbsp;&nbsp;
                                                <div class="media-body">
                                                    <form action="{{ $event->users()->where(['user_id' => Auth::id()])->count() === 0 ? route('events.follow', $event->id) : route('events.unfollow', $event->id) }}" method="POST">
                                                        {{ csrf_field() }}
                                                        @if($event->users()->where(['user_id' => Auth::id()])->count() === 0)
                                                            <button type="submit" class="btn btn-primary pull-right">Ikuti</button>
                                                        @else
                                                            <button type="submit" class="btn btn-danger pull-right">Batal Ikuti</button>
                                                        @endif
                                                    </form>
                                                    
                                                    <h4 class="text-primary font-16 m-0">
                                                        {{ $event->user->fullname }}
                                                        <small class="text-muted">
                                                            <i class="ion-clock"></i>
                                                            {{ $event->updated_at->diffForHumans() }}
                                                        </small>
                                                    </h4>

                                                </div>
                                            </div>
                                            <h6>
                                                <span class="fa fa-calendar"></span> {{ date('d M Y', strtotime($event->date)) }}</h6>
                                            <h6>{{ $event->title }}</h6>
                                            <p>{{ $event->body }}</p>

                                            <hr>

                                            <div class="row mb-5">
                                                <div class="col-md-2">
                                                    <i class="fa fa-users "> {{ count($event->users) }}</i>
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                </div>
                            @endforeach
                        <!-- end timelines tab -->


                        </div>

                        <!-- my events tab -->
                        <div class="tab-pane" id="myevents">
                            <table class="table table-striped">
                                <button type="button" name="button" class="btn btn-primary m-2" data-toggle="modal"
                                        data-target="#modalAddEvent">
                                    <span class="fa fa-plus"></span>
                                    Tambah Event
                                </button>
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name Event</th>
                                    <th>Follower</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach(Auth::user()->myevents as $indexEvent => $event)
                                    <tr id="event{{ $event->id }}">
                                        <th scope="row">{{ $indexEvent+1 }}</th>
                                        <td>{{ $event->title }}</td>
                                        <td>{{ $indexEvent+1 }}</td>
                                        <td>
                                            <button type="button" class="btn btn-danger pull-right" data-toggle="modal"
                                                    data-target="#modalDelete{{ $event->id }}">
                                                <span class="fa fa-x"></span>
                                                Cancel Event
                                            </button>
                                        </td>
                                    </tr>

                                    <!--  Start Modal-->
                                    <div class="modal fade" id="modalDelete{{ $event->id }}">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="defaultModalLabel">
                                                        Cancel Event?</h4>
                                                </div>
                                                <div class="modal-body">
                                                    Jika anda menghapus event ini maka anda maka seluruh pengikut dari
                                                    event ini akan hilang dan terhapus, Yakin?
                                                </div>
                                                <div class="modal-footer">

                                                    <button type="button" class="btn btn-default"
                                                            data-dismiss="modal"> Batal
                                                    </button>
                                                    <form action="{{ route('social-learning-events.destroy', $event->id) }}" method="POST">

                                                        {{ csrf_field() }}

                                                        <input type="hidden" name="_method" value="DELETE">

                                                        <button type="submit" class="btn btn-danger">
                                                            Hapus
                                                        </button>
                                                    </form>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Modal -->

                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- end myevents tab -->


                        <!-- Modal Add Events-->
                        <div class="modal fade" id="modalAddEvent" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLongTitle">Add Event</h5>
                                        <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>

                                    <div class="modal-body">

                                        <form method="post"
                                              action="{{ route('social-learning-events.store', $_SERVER['HTTP_HOST']) }}"
                                              data-parsley-validate>

                                            <div class="modal-body">
                                                <fieldset>
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <input type="hidden" name="user_id" value="{{ Auth::id() }}">
                                                    <input type="hidden" name="tenant_id"
                                                           value="{{ Auth::user()->tenant->id }}">

                                                    <input type="hidden" name="log" value="LOG_CREATE_EVENTS">

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <input type="text" name="title" value=""
                                                                   class="form-control mb-1" placeholder="Judul Event"
                                                                   required>
                                                            <textarea type="text" name="body"
                                                                      placeholder="Deskripsi singkat"
                                                                      class="form-control"
                                                                      rows="4" placeholder="" required></textarea>
                                                            <input type="date" name="date" value=""
                                                                   class="form-control mb-1" required>
                                                        </div>
                                                    </div>

                                                </fieldset>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default"
                                                        data-dismiss="modal">
                                                    <i class="fa fa-close"></i>
                                                    Tutup
                                                </button>
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="fa fa-save"></i>
                                                    Simpan
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- events followed -->
                        <div class="tab-pane" id="followed">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Event Followed</th>
                                    <th>Tanggal</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach(Auth::user()->events as $indexEvent => $event)
                                    <tr>
                                        <th scope="row">{{ $indexEvent+1 }}</th>
                                        <td>{{ str_limit($event->title, 14) }}</td>
                                        <td>{{ date('d M Y', strtotime($event->date)) }}</td>
                                        <td>
                                            <form action="{{ $event->users()->where(['user_id' => Auth::id()])->count() === 0 ? route('events.follow', $event->id) : route('events.unfollow', $event->id) }}" method="POST">
                                                {{ csrf_field() }}
                                                @if($event->users()->where(['user_id' => Auth::id()])->count() === 0)
                                                    <button type="submit" class="btn btn-primary pull-right">Ikuti</button>
                                                @else
                                                    <button type="submit" class="btn btn-danger pull-right">Batal Ikuti</button>
                                                @endif
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- end events followed -->
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
@section('script')
    <script type="text/javascript">

        function onJoinEvent(eventId) {
            var formData = new FormData();
            formData.append('log', 'LOG_JOIN_EVENTS');

            $.ajax({
                url: '/social-learning-events/' + eventId + '/follow',
                type: 'POST',
                data: formData,
                success: function (data) {
                    console.log(data);
                    $('#join' + eventId).replaceWith(data);
                    swal("Berhasil !", "Berhasil menambahkan event", "success");
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onLeaveEvent(eventId) {
            // console.log('wfwfwf');
            $.ajax({
                url: '/social-learning-events/' + eventId + '/unfollow',
                type: 'POST',
                data: null,
                success: function (data) {
                    console.log(data);
                    $('#join' + eventId).replaceWith(data);
                    swal("Berhasil !", "Berhasil mengikuti event", "success");
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onRemoveEventList(eventId) {
            // console.log('wfwfwf');
            $.ajax({
                url: '/social-learning-events/' + eventId,
                type: 'DELETE',
                data: null,
                success: function (data) {
                    console.log(data);
                    $('#event' + eventId).remove();
                    $('#modalDelete' + eventId).modal('toggle');
                    swal("Berhasil !", "Berhasil Membatalkan Event", "success");
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }

    </script>
@endsection
