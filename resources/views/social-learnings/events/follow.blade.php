<div id="join{{ $event->id }}" class="col-md-4">
    @if($event->users()->where(['user_id' => Auth::id()])->count() === 0)
        <a class="dropdown-item" onclick="onJoinEvent({{ $event->id }})">+ Ikuti</a>
    @else
        <a class="dropdown-item" onclick="onLeaveEvent({{ $event->id }})"> Batal Ikuti</a>
    @endif
</div>
