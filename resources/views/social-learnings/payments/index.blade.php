@extends('layouts.frontend')

@section('content')

    <div class="col-md-8 col-lg-6 footer-space">
        <!-- <img src="/images/nbls-profile-header.jpg" class="img-fluid" alt="" height="50px"> -->
        <h4>Check Out</h4>
        <hr>

        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            
            <strong>Kursus yang gratis</strong> akan otomatis masuk ke My Course.

            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <!-- <div class="alert alert-warning" role="alert">
            <div class="row">
                <div class="col-1 col-md-1">
                    <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                </div>
                <div class="col col-md-11">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Warning!</strong> You have a live listring with a simillar tittle.
                                              Please see the duplicate listring policy on eBay for more information.
                </div>
            </div>
        </div> -->


        <div class="card-box">
            <div class="row text-center">
                <div class="col-2">
                    <div class="profileStep">
                        <div class="connect bg-primary"></div>
                        <div class="icon bg-primary rounded-circle">
                            <i class="fa fa-check"></i>
                        </div>
                        <div class="text-primary text-center">Select Course</div>
                    </div>
                </div>
                <div class="col-2">
                    <div class="profileStep text-fluid">
                        <div class="connect bg-primary"></div>
                        <div class="icon bg-primary rounded-circle">
                            <i class="fa fa-check"></i>
                        </div>
                        <div class="text-primary">Login</div>
                    </div>
                </div>
                <div class="col-2">
                    <div class="profileStep">
                        <div class="connect bg-primary"></div>
                        <div class="icon bg-primary rounded-circle">
                            <i class="fa fa-check"></i>
                        </div>
                        <div class="info text-primary">Review</div>
                    </div>
                </div>
                <div class="col-2">
                    <div class="profileStep">
                        <div class="connect bg-muted"></div>
                        <div class="icon bg-primary rounded-circle">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        <div class="text-primary">Check Out</div>
                    </div>
                </div>
                <div class="col-2">
                    <div class="profileStep">
                        <div class=""></div>
                        <div class="icon bg-muted rounded-circle">
                            <i class="md md-payment"></i>
                        </div>
                        <div class="text-muted">payment</div>
                    </div>
                </div>
            </div>
        </div>

        {{-- <form action="payment-method/checkout.php" method="POST"> --}}
        <form action="{{ route('payments.register') }}" method="POST">

            {{ csrf_field() }}

            <div class="card-box">
                <h5 class="text-primary"><strong>Your Order Summary</strong></h5>
                <div class="row">

                    <table class="table">
                        <thead>
                        <tr>
                            <th class="w-25">Course</th>
                            <th class="w-50">Item Description</th>
                            <th class="text-center">Price</th>
                            <!-- <th class="text-center">Action</th> -->
                        </tr>
                        </thead>
                        <tbody>
                        @foreach(Auth::user()->carts as $cart)
                            <tr>
                                <th scope="row">
                                    @if($cart->image === null)
                                        <img src="/images/order/web+design.jpg" alt="" class="w-100 p-1">
                                    @else
                                        <img src="/xxcourses/images/{{ $cart->image }}" alt="" class="w-100 p-1">
                                    @endif

                                </th>

                                <td>
                                    {{ $cart->title }}
                                    <p class="text-muted">By : {{ $cart->author->fullname }}</p>
                                </td>

                                <td class="text-center">
                                    Rp. {{ number_format($cart->price, 0, ',', '.') }}
                                </td>

                                <!-- <td class="text-center">
                                    <a
                                        href="social-learning-my-course/delete-cart/{{ $cart->id }}"
                                        class="btn btn-danger btn-xs">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td> -->
                            </tr>
                        @endforeach
                        
                        @foreach(Auth::user()->package_carts as $package_cart)
                            <tr>
                                <th scope="row">
                                    @if($package_cart->image === null)
                                        <img src="/images/order/web+design.jpg" alt="" class="w-100 p-1">
                                    @else
                                        <img src="/course-package/images/{{ $package_cart->image }}" alt="" class="w-100 p-1">
                                    @endif

                                </th>

                                <td>
                                    {{ $package_cart->title }}
                                    <p class="text-muted">By : {{ $package_cart->user->fullname }}</p>
                                </td>

                                <td class="text-center">
                                    Rp. {{ number_format($package_cart->price, 0, ',', '.') }}
                                </td>

                                <!-- <td class="text-center">
                                    <a href="" class="fa fa-trash"></a>
                                </td> -->
                                
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="card-box">
                <h5 class="text-primary">I Prepare to pay by :</h5>
                <table class="table">
                    <tbody>
                    <tr>
                        <td>
                            <div class="radio">
                                <input type="radio" name="paymentMethod" id="radioCredit" value="option1"
                                       onclick="onSelectCredit()" checked>

                                {{--  <input type="hidden" name="payMethod" value="01">  --}}
                            </div>
                        </td>
                        <td class="w-50">
                            <label for="radio1">Credit Card<p class="text-fluid">Safe money trancer using you bang
                                                                                 account, Visa, Master Card.</p></label>
                        </td>
                        <td><img src="/images/order/visa+mastercard.png" alt="" class="w-75 p-1 float-right"></td>
                    </tr>

                    {{--  <tr id="paymentMethodCredit">
                        <td colspan="3">
                            <!-- awal container -->
                            <div class="card-box bg-muted">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <div class="col-12 p-0">
                                                <div class="input-group">
                                                    <span class="input-group-addon p-0 p-t-0 w-25"><img
                                                                src="/images/order/master+card.jpg" alt=""
                                                                class="mastercard w-50"></span>
                                                    <input type="text" id="input-mastercard" name="input-mastercard"
                                                           class="form-control  text-center"
                                                           placeholder="1234 - 5678 - 9876 - 5432">
                                                    <i class="fa fa-question-circle text-center fa-2x text-muted"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <div class="col-12 p-0">
                                                <div class="input-group">
                                                    <input type="text" id="input-carholder-name"
                                                           name="input-carholder-name" class="form-control"
                                                           placeholder="Chalholder Name">
                                                    <i class="fa fa-question-circle fa-2x text-muted"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-3">
                                        <div class="form-group btn-group">
                                            <div class="col-12 p-0">
                                                <div class="input-group">
                                                    <button type="button" class="btn">mm</button>
                                                    <button type="button"
                                                            class="btn dropdown-toggle dropdown-toggle-split"
                                                            data-toggle="dropdown"></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-1">
                                        <i class="fa fa-question-circle fa-2x text-muted ml-0"></i>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group btn-group">
                                            <div class="col-12 p-0">
                                                <div class="input-group">
                                                    <button type="button" class="btn">YYYY</button>
                                                    <button type="button"
                                                            class="btn dropdown-toggle dropdown-toggle-split"
                                                            data-toggle="dropdown"></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group btn-group">
                                            <div class="col-12 p-0">
                                                <div class="input-group">
                                                    <input type="text" id="input-mastercard" name="input-mastercard"
                                                           class="form-control ml-2" placeholder="CVC/CVV">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-1">
                                        <i class="fa fa-question-circle fa-2x text-muted"></i>
                                    </div>
                                </div>
                            </div>

                    <div class="row">
                      <div class="col-sm-4">
                        <p>Type Pembayaran</p>
                      </div>
                      <div class="col-sm-8">
                        <div class="radio form-check-inline">
                            <input type="radio" id="inlineRadio1" value="option1" name="radioInline" checked>
                            <label for="inlineRadio1"> PembayranPenuh </label>
                        </div>
                        <div class="radio form-check-inline">
                          <input type="radio" id="inlineRadio2" value="option2" name="radioInline">
                          <label for="inlineRadio2"> Cicilan 0% </label>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-sm-4">
                        <p>Durasi Cicilan</p>
                      </div>
                      <div class="col-sm-8">
                        <div class="dropdown btn-group">
                          <input type="text" name="" value="" placeholder="- Choose One -" >
                          <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"></button>
                            <div class="dropdown-menu dropdown-menu-right">
                              <a class="dropdown-item" href="#">3 bulan - 74.000</a>
                              <a class="dropdown-item" href="#">6 bulan - 54.000</a>
                              <a class="dropdown-item" href="#">12 bulan - 34.000</a>
                            </div>
                          </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-1">
                        <div class="checkbox checkbox-inverse form-check-inline">
                          <input type="checkbox" id="inlineCheckbox1" value="option1">
                        </div>
                      </div>
                      <div class="col-11">
                        <label for="inlineCheckbox1"> Save the credit card information for your hext purchase </label>
                      </div>
                    </div>

                     </td>
                </tr>    --}}

                    <tr>
                        <td>
                            <div class="radio">
                                <input type="radio" name="paymentMethod" id="radioBank" value="option1"
                                       onclick="onSelectBank()" checked>
                            </div>
                        </td>
                        <td class="w-50"><label for="radio1">
                                Bank Transfer
                                <p>
                                    Pilih Bank terpercaya Anda untuk melakukan transfer antar
                                    Bank BCA, Mandiri dan ATM Bersama.
                                </p>
                            </label></td>
                        <td><img src="/images/order/bca+mandiri.png" alt="" class="w-75 p-1 float-right">
                            <p><img src="/images/order/atm+bersama.png" alt="" class="w-50 p-1 float-right"></p></td>
                    </tr>
                    <tr id="paymentMethodBank">
                        <td colspan="3">
                            <div class=" m-b-15 container">

                                {{--  <div class="alert alert-info">
                                    <span>Place note that with this paymento option, we will not hold stock util yuo has-value
                                     mode your payment. To yout items, please kindly transfer the payment immediately.</span> <br>
                                     <span>Payment transfer has to be made within 24 hours, otherwise the other will
                                     automatically canceled. Thetransfer using virtual accont in verified with real time
                                   verification.</span>
                                </div>  --}}

                                <select class="form-control" name="bankCd">
                                    <option value="CENA">BCA</option>
                                    <option value="BNIN">BNI</option>
                                    <option value="BMRI">Mandiri</option>
                                    <option value="BBBA">Permata</option>
                                    <option value="IBBK">BII Maybank</option>
                                    <option value="BNIA">CIMB Niaga</option>
                                    <option value="HNBN">KEB Hana Bank</option>
                                    <option value="BRIN">BRI</option>
                                    <option value="BDIN">Danamon</option>
                                </select>

                                <input id="payMethod" type="hidden" name="payMethod" value="02">

                                <input type="hidden" name="carts" value="{{ $nicepay_carts }}">

                                <input type="hidden" name="user_id" value="{{ Auth::id() }}">

                                <input type="hidden" name="amount" value="{{ Auth::user()->carts()->sum('price') + Auth::user()->package_carts()->sum('price') }}">

                                {{--  <div class="row">
                                  <div class="col-md-4 text-center">
                                    <input type="radio" name="bankCd" value="">
                                    <img class="w-50" src="/images/order/bank_bca.png" alt="">
                                  </div>

                                  <div class="col-md-4 text-center">
                                    <input type="radio" name="bankCd" value="">
                                    <img class="w-50" src="/images/order/bank_mandiri.png" alt="">
                                  </div>

                                  <div class="col-md-4 text-center">
                                    <input type="radio" name="bankCd" value="">
                                    <img class="w-25" src="/images/order/atm+bersama.png" alt="">
                                  </div>
                                </div>  --}}

                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

            @include('social-learnings.payments.child.section-payment')

            <div class="m-b-15">
                <input type="hidden" name="email" value="{{ Auth::user()->email }}">

                <button type="button" class="tombol btn waves-effect waves-light w-lg float-left">
                    <i class="ti-arrow-left icon-zize"></i>&nbsp;BACK REVIEW
                </button>
                <button type="submit" class="tombol btn btn-primary waves-effect waves-light w-lg float-right">PAY NOW&nbsp;<i
                            class="ti-arrow-right icon-zize"></i>
                </button>
            </div>

        </form>
    </div>
@endsection

@section('script')
    <script>
        $(function () {
            $('#paymentMethodCredit').hide();
            $('#paymentMethodBank').show();

            {{--  $('#discountSection').hide();  --}}
        });
    </script>

    <script>
        function onSelectBank() {
            $('#paymentMethodCredit').hide();
            $('#paymentMethodBank').show();

            $('input#payMethod').val('02');
        }

        function onSelectCredit() {
            $('#paymentMethodBank').hide();
            $('#paymentMethodCredit').show();

            $('input#payMethod').val('01');
        }

        function onCheckVoucher() {

            var voucherCode = $('input#voucherCodeInput').val();
            
            var formData = new FormData();
            formData.append('_token', '{{ csrf_token() }}');
            formData.append('voucher_code', voucherCode);
            formData.append('subtotal', {{ Auth::user()->carts()->sum('price') }});

            $.ajax({
                url: '/vouchers/check',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#totalPriceSection').replaceWith(data);
                    swal("Berhasil !", "Kode voucher benar", "success");
                },
                error: function (error) {
                    swal("Gagal !", "Kode voucher salah", "error");
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    </script>
@endsection