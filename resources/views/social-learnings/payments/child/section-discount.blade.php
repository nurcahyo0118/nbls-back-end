<div id="totalPriceSection" class="card-box">

    <div id="discountSection" class="row text-danger">
        <div class="col-6 text-right">
            <p>Diskon</p>
        </div>
        <div class="col-6 text-right">
            <p>
                Rp. {{ number_format($voucher->discount_price, 0, ',', '.') }}
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-6 text-right">
            <p>Subtotal</p>
        </div>
        <div class="col-6 text-right">
            <p>
                Rp. {{ number_format(Auth::user()->carts()->sum('price'), 0, ',', '.') }}
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-6 text-right">
            <p>Total</p>
        </div>
        <div class="col-6 text-right text-success">
            <p></p>
            Rp. {{ number_format($voucher->total, 0, ',', '.') }}
            </p>
        </div>
    </div>

</div>