<div id="totalPriceSection" class="card-box">
    {{-- <div class="row responsive m-b-15">
        <div class="col-md-5 col-sm-5 p-0 text-primary text-center">
            Punya Kode Voucher BabaStudio ?
        </div>

        <div class="col-md-7 col-sm-7 text-danger text-right p-l-5">
        <div class="input-group font-size-small">
            <input id="voucherCodeInput" type="text" class="form-control" placeholder="Masukkan Kode Voucher">
            <span class="input-group-btn" id="basic-addon2">
                <button class="btn btn-primary" type="button" onclick="onCheckVoucher()">Check</button>
            </span>
            </div>
        </div>

    </div> --}}

    <div class="row">
        <div class="col-6 text-right">
            <p>Subtotal</p>
        </div>
        <div class="col-6 text-right">
            <p>
                Rp. {{ number_format(Auth::user()->carts()->sum('price') + Auth::user()->package_carts()->sum('price'), 0, ',', '.') }}
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-6 text-right">
            <p>Total</p>
        </div>
        <div class="col-6 text-right text-success">
            <p></p>
            Rp. {{ number_format(Auth::user()->carts()->sum('price') + Auth::user()->package_carts()->sum('price'), 0, ',', '.') }}
            </p>
        </div>
    </div>

</div>