@extends('layouts.frontend')

@section('content')
    <!-- isi tengah -->

    <div class="col-md-8 col-lg-6 footer-space">

        <table class="table table-hover">
            <thead>
            <tr>
                <th>#</th>
                <th>Foto</th>
                <th>Full Name</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php $no=1;?>
            @foreach($i_time as $search)
                <tr>
                    <td>{{ $no ++ }}</td>

                    @if( $search->photo === null)
                        <td>
                            <img src="/images/default.png" alt="" class="img-rounded center-block" height="50px" width="50px">
                        </td>
                    @else
                        <td>
                            <img src="/images/photo/{{ $search->photo }}" alt="" class="img-rounded center-block" height="50px" width="50px">
                        </td>
                    @endif

                    <td>{{ substr($search->fullname,0,16) }}</td>
                    <td>
                        <a href="social-learning-posts/{{$search->username}}">
                            <button type="button" name="button" class="btn btn-primary">Profile</button>
                        </a>

                        @if(Auth::user()->followings()->where(['following_id' => $search->id])->count() == 0)
                            <a id="searchUser{{ $search->id }}" onclick="onFollow({{ $search->id }})">
                                <button type="button" class="btn btn-primary" name="button">Follow</button>
                            </a>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>

    <!-- akhir isi tengah -->
@endsection

@section('script')
<script type="text/javascript">

    function onFollow(userId) {

        var formData = new FormData();

        formData.append('_token', '{{ csrf_token() }}');
        formData.append('log', 'LOG_FOLLOW');
        formData.append('following_id', userId);

        $.ajax({
            url: '/users/' + userId + '/follow',
            type: 'POST',
            data: formData,
            success: function (data) {
                console.log(data);
                $('#searchUser' + userId).remove();
                swal("Berhasil !", "Berhasil mengikuti", "success");
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }

    function onUnfollow(userId) {

        var formData = new FormData();
        formData.append('_token', '{{ csrf_token() }}');
        formData.append('log', 'LOG_UNFOLLOW');

        $.ajax({
            url: '/users/' + userId + '/unfollow',
            type: 'POST',
            data: formData,
            success: function (data) {
                console.log(data);
                $('#followUser').replaceWith(data);
                swal("Berhasil !", "Batal mengikuti", "success");
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
</script>
@endsection
