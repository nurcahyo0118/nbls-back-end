@extends('layouts.frontend')

@section('content')
    <!-- isi tengah -->
    @php
        $users = App\User::where('id', '<>', Auth::id())->whereNotIn('id', Auth::user()->followings)->take(7)->inRandomOrder()->get();
    @endphp
    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-body mt-0">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Cari Teman baru</h4>
            <table class="table">
                <thead> 
                <tr>
                    <th>#</th>
                    <th>Foto</th>
                    <th>Full Name</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  <?php $no=1;?>

                @foreach($users as $search)
                    <tr>
                        <td>{{ $no ++ }}</td>

                        @if( $search->photo === null)
                        <td><img src="/images/default.png" alt="" class="img-rounded center-block" height="50px" width="50px"></td>
                        @else
                        <td><img src="/images/photo/{{ $search->photo }}" alt="" class="img-rounded center-block" height="50px" width="50px"></td>
                        @endif

                        <td>{{ substr($search->fullname,0,16) }}</td>
                        <td> <a href="social-learning-posts/{{$search->username}}"><button type="button" name="button" class="btn btn-primary">Profile</button></a>

                          @if(Auth::user()->followings()->where(['following_id' => $search->id])->count() == 0)
                            <a id="searchUser{{ $search->id }}" onclick="onFollow({{ $search->id }})">
                                <button type="button" class="btn btn-primary" name="button">Follow</button>
                            </a>
                        @endif
                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>

      </div>
    </div>
    <div class="col-md-8 col-lg-6 footer-space">
        <h4>Daftar Teman Yang Mengikutimu</h4>
        <table class="table table-hover">
            <thead>
            <tr>
                <th>#</th>
                <th>Foto</th>
                <th>Full Name</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
              <?php $no=1;?>

            @foreach(Auth::user()->followers->take(8) as $search)
                <tr>
                    <td>{{ $no ++ }}</td>

                    @if( $search->photo === null)
                    <td><img src="/images/default.png" alt="" class="img-rounded center-block" height="50px" width="50px"></td>
                    @else
                    <td><img src="/images/photo/{{ $search->photo }}" alt="" class="img-rounded center-block" height="50px" width="50px"></td>
                    @endif

                    <td>{{ substr($search->fullname,0,16) }}</td>
                    <td>
                       <a href="social-learning-posts/{{$search->username}}"><button type="button" name="button" class="btn btn-primary">Profile</button></a>
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>

    </div>


    <!-- akhir isi tengah -->
@endsection

@section('script')
<script type="text/javascript">

      function onFollow(userId) {

          var formData = new FormData();

          formData.append('_token', '{{ csrf_token() }}');
          formData.append('log', 'LOG_FOLLOW');
          formData.append('following_id', {{ Auth::id() }});

          $.ajax({
              url: '/users/' + userId + '/follow',
              type: 'POST',
              data: formData,
              success: function (data) {
                  console.log(data);
                  $('#searchUser' + userId).remove();
                  swal("Berhasil !", "Berhasil mengikuti", "success");
              },
              cache: false,
              contentType: false,
              processData: false
          });
      }

      function onUnfollow(userId) {

          var formData = new FormData();
          formData.append('_token', '{{ csrf_token() }}');
          formData.append('log', 'LOG_UNFOLLOW');

          $.ajax({
              url: '/users/' + userId + '/unfollow',
              type: 'POST',
              data: formData,
              success: function (data) {
                  console.log(data);
                  $('#followUser').replaceWith(data);
                  swal("Berhasil !", "Batal mengikuti", "success");
              },
              cache: false,
              contentType: false,
              processData: false
          });
      }
      $(window).on('load',function(){
          $('#myModal').modal('show');
      });
</script>
@endsection
