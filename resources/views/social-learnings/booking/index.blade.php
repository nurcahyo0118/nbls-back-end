@extends('layouts.frontend')

@section('content')
    <!-- isi tengah -->

    <div class="col-md-8 col-lg-6 footer-space">

        @include('layouts.partials.message')

        <h6 class="text-muted">
            <span class="fa fa-home"></span>
            Courses
            <span class="fa fa-long-arrow-right"></span>
            Booking
        </h6>

        <strong class="mt-4">List Booking</strong>

        <table class="table">
            <thead>
            <tr class="">
                <th>Instruktur</th>
                <th>Tanggal</th>
                <th>Kelas</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach(Auth::user()->bookings as $index => $booking)
                <tr id="booking{{ $booking->id }}">
                    <td>{{ $booking->author->fullname }}</td>
                    <td>{{ $booking->pivot->date }}</td>
                    <td>{{ $booking->title }}</td>
                    <td>
                        @if($booking->pivot->date === Carbon\Carbon::now()->toDateString())
                            <a href="{{ route('social-learning-exam.form-input', [$_SERVER['HTTP_HOST'], $booking->id])}}"
                            class="btn btn-primary">
                                <i class="fa fa-eye"></i>
                                Lihat Ujian
                            </a>
                        @endif
                        <!-- <form action="{{ route('social-learning-booking.destroy', [$_SERVER['HTTP_HOST'], $booking->id]) }}" method="POST">

                            {{ csrf_field() }}

                            <input type="hidden" name="_method" value="DELETE"> -->

                            <button type="button"
                                onclick="onDeleteBooking({{ $booking->id }})"
                                class="btn btn-danger">
                                <i class="fa fa-trash"></i>
                                Batal
                            </button>
                        <!-- </form> -->
                        
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>

    <!-- akhir isi tengah -->
@endsection

@section('script')
<script>
    function onDeleteBooking(id) {
        swal({
            title: "Apa kamu yakin?",
            text: "Data yang telah di hapus tidak dapat dikembalikan",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                
                $.ajax({
                    url: '/social-learning-booking/' + id,
                    type: 'DELETE',
                    data: null,
                    success: function (data) {
                        location.reload();
                    },
                    error: function (error) {
                        location.reload();
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });

            }
        });
    }
</script>
@endsection