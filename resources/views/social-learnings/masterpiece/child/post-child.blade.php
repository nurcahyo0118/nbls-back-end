<div id="post{{ $master->id }}" class="col-lg-12">
    <div class=" m-t-20">

        <div class="media m-b-30">
            <a href="{{ route('social-learning-posts.show', $master->user->username) }}">
                @if($master->user->photo === null)
                    <img class="d-flex mr-3 rounded-circle thumb-sm"
                         src="/images/default.png" alt="Generic placeholder image">
                @else
                    <img class="d-flex mr-3 rounded-circle thumb-sm"
                         src="/images/photo/{{ $master->user->photo }}" alt="Generic placeholder image">
                @endif
            </a>

            <div class="media-body">

                @if($master->user->id === Auth::id())
                    <a class="media-meta pull-right"
                          role="button" id="dropdownMenuLink" data-toggle="dropdown"
                          aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-bookmark"></i>
                    </a>

                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item" data-toggle="modal"
                           data-target="#modalEdit{{ $master->id }}">Edit
                        </a>
                        <a class="dropdown-item" onclick="onDeletePost({{ $master->id }})">Delete
                        </a>
                    </div>

                @endif

                <h4 class="text-primary font-16 m-0">
                    {{ $master->user->fullname }}
                    <small class="text-muted">
                        <i class="ion-clock"></i>
                        {{ $master->created_at->diffForHumans() }}
                    </small>
                </h4>


            </div>
        </div>

        <p>{{ $master->content }}</p>

        @if($master->file != null)
            <hr>
            <a href="#!" class="btn btn-info" download="{{ $master->file }}">Download File</a>
            <hr>
        @endif

        <div class="row">
            <div class="col-md-12">
                <a class="post-icon" onclick="onLike({{ $master->id }})">
                    <i class="fa fa-thumbs-o-up {{ $master->likesMasterpieces()->where('user_id', Auth::id())->count() ? 'like' : '' }}">
                        {{ $master->likesMasterpieces()->count() }}
                    </i>
                </a>

                <a class="post-icon" onclick="onDislike({{ $master->id }})">
                    <i class="fa fa-thumbs-o-down {{ $master->dislikesMasterpieces()->where('user_id', Auth::id())->count() ? 'dislike' : '' }}">
                        {{ $master->dislikesMasterpieces()->count() }}
                    </i>
                </a>

                <a class="pull-right post-icon">
                    <p class="fa fa-comments-o"> {{ $master->postCommentsMasterpieces()->count() }} </p>
                </a>
            </div>

        </div>
        <hr>

        <div>

            @foreach($master->postCommentsMasterpieces as $comment)
                <div class="comment" style="background-colot:p ">
                    @if($comment->user->photo === null)
                        <img src="/images/default.png" alt="" class="comment-avatar">
                    @else
                        <img src="/images/photo/{{ $comment->user->photo }}" alt="" class="comment-avatar">
                    @endif

                    <div class="comment-body">
                        <div class="comment-text">
                            <div class="comment-header">
                                <a href="{{ route('social-learning-posts.show', $comment->user->username) }}" title="">{{ $comment->user->fullname }}</a>
                                <span>about {{ $comment->created_at->diffForHumans() }}</span>
                            </div>
                            {{ $comment->body }}

                        </div>
                        <div class="comment-footer">
                            <a onclick="onLikeComment({{ $comment->id }}, {{ $master->id }})">
                                <i class="fa fa-thumbs-o-up {{ $comment->likesMasterpieces()->where('user_id', Auth::id())->count() ? 'like' : '' }}"></i>
                                {{ $comment->likesMasterpieces()->count() }}
                            </a>
                            &nbsp;
                            <a onclick="onDislikeComment({{ $comment->id }}, {{ $master->id }})">
                                <i class="fa fa-thumbs-o-down {{ $comment->dislikesMasterpieces()->where('user_id', Auth::id())->count() ? 'dislike' : '' }}"></i>
                                {{ $comment->dislikesMasterpieces()->count() }}
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach

            <div class="media m-b-30">

                @if(Auth::user()->photo === null)
                    <img class="d-flex mr-3 rounded-circle thumb-sm"
                         src="/images/default.png" alt="Generic placeholder image">
                @else
                    <img class="d-flex mr-3 rounded-circle thumb-sm"
                         src="/images/photo/{{ Auth::user()->photo }}" alt="Generic placeholder image">
                @endif

                <div class="media-body">
                    <form id="formaddcomment" onsubmit="onComment({{ $master->id }});return false;"
                          method="post" data-parsley-validate>

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <input id="postcomment{{ $master->id }}" type="text" name="content" class="form-control"
                               placeholder="Tulis komentar...">

                    </form>

                </div>
            </div>
            <hr>
        </div>

    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalEdit{{ $master->id }}" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Status</h5>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">

                <form id="formeditpost" onsubmit="onEditPost({{ $master->id }});return false;" method="post"
                      data-parsley-validate>

                    <div class="modal-body">
                        <fieldset>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <input name="_method" type="hidden" value="PUT">

                            <input type="hidden" name="post_id" value="{{ $master->id }}">

                            <div class="col-md-12">
                                <div class="form-group">
                                  <textarea type="text" name="content"
                                            class="form-control"
                                            rows="4">{{ $master->content }}</textarea>
                                </div>
                            </div>

                        </fieldset>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal">
                            <i class="fa fa-close"></i>
                            Tutup
                        </button>
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-save"></i>
                            Simpan
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
