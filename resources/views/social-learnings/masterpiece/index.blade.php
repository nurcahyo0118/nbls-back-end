@extends('layouts.frontend')

@section('content')
    <!-- isi tengah -->

    <div class="col-md-8 col-lg-6 footer-space">
        <h6 class="text-muted">
            <span class="fa fa-home"></span>
            Courses
            <span class="fa fa-long-arrow-right"></span>
            Hasil Karya
        </h6>
        <div class="card mb-3">
            <div class="card-header bg-info">
                <strong class="text-white">Hasil Karya</strong>
            </div>

            <div class="row m-2">

                <div class="col-md-12">
                    <h4>Share Hasil Karyamu Disini !!</h4>
                    <hr>
                </div>

                <form id="formaddpost" method="post"
                      data-parsley-validate>

                    <div class="row ml-4">

                        <div class="col-md-2 m-b-20">
                            @if( Auth::user()->photo === null)
                                <img class="d-flex mr-3 rounded-circle"
                                     src="/images/default.png" alt="Generic placeholder image" height="54">
                            @else
                                <img class="d-flex mr-3 rounded-circle"
                                     src="/images/photo/{{ Auth::user()->photo }}"
                                     alt="Generic placeholder image" height="54">
                            @endif
                        </div>


                        <div class="col-md-10 m-b-20">


                            <fieldset>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <input type="hidden" name="log" value="LOG_SHARE_POSTS">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea type="text" name="content" class="form-control" rows="4"
                                                  placeholder="Bagikan sesuatu..." required></textarea>
                                    </div>
                                </div>
                            </fieldset>

                        </div>


                        <div class="col-md-12">
                            <hr>
                            <div class="row">
                                <div class="col-md-10">
                                    <ul class="nav nav-pills profile-pills m-t-10">
                                        <li>
                                        </li>
                                        <li>
                                        </li>
                                        <li>
                                        </li>
                                    </ul>
                                </div>

                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-primary float-right" name="button">
                                        Share
                                    </button>
                                </div>

                            </div>
                        </div>
                    </div>

                </form>

            </div>
        </div>

        <div id="masterList" class="card mt-3">
            @foreach($masters as $master)

                @include('social-learnings.masterpiece.child.post-child')

                <div class="modal fade" id="modalDelete{{ $master->id }}">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="defaultModalLabel">
                                    Hapus {{ $master->status }} ?</h4>
                            </div>
                            <div class="modal-body">
                                Anda yakin ingin menghapus data ini?
                            </div>
                            <div class="modal-footer">
                                {!! Form::open(['route' => ['social-learning-posts.destroy', $_SERVER['HTTP_HOST'], $master->id], 'method' => 'DELETE']) !!}
                                <button type="button" class="btn btn-default"
                                        data-dismiss="modal"> Batal
                                </button>
                                <button type="submit" class="btn btn-danger"> Hapus</button>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Modal -->

            @endforeach
        </div>

    </div>

    <!-- akhir isi tengah -->
@endsection



@section('script')
    <script type="text/javascript">

        $(function () {

            $("form#formaddpost").submit(function (e) {
                e.preventDefault();
                var formData = new FormData(this);

                $.ajax({
                    url: '/social-learning-masterpiece',
                    type: 'POST',
                    data: formData,
                    success: function (data) {
                        {{--  $('#masterList').prepend(data);  --}}
                        $('#masterList').prepend(data);
                        $("form#formaddpost")[0].reset();
                        swal("Berhasil !", "Modul berhasil ditambahkan", "success");
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });

                return false;
            });

        });

        function onEditPost(postId) {
            $('#modalEdit' + postId).modal('hide');

            var formData = $('#formeditpost').serializeArray();
            console.log(formData);

            $.ajax({
                url: '/social-learning-masterpiece/' + postId,
                type: 'PUT',
                data: JSON.stringify(objectifyForm(formData)),
                success: function (data) {
                    $('#modalEdit' + postId).modal('hide');
                    $('#post' + postId).replaceWith(data);
                    swal("Berhasil !", "Post berhasil diedit", "success");
                },
                cache: false,
                contentType: false,
                processData: false
            });

            return false;

        }

        function onLike(postId) {
            var formData = new FormData();
            formData.append('log', 'LOG_LIKE_POSTS');
            formData.append('user_id', {{ Auth::id() }});

            $.ajax({
                url: '/posts-masterpieces/' + postId + '/likes',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#post' + postId).replaceWith(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onDislike(postId) {
            var formData = new FormData();
            formData.append('log', 'LOG_DISLIKE_POSTS');
            formData.append('user_id', {{ Auth::id() }});

            $.ajax({
                url: '/posts-masterpieces/' + postId + '/dislikes',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#post' + postId).replaceWith(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onComment(postId) {

            var formData = $('#formaddcomment').serializeArray();
            formData.log = 'LOG_COMMENTS';

            $.ajax({
                url: '/posts-masterpieces/' + postId + '/comments',
                type: 'POST',
                data: JSON.stringify(objectifyForm(formData)),
                success: function (data) {
                    $('#post' + postId).replaceWith(data);
                },
                error: function (data) {
                    console.log(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

            return false;

        }

        function onLikeComment(commentId, postId) {
            var formData = new FormData();
            formData.append('log', 'LOG_LIKE_COMMENTS');

            $.ajax({
                url: '/comments-masterpieces/' + commentId + '/likes',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#post' + postId).replaceWith(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onDislikeComment(commentId, postId) {
            var formData = new FormData();
            formData.append('log', 'LOG_DISLIKE_COMMENTS');

            $.ajax({
                url: '/comments-masterpieces/' + commentId + '/dislikes',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#post' + postId).replaceWith(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onDeletePost(postId) {

            swal({
                title: "Apa kamu yakin?",
                text: "Data yang telah di hapus tidak dapat dikembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {

                if (willDelete) {

                    $.ajax({
                        url: '/social-learning-masterpiece/' + postId,
                        type: 'DELETE',
                        data: null,
                        success: function (data) {
                            $('#post' + postId).remove();
                            swal("Berhasil !", "Kiriman berhasil dihapus", "success");
                        },
                        error: function (data) {
                            swal("Oops !", "Gagal menghapus kiriman", "error");
                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    });

                }

            });
        }

        function objectifyForm(formArray) {//serialize data function

            var returnArray = {};
            for (var i = 0; i < formArray.length; i++) {
                returnArray[formArray[i]['name']] = formArray[i]['value'];
            }
            return returnArray;
        }

    </script>
@endsection
