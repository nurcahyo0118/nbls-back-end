@extends('layouts.frontend')

@section('content')
    <div class="col-md-8 col-lg-6 footer-space">

        <div class="row">
            <div class="col-lg-12">

                @include('layouts.partials.message')

                <div class="search-result-box">
                    <ul class="nav nav-tabs text-center">
                        <li class="nav-item col-md-4 p-0 ">
                            <a href="#howto" data-toggle="tab" aria-expanded="true" class="nav-link active">
                                <b>How To</b>
                            </a>
                        </li>
                        <li class="nav-item col-md-4 p-0">
                            <a href="#home" data-toggle="tab" aria-expanded="false" class="nav-link">
                                <b>Store</b>
                            </a>
                        </li>
                        <li class="nav-item col-md-4 p-0">
                            <a href="#history" data-toggle="tab" aria-expanded="false" class="nav-link">
                                <b>History</b>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">

                        <div class="tab-pane active" id="howto">
                            <h5>Pilih Reward, Dapatkan Kursus, dapatkan juga 100 gems</h5>
                        </div>

                        <!-- start home -->
                        <div class="tab-pane" id="home">
                            <div class="row">
                                <div class="col-md-2">
                                    <!-- <img src="/images/users/avatar-2.jpg" class="rounded-circle mr-3" alt="friend"> -->
                                    <div class="media">
                                        @if( Auth::user()->photo === null)
                                            <img class="d-flex rounded-circle"
                                                 src="/images/default.png" alt="Generic placeholder image" height="75" width="75">
                                        @else
                                            <img class="d-flex rounded-circle"
                                                 src="/images/photo/{{ Auth::user()->photo }}"
                                                 alt="Generic placeholder image" height="75" width="75">
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h4>{{ Auth::user()->fullname }}</h4>
                                            <p class="text-muted">{{ Auth::user()->skill }}</p>
                                        </div>

                                        <div class="col-md-3 waves-effect" data-toggle="modal"
                                             data-target="#con-close-modal">
                                            <div class="row">
                                                <div class="col-4">
                                                    <h3 class="ion-social-usd-outline"></h3>
                                                </div>
                                                <div class="col-md-8">
                                                    <strong class="text-primary">{{ Auth::user()->points->sum('point') }}</strong>
                                                    <p>Point</p>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-3 waves-effect" data-toggle="modal"
                                             data-target="#con-close-modal">
                                            <div class="row">
                                                <div class="col-4">
                                                    <h3>
                                                        <i class="fa fa-diamond"></i>
                                                    </h3>
                                                </div>
                                                <div class="col-md-8">
                                                    <strong class="text-primary">{{ Auth::user()->gems->sum('gem') }}</strong>
                                                    <p>Gems</p>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <hr>
                                    <div class="m-b-10">
                                      <div>
                                          @foreach(Auth::user()->courses as $course)
                                              <span class="label label-primary">{{ $course->title }}</span>
                                          @endforeach
                                      </div>
                                    </div>
                                </div>

                                    <!-- All Masa Belanja -->
                                    <div class="col-md-12">
                                        <div class="">
                                            <div class="row m-1">
                                              @foreach($courses as $course)
                                                <!-- card buy -->
                                                <div class="col-md-4 border card-box text-center">
                                                    @if( Auth::user()->photo === null)
                                                        <img class="rounded-circle"
                                                             src="/images/default.png" alt="Generic placeholder image" height="75" width="75">
                                                    @else
                                                        <img class="rounded-circle"
                                                             src="data:image/jpeg;base64,{{ $course->image }}"
                                                             alt="Generic placeholder image" height="75" width="75">
                                                    @endif

                                                    <h5>{{ str_limit($course->title, 12) }}</h5>

                                                    <div class="row">

                                                        <div class="col-md-6">
                                                            <h3>
                                                                <i class="ion-social-usd-outline"></i>
                                                            </h3>
                                                            <p>10000 Point</p>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <h3>
                                                                <i class="fa fa-diamond"></i>
                                                            </h3>
                                                            <p>100 Gems</p>
                                                        </div>

                                                    </div>
                                                    <form id="formExchange{{ $course->id }}" action="{{ route('exchange-point', $course->id) }}" method="POST">
                                                        {{ csrf_field() }}
                                                        <button onclick="changePoint({{ $course->id }})" type="button" class="btn btn-primary col-md-12">Buy</button>
                                                    </form>
                                                </div>


                                            @endforeach
                                            </div>
                                        </div>
                                    </div>

                                <!-- end All Belanja -->

                            </div>
                        </div> 

                        <!-- Users tab -->
                        <div class="tab-pane" id="users">
                            <div class="search-item">

                            </div>

                        </div>
                        <!-- end Users tab -->

                        <!-- Start History -->
                        <div class="tab-pane" id="history">
                            <div class="row">
                                <div class="col-md-2">

                                </div>
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-3 col-md-3-offside">

                                        </div>
                                        <div class="col-md-6">
                                            <h4>My Points</h4>
                                        </div>

                                        <div class="col-md-3 ">
                                            <div class="row">
                                                <div class="col-4">
                                                    <h3 class="ion-social-usd-outline"></h3>
                                                </div>
                                                <div class="col-md-8">
                                                    <strong class="text-primary">{{ Auth::user()->points->sum('point') }}</strong>
                                                    <p>Point</p>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- start card -->
                            <div class="">

                                <table class="table table-striped"
                                       class="table table-striped table-bordered m-b-0 toggle-circle"
                                       data-page-size="7">
                                    <thead>
                                    <tr>
                                        <th>Item (Kursus)</th>
                                        <th>Tanggal</th>
                                        <th>Poin</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($exchange_histories as $history)
                                            <tr>
                                                <td>{{ $history->course->title }}</td>
                                                <td>{{ $history->exchange_date }}</td>
                                                <td>10.000</td>
                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>

                            <!-- end card -->


                        </div>

                        <!-- end history -->

                    </div>


                </div>
            </div>
        </div>

        <!-- Responsive modal -->
        <!-- <button type="button" class="btn btn-secondary waves-effect" data-toggle="modal" data-target="#con-close-modal">Responsive Modal</button> -->

    {{--<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
          aria-hidden="true" style="display: none;">
         <div class="modal-dialog">
             <div class="card">
                 <div class="text-center">
                     <h3>Change Point To Gems</h3>
                     <img class="rounded-circle thumb-md" src="/images/users/avatar-3.jpg"
                          alt="Generic placeholder image">
                     <h6><small class="text-center"> <span class="ion-social-usd-outline"> 1000</span> = <span class="fa fa-diamond"> 1</span> </small></h6>
                     <h5>Are You Sure Want to Change Points Items?</h5>

                     <p class="mx-5">Quantity Points
                       <select class="form-control" name="">
                         <option value="1000">1000</option>
                         <option value="2000">2000</option>
                         <option value="5000">5000</option>
                       </select>
                     </p>

                     <!-- <button  type="sumbit" class="btn btn-primary" name="button">Buy</button> -->
                     <!-- </div> -->
                     <div class="m-b-10">
                         <button type="button" class="btn btn-info waves-effect waves-light">Points To Gems</button>
                         <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">CANCEL
                         </button>
                     </div>
                 </div>
             </div>
         </div>
     </div>--}}
    <!-- /.modal -->


    </div>
@endsection

@section('script')
    <script>
        function changePoint(courseId) {
            swal({
                title: "Apa kamu yakin?",
                text: "Setelah menukar point tidak dapat dikembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: false,
            }).then((willSubmit) => {
                if (willSubmit) {
                    $('form#formExchange' + courseId).submit();
                }
            });

            {{--  $.ajax({
                url: '/exchange-point/' + courseId,
                type: 'POST',
                data: null,
                success: function (data) {
                    swal("Berhasil !", "Berhasil menukar point", "success");
                },
                error: function (data) {
                    console.log(data);
                    swal("Gagal !", "Point tidak cukup", "error");
                },
                cache: false,
                contentType: false,
                processData: false
            });  --}}
        }
    </script>
@endsection
