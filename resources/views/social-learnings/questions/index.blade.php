@extends('layouts.frontend')

@section('content')
    <div class="col-md-8 col-lg-6 footer-space">
        <div id="carouselExampleIndicators" class="carousel slide d-none d-sm-block d-sm-none d-md-block"
             data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="/images/nbls-profile-header.JPG" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="/images/nbls-profile-header.JPG" alt="Second slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="/images/nbls-profile-header.JPG" alt="Third slide">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="search-result-box">
                    <ul class="nav nav-tabs">
                        <li class="nav-item col-md-4 p-0 ">
                            <a href="#home" data-toggle="tab" aria-expanded="true" class="nav-link active">
                                <p class="fa fa-comments-o fa-2x"></p>
                                <b>Status</b>
                            </a>
                        </li>
                        <li class="nav-item col-md-4 p-0">
                            <a href="#users" data-toggle="tab" aria-expanded="false" class="nav-link">
                                <p class="fa fa-question fa-2x"></p>
                                <b>Tanya</b>
                            </a>
                        </li>
                        <li class="nav-item col-md-4 p-0">
                            <a href="#other-tab" data-toggle="tab" aria-expanded="false" class="nav-link">
                                <p class="fa fa-sitemap fa-2x"></p>
                                <b>Hasil Karya</b>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="home">

                            <form id="formaddpost" method="post"
                                  data-parsley-validate>

                                <div class="row">

                                    <div class="col-md-12">
                                        <h4>Whats Happening?</h4>
                                        <hr>
                                    </div>

                                    <div class="col-md-2 m-b-20">
                                        @if( Auth::user()->photo === null)
                                            <img class="d-flex mr-3 rounded-circle"
                                                 src="/images/default.png" alt="Generic placeholder image" height="54">
                                        @else
                                            <img class="d-flex mr-3 rounded-circle"
                                                 src="/images/photo/{{ Auth::user()->photo }}"
                                                 alt="Generic placeholder image" height="54">
                                        @endif
                                    </div>


                                    <div class="col-md-10 m-b-20">


                                        <fieldset>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                            <input type="hidden" name="log" value="LOG_SHARE_POSTS">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <textarea type="text" name="content" class="form-control" rows="4"
                                                              placeholder="Bagikan sesuatu..." required></textarea>
                                                </div>
                                            </div>
                                        </fieldset>

                                    </div>


                                    <div class="col-md-12">
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-10">
                                                <ul class="nav nav-pills profile-pills m-t-10">
                                                    <li>
                                                        <a data-toggle="tooltip" data-placement="top"
                                                           title="choose photos"
                                                           href="/social-learning-posts">
                                                            <i class="fa fa-camera"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a data-toggle="tooltip" data-placement="top"
                                                           title="flip max 50 kb"
                                                           href="/social-learning-posts">
                                                            <i class="fa fa-paperclip"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a data-toggle="tooltip" data-placement="top" title="link"
                                                           href="/social-learning-posts">
                                                            <i class="fa fa-link"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div class="col-md-2">
                                                <button type="submit" class="btn btn-primary float-right" name="button">
                                                    Share
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </form>

                        </div>

                        <!-- end All results tab -->


                        <!-- Users tab -->
                        <div class="tab-pane" id="users">

                            <form id="formaddpost" method="post"
                                  data-parsley-validate>

                                <div class="row">

                                    <div class="col-md-12">
                                        <h4>Any Question?</h4>
                                        <hr>
                                    </div>

                                    <div class="col-md-2 m-b-20">
                                        @if( Auth::user()->photo === null)
                                            <img class="d-flex mr-3 rounded-circle"
                                                 src="/images/default.png" alt="Generic placeholder image" height="54">
                                        @else
                                            <img class="d-flex mr-3 rounded-circle"
                                                 src="/images/photo/{{ Auth::user()->photo }}"
                                                 alt="Generic placeholder image" height="54">
                                        @endif
                                    </div>


                                    <div class="col-md-10 m-b-20">


                                        <fieldset>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                        <textarea type="text" name="content" class="form-control"
                                                                  rows="4"
                                                                  placeholder="Share" required></textarea>
                                                </div>
                                            </div>
                                        </fieldset>

                                    </div>


                                    <div class="col-md-12">
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-10">
                                                <ul class="nav nav-pills profile-pills m-t-10">
                                                    <li>
                                                        <a data-toggle="tooltip" data-placement="top"
                                                           title="choose photos"
                                                           href="#">
                                                            <i class="fa fa-camera"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a data-toggle="tooltip" data-placement="top"
                                                           title="flip max 50 kb"
                                                           href="#">
                                                            <i class="fa fa-paperclip"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a data-toggle="tooltip" data-placement="top" title="link"
                                                           href="#">
                                                            <i class="fa fa-link"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-2">
                                                <button type="submit" class="btn btn-primary float-right" name="button">
                                                    SEND
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </form>

                        </div>
                        <!-- end Users tab -->

                        <div class="tab-pane" id="other-tab">
                            <p>
                                Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo
                                rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis
                                pretium.
                                Integer tincidunt.Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend
                                tellus.
                                Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.
                            </p>

                        </div>

                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-12">
                <div class="search-result-box">
                    <ul class="nav nav-tabs">
                        <li class="nav-item col-md-6 p-0">
                            <a href="#timeline" data-toggle="tab" aria-expanded="true" class="nav-link active">
                                <b>TimeLine</b>
                            </a>
                        </li>
                        <li class="nav-item col-md-6 p-0">
                            <a href="#newquestion" data-toggle="tab" aria-expanded="false" class="nav-link">
                                <b>NewQuestion</b>
                            </a>
                        </li>

                    </ul>

                    <!-- timeline -->

                    <div class="tab-content">
                        <div class="tab-pane active" id="timeline">
                            <div id="postList" class="row">

                                @foreach($posts as $post)

                                    @include('social-learnings.posts.child.post-child')

                                    <div class="modal fade" id="modalDelete{{ $post->id }}">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="defaultModalLabel">
                                                        Hapus {{ $post->status }} ?</h4>
                                                </div>
                                                <div class="modal-body">
                                                    Anda yakin ingin menghapus data ini?
                                                </div>
                                                <div class="modal-footer">
                                                    {!! Form::open(['route' => ['social-learning-posts.destroy', $_SERVER['HTTP_HOST'], $post->id], 'method' => 'DELETE']) !!}
                                                    <button type="button" class="btn btn-default"
                                                            data-dismiss="modal"> Batal
                                                    </button>
                                                    <button type="submit" class="btn btn-danger"> Hapus</button>
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Modal -->

                                @endforeach


                            </div>

                        </div>

                        <!-- end timelines tab -->
                        <div class="tab-pane active" id="newquestion"></div>

                    </div>

                </div>
            </div>
        </div>
        @include('layouts.partials-social-learning.follow-suggested')

    </div>

@endsection

@section('script')
    <script type="text/javascript">

        $(function () {

            $("form#formaddpost").submit(function (e) {
                e.preventDefault();
                var formData = new FormData(this);

                $.ajax({
                    url: '/social-learning-posts',
                    type: 'POST',
                    data: formData,
                    success: function (data) {
                        $('#postList').prepend(data);
                        $("form#formaddpost")[0].reset();
                        swal("Berhasil !", "Modul berhasil ditambahkan", "success");
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });

                return false;
            });

        });

        function onEditPost(postId) {
            $('#modalEdit' + postId).modal('hide');

            var formData = $('#formeditpost').serializeArray();
            console.log(formData);

            $.ajax({
                url: '/social-learning-posts/' + postId,
                type: 'PUT',
                data: JSON.stringify(objectifyForm(formData)),
                success: function (data) {
                    $('#modalEdit' + postId).modal('hide');
                    $('#post' + postId).replaceWith(data);
                    swal("Berhasil !", "Post berhasil diedit", "success");
                },
                cache: false,
                contentType: false,
                processData: false
            });

            return false;

        }

        function onLike(postId) {
            var formData = new FormData();
            formData.append('log', 'LOG_LIKE_POSTS');

            $.ajax({
                url: '/posts/' + postId + '/likes',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#post' + postId).replaceWith(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onDislike(postId) {
            var formData = new FormData();
            formData.append('log', 'LOG_DISLIKE_POSTS');

            $.ajax({
                url: '/posts/' + postId + '/dislikes',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#post' + postId).replaceWith(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onComment(postId) {

            var formData = $('#formaddcomment').serializeArray();
            formData.log = 'LOG_COMMENTS';

            $.ajax({
                url: '/posts/' + postId + '/comments',
                type: 'POST',
                data: JSON.stringify(objectifyForm(formData)),
                success: function (data) {
                    $('#post' + postId).replaceWith(data);
                },
                error: function (data) {
                    console.log(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

            return false;

        }

        function onLikeComment(commentId, postId) {
            var formData = new FormData();
            formData.append('log', 'LOG_LIKE_COMMENTS');

            $.ajax({
                url: '/comments/' + commentId + '/likes',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#post' + postId).replaceWith(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onDislikeComment(commentId, postId) {
            var formData = new FormData();
            formData.append('log', 'LOG_DISLIKE_COMMENTS');

            $.ajax({
                url: '/comments/' + commentId + '/dislikes',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#post' + postId).replaceWith(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onDeletePost(postId) {

            swal({
                title: "Apa kamu yakin?",
                text: "Data yang telah di hapus tidak dapat dikembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {

                if (willDelete) {

                    $.ajax({
                        url: '/social-learning-posts/' + postId,
                        type: 'DELETE',
                        data: null,
                        success: function (data) {
                            $('#post' + postId).remove();
                            swal("Berhasil !", "Kiriman berhasil dihapus", "success");
                        },
                        error: function (data) {
                            swal("Oops !", "Gagal menghapus kiriman", "error");
                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    });

                }

            });
        }

        function objectifyForm(formArray) {//serialize data function

            var returnArray = {};
            for (var i = 0; i < formArray.length; i++) {
                returnArray[formArray[i]['name']] = formArray[i]['value'];
            }
            return returnArray;
        }

    </script>
@endsection
