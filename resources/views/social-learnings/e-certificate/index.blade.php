@extends('layouts.frontend')

@section('style')
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
@endsection

@section('content')
    <div class="col-md-8 col-lg-6 footer-space">

        <h6 class="text-muted">
            <span class="fa fa-home"></span>
            Courses
            <span class="fa fa-long-arrow-right"></span>
            E Certificate
        </h6>

        <div class="card">
            <div class="card-header">
                <h4>Cerfiticate</h4>
            </div> <!-- end card header -->
            <div class="card-body">
                <!-- <div class=""> -->
                <table class="table table-striped">
                    <thead class="bg-info">
                    <tr>
                        <th class="text-center text-white">No</th>
                        <th class="text-center text-white">Kursus</th>
                        <th class="text-center text-white">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                        $no=1;
                    @endphp

                    @foreach(Auth::user()->courses as $course)

                        @php
                            $encounter = App\Encounter::where('course_id', $course->id)->first();

                            $is_submitted = null;

                            if($encounter != null) {
                                $is_submitted = DB::table('user_encounters')->where([
                                    'user_id' => Auth::id(), 
                                    'encounter_id' => $encounter->id
                                ])->first();
                            }

                        @endphp

                        @if($is_submitted != null)
                            <tr>
                                <th scope="row">{{$no++}}</th>
                                <td>
                                    <div class="row">
                                        <div class="col-md-4 col-xs-12">
                                            <img height="50" src="/images/order/web_design.jpg" alt="">
                                        </div>
                                        <div class="col-md-8 col-xs-12 text-left">
                                            <span>{{ $course->title }}</span>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <a target="_blank"
                                            href="{{ route('social-learning-e-certificate.preview', $course->id) }}"
                                            class="btn btn-info btn-sm" style="margin-right:10px"
                                            data-original-title="Preview">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <a target="_blank" 
                                            href="{{ route('social-learning-e-certificate.show', $course->id) }}"
                                            class="btn btn-success btn-sm tooltips" data-placement="bottom"
                                            data-toggle="tooltip" data-original-title="Download">
                                            <i class="fa fa-download"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endif

                    @endforeach
                    </tbody>
                </table>
                <!-- </div> -->

            </div>
        </div> <!-- end card -->
    </div>


    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">E Certificate</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img src="/images/order/web_design.jpg" class="img-fluid" alt="">
                </div>
                <!-- <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
                </div> -->
            </div>
        </div>
    </div>
@endsection

@section('script')


@endsection
