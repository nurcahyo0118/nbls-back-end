<title>Sertifikat</title>
<style type="text/css">

    {{--  @page {
		size: landscape;
	}  --}}

#frame {
        -webkit-print-color-adjust: exact;
    }

    #title {
        padding: 5px;
        text-align: center;
        font-style: normal;
    }

    #title h5 {
        text-align: center;
        font-family: "Comic Sans MS", cursive;
    }

    #title h2 {
        font-size: 80px;
        margin-top: -20px;
    }

    #title p {
        font-size: 18px;
        margin-top: -50px;
    }

    #kursus {
        margin-left: 150px;
        font-family: Georgia, "Times New Roman", Times, serif;
        font-size: 24px;
        width: 1000px;
        margin-top: -70px;
    }

    #ttd {
        margin-left: 750px;
        font-family: Georgia, "Times New Roman", Times, serif;
        font-size: 24px;
        width: 300px;
        text-align: center;
        margin-top: 30px;
    }

    left {
        float: right;
        display: inline-table;
    }
</style>
</head>

<body>
<div id="frame"
     style="width: 1280px; height: 888px; background-image: url(/images/photo/{{ $certificate->template }}); background-size: 1280px 888px; background-repeat:no-repeat; font-size: 36px; font-family: Georgia, 'Times New Roman', Times, serif;">
    <div style="width:200px; height:100px;">
        {{--  @if( Auth::user()->tenant->company_logo === null)

        @else
            <img src="/images/photo/tenant/{{ Auth::user()->tenant->company_logo }}" width="100px" height="100px"
                 style="margin-left:170px; margin-top:90px;">

        @endif  --}}
    </div>
    <br>
    <br>
    <br>
    <div id="title">

        <h5>
            <div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;display: none;">
            </div>
        </h5>
        <h2> Certificate</h2>
        <p>for succesfully completing the course with grade <br> is presented to: </p>
        <h4>{{ Auth::user()->fullname }}</h4>
        <p>{{ $course->title }}</p>

    </div>

    <div id="ttd">
        <p><strong>Issued by :</strong></p>
        <p>
        @if(Auth::user()->tenant->company_logo === null)
            <p style="margin-top:70px"></p>
        @else
            <img src="/images/photo/{{ $certificate->signature }}" width="100px" height="100px">
        @endif
        </p>
        {{--  @php
            $ku = Auth::user()->courses->first()
        @endphp  --}}
        <p><h5>{{ $course->author->fullname }}</h5></p>

        <p style="margin-top:-30px">Instruktur</p>
    </div>
</div>
</body>
<body>
</html>
