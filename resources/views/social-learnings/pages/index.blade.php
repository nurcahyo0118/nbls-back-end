@extends('social-learnings.course-front-end.main')

@section('content')
    <div class="container">
        <br>
        <br>

        {{--  <i><h5>Category/chatbot</h5></i>  --}}
        <br>
        <h1>{{ $page->name }}</h1>
        <i><h5>{!! $page->subtitle !!}</h5></i>
        <br>

        <div class="media">
            <a href="{{ route('social-learning-posts.show', $page->user->username) }}">
                @if($page->user->photo === null)
                    <img class="d-flex mr-3 rounded-circle thumb-sm"
                         src="/images/default.png" alt="Generic placeholder image">
                @else
                    <img class="d-flex mr-3 rounded-circle thumb-sm"
                         src="/images/photo/{{ $page->user->photo }}" alt="Generic placeholder image">
                @endif
            </a>

            <div class="media-body">

                <h4 class="text-primary font-16 m-1">
                    <a href="{{ route('social-learning-posts.show', $page->user->username) }}" class="m-2">
                        {{ $page->user->fullname }}
                    </a>
                    {{--  <br>  --}}
                    {{--  <small class="text-muted m-2">
                        <i class="ion-clock"></i>
                        14 minute ago
                    </small>  --}}
                </h4>

            </div>
        </div>


        <br>
        <p>{!! $page->content !!}</p>
         
        {{--  <hr>
        <div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button"
             data-size="small" data-mobile-iframe="true">
            <a target="_blank"
               href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse"
               class="fb-xfbml-parse-ignore">Bagikan
            </a>
        </div>  --}}
        {{--  <div id="fb-root"></div>  --}}
        {{--  <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s);
                js.id = id;
                js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.12';
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>  --}}
    </div>
    <br>

@endsection
