@extends('layouts.frontend')

@section('content')
    <!-- isi tengah -->

    <div class="col-md-8 col-lg-6 footer-space">
        <h6 class="text-muted">
            <span class="fa fa-home"></span>
            Courses
            <span class="fa fa-long-arrow-right"></span>
            My Performance
        </h6>

        <!-- card  -->
        @foreach(Auth::user()->courses as $course)

            @php
                $answer = null;
                $encounterAssessments = [];
                $materials = [];
                $exercises = [];
                $quizzes = [];
                $encounterAssessmentValue = 0;
                $exerciseAssessmentValue = 0;
                $encounter = App\Encounter::where('course_id', $course->id)->first();

                if($encounter != null)
                {
                    $answer = $encounter->submittedEncounters()->where(['user_id' => Auth::id()])->first();

                    $encounterAssessments = DB::table('encounter_assessments')
                        ->where('user_id', Auth::id())
                        ->where('encounter_id', $encounter->id)
                        ->get();
                    
                    $student_value = 0;
                    $student_values = DB::table('encounter_assessments')->where('encounter_id', $encounter->id)->where('user_id', Auth::id())->get();

                    foreach($student_values as $sv)
                    {
                        $value_criteria = App\Criteria::find($sv->criteria_id);

                        $student_value += ($sv->value) * ($value_criteria->weight / 100);
                    }

                    $encounterAssessmentValue += $student_value;
                }

                foreach($course->sections as $section) {
                    foreach($section->materials as $material)
                    {
                        array_push($materials, $material->id);

                        foreach($material->quizzes as $quiz)
                        {
                            array_push($quizzes, $quiz->id);
                        }

                    }
                }

                foreach($course->sections as $section) {
                    foreach($section->exercises as $exercise)
                    {
                        array_push($exercises, $exercise->id);
                    }
                }

                $videoPerformance = DB::table('viewed_videos')
                    ->where('user_id', Auth::id())
                    ->whereIn('material_id', $materials)
                    ->get();


                $exercisePerformance = DB::table('user_exercises')
                    ->where('user_id', Auth::id())
                    ->whereIn('exercise_id', $exercises)
                    ->get();

                $quizPerformance = DB::table('user_quizzes')
                    ->where('user_id', Auth::id())
                    ->whereIn('quiz_id', $quizzes)
                    ->get();

                if(collect($materials)->count() < 1)
                {
                    $videoProgress = 0;
                }
                else
                {
                    $videoProgress = $videoPerformance->count() / collect($materials)->count() * 100;
                }

                if(collect($exercises)->count() < 1 && collect($quizzes)->count() < 1)
                {
                    $exerciseProgress = 0;
                }
                else
                {
                    $exerciseProgress = ($exercisePerformance->count() + $quizPerformance->count()) / (collect($exercises)->count() + collect($quizzes)->count()) * 100;
                }

                $encounterProgress = $answer != null ? 1 * 100 : 0;

                $overallProgress = ($videoProgress * (30 / 100)) + ($exerciseProgress * (20 / 100)) + ($encounterProgress * (50 / 100));

                foreach($exercises as $exercise)
                {
                    $student_value = 0;
                    $student_values = DB::table('exercise_assessments')->where('exercise_id', $exercise)->where('user_id', Auth::id())->get();

                    foreach($student_values as $sv)
                    {
                        $value_criteria = App\ExerciseCriteria::find($sv->criteria_id);

                        $student_value += ($sv->value) * ($value_criteria->weight / 100);
                    }

                    $exerciseAssessmentValue += $student_value;
                }

            @endphp

            <div class="card mb-2">
                <a href="{{ route('social-learning-my-course.show', [$_SERVER['HTTP_HOST'], $course->id]) }}">
                    <div class="card-header bg-info">
                        <strong class="text-white">{{ $course->title }}</strong>
                        <a class="btn btn-danger btn-sm ml-3">
                            <i class="fa fa-trophy"></i>
                            Semangat
                        </a>
                    </div>
                </a>

                <div class="card-body" id="isi">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="contact-card">
                                <a class="pull-left" href="{{ route('social-learning-my-course.show', [$_SERVER['HTTP_HOST'], $course->id]) }}">
                                    @if($course->image === null)
                                        <img class="rounded" src="/images/order/web_design.jpg" alt="">
                                    @else
                                        <img class="rounded" src="/xxcourses/images/{{ $course->image }}" alt="">
                                    @endif
                                </a>
                                <div class="member-info">
                                    <p class="text-muted">
                                        <i class="ion-clock"></i> {{ $course->updated_at->diffForHumans() }} </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <h2>{{ number_format($overallProgress, 2, '.', '') }}%</h2>
                        </div>

                        <div class="col-md-4 mt-2 text-center">
                            <div class="progress">
                                <div class="progress-bar progress-bar-custom progress-bar-striped" role="progressbar"
                                     aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"
                                     style="width: {{ $videoProgress }}%;">
                                </div>
                            </div>
                            <p>Video : {{ $videoPerformance->count() }} / {{ collect($materials)->count() }}</p>
                        </div>

                        <div class="col-md-4 mt-2 text-center">
                            <div class="progress">
                                <div class="progress-bar progress-bar-custom progress-bar-striped" role="progressbar"
                                     aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"
                                     style="width: {{ $exerciseProgress }}%;">
                                    <span class="sr-only">95% Complete</span>
                                </div>
                            </div>
                            <p>Latihan : {{ $quizPerformance->count() + $exercisePerformance->count() }} / {{ collect($exercises)->count() + collect($quizzes)->count() }}</p>
                        </div>

                        <div class="col-md-4 mt-2 text-center">
                            <div class="progress">
                                <div class="progress-bar progress-bar-custom progress-bar-striped" role="progressbar"
                                     aria-valuenow="0" aria-valuemin="0" aria-valuemax="0" style="width: {{ $encounterProgress }}%;">
                                </div>
                            </div>
                            <p>Encounter : 1</p>

                        </div>

                        <div class="col-md-12">
                            <hr>
                        </div>

                        <div class="col-md-6 mt-2 text-center">
                            <div class="progress">
                                <div class="progress-bar progress-bar-custom progress-bar-striped" role="progressbar"
                                        aria-valuenow="0" aria-valuemin="0" aria-valuemax="0" style="width: {{ number_format($exerciseAssessmentValue, 0, '.', '') }}%;">
                                </div>
                            </div>
                            <p>Nilai Latihan : {{ $exerciseAssessmentValue != 0 ? number_format($exerciseAssessmentValue, 0, '.', '') : 'Belum Dinilai' }}</p>

                        </div>

                        <div class="col-md-6 mt-2 text-center">
                            <div class="progress">
                                <div class="progress-bar progress-bar-custom progress-bar-striped" role="progressbar"
                                        aria-valuenow="0" aria-valuemin="0" aria-valuemax="0" style="width: {{ number_format($encounterAssessmentValue, 0, '.', '') }}%;">
                                </div>
                            </div>
                            <p>Nilai Encounter : {{ $encounterAssessmentValue != 0 ? number_format($encounterAssessmentValue, 0, '.', '') : 'Belum Dinilai' }}</p>

                        </div>
                    </div>
                </div>
            </div>
    @endforeach
    <!-- end card -->


    </div>
    <!-- akhir isi tengah -->
@endsection
