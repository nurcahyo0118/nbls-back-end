@extends('layouts.frontend')

@section('content')
    <!-- isi tengah -->

    <div class="col-md-8 col-lg-6">
        <h6 class="text-muted">
            <span class="fa fa-home"></span>
            Courses
            <span class="fa fa-long-arrow-right"></span>
            My Performance
        </h6>
        <!-- card  -->
        <div class="card mb-2">
            <a href="#isi">
                <div class="card-header bg-info">
                    <strong class="text-white">PHP Framwork CodeIgniter 2017</strong>
                    <a href="#" class="btn btn-danger btn-sm ml-3">
                        <i class="fa fa-trophy"></i>
                        Detail
                    </a>
                </div>
            </a>
            <div class="card-body" id="isi">
                <div class="row">
                    <div class="col-md-8">
                        <div class="contact-card">
                            <a class="pull-left" href="#">
                                <img class="rounded" src="/images/order/web_design.jpg" alt="">
                            </a>
                            <div class="member-info">
                                <p class="text-muted">PHP Framwork CodeIgniter 2017</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h1>2%</h1>
                    </div>

                    <div class="col-md-4 mt-2 text-center">
                        <div class="progress">
                            <div class="progress-bar progress-bar-custom progress-bar-striped" role="progressbar"
                                 aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" style="width: 95%;">
                                <span class="sr-only">95% Complete</span>
                            </div>
                        </div>
                        <p class="">Vidio : 2/25</p>
                    </div>

                    <div class="col-md-4 mt-2 text-center">
                        <div class="progress">
                            <div class="progress-bar progress-bar-custom progress-bar-striped" role="progressbar"
                                 aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" style="width: 95%;">
                                <span class="sr-only">95% Complete</span>
                            </div>
                        </div>
                        <p class="">Latihan : 2/25
                            <i class="text-muted">rata-rata</i>
                        </p>
                    </div>

                    <div class="col-md-4 mt-2 text-center">
                        <div class="progress">
                            <div class="progress-bar progress-bar-custom progress-bar-striped" role="progressbar"
                                 aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" style="width: 95%;">
                                <span class="sr-only">95% Complete</span>
                            </div>
                        </div>
                        <p class="">Encaouter : -</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- end card -->

        <!-- card  -->
        <div class="card mb-2">
            <a href="#isi">
                <div class="card-header bg-info">
                    <strong class="text-white">PHP Framwork CodeIgniter 2017</strong>
                    <a href="#" class="btn btn-danger btn-sm ml-3">
                        <i class="fa fa-trophy"></i>
                        Detail
                    </a>
                </div>
            </a>
            <div class="card-body" id="isi">
                <div class="row">
                    <div class="col-md-8">
                        <div class="contact-card">
                            <a class="pull-left" href="#">
                                <img class="rounded" src="/images/order/web_design.jpg" alt="">
                            </a>
                            <div class="member-info">
                                <p class="text-muted">PHP Framwork CodeIgniter 2017</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h1>2%</h1>
                    </div>

                    <div class="col-md-4 mt-2 text-center">
                        <div class="progress">
                            <div class="progress-bar progress-bar-custom progress-bar-striped" role="progressbar"
                                 aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" style="width: 95%;">
                                <span class="sr-only">95% Complete</span>
                            </div>
                        </div>
                        <p class="">Vidio : 2/25</p>
                    </div>

                    <div class="col-md-4 mt-2 text-center">
                        <div class="progress">
                            <div class="progress-bar progress-bar-custom progress-bar-striped" role="progressbar"
                                 aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" style="width: 95%;">
                                <span class="sr-only">95% Complete</span>
                            </div>
                        </div>
                        <p class="">Latihan : 2/25
                            <i class="text-muted">rata-rata</i>
                        </p>
                    </div>

                    <div class="col-md-4 mt-2 text-center">
                        <div class="progress">
                            <div class="progress-bar progress-bar-custom progress-bar-striped" role="progressbar"
                                 aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" style="width: 95%;">
                                <span class="sr-only">95% Complete</span>
                            </div>
                        </div>
                        <p class="">Encaouter : -</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- end card -->

    </div>


    <!-- akhir isi tengah -->
@endsection
