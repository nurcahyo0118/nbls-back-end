@extends('layouts.frontend')

@section('content')
    <!-- isi tengah -->

    <div class="col-md-8 col-lg-6 footer-space">

        <h6 class="text-muted">
            <span class="fa fa-home"></span>
            Courses
            <span class="fa fa-long-arrow-right"></span>
            My Courses
        </h6>

        <div class="row">
            <div class="col-md-2 text-muted">
                Filter by
            </div>

            <div class="col-md-5">
                <div class="form-group">
                    <select class="form-control" id="exampleFormControlSelect1">
                        <option>All</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>
                </div>

            </div>

            <div class="col-md-5">
                <div class="input-group">
                    <input id="" type="text" class="form-control" placeholder="Search">
                    <span class="btn btn-primary">
                        <i class="fa fa-search"></i>
                    </span>
                </div>
            </div>

        </div>

        <div class="row">

            @foreach(Auth::user()->courses as $course)
                @php
                    $materials = [];
                    $exercises = [];
                    
                    foreach($course->sections as $section) {
                        foreach($section->materials as $material)
                        {
                            array_push($materials, $material->id);
                        }
                    }

                    foreach($course->sections as $section) {
                        foreach($section->exercises as $exercise)
                        {
                            array_push($exercises, $exercise->id);
                        }
                    }

                    $videoPerformance = DB::table('viewed_videos')
                        ->where('user_id', Auth::id())
                        ->whereIn('material_id', $materials)
                        ->get();


                    $exercisePerformance = DB::table('viewed_exercises')
                        ->where('user_id', Auth::id())
                        ->whereIn('exercise_id', $exercises)
                        ->get();

                    {{-- $videoProgress = $videoPerformance->count() / collect($materials)->count() * 100;
                    $exerciseProgress = $exercisePerformance->count() / collect($exercises)->count() * 100;
                    $overallProgress = ($videoPerformance->count() + $exercisePerformance->count() + 0) / (collect($materials)->count() + collect($exercises)->count() + 1) * 100; --}}

                @endphp

                <div class="col-md-6 m-t-20">
                    <div class="card">
                        @if($course->image === null)
                            <img class="card-img-top" src="/images/order/web_design.jpg" alt="Card image cap">
                        @else
                            <img class="card-img-top" src="/xxcourses/images/{{ $course->image }}" alt="Card image cap">
                        @endif
                        <div class="card-body">
                            <p class="text-muted">
                                <small>By {{ $course->author->fullname }} |
                                    <i class="ion-clock"></i> {{ $course->updated_at->diffForHumans() }}
                                </small>
                            </p>
                            <h6><strong>{{ $course->title }}</strong></h6>
                            <p>
                                <i class="fa fa-users"> {{ $course->users->count() }} Siswa</i>
                            </p>
                            <hr>
                            {{--  <p>Sisa Waktu Belajar : 300 hari</p>  --}}
                            {{-- <div class="progress">
                                <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuemin="0"
                                     aria-valuemax="100" style="width: {{ $overallProgress }}%;">
                                </div>
                            </div> --}}
                        </div>

                        <div class="card-footer text-center btn-primary btn-block" style="cursor: pointer;">
                            @if($course->encounters->count() != 0)
                                <a class="text-white"
                                href="{{ route('social-learning-exam.show', [$_SERVER['HTTP_HOST'], $course->id]) }}">
                                    VIEW DETAILS
                                    <i class="md-arrow-forward"></i>
                                </a>
                            @else
                                <a class="text-white"
                                    onclick="swal('Peringatan !', 'Ujian tidak tersedia', 'warning');">
                                    VIEW DETAILS
                                    <i class="md-arrow-forward"></i>
                                </a>
                            @endif
                        </div>
                    </div>
                </div>

            @endforeach

        </div>


    </div>

    <!-- akhir isi tengah -->
@endsection
