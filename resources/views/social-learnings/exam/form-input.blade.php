@extends('layouts.frontend')

@section('content')
    <!-- isi tengah -->

    <div class="col-md-8 col-lg-6 footer-space">

        <h6 class="text-muted">
            <span class="fa fa-home"></span>
            Courses
            <span class="fa fa-long-arrow-right"></span>
            Ujian
            <span class="fa fa-long-arrow-right"></span>
            Details
        </h6>

        <div class="card">
            {{-- {{ $encounter }} --}}
            @if(DB::table('user_encounters')->where(['encounter_id' => $encounter->id, 'user_id' => Auth::id()])->first() != null)
                <div class="card-body">
                    <h3 class="text-center">Anda sudah mengirim jawaban !</h3>
                </div>
            @else
                <div class="card-body">

                    <label></label>Soal Ujian : </label>
                    <br>
                    <p>{{ $encounter->title }}</p>
                    <a href="/encounter-file/{{ $encounter->file }}" class="btn btn-default" download>
                        <i class="fa fa-download"></i>
                        Unduh Soal Ujian
                    </a>
                    <hr>

                    <form 
                        action="{{ route('social-learning-my-course.encounter.store', [$_SERVER['HTTP_HOST'], $encounter->id]) }}" 
                        method="POST"
                        enctype="multipart/form-data">

                        <label>Unggah Jawaban : </label>
                        <br>

                        {{ csrf_field() }}

                        <input type="hidden" name="log" value="LOG_ANSWER_ENCOUNTER">

                        <div class="form-group">
                            <label> Tipe Kursus : </label>
                            <input type="radio" name="is_file" value="1" onclick="onSelectFile()" checked>
                            File
                            <input type="radio" name="is_file" value="0" onclick="onSelectLink()">
                            Link
                        </div>

                        <input id="encounterFileInput" type="file" name="file">

                        <div id="linkFormGroup" class="from-group">
                            <label>Link Jawaban : </label>
                            <input id="encounterLinkInput" type="url" name="link" class="form-control">
                        </div>
                            
                        <br>
                        <br>

                        <button type="submit" class="btn btn-primary pull-right">
                            <i class="fa fa-paper-plane"></i>
                            Kirim
                        </button>
                    </form>

                </div>
            @endif
            
        </div>
    </div>

    <!-- akhir isi tengah -->
@endsection

@section('script')
    <script>
        onSelectFile();

        function onSelectFile() {
            document.getElementById('linkFormGroup').style.display = 'none';
            document.getElementById('encounterLinkInput').style.display = 'none';
            document.getElementById('encounterFileInput').style.display = 'block';
            document.getElementById('encounterFileInput').required = true;
            document.getElementById('encounterLinkInput').required = false;
        }

        function onSelectLink() {
            document.getElementById('linkFormGroup').style.display = 'block';
            document.getElementById('encounterFileInput').style.display = 'none';
            document.getElementById('encounterLinkInput').style.display = 'block';
            document.getElementById('encounterLinkInput').required = true;
            document.getElementById('encounterFileInput').required = false;
        }
    </script>
@endsection