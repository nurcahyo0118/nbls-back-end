@extends('layouts.frontend')

@section('content')
    <!-- isi tengah -->

    <div class="col-md-8 col-lg-6 footer-space">

        <h6 class="text-muted">
            <span class="fa fa-home"></span>
            Courses
            <span class="fa fa-long-arrow-right"></span>
            Ujian
            <span class="fa fa-long-arrow-right"></span>
            Details
        </h6>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/cwEp7hamCoo" frameborder="0"
                allow="autoplay; encrypted-media" allowfullscreen></iframe>

        <p class="mt-2"><strong>
                Syarat Mengikuti Encounter {{ $course->title }}:<br><br>
                1. Telah Melihat seluruh materi video.
                {{--  <a class="float-right"> 0/32</a>  --}}
                <br>
                2. Telah menyelesaikan seluruh materi latihan.
                {{--  <a class="float-right"> 0/10</a>  --}}
                <br><br>
                Apabila anda telah memenuhi syarat diatas, maka Anda dapat melakukan request jadwal encounter dibawah
                ini.<br>
            </strong></p>
        <br>
        <div class="text-center">
            <form action="{{ route('social-learning-booking.store', $_SERVER['HTTP_HOST']) }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <input type="hidden" name="course_id" value="{{ $course->id }}">

                <!-- Button trigger modal -->
                @if(DB::table('course_bookings')->where(['course_id' => $course->id, 'user_id' => Auth::id()])->first() != null)
                <a href="/social-learning-booking">
                    <button type="button" class="btn btn-warning">
                        <i class="fa fa-ticket"></i>
                        Anda Sudah Mengikuti Ujian Ini
                        <i class="fa fa-long-arrow-right"></i>
                    </button>
                </a> 
                 @else
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
                    <i class="fa fa-ticket"></i>
                    Booking Ujian Ini
                    <i class="fa fa-long-arrow-right"></i>
                </button>
                @endif

                {{--  Modal  --}}
                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
                     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLongTitle">Booking</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="col-md-12">
                                    <div class="form-group text-left">
                                        <label>Kelas (Optional) : </label>
                                        <select name="class_id" class="form-control" onchange="onSelectClass(value)">
                                            <option value="0">Pilih Kelas</option>
                                            @foreach($course->classes as $class)
                                                <option value="{{ $class->id }}">{{ $class->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group text-left">
                                        <label>Tanggal : </label>
                                        <input class="form-control" type="date" name="date" required>
                                    </div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                <button type="submit" class="btn btn-primary">Booking</button>
                            </div>
                        </div>
                    </div>
                </div>
                {{--  END MODAL  --}}

            </form>
        </div>
    </div>

    <!-- akhir isi tengah -->
@endsection

@section('script')
    <script>
        function onSelectClass(value) {
            {{--  $('p#capacity').replaceWith('<p id="capacity">{{ $course->classes->where("id", ' + value + ') }}</p>');  --}}
        }
    </script>
@endsection