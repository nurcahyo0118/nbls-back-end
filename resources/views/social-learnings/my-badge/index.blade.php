@extends('layouts.frontend')

@section('content')
    <!-- isi tengah -->

    <div class="col-md-8 col-lg-6 footer-space">

        <table class="table table-hover">
            <thead>
            <tr>
                <th>Lencana</th>
                <th>Badge</th>
                {{--<th></th>--}}
            </tr>
            </thead>
            <tbody>
            @php
                $user = Auth::user();
                $badges = App\BadgeSocialLearning::all();
            @endphp

            @foreach($badges as $index => $badge)
                <tr>
                    @if($badge->simple_name == 'badge_make_friend' && $user->followers()->count() >= 10)
                        <td>
                            <i class="fa {{ $badge->logo }} 2x"></i>
                        </td>
                        <td>{{ $badge->name }}</td>
                    @endif

                    @if($badge->simple_name == 'badge_fans' && $user->followers()->count() >= 50)
                        <td>
                            <i class="fa {{ $badge->logo }} fa-2x"></i>
                        </td>
                        <td>{{ $badge->name }}</td>
                    @endif

                    @if($badge->simple_name == 'badge_popular' && $user->followers()->count() >= 100)
                        <td>
                            <i class="fa {{ $badge->logo }} fa-2x"></i>
                        </td>
                        <td>{{ $badge->name }}</td>
                    @endif

                    @if($badge->simple_name == 'badge_alumni' && DB::table('user_encounters')->where(['user_id' => $user->id])->first() != null)
                        <td>
                            <i class="fa {{ $badge->logo }} fa-2x"></i>
                        </td>
                        <td>{{ $badge->name }}</td>
                    @endif

                    @if($badge->simple_name == 'badge_welcome' && DB::table('logs')->where(['reason' => 'LOG_LOGIN', 'user_id' => $user->id])->count() > 7)
                        <td>
                            <i class="fa fa-sign-out fa-2x"></i>
                        </td>
                        <td>{{ $badge->name }}</td>
                    @endif

                    @if($badge->simple_name == 'badge_nocturnal' && DB::table('logs')->where(['reason' => 'LOG_LOGIN', 'user_id' => $user->id])->count() > 7)
                        <td>
                            <i class="fa {{ $badge->logo }} fa-2x"></i>
                        </td>
                        <td>{{ $badge->name }}</td>
                    @endif

                    @if($badge->simple_name == 'badge_i_time' && DB::table('itime_social_learnings')->where('user_id', $user->id)->first() != null)
                        <td>
                            <i class="fa {{ $badge->logo }} fa-2x"></i>
                        </td>
                        <td>{{ $badge->name }}</td>
                    @endif

                </tr>

            @endforeach
            </tbody>
        </table>

    </div>

@endsection

{{--  @if($badge < 399 or $badge > 399)
    <tr>
        <td>1</td>
        <td><img src="/images/brand/TOSCA.png" alt="" class="img-rounded center-block" height="50px"></td>
        <td>Newbie</td>
        <td>Ayo Semangat</td>
    </tr>
@endif
@if($badge > 899)
    <tr>
        <td>2</td>
        <td><img src="/images/brand/MERAH.png" alt="" class="img-rounded center-block" height="50px"></td>
        <td>Trained</td>
        <td>Kamu Pasti Bisa</td>
    </tr>
@endif
@if($badge > 2999)
    <tr>
        <td>3</td>
        <td><img src="/images/brand/BLUE.png" alt="" class="img-rounded center-block" height="50px"></td>
        <td>Super</td>
        <td>Kamu Keren</td>
    </tr>
@endif
@if($badge > 6999)
    <tr>
        <td>4</td>
        <td><img src="/images/brand/ORANGE.png" alt="" class="img-rounded center-block" height="50px"></td>
        <td>Geek</td>
        <td> I am Geeker</td>
    </tr>
@endif
@if($badge > 10000)
    <tr>
        <td>5</td>
        <td><img src="/images/brand/HIJAU.png" alt="" class="img-rounded center-block" height="50px"></td>
        <td>Freak</td>
        <td>Freaky is Tricky</td>
    </tr>
@endif
@if($badge > 18000)
    <tr>
        <td>6</td>
        <td><img src="/images/brand/HIJAU.png" alt="" class="img-rounded center-block" height="50px"></td>
        <td>Master</td>
        <td>I AM THE MASTER</td>
    </tr>
@endif  --}}