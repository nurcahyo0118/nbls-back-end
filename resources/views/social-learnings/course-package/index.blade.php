@extends('layouts.frontend-no-right-left')

@section('content')

    <div class="row">

        <!-- start Carousel -->
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" style="height: 500px;" src="/images/big/banner.jpg" alt="First slide">

                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" style="height: 500px;" src="/images/big/img3b.jpg" alt="Second slide">

                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" style="height: 500px;" src="/images/big/banner.jpg" alt="Third slide">

                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <!-- end Caraousel -->


        <div class="container">

            <h3>Paket Kursus</h3>

            <!-- form input -->
            <div class="row">
                {{--<div class="col-md-1">--}}
                {{--Filter by--}}
                {{--</div>--}}

                {{--<div class="col-md-5">--}}
                {{--<div class="row">--}}
                {{--<div class="col-md-6">--}}
                {{--<div class="form-group">--}}
                {{--<select class="form-control" onchange="onSelectFilter(value);">--}}
                {{--<option>All</option>--}}
                {{--<option value="category">Kategory</option>--}}
                {{--<option value="price">Harga</option>--}}
                {{--<option value="popular">Populer</option>--}}
                {{--</select>--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--<div class="col-md-6">--}}
                {{--<div class="form-group">--}}
                {{--<select id="selectChildFilter" class="form-control" onchange="onSelectCategory(value);">--}}
                {{--<option>All</option>--}}
                {{--</select>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--</div>--}}

                {{--<div class="col-md-6">--}}
                {{--<form id="inputSearchForm" onsubmit="onSearchCourse(); return false;" method="POST">--}}
                {{--<div class="input-group">--}}

                {{--{{ csrf_field() }}--}}

                {{--<input id="inputSearch" type="text" class="form-control" placeholder="Search" required>--}}

                {{--<button type="submit" class="btn btn-primary">--}}
                {{--<i class="fa fa-search"></i>--}}
                {{--</button>--}}
                {{--</div>--}}
                {{--</form>--}}
                {{--</div>--}}

                <div class="container">
                    <div class="row">
                        @foreach($course_packages as $course_package)
                            @include('social-learnings.course-package.child.child-all-course-package')
                        @endforeach
                    </div>
                </div>

            </div>
            <!-- end form input -->

        </div>
        <!-- end row -->


        @endsection
        @section('script')
            <script>
                // $(function () {
                //     $.ajax({
                //         type: 'GET',
                //         url: '/loads/social-learning-course-package/all',
                //         success: function (response) {
                //             $('#allCourseList').replaceWith(response);
                //         },
                //         error: function (error) {
                //             $('#allCourseList').replaceWith('<div id="allCourseList" class="text-center">Course Not Found.</div>');
                //         }
                //     });
                // });
            </script>

            <script>
                function onSearchCourse() {

                    $('#allCourseList').replaceWith('<div id="allCourseList" class="text-center"><i class="fa fa-spinner fa-spin fa-5x"></i></div>');

                    var keyword = $('#inputSearch').val();

                    $.ajax({
                        type: 'GET',
                        url: '/loads/social-learning-course/' + keyword,
                        success: function (response) {
                            if (response.count != 0) {
                                $('#allCourseList').replaceWith(response.view);
                            }
                            else {
                                $('#allCourseList').replaceWith('<div id="allCourseList" class="text-center">Course Not Found.</div>');
                            }
                        },
                        error: function (error) {
                            $('#allCourseList').replaceWith('<div id="allCourseList" class="text-center">Course Not Found.</div>');
                        }
                    });
                }

                {{--  On Select Filter By  --}}
                function onSelectFilter(value) {
                    console.log(value);
                    switch (value) {
                        case 'category':

                            var selectChildFilter = $('#selectChildFilter');

                            $.ajax({
                                type: 'GET',
                                url: '/loads/categories/all',
                                success: function (response) {
                                    console.log(response);

                                    selectChildFilter.empty();
                                    selectChildFilter.append('<option>Pilih Kategori</option>');

                                    for (let category of response) {
                                        console.log(category);
                                        selectChildFilter.append('<option value="' + category.id + '">' + category.name + '</option>');
                                    }

                                },
                                error: function (error) {
                                    $('#allCourseList').replaceWith('<div id="allCourseList" class="text-center">Course Not Found.</div>');
                                }
                            });

                            break;

                        case 'price':

                            break;

                        case 'popular':

                            break;
                    }
                }

                function onSelectCategory(value) {
                    console.log(value);
                    switch (value) {
                        case 'category':

                            var selectChildFilter = $('#selectChildFilter');

                            $.ajax({
                                type: 'GET',
                                url: '/loads/categories/' + value,
                                success: function (response) {
                                    console.log(response);

                                    selectChildFilter.empty();

                                    for (let category of response) {
                                        console.log(category);
                                        selectChildFilter.append('<option value="' + category.id + '">' + category.name + '</option>');
                                    }

                                },
                                error: function (error) {
                                    $('#allCourseList').replaceWith('<div id="allCourseList" class="text-center">Course Not Found.</div>');
                                }
                            });

                            break;

                        case 'price':

                            break;

                        case 'popular':

                            break;
                    }
                }
            </script>
@endsection
