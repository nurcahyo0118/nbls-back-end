<div class="col-md-4 m-t-20">
    <div class="card">
        @if($course_package->image === null)
            <img class="card-img-top" src="/images/order/web_design.jpg" style="height: 200px"
                 alt="Card image cap">
        @else
            <img class="card-img-top" src="/course-package/images/{{ $course_package->image }}" style="height: 200px"
                 alt="Card image cap">
        @endif
        <div class="card-body">
            <p class="text-muted">
                <small>By {{ $course_package->user->fullname }} |
                    <i class="ion-clock"></i> {{ $course_package->updated_at->diffForHumans() }}
                </small>
            </p>
            <h6 style="height: 40px"><strong>{{ $course_package->title }}</strong></h6>
        </div>

        <div class="card-footer text-center btn-primary btn-block">
            <a class="text-white"
               href="{{ route('social-learning-course-packages.show', [$_SERVER['HTTP_HOST'], $course_package->id]) }}">
                VIEW DETAILS
                <i class="md-arrow-forward"></i>
            </a>
        </div>
    </div>
</div>
