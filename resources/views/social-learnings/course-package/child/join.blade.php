<div id="join{{ $course_package->id }}" class="col-md-4">

    @if($course_package->users()->where(['user_id' => Auth::id()])->count() === 0)
        @if($course_package->price == 0)
            <button type="button" onclick="onTakeFreePackageCourse({{ $course_package->id }})"
                    class="btn btn-primary btn-block">
                Gabung
            </button>
        @else
            <button type="button" onclick="onTakePackageCourse({{ $course_package->id }})"
                    class="btn btn-primary btn-block">Gabung
            </button>
        @endif
    @else
        <button type="button" onclick="onLeavePackageCourse({{ $course_package->id }})"
                class="btn btn-primary btn-block">Telah
                                                  Bergabung
        </button>
    @endif
</div>
