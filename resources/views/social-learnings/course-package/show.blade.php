@extends('layouts.frontend-no-right-left')

@section('content')

    <div class="row">

        <div class="container">

            <h6 class="text-muted">
                <span class="fa fa-home"></span>
                Home
                <span class="fa fa-long-arrow-right"></span>
                Course Package
            </h6>

            <center>
                <h3>Kursus Paket <br> <h6 class="text-muted">Harga Lebih Murah</h6></h3>
                <hr width="35%">

                <h3>{{ $course_package->title }}</h3>

                @if($course_package->price != 0)
                    <h1>Rp. {{ number_format($course_package->price, 0, ',', '.') }}</h1>
                @else
                    <span class="label label-default">Gratis</span>
                @endif
            </center>

            <form action="{{ route('course-package.join', [$_SERVER['HTTP_HOST'], $course_package->id]) }}"
                  method="POST" class="text-center">

                {{ csrf_field() }}
                <button type="submit" class="btn btn-primary btn-block col-md-2 pull-right">
                    Beli Paket
                </button>
            </form>

            {{--            <p>{{ $course_package->description }}</p>--}}

            <br>

            <div class="row">

                @foreach($course_package->packages as $course)
                    <div class="col-md-4 m-t-20">
                        <div class="card">
                            @if( $course->image === null)
                                <img class="card-img-top" src="/images/order/web_design.jpg"
                                     alt="Card image cap">
                            @else
                                <img class="card-img-top" src="/xxcourses/images/{{ $course->image }}"
                                     alt="Card image cap">
                            @endif
                            <div class="card-body">
                                <p class="text-muted">
                                    <small>By {{ $course->author->fullname }} |
                                        <i class="ion-clock"></i> {{ $course->updated_at->diffForHumans() }}
                                    </small>
                                </p>
                                <h6><strong>{{ $course->title }}</strong></h6>
                                <p>
                                    <i class="fa fa-users"> {{ $course->users->count() }} Siswa</i>
                                </p>
                            </div>

                            <div class="card-footer text-center btn-primary btn-block">
                                <a class="text-white"
                                   href="{{ route('social-learning-course.show', [$_SERVER['HTTP_HOST'], $course->id]) }}">
                                    VIEW DETAILS
                                    <i class="md-arrow-forward"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
            <!-- Pagination -->
            <div class="m-t-15">

                <div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/"
                     data-layout="button_count" data-size="large" data-mobile-iframe="true">
                    <a target="_blank"
                       href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse"
                       class="fb-xfbml-parse-ignore">Bagikan
                    </a>
                </div>
            </div>
            <!-- end Pagination -->
        </div>
        <!-- end row -->


        <div id="fb-root"></div>
        <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s);
                js.id = id;
                js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.12';
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
@endsection
