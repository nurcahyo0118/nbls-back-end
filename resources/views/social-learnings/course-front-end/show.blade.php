<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>NBLS - Babastudio</title>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link href="/baba-custom/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/baba-custom/css/custom.css" rel="stylesheet">
    <link href="/baba-custom/lib/animate/animate.min.css" rel="stylesheet">
    <link href="/baba-custom/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="/baba-custom/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="/baba-custom/lib/lightbox/css/lightbox.min.css" rel="stylesheet">
    <link href="/baba-custom/css/style.css" rel="stylesheet">
</head>
<body>


@include('layouts.partials-baba-custom.header')

<div class="row">

    <div class="container footer-space">
        <h6 class="text-muted">
            <span class="fa fa-home"></span>
            Home
            <span class="fa fa-long-arrow-right"></span>
            Paket Kursus
        </h6>

        <h4 class="text-center mt-5">
            <strong>{{ $course->title }}</strong>
        </h4>

        <hr width="35%">

        <div class="row">
            <div class="col-md-8">
                <div class="row mt-5 mb-5">
                    <div class="col-md-12">

                        <img src="/xxcourses/images/{{ $course->image }}" class="img-fluid">
                    </div>

                    <div class="col-md-12">
                        <div class="card h-100" style="margin-top: 40px">

                            <div class="card-body">
                                <h3>
                                    Deskripsi
                                </h3>
                                {!! $course->description !!}

                                <hr>

                                <h3>
                                    Siapa Yang Cocok mengambil kursus Ini ?
                                </h3>
                                {!! $course->suitable !!}

                                <hr>

                                <h3>
                                    Apa Yang Akan Anda Peroleh ?
                                </h3>
                                {!! $course->can_be !!}

                                <hr>

                                <h3>
                                    Apa Yang Harus Anda Persiapkan ?
                                </h3>
                                {!! $course->requirement !!}

                                <hr>

                                <h3>
                                    Tinjau Kursus Ini
                                </h3>
                                <div style="position: relative;padding-bottom: 56.25%;padding-top: 30px;height: 0;overflow: hidden;">

                                    <iframe
                                            src="{{ str_replace('watch?v=', 'embed/', $course->video) }}"
                                            frameborder="0"
                                            allow="autoplay; encrypted-media"
                                            width="800" height="450"
                                            style="position: absolute; top: 0; left: 0; width: 100%; height: 100%"
                                            allowfullscreen>
                                    </iframe>

                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-md-4">
                <div class="row mt-5 mb-5">
                    <div class="col-md-12">
                        <div class="card h-100">

                            <div class="card-body">
                                <div class="row mt-5 ">
                                    <div class="col-md-6 text-center">
                                        <div class="col-md-12">
                                            <img src="/images/icon/sertifikat.png" alt="">
                                        </div>
                                        <div class="col-md-12 mt-2">
                                            <strong class="mt-2">SERTIFIKAT RESMI</strong>
                                        </div>
                                    </div>

                                    <div class="col-md-6 text-center">
                                        <div class="col-md-12">
                                            <img src="/images/icon/ternama.png" alt="">
                                        </div>
                                        <div class="col-md-12 mt-2">
                                            <strong class="mt-2">PRAKTIS TERNAMA</strong>
                                        </div>
                                    </div>

                                    <div class="col-md-6 text-center">
                                        <div class="col-md-12">
                                            <img src="/images/icon/pengajar.png" alt="">
                                        </div>
                                        <div class="col-md-12 mt-2">
                                            <strong class="mt-2">INSTRUKTUR SELALU ONLINE</strong>
                                        </div>
                                    </div>

                                    <div class="col-md-6 text-center">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <img src="/images/icon/kampus.png" alt="">
                                            </div>
                                            <div class="col-md-12 mt-2">
                                                <strong class="mt-2">KOMUNITAS ESKLUSIF</strong>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                @if($course->price != 0)
                                    Rp. {{ number_format($course->price, 0, ',', '.') }}
                                @else
                                    Gratis
                                @endif
                            </div>

                            <div class="card-body">
                                <button type="button" onclick="addToCart({{ $course->id }})"
                                        class="btn btn-primary col-md-12">Beli
                                </button>

                                <hr>

                                <p>Metode Pembayaran bisa dilakukan dengan cara :</p>
                                <p>1. Transfer ATM</p>
                                <p>2. Mobile Banking</p>
                                <p>3. Internet Banking</p>
                                <p>4. SMS Banking</p>
                            </div>

                        </div>
                    </div>

                </div>
            </div>

            <div class="col-md-12">
                <div class="card h-100" style="margin-top: 40px">

                    <div class="card-body">
                        <h3>
                            Kurikulum
                        </h3>
                        <div class="accordion" id="accordionExample">

                            @foreach($course->sections as $section)
                                <div class="card">
                                    <div class="card-body">
                                        <h5>{{ $section->title }}</h5>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <hr>

                        <h3>
                            Ulasan
                        </h3>

                        @foreach($course->reviews as $review)
                            @include('social-learnings.course.child.review-child')
                        @endforeach

                    </div>

                </div>
            </div>
        </div>

        <br>
        <br>
        <br>
        <br>

    </div>
</div>

@include('layouts.partials-baba-custom.footer')


<script>
    $(function () {
        {{--  $("form#rating").submit(function(e) {
            e.preventDefault();
            var formData = new FormData(this);

            $.ajax({
                url: '/course/rate',
                type: 'POST',
                data: formData,
                success: function (data) {
                    swal("Berhasil !", "Berhasil mengirim ulasan", "success");
                },
                cache: false,
                contentType: false,
                processData: false
            });

        });  --}}

    });
</script>
<script>
    function onTakeCourse(courseId) {
        var formData = new FormData();
        formData.append('_token', '{{ csrf_token() }}')
        $.ajax({
            url: '/social-learning-my-course/' + courseId + '/add-to-cart-without-login',
            type: 'POST',
            data: formData,
            success: function (data) {
                console.log(data);
                $('#join' + courseId).replaceWith(data);
                swal("Berhasil !", "Berhasil menambahkan kursus kedalam keranjang", "success");
            },
            error: function (error) {
                swal("Gagal !", "Gagal bergabung", "error");
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }

    function onTakeFreeCourse(courseId) {

        $.ajax({
            url: '/courses/' + courseId + '/join',
            type: 'POST',
            data: null,
            success: function (data) {
                console.log(data);
                $('#join' + courseId).replaceWith(data);
                swal("Berhasil !", "Berhasil bergabung", "success");
            },
            error: function (error) {
                swal("Gagal !", "Gagal bergabung", "error");
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }

    function onLeaveCourse(courseId) {
        $.ajax({
            url: '/courses/' + courseId + '/leave',
            type: 'POST',
            data: null,
            success: function (data) {
                console.log(data);
                $('#join' + courseId).replaceWith(data);
                swal("Berhasil !", "Berhasil meninggalkan kursus", "success");
            },
            error: function (error) {
                swal("Gagal !", "Gagal meninggalkan kursus", "error");
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }

    function onDeleteReview(id) {
        swal({
            title: "Apa kamu yakin?",
            text: "Data yang telah di hapus tidak dapat dikembalikan",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {

                $.ajax({
                    url: '/course/rate',
                    type: 'DELETE',
                    data: null,
                    success: function (data) {
                        window.location.reload();
                    },
                    error: function (error) {
                        swal("Gagal !", "Gagal meninggalkan kursus", "error");
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });

            }
        });
    }

    function addToCart(course_id) {
        var carts = [];

        var carts_from_local = JSON.parse(window.localStorage.getItem('carts'));

        if (carts_from_local === null) {
            carts.push(course_id);
            window.localStorage.setItem('carts', JSON.stringify(carts));

            swal("Berhasil memasukkan kursus ke dalam keranjang");
        }
        else {
            if (carts_from_local.indexOf(course_id) < 0) {
                carts_from_local.push(course_id);
                window.localStorage.setItem('carts', JSON.stringify(carts_from_local));

                swal("Berhasil memasukkan kursus ke dalam keranjang");
            }

        }
    }

    function removeFromCart(course_id) {
        var carts_from_local = JSON.parse(window.localStorage.getItem('carts'));
        var index = carts_from_local.indexOf(course_id);
        if (carts_from_local !== null && index > -1) {
            carts_from_local.splice(index, 1);
            window.localStorage.setItem('carts', JSON.stringify(carts_from_local));

            // remove item cart
            $('#cartsItem' + course_id).remove();
            $('#cartsItemDivider' + course_id).remove();

            var carts_from_local_latest = JSON.parse(window.localStorage.getItem('carts'));
            if (carts_from_local_latest.length === 0) {
                $('#cartsList').replaceWith('<div id="cartsList" class="col-sm-12 col-md-12 col-md-12 text-center" style="color: #424242">Kosong !</div>');
            }
        }
    }

    function loadCartData(carts) {
        var url = '/loads/frontend/carts?';

        if (carts !== null && carts.length !== 0) {
            for (i = 0; i < carts.length; i++) {
                url = url + '&carts[]=' + carts[i];
                console.log(url);
            }

            $.ajax({
                type: 'GET',
                url: url,
                success: function (response) {
                    $('#cartsList').replaceWith(response.view);
                },
                error: function (error) {
                    console.log('ddddd');
                    $('#cartsList').replaceWith('<div id="cartsList" class="col-sm-12 col-md-12 col-md-12 text-center" style="color: #424242">Kosong !</div>');
                }
            });
        }
        else {
            $('#cartsList').replaceWith('<div id="cartsList" class="col-sm-12 col-md-12 col-md-12 text-center" style="color: #424242">Kosong !</div>');
        }

    }
</script>
