<div class="col-md-12" style="margin-bottom: 10px;">
    <div class="card card-default">
        <div class="card-body">
            <div class="media">
                <img class="d-flex mr-3 rounded-circle thumb-sm" src="/images/default.png"
                     alt="Generic placeholder image">
                <div class="media-body">
                    <h5 class="mt-0"><strong>{{ $review->user->fullname }}</strong></h5>
                    <h5>
                        <i class="fa fa-star{{ $review->rate >= 1 ? '' : '-o' }}"></i>
                        <i class="fa fa-star{{ $review->rate >= 2 ? '' : '-o' }}"></i>
                        <i class="fa fa-star{{ $review->rate >= 3 ? '' : '-o' }}"></i>
                        <i class="fa fa-star{{ $review->rate >= 4 ? '' : '-o' }}"></i>
                        <i class="fa fa-star{{ $review->rate >= 5 ? '' : '-o' }}"></i>
                    </h5>
                    {{ $review->suggestion }}
                </div>
            </div>
        </div>
    </div>

</div>