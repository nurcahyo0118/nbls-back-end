<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>NBLS - Babastudio</title>
    <!-- other -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="/baba-custom/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles -->
    <link href="/baba-custom/css/modern-business.css" rel="stylesheet">
    <link href="/baba-custom/css/custom.css" rel="stylesheet">
</head>
<body>

@include('social-learnings.course-front-end.partials.header')

@yield('content')

<header>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <!-- Slide One - Set the background image for this slide in the line below -->
            <div class="carousel-item active" style="background-image: url('/baba-custom/images/banner.jpg')">
                <div class="carousel-caption d-none d-md-block">
                    <h3>
                        <span class="f-orange">BABASTUDIO</span>
                        <br/>
                        <span class="f-orange f-slim">HYBRID</span>
                        <span class="f-white"> LEARNING</span>
                    </h3>
                    <p>Lebih dari 3000 orang telah menggunakan sistem
                        <br/>Babastudio E-learning untuk kurusu online
                    </p>
                </div>
            </div>
            <!-- Slide Two - Set the background image for this slide in the line below -->
            <div class="carousel-item" style="background-image: url('/baba-custom/images/banner.jpg')">
                <div class="carousel-caption d-none d-md-block">
                    <h3>
                        <span class="f-orange">BABASTUDIO</span>
                        <br/>
                        <span class="f-orange f-slim">HYBRID</span>
                        <span class="f-white"> LEARNING</span>
                    </h3>
                    <p>Lebih dari 3000 orang telah menggunakan sistem
                        <br/>Babastudio E-learning untuk kurusu online
                    </p>
                </div>
            </div>
            <!-- Slide Three - Set the background image for this slide in the line below -->
            <div class="carousel-item" style="background-image: url('/baba-custom/images/banner.jpg')">
                <div class="carousel-caption d-none d-md-block">
                    <h3>
                        <span class="f-orange">BABASTUDIO</span>
                        <br/>
                        <span class="f-orange f-slim">HYBRID</span>
                        <span class="f-white"> LEARNING</span>
                    </h3>
                    <p>Lebih dari 3000 orang telah menggunakan sistem
                        <br/>Babastudio E-learning untuk kurusu online
                    </p>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</header>

<div class="container mt-5">
    <div class="col-md-12 no-padding message">
        <h3>Kursus</h3>

        <div class="card">
            <div class="card-body">
                <form class="form-inline">
                    <div class="col-md-1 no-padding">
                        <h4>Filter By</h4>
                    </div>
                    <div class="col-md-10 offset-md-1">
                        <select class="form-control mb-2 mr-sm-2 mr-md-3" onchange="onSelectFilter(value);">
                            <option value="all">-- All --</option>
                            <option value="category">Kategori</option>
                            <option value="price">Harga</option>
                            <option value="popular">Populer</option>
                        </select>

                        <select id="selectChildFilter" class="form-control  mb-2 mr-sm-2 mr-md-3"
                                onchange="onSelectCategory(value);">
                            <option value="">-- Select One --</option>
                        </select>

                        <input id="inputSearch" type="text" class="form-control mb-2 mr-sm-2 w-50"
                               placeholder="Search for..." onkeypress="return event.keyCode != 13;">

                        <span class="input-group-btn">
                            <button class="btn btn-defaul mb-2 mr-sm-2" type="button" onclick="onSearchCourse()">
                                <span class="fa fa-search"></span>
                            </button>
                        </span>

                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end filter kursus -->
    <!-- daftar kursus -->

    <br>

    <div id="courseContent">
    </div>

</div>

<!-- Footer -->
<section class="section-footer" id="footer">
    <div class="container">
        <div class="row text-center text-xs-center text-sm-left text-md-left">
            <div class="col-xs-12 col-sm-4 col-md-3">
                <img src="/baba-custom/images/logo.jpg" class="img-fluid">
                <p class="f-white">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                   Lorem Ipsum has been the industry's
                                   standard dummy text ever since the 1500s,
                                   when an unknown printer took a galley of type and scrambled
                                   it to make a type specimen book.
                </p>
                <ul class="list-unstyled quick-links">
                    <li>
                        <a href="javascript:void();">
                            <i class="fa fa-phone"></i>
                            (021) 5366 4008
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void();">
                            <i class="fa fa-envelope"></i>
                            info@babastudio.com
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3">
                <h5>LINK MAP</h5>
                <ul class="list-unstyled quick-links">
                    <li>
                        <a href="javascript:void();">
                            <i class="fa fa-circle"></i>
                            Home
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void();">
                            <i class="fa fa-circle"></i>
                            About
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void();">
                            <i class="fa fa-circle"></i>
                            FAQ
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void();">
                            <i class="fa fa-circle"></i>
                            Get Started
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void();">
                            <i class="fa fa-circle"></i>
                            Videos
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3">
                <h5>GET NEWSLETTER</h5>
                <p class="f-white">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                   Lorem Ipsum has been the industry's
                </p>
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="" aria-describedby="basic-addon2">
                    <span class="input-group-addon" id="basic-addon2">
                        <i class="fa fa-paper-plane" aria-hidden="true"></i>
                    </span>
                    <br/>
                    <!-- <h6 class="f-white f-slim">Yout Information are safe with us </h6> -->
                </div>
                <p class="f-white">Yout Information are safe with us
                </p>
                <div id="social">
                    <a class="facebookBtn smGlobalBtn" href="#"></a>
                    <a class="twitterBtn smGlobalBtn" href="#"></a>
                    <a class="googleplusBtn smGlobalBtn" href="#"></a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3">
                <h5>OUR INSTRUCTOR</h5>
                @php

                    $users = App\User::where('role_id', '4')->inRandomOrder()->take(9)->get();

                @endphp

                @foreach($users as $user)

                    @if($user->photo === null)
                        <div class="col-xs-3 float-left mr-1 mt-1"><img src="/images/users/avatar-1.jpg" width="70em">
                        </div>
                    @else
                        <div class="col-xs-3 float-left mr-1 mt-1"><img src="/images/photo/{{ $user->photo }}"
                                                                        width="70em"></div>
                    @endif

                @endforeach
            </div>
        </div>
        <hr
        / style="border:1px solid #152e48;">
        <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <p class="h6 float-left">&copy All right Reversed.
                    <a class="text-green ml-2" href="" target="_blank">BABASTUDIO</a>
                </p>
            </div>
            <div class="credits" style="font-size:5px; color:#ccc; opacity: 0.2; float:right;">
                Best
                <a href="https://bootstrapmade.com/">Bootstrap Templates</a>
                by BootstrapMade
            </div>
        </div>
        </hr>
    </div>
</section>
<!-- end Footer -->

<!-- Bootstrap core JavaScript -->
<script src="/baba-custom/vendor/jquery/jquery.min.js"></script>
<script src="/baba-custom/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- custom Javascript -->
<script src="/baba-custom/js/custom.js"></script>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<script>

    var paginations = $('#coursesPagination');

    function onSearchCourse() {
        $('#selectChildFilter').hide();

        $('#courseContent').replaceWith('<div id="courseContent" class="text-center"><i class="fa fa-spinner fa-spin fa-5x"></i></div>');

        var keyword = $('#inputSearch').val();

        if (keyword) {
            $.ajax({
                type: 'GET',
                url: '/loads/social-learning-course-front-end/' + keyword,
                success: function (response) {
                    if (response.count != 0) {
                        $('#courseContent').replaceWith(response.view);
                    }
                    else {
                        $('#courseContent').replaceWith('<div id="courseContent" class="text-center">Course Not Found.</div>');
                    }
                },
                error: function (error) {
                    $('#courseContent').replaceWith('<div id="courseContent" class="text-center">Course Not Found.</div>');
                }
            });
        }
        else {
            loadAllCourses();
        }

    }

    function loadAllCourses() {
        $('#selectChildFilter').hide();

        $.ajax({
            type: 'GET',
            url: '/loads/social-learning-course-front-end/all',
            success: function (response) {
                console.log(response.view);
                $('#courseContent').replaceWith(response.view);
            }
        });
    }

    function onSelectFilter(value) {
        console.log(value);
        $('#selectChildFilter').hide();
        switch (value) {
            case 'all':

                loadAllCourses();
                break;

            case 'category':

                var selectChildFilter = $('#selectChildFilter');

                $.ajax({
                    type: 'GET',
                    url: '/loads/social-learning-course-front-end/categories/all',
                    success: function (response) {
                        selectChildFilter.empty();
                        selectChildFilter.append('<option>Pilih Kategori</option>');

                        for (let category of response) {
                            console.log(category);
                            selectChildFilter.append('<option value="' + category.id + '">' + category.name + '</option>');
                        }

                    },
                    error: function (error) {
                        $('#courseContent').replaceWith('<div id="courseContent" class="text-center">Course Not Found.</div>');
                    }
                });

                selectChildFilter.show();

                break;

            case 'price':
                $.ajax({
                    type: 'GET',
                    url: '/loads/social-learning-course-front-end/orderby/prices',
                    success: function (response) {
                        $('#courseContent').replaceWith(response);
                    },
                    error: function (error) {
                        console.log('ddddd');
                        $('#courseContent').replaceWith('<div id="courseContent" class="text-center">Course Not Found.</div>');
                    }
                });
                break;

            case 'popular':
                $.ajax({
                    type: 'GET',
                    url: '/loads/social-learning-course-front-end/orderby/popular',
                    success: function (response) {
                        $('#courseContent').replaceWith(response);
                    },
                    error: function (error) {
                        console.log('ddddd');
                        $('#courseContent').replaceWith('<div id="courseContent" class="text-center">Course Not Found.</div>');
                    }
                });
                break;
        }
    }

    function onSelectCategory(value) {
        console.log(value);

        var selectChildFilter = $('#selectChildFilter');

        $.ajax({
            type: 'GET',
            url: '/loads/social-learning-course-front-end/categories/' + value + '/courses',
            success: function (response) {
                console.log(response);
                $('#courseContent').replaceWith(response);
            },
            error: function (error) {
                $('#courseContent').replaceWith('<div id="courseContent" class="text-center">Course Not Found.</div>');
            }
        });
    }

    function selectPage(page, filter) {

        console.log('Page = ' + page, 'Filter = ' + filter);
        switch (filter) {
            case 'all':

                $.ajax({
                    type: 'GET',
                    url: '/loads/social-learning-course-front-end/all?page=' + page,
                    success: function (response) {
                        console.log(response.view);
                        $('#courseContent').replaceWith(response.view);
                    }
                });

                break;

            case 'category':

                var selectChildFilter = $('#selectChildFilter');

                $.ajax({
                    type: 'GET',
                    url: '/loads/social-learning-course-front-end/categories/all?page=' + page,
                    success: function (response) {
                        selectChildFilter.empty();
                        selectChildFilter.append('<option>Pilih Kategori</option>');

                        for (let category of response) {
                            console.log(category);
                            selectChildFilter.append('<option value="' + category.id + '">' + category.name + '</option>');
                        }

                    },
                    error: function (error) {
                        $('#courseContent').replaceWith('<div id="courseContent" class="text-center">Course Not Found.</div>');
                    }
                });

                selectChildFilter.show();

                break;

            case 'price':
                $.ajax({
                    type: 'GET',
                    url: '/loads/social-learning-course-front-end/orderby/prices?page=' + page,
                    success: function (response) {
                        $('#courseContent').replaceWith(response);
                    },
                    error: function (error) {
                        console.log('ddddd');
                        $('#courseContent').replaceWith('<div id="courseContent" class="text-center">Course Not Found.</div>');
                    }
                });
                break;

            case 'popular':
                $.ajax({
                    type: 'GET',
                    url: '/loads/social-learning-course-front-end/orderby/popular?page=' + page,
                    success: function (response) {
                        $('#courseContent').replaceWith(response);
                    },
                    error: function (error) {
                        console.log('ddddd');
                        $('#courseContent').replaceWith('<div id="courseContent" class="text-center">Course Not Found.</div>');
                    }
                });
                break;
        }
    }

    function addToCart(course_id) {
        var carts = [];

        var carts_from_local = JSON.parse(window.localStorage.getItem('carts'));

        if (carts_from_local === null) {
            carts.push(course_id);
            window.localStorage.setItem('carts', JSON.stringify(carts));
        }
        else {
            if (carts_from_local.indexOf(course_id) < 0) {
                carts_from_local.push(course_id);
                window.localStorage.setItem('carts', JSON.stringify(carts_from_local));
            }

        }
    }

    function removeFromCart(course_id) {
        var carts_from_local = JSON.parse(window.localStorage.getItem('carts'));
        var index = carts_from_local.indexOf(course_id);
        if (carts_from_local !== null && index > -1) {
            carts_from_local.splice(index, 1);
            window.localStorage.setItem('carts', JSON.stringify(carts_from_local));

            // remove item cart
            $('#cartsItem' + course_id).remove();
            $('#cartsItemDivider' + course_id).remove();

            var carts_from_local_latest = JSON.parse(window.localStorage.getItem('carts'));
            if (carts_from_local_latest.length === 0) {
                $('#cartsList').replaceWith('<div id="cartsList" class="col-sm-12 col-md-12 col-md-12 text-center" style="color: #424242">Kosong !</div>');
            }
        }
    }

    function loadCartData(carts) {
        var url = '/loads/frontend/carts?';

        if (carts !== null && carts.length !== 0) {
            for (i = 0; i < carts.length; i++) {
                url = url + '&carts[]=' + carts[i];
                console.log(url);
            }

            $.ajax({
                type: 'GET',
                url: url,
                success: function (response) {
                    $('#cartsList').replaceWith(response.view);
                },
                error: function (error) {
                    console.log('ddddd');
                    $('#cartsList').replaceWith('<div id="cartsList" class="col-sm-12 col-md-12 col-md-12 text-center" style="color: #424242">Kosong !</div>');
                }
            });
        }
        else {
            $('#cartsList').replaceWith('<div id="cartsList" class="col-sm-12 col-md-12 col-md-12 text-center" style="color: #424242">Kosong !</div>');
        }

    }
</script>

<script>
    $(function () {

        var pages;

        var paginations = $('#coursesPagination');

        loadAllCourses();
    });
</script>
</body>
</html>
