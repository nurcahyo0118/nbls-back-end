@extends('layouts.frontend-no-right-left')

@section('content')
    <div class="row">

        @include('social-learnings.course.child.sidebar')

        <div class="col-md-8">

            {{-- <div class="wistia_embed wistia_async_{{ $essay->wistia_media_hashed_id }}"
                style="height:430px;width:100%">&nbsp;
            </div>

            <br> --}}
            <div class="card">

                @if(DB::table('essay_answer_social_learnings')
                    ->where([
                        'essay_id' => $essay->id, 
                        'user_id' => Auth::id()
                    ])
                    ->first() != null)

                    <div class="card-body">
                        <h4 class="text-center">Essay</h4>
                        <hr style="width: 30%">

                        <div class="col-md-12">
                            <h5>{{ $essay->essay }}</h5>
                            <hr>

                        </div>

                        <h4 class="text-center">Jawaban</h4>
                        <hr style="width: 30%">

                        @foreach($essay->answers as $answer)

                            @include('social-learnings.course.essay.answer-child')

                        @endforeach
                    </div>
                @else
                    <div class="card-body">

                        <h4 class="text-center">Essay</h4>
                        <hr style="width: 30%">

                        <h5>{{ $essay->essay }}</h5>

                        <hr>

                        <form action="{{ route('essays.answers.store', [$_SERVER['HTTP_HOST'], $essay->id]) }}"
                              method="POST">
                            <h4 class="text-center">Jawaban Mu</h4>
                            <hr style="width: 30%">

                            {{ csrf_field() }}

                            <input type="hidden" name="log" value="LOG_ANSWER_ESSAY">

                            <textarea name="body" id="answerBody" rows="6" class="form-control"></textarea>
                            <br>
                            <button type="submit" class="btn btn-primary pull-right">
                                <i class="fa fa-paper-plane"></i>
                                Kirim
                            </button>
                        </form>


                    </div>
                @endif

            </div>
        </div>
        <!-- end row -->
    </div>

@endsection

@section('script')
    <script src="//fast.wistia.com/embed/medias/{{ $essay->wistia_media_hashed_id }}.jsonp" async></script>
    <script src="//fast.wistia.com/assets/external/E-v1.js" async></script>

    <script>
        $(function () {

            $("form#addanswer").submit(function (e) {
                e.preventDefault();
                var formData = new FormData(this);

                $.ajax({
                    url: '/social-learning-answers',
                    type: 'POST',
                    data: formData,
                    success: function (data) {
                        $('#answerList').prepend(data);
                        $("form#addanswer")[0].reset();
                        swal("Berhasil !", "Modul berhasil ditambahkan", "success");
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });

                return false;
            });

        });
    </script>

    <script>
        function onAnswerComment(answerId) {
            console.log('Try Submit');
            var body = $('#answerBody' + answerId).val();

            var formData = new FormData();
            formData.append('_token', '{{ csrf_token() }}');
            formData.append('body', body);
            formData.append('log', 'LOG_COMMENTS');

            $.ajax({
                url: '/answers/' + answerId + '/comments',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#answer' + answerId).replaceWith(data);
                },
                error: function (data) {
                    console.log(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onAnswerLike(answerId) {
            var formData = new FormData();
            formData.append('log', 'LOG_LIKE_POSTS');

            $.ajax({
                url: '/answers/' + answerId + '/likes',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#answer' + answerId).replaceWith(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onAnswerDislike(answerId) {
            var formData = new FormData();
            formData.append('log', 'LOG_DISLIKE_POSTS');

            $.ajax({
                url: '/answers/' + answerId + '/dislikes',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#answer' + answerId).replaceWith(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onComment(answerId) {

            var body = $('#answerBody' + answerId).val();
            console.log(content);

            var formData = new FormData();
            formData.append('_token', '{{ csrf_token() }}');
            formData.append('body', body);
            formData.append('log', 'LOG_COMMENTS');

            $.ajax({
                url: '/answers/' + answerId + '/comments',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#answer' + answerId).replaceWith(data);
                },
                error: function (data) {
                    console.log(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

            return false;

        }

        function onAnswerLikeComment(commentId, answerId) {
            var formData = new FormData();
            formData.append('log', 'LOG_LIKE_COMMENTS');

            $.ajax({
                url: '/answers/comments/' + commentId + '/likes',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#answer' + answerId).replaceWith(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onAnswerDislikeComment(commentId, answerId) {
            var formData = new FormData();
            formData.append('log', 'LOG_DISLIKE_COMMENTS');

            $.ajax({
                url: '/answers/comments/' + commentId + '/dislikes',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#answer' + answerId).replaceWith(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }
    </script>
@endsection
