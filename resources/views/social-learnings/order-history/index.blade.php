@extends('layouts.frontend')

@section('content')
    <div class="col-md-8 col-lg-6 footer-space">
        <h3>Order History</h3>
        <div class="alert alert-warning" role="alert">
            <div class="row">
                <div class="col-1 col-md-1">
                    <i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
                </div>
                <div class="col col-md-11">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Warning!</strong> You have a live listring with a simillar tittle.
                                              Please see the duplicate listring policy on eBay for more information.
                </div>
            </div>
        </div>

        <!-- start card -->
        <div class="card h-75">
            <div class="card-box">

                <table class="table table-striped">
                    <thead>
                    <tr>
                        {{--  <th>
                            <div class="checkbox checkbox-primary">
                                <input id="checkbox2" type="checkbox" checked>
                            </div>
                        </th>  --}}
                        <th>Order Date</th>
                        <th>Virtual Account Number</th>
                        <th>Methode</th> 
                        <th>Order Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($orders as $order)
                        <tr>
                            {{--  <th scope="row">
                                <div class="checkbox checkbox-primary">
                                    <input id="checkbox2" type="checkbox" checked>
                                </div>
                            </th>  --}}
                            <td>{{ $order->created_at }}</td>
                            <td>{{ $order->payMethod === '02' ? $order->bankVacctNo : '-' }}</td>
                            <td>{{ $order->payMethod === '01' ? 'Credit Card' : 'Transfer' }}</td> 
                            <td>
                            <span class="text-warning">{{ $order->status }}</span>
                                <i class=" icon-arrow-right"></i>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <!-- end card -->
    </div>
@endsection
