@extends('layouts.frontend')

@section('content')
    <div class="modal fade" id="modalItime">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">
                        Tolong lengkapi Quessioner I-Time Berikut</h4>
                </div>

                <form id="additime" method="POST">

                    <div class="modal-body">

                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab"
                                   href="#instructor"
                                   role="tab" aria-controls="konten-home" aria-selected="true">Instruktur
                                </a>

                                <a class="nav-item nav-link" id="nav-5-tab" data-toggle="tab" href="#material"
                                   role="tab"
                                   aria-controls="konten-5" aria-selected="false">Materi
                                </a>

                                <a class="nav-item nav-link" id="nav-5-tab" data-toggle="tab" href="#facility"
                                   role="tab"
                                   aria-controls="konten-5" aria-selected="false">Fasilitas
                                </a>

                                <a class="nav-item nav-link" id="nav-5-tab" data-toggle="tab" href="#learning_system"
                                   role="tab"
                                   aria-controls="konten-5" aria-selected="false">Sistem Belajar
                                </a>

                            </div>
                        </nav>

                        <div class="tab-content">

                            <div class="tab-pane active" id="instructor">
                                <div class="form-group">
                                    <label>1. Bagaimana penjelasan Instruktur : </label>
                                    <br>
                                    <label><input type="radio" name="instructor_explanation" value="Sangat Jelas">
                                        Sangat Jelas</label>
                                    <br>
                                    <label><input type="radio" name="instructor_explanation" value="Jelas">
                                        Jelas</label>
                                    <br>
                                    <label><input type="radio" name="instructor_explanation" value="Tidak Jelas"> Tidak
                                                                                                                  Jelas</label>
                                </div>

                                <div class="form-group">
                                    <label>2. Response Instruktur saat Anda bertanya : </label>
                                    <br>
                                    <label><input type="radio" name="instructor_response" value="Sangat Response">
                                        Sangat Response</label>
                                    <br>
                                    <label><input type="radio" name="instructor_response" value="Meresponse"> Meresponse</label>
                                    <br>
                                    <label><input type="radio" name="instructor_response" value="Tidak Meresponse">
                                        Tidak Meresponse</label>
                                </div>

                                <div class="form-group">
                                    <label>3. Menurut Anda, Instruktur terbaik itu siapa dan apa alasannya : </label>
                                    <br>
                                    <select class="form-control" name="best_instructor" required>
                                        <option value="">Pilih Instruktur</option>
                                        @foreach($instructors as $instructor)
                                            <option value="{{ $instructor->id }}">{{ $instructor->fullname }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>4. Dan Instruktur yang kurang menurut anda itu siapa ? : </label>
                                    <br>
                                    <select class="form-control" name="worst_instructor">
                                        <option value="">Pilih Instruktur</option>
                                        @foreach($instructors as $instructor)
                                            <option value="{{ $instructor->id }}">{{ $instructor->fullname }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>5. Saran anda terhadap Instruktur : </label>
                                    <br>
                                    <textarea class="form-control" name="instructor_suggestion" id="" cols="30"
                                              rows="10"></textarea>
                                </div>
                            </div>

                            <div class="tab-pane active" id="material">
                                <div class="form-group">
                                    <label>1. Materi yang saat ini sedang anda pelajari, Bagaimana penjelasannya ?
                                           : </label>
                                    <br>
                                    <label><input type="radio" name="course_explanation" value="Sangat Jelas"> Sangat
                                                                                                               Jelas</label>
                                    <br>
                                    <label><input type="radio" name="course_explanation" value="Jelas"> Jelas</label>
                                    <br>
                                    <label><input type="radio" name="course_explanation" value="Tidak Jelas"> Tidak
                                                                                                              Jelas</label>
                                </div>

                                <div class="form-group">
                                    <label>2. Materi yang anda pelajari, yang terbaik penjelasannya adalah materi ?
                                           : </label>
                                    <br>
                                    <select class="form-control" name="best_course">
                                        <option value="">Pilih Materi</option>
                                        @foreach($courses as $course)
                                            <option value="{{ $course->id }}">{{ $course->title }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>3. Materi yang anda pelajari, yang terburuk penjelasannya adalah materi ?
                                           : </label>
                                    <br>
                                    <select class="form-control" name="worst_course">
                                        <option value="">Pilih Materi</option>
                                        @foreach($courses as $course)
                                            <option value="{{ $course->id }}">{{ $course->title }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>4. Saran anda untuk materi ? : </label>
                                    <br>
                                    <textarea class="form-control" name="course_suggestion" id="" cols="30"
                                              rows="10"></textarea>
                                </div>
                            </div>

                            <div class="tab-pane active" id="facility">
                                <div class="form-group">
                                    <label>1. Apakah Hardware Komputer seperti Monitor, Keyboard dan Mouse, apakah
                                           membantu dalama proses belajar ? : </label>
                                    <br>
                                    <label><input type="radio" name="computer_help" value="Sangat Membantu"> Sangat
                                                                                                             Membantu</label>
                                    <br>
                                    <label><input type="radio" name="computer_help" value="Membantu"> Membantu</label>
                                    <br>
                                    <label><input type="radio" name="computer_help" value="Tidak Membantu"> Tidak
                                                                                                            Membantu</label>
                                </div>

                                <div class="form-group">
                                    <label>2. Apakah AC di dalam Kelas cukup membuat anda nyaman dalam belajar ?
                                           : </label>
                                    <br>
                                    <label><input type="radio" name="ac_comfortable" value="Sangat Nyaman"> Sangat
                                                                                                            Nyaman</label>
                                    <br>
                                    <label><input type="radio" name="ac_comfortable" value="Nyaman"> Nyaman</label>
                                    <br>
                                    <label><input type="radio" name="ac_comfortable" value="Tidak Nyaman"> Tidak Nyaman</label>
                                </div>

                                <div class="form-group">
                                    <label>3. Kebersihan pada kelas, menurut anda ? : </label>
                                    <br>
                                    <label><input type="radio" name="class_cleanliness" value="Sangat Bersih"> Sangat
                                                                                                               Bersih</label>
                                    <br>
                                    <label><input type="radio" name="class_cleanliness" value="Bersih"> Bersih</label>
                                    <br>
                                    <label><input type="radio" name="class_cleanliness" value="Tidak Bersih"> Tidak
                                                                                                              Bersih</label>
                                </div>

                                <div class="form-group">
                                    <label>4. Fasilitas Pantry (Teh, Kopi, Gula dan Air) apakah cukup membantu Anda jika
                                           ingin membuat Teh, Kopi atau hanya minum ? : </label>
                                    <br>
                                    <label><input type="radio" name="facility_help" value="Sangat Membantu"> Sangat
                                                                                                             Membantu</label>
                                    <br>
                                    <label><input type="radio" name="facility_help" value="Membantu"> Membantu</label>
                                    <br>
                                    <label><input type="radio" name="facility_help" value="Tidak Membantu"> Tidak
                                                                                                            Membantu</label>
                                </div>

                                <div class="form-group">
                                    <label>5. Saran Anda untuk Fasilitas ? : </label>
                                    <br>
                                    <textarea class="form-control" name="facility_suggestion" cols="30"
                                              rows="10"></textarea>
                                </div>
                            </div>

                            <div class="tab-pane" id="learning_system">
                                <div class="form-group">
                                    <label>1. Apakah Anda ada Masalah dalam login ke Sistem Belajar baik itu di
                                           Babastudio atau online ? : </label>
                                    <br>
                                    <textarea class="form-control" name="login_problem" cols="30" rows="10"></textarea>
                                </div>

                                <div class="form-group">
                                    <label>2. Saran Anda ? : </label>
                                    <br>
                                    <textarea class="form-control" name="learning_system_suggestion" cols="30"
                                              rows="10"></textarea>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="modal-footer">
                        <button class="btn btn-success" type="submit">
                            <i class="fa fa-save"></i>
                            Simpan
                        </button>
                    </div>

                </form>

            </div>
        </div>
    </div>


    <div class="col-md-8 col-lg-6 footer-space">

        @include('layouts.partials-social-learning.birthday')
        @include('layouts.partials-social-learning.broadcast')

        <div id="carouselExampleIndicators" class="carousel slide d-none d-sm-block d-sm-none d-md-block"
             data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">

                @if( !Auth::user()->tenant->banner )
                  <div class="carousel-item active">
                      <img class="d-block w-100" src="/images/nbls-profile-header.JPG" alt="First slide">
                  </div>
                  @else
                  <div class="carousel-item active">
                      <img class="d-block w-100" src="/images/photo/tenant/{{ Auth::user()->tenant->banner }}" alt="First slide">
                  </div>
                @endif
                @if( !Auth::user()->tenant->banner2 )
                  <div class="carousel-item">
                      <img class="d-block w-100" src="/images/nbls-profile-header.JPG" alt="First slide">
                  </div>
                  @else
                  <div class="carousel-item">
                      <img class="d-block w-100" src="/images/photo/tenant/{{ Auth::user()->tenant->banner2 }}" alt="second slide">
                  </div>
                @endif
                @if( !Auth::user()->tenant->banner3 )
                  <div class="carousel-item">
                      <img class="d-block w-100" src="/images/nbls-profile-header.JPG" alt="First slide">
                  </div>
                @else
                  <div class="carousel-item">
                      <img class="d-block w-100" src="/images/photo/tenant/{{ Auth::user()->tenant->banner3 }}" alt="First slide">
                  </div>
                @endif

            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="search-result-box">
                    <ul class="nav nav-tabs">
                        <li class="nav-item col-md-4 p-0 ">
                            <a href="#home" data-toggle="tab" aria-expanded="true" class="nav-link active">
                                <p class="fa fa-comments-o fa-2x"></p>
                                <b>Status</b>
                            </a>
                        </li>
                        <li class="nav-item col-md-4 p-0">
                            <a href="#users" data-toggle="tab" aria-expanded="false" class="nav-link">
                                <p class="fa fa-question fa-2x"></p>
                                <b>Tanya</b>
                            </a>
                        </li>
                        <li class="nav-item col-md-4 p-0">
                            <a href="#other-tab" data-toggle="tab" aria-expanded="false" class="nav-link">
                                <p class="fa fa-sitemap fa-2x"></p>
                                <b>Hasil Karya</b>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="home">

                            <form id="formaddpost" method="post" data-parsley-validate>

                                <div class="row">

                                    <div class="col-md-12">
                                        <h4>Whats Happening?</h4>
                                        <hr>
                                    </div>

                                    <div class="col-md-2 m-b-20">
                                        @if( Auth::user()->photo === null)
                                            <img class="d-flex rounded-circle"
                                                 src="/images/default.png" alt="Generic placeholder image" height="85"
                                                 width="85">
                                        @else
                                            <img class="d-flex rounded-circle"
                                                 src="/images/photo/{{ Auth::user()->photo }}"
                                                 alt="Generic placeholder image" height="85" width="85">
                                        @endif
                                    </div>


                                    <div class="col-md-10 m-b-20">


                                        <fieldset>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                            <input type="hidden" name="log" value="LOG_SHARE_POSTS">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <textarea type="text" name="content" class="form-control" rows="4"
                                                              placeholder="Bagikan sesuatu..." required></textarea>

                                                    <hr>

                                                    <p id="linkPreview"></p>

                                                    <img id="postImage" class="img-fluid" src="#" alt="">
                                                </div>
                                            </div>
                                        </fieldset>

                                    </div>


                                    <div class="col-md-12">
                                        <div class="modal fade" id="modalLink">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">
                                                            Share Link
                                                        </h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-input">
                                                            <label>Link : </label>
                                                            <input id="inputLink" class="form-control" type="url"
                                                                   name="link">
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">

                                                        <button onclick="showLink()" type="button"
                                                                class="btn btn-primary"> Konfirmasi
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <hr>
                                        <div class="row">
                                            <div class="col-md-10">
                                                <ul class="nav nav-pills profile-pills m-t-10">
                                                    <li>
                                                        <a>
                                                            <label style="cursor: pointer" class="fa fa-camera">
                                                                <input id="inputPostImage" type="file" name="photo"
                                                                       hidden>
                                                            </label>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a data-toggle="modal" href="#modalLink">
                                                            <i class="fa fa-link"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div class="col-md-2">
                                                <button type="submit" class="btn btn-primary float-right" name="button">
                                                    Share
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </form>

                        </div>

                        <!-- end All results tab -->


                        <!-- Users tab -->
                        <div class="tab-pane" id="users">

                            <form id="formaddquestion" method="post"
                                  data-parsley-validate>

                                <div class="row">

                                    <div class="col-md-12">
                                        <h4>Any Question?</h4>
                                        <hr>
                                    </div>

                                    <div class="col-md-2 m-b-20">
                                        @if(Auth::user()->photo === null)
                                            <img class="d-flex rounded-circle"
                                                 src="/images/default.png" alt="Generic placeholder image" height="85"
                                                 width="85">
                                        @else
                                            <img class="d-flex rounded-circle"
                                                 src="/images/photo/{{ Auth::user()->photo }}"
                                                 alt="Generic placeholder image" height="85" width="85">
                                        @endif
                                    </div>


                                    <div class="col-md-10 m-b-20">


                                        <fieldset>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                            <input type="hidden" name="material_id" value="0">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <textarea type="text" name="content" class="form-control"
                                                              rows="4"
                                                              placeholder="Any Question" required></textarea>

                                                    <img id="questionImage" class="img-fluid" src="#" alt="">

                                                    <!-- <div class="form-inline">
                                                                <i class="fa fa-diamond mr-2"></i>
                                                    <input type="number" name="gems" value="" placeholder="0" class="form-control mt-2" min="0">
                                                    </div> -->
                                                </div>
                                            </div>
                                        </fieldset>

                                    </div>


                                    <div class="col-md-12">
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-10">
                                                <ul class="nav nav-pills profile-pills m-t-10">
                                                    <li>
                                                        <a>
                                                            <label style="cursor: pointer" class="fa fa-camera">
                                                                <input id="inputQuestionImage" type="file" name="photo"
                                                                       hidden>
                                                            </label>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-2">
                                                <button type="submit" class="btn btn-primary float-right" name="button">
                                                    SEND
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </form>

                        </div>
                        <!-- end Users tab -->

                        <div class="tab-pane" id="other-tab">
                            <p>
                                Hasil Karya Adalah tempat diamana kamu bisa menampilkan apa yang kamu pelajari dan apa
                                yang kamu buat,
                                jika kamu telah menyelesaikan sebuah course ataupun kamu punya suatu karya untuk
                                ditampilkan maka kami bisa
                                post di hasil karya.
                                <a href="/social-learning-masterpiece">Pergi ke hasil karya dan posting karyamu.</a>
                            </p>

                        </div>

                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-12">
                <div class="search-result-box">
                    <ul class="nav nav-tabs">
                        <li class="nav-item col-md-6 p-0">
                            <a href="#timeline" data-toggle="tab" aria-expanded="true" class="nav-link active">
                                <b>TimeLine</b>
                            </a>
                        </li>
                        <li class="nav-item col-md-6 p-0">
                            <a href="#newquestion" data-toggle="tab" aria-expanded="false" class="nav-link">
                                <b>NewQuestion</b>
                            </a>
                        </li>

                    </ul>

                    <!-- timeline -->

                    <div class="tab-content bg-default">
                        <div class="tab-pane active" id="timeline">
                            <div id="postList" class="row">

                                @foreach($posts as $post)

                                    @include('social-learnings.posts.child.post-child')

                                @endforeach


                            </div>

                        </div>

                        <!-- end timelines tab -->
                        <div class="tab-pane active" id="newquestion">
                            <div id="questionList" class="row">
                                @foreach($questions as $question)
                                    @include('social-learnings.posts.child.question-child')
                                @endforeach
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>

        @include('layouts.partials-social-learning.follow-suggested')

    </div>

@endsection

@section('script')
    <script type="text/javascript">

        $(function () {

            if ('{{ Carbon\Carbon::now()->format("z") - Auth::user()->logs()->where("reason", "LOG_LOGIN")->latest()->first()->created_at->format("z")  }}' > 14 && {{ $courses->count() }} > 0) {
                $('#modalItime').modal({
                    backdrop: 'static',
                    keyboard: false
                });
            }

            {{--  if (true) {
                $('#modalItime').modal({
                    backdrop: 'static',
                    keyboard: false
                });
            }  --}}

            $("form#formaddpost").submit(function (e) {
                e.preventDefault();
                var formData = new FormData(this);

                $.ajax({
                    url: '/social-learning-posts',
                    type: 'POST',
                    data: formData,
                    success: function (data) {
                        $('#postList').prepend(data);
                        $("form#formaddpost")[0].reset();
                        $('#linkPreview').replaceWith('<p id="linkPreview"></p>');
                        $('#postImage').replaceWith('<img id="postImage" class="img-fluid" src="#" alt="">');
                        swal("Berhasil !", "Post berhasil ditambahkan", "success");
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });

            });

            $("form#formaddquestion").submit(function (e) {
                e.preventDefault();
                var formData = new FormData(this);

                $.ajax({
                    url: '/social-learning-questions',
                    type: 'POST',
                    data: formData,
                    success: function (data) {
                        $('#questionList').prepend(data);
                        $("form#formaddquestion")[0].reset();
                        $('#questionImage').replaceWith('<img id="questionImage" class="img-fluid" src="#" alt="">');
                        swal("Berhasil !", "Pertanyaan berhasil ditambahkan", "success");
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });

                return false;
            });

            $("form#additime").submit(function (e) {
                e.preventDefault();
                var formData = new FormData(this);

                $.ajax({
                    url: '/social-learning-itime',
                    type: 'POST',
                    data: formData,
                    success: function (data) {
                        console.log(data);
                        $('#modalItime').modal('hide');
                        swal("Berhasil !", "Quissioner berhasil dikirim", "success");
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });

                return false;
            });

            syncCartsData();
        });

        $('#inputPostImage').change(function () {
            if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#postImage').attr('src', e.target.result);
                }

                reader.readAsDataURL(this.files[0]);
            }
        });

        $('#inputQuestionImage').change(function () {
            if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#questionImage').attr('src', e.target.result);
                }

                reader.readAsDataURL(this.files[0]);
            }
        });

        {{--  function onSubmitItime() {
            $.ajax({
                url: '/social-learning-itime',
                type: 'POST',
                data: JSON.stringify(objectifyForm(formData)),
                success: function (data) {
                    $('#modalEdit' + postId).modal('hide');
                    $('#post' + postId).replaceWith(data);
                    swal("Berhasil !", "Post berhasil diedit", "success");
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }  --}}

        function onEditPost(postId) {
            $('#modalEdit' + postId).modal('hide');

            // var content = $('#inputPostContent').val();
            // var link = $('#inputPostLink').val();
            // var photo = $('input#inputPostPhoto')[0].files[0];
            //
            // console.log(content);
            // console.log(link);
            // console.log(photo);
            //
            // var formData = new FormData();

            {{--console.log('{{ csrf_token() }}');--}}

            {{--formData.append('_token', '{{ csrf_token() }}');--}}
            {{--formData.append('content', content);--}}
            {{--formData.append('link', link);--}}
            {{--if (photo != null) {--}}
            {{--formData.append('photo', photo);--}}
            {{--}--}}

            // console.log(formData.get('content'));
            // console.log(formData.get('link'));
            {{--console.log(formData.get('photo'));--}}

            // $.ajax({
            //     url: '/social-learning-posts/' + postId,
            //     type: 'PUT',
            //     data: formData,
            //     success: function (data) {
            //         $('#post' + postId).replaceWith(data);
            //     },
            //     cache: false,
            //     contentType: false,
            //     processData: false
            // });

        }

        function onLike(postId, userId) {
            var formData = new FormData();
            formData.append('log', 'LOG_LIKE_QUESTIONS');
            formData.append('user_id', userId);

            $.ajax({
                url: '/posts/' + postId + '/likes',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#post' + postId).replaceWith(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onDislike(postId, userId) {
            var formData = new FormData();
            formData.append('log', 'LOG_DISLIKE_POSTS');
            formData.append('user_id', userId);

            $.ajax({
                url: '/posts/' + postId + '/dislikes',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#post' + postId).replaceWith(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onComment(postId, userId) {

            var content = $('#postcomment' + postId).val();
            console.log(content);

            var formData = new FormData();
            formData.append('_token', '{{ csrf_token() }}');
            formData.append('content', content);
            formData.append('log', 'LOG_COMMENTS');
            formData.append('user_id', userId);

            $.ajax({
                url: '/posts/' + postId + '/comments',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#post' + postId).replaceWith(data);
                },
                error: function (data) {
                    console.log(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

            return false;

        }

        function onLikeComment(commentId, postId) {
            var formData = new FormData();
            formData.append('log', 'LOG_LIKE_COMMENTS');

            $.ajax({
                url: '/comments/' + commentId + '/likes',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#post' + postId).replaceWith(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onDislikeComment(commentId, postId) {
            var formData = new FormData();
            formData.append('log', 'LOG_DISLIKE_COMMENTS');

            $.ajax({
                url: '/comments/' + commentId + '/dislikes',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#post' + postId).replaceWith(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onDeletePost(postId) {

            swal({
                title: "Apa kamu yakin?",
                text: "Data yang telah di hapus tidak dapat dikembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {

                if (willDelete) {

                    $.ajax({
                        url: '/social-learning-posts/' + postId,
                        type: 'DELETE',
                        data: null,
                        success: function (data) {
                            $('#post' + postId).remove();
                            swal("Berhasil !", "Kiriman berhasil dihapus", "success");
                        },
                        error: function (data) {
                            swal("Oops !", "Gagal menghapus kiriman", "error");
                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    });

                }

            });
        }

        function onDeleteQuestion(postId) {

            swal({
                title: "Apa kamu yakin?",
                text: "Data yang telah di hapus tidak dapat dikembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {

                if (willDelete) {

                    $.ajax({
                        url: '/social-learning-questions/' + postId,
                        type: 'DELETE',
                        data: null,
                        success: function (data) {
                            $('#question' + postId).remove();
                            swal("Berhasil !", "Pertanyaan berhasil dihapus", "success");
                        },
                        error: function (data) {
                            $('#question' + postId).remove();
                            swal("Berhasil !", "Pertanyaan berhasil dihapus", "success");
                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    });

                }

            });
        }

        function objectifyForm(formArray) {//serialize data function

            var returnArray = {};
            for (var i = 0; i < formArray.length; i++) {
                returnArray[formArray[i]['name']] = formArray[i]['value'];
            }
            return returnArray;
        }

        function showLink() {
            var inputLink = $('#inputLink').val();
            console.log(inputLink);
            $('#linkPreview').replaceWith('<a id="linkPreview" target="_blank" href="' + inputLink + '">' + inputLink.substring(0, 30) + '...' + '</>');

            $('#modalLink').modal('hide');
        }

        // Question
        function onQuestionComment(questionId, userId) {
            console.log('Try Submit');
            var content = $('#questioncomment' + questionId).val();

            var formData = new FormData();
            formData.append('_token', '{{ csrf_token() }}');
            formData.append('content', content);
            formData.append('log', 'LOG_COMMENTS');
            formData.append('user_id', userId);

            $.ajax({
                url: '/social-learning-questions/' + questionId + '/comments',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#question' + questionId).replaceWith(data);
                },
                error: function (data) {
                    console.log(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onQuestionLike(questionId, userId) {
            var formData = new FormData();
            formData.append('log', 'LOG_LIKE_QUESTIONS');
            formData.append('user_id', userId);

            $.ajax({
                url: '/social-learning-questions/' + questionId + '/likes',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#question' + questionId).replaceWith(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onQuestionDislike(questionId, userId) {
            var formData = new FormData();
            formData.append('log', 'LOG_DISLIKE_QUESTIONS');
            formData.append('user_id', userId);

            $.ajax({
                url: '/social-learning-questions/' + questionId + '/dislikes',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#question' + questionId).replaceWith(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onQuestionLikeComment(commentId, questionId) {
            var formData = new FormData();
            formData.append('log', 'LOG_LIKE_COMMENTS');

            $.ajax({
                url: '/social-learning-questions/comments/' + commentId + '/likes',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#question' + questionId).replaceWith(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onQuestionDislikeComment(commentId, questionId) {
            var formData = new FormData();
            formData.append('log', 'LOG_DISLIKE_COMMENTS');

            $.ajax({
                url: '/social-learning-questions/comments/' + commentId + '/dislikes',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#question' + questionId).replaceWith(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onFollow(userId) {

            var formData = new FormData();

            formData.append('_token', '{{ csrf_token() }}');
            formData.append('log', 'LOG_FOLLOW');
            formData.append('following_id', {{ Auth::id() }});

            $.ajax({
                url: '/users/' + userId + '/follow',
                type: 'POST',
                data: formData,
                success: function (data) {
                    console.log(data);
                    $('#suggestion' + userId).remove();
                    swal("Berhasil !", "Berhasil mengikuti", "success");
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }

        function onUnfollow(userId) {

            var formData = new FormData();
            formData.append('_token', '{{ csrf_token() }}');
            formData.append('log', 'LOG_UNFOLLOW');

            $.ajax({
                url: '/users/' + userId + '/unfollow',
                type: 'POST',
                data: formData,
                success: function (data) {
                    console.log(data);
                    $('#followUser').replaceWith(data);
                    swal("Berhasil !", "Batal mengikuti", "success");
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }

        function syncCartsData()
        {

            var formData = new FormData();
            var cartsData = window.localStorage.getItem('carts');
            var packageCartsData = window.localStorage.getItem('package_carts');

            console.log(packageCartsData);

            if(cartsData !== null || packageCartsData !== null)
            {
                formData.append('_token', '{{ csrf_token() }}');
                formData.append('carts_data', cartsData);
                formData.append('package_carts_data', packageCartsData);

                console.log(formData.get('_token'));

                $.ajax({
                    url: '/sync/carts',
                    type: 'POST',
                    data: formData,
                    success: function (data) {
                        
                        window.localStorage.removeItem('carts');
                        window.localStorage.removeItem('package_carts');

                        console.log('Cart synchronize success');
                        window.location.replace('{{ env("APP_URL") }}' + '/social-learning-payment');
                    },
                    error: function (error) {
                        window.location.replace('{{ env("APP_URL") }}' + '/social-learning-payment');
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }

        }
    </script>
@endsection
