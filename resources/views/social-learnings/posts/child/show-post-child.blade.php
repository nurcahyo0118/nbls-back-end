
<div id="post{{ $post->id }}" style="width:100%">
    <div class="m-t-20">
        <div class="card-box">
            <div class="media">
                  <a href="{{ route('social-learning-posts.show', [$_SERVER['HTTP_HOST'], $post->user->username]) }}">
                    @if($post->user->photo === null)
                        <img class="d-flex mr-3 rounded-circle thumb-sm"
                             src="/images/default.png" alt="Generic placeholder image">
                    @else
                        <img class="d-flex mr-3 rounded-circle thumb-sm"
                             src="/images/photo/{{ $post->user->photo }}" alt="Generic placeholder image">
                    @endif
                </a>

                <div class="media-body">

                    @if($post->user->id === Auth::id())
                        <span class="media-meta pull-right" href="#"
                              role="button" id="dropdownMenuLink" data-toggle="dropdown"
                              aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-v"></i>
                        </span>

                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <a class="dropdown-item" data-toggle="modal"
                               data-target="#modalEdit{{ $post->id }}">Edit
                            </a>
                            <a class="dropdown-item" onclick="onDeletePost({{ $post->id }})">Delete
                            </a>
                        </div>
                    @endif

                    <h4 class="text-primary font-16 m-0">
                        <a href="{{ route('social-learning-posts.show', [$_SERVER['HTTP_HOST'], $post->user->username]) }}">
                            {{ $post->user->fullname }}
                        </a>
                        <small class="text-muted">
                            <i class="ion-clock"></i>
                            {{ $post->created_at->diffForHumans() }}
                        </small>
                    </h4>

                </div>
            </div>
            <p>
                @if($post->photo === null)
                @else
                    <img class="img-fluid"
                         src="/images/photo/{{ $post->photo }}" alt="Generic placeholder image">
                @endif
            </p>
            <p>{{ $post->content }}</p>

            @php
            $link = App\LinkPostSocialLearning::where('post_id', $post->id)->first();
            @endphp

            @if($link != null)
                <hr>
                <a target="_blank" href="{{ $link->link }}">
                    {{ str_limit($link->link, 30) }}
                </a>
            @endif

            <hr>

            <div class="row">
                <div class="col-md-12">
                    <a class="post-icon" onclick="onLike({{ $post->id }}, {{ $post->user_id }})">
                        <i class="fa fa-thumbs-o-up {{ $post->likes()->where('user_id', Auth::id())->count() ? 'like' : '' }}">
                            {{ $post->likes()->count() }}
                        </i>
                    </a>

                    <a class="post-icon" onclick="onDislike({{ $post->id }}, {{ $post->user_id }})">
                        <i class="fa fa-thumbs-o-down {{ $post->dislikes()->where('user_id', Auth::id())->count() ? 'dislike' : '' }}">
                            {{ $post->dislikes()->count() }}
                        </i>
                    </a>

                    <a class="pull-right post-icon">
                        <p class="fa fa-comments-o"> {{ $post->postComments()->count() }} </p>
                    </a>
                </div>

            </div>
            <hr>

            <div>

                @foreach($post->postComments as $comment)
                    <div class="comment" style="background-colot:p ">
                        @if( Auth::user()->photo === null)
                            <img src="/images/default.png" alt="" class="comment-avatar">
                        @else
                            <img src="/images/photo/{{ Auth::user()->photo }}" alt="" class="comment-avatar">
                        @endif

                        <div class="comment-body">
                            <div class="comment-text">
                                <div class="comment-header">
                                    <a href="#" title="">{{ $comment->user->fullname }}</a>
                                    <span>about {{ $comment->created_at->diffForHumans() }}</span>
                                </div>
                                {{ $comment->body }}
                            </div>
                            <div class="comment-footer">
                                <a onclick="onLikeComment({{ $comment->id }}, {{ $post->id }})">
                                    <i class="fa fa-thumbs-o-up {{ $comment->likes()->where('user_id', Auth::id())->count() ? 'like' : '' }}"></i>
                                    {{ $comment->likes()->count() }}
                                </a>
                                &nbsp;
                                <a onclick="onDislikeComment({{ $comment->id }}, {{ $post->id }})">
                                    <i class="fa fa-thumbs-o-down {{ $comment->dislikes()->where('user_id', Auth::id())->count() ? 'dislike' : '' }}"></i>
                                    {{ $comment->dislikes()->count() }}
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach

                <div class="media m-b-30">

                    @if(Auth::user()->photo === null)
                        <img class="d-flex mr-3 rounded-circle thumb-sm"
                             src="/images/default.png" alt="Generic placeholder image">
                    @else
                        <img class="d-flex mr-3 rounded-circle thumb-sm"
                             src="/images/photo/{{ Auth::user()->photo }}" alt="Generic placeholder image">
                    @endif

                    <div class="media-body">
                        <form id="formaddcomment" onsubmit="onComment({{ $post->id }}, {{ $post->user_id }});return false;"
                              method="post" data-parsley-validate>

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <input id="postcomment{{ $post->id }}" type="text" name="content" class="form-control"
                                   placeholder="Tulis komentar...">

                        </form>

                    </div>
                </div>
            </div>

        </div>
    </div>

</div>

<!-- Modal -->
<div class="modal fade" id="modalEdit{{ $post->id }}" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Status</h5>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">

                <form id="formeditpost{{ $post->id }}" onsubmit="onEditPost({{ $post->id }});return false;" method="post" enctype="multipart/form-data"
                      data-parsley-validate>

                    <div class="modal-body">
                        <fieldset>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <input name="_method" type="hidden" value="PUT">

                            <input type="hidden" name="post_id" value="{{ $post->id }}">

                            <div class="col-md-12">
                                <div class="form-group">
                                  <textarea id="inputPostContent" type="text" name="content"
                                            class="form-control"
                                            rows="4">{{ $post->content }}</textarea>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Link : </label>
                                    <input id="inputPostLink" type="url" name="content" class="form-control" value="{{ $post->link['link'] }}">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <label>Photo : </label>
                                <div class="form-group">

                                    <input type="file" name="photo" id="inputPostPhoto" accept="image/*">
                                </div>
                            </div>

                        </fieldset>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal">
                            <i class="fa fa-close"></i>
                            Tutup
                        </button>
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-save"></i>
                            Simpan
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
