<!-- Modal -->
<div class="modal fade" id="modalEditQuestion{{ $question->id }}" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Status</h5>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">

                <form id="formeditpost{{ $question->id }}"
                      action="{{ route('social-learning-questions.update', $question->id) }}"
                      method="post"
                      enctype="multipart/form-data"
                      data-parsley-validate>

                    <div class="modal-body">
                        <fieldset>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <input type="hidden" name="post_id" value="{{ $question->id }}">

                            <input type="hidden" name="_method" value="put">

                            <div class="col-md-12">
                                <div class="form-group">
                                  <textarea id="inputPostContent"
                                            name="content"
                                            class="form-control"
                                            rows="4">{{ $question->content }}</textarea>
                                </div>
                            </div>

                            {{--<div class="col-md-12">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label>Link : </label>--}}
                                    {{--<input id="inputPostLink" type="url" name="link" class="form-control"--}}
                                           {{--value="{{ $question->link['link'] }}">--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="col-md-12">--}}
                                {{--<label>Photo : </label>--}}
                                {{--<div class="form-group">--}}

                                    {{--<input type="file" name="photo" id="inputPostPhoto" accept="image/*">--}}
                                {{--</div>--}}
                            {{--</div>--}}

                        </fieldset>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal">
                            <i class="fa fa-close"></i>
                            Tutup
                        </button>
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-save"></i>
                            Simpan
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>