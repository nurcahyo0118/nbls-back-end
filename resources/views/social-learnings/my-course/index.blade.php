@extends('layouts.frontend')

@section('content')
    <!-- isi tengah -->

    <div class="col-md-8 col-lg-6 footer-space">

        @include('layouts.partials.message')

        <h6 class="text-muted">
            <span class="fa fa-home"></span>
            Courses
            <span class="fa fa-long-arrow-right"></span>
            My Courses
        </h6>

        <div class="row">
            <div class="col-md-2 text-muted">
                Filter by
            </div>

            <div class="col-md-5">
                <div class="form-group">
                    <select class="form-control" id="exampleFormControlSelect1">
                        <option>All</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>
                </div>

            </div>

            <div class="col-md-5">
                <div class="input-group">
                    <input id="" type="text" class="form-control" placeholder="Search">
                    <span class="btn btn-primary">
                        <i class="fa fa-search"></i>
                    </span>
                </div>
            </div>

        </div>

        <div class="row">

        @foreach(Auth::user()->courses as $course)
            <!-- card Kursus -->
                <div class="col-md-6 m-t-20">
                    <div class="card">
                        @if($course->image === null)
                            <img class="card-img-top" src="/images/order/web_design.jpg" alt="Card image cap">
                        @else
                            <img class="card-img-top" src="/xxcourses/images/{{ $course->image }}" alt="Card image cap" height="200px">
                        @endif
                        <div class="card-body">
                            <p class="text-muted">
                                <small>By {{ $course->author->fullname }} |
                                    <i class="ion-clock"></i> {{ $course->updated_at->diffForHumans() }}
                                </small>
                            </p>
                            <h6><strong>{{ $course->title }}</strong></h6>
                            <p>
                                <i class="fa fa-users"> {{ $course->users->count() }} Siswa</i>
                            </p>
                            {{--  <hr>  --}}
                            {{--  <p>Sisa Waktu Belajar : 300 hari</p>  --}}
                            {{--  <div class="progress">
                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="95"
                                     aria-valuemin="0" aria-valuemax="100" style="width: %;">
                                    <span class="sr-only">95% Complete</span>
                                </div>
                            </div>  --}}
                        </div>

                        <div class="card-footer text-center btn-primary btn-block">
                            <a class="text-white"
                               href="{{ route('social-learning-my-course.show', [$_SERVER['HTTP_HOST'], $course->id]) }}">
                                VIEW DETAILS
                                <i class="md-arrow-forward"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- end card Kursus -->
            @endforeach

        </div>


    </div>

    <!-- akhir isi tengah -->
@endsection
