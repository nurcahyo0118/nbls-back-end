@php
$tenant = NULL;

$tenant = App\Tenant::where(['domain' => $_SERVER['HTTP_HOST']])->first();

if($tenant === null)
{
    $explode = explode('.', $_SERVER['HTTP_HOST']);
    $subdomain = array_shift($explode);

    $tenant = App\Tenant::where('subdomain', $subdomain)->first();
}

@endphp
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>NBLS - Babastudio</title>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link href="/baba-custom/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/baba-custom/css/custom.css" rel="stylesheet">
    <link href="/baba-custom/lib/animate/animate.min.css" rel="stylesheet">
    <link href="/baba-custom/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="/baba-custom/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="/baba-custom/lib/lightbox/css/lightbox.min.css" rel="stylesheet">
    <link href="/baba-custom/css/style.css" rel="stylesheet">
</head>
<body>


@include('layouts.partials-baba-custom.header')

    <!-- section 1 -->
    <section id="intro">
        <div class="intro-container">
            <div id="introCarousel" class="carousel  slide carousel-slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <div class="carousel-background">
                        @if($tenant->banner === null )
                        <img src="/baba-custom/images/1.jpg" alt="">
                        @else
                        <img src="/images/photo/tenant/{{ $tenant->banner }}" alt="">
                        @endif
                    <div class="carousel-item active">

                    </div>
                    </div>
                    <div class="carousel-container">
                        <div class="carousel-content">
                        <h2><span class="f-orange f-slim mt-2">KURSUS</span> <br/><span class="f-orange f-slim">ONLINE HYBRID</span><span class="f-white"> NBLS</span></h2>
                        <h4 class="f-white mb-5 f-slim">Kursus Online dengan Live</h4>
                        <div class="col-md-6 float-left">
                            <img src="/baba-custom/images/ditemani.jpg" class="img-fluid img-caption">
                            <h5 class="f-white f-slim t-slide">Belajar</h5>
                            <h5 class="f-white">DITEMANI & DIBIMBING</h5>
                        </div>
                        <div class="col-md-6 float-left">
                            <img src="/baba-custom/images/kur.jpg" class="img-fluid img-caption">
                            <h5 class="f-white f-slim t-slide">Pertanyaan</h5>
                            <h5 class="f-white">DIJAWAB REALTIME</h5>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="carousel-background">
                        @if($tenant->banner2 === null )
                        <img src="/baba-custom/images/1.jpg" alt="">
                        @else
                        <img src="/images/photo/tenant/{{ $tenant->banner2 }}" alt="">
                        @endif
                    </div>
                    <div class="carousel-container">
                        <div class="carousel-content">
                        <h2><span class="f-orange f-slim">KURSUS</span> <br/><span class="f-orange f-slim">ONLINE HYBRID</span><span class="f-white"> NBLS</span></h2>
                        <h4 class="f-white mb-5 f-slim">Kursus Online dengan Live</h4>
                        <div class="col-md-6 float-left">
                            <img src="/baba-custom/images/ditemani.jpg" class="img-fluid img-caption">
                            <h5 class="f-white f-slim t-slide">Belajar</h5>
                            <h4 class="f-white">ditemani & dibimbing</h4>
                        </div>
                        <div class="col-md-6 float-left">
                            <img src="/baba-custom/images/kur.jpg" class="img-fluid img-caption">
                            <h5 class="f-white f-slim t-slide">Belajar</h5>
                            <h4 class="f-white">ditemani & dibimbing</h4>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="carousel-background">
                        @if($tenant->banner3 === null )
                        <img src="/baba-custom/images/1.jpg" alt="">
                        @else
                        <img src="/images/photo/tenant/{{ $tenant->banner3 }}" alt="">
                        @endif
                    </div>
                    <div class="carousel-container">
                        <div class="carousel-content">
                            <h2><span class="f-orange f-slim">KURSUS</span> <br/><span class="f-orange f-slim">ONLINE HYBRID</span><span class="f-white"> NBLS</span></h2>
                            <h4 class="f-white mb-5 f-slim">Kursus Online dengan Live</h4>
                            <div class="col-md-6 float-left">
                            <img src="/baba-custom/images/ditemani.jpg" class="img-fluid img-caption">
                            <h5 class="f-white f-slim t-slide">Belajar</h5>
                            <h4 class="f-white">ditemani & dibimbing</h4>
                            </div>
                            <div class="col-md-6 float-left">
                            <img src="/baba-custom/images/kur.jpg" class="img-fluid img-caption">
                            <h5 class="f-white f-slim t-slide">Belajar</h5>
                            <h4 class="f-white">ditemani & dibimbing</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#introCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon ion-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#introCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon ion-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
            </a>
            </div>
        </div>
    </section>
    <!-- end section 1 -->
    <!-- main -->
    <main id="main">
        <!-- section 2 -->
        <section id="clients" class="bg-grey">
            <div class="container media row offset-md-1">
            <div class="col-md-4 mt-3">
                <h2 style="margin-bottom:-1em;">TELAH DILIPUT</h2>
                <br/>
                <h2 class="f-blue f-bold">DI MEDIA TELEVISI</h2>
            </div>
            <div class="col-md-2 col-sm-12 mb-2">
                <div class="card">
                    <div class="card-body"><img src="/baba-custom/images/metrologo.png"></div>
                </div>
            </div>
            <div class="col-md-2 col-sm-12 mb-2">
                <div class="card">
                    <div class="card-body"><img src="/baba-custom/images/kompaslogo.png"></div>
                </div>
            </div>
            <div class="col-md-2 col-sm-12 mb-2">
                <div class="card">
                    <div class="card-body"><img src="/baba-custom/images/mnclogo.png"></div>
                </div>
            </div>
            <div class="col-md-2 col-sm-12 mb-2">
                <div class="card">
                    <div class="card-body"><img src="/baba-custom/images/tvonelogo.png"></div>
                </div>
            </div>
            </div>
        </section>
        <!-- end section 2 -->
        <!-- section 3 -->
        <section id="services" class="bg-white">
            <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 bg-blue f-white">
                    <h2 class="title mb-5"><span class="f-orange f-slim">LIPUTAN</span>
                        <br/><span>MEDIA</span>
                    </h2>
                    <h4 class="mb-3" >Liputan Babastudio E-learning di kompas TV bersama Alumni</h4>
                    <p class="description mb-5">Kompas TV meliput Babastudio E-learning karena, memberikan kursus gratis pada lebih dari 500 orang setiap harinya. </p>
                    <div class="video-container mb-5">
                        <iframe src="https://www.youtube.com/embed/zgbFOxI4ROw" frameborder="0" width="560" height="315"></iframe>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 pt-5" >
                    <div class="video-container mt-5">
                        <iframe src="https://www.youtube.com/embed/zgbFOxI4ROw" frameborder="0" width="560" height="315"></iframe>
                    </div>
                    <h4 class="mt-5 mb-4" >Membantu Pusta Intel AD & AU Menguasai SEO & Digital Marketing</h4>
                    <p class="description mt-2">Kompas TV meliput Babastudio E-learning karena, memberikan kursus gratis pada lebih dari 500 orang setiap harinya.</p>
                </div>
            </div>
            </div>
        </section>
        <!-- end section 3 -->
        <!-- section 4 -->
        <section id="services" class="bg-image">
            <div class="container">
            <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 bg-grey">
                <h2 class="title2"> <br/> <span class="">KENAPA</span>
                    <span class="f-blue f-slim">HYBRID LEARNING</span><br/><span>Dapat membantu Anda</span>
                </h2>
                <!-- Vertical Timeline -->
                <section id="conference-timeline">
                    <div class="conference-center-line"></div>
                    <div class="conference-timeline-content">
                        <div class="timeline-article mb-4">
                        <div class="content-left-container">
                            <div class="content-left">
                                <img src="/baba-custom/images/kur.jpg" class="img-fluid">
                            </div>
                        </div>
                        <div class="content-right-container">
                            <div class="content-right">
                                <h5>INSTRUKTUR SAMA</h5>
                                <p>Instruktur SAMA PERSIS dengan kelas Reguler tatap muka di babastudio</p>
                            </div>
                        </div>
                        <div class="meta-date">
                        </div>
                        </div>
                        <div class="timeline-article mb-4">
                        <div class="content-left-container">
                            <div class="content-left">
                                <img src="/baba-custom/images/materi.jpg" class="img-fluid">
                            </div>
                        </div>
                        <div class="content-right-container">
                            <div class="content-right">
                                <h5>MATERI SAMA</h5>
                                <p>Materi SAMA PERSIS dengan kelas Reguler tatap muka di babastudio</p>
                            </div>
                        </div>
                        <div class="meta-date">
                        </div>
                        </div>
                        <div class="timeline-article">
                        <div class="content-left-container">
                            <div class="content-left">
                                <img src="/baba-custom/images/ditemani.jpg" class="img-fluid">
                            </div>
                        </div>
                        <div class="content-right-container">
                            <div class="content-right">
                                <h5>SERTIFIKAT SAMA</h5>
                                <p>Sertifikat SAMA PESIS dengan kelas Regular tatap muka di babastudio</p>
                            </div>
                        </div>
                        <div class="meta-date">
                        </div>
                        </div>
                    </div>
                </section>
            </div>
            </div>
        </section>
        <!-- end section 4 -->
        <!-- section 5 -->
        <section id="services" class="bg-white">
            <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 bg-white f-white">
                    <!-- <div class="icon"><i class="ion-ios-analytics-outline"></i></div> -->
                    <div class="video-container mb-5 mt-5">
                        <iframe src="https://www.youtube.com/embed/zgbFOxI4ROw" frameborder="0" width="560" height="315"></iframe>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 pt-5" >
                    <div class="services-top">
                        <div class="container bootstrap snippet">
                        <div class="row text-left">
                            <div class="col-sm-12 col-md-12 col-md-12">
                                <h2>APA ITU</h2>
                                <h2><b>LIVE INSTRUKTUR</b></h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-offset-1 col-sm-12 col-md-12 col-md-10">
                                <div class="services-list">
                                    <div class="row">
                                    <div class="col-sm-12 col-md-12 col-md-12">
                                        <div class="service-block" style="visibility: visible;">
                                            <div class="ico highlight mr-2 mb-2"><img src="/baba-custom/images/icon1.png" class="img-fluid"></div>
                                            <div class="text-block">
                                                <div class="text">Akan memastikan Anda mudah menguasai Skill yang Anda pelajari kapanpun Anda bertanya pada masa belajar, Instruktur akan menjawab secara realtime.</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-md-12">
                                        <div class="service-block" style="visibility: visible;">
                                            <div class="ico highlight mr-2 mb-2"><img src="/baba-custom/images/icon2.png" class="img-fluid"></div>
                                            <div class="text-block">
                                                <div class="text">Jika ada masalah dalam bahasa pemrograman Anda, Instruktur akan LIVE mngkoreksi kesalahan Anda. Progress Anda akan di Bimbing secara menyeluruh.</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-md-12">
                                        <div class="service-block" style="visibility: visible;">
                                            <div class="ico highlight mr-2 mb-2"><img src="/baba-custom/images/icon3.png" class="img-fluid"></div>
                                            <div class="text-block">
                                                <div class="text">Anda pun dapat berkomunitas dan Berkomunikasi dengan para Murid dan Alumni babastudio.</div>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </section>
        <!-- end section 5 -->
        <!-- section 6 -->
        <section id="about" class="bg-grey">
            <div class="container">
            <header class="section-header">
                <h3 class="text-center mb-5">Hanya 1 Harga dapatkan Seluruh paket Kursus <br/>babastudio ini senilai 27 Juta</h3>
            </header>
            <div class="row about-cols">

                @php
                    $course_packages = App\CoursePackage::inRandomOrder()->take(3)->get();
                @endphp

                @foreach($course_packages as $package)
                <div class="col-md-4">
                    <div class="about-col">
                        <div class="card h-100">
                            <div class="img">
                            @if($package->image === null)
                                <img class="img-fluid" src="/images/order/web_design.jpg" style="height: 200px" alt="Card image cap">
                            @else
                                <img class="img-fluid" src="/course-package/images/{{ $package->image }}" style="height: 200px" alt="Card image cap">
                            @endif
                                <!-- <img src="/baba-custom/images/game.jpg" alt="" class="img-fluid"> -->
                            </div>
                            <div class="card-body mt-3">
                                <small class="text-muted ml-3">
                                <i class="fa fa-calendar ico" aria-hidden="true"></i>
                                    {{ $package->created_at->diffForHumans() }}
                                </small>
                                <h2 class="title ml-3">
                                <a href="#" class="f-blue f-slim">{{ str_limit($package->title, 25) }}</a>
                                </h2>
                                <p>
                                {{ str_limit($package->description, 30) }}
                                </p>
                            </div>
                            <div class="card-footer">
                                <h5 class="float-left mt-1 ml-2">{{ str_limit($package->user->fullname, 13) }}</h5>
                                <a href="{{ route('course-package-frontend.show', $package->id) }}" class="btn btn-primary ml-2 pull-right">Lihat Detail</a>
                            </div>
                        </div>
                    </div>
                </div>
              @endforeach

            </div>
            <div class="container mt-3">
                <div class="col-md-3 mx-auto">
                <a href="{{ route('course-package-frontend.index') }}" class="btn btn-primary btn-lg ">LIHAT SELENGKAPNYA</a>
                </div>
            </div>
            </div>
        </section>
        <!-- end section 6 -->
        <!-- section 7 -->
        <section id="services" class="bg-white">
            <div class="container">
            <header class="section-header mt-5">
                <h3 class="text-center mb-4">Kami telah membantu lebih dari 21000  <br/>alumni untuk mengupgrade skill nya</h3>
                <h3 class="text-center mb-4 f-blue f-bold">Apa Kata Mereka ?</h3>
            </header>
            <div class="row">
                <div class="col-lg-6 col-md-6" >
                    <div class="services-top">
                        <div class="container bootstrap snippet">
                        <div class="row text-left">
                            <div class="col-sm-12 col-md-12 col-md-12">
                                <h3>21 Ribu Murid dan Bertambah terus</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-offset-1 col-sm-12 col-md-12 col-md-10">
                                <div class="services-list murid scrollbar" id="style-1">
                                    <div class="row">
                                    <div class="col-sm-12 col-md-12 col-md-12">
                                        <div class="service-block" style="visibility: visible;">
                                            <div class="mr-2 foto"><img src="/baba-custom/images/pp.jpg" class="img-fluid" width="70em"></div>
                                            <div class="text-block">
                                                <div class="name">M. Saepul Bahri</div>
                                                <div class="info pl-1 ">Programmer & Graphic Designer</div>
                                                <div class="text desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-md-12">
                                        <div class="service-block" style="visibility: visible;">
                                            <div class="mr-2 foto"><img src="/baba-custom/images/pp.jpg" class="img-fluid" width="70em"></div>
                                            <div class="text-block">
                                                <div class="name">M. Saepul Bahri</div>
                                                <div class="info pl-1">Programmer & Graphic Designer</div>
                                                <div class="text desc" >Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-md-12">
                                        <div class="service-block" style="visibility: visible;">
                                            <div class="mr-2 foto"><img src="/baba-custom/images/pp.jpg" class="img-fluid" width="70em"></div>
                                            <div class="text-block">
                                                <div class="name">M. Saepul Bahri</div>
                                                <div class="info pl-1">Programmer & Graphic Designer</div>
                                                <div class="text desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-md-12">
                                        <div class="service-block" style="visibility: visible;">
                                            <div class="mr-2 foto"><img src="/baba-custom/images/pp.jpg" class="img-fluid" width="70em"></div>
                                            <div class="text-block">
                                                <div class="name">M. Saepul Bahri</div>
                                                <div class="info pl-1">Programmer & Graphic Designer</div>
                                                <div class="text desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames </div>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 bg-white">
                    <h3>Apa Kata Murid Babastudio ?</h3>
                    <div class="video-container mb-5">
                        <iframe src="https://www.youtube.com/embed/zgbFOxI4ROw" frameborder="0" width="560" height="315"></iframe>
                    </div>
                </div>
            </div>
            </div>
        </section>
        <!-- end section 7 -->
    </main>
    <!-- end main -->

@include('layouts.partials-baba-custom.footer')

    <script src="/baba-custom/vendor/jquery/jquery.min.js"></script>
    <script src="/baba-custom/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="/baba-custom/js/custom.js"></script>
    <script src="/baba-custom/lib/jquery/jquery-migrate.min.js"></script>
    <script src="/baba-custom/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="/baba-custom/lib/easing/easing.min.js"></script>
    <script src="/baba-custom/lib/superfish/hoverIntent.js"></script>
    <script src="/baba-custom/lib/superfish/superfish.min.js"></script>
    <script src="/baba-custom/lib/wow/wow.min.js"></script>
    <script src="/baba-custom/lib/waypoints/waypoints.min.js"></script>
    <script src="/baba-custom/lib/counterup/counterup.min.js"></script>
    <script src="/baba-custom/lib/owlcarousel/owl.carousel.min.js"></script>
    <script src="/baba-custom/lib/isotope/isotope.pkgd.min.js"></script>
    <script src="/baba-custom/lib/lightbox/js/lightbox.min.js"></script>
    <script src="/baba-custom/lib/touchSwipe/jquery.touchSwipe.min.js"></script>
    <script src="/baba-custom/js/main.js"></script>

    <script>
        function addToCart(course_id)
        {
            var carts = [];
            var carts_from_local = JSON.parse(window.localStorage.getItem('carts'));

            if(carts_from_local === null)
            {
                carts.push(course_id);
                window.localStorage.setItem('carts', JSON.stringify(carts));
            }
            else
            {
                if(carts_from_local.indexOf(course_id) < 0)
                {
                    carts_from_local.push(course_id);
                    window.localStorage.setItem('carts', JSON.stringify(carts_from_local));
                }

            }
        }

        function removeFromCart(course_id)
        {
            var carts_from_local = JSON.parse(window.localStorage.getItem('carts'));
            var index = carts_from_local.indexOf(course_id);
            if(carts_from_local !== null && index > -1)
            {
                carts_from_local.splice(index, 1);
                window.localStorage.setItem('carts', JSON.stringify(carts_from_local));

                // remove item cart
                $('#cartsItem' + course_id).remove();
                $('#cartsItemDivider' + course_id).remove();

                var carts_from_local_latest = JSON.parse(window.localStorage.getItem('carts'));
                if(carts_from_local_latest.length === 0)
                {
                    $('#cartsList').replaceWith('<div id="cartsList" class="col-sm-12 col-md-12 col-md-12 text-center" style="color: #424242">Kosong !</div>');
                }
            }
        }

        function loadCartData(carts)
        {
            var url = '/loads/frontend/carts?';

            if(carts !== null && carts.length !== 0)
            {
                for(i = 0; i < carts.length; i++)
                {
                    url = url + '&carts[]=' + carts[i];
                    console.log(url);
                }

                $.ajax({
                    type: 'GET',
                    url: url,
                    success: function (response) {
                        $('#cartsList').replaceWith(response.view);
                    },
                    error: function (error) {
                        console.log('ddddd');
                        $('#cartsList').replaceWith('<div id="cartsList" class="col-sm-12 col-md-12 col-md-12 text-center" style="color: #424242">Kosong !</div>');
                    }
                });
            }
            else
            {
                $('#cartsList').replaceWith('<div id="cartsList" class="col-sm-12 col-md-12 col-md-12 text-center" style="color: #424242">Kosong !</div>');
            }

        }
    </script>
</body>
</html>
