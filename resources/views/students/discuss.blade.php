@extends('layouts.main')

@section('content')

    @php
        {{--  $discuss = Auth::user()->discuss;
        $permission = App\Permission::where(['role_id' => Auth::user()->role['id'], 'comment_id' => $discuss->id])->first();  --}}
    @endphp

    <section class="content-header">
        <h1>
            Daftar Diskusi
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="#">
                    <i class="fa fa-dashboard"></i>
                    Dashboard
                </a>
            </li>
            <li class="active">
                <a href="#">
                    <i class="fa fa-building"></i>
                    Komentar Video
                </a>
            </li>
        </ol>
    </section>

    <section class="content">

        <div class="row">

            <div class="col-xs-12">

                @foreach ($errors->all() as $error)
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span
                                    aria-hidden="true">&times;
                            </span>
                        </button>
                        {{ $error }}
                    </div>
                @endforeach

                @include('layouts.partials.message')

                <div class="box box-primary">
                    <div class="box-body">
                        <table class="table table-export">
                            <thead>
                            <tr>
                                <th>Nama Murid</th>
                                <th>Jawaban</th>
                                <th>Essay (Kursus)</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($discussion as $discuss)
                                <tr>
                                    <td>{{ App\User::find($discuss->user_id)->fullname }}</td>
                                    <td>{{ str_limit($discuss->body, 20) }}</td>
                                    <td>{{ $discuss->essay->title }}
                                        <b>({{ $discuss->essay->section->course->title }})</b></td>
                                    <td class="pull-right">
                                        <a href="{{ route('social-learning-my-course.essay', [$_SERVER['HTTP_HOST'], $discuss->essay->section->course->id, $discuss->essay_id]) }}" target="_blank" class="btn btn-info btn-xs">
                                            <i class="fa fa-eye"></i>
                                            Lihat
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>

                </div>

            </div>
        </div>

    </section>

@endsection

@section('script')
    <script>
        customDomain();

        function customDomain() {
            document.getElementById('subdomain').style.display = 'none';
            document.getElementById('domain').style.display = 'block';
        }

        function customSubdomain() {
            document.getElementById('subdomain').style.display = 'block';
            document.getElementById('domain').style.display = 'none';
        }
    </script>
@endsection
