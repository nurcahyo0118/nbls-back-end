@extends('layouts.main')

@section('content')

    @php
        {{--  $quiz = Auth::user()->quiz;
        $permission = App\Permission::where(['role_id' => Auth::user()->role['id'], 'comment_id' => $quiz->id])->first();  --}}
    @endphp

    <section class="content-header">
        <h1>
            Daftar Jawaban Kuis
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="#">
                    <i class="fa fa-dashboard"></i>
                    Dashboard
                </a>
            </li>
            <li class="active">
                <a href="#">
                    <i class="fa fa-building"></i>
                    Komentar Video
                </a>
            </li>
        </ol>
    </section>

    <section class="content">

        <div class="row">

            <div class="col-xs-12">

                @foreach ($errors->all() as $error)
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span
                                    aria-hidden="true">&times;
                            </span>
                        </button>
                        {{ $error }}
                    </div>
                @endforeach

                @include('layouts.partials.message')

                <div class="box box-primary">
                    <div class="box-body">
                        <table class="table table-export">
                            <thead>
                            <tr>
                                <th>Nama Murid</th>
                                <th>Jawaban</th>
                                <th>Materi (Kursus)</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($quiz_answers as $answer)
                                @php
                                    $student = App\User::find($answer->user_id);
                                    $quiz = App\Quiz::find($answer->pivot->quiz_id);
                                @endphp
                                <tr>
                                    <td>{{ $answer->fullname }}</td>
                                    <td>{{ $answer->pivot->answer }}</td>
                                    <td>
                                        {{ $quiz->material->title . ' (' . $quiz->material->section->course->title . ')' }}
                                    </td>
                                    <td></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>

                </div>

            </div>
        </div>

    </section>

@endsection

@section('script')
    <script>
        customDomain();

        function customDomain() {
            document.getElementById('subdomain').style.display = 'none';
            document.getElementById('domain').style.display = 'block';
        }

        function customSubdomain() {
            document.getElementById('subdomain').style.display = 'block';
            document.getElementById('domain').style.display = 'none';
        }
    </script>
@endsection
