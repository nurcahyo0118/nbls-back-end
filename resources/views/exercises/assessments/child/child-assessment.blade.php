@php
    $exercise = App\Exercise::find($assessment->pivot->exercise_id);
    $answer = $exercise->submittedExercises()->where(['user_id' => $assessment->id])->first();
    $student_value = 0;
    $student_values = DB::table('exercise_assessments')->where('exercise_id', $exercise->id)->where('user_id', $assessment->id)->get();
    foreach($student_values as $sv)
    {
        $value_criteria = App\ExerciseCriteria::find($sv->criteria_id);

        $student_value += ($sv->value) * ($value_criteria->weight / 100);
    }

@endphp
<tr>
    <td>{{ $exercise->section->course->title }}</td>
    <td>{{ $exercise->title }}</td>
    <td>{{ $assessment->fullname }}</td>
    <td>{{ $student_value === 0 ? 'Belum Dinilai' : $student_value }}</td>
    <td class="pull-right">
        @if($answer->pivot->file != null)
            <a href="/exercise-file/{{ $exercise->file }}" class="btn btn-default btn-xs" download>
                <i class="fa fa-eye"></i>
                Lihat Jawaban
            </a>
        @else
            <a href="{{ $answer->pivot->link }}" target="_blank" class="btn btn-default btn-xs">
                <i class="fa fa-eye"></i>
                Lihat Jawaban
            </a>
        @endif
        <a class="btn btn-info btn-xs" data-toggle="modal" data-target="#modalAssessment{{ $assessment->id }}">
            <i class="fa fa-sort-numeric-asc"></i>
            Nilai
        </a>
    </td>
</tr>

<div class="modal fade" id="modalAssessment{{ $assessment->id }}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Nilai</h4>
            </div>

            <form id="formaddcriteria" 
                action="{{ route('exercises.assessments.store', [$_SERVER['HTTP_HOST'], $exercise->id, $assessment->id]) }}"
                method="POST">

                <div class="modal-body">
                    <fieldset>
                        {{ csrf_field() }}
                        
                        @foreach($exercise->criterias as $index => $criteria)
                            @php
                                $value = DB::table('exercise_assessments')->where('criteria_id', $criteria->id)->where('user_id', $assessment->id)->first();
                            @endphp
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label> {{ $criteria->content }} : </label>
                                    <input type="number" name="value[]" class="form-control" min="0" max="100" value="{{ $value != null ? $value->value : 0 }}" required>
                                </div>
                                <hr>
                            </div>
                        @endforeach
                        
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <i class="fa fa-close"></i>
                        Tutup
                    </button>
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-save"></i>
                        Simpan
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>