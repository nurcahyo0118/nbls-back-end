@extends('layouts.main')

@section('content')

    @php
        $tenant = Auth::user()->tenant;
        $permission = App\Permission::where(['role_id' => Auth::user()->role['id'], 'tenant_id' => $tenant->id])->first();
    @endphp

    <div class="row">

        <div class="col-xs-12">

            @include('layouts.partials.message')

            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Daftar Penilaian</h3>
                    
                </div>

                <div class="box-body">
                    <table class="table js-basic-example dataTable">
                        <thead>
                        <tr>
                            <th>Nama Kursus</th>
                            <th>Nama Soal</th>
                            <th>Nama Murid</th>
                            <th>Nilai</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody id="valueList">
                            @foreach($assessments as $assessment)
                                @include('exercises.assessments.child.child-assessment')
                            @endforeach
                        </tbody>
                    </table>

                </div>

            </div>

        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript">
    </script>
@endsection