@extends('layouts.main')

@section('content')

    @php
        $tenant = Auth::user()->tenant;
        $permission = App\Permission::where(['role_id' => Auth::user()->role['id'], 'tenant_id' => $tenant->id])->first();
    @endphp

    <div class="row">

        <div class="col-xs-12">

            @include('layouts.partials.message')

            <div class="modal fade" id="modalAddCriteria">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Tambah Kriteria</h4>
                        </div>
            
                        <form id="formaddcriteria" 
                            action="{{ route('exercises.criterias.store', [$_SERVER['HTTP_HOST'], $exercise->id]) }}" 
                            enctype="multipart/form-data" 
                            method="POST" 
                            data-parsley-validate>

                            <div class="modal-body">
                                <fieldset>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
            
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label> Konten : </label>
                                            <input type="text" name="content" class="form-control" required>
                                        </div>
                                    </div>
            
                                </fieldset>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                    <i class="fa fa-close"></i>
                                    Tutup
                                </button>
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-save"></i>
                                    Simpan
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            

            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Daftar Kriteria {{ $exercise->name }}</h3>
                    <br>

                    <button class="btn btn-info pull-left col-md-1" data-toggle="modal" data-target="#modalAddCriteria">
                        <span class="fa fa-plus"></span>
                    </button>

                </div>
                
                <form action="{{ route('exercises.criterias.weight.update', [$_SERVER['HTTP_HOST'], $exercise->id]) }}" method="POST">
                    
                    {{ csrf_field() }}

                    <input type="hidden" name="_method" value="put">
                    
                    <div class="box-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Nama Kursus</th>
                                <th>Nama Soal</th>
                                <th>Kriteria</th>
                                <th>Bobot</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="criteriaList">

                            @foreach($criterias as $criteria)

                                @include('exercises.criterias.child.child-criteria')

                            @endforeach

                            </tbody>
                        </table>

                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary pull-right">
                            <i class="fa fa-save"></i>
                            Simpan
                        </button>
                    </div>
                </form>

                @foreach($criterias as $criteria)

                <form id="formaddcriteria{{ $criteria->id }}" 
                    action="{{ route('exercises.criterias.update', [$_SERVER['HTTP_HOST'], $criteria->exercise_id, $criteria->id]) }}" 
                    enctype="multipart/form-data" 
                    method="POST" 
                    data-parsley-validate>

                </form>

                @endforeach

            </div>

        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        function onDeleteCriteria(exerciseId, criteriaId) {
            swal({
                title: "Apa kamu yakin?",
                text: "Data yang telah di hapus tidak dapat dikembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {

                    $.ajax({
                        url: '/exercises/' + exerciseId + '/criteria/' + criteriaId,
                        type: 'DELETE',
                        success: function (response) {
                            window.location.reload();
                        },
                        error: function (error) {
                            window.location.reload();
                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    });

                }
            });
        }
    </script>
@endsection