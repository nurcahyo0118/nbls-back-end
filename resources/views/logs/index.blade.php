@extends('layouts.main')

@section('content')

    @php
        $tenant = Auth::user()->tenant;
        $permission = App\Permission::where(['role_id' => Auth::user()->role['id'], 'tenant_id' => $tenant->id])->first();
    @endphp

    <div class="row">

        <div class="col-xs-12">

            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Log</h3>

                </div>

                <div class="box-body">
                    <table class="table table-export">
                        <thead>
                        <tr>
                            <th>Nama</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody id="encounterList">

                        @foreach($users as $user)
                            <tr>
                                <td>{{ $user->fullname }}</td>
                                <td class="text-right">
                                    <a
                                            href="{{ route('logs.show', [$_SERVER['HTTP_HOST'], $user->id]) }}"
                                            class="btn btn-info btn-xs">
                                        <i class="fa fa-eye"></i>
                                        Lihat Log
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div>

            </div>

        </div>
    </div>

@endsection