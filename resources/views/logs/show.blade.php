@extends('layouts.main')

@section('content')

    @php
        $tenant = Auth::user()->tenant;
        $permission = App\Permission::where(['role_id' => Auth::user()->role['id'], 'tenant_id' => $tenant->id])->first();
    @endphp

    <div class="row">

        <div class="col-xs-12">

            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Log</h3>

                </div>

                <div class="box-body">
                    <table class="table js-basic-example dataTable">
                        <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Aktifitas</th>
                            <th>Waktu</th>
                        </tr>
                        </thead>
                        <tbody id="encounterList">

                        @foreach($logs as $log)
                            <tr>
                                <td>{{ $log->user->fullname }}</td>
                                <td>{{ $log->reason }}</td>
                                <td>{{ $log->created_at }}</td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div>

            </div>

        </div>
    </div>

@endsection