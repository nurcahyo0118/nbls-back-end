<!DOCTYPE html>
<html lang="en">
<head>
    <title>NICEPAY - Secure Checkout</title>
    <link rel='stylesheet' href='index.css' type='text/css'/>
    <meta charset="UTF-8">
</head>
<body>
<div class="wrapper">
    <div class="btn-group" id="form-selector">
        <button type="button" class="btn btn-selector active" id="va-btn">Bank Transfer</button>
        <button type="button" class="btn btn-selector" id="cc-btn">Credit Card</button>
    </div>
    <div class="form" id="va-form">
        <form action="/payment-method/checkout.php" method="post">
            <h2 class="form-title">Bank Transfer</h2>
            <h4>Total</h4>
            <hr>
            <h3>Rp. 12.000,00</h3>
            <hr>
            <select name="bankCd">
                <option value="CENA">BCA</option>
                <option value="BNIN">BNI</option>
                <option value="BMRI">Mandiri</option>
                <option value="BBBA">Permata</option>
                <option value="IBBK">BII Maybank</option>
                <option value="BNIA">CIMB Niaga</option>
                <option value="HNBN">KEB Hana Bank</option>
                <option value="BRIN">BRI</option>
                <option value="BDIN">Danamon</option>
            </select>
            <input type="hidden" name="payMethod" value="02">
            <button type="submit" class="btn-submit" id="va">Checkout</button>
        </form>
    </div>

    <div class="form" id="cc-form">
        <form action="/payment-method/checkout.php" method="post">
            <h2 class="form-title">Credit Card</h2>
            <h4>Total</h4>
            <hr>
            <h3>Rp. 1.000,00</h3>
            <hr>
            <input type="hidden" name="payMethod" value="01">
            <button type="submit" class="btn-submit" id="cc">Checkout</button>
        </form>
    </div>
</div>

<script type="text/javascript" src="http://code.jquery.com/jquery-1.5.1.min.js?ver=3.5"></script>
<script type="text/javascript" src="index.js"></script>
<body>
</html>