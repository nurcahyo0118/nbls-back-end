@extends('layouts.main')

@section('content')

    <div class="col-md-12">
        <div class="container">
            <h3> Dashboard</h3>
            <div class="col-md-3">
                <div class="panel panel-primary text-center">
                    <div class="panel-heading">Jumlah User</div>
                    <div class="panel-body">User</div>
                    <span class="fa fa-users fa-4x"></span>
                    <br>
                    <h2>{{ Auth::user()->tenant->users->count() }}</h2>
                    <div class="panel-body">Panel User</div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="panel panel-primary text-center">
                    <div class="panel-heading">Jumlah Pembayaran</div>
                    <div class="panel-body">Pembayaran</div>
                    <span class="fa fa-money fa-4x"></span>
                    <br>
                    <h2>0</h2>
                    <div class="panel-body">Panel User</div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="panel panel-primary text-center">
                    <div class="panel-heading">Jumlah Course</div>
                    <div class="panel-body">Course</div>
                    <span class="fa fa-calendar fa-4x"></span>
                    <br>
                    <h2>{{Auth::user()->tenant->courses->count()}}</h2>
                    <div class="panel-body">Panel User</div>
                </div>
            </div>
            <div class="col-md-4">

            </div>
        </div>
    </div>

@endsection
