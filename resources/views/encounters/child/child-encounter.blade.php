<tr id="encounterId{{ $encounter->id }}">
    <td>{{ $encounter->course->title }}</td>
    <td>{{ $encounter->name }}</td>
    {{--  <td>
        <a href="#!" class="btn btn-info btn-xs">
            <i class="fa fa-plus"></i>
            Tambah
        </a>
    </td>  --}}
    <td class="text-right">
        <a href="{{ route('encounters.assessments.index', [$_SERVER['HTTP_HOST'], $encounter->id]) }}"
            class="btn btn-default btn-xs">
            <i class="fa fa-sort-numeric-asc"></i>
            Penilaian
        </a>
        <a href="{{ route('encounters.criterias.index', [$_SERVER['HTTP_HOST'], $encounter->id]) }}"
            class="btn btn-default btn-xs">
            <i class="fa fa-list"></i>
            Kriteria
        </a>
        <a href="{{ route('encounters.edit', [$_SERVER['HTTP_HOST'], $encounter->id]) }}"
           class="btn btn-default btn-xs">
            <i class="fa fa-edit"></i>
            Edit
        </a>
        <a onclick="onDeleteEncounter({{ $encounter->id }})" class="btn btn-danger btn-xs">
            <i class="fa fa-trash"></i>
            Hapus
        </a>
    </td>
</tr>

{{--  @include('encounters.modal.edit-encounter')  --}}