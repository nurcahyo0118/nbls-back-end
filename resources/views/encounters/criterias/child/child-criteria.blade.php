<tr id="criteriaId{{ $criteria->id }}">
    <td>{{ str_limit($criteria->encounter->course->title, 15) }}</td>
    <td>{{ str_limit($criteria->encounter->name, 15) }}</td>
    <td>{{ str_limit($criteria->content, 30) }}</td>
    <td>
        <input type="number" name="weight[]" max="100" value="{{ $criteria->weight }}" class="form-control">
    </td>
    <td class="text-right">
        <a data-toggle="modal" data-target="#modalEditCriteria{{ $criteria->id }}"
           class="btn btn-default btn-xs">
            <i class="fa fa-edit"></i>
            Edit
        </a>
        <a onclick="onDeleteCriteria({{ $criteria->encounter_id }}, {{ $criteria->id }})" class="btn btn-danger btn-xs">
            <i class="fa fa-trash"></i>
            Hapus
        </a>
    </td>
</tr>

<div class="modal fade" id="modalEditCriteria{{ $criteria->id }}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Edit Kriteria</h4>
            </div>

                <div class="modal-body">
                    <fieldset>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" form="formaddcriteria{{ $criteria->id }}">

                        <input type="hidden" name="_method" value="put" form="formaddcriteria{{ $criteria->id }}">

                        <div class="col-md-12">
                            <div class="form-group">
                                <label> Konten : </label>
                                <input type="text" name="content" class="form-control" value="{{ $criteria->content }}" form="formaddcriteria{{ $criteria->id }}" required>
                            </div>
                        </div>

                    </fieldset>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <i class="fa fa-close"></i>
                        Tutup
                    </button>
                    <button type="submit" form="formaddcriteria{{ $criteria->id }}" value="formaddcriteria{{ $criteria->id }}" class="btn btn-primary">
                        <i class="fa fa-save"></i>
                        Simpan
                    </button>
                </div>
            
        </div>
    </div>
</div>