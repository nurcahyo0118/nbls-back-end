@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-xs-12">

            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Daftar Kriteria</h3>

                    <br>

                    <a class="btn btn-info pull-left col-md-1"
                       href="{{ route('encounters.create', $_SERVER['HTTP_HOST']) }}">
                        <span class="fa fa-plus"></span>
                    </a>
                </div>

                <div class="box-body">

                    <div class="col-md-12">
                        <form action="{{ route('encounters.update', [$_SERVER['HTTP_HOST'], $encounter->id]) }}"
                              method="post" class="form-horizontal" enctype="multipart/form-data" data-parsley-validate>

                            {{ csrf_field() }}

                            <input type="hidden" name="_method" value="put">

                            <fieldset>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label> Kursus : </label>
                                        <select id="encounterCourse" type="text" name="course_id" class="form-control"
                                                required>
                                            @foreach($courses as $course)
                                                <option value="{{ $course->id }}" {{ $encounter->course_id == $course->id ? 'selected' : ''  }}>{{ $course->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label> Nama Soal : </label>
                                        <input id="encounterUrl" type="text" name="name" class="form-control"
                                               value="{{ $encounter->name }}" required>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label> Status : </label>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label><input id="encounterIsActive" type="radio" name="is_active" value="1"
                                                      {{ $encounter->is_active != 0 ? 'checked' : ''  }} required> Aktif
                                        </label>
                                        <label><input id="encounterIsInActive" type="radio" name="is_active" value="0"
                                                      {{ $encounter->is_active === 0 ? 'checked' : ''  }} required> Non Aktif
                                        </label>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label> File Soal : </label>
                                        <input id="encounterFile" type="file" name="file">
                                    </div>
                                </div>

                            </fieldset>

                            <div class="modal-footer">
                                <a href="{{ route('encounters.index', $_SERVER['HTTP_HOST']) }}"
                                   class="btn btn-default">
                                    <i class="fa fa-close"></i>
                                    Tutup
                                </a>
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-save"></i>
                                    Simpan
                                </button>
                            </div>
                        </form>
                    </div>

                </div>

            </div>

        </div>
    </div>
@endsection