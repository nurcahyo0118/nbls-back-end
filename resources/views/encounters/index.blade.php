@extends('layouts.main')

@section('content')

    @php
        $tenant = Auth::user()->tenant;
        $permission = App\Permission::where(['role_id' => Auth::user()->role['id'], 'tenant_id' => $tenant->id])->first();
    @endphp

    <div class="row">

        <div class="col-xs-12">

            @include('layouts.partials.message')

            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Daftar Pertemuan</h3>
                    <br>

                    <a class="btn btn-info pull-left col-md-1"
                       href="{{ route('encounters.create', $_SERVER['HTTP_HOST']) }}">
                        <span class="fa fa-plus"></span>
                    </a>

                </div>

                <div class="box-body">
                    <table class="table js-basic-example dataTable">
                        <thead>
                        <tr>
                            <th>Nama Kursus</th>
                            <th>Nama Soal</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody id="encounterList">

                        @foreach($encounters as $encounter)

                            @include('encounters.child.child-encounter')

                        @endforeach

                        </tbody>
                    </table>

                </div>

            </div>

        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        function onSubmitEncounter() {

            var name = $('#encounterName').val();
            var url = $('#encounterUrl').val();
            var isPrivate = $('#encounterIsPrivate').is(":checked");
            var isPublished = $('#encounterIsPublished').is(":checked");

            var formData = new FormData();
            formData.append('_token', '{{ csrf_token() }}');
            formData.append('name', name);
            formData.append('url', url);
            formData.append('is_private', isPrivate);
            formData.append('is_published', isPublished);

            console.log(formData.get('_token'));
            console.log(formData.get('name'));
            console.log(formData.get('url'));
            console.log(formData.get('is_private'));
            console.log(formData.get('is_published'));

            $.ajax({
                url: '/encounters',
                type: 'POST',
                data: formData,
                success: function (response) {
                    $('#encounterList').append(response);
                    $('#modalAdd').modal('toggle');
                    $('form#formAddEncounter')[0].reset();
                    swal("Berhasil !", "Halaman berhasil ditambahkan", "success");
                },
                error: function (error) {
                    swal("Berhasil !", "Halaman gagal ditambahkan", "error");
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onSubmitUpdateEncounter(encounterId) {

            var name = $('#encounterName' + encounterId).val();
            var url = $('#encounterUrl' + encounterId).val();
            var isPrivate = $('#encounterIsPrivate' + encounterId).is(":checked");
            var isPublished = $('#encounterIsPublished' + encounterId).is(":checked");

            var formData = new FormData();
            formData.append('_token', '{{ csrf_token() }}');
            formData.append('name', name);
            formData.append('url', url);
            formData.append('is_private', isPrivate);
            formData.append('is_published', isPublished);

            console.log(formData.get('_token'));
            console.log(formData.get('name'));
            console.log(formData.get('url'));
            console.log(formData.get('is_private'));
            console.log(formData.get('is_published'));

            {{--  $.ajax({
                url: '/encounters/' + encounterId,
                type: 'PUT',
                data: formData,
                success: function(response) {
                    $('#encounterList').append(response);
                    $('#modalAdd').modal('toggle');
                    $('form#formAddEncounter')[0].reset();
                    swal("Berhasil !", "Halaman berhasil ditambahkan", "success");
                },
                error: function(error) {
                    swal("Berhasil !", "Halaman gagal ditambahkan", "error");
                },
                cache: false,
                contentType: false,
                processData: false
            });  --}}

            $.ajax({
                url: '/encounters/' + encounterId,
                type: 'PUT',
                data: formData,
                success: function (response) {
                    $('#encounterList').replaceWith(response);
                    $('#modalEditEncounter' + encounterId).modal('toggle');
                    $('form#formEditEncounter')[0].reset();
                    swal("Berhasil !", "Halaman berhasil diedit", "success");
                },
                error: function (error) {
                    swal("Berhasil !", "Halaman gagal diedit", "error");
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onDeleteEncounter(encounterId) {
            swal({
                title: "Apa kamu yakin?",
                text: "Data yang telah di hapus tidak dapat dikembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {

                    $.ajax({
                        url: '/encounters/' + encounterId,
                        type: 'DELETE',
                        success: function (response) {
                            $('#encounterId' + encounterId).remove();
                            swal("Berhasil !", "Halaman berhasil dihapus", "success");
                        },
                        error: function (error) {
                            swal("Berhasil !", "Halaman gagal dihapus", "error");
                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    });

                }
            });
        }
    </script>
@endsection