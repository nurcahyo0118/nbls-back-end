@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-xs-12">

            <div class="box box-primary">

                <div class="box-header">
                    <h4 class="modal-title" id="defaultModalLabel">Tambah Halaman</h4>
                </div>

                <form id="formAddPage" action="{{ route('pages.store', $_SERVER['HTTP_HOST']) }}" method="post"
                      class="form-horizontal" data-parsley-validate>

                      {{ csrf_field() }}

                    <div class="box-body">
                        <fieldset>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label> Title : </label>
                                    <input id="pageName" type="text" name="name" class="form-control" required>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label> Subtitle : </label>
                                    <input class="form-control" name="subtitle" placeholder="Subtitle" maxlength="150" required>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label> Konten : </label>
                                    <textarea class="textarea" name="content"
                                                placeholder="Konten"></textarea>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label> URL : </label>
                                    <div class="input-group">
                                        <span class="input-group-addon">{{ Auth::user()->tenant->is_cname ? '' : Auth::user()->tenant->subdomain . '.' }}{{ Auth::user()->tenant->is_cname ? Auth::user()->tenant->domain : env('HOST_NAME') }}/cpages/</span>
                                        <input id="pageUrl" type="text" name="url" class="form-control"  placeholder="Ex : contacts" required>
                                    </div>

                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <input type="radio" name="is_private" value="1" checked>
                                    Private
                                    <input type="radio" name="is_private" value="0">
                                    Published
                                </div>
                            </div>

                            {{--  <div class="col-md-2">
                                <div class="form-group">
                                    <label><input id="pageIsPrivate" type="checkbox" name="is_private"> Private
                                    </label>
                                    <label><input id="pageIsPublished" type="checkbox" name="is_published">
                                        Published </label>
                                </div>
                            </div>  --}}

                        </fieldset>
                    </div>

                    <div class="modal-footer">
                        <a href="{{ route('pages.index', $_SERVER['HTTP_HOST']) }}" class="btn btn-default">
                            <i class="fa fa-close"></i>
                            Tutup
                        </a>
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-save"></i>
                            Simpan
                        </button>
                    </div>
                </form>

            </div>

        </div>
    </div>
@endsection


@section('script')
<script>
    $(function () {
        $('.textarea').wysihtml5();
    })
</script>
@endsection
