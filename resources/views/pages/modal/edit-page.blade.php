<div class="modal fade" id="modalEditPage{{ $page->id }}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Edit Halaman</h4>
            </div>

            <form id="formEditPage" onsubmit="onSubmitUpdatePage({{ $page->id }}); return false;" method="post"
                  class="form-horizontal" data-parsley-validate>

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="_method" value="put">

                <div class="modal-body">
                    <fieldset>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label> Nama : </label>
                                <input id="pageName{{ $page->id }}" type="text" name="name" class="form-control"
                                       value="{{ $page->name }}" required>
                            </div>
                        </div>
                        {{--  onsubmit="onSubmitUpdatePage({{ $page->id }}); return false;"  --}}

                        <div class="col-md-12">
                            <div class="form-group">
                                <label> URL : </label>
                                <input id="pageUrl{{ $page->id }}" type="url" name="url" class="form-control"
                                       value="{{ $page->url }}" required>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <label><input id="pageIsPrivate{{ $page->id }}" type="checkbox"
                                              name="is_private" {{ $page->is_private ? 'checked' : '' }}> Private
                                </label>
                                <label><input id="pageIsPublished" type="checkbox"
                                              name="is_published" {{ $page->is_published ? 'checked' : '' }}> Published
                                </label>
                            </div>
                        </div>

                    </fieldset>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <i class="fa fa-close"></i>
                        Tutup
                    </button>
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-save"></i>
                        Simpan
                    </button>
                </div>

            </form>
        </div>
    </div>
</div>