@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-xs-12">

            <div class="box box-primary">

                <div class="box-header">
                    <h4 class="modal-title" id="defaultModalLabel">Edit Halaman</h4>
                </div>

                <form id="formAddPage" action="{{ route('pages.update', [$_SERVER['HTTP_HOST'], $page->id]) }}" method="post"
                    class="form-horizontal" data-parsley-validate>

                    {{ csrf_field() }}

                    <input type="hidden" name="_method" value="PUT">

                    <div class="box-body">
                        <fieldset>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label> Title : </label>
                                    <input id="pageName" type="text" name="name" class="form-control" value="{{ $page->name }}" required>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label> Subtitle : </label>
                                    <textarea class="textarea" name="subtitle"
                                                placeholder="Subtitle" required>{{ $page->subtitle }}</textarea>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label> Konten : </label>
                                    <textarea class="textarea" name="content"
                                                placeholder="Konten" required>{{ $page->content }}</textarea>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label> URL : </label>
                                    <div class="input-group">
                                        <span class="input-group-addon">{{ env('HOST') . '/cpages/' }}</span>
                                        <input id="pageUrl" type="text" name="url" class="form-control" value="{{ $page->url }}" placeholder="Ex : contacts" required>
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <input type="radio" name="is_private" value="1" {{ $page->is_private ? 'checked' : '' }}>
                                    Private
                                    <input type="radio" name="is_private" value="0" {{ $page->is_published ? 'checked' : '' }}>
                                    Published
                                </div>
                            </div>

                        </fieldset>
                    </div>

                    <div class="modal-footer">
                        <a href="{{ route('pages.index', $_SERVER['HTTP_HOST']) }}" class="btn btn-default">
                            <i class="fa fa-close"></i>
                            Tutup
                        </a>
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-save"></i>
                            Simpan
                        </button>
                    </div>
                </form>

            </div>

        </div>
    </div>
@endsection