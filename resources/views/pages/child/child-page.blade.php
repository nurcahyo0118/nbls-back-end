<tr id="pageId{{ $page->id }}">
    <td>{{ $page->name }}</td>
    <td>
        @if($tenant->is_cname && $tenant->domain === env('HOST_NAME'))
            <a href="{{ env('APP_URL') . '/cpages/' }}{{ $page->url }}" target="_blank">
                {{ env('APP_URL') . '/cpages/' }}{{ $page->url }}
            </a>
        @elseif($tenant->is_cname && $tenant->domain !== env('HOST_NAME'))
            <a href="{{ env('APP_URL') . '/cpages/' }}{{ $page->url }}" target="_blank">
                {{ env('APP_URL') . '/cpages/' }}{{ $page->url }}
            </a>
        @else
            <a href="{{ 'http://' . $tenant->subdomain . '.' . env('HOST_NAME') . '/cpages/' }}{{ $page->url }}" target="_blank">
                {{ 'http://' . $tenant->subdomain . '.' . env('HOST_NAME') . '/cpages/' }}{{ $page->url }}
            </a>
        @endif
    </td>
    <td>{{ $page->is_private ? 'Yes' : 'No' }}</td>
    <td>{{ $page->is_published ? 'Yes' : 'No' }}</td>
    <td class="text-right">
        <a href="{{ route('pages.edit', [$_SERVER['HTTP_HOST'], $page->id]) }}" class="btn btn-default btn-xs">
            <i class="fa fa-edit"></i>
            Edit
        </a>
        <a onclick="onDeletePage({{ $page->id }})" class="btn btn-danger btn-xs">
            <i class="fa fa-trash"></i>
            Hapus
        </a>
    </td>
</tr>

@include('pages.modal.edit-page')