@extends('layouts.main')

@section('content')

    @php
        $tenant = Auth::user()->tenant;
        $permission = App\Permission::where(['role_id' => Auth::user()->role['id'], 'tenant_id' => $tenant->id])->first();
    @endphp

    <div class="row">

        <div class="col-xs-12">

            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Daftar Halaman</h3>
                    <br>
                    <a class="btn btn-info pull-left col-md-1" href="{{ route('pages.create', $_SERVER['HTTP_HOST']) }}">
                        <span class="fa fa-plus"></span>
                    </a>

                </div>

                <div class="box-body">
                    <table class="table js-basic-example dataTable">
                        <thead>
                        <tr>
                            <th>Nama</th>
                            <th>URL</th>
                            <th>Private</th>
                            <th>Published</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody id="pageList">

                        @foreach($pages as $page)
                            @include('pages.child.child-page')
                        @endforeach

                        </tbody>
                    </table>

                </div>

            </div>

        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        function onSubmitPage() {

            var name = $('#pageName').val();
            var url = $('#pageUrl').val();
            var isPrivate = $('#pageIsPrivate').is(":checked");
            var isPublished = $('#pageIsPublished').is(":checked");

            var formData = new FormData();
            formData.append('_token', '{{ csrf_token() }}');
            formData.append('name', name);
            formData.append('url', url);
            formData.append('is_private', isPrivate);
            formData.append('is_published', isPublished);

            console.log(formData.get('_token'));
            console.log(formData.get('name'));
            console.log(formData.get('url'));
            console.log(formData.get('is_private'));
            console.log(formData.get('is_published'));

            $.ajax({
                url: '/pages',
                type: 'POST',
                data: formData,
                success: function (response) {
                    $('#pageList').append(response);
                    $('#modalAdd').modal('toggle');
                    $('form#formAddPage')[0].reset();
                    swal("Berhasil !", "Halaman berhasil ditambahkan", "success");
                },
                error: function (error) {
                    swal("Berhasil !", "Halaman gagal ditambahkan", "error");
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onSubmitUpdatePage(pageId) {

            var name = $('#pageName' + pageId).val();
            var url = $('#pageUrl' + pageId).val();
            var isPrivate = $('#pageIsPrivate' + pageId).is(":checked");
            var isPublished = $('#pageIsPublished' + pageId).is(":checked");

            var formData = new FormData();
            formData.append('_token', '{{ csrf_token() }}');
            formData.append('name', name);
            formData.append('url', url);
            formData.append('is_private', isPrivate);
            formData.append('is_published', isPublished);

            console.log(formData.get('_token'));
            console.log(formData.get('name'));
            console.log(formData.get('url'));
            console.log(formData.get('is_private'));
            console.log(formData.get('is_published'));

            {{--  $.ajax({
                url: '/pages/' + pageId,
                type: 'PUT',
                data: formData,
                success: function(response) {
                    $('#pageList').append(response);
                    $('#modalAdd').modal('toggle');
                    $('form#formAddPage')[0].reset();
                    swal("Berhasil !", "Halaman berhasil ditambahkan", "success");
                },
                error: function(error) {
                    swal("Berhasil !", "Halaman gagal ditambahkan", "error");
                },
                cache: false,
                contentType: false,
                processData: false
            });  --}}

            $.ajax({
                url: '/pages/' + pageId,
                type: 'PUT',
                data: formData,
                success: function (response) {
                    $('#pageList').replaceWith(response);
                    $('#modalEditPage' + pageId).modal('toggle');
                    $('form#formEditPage')[0].reset();
                    swal("Berhasil !", "Halaman berhasil diedit", "success");
                },
                error: function (error) {
                    swal("Berhasil !", "Halaman gagal diedit", "error");
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }

        function onDeletePage(pageId) {
            swal({
                title: "Apa kamu yakin?",
                text: "Data yang telah di hapus tidak dapat dikembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {

                    $.ajax({
                        url: '/pages/' + pageId,
                        type: 'DELETE',
                        success: function (response) {
                            $('#pageId' + pageId).remove();
                            swal("Berhasil !", "Halaman berhasil dihapus", "success");
                        },
                        error: function (error) {
                            swal("Berhasil !", "Halaman gagal dihapus", "error");
                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    });

                }
            });
        }
    </script>
@endsection