@extends('layouts.main')

@section('content')

    @php
        $tenant = Auth::user()->tenant;
        $permission = App\Permission::where(['role_id' => Auth::user()->role['id'], 'tenant_id' => $tenant->id])->first();
    @endphp

    <div class="row">

        <div class="col-xs-12">

            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">I-Time</h3>

                </div>

                <div class="box-body">
                    
                    <label>Penjelasan Instruktur </label>
                    <p>{{ $i_time->instructor_explanation }}</p>

                    <hr>

                    <label>Respon Instruktur </label>
                    <p>{{ $i_time->instructor_response }}</p>
                    
                    <hr>

                    <label>Instruktur Terbaik </label>
                    <p>{{ App\User::find($i_time->best_instructor_id)->fullname }}</p>

                    <hr>

                    <label>Instruktur Terburuk </label>
                    <p>{{ App\User::find($i_time->worst_instructor_id)->fullname }}</p>

                    <hr>

                    <label>Saran Untuk Instruktur </label>
                    <p>{{ $i_time->instructor_suggestion }}</p>

                    <hr>

                    <label>Penjelasan Kursus </label>
                    <p>{{ $i_time->course_explanation }}</p>

                    <hr>

                    <label>Kursus Terbaik </label>
                    <p>{{ App\Course::find($i_time->best_course_id)->title }}</p>

                    <hr>

                    <label>Kursus Terburuk </label>
                    <p>{{ App\Course::find($i_time->worst_course_id)->title }}</p>

                    <hr>

                    <label>Saran Untuk Kursus </label>
                    <p>{{ $i_time->course_suggestion }}</p>

                    <hr>

                    <label>Bantuan Komputer </label>
                    <p>{{ $i_time->computer_help }}</p>

                    <hr>

                    <label>Kenyamanan AC </label>
                    <p>{{ $i_time->ac_comfortable }}</p>

                    <hr>

                    <label>Kebersihan Ruangan </label>
                    <p>{{ $i_time->class_cleanliness }}</p>

                    <hr>

                    <label>Fasilitas </label>
                    <p>{{ $i_time->facility_help }}</p>

                    <hr>

                    <label>Saran Untuk Fasilitas </label>
                    <p>{{ $i_time->facility_suggestion }}</p>

                    <hr>

                    <label>Permasalahan Login </label>
                    <p>{{ $i_time->login_problem }}</p>

                    <hr>

                    <label>Saran Untuk Sistem Belajar </label>
                    <p>{{ $i_time->learning_system_suggestion }}</p>

                </div>

            </div>

        </div>
    </div>

@endsection