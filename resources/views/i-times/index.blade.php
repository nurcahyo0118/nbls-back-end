@extends('layouts.main')

@section('content')

    @php
        $tenant = Auth::user()->tenant;
        $permission = App\Permission::where(['role_id' => Auth::user()->role['id'], 'tenant_id' => $tenant->id])->first();
    @endphp

    <div class="row">

        <div class="col-xs-12">

            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">I-Times</h3>

                </div>

                <div class="box-body">
                    <table class="table table-export">
                        <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Waktu</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody id="iTimeList">

                        @foreach($i_times as $i_time)
                            <tr>
                                <td>{{ $i_time->user->fullname }}</td>
                                <td>{{ $i_time->created_at }}</td>
                                <td class="text-right">
                                    <a
                                            href="{{ route('i-times.show', [$_SERVER['HTTP_HOST'], $i_time->id]) }}"
                                            class="btn btn-default btn-xs">
                                        <i class="fa fa-eye"></i>
                                        Detail
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div>

            </div>

        </div>
    </div>

@endsection