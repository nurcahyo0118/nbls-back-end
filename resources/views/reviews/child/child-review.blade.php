<tr id="reviewId{{ $course->id }}">
    {{--  <td>
        <input type="checkbox" name="is_enable_review" {{ $course->is_enable_review ? 'checked' : '' }}>
    </td>  --}}
    <td>{{ $course->title }}</td>
    <td>{{ $course->author->fullname }}</td>
    <td>{{ $course->reviews()->count() }}</td>
    @php
        $rates = [];
        foreach($course->reviews as $review) {
            array_push($rates, $review->rate);
        }
    @endphp
    <td>{{ collect($rates)->avg() }}</td>
    <td class="text-right">
        <a href="{{ route('social-learning-course.show', [$_SERVER['HTTP_HOST'], $course->id]) }}" target="_blank" class="btn btn-default btn-xs">
            Lihat Ulasan
        </a>
    </td>

</tr>