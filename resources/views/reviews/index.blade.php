@extends('layouts.main')

@section('content')

    @php
        $tenant = Auth::user()->tenant;
        $permission = App\Permission::where(['role_id' => Auth::user()->role['id'], 'tenant_id' => $tenant->id])->first();
    @endphp

    <div class="row">

        <div class="col-xs-12">

            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Semua Ulasan</h3>
                    {{--  <br>
                    <br>
                    <a class="btn btn-info pull-left" href="#!" style="margin-right: 5px;">
                        Aktifkan ulasan untuk semua
                    </a>
                    <a class="btn btn-info pull-left" href="#!">
                        Nonaktifkan ulasan untuk semua
                    </a>  --}}
                </div>

                <div class="box-body">
                    <table class="table js-basic-example dataTable">
                        <thead>
                        <tr>
                            {{--  <th>Ulasan Diaktifkan</th>  --}}
                            <th>Kursus</th>
                            <th>Instruktur</th>
                            <th>Ulasan</th>
                            <th>Rating</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody id="reviewList">
                        @foreach($courses as $course)
                            @include('reviews.child.child-review')
                        @endforeach
                        </tbody>
                    </table>

                </div>

            </div>

        </div>
    </div>

@endsection