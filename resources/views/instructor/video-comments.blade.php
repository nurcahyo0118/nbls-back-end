@extends('layouts.main')

@section('content')

    <div class="row">

        <div class="col-xs-12">

            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Daftar Komen Video</h3>
                    <br>
                </div>

                <div class="box-body">
                    <table class="table js-basic-example dataTable">
                        <thead>
                        <tr>
                            <th>Nama Murid</th>
                            <th>Nama Material (Kursus)</th>
                            <th>Komentar</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody id="commentList">

                        @foreach($comments as $comment)
                            <tr>
                                <td>{{ $comment->user->fullname }}</td>
                                <td>{{ $comment->material->title }}</td>
                                <td>{{ $comment->content }}</td>
                                <td class="pull-right">
                                    <a href="{{ route('social-learning-my-course.material', [$_SERVER['HTTP_HOST'], $comment->material->section->course_id, $comment->material_id]) }}" target="_blank" class="btn btn-default btn-xs">
                                        <i class="fa fa-eye"></i>
                                        Lihat Komentar
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div>

            </div>

        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript">

        function onDeleteUser(commentId) {
            swal({
                title: "Apa kamu yakin?",
                text: "Data yang telah di hapus tidak dapat dikembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {

                    $.ajax({
                        type: "DELETE",
                        url: '/comments/' + commentId,
                        success: function (response) {
                            console.log(response);
                            $("#commentId" + commentId).remove();
                            swal("Berhasil !", "User berhasil dihapus", "success");
                        }
                    });

                }
            });
        }
    </script>
@endsection