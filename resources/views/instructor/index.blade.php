@extends('layouts.main')

@section('content')

    <div class="row">

        <div class="col-xs-12">

            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Daftar Murid</h3>
                    <br>
                </div>

                <div class="box-body">
                    <table class="table table-export">
                        <thead>
                        <tr>
                            <th>Nama</th>
                            <th>E-Mail</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody id="studentList">

                        @foreach($students as $student)
                            <tr>
                                <td>{{ $student->fullname }}</td>
                                <td>{{ $student->email }}</td>
                                <td class="text-right">
                                    
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div>

            </div>

        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript">

        function onDeleteUser(studentId) {
            swal({
                title: "Apa kamu yakin?",
                text: "Data yang telah di hapus tidak dapat dikembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {

                    $.ajax({
                        type: "DELETE",
                        url: '/students/' + studentId,
                        success: function (response) {
                            console.log(response);
                            $("#studentId" + studentId).remove();
                            swal("Berhasil !", "User berhasil dihapus", "success");
                        }
                    });

                }
            });
        }
    </script>
@endsection