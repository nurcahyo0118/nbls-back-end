@extends('layouts.main')

@section('content')

    <div class="row">

        <div class="col-xs-12">

            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Daftar Kursus</h3>
                    <br>
                </div>

                <div class="box-body">
                    <table class="table js-basic-example dataTable">
                        <thead>
                        <tr>
                            <th>Kursus</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody id="courseList">

                        @foreach($courses as $course)
                            <tr>
                                <td>{{ $course->title }}</td>
                                <td class="text-right">
                                    <a 
                                        href="{{ route('instructor.show-exercise', [$_SERVER['HTTP_HOST'], $course->id]) }}"
                                        class="btn btn-info btn-xs">
                                        <i class="fa fa-eye"></i>
                                        Lihat Daftar Latihan
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div>

            </div>

        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript">

        function onDeleteUser(courseId) {
            swal({
                title: "Apa kamu yakin?",
                text: "Data yang telah di hapus tidak dapat dikembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {

                    $.ajax({
                        type: "DELETE",
                        url: '/courses/' + courseId,
                        success: function (response) {
                            console.log(response);
                            $("#courseId" + courseId).remove();
                            swal("Berhasil !", "User berhasil dihapus", "success");
                        }
                    });

                }
            });
        }
    </script>
@endsection