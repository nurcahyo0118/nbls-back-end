@extends('layouts.main')

@section('content')

    <div class="row">

        <div class="col-xs-12">

            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Daftar Jawaban</h3>
                    <br>
                </div>

                <div class="box-body">
                    <table class="table js-basic-example dataTable">
                        <thead>
                        <tr>
                            <th>Nama Murid</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody id="encounterList">

                        @foreach($encounter->submittedEncounters as $answer)
                            <tr>
                                <td>{{ $answer->fullname }}</td>
                                <td class="text-right">
                                    <a href="#!" download="/submitted-encounter-file/{{ $answer->pivot->file }}" class="btn btn-info btn-xs">
                                        <i class="fa fa-download"></i>
                                        Unduh Jawaban
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div>

            </div>

        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript">

        function onDeleteUser(encounterId) {
            swal({
                title: "Apa kamu yakin?",
                text: "Data yang telah di hapus tidak dapat dikembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {

                    $.ajax({
                        type: "DELETE",
                        url: '/encounters/' + encounterId,
                        success: function (response) {
                            console.log(response);
                            $("#encounterId" + encounterId).remove();
                            swal("Berhasil !", "User berhasil dihapus", "success");
                        }
                    });

                }
            });
        }
    </script>
@endsection