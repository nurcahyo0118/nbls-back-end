@extends('layouts.main')

@section('content')

    <div class="row">

        <div class="col-xs-12">

            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Daftar Modul</h3>
                    <br>
                </div>

                <div class="box-body">
                    <table class="table js-basic-example dataTable">
                        <thead>
                        <tr>
                            <th>Modul</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody id="exerciseList">

                        @foreach($exercises as $exercise)
                            <tr>
                                <td>{{ $exercise->title }}</td>
                                <td class="text-right">
                                    <a 
                                        href="{{ route('instructor.submitted-exercise', [$_SERVER['HTTP_HOST'], $exercise->id]) }}"
                                        class="btn btn-info btn-xs">
                                        <i class="fa fa-eye"></i>
                                        Lihat Jawaban Murid
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div>

            </div>

        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript">

        function onDeleteUser(exerciseId) {
            swal({
                title: "Apa kamu yakin?",
                text: "Data yang telah di hapus tidak dapat dikembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {

                    $.ajax({
                        type: "DELETE",
                        url: '/exercises/' + exerciseId,
                        success: function (response) {
                            console.log(response);
                            $("#exerciseId" + exerciseId).remove();
                            swal("Berhasil !", "User berhasil dihapus", "success");
                        }
                    });

                }
            });
        }
    </script>
@endsection