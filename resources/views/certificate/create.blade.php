<form action="{{ route('certificate.store', [$_SERVER['HTTP_HOST']]) }}" method="post" class="form-horizontal"
      enctype="multipart/form-data" data-parsley-validate>

    {{ csrf_field() }}

    <input type="hidden" name="_method" value="put">

    <fieldset>
        <div class="col-md-12">
            <div class="form-group">
                <label> Upload TTD : </label>
                <input type="file" name="signature" value="">
            </div>
            <div class="form-group">
                <label> Template : </label>
                <input type="file" name="template" value="">
            </div>
        </div>

    </fieldset>

    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-save"></i>
            Simpan
        </button>
    </div>
</form>
