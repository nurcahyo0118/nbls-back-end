@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-xs-12">

            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Edit Default</h3>

                </div>

                <div class="box-body">

                    <div class="col-md-12">
                        <form action="{{ route('certificate.store', [$_SERVER['HTTP_HOST'], $certificate->id]) }}"
                              method="post" class="form-horizontal" enctype="multipart/form-data" data-parsley-validate>

                            {{ csrf_field() }}

                            {{--<input type="hidden" name="_method" value="put">--}}

                            <fieldset>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label> Upload TTD : </label>
                                        <input type="file" name="signature" value="{{ $certificate->signature }}">

                                        @if($certificate->signature === null)
                                            <p>Belum ada Tanda Tangan Yang diupload</p>
                                        @else
                                            <img src="/images/photo/{{ $certificate->signature }}" width="100"
                                                 height="100" style="margin:10px;">
                                        @endif

                                    </div>
                                    <div class="form-group">
                                        <label> Template : </label>
                                        <input type="file" name="template" value="{{$certificate->template}}">

                                        @if( $certificate->template === null)
                                            <p>Belum ada Tanda Tangan Yang diupload</p>
                                        @else
                                            <img src="/images/photo/{{ $certificate->template }}" width="150"
                                                 height="100" style="margin:10px;">
                                        @endif
                                    </div>
                                </div>

                            </fieldset>

                            <div class="modal-footer">
                                <a href="{{ route('encounters.index', $_SERVER['HTTP_HOST']) }}"
                                   class="btn btn-default">
                                    <i class="fa fa-close"></i>
                                    Tutup
                                </a>
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-save"></i>
                                    Simpan
                                </button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>

        </div>
    </div>
@endsection
