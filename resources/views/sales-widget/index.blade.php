@extends('layouts.main')

@section('content')

    @php
        $tenant = Auth::user()->tenant;
        $permission = App\Permission::where(['role_id' => Auth::user()->role['id'], 'tenant_id' => $tenant->id])->first();
    @endphp

    <div class="row">

        <div class="col-xs-12">

            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Daftar Sales Widget</h3>
                    <br>
                    <a class="btn btn-primary pull-left col-md-1" href="{{ route('social-learning-sales-widget.create', $_SERVER['HTTP_HOST']) }}">
                        <span class="fa fa-plus"></span>
                    </a>
                </div>

                <div class="box-body">
                    <table class="table js-basic-example dataTable">
                        <thead>
                        <tr>
                            <th>Kursus</th>
                            <th>Diterbitkan</th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody id="coursePackagesList">

                        @foreach($sales_widgets as $sales_widget)
                        
                            <tr>
                                <td>{{ $sales_widget->course->title }}</td>
                                <td>
                                    {{ $sales_widget->is_publish ? 'Diterbitkan' : 'Belum Diterbitkan' }}
                                </td>
                                <td>
                                    @if($sales_widget->is_publish)

                                        <form action="{{ route('social-learning-sales-widget.unpublish', [$_SERVER['HTTP_HOST'], $sales_widget->id]) }}" method="POST">

                                            {{ csrf_field() }}

                                            <button
                                                type="submit"
                                                class="btn btn-default btn-xs">
                                                <i class="fa fa-close"></i>
                                                Benamkan
                                            </button>
                                        </form>
                                    
                                    @else
                                        <form action="{{ route('social-learning-sales-widget.publish', [$_SERVER['HTTP_HOST'], $sales_widget->id]) }}" method="POST">

                                            {{ csrf_field() }}

                                            <button
                                                type="submit"
                                                class="btn btn-success btn-xs">
                                                <i class="fa fa-paper-plane"></i>
                                                Terbitkan
                                            </button>
                                        </form>
                                    @endif
                                </td>
                                <td class="text-right">
                                    <a
                                        href="{{ route('social-learning-sales-widget.edit', [$_SERVER['HTTP_HOST'], $sales_widget->id]) }}"
                                        class="btn btn-default btn-xs">
                                        <i class="fa fa-edit"></i>
                                        Edit
                                    </a>
                                    <a
                                        href="#!"
                                        class="btn btn-danger btn-xs"
                                        onclick="onDelete({{ $sales_widget->id }})">
                                        <i class="fa fa-trash"></i>
                                        Hapus
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div>

            </div>

        </div>
    </div>

@endsection

@section('script')
    <script>
        function publish() {
            swal("Berhasil !", "Paket kursus berhasil diterbitkan", "success");
        }

        function hide() {
            swal("Berhasil !", "Paket kursus berhasil disembunyikan", "success");
        }

        function onDelete(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "Data yang telah di hapus tidak dapat dikembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {

                    $.ajax({
                        url: '/social-learning-sales-widget/' + id,
                        type: 'DELETE',
                        data: null,
                        success: function (data) {
                            window.location.reload();
                        },
                        error: function (error) {
                            swal("Gagal !", "Gagal menghapus kursus", "error");
                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    });

                }
            });
        }
    </script>
@endsection