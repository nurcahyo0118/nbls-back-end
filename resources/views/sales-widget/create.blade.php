@extends('layouts.main')

@section('content')

    @php
        $tenant = Auth::user()->tenant;
        $permission = App\Permission::where(['role_id' => Auth::user()->role['id'], 'tenant_id' => $tenant->id])->first();
    @endphp

    <div class="row">

        <div class="col-xs-12">

            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Buat Widget</h3>

                </div>

                <form 
                    action="{{ route('social-learning-sales-widget.store', $_SERVER['HTTP_HOST']) }}"
                    method="POST"
                    enctype="multipart/form-data"
                    data-parsley-validate>

                    {{ csrf_field() }}

                    <div class="box-body">
                        
                        <fieldset>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Kursus : </label>
                                    <select class="form-control" type="text" name="course_id" required>
                                        <option value="">Pilih Kursus</option>
                                        @foreach(Auth::user()->tenant->courses as $course)
                                            <option value="{{ $course->id }}">{{ $course->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Teks Tombol : </label>
                                    <input class="form-control" type="text" name="button_text" required>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Gambar : </label>
                                    <input class="form-control" type="file" name="image" accept="image/*" required>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Warna Latar Kartu : </label>
                                    <select class="form-control" type="text" name="card_background_color" required>
                                        <option value="">Pilih Warna</option>
                                        
                                        <option value="card-primary" class="text-primary">Biru</option>
                                        <option value="card-info" class="text-info">Biru Muda</option>
                                        <option value="card-warning" class="text-warning">Kuning</option>
                                        <option value="card-default" class="text-default">Abu-Abu</option>
                                        <option value="card-danger" class="text-danger">Merah</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Warna Latar Tombol : </label>
                                    <select class="form-control" type="text" name="button_background_color" required>
                                        <option value="">Pilih Warna</option>
                                        
                                        <option value="btn-primary" class="text-primary">Biru</option>
                                        <option value="btn-info" class="text-info">Biru Muda</option>
                                        <option value="btn-warning" class="text-warning">Kuning</option>
                                        <option value="btn-default" class="text-default">Abu-Abu</option>
                                        <option value="btn-danger" class="text-danger">Merah</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Warna Teks Kartu : </label>
                                    <select class="form-control" type="text" name="card_text_color" required>
                                        <option value="">Pilih Warna</option>
                                        
                                        <option value="text-primary" class="text-primary">Biru</option>
                                        <option value="text-info" class="text-info">Biru Muda</option>
                                        <option value="text-warning" class="text-warning">Kuning</option>
                                        <option value="text-default" class="text-default">Abu-Abu</option>
                                        <option value="text-danger" class="text-danger">Merah</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Warna Teks Tombol : </label>
                                    <select class="form-control" type="text" name="button_text_color" required>
                                        <option value="">Pilih Warna</option>
                                        
                                        <option value="text-primary" class="text-primary">Biru</option>
                                        <option value="text-info" class="text-info">Biru Muda</option>
                                        <option value="text-warning" class="text-warning">Kuning</option>
                                        <option value="text-default" class="text-default">Abu-Abu</option>
                                        <option value="text-danger" class="text-danger">Merah</option>
                                        
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>
                                        <input type="checkbox" name="is_publish">
                                        Publikasikan
                                    </label>
                                </div>
                            </div>

                        </fieldset>
                        
                    </div>

                    <div class="box-footer">
                        <div class="col-md-12 text-right">
                            <a 
                                href="{{ route('social-learning-sales-widget.index', $_SERVER['HTTP_HOST']) }}"
                                class="btn btn-default">
                                <i class="fa fa-close"></i>
                                Batal
                            </a>
                            <button type="submit" class="btn btn-success">
                                <i class="fa fa-save"></i>
                                Simpan
                            </button>
                        </div>
                    </div>

                </form>

            </div>

        </div>
    </div>

@endsection

@section('script')
    <script>
        function onSelectCategory(value) {
            var courseSubCategoryId = $('#course_sub_category_id');

            $.ajax({
                type: 'GET',
                url: '/loads/subcategories/' + value,
                success: function (response) {
                    console.log(response);

                    courseSubCategoryId.empty();

                    courseSubCategoryId.append('<option value="">Pilih Sub Kategori</option>');

                    for (let subcategory of response) {
                        console.log(subcategory);
                        courseSubCategoryId.append('<option value="' + subcategory.id + '">' + subcategory.name + '</option>');
                    }

                },
                error: function (error) {
                    console.log(error);
                }
            });

        }
    </script>
@endsection