@extends('layouts.main')

@section('content')

    @php
        $tenant = Auth::user()->tenant;
        $permission = App\Permission::where(['role_id' => Auth::user()->role['id'], 'tenant_id' => $tenant->id])->first();
    @endphp

    <div class="row">

        <div class="col-xs-12">

            <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">Edit Widget</h3>

                </div>

                <form 
                    action="{{ route('social-learning-sales-widget.update', [$_SERVER['HTTP_HOST'], $widget->id]) }}"
                    method="POST"
                    enctype="multipart/form-data"
                    data-parsley-validate>

                    {{ csrf_field() }}

                    <input type="hidden" name="_method" value="PUT">

                    <div class="box-body">
                        
                        <fieldset>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Kursus : </label>
                                    <select class="form-control" type="text" name="course_id" required>
                                        <option value="">Pilih Kursus</option>
                                        @foreach(Auth::user()->tenant->courses as $course)
                                            <option value="{{ $course->id }}" {{ $widget->course_id == $course->id ? 'selected' : '' }}>{{ $course->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Teks Tombol : </label>
                                    <input class="form-control" type="text" name="button_text" value="{{ $widget->button_text }}" required>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Gambar : </label>
                                    <input class="form-control" type="file" name="image" accept="image/*">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Warna Latar Kartu : </label>
                                    <select class="form-control" type="text" name="card_background_color" required>
                                        <option value="">Pilih Warna</option>

                                        <option value="card-primary" class="text-primary" {{ $widget->card_background_color == 'card-primary' ? 'selected' : '' }}>Biru</option>
                                        <option value="card-info" class="text-info" {{ $widget->card_background_color == 'card-info' ? 'selected' : '' }}>Biru Muda</option>
                                        <option value="card-warning" class="text-warning" {{ $widget->card_background_color == 'card-warning' ? 'selected' : '' }}>Kuning</option>
                                        <option value="card-default" class="text-default" {{ $widget->card_background_color == 'card-default' ? 'selected' : '' }}>Abu-Abu</option>
                                        <option value="card-danger" class="text-danger" {{ $widget->card_background_color == 'card-danger' ? 'selected' : '' }}>Merah</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Warna Latar Tombol : </label>
                                    <select class="form-control" type="text" name="button_background_color" required>
                                        <option value="">Pilih Warna</option>
                                        
                                        <option value="btn-primary" class="text-primary" {{ $widget->button_background_color == 'btn-primary' ? 'selected' : '' }}>Biru</option>
                                        <option value="btn-info" class="text-info" {{ $widget->button_background_color == 'btn-info' ? 'selected' : '' }}>Biru Muda</option>
                                        <option value="btn-warning" class="text-warning" {{ $widget->button_background_color == 'btn-warning' ? 'selected' : '' }}>Kuning</option>
                                        <option value="btn-default" class="text-default" {{ $widget->button_background_color == 'btn-default' ? 'selected' : '' }}>Abu-Abu</option>
                                        <option value="btn-danger" class="text-danger" {{ $widget->button_background_color == 'btn-danger' ? 'selected' : '' }}>Merah</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Warna Teks Kartu : </label>
                                    <select class="form-control" type="text" name="card_text_color" required>
                                        <option value="">Pilih Warna</option>
                                        
                                        <option value="text-primary" class="text-primary" {{ $widget->card_text_color == 'text-primary' ? 'selected' : '' }}>Biru</option>
                                        <option value="text-info" class="text-info" {{ $widget->card_text_color == 'text-info' ? 'selected' : '' }}>Biru Muda</option>
                                        <option value="text-warning" class="text-warning" {{ $widget->card_text_color == 'text-warning' ? 'selected' : '' }}>Kuning</option>
                                        <option value="text-default" class="text-default" {{ $widget->card_text_color == 'text-default' ? 'selected' : '' }}>Abu-Abu</option>
                                        <option value="text-danger" class="text-danger" {{ $widget->card_text_color == 'text-danger' ? 'selected' : '' }}>Merah</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Warna Teks Tombol : </label>
                                    <select class="form-control" type="text" name="button_text_color" required>
                                        <option value="">Pilih Warna</option>
                                        
                                        <option value="text-primary" class="text-primary" {{ $widget->button_text_color == 'text-primary' ? 'selected' : '' }}>Biru</option>
                                        <option value="text-info" class="text-info" {{ $widget->button_text_color == 'text-info' ? 'selected' : '' }}>Biru Muda</option>
                                        <option value="text-warning" class="text-warning" {{ $widget->button_text_color == 'text-warning' ? 'selected' : '' }}>Kuning</option>
                                        <option value="text-default" class="text-default" {{ $widget->button_text_color == 'text-default' ? 'selected' : '' }}>Abu-Abu</option>
                                        <option value="text-danger" class="text-danger" {{ $widget->button_text_color == 'text-danger' ? 'selected' : '' }}>Merah</option>
                                        
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>
                                        <input type="checkbox" name="is_publish" {{ $widget->is_publish ? 'checked' : '' }}>
                                        Publikasikan
                                    </label>
                                </div>
                            </div>

                        </fieldset>
                        
                    </div>

                    <div class="box-footer">
                        <div class="col-md-12 text-right">
                            <a 
                                href="{{ route('social-learning-sales-widget.index', $_SERVER['HTTP_HOST']) }}"
                                class="btn btn-default">
                                <i class="fa fa-close"></i>
                                Batal
                            </a>
                            <button type="submit" class="btn btn-success">
                                <i class="fa fa-save"></i>
                                Simpan
                            </button>
                        </div>
                    </div>

                </form>

            </div>

        </div>
    </div>

@endsection

@section('script')
    <script>
        function onSelectCategory(value) {
            var courseSubCategoryId = $('#course_sub_category_id');

            $.ajax({
                type: 'GET',
                url: '/loads/subcategories/' + value,
                success: function (response) {
                    console.log(response);

                    courseSubCategoryId.empty();

                    courseSubCategoryId.append('<option value="">Pilih Sub Kategori</option>');

                    for (let subcategory of response) {
                        console.log(subcategory);
                        courseSubCategoryId.append('<option value="' + subcategory.id + '">' + subcategory.name + '</option>');
                    }

                },
                error: function (error) {
                    console.log(error);
                }
            });

        }
    </script>
@endsection