<?php
Route::group(['middleware' => ['log']], function () {

    // Load All Course Package
    Route::get('loads/course-package-front-end/all', 'CoursePackageFrontendController@loadAllCourses');
    // Load Searching Course Package
    Route::get('loads/course-package-front-end/{keyword}', 'CoursePackageFrontendController@loadSearchAllCourses');
    // Load All Category Package
    Route::get('loads/course-package-front-end/categories/all', 'CoursePackageFrontendController@loadAllCategories');

    Route::get('loads/course-package-front-end/categories/{id}/courses', 'CoursePackageFrontendController@loadCategoryCourses');
    // Load SubCategory by Category Package
    Route::get('loads/course-package-front-end/subcategories/{id}', 'CoursePackageFrontendController@loadSubCategory');
    // Load All Course Order By Price Package
    Route::get('loads/course-package-front-end/orderby/prices', 'CoursePackageFrontendController@loadAllCoursesByPrices');
    // Load All Course Order By Price Package
    Route::get('loads/course-package-front-end/orderby/popular', 'CoursePackageFrontendController@loadAllCoursesByPopular');

    // Load All Course
    Route::get('loads/social-learning-course-front-end/all', 'CourseFrontEndSocialLearning@loadAllCourses');
    // Load Searching Course
    Route::get('loads/social-learning-course-front-end/{keyword}', 'CourseFrontEndSocialLearning@loadSearchAllCourses');
    // Load All Category
    Route::get('loads/social-learning-course-front-end/categories/all', 'CourseFrontEndSocialLearning@loadAllCategories');

    Route::get('loads/social-learning-course-front-end/categories/{id}/courses', 'CourseFrontEndSocialLearning@loadCategoryCourses');
    // Load SubCategory by Category
    Route::get('loads/social-learning-course-front-end/subcategories/{id}', 'CourseFrontEndSocialLearning@loadSubCategory');
    // Load All Course Order By Price
    Route::get('loads/social-learning-course-front-end/orderby/prices', 'CourseFrontEndSocialLearning@loadAllCoursesByPrices');
    // Load All Course Order By Price
    Route::get('loads/social-learning-course-front-end/orderby/popular', 'CourseFrontEndSocialLearning@loadAllCoursesByPopular');

    // Load Carts Frontend Data
    Route::get('loads/frontend/carts', 'CourseFrontEndSocialLearning@loadAllCarts');
    Route::get('loads/frontend/package-carts', 'CourseFrontEndSocialLearning@loadAllPackageCarts');

    // Course Packages
    Route::resource('course-package-frontend', 'CoursePackageFrontendController');

    // Pages
    Route::get('/cpages/{url}', [
        'as' => 'password.request',
        'uses' => 'PageController@showPage'
    ]);

    Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');

    // Reset Password
    Route::get('/password/reset', [
        'as' => 'password.request',
        'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm'
    ]);

    Route::post('/password/email', [
        'as' => 'password.email',
        'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail'
    ]);

    Route::get('/password/reset/{token}', [
        'as' => 'password.reset',
        'uses' => 'Auth\ResetPasswordController@showResetForm'
    ]);

    Route::post('/password/reset', [
        'as' => 'reset',
        'uses' => 'Auth\ResetPasswordController@reset'
    ]);


    Route::get('phpinfo', function () {
        return phpinfo();
    });

    Route::get('payments', 'HomeController@payments');

    Route::get('noPermission', 'DashboardController@noPermission')->name('noPermission');

    // Route::post('register', 'Auth\RegisterController@register');

    Route::get('social-learning', 'HomeController@index');


    Route::post('register', [
        'as' => 'register',
        'uses' => 'LoginController@postRegister'
    ]);


    // Route::post('login', [
    //     'as' => 'login',
    //     'uses' => 'LoginController@postLogin'
    // ]);

    Route::group(['middleware' => ['authen']], function () {

        // Load Carts Frontend Data
        Route::get('loads/carts', 'MyCourseSocialLearningController@loadAllCarts');
        Route::get('loads/package-carts', 'MyCourseSocialLearningController@loadAllPackageCarts');

        // Remove Item Cart
        Route::delete('carts/{course_id}/remove', 'MyCourseSocialLearningController@removeCourseFromCart');
        Route::delete('package-carts/{course_id}/remove', 'MyCourseSocialLearningController@removePackageCourseFromCart');

        // Sync Carts Data
        Route::post('sync/carts', [
            'as' => 'social-learning-my-course.sync-carts-data',
            'uses' => 'MyCourseSocialLearningController@syncCartsData'
        ]);

        // Payments
        Route::post('payments/register', [
            'as' => 'payments.register',
            'uses' => 'PaymentSocialLearningController@createTransaction'
        ]);

        // DB Proccess URL
        Route::get('payments/confirm/{id}', [
            'as' => 'payments.order.confirm',
            'uses' => 'PaymentSocialLearningController@confirmPayment'
        ]);

        Route::get('notification', [
            'as' => 'notification.setting',
            'uses' => 'NotificationSettingController@index'
        ]);

        Route::get('notification/all', [
            'as' => 'notification.show.all',
            'uses' => 'NotificationSettingController@showAll'
        ]);

        Route::put('notification', [
            'as' => 'notification.setting.update',
            'uses' => 'NotificationSettingController@update'
        ]);

        // Profile
        Route::resource('social-learning-profile', 'ProfileSocialLearningController');
        
        // Posts
        Route::resource('social-learning-posts', 'PostSocialLearningController');

        Route::put('me/update-image/social', 'ProfileController@changeSocialImage');

        // Badge
        Route::post('badges/{id}/claim', [
            'as' => 'badges.claim',
            'uses' => 'BadgeSocialLearningController@claim'
        ]);

        // Question
        Route::post('social-learning-questions/{id}/likes', [
            'as' => 'questions.like',
            'uses' => 'QuestionSocialLearningController@like'
        ]);

        Route::post('social-learning-questions/{id}/dislikes', [
            'as' => 'questions.dislike',
            'uses' => 'QuestionSocialLearningController@dislike'
        ]);

        Route::post('social-learning-questions/{id}/comments', [
            'as' => 'questions.comments',
            'uses' => 'QuestionSocialLearningController@postComment'
        ]);

        Route::post('social-learning-questions/comments/{id}/likes', [
            'as' => 'questions.comments.like',
            'uses' => 'QuestionSocialLearningController@likeComment'
        ]);

        Route::post('social-learning-questions/comments/{id}/dislikes', [
            'as' => 'questions.comments.dislike',
            'uses' => 'QuestionSocialLearningController@dislikeComment'
        ]);

        // Question
        Route::post('questions/{id}/likes', [
            'as' => 'questions.like',
            'uses' => 'QuestionController@like'
        ]);

        Route::post('questions/{id}/dislikes', [
            'as' => 'questions.dislike',
            'uses' => 'QuestionController@dislike'
        ]);

        Route::post('questions/{id}/comments', [
            'as' => 'questions.comments',
            'uses' => 'QuestionController@postComment'
        ]);

        Route::post('questions/comments/{id}/likes', [
            'as' => 'questions.comments.like',
            'uses' => 'QuestionController@likeComment'
        ]);

        Route::post('questions/comments/{id}/dislikes', [
            'as' => 'questions.comments.dislike',
            'uses' => 'QuestionController@dislikeComment'
        ]);


        /* Post */
        Route::post('/posts/{id}/likes', [
            'as' => 'like',
            'uses' => 'PostSocialLearningController@like'
        ]);

        Route::post('/posts/{id}/dislikes', [
            'as' => 'dislike',
            'uses' => 'PostSocialLearningController@dislike'
        ]);

        Route::post('/posts/{id}/comments', [
            'as' => 'comments',
            'uses' => 'PostSocialLearningController@postComment'
        ]);

        Route::post('/comments/{id}/likes', [
            'as' => 'comments.like',
            'uses' => 'PostSocialLearningController@likeComment'
        ]);

        Route::post('/comments/{id}/dislikes', [
            'as' => 'comments.dislike',
            'uses' => 'PostSocialLearningController@dislikeComment'
        ]);

        /* End Post */

        /* MasterPiece */
        Route::post('/posts-masterpieces/{id}/likes', [
            'as' => 'like.masterpieces',
            'uses' => 'MasterpieceSocialLearningController@like'
        ]);

        Route::post('/posts-masterpieces/{id}/dislikes', [
            'as' => 'dislike.masterpieces',
            'uses' => 'MasterpieceSocialLearningController@dislike'
        ]);

        Route::post('/posts-masterpieces/{id}/comments', [
            'as' => 'comments.masterpieces',
            'uses' => 'MasterpieceSocialLearningController@postComment'
        ]);

        Route::post('/comments-masterpieces/{id}/likes', [
            'as' => 'comments.like.masterpieces',
            'uses' => 'MasterpieceSocialLearningController@likeComment'
        ]);

        Route::post('/comments-masterpieces/{id}/dislikes', [
            'as' => 'comments.dislike.masterpieces',
            'uses' => 'MasterpieceSocialLearningController@dislikeComment'
        ]);

        /* End MasterPiece */

        /* Course */
        Route::post('/courses/{id}/join', 'CourseController@join');

        Route::post('/courses/{id}/leave', 'CourseController@leave');



        // Courses Pages

        Route::get('courses/{id}/sections-page', [
            'as' => 'courses.page.sections',
            'uses' => 'CourseController@sectionsPage'
        ]);

        Route::get('courses/{id}/quizzes-page', [
            'as' => 'courses.page.quizzes',
            'uses' => 'CourseController@quizzesPage'
        ]);

        Route::get('courses/{id}/exercises-page', [
            'as' => 'courses.page.exercises',
            'uses' => 'CourseController@exercisesPage'
        ]);

        Route::get('courses/{id}/references-page', [
            'as' => 'courses.page.references',
            'uses' => 'CourseController@referencesPage'
        ]);

        /* Event Routes */
        Route::resource('social-learning-events', 'EventSocialLearningController');
        
        Route::post('/social-learning-events/{id}/follow', [
            'as' => 'events.follow',
            'uses' => 'EventSocialLearningController@followEvent'
        ]);

        Route::post('/social-learning-events/{id}/unfollow', [
            'as' => 'events.unfollow',
            'uses' => 'EventSocialLearningController@unFollowEvent'
        ]);
        /* End Event */


        // Follow user
        Route::post('/users/{id}/follow', [
            'as' => 'users.follow',
            'uses' => 'UserController@follow'
        ]);

        Route::post('/users/{id}/unfollow', [
            'as' => 'users.unfollow',
            'uses' => 'UserController@unfollow'
        ]);

        Route::resource('social-learning-questions', 'QuestionSocialLearningController');


        Route::resource('social-learning-itime', 'ItimeSocialLearningController');

        Route::post('exchange-point/{id}', [
            'as' => 'exchange-point',
            'uses' => 'ExchangePointSocialLearningController@sampleExchange'
        ]);

        Route::resource('reviews', 'ReviewSocialLearningController');

        Route::post('course/rate', [
            'as' => 'social-learning-course.rate',
            'uses' => 'CourseSocialLearningController@rating'
        ]);

        Route::put('course/rate', [
            'as' => 'social-learning-course.update-rating',
            'uses' => 'CourseSocialLearningController@updateRating'
        ]);

        Route::delete('course/{course_id}/rate', [
            'as' => 'social-learning-course.delete-rating',
            'uses' => 'CourseSocialLearningController@deleteRating'
        ]);

        // Certificates
        Route::resource('social-learning-e-certificate', 'ECertificateSocialLearningController');

        Route::get('social-learning-e-certificate/{id}/preview', [
            'as' => 'social-learning-e-certificate.preview',
            'uses' => 'ECertificateSocialLearningController@preview'
        ]);

    });

    /* Tenant Routes */
    Route::group(array(
        'domain' => '{subdomain}.' . env('HOST_NAME'),
        'as' => 'tenant.',
        'middleware' => 'multisubdomain'
    ), (function () {

        // Frontend courses
        Route::resource('social-learning-course-front-end', 'CourseFrontEndSocialLearning');

        /* Auth Routes*/
        Route::get('/', [
            'as' => 'landing',
            'uses' => 'HomeController@portal'
        ]);

        Route::post('login', [
            'as' => 'login',
            'uses' => 'LoginController@postTenantLogin'
        ]);

        Route::get('login', 'LoginController@getTenantLogin')->name('login');

        Route::get('register', 'HomeController@tenantRegister')->name('register');

        Route::group(['middleware' => ['authen.tenant']], function () {

            Route::post('/logout', [
                'as' => 'logout',
                'uses' => 'LoginController@tenantLogout'
            ]);

            Route::get('/dashboard', [
                'as' => 'dashboard',
                'uses' => 'DashboardController@dashboard'
            ]);

            Route::get('me', [
                'as' => 'me',
                'uses' => 'ProfileController@index'
            ]);

            Route::get('me/edit', [
                'as' => 'me.edit',
                'uses' => 'ProfileController@edit'
            ]);

            Route::put('me/update', [
                'as' => 'me.update',
                'uses' => 'ProfileController@update'
            ]);

            Route::put('me/update-image', [
                'as' => 'me.update.image',
                'uses' => 'ProfileController@changeImage'
            ]);

            Route::put('me/update-password', [
                'as' => 'me.update.password',
                'uses' => 'ProfileController@changePassword'
            ]);

            /* My Tenant */

            Route::get('tenants/me', [
                'as' => 'tenants.me',
                'uses' => 'tenant\TenantController@me'
            ]);

            Route::get('tenants/me/edit', [
                'as' => 'tenants.me.edit',
                'uses' => 'tenant\TenantController@meEdit'
            ]);

            Route::get('tenants/me/select-superadmin', [
                'as' => 'tenants.selectExpertPage',
                'uses' => 'tenant\TenantController@selectExpertPage'
            ]);

            Route::post('tenants/me/select-superadmin', [
                'as' => 'tenants.selectExpert',
                'uses' => 'tenant\TenantController@selectExpert'
            ]);

            Route::post('tenants/me/unselect-superadmin', [
                'as' => 'tenants.unselectExpert',
                'uses' => 'tenant\TenantController@unselectExpert'
            ]);

            /* End My Tenant */

            Route::resource('tenants', 'tenant\TenantController');

            Route::resource('categories', 'CategoryController');

            Route::resource('sub_categories', 'tenant\SubCategoryController');

            Route::resource('users', 'UserController');

            Route::resource('roles', 'tenant\RoleController');

            Route::resource('sections', 'tenant\SectionController');

            Route::post('courses/{id}/section', [
                'as' => 'courses.add.section',
                'uses' => 'SectionController@store'
            ]);

            Route::resource('courses', 'tenant\CourseController');

            Route::get('users/role/{id}', [
                'as' => 'users.role',
                'uses' => 'UserController@usersByRole'
            ]);

            Route::put('user/update-role/{id}', [
                'as' => 'changeRole',
                'uses' => 'UserController@changeRole'
            ]);

            Route::post('users/select-superadmin', 'UserController@selectExpert')->name('users.selectExpert');

            Route::delete('sections/delete-all/{id}', [
                'as' => 'sections.delete.all',
                'uses' => 'SectionController@deleteAll'
            ]);

            Route::post('material/store', 'MaterialController@postMaterial');

            Route::delete('material/delete/{id}', 'MaterialController@deleteMaterial');

            Route::resource('quizzes', 'QuizController');

            Route::resource('quizzes', 'QuizController');

        });
        /* End Auth Routes*/

        // Instructor Routes
        Route::get('instructor/students', [
            'as' => 'instructor.students',
            'uses' => 'UserController@students'
        ]);

        // Route::get('instructor/exercises', [
        //     'as' => 'instructor.exercises',
        //     'uses' => 'UserController@exercises'
        // ]);

        Route::get('instructor/encounters', [
            'as' => 'instructor.encounters',
            'uses' => 'EncounterController@instructorEncounter'
        ]);

        Route::get('instructor/encounters/{id}/answers', [
            'as' => 'instructor.encounters.answers',
            'uses' => 'EncounterController@answers'
        ]);

        Route::get('instructor/video-comments', [
            'as' => 'instructor.video-comments',
            'uses' => 'QuestionController@encounterVideoComments'
        ]);

        Route::get('instructor/students/performance', [
            'as' => 'instructor.students.performance',
            'uses' => 'PerformanceController@index'
        ]);

        Route::get('instructor/students/performance/{id}', [
            'as' => 'instructor.students.performance.show',
            'uses' => 'PerformanceController@show'
        ]);

        // END Instructor Routes

        // Voucher
        Route::resource('vouchers', 'VoucherController');

        Route::post('vouchers/check', [
            'as' => 'vouchers.check',
            'uses' => 'VoucherController@check'
        ]);
        // End Voucher

    }));
    /* end Tenant Routes */

    Route::group(array('domain' => '{domain}'), (function () {

        // Frontend routes
        Route::resource('social-learning-course-front-end', 'CourseFrontEndSocialLearning');

        Route::get('/', [
            'as' => '/',
            'uses' => 'HomeController@portal',
            'middleware' => 'multidomain'
        ]);

        Route::post('login', [
            'as' => 'login',
            'uses' => 'LoginController@postLogin'
        ]);

        Route::get('login', 'LoginController@getLogin')->name('login');

        Route::post('/logout', [
            'as' => 'logout',
            'uses' => 'LoginController@getLogout'
        ]);

        Route::get('register', 'HomeController@register')->name('register');

        Route::group(['middleware' => ['authen']], function () {

            Route::post('/logout', [
                'as' => 'logout',
                'uses' => 'LoginController@getLogout'
            ]);

            Route::get('/dashboard', [
                'as' => 'dashboard',
                'uses' => 'DashboardController@dashboard'
            ]);

            Route::get('me', [
                'as' => 'me',
                'uses' => 'ProfileController@index'
            ]);

            Route::get('me/edit', [
                'as' => 'me.edit',
                'uses' => 'ProfileController@edit'
            ]);

            Route::put('me/update', [
                'as' => 'me.update',
                'uses' => 'ProfileController@update'
            ]);

            Route::put('me/update-image', [
                'as' => 'me.update.image',
                'uses' => 'ProfileController@changeImage'
            ]);

            Route::put('me/update-password', [
                'as' => 'me.update.password',
                'uses' => 'ProfileController@changePassword'
            ]);

            Route::post('notifications/see', [
                'as' => 'notifications.see',
                'uses' => 'MyProfileController@seeNotification'
            ]);

            /* My Tenant */

            Route::get('tenants/me', [
                'as' => 'tenants.me',
                'uses' => 'TenantController@me'
            ]);

            Route::get('tenants/me/edit', [
                'as' => 'tenants.me.edit',
                'uses' => 'TenantController@meEdit'
            ]);

            Route::get('tenants/{id}/select-superadmin', [
                'as' => 'tenants.selectSuperAdminPage',
                'uses' => 'TenantController@selectSuperAdminPage'
            ]);

            Route::post('tenants/{id}/select-superadmin', [
                'as' => 'tenants.selectSuperAdmin',
                'uses' => 'TenantController@selectSuperAdmin'
            ]);

            Route::post('tenants/{id}/unselect-superadmin', [
                'as' => 'tenants.unselectSuperAdmin',
                'uses' => 'TenantController@unselectSuperAdmin'
            ]);

            /* End My Tenant */

            Route::resource('tenants', 'TenantController');

            Route::resource('categories', 'CategoryController');

            // Route::resource('payments', 'PaymentController');

            Route::resource('post', 'PostController');

            Route::resource('sub_categories', 'SubCategoryController');

            Route::resource('users', 'UserController');

            Route::resource('roles', 'RoleController');

            Route::resource('sections', 'SectionController');
            Route::resource('references', 'ReferenceController');

            Route::resource('exercises', 'ExerciseController');

            Route::resource('social-learning-search-friend', 'SearchFriendSocialLearningController');

            Route::resource('social-learning-best-answers', 'BestAnswerSocialLearningController');

            Route::resource('social-learning-setting', 'SettingSocialLearningController');

            Route::resource('social-learning-exchange-point', 'ExchangePointSocialLearningController');

            Route::resource('social-learning-payment', 'PaymentSocialLearningController');

            Route::resource('social-learning-course', 'CourseSocialLearningController');

            Route::resource('social-learning-sales-widget', 'SalesWidgetSocialLearningController');

            Route::resource('broadcast', 'BroadcastController');

            Route::post('social-learning-sales-widget/{id}/publish', [
                'as' => 'social-learning-sales-widget.publish',
                'uses' => 'SalesWidgetSocialLearningController@publish'
            ]);

            Route::post('social-learning-sales-widget/{id}/unpublish', [
                'as' => 'social-learning-sales-widget.unpublish',
                'uses' => 'SalesWidgetSocialLearningController@unpublish'
            ]);

            // Load All Course
            Route::get('loads/social-learning-course/all', 'CourseSocialLearningController@loadAllCourses');
            // Load Searching Course
            Route::get('loads/social-learning-course/{keyword}', 'CourseSocialLearningController@loadSearchAllCourses');
            // Load All Category
            Route::get('loads/categories/all', 'CourseSocialLearningController@loadAllCategories');
            // Load SubCategory by Category
            Route::get('loads/subcategories/{id}', 'CategoryController@loadSubCategory');
            // Load All Course Order By Price
            Route::get('loads/social-learning-course/orderby/prices', 'CourseSocialLearningController@loadAllCoursesByPrices');
            // Load All Course Order By Price
            Route::get('loads/social-learning-course/orderby/popular', 'CourseSocialLearningController@loadAllCoursesByPopular');

            Route::resource('social-learning-exam', 'ExamSocialLearningController');
            Route::resource('social-learning-order-history', 'OrderHistorySocialLearningController');
            Route::resource('social-learning-masterpiece', 'MasterpieceSocialLearningController');
            Route::resource('social-learning-my-course', 'MyCourseSocialLearningController');
            
            // Route::resource('social-learning-e-certificate', 'ECertificateSocialLearningController');

            Route::resource('certificate', 'CertificateController');

            Route::resource('social-learning-my-performance', 'MyPerformanceSocialLearningController');
            Route::resource('social-learning-booking', 'BookingSocialLearningController');
            Route::resource('social-learning-my-badge', 'MyBadgeSocialLearningController');
            Route::resource('social-learning-course-detail', 'ListDetailSocialLearningController');

            Route::get('social-learning-my-course/{id}/material/{material_id}', [
                'as' => 'social-learning-my-course.material',
                'uses' => 'MyCourseSocialLearningController@currentMaterial'
            ]);

            Route::get('social-learning-my-course/{id}/essay/{essay_id}', [
                'as' => 'social-learning-my-course.essay',
                'uses' => 'MyCourseSocialLearningController@currentEssay'
            ]);

            Route::get('social-learning-my-course/{id}/exercise/{exercise_id}', [
                'as' => 'social-learning-my-course.exercise',
                'uses' => 'MyCourseSocialLearningController@currentExercise'
            ]);

            Route::post('social-learning-my-course/exercise/{id}/store', [
                'as' => 'social-learning-my-course.exercise.store',
                'uses' => 'MyCourseSocialLearningController@storeExercise'
            ]);

            Route::post('social-learning-my-course/encounter/{id}/store', [
                'as' => 'social-learning-my-course.encounter.store',
                'uses' => 'MyCourseSocialLearningController@storeEncounter'
            ]);

            Route::post('social-learning-my-course/{id}/add-to-cart', [
                'as' => 'social-learning-my-course.addtocart',
                'uses' => 'MyCourseSocialLearningController@addToCart'
            ]);

            Route::post('social-learning-my-course/{id}/add-to-cart-without-login', [
                'as' => 'social-learning-my-course.addtocartwithoutlogin',
                'uses' => 'MyCourseSocialLearningController@addToCartWithoutLogin'
            ]);

            Route::delete('social-learning-my-course/delete-cart/{id}', [
                'as' => 'social-learning-my-course.delete.cart',
                'uses' => 'MyCourseSocialLearningController@deleteCart'
            ]);

            Route::delete('sections/delete-all/{id}', [
                'as' => 'sections.delete.all',
                'uses' => 'SectionController@deleteAll'
            ]);

             Route::post('social-learning-my-course/{id}/add-to-cart', [
                'as' => 'social-learning-my-course.addtocart',
                'uses' => 'MyCourseSocialLearningController@addToCart'
            ]);

            Route::get('social-learning-exam/{id}/form', [
                'as' => 'social-learning-exam.form-input',
                'uses' => 'ExamSocialLearningController@formInputEncounter'
            ]);


            Route::post('courses/{id}/section', [
                'as' => 'courses.add.section',
                'uses' => 'SectionController@store'
            ]);

            Route::resource('courses', 'CourseController');

            Route::get('users/role/{id}', [
                'as' => 'users.role',
                'uses' => 'UserController@usersByRole'
            ]);

            Route::put('user/update-role/{id}', [
                'as' => 'changeRole',
                'uses' => 'UserController@changeRole'
            ]);

            Route::post('users/select-superadmin', 'UserController@selectExpert')->name('users.selectExpert');

            Route::delete('sections/delete-all/{id}', [
                'as' => 'sections.delete.all',
                'uses' => 'SectionController@deleteAll'
            ]);

            Route::post('material/store', 'MaterialController@postMaterial');

            Route::delete('material/delete/{id}', 'MaterialController@deleteMaterial');

            Route::resource('quizzes', 'QuizController');

            Route::post('social-learning-my-course/quiz/{id}/store', [
                'as' => 'social-learning-my-course.quiz.store',
                'uses' => 'QuizController@answerQuiz'
            ]);

            Route::resource('social-learning-course-packages', 'CoursePackageSocialLearningController');

            // Course Package
            Route::post('/course-packages/{id}/join', [
                'as' => 'course-package.join',
                'uses' => 'CoursePackageSocialLearningController@buyPackage'
            ]);

            Route::resource('pages', 'PageController');

            // Encounters
            Route::resource('encounters', 'EncounterController');

            Route::get('encounters/{id}/criteria', [
                'as' => 'encounters.criterias.index',
                'uses' => 'EncounterController@getCriterias'
            ]);

            Route::post('encounters/{id}/criteria', [
                'as' => 'encounters.criterias.store',
                'uses' => 'EncounterController@storeCriteria'
            ]);

            Route::put('encounters/{id}/criteria/{criteria_id}', [
                'as' => 'encounters.criterias.update',
                'uses' => 'EncounterController@updateCriteria'
            ]);

            Route::put('encounters/{id}/criteria', [
                'as' => 'encounters.criterias.weight.update',
                'uses' => 'EncounterController@updateCriteriaWeight'
            ]);

            Route::delete('encounters/{id}/criteria/{criteria_id}', [
                'as' => 'encounters.criterias.delete',
                'uses' => 'EncounterController@deleteCriteria'
            ]);

            // Assessments
            Route::get('encounters/{id}/assessments', [
                'as' => 'encounters.assessments.index',
                'uses' => 'EncounterController@getAssessments'
            ]);

            Route::post('encounters/{id}/criteria/{user_id}', [
                'as' => 'encounters.assessments.store',
                'uses' => 'EncounterController@storeAssessment'
            ]);

            // Exercises Assessments
            Route::get('exercises/{id}/criteria', [
                'as' => 'exercises.criterias.index',
                'uses' => 'ExerciseController@getCriterias'
            ]);

            Route::post('exercises/{id}/criteria', [
                'as' => 'exercises.criterias.store',
                'uses' => 'ExerciseController@storeCriteria'
            ]);

            Route::put('exercises/{id}/criteria/{criteria_id}', [
                'as' => 'exercises.criterias.update',
                'uses' => 'ExerciseController@updateCriteria'
            ]);

            Route::put('exercises/{id}/criteria', [
                'as' => 'exercises.criterias.weight.update',
                'uses' => 'ExerciseController@updateCriteriaWeight'
            ]);

            Route::delete('exercises/{id}/criteria/{criteria_id}', [
                'as' => 'exercises.criterias.delete',
                'uses' => 'ExerciseController@deleteCriteria'
            ]);

            // Assessments
            Route::get('exercises/{id}/assessments', [
                'as' => 'exercises.assessments.index',
                'uses' => 'ExerciseController@getAssessments'
            ]);

            Route::post('exercises/{id}/criteria/{user_id}', [
                'as' => 'exercises.assessments.store',
                'uses' => 'ExerciseController@storeAssessment'
            ]);

            Route::resource('classes', 'BackendClassController');

            Route::get('video-libraries', [
                'as' => 'video-libraries.index',
                'uses' => 'VideoLibraryController@index'
            ]);

            Route::get('video-libraries/videos', [
                'as' => 'video-libraries.videos',
                'uses' => 'VideoLibraryController@materialVideos'
            ]);

            Route::get('/instructor/videos-comments', [
                'as' => 'instructor.videos.comments',
                'uses' => 'UserController@videoComments'
            ]);

            Route::get('/instructor/quizzes', [
                'as' => 'instructor.quizzes',
                'uses' => 'UserController@videoQuizzes'
            ]);

            Route::get('/instructor/essays', [
                'as' => 'instructor.essays',
                'uses' => 'UserController@essays'
            ]);

            // Reports
            Route::get('/finances', [
                'as' => 'finances.index',
                'uses' => 'FinanceController@index'
            ]);

            Route::get('/logs', [
                'as' => 'logs.index',
                'uses' => 'LogController@index'
            ]);

            Route::get('/logs/{id}', [
                'as' => 'logs.show',
                'uses' => 'LogController@show'
            ]);

            Route::get('/i-times', [
                'as' => 'i-times.index',
                'uses' => 'ITimeController@index'
            ]);

            Route::get('/i-times/{id}', [
                'as' => 'i-times.show',
                'uses' => 'ITimeController@show'
            ]);

            // Instructor Routes
            Route::get('instructor/students', [
                'as' => 'instructor.students',
                'uses' => 'UserController@students'
            ]);

            Route::get('instructor/exercises', [
                'as' => 'instructor.exercises',
                'uses' => 'ExerciseController@index'
            ]);

            Route::get('instructor/exercises/{id}', [
                'as' => 'instructor.show-exercise',
                'uses' => 'ExerciseController@show'
            ]);

            Route::get('instructor/exercises/{id}/submitted', [
                'as' => 'instructor.submitted-exercise',
                'uses' => 'ExerciseController@submittedExercise'
            ]);

            Route::get('instructor/encounters', [
                'as' => 'instructor.encounters',
                'uses' => 'EncounterController@instructorEncounter'
            ]);

            Route::get('instructor/encounters/{id}/answers', [
                'as' => 'instructor.encounters.answers',
                'uses' => 'EncounterController@answers'
            ]);

            Route::get('instructor/video-comments', [
                'as' => 'instructor.video-comments',
                'uses' => 'QuestionController@encounterVideoComments'
            ]);

            Route::get('instructor/students/performance', [
                'as' => 'instructor.students.performance',
                'uses' => 'PerformanceController@index'
            ]);

            Route::get('instructor/students/performance/{id}', [
                'as' => 'instructor.students.performance.show',
                'uses' => 'PerformanceController@show'
            ]);
            // END Instructor Routes

            // Course Package
            Route::resource('course-packages', 'CoursePackageController');

            Route::post('course-packages/{id}/select/{course_id}', [
                'as' => 'course-packages.select-course',
                'uses' => 'CoursePackageController@select'
            ]);

            Route::post('course-packages/{id}/unselect/{course_id}', [
                'as' => 'course-packages.unselect-course',
                'uses' => 'CoursePackageController@unselect'
            ]);

            // END Course Package

            // Essay Routes
            Route::resource('essays', 'EssaySocialLearningController');

            Route::post('essays/{id}/answers', [
                'as' => 'essays.answers.store',
                'uses' => 'EssaySocialLearningController@postAnswer'
            ]);

            Route::post('answers/{id}/likes', [
                'as' => 'answers.like',
                'uses' => 'EssaySocialLearningController@like'
            ]);

            Route::post('answers/{id}/dislikes', [
                'as' => 'answers.dislike',
                'uses' => 'EssaySocialLearningController@dislike'
            ]);

            Route::post('answers/{id}/comments', [
                'as' => 'answers.comments',
                'uses' => 'EssaySocialLearningController@comment'
            ]);
            // END Essay Routes

        });

        // Voucher
        Route::resource('vouchers', 'VoucherController');

        Route::post('vouchers/check', [
            'as' => 'vouchers.check',
            'uses' => 'VoucherController@check'
        ]);
        // End Voucher

    }));

    Route::get('/', 'HomeController@portal')->name('portal');

    Route::get('/how-it-works', 'HomeController@howItWorks')->name('howItWorks');

    Route::post('/users/add-role', 'UserController@addRole');

    Route::post('/users/delete-role', 'UserController@deleteRole');

    Route::get('404', [
        'as' => 'notfound',
        'uses' => 'HomeController@notfound'
    ]);


});
