<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'api\LoginController@postLogin');

Route::post('register', 'api\LoginController@postRegister');


// API Group Routes
Route::group(array('prefix' => 'v1', 'middleware' => ['auth:api']), function () {

    Route::post('logout', 'api\LoginController@logout');

    Route::post('material/store', 'api\MaterialController@postMaterial');

    /* Post */
    Route::post('/posts/{id}/likes', [
        'as' => 'like',
        'uses' => 'api\PostSocialLearningController@like'
    ]);

    Route::post('/posts/{id}/dislikes', [
        'as' => 'dislike',
        'uses' => 'api\PostSocialLearningController@dislike'
    ]);

    Route::post('/posts/{id}/comments', [
        'as' => 'comments',
        'uses' => 'api\PostSocialLearningController@postComment'
    ]);

    Route::post('/comments/{id}/likes', [
        'as' => 'comments.like',
        'uses' => 'api\PostSocialLearningController@likeComment'
    ]);

    Route::post('/comments/{id}/dislikes', [
        'as' => 'comments.dislike',
        'uses' => 'api\PostSocialLearningController@dislikeComment'
    ]);

    /* End Post */


    /* Course */
    Route::post('/courses/{id}/join', 'api\CourseController@join');

    Route::post('/courses/{id}/leave', 'api\CourseController@leave');

    /* Event Routes */
    Route::post('/social-learning-events/{id}/follow', [
        'as' => 'events.follow',
        'uses' => 'api\EventSocialLearningController@followEvent'
    ]);

    Route::post('/social-learning-events/{id}/unfollow', [
        'as' => 'events.unfollow',
        'uses' => 'api\EventSocialLearningController@unFollowEvent'
    ]);
    /* End Event */


    // Follow user
    Route::post('/users/{id}/follow', [
        'as' => 'users.follow',
        'uses' => 'api\UserController@follow'
    ]);

    Route::post('/users/{id}/unfollow', [
        'as' => 'users.unfollow',
        'uses' => 'api\UserController@unfollow'
    ]);

    //   Me Route

    Route::resource('users', 'api\UserController');

    Route::get('/users/{name}/search', [
        'as' => 'users.search',
        'uses' => 'api\UserController@getUsersByName'
    ]);

    Route::resource('social-learning-posts', 'api\PostSocialLearningController');

    Route::post('social-learning-posts/{id}/likes', [
        'as' => 'posts.like',
        'uses' => 'api\PostSocialLearningController@like'
    ]);

    Route::post('social-learning-posts/{id}/dislikes', [
        'as' => 'posts.dislike',
        'uses' => 'api\PostSocialLearningController@dislike'
    ]);

    Route::post('social-learning-posts/{id}/comments', [
        'as' => 'posts.comments',
        'uses' => 'api\PostSocialLearningController@postComment'
    ]);

    Route::post('social-learning-posts/{post_id}/comments/{id}/likes', [
        'as' => 'posts.comments.like',
        'uses' => 'api\PostSocialLearningController@likeComment'
    ]);

    Route::post('social-learning-posts/{post_id}/comments/{id}/dislikes', [
        'as' => 'posts.comments.dislike',
        'uses' => 'api\PostSocialLearningController@dislikeComment'
    ]);

    // Route::get('/social-learning-posts', [
    //     'as' => 'social-learning-posts.index',
    //     'uses' => 'api\PostSocialLearningController@index'
    // ]);

    // Route::post('/social-learning-posts', [
    //     'as' => 'social-learning-posts.store',
    //     'uses' => 'api\PostSocialLearningController@store'
    // ]);

    Route::get('/social-learning-posts/user/{id}', [
        'as' => 'social-learning-posts.getByUser',
        'uses' => 'api\PostSocialLearningController@getByUser'
    ]);

    Route::get('/social-learning-posts/{id}/image', [
        'as' => 'social-learning-posts.getImage',
        'uses' => 'api\PostSocialLearningController@getImage'
    ]);

    Route::resource('social-learning-events', 'api\EventSocialLearningController');


    Route::resource('social-learning-profile', 'api\ProfileSocialLearningController');

    Route::resource('social-learning-setting', 'api\SettingSocialLearningController');

    Route::resource('social-learning-exchange-point', 'api\ExchangePointSocialLearningController');

    Route::resource('social-learning-course', 'api\CourseSocialLearningController');
    Route::resource('social-learning-exam', 'api\ExamSocialLearningController');
    Route::resource('social-learning-order-history', 'api\OrderHistorySocialLearningController');
    
    Route::resource('social-learning-my-course', 'api\MyCourseSocialLearningController');
    Route::resource('social-learning-e-certificate', 'api\ECertificateSocialLearningController');

    Route::resource('social-learning-my-performance', 'api\MyPerformanceSocialLearningController');
    Route::resource('social-learning-booking', 'api\BookingSocialLearningController');
    Route::resource('social-learning-my-badge', 'api\MyBadgeSocialLearningController');
    Route::resource('social-learning-course-detail', 'api\ListDetailSocialLearningController');

    // Question
    Route::resource('social-learning-questions', 'api\QuestionSocialLearningController');

    Route::post('social-learning-questions/{id}/likes', [
        'as' => 'questions.like',
        'uses' => 'api\QuestionSocialLearningController@like'
    ]);

    Route::post('social-learning-questions/{id}/dislikes', [
        'as' => 'questions.dislike',
        'uses' => 'api\QuestionSocialLearningController@dislike'
    ]);

    Route::post('social-learning-questions/{id}/comments', [
        'as' => 'questions.comments',
        'uses' => 'api\QuestionSocialLearningController@comment'
    ]);

    Route::post('social-learning-questions/{question_id}/comments/{id}/likes', [
        'as' => 'questions.comments.like',
        'uses' => 'api\QuestionSocialLearningController@likeComment'
    ]);

    Route::post('social-learning-questions/{question_id}/comments/{id}/dislikes', [
        'as' => 'questions.comments.dislike',
        'uses' => 'api\QuestionSocialLearningController@dislikeComment'
    ]);

    // Master Piece
    Route::resource('social-learning-masterpieces', 'api\MasterpieceSocialLearningController');

    Route::post('/social-learning-masterpieces/{id}/likes', [
        'as' => 'masterpieces.like',
        'uses' => 'api\MasterpieceSocialLearningController@like'
    ]);

    Route::post('/social-learning-masterpieces/{id}/dislikes', [
        'as' => 'masterpieces.dislike',
        'uses' => 'api\MasterpieceSocialLearningController@dislike'
    ]);

    Route::post('/social-learning-masterpieces/{id}/comments', [
        'as' => 'masterpieces.comments',
        'uses' => 'api\MasterpieceSocialLearningController@postComment'
    ]);

    Route::post('/social-learning-masterpieces/{master_piece_id}/comments/{id}/likes', [
        'as' => 'masterpieces.comments.like',
        'uses' => 'api\MasterpieceSocialLearningController@likeComment'
    ]);

    Route::post('/social-learning-masterpieces/{master_piece_id}/comments/{id}/dislikes', [
        'as' => 'masterpieces.comments.dislike',
        'uses' => 'api\MasterpieceSocialLearningController@dislikeComment'
    ]);

    // My Profile
    Route::get('my-profile/{id}', [
        'as' => 'my-profile',
        'uses' => 'api\MyProfileController@index'
    ]);

    Route::get('my-profile/{id}/i-friend', [
        'as' => 'my-profile.ifriend',
        'uses' => 'api\MyProfileController@iFriend'
    ]);

    Route::get('my-profile/{id}/my-courses', [
        'as' => 'my-profile.my-courses',
        'uses' => 'api\MyProfileController@myCourses'
    ]);

    // points and gems
    Route::get('my-profile/{id}/points', [
        'as' => 'my-profile.points',
        'uses' => 'api\MyProfileController@points'
    ]);

    Route::get('my-profile/{id}/badges', [
        'as' => 'my-profile.points',
        'uses' => 'api\MyProfileController@badges'
    ]);

    Route::put('my-profile/{id}/update-profile', [
        'as' => 'my-profile.update-profile',
        'uses' => 'api\MyProfileController@updateProfile'
    ]);

    Route::put('my-profile/{id}/change-photo', [
        'as' => 'my-profile.change-photo',
        'uses' => 'api\MyProfileController@changePhoto'
    ]);

    Route::get('my-profile/{id}/exchange-point-list', [
        'as' => 'my-profile.exchange-point-list',
        'uses' => 'api\MyProfileController@exchangePointList'
    ]);

    Route::post('my-profile/{user_id}/exchange-point/{id}', [
        'as' => 'my-profile.exchange-point',
        'uses' => 'api\MyProfileController@sampleExchange'
    ]);

    Route::put('my-profile/{user_id}/change-password', [
        'as' => 'my-profile.change-password',
        'uses' => 'api\SettingSocialLearningController@changePassword'
    ]);

    Route::get('my-profile/{id}/user-suggested', [
        'as' => 'my-profile.user-suggested',
        'uses' => 'api\MyProfileController@userSuggested'
    ]);

    // Add Cart

    Route::post('social-learning-my-course/{id}/add-to-cart', [
        'as' => 'social-learning-my-course.addtocart',
        'uses' => 'api\MyCourseSocialLearningController@addToCart'
    ]);


    Route::post('courses/{id}/section', [
        'as' => 'courses.add.section',
        'uses' => 'api\SectionController@store'
    ]);

    Route::resource('courses', 'CourseController');

    Route::get('users/role/{id}', [
        'as' => 'users.role',
        'uses' => 'api\UserController@usersByRole'
    ]);

    Route::put('user/update-role/{id}', [
        'as' => 'changeRole',
        'uses' => 'api\UserController@changeRole'
    ]);

    Route::post('users/select-superadmin', 'UserController@selectExpert')->name('users.selectExpert');

    Route::delete('sections/delete-all/{id}', [
        'as' => 'sections.delete.all',
        'uses' => 'api\SectionController@deleteAll'
    ]);

    Route::post('material/store', 'api\MaterialController@postMaterial');

    Route::delete('material/delete/{id}', 'api\MaterialController@deleteMaterial');

    Route::resource('quizzes', 'api\QuizController');

    Route::get('social-learning-badges', 'api\MyBadgeSocialLearningController@index');

});
// End Middleware auth
