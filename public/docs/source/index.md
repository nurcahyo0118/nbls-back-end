---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->


















# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)
<!-- END_INFO -->

#Booking Social Learning

Longer description
<!-- START_a98d88edaea5723864860b8298538e01 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-booking" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-booking",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-booking`

`HEAD api/v1/social-learning-booking`


<!-- END_a98d88edaea5723864860b8298538e01 -->

<!-- START_c5c735ea1a81ca461fd4ea20f4f620f5 -->
## Show the form for creating a new resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-booking/create" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-booking/create",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-booking/create`

`HEAD api/v1/social-learning-booking/create`


<!-- END_c5c735ea1a81ca461fd4ea20f4f620f5 -->

<!-- START_05544de7ebebebe8c7b2f93eed1ff68c -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/social-learning-booking" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-booking",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/social-learning-booking`


<!-- END_05544de7ebebebe8c7b2f93eed1ff68c -->

<!-- START_cbbc67e0603faa64a41445d742da67ca -->
## Display the specified resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-booking/{social_learning_booking}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-booking/{social_learning_booking}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-booking/{social_learning_booking}`

`HEAD api/v1/social-learning-booking/{social_learning_booking}`


<!-- END_cbbc67e0603faa64a41445d742da67ca -->

<!-- START_7b89dfb2817231d918ece6fa7ea3682f -->
## Show the form for editing the specified resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-booking/{social_learning_booking}/edit" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-booking/{social_learning_booking}/edit",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-booking/{social_learning_booking}/edit`

`HEAD api/v1/social-learning-booking/{social_learning_booking}/edit`


<!-- END_7b89dfb2817231d918ece6fa7ea3682f -->

<!-- START_0666cd11dee76087e646a47e1460a6e7 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT "http://babastudio.test/api/v1/social-learning-booking/{social_learning_booking}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-booking/{social_learning_booking}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/social-learning-booking/{social_learning_booking}`

`PATCH api/v1/social-learning-booking/{social_learning_booking}`


<!-- END_0666cd11dee76087e646a47e1460a6e7 -->

<!-- START_2f427ddba9598d9246a371be9024c234 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE "http://babastudio.test/api/v1/social-learning-booking/{social_learning_booking}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-booking/{social_learning_booking}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/v1/social-learning-booking/{social_learning_booking}`


<!-- END_2f427ddba9598d9246a371be9024c234 -->

#E-certificate Social Learning

Longer description
<!-- START_23bef33aa6ff3a39eed0323c184f8967 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-e-certificate" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-e-certificate",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-e-certificate`

`HEAD api/v1/social-learning-e-certificate`


<!-- END_23bef33aa6ff3a39eed0323c184f8967 -->

<!-- START_acd87a768ae7f9afdab401488b60c91f -->
## Show the form for creating a new resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-e-certificate/create" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-e-certificate/create",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-e-certificate/create`

`HEAD api/v1/social-learning-e-certificate/create`


<!-- END_acd87a768ae7f9afdab401488b60c91f -->

<!-- START_ef566e22538d1feaae5a70b8adbc9af1 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/social-learning-e-certificate" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-e-certificate",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/social-learning-e-certificate`


<!-- END_ef566e22538d1feaae5a70b8adbc9af1 -->

<!-- START_c4a54ce47c240c1e03e3b49dc94c963c -->
## Display the specified resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-e-certificate/{social_learning_e_certificate}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-e-certificate/{social_learning_e_certificate}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-e-certificate/{social_learning_e_certificate}`

`HEAD api/v1/social-learning-e-certificate/{social_learning_e_certificate}`


<!-- END_c4a54ce47c240c1e03e3b49dc94c963c -->

<!-- START_94c6a8b63e3dcca0fba9c44587493988 -->
## Show the form for editing the specified resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-e-certificate/{social_learning_e_certificate}/edit" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-e-certificate/{social_learning_e_certificate}/edit",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-e-certificate/{social_learning_e_certificate}/edit`

`HEAD api/v1/social-learning-e-certificate/{social_learning_e_certificate}/edit`


<!-- END_94c6a8b63e3dcca0fba9c44587493988 -->

<!-- START_cf798273313f28732cb9aff734efb819 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT "http://babastudio.test/api/v1/social-learning-e-certificate/{social_learning_e_certificate}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-e-certificate/{social_learning_e_certificate}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/social-learning-e-certificate/{social_learning_e_certificate}`

`PATCH api/v1/social-learning-e-certificate/{social_learning_e_certificate}`


<!-- END_cf798273313f28732cb9aff734efb819 -->

<!-- START_116db8ce8a368ee39e5b0a769e7f6d7c -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE "http://babastudio.test/api/v1/social-learning-e-certificate/{social_learning_e_certificate}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-e-certificate/{social_learning_e_certificate}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/v1/social-learning-e-certificate/{social_learning_e_certificate}`


<!-- END_116db8ce8a368ee39e5b0a769e7f6d7c -->

#Event Social Learning

Longer description
<!-- START_c2e96afbed4544582d928a916c452228 -->
## api/v1/social-learning-events/{id}/follow

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/social-learning-events/{id}/follow" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-events/{id}/follow",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/social-learning-events/{id}/follow`


<!-- END_c2e96afbed4544582d928a916c452228 -->

<!-- START_766b5a4f1e112b9a845a71fdacf883ba -->
## api/v1/social-learning-events/{id}/unfollow

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/social-learning-events/{id}/unfollow" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-events/{id}/unfollow",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/social-learning-events/{id}/unfollow`


<!-- END_766b5a4f1e112b9a845a71fdacf883ba -->

<!-- START_65e01e8e363f524ff00e341761abcab1 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-events" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-events",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-events`

`HEAD api/v1/social-learning-events`


<!-- END_65e01e8e363f524ff00e341761abcab1 -->

<!-- START_f9b003539c741d6c214ad140679a698e -->
## Show the form for creating a new resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-events/create" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-events/create",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-events/create`

`HEAD api/v1/social-learning-events/create`


<!-- END_f9b003539c741d6c214ad140679a698e -->

<!-- START_d9f669b4037ac7437bc8a4af72c8c05b -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/social-learning-events" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-events",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/social-learning-events`


<!-- END_d9f669b4037ac7437bc8a4af72c8c05b -->

<!-- START_b80429db501703207bd1dcd1233a550c -->
## Display the specified resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-events/{social_learning_event}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-events/{social_learning_event}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-events/{social_learning_event}`

`HEAD api/v1/social-learning-events/{social_learning_event}`


<!-- END_b80429db501703207bd1dcd1233a550c -->

<!-- START_3ee1ca1153ddaeb5c8961a0496fd1f5b -->
## Show the form for editing the specified resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-events/{social_learning_event}/edit" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-events/{social_learning_event}/edit",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-events/{social_learning_event}/edit`

`HEAD api/v1/social-learning-events/{social_learning_event}/edit`


<!-- END_3ee1ca1153ddaeb5c8961a0496fd1f5b -->

<!-- START_cabd47a59cd0b1a8550b4f4762083340 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT "http://babastudio.test/api/v1/social-learning-events/{social_learning_event}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-events/{social_learning_event}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/social-learning-events/{social_learning_event}`

`PATCH api/v1/social-learning-events/{social_learning_event}`


<!-- END_cabd47a59cd0b1a8550b4f4762083340 -->

<!-- START_cae7bab5c474cefecc6563ed206de2a3 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE "http://babastudio.test/api/v1/social-learning-events/{social_learning_event}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-events/{social_learning_event}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/v1/social-learning-events/{social_learning_event}`


<!-- END_cae7bab5c474cefecc6563ed206de2a3 -->

#Exam Social Learning

Longer description
<!-- START_f6f516cea8df3dce366073d073f84d73 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-exam" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-exam",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-exam`

`HEAD api/v1/social-learning-exam`


<!-- END_f6f516cea8df3dce366073d073f84d73 -->

<!-- START_7d12ff3751449ee213627a6fa33db85d -->
## Show the form for creating a new resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-exam/create" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-exam/create",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-exam/create`

`HEAD api/v1/social-learning-exam/create`


<!-- END_7d12ff3751449ee213627a6fa33db85d -->

<!-- START_7165b07540b39ac50ff76d22fc063f6b -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/social-learning-exam" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-exam",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/social-learning-exam`


<!-- END_7165b07540b39ac50ff76d22fc063f6b -->

<!-- START_8cd26e4997031391070908013cc2df7d -->
## Display the specified resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-exam/{social_learning_exam}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-exam/{social_learning_exam}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-exam/{social_learning_exam}`

`HEAD api/v1/social-learning-exam/{social_learning_exam}`


<!-- END_8cd26e4997031391070908013cc2df7d -->

<!-- START_1127ba3ba89597cfde8bf01a189fffe1 -->
## Show the form for editing the specified resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-exam/{social_learning_exam}/edit" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-exam/{social_learning_exam}/edit",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-exam/{social_learning_exam}/edit`

`HEAD api/v1/social-learning-exam/{social_learning_exam}/edit`


<!-- END_1127ba3ba89597cfde8bf01a189fffe1 -->

<!-- START_b55eb7fc1b16fcbb8bc59effa9049961 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT "http://babastudio.test/api/v1/social-learning-exam/{social_learning_exam}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-exam/{social_learning_exam}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/social-learning-exam/{social_learning_exam}`

`PATCH api/v1/social-learning-exam/{social_learning_exam}`


<!-- END_b55eb7fc1b16fcbb8bc59effa9049961 -->

<!-- START_4dbd1a8d1249e72dee4ccfc163a116f4 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE "http://babastudio.test/api/v1/social-learning-exam/{social_learning_exam}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-exam/{social_learning_exam}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/v1/social-learning-exam/{social_learning_exam}`


<!-- END_4dbd1a8d1249e72dee4ccfc163a116f4 -->

#Exchange Point Social Learning

Longer description
<!-- START_9ff0d6c5ccc253f890ec7c8053ad689e -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-exchange-point" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-exchange-point",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-exchange-point`

`HEAD api/v1/social-learning-exchange-point`


<!-- END_9ff0d6c5ccc253f890ec7c8053ad689e -->

<!-- START_cad3a0736e2c78f4a4edca04511d5138 -->
## Show the form for creating a new resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-exchange-point/create" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-exchange-point/create",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-exchange-point/create`

`HEAD api/v1/social-learning-exchange-point/create`


<!-- END_cad3a0736e2c78f4a4edca04511d5138 -->

<!-- START_ab1ca6d412bec2301bd0f8c7f93caf0f -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/social-learning-exchange-point" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-exchange-point",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/social-learning-exchange-point`


<!-- END_ab1ca6d412bec2301bd0f8c7f93caf0f -->

<!-- START_c5a229b6e7a0955aed00c11dac2a1183 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-exchange-point/{social_learning_exchange_point}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-exchange-point/{social_learning_exchange_point}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-exchange-point/{social_learning_exchange_point}`

`HEAD api/v1/social-learning-exchange-point/{social_learning_exchange_point}`


<!-- END_c5a229b6e7a0955aed00c11dac2a1183 -->

<!-- START_d0660b1b2f3c6bf3dc22ae8c5bfd9308 -->
## Show the form for editing the specified resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-exchange-point/{social_learning_exchange_point}/edit" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-exchange-point/{social_learning_exchange_point}/edit",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-exchange-point/{social_learning_exchange_point}/edit`

`HEAD api/v1/social-learning-exchange-point/{social_learning_exchange_point}/edit`


<!-- END_d0660b1b2f3c6bf3dc22ae8c5bfd9308 -->

<!-- START_bd93f7c751deb398f9414b16fd60672f -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT "http://babastudio.test/api/v1/social-learning-exchange-point/{social_learning_exchange_point}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-exchange-point/{social_learning_exchange_point}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/social-learning-exchange-point/{social_learning_exchange_point}`

`PATCH api/v1/social-learning-exchange-point/{social_learning_exchange_point}`


<!-- END_bd93f7c751deb398f9414b16fd60672f -->

<!-- START_b502860c841416d2fa399bc7dd7cf783 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE "http://babastudio.test/api/v1/social-learning-exchange-point/{social_learning_exchange_point}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-exchange-point/{social_learning_exchange_point}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/v1/social-learning-exchange-point/{social_learning_exchange_point}`


<!-- END_b502860c841416d2fa399bc7dd7cf783 -->

#List Course

Longer description
<!-- START_5197ec007e63bd5f0b46874010b49a0d -->
## api/v1/courses/{id}/join

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/courses/{id}/join" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/courses/{id}/join",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/courses/{id}/join`


<!-- END_5197ec007e63bd5f0b46874010b49a0d -->

<!-- START_003a9bbfb49284c9e036db725ef9a345 -->
## api/v1/courses/{id}/leave

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/courses/{id}/leave" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/courses/{id}/leave",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/courses/{id}/leave`


<!-- END_003a9bbfb49284c9e036db725ef9a345 -->

#List Detail Social Learning

Longer description
<!-- START_d2f7d39d13d07024698ba582af246e2c -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-course-detail" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-course-detail",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-course-detail`

`HEAD api/v1/social-learning-course-detail`


<!-- END_d2f7d39d13d07024698ba582af246e2c -->

<!-- START_431176e9baf0ccd4fbeb82dddc5eae6e -->
## Show the form for creating a new resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-course-detail/create" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-course-detail/create",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-course-detail/create`

`HEAD api/v1/social-learning-course-detail/create`


<!-- END_431176e9baf0ccd4fbeb82dddc5eae6e -->

<!-- START_b9dc357668e53033621577818d61c2ee -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/social-learning-course-detail" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-course-detail",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/social-learning-course-detail`


<!-- END_b9dc357668e53033621577818d61c2ee -->

<!-- START_08926b9f87101eaab592c9e6858991cd -->
## Display the specified resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-course-detail/{social_learning_course_detail}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-course-detail/{social_learning_course_detail}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-course-detail/{social_learning_course_detail}`

`HEAD api/v1/social-learning-course-detail/{social_learning_course_detail}`


<!-- END_08926b9f87101eaab592c9e6858991cd -->

<!-- START_6692aa184832bef4ea04db6cb12280a7 -->
## Show the form for editing the specified resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-course-detail/{social_learning_course_detail}/edit" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-course-detail/{social_learning_course_detail}/edit",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-course-detail/{social_learning_course_detail}/edit`

`HEAD api/v1/social-learning-course-detail/{social_learning_course_detail}/edit`


<!-- END_6692aa184832bef4ea04db6cb12280a7 -->

<!-- START_748436362af40dd45454b2a762daf6d6 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT "http://babastudio.test/api/v1/social-learning-course-detail/{social_learning_course_detail}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-course-detail/{social_learning_course_detail}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/social-learning-course-detail/{social_learning_course_detail}`

`PATCH api/v1/social-learning-course-detail/{social_learning_course_detail}`


<!-- END_748436362af40dd45454b2a762daf6d6 -->

<!-- START_6eafd6073c986f8ab0315e4e67ff6022 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE "http://babastudio.test/api/v1/social-learning-course-detail/{social_learning_course_detail}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-course-detail/{social_learning_course_detail}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/v1/social-learning-course-detail/{social_learning_course_detail}`


<!-- END_6eafd6073c986f8ab0315e4e67ff6022 -->

#Login &amp; Register

Longer description
<!-- START_c3fa189a6c95ca36ad6ac4791a873d23 -->
## /api/login

> Example request:

```bash
curl -X POST "http://babastudio.test/api/login" \
-H "Accept: application/json" \
    -d "username"="rerum" \
    -d "password"="rerum" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/login",
    "method": "POST",
    "data": {
        "username": "rerum",
        "password": "rerum"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/login`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    username | string |  required  | 
    password | string |  required  | 

<!-- END_c3fa189a6c95ca36ad6ac4791a873d23 -->

<!-- START_d7b7952e7fdddc07c978c9bdaf757acf -->
## api/register

> Example request:

```bash
curl -X POST "http://babastudio.test/api/register" \
-H "Accept: application/json" \
    -d "fullname"="et" \
    -d "ktp_id"="et" \
    -d "username"="et" \
    -d "password"="et" \
    -d "email"="milo77@example.org" \
    -d "phone"="et" \
    -d "mobile"="et" \
    -d "mobile2"="et" \
    -d "pin_bb"="et" \
    -d "zipcode"="et" \
    -d "fb"="et" \
    -d "tw"="et" \
    -d "ig"="et" \
    -d "website"="et" \
    -d "status"="et" \
    -d "hobby"="et" \
    -d "about"="et" \
    -d "skill"="et" \
    -d "hope"="et" \
    -d "reason"="et" \
    -d "birth_date"="2000-05-05" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/register",
    "method": "POST",
    "data": {
        "fullname": "et",
        "ktp_id": "et",
        "username": "et",
        "password": "et",
        "email": "milo77@example.org",
        "phone": "et",
        "mobile": "et",
        "mobile2": "et",
        "pin_bb": "et",
        "zipcode": "et",
        "fb": "et",
        "tw": "et",
        "ig": "et",
        "website": "et",
        "status": "et",
        "hobby": "et",
        "about": "et",
        "skill": "et",
        "hope": "et",
        "reason": "et",
        "birth_date": "2000-05-05"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/register`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    fullname | string |  required  | Maximum: `255`
    ktp_id | string |  optional  | 
    username | string |  required  | 
    password | string |  required  | 
    email | email |  required  | 
    phone | string |  optional  | 
    mobile | string |  optional  | 
    mobile2 | string |  optional  | 
    pin_bb | string |  optional  | 
    zipcode | string |  optional  | 
    fb | string |  optional  | 
    tw | string |  optional  | 
    ig | string |  optional  | 
    website | string |  optional  | 
    status | string |  optional  | 
    hobby | string |  optional  | 
    about | string |  optional  | 
    skill | string |  optional  | 
    hope | string |  optional  | 
    reason | string |  optional  | 
    birth_date | date |  optional  | 

<!-- END_d7b7952e7fdddc07c978c9bdaf757acf -->

<!-- START_fb2ae43e2e99ff4e90f22ba03801a735 -->
## Logout

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/logout" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/logout",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/logout`


<!-- END_fb2ae43e2e99ff4e90f22ba03801a735 -->

#Master Piece

This module requires full authentication
 Headers : 
   - Authorization : &quot;Bearer ACCESS_TOKEN&quot;
<!-- START_c754b16202727893ed388e0065a7e791 -->
## Get All

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/masterpieces" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/masterpieces",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/masterpieces`

`HEAD api/v1/masterpieces`


<!-- END_c754b16202727893ed388e0065a7e791 -->

<!-- START_e733e76ca7457aea4d35ff4c8829d569 -->
## Show the form for creating a new resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/masterpieces/create" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/masterpieces/create",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/masterpieces/create`

`HEAD api/v1/masterpieces/create`


<!-- END_e733e76ca7457aea4d35ff4c8829d569 -->

<!-- START_92c00266596b05a1bec61a1ed9b619ac -->
## Insert.

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/masterpieces" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/masterpieces",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/masterpieces`


<!-- END_92c00266596b05a1bec61a1ed9b619ac -->

<!-- START_0e8ff6be76e917c43989e0f557c61226 -->
## Get One

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/masterpieces/{masterpiece}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/masterpieces/{masterpiece}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/masterpieces/{masterpiece}`

`HEAD api/v1/masterpieces/{masterpiece}`


<!-- END_0e8ff6be76e917c43989e0f557c61226 -->

<!-- START_dd0951773659b79064252c4973da417d -->
## Show the form for editing the specified resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/masterpieces/{masterpiece}/edit" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/masterpieces/{masterpiece}/edit",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/masterpieces/{masterpiece}/edit`

`HEAD api/v1/masterpieces/{masterpiece}/edit`


<!-- END_dd0951773659b79064252c4973da417d -->

<!-- START_f649cde0382ed12140b2c1203ac1a587 -->
## Update

> Example request:

```bash
curl -X PUT "http://babastudio.test/api/v1/masterpieces/{masterpiece}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/masterpieces/{masterpiece}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/masterpieces/{masterpiece}`

`PATCH api/v1/masterpieces/{masterpiece}`


<!-- END_f649cde0382ed12140b2c1203ac1a587 -->

<!-- START_9eed3b616c9607776c1a83aa20f636f6 -->
## Delete

> Example request:

```bash
curl -X DELETE "http://babastudio.test/api/v1/masterpieces/{masterpiece}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/masterpieces/{masterpiece}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/v1/masterpieces/{masterpiece}`


<!-- END_9eed3b616c9607776c1a83aa20f636f6 -->

<!-- START_d816da455d90bea5de4866cb3ef31054 -->
## Like

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/masterpieces/{id}/likes" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/masterpieces/{id}/likes",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/masterpieces/{id}/likes`


<!-- END_d816da455d90bea5de4866cb3ef31054 -->

<!-- START_af38728bf52c9a4a71319afa7bb5568b -->
## Dislike

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/masterpieces/{id}/dislikes" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/masterpieces/{id}/dislikes",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/masterpieces/{id}/dislikes`


<!-- END_af38728bf52c9a4a71319afa7bb5568b -->

<!-- START_34a57ff7f504e1e3e4131eb05c184615 -->
## Comment.

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/masterpieces/{id}/comments" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/masterpieces/{id}/comments",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/masterpieces/{id}/comments`


<!-- END_34a57ff7f504e1e3e4131eb05c184615 -->

<!-- START_0215998003b518977e8dd290632f1b23 -->
## Like Comment

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/masterpieces/comments/{id}/likes" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/masterpieces/comments/{id}/likes",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/masterpieces/comments/{id}/likes`


<!-- END_0215998003b518977e8dd290632f1b23 -->

<!-- START_415dbe763fbfeaa556b9953d3fc0c713 -->
## Dislike Comment

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/masterpieces/comments/{id}/dislikes" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/masterpieces/comments/{id}/dislikes",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/masterpieces/comments/{id}/dislikes`


<!-- END_415dbe763fbfeaa556b9953d3fc0c713 -->

#My Performance Social Learning

Longer description
<!-- START_aae053bfdef45c76267080eb91b63981 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-my-performance" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-my-performance",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-my-performance`

`HEAD api/v1/social-learning-my-performance`


<!-- END_aae053bfdef45c76267080eb91b63981 -->

<!-- START_9c293ae07a4b0e06e92ccb6edb286423 -->
## Show the form for creating a new resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-my-performance/create" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-my-performance/create",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-my-performance/create`

`HEAD api/v1/social-learning-my-performance/create`


<!-- END_9c293ae07a4b0e06e92ccb6edb286423 -->

<!-- START_9317360b1977d7285075a1fd18f47685 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/social-learning-my-performance" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-my-performance",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/social-learning-my-performance`


<!-- END_9317360b1977d7285075a1fd18f47685 -->

<!-- START_cb662d1a2fe5363172753d940e9d3bef -->
## Display the specified resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-my-performance/{social_learning_my_performance}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-my-performance/{social_learning_my_performance}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-my-performance/{social_learning_my_performance}`

`HEAD api/v1/social-learning-my-performance/{social_learning_my_performance}`


<!-- END_cb662d1a2fe5363172753d940e9d3bef -->

<!-- START_41c424ccb61d5c0a0ebc03bcb3baabb2 -->
## Show the form for editing the specified resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-my-performance/{social_learning_my_performance}/edit" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-my-performance/{social_learning_my_performance}/edit",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-my-performance/{social_learning_my_performance}/edit`

`HEAD api/v1/social-learning-my-performance/{social_learning_my_performance}/edit`


<!-- END_41c424ccb61d5c0a0ebc03bcb3baabb2 -->

<!-- START_142aff5fe06a20514341c7700583ff11 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT "http://babastudio.test/api/v1/social-learning-my-performance/{social_learning_my_performance}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-my-performance/{social_learning_my_performance}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/social-learning-my-performance/{social_learning_my_performance}`

`PATCH api/v1/social-learning-my-performance/{social_learning_my_performance}`


<!-- END_142aff5fe06a20514341c7700583ff11 -->

<!-- START_d1f6da27497086b2aefb2af332882182 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE "http://babastudio.test/api/v1/social-learning-my-performance/{social_learning_my_performance}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-my-performance/{social_learning_my_performance}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/v1/social-learning-my-performance/{social_learning_my_performance}`


<!-- END_d1f6da27497086b2aefb2af332882182 -->

#My Profile

This module requires full authentication
 Headers : 
   - Authorization : &quot;Bearer ACCESS_TOKEN&quot;
<!-- START_4f404191119f649b2199a55a9c85eb1b -->
## My Profile

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/my-profile" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/my-profile",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/my-profile`

`HEAD api/v1/my-profile`


<!-- END_4f404191119f649b2199a55a9c85eb1b -->

<!-- START_1940ec484f07c3e51056f93fc4d13e7d -->
## I - Friend

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/my-profile/i-friend" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/my-profile/i-friend",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/my-profile/i-friend`

`HEAD api/v1/my-profile/i-friend`


<!-- END_1940ec484f07c3e51056f93fc4d13e7d -->

<!-- START_6b882369a04f917c62aa6cd0ead4e041 -->
## My Courses

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/my-profile/my-courses" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/my-profile/my-courses",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
"[\n{\n  \"id\": 2,\n  \"title\": \"Intro\",\n  \"description\": \"nnn\",\n  \"duration\": 0,\n  \"status\": \"POSTED\",\n  \"price\": 0,\n  \"suitable\": null,\n  \"requirement\": null,\n  \"can_be\": null,\n  \"image\": null,\n  \"video\": null,\n  \"wistia_hashed_id\": \"ubxlnyti20\",\n  \"category_id\": 1,\n  \"sub_category_id\": 0,\n  \"tenant_id\": 1,\n  \"author_id\": 1,\n  \"is_enable_review\": 1,\n  \"created_at\": \"2018-04-13 11:55:21\",\n  \"updated_at\": \"2018-04-13 11:55:21\",\n  \"pivot\": {\n      \"user_id\": 1,\n      \"course_id\": 2\n  }\n }\n]"
```

### HTTP Request
`GET api/v1/my-profile/my-courses`

`HEAD api/v1/my-profile/my-courses`


<!-- END_6b882369a04f917c62aa6cd0ead4e041 -->

<!-- START_c59fe4ed86324bcaa6a9931589a815dd -->
## Update Profile

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/my-profile/update-profile" \
-H "Accept: application/json" \
    -d "id"="99203313" \
    -d "ktp_id"="voluptas" \
    -d "password"="voluptas" \
    -d "birth_date"="2010-08-16" \
    -d "phone"="voluptas" \
    -d "mobile"="voluptas" \
    -d "mobile2"="voluptas" \
    -d "pin_bb"="voluptas" \
    -d "notes"="voluptas" \
    -d "photo"="voluptas" \
    -d "fullname"="voluptas" \
    -d "address"="voluptas" \
    -d "sex"="P" \
    -d "zipcode"="voluptas" \
    -d "fb"="voluptas" \
    -d "tw"="voluptas" \
    -d "ig"="voluptas" \
    -d "website"="voluptas" \
    -d "status"="voluptas" \
    -d "hobby"="voluptas" \
    -d "about"="voluptas" \
    -d "skill"="voluptas" \
    -d "hope"="voluptas" \
    -d "reason"="voluptas" \
    -d "is_patnership"="1" \
    -d "role_id"="99203313" \
    -d "point_id"="99203313" \
    -d "branch_id"="99203313" \
    -d "tenant_id"="99203313" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/my-profile/update-profile",
    "method": "POST",
    "data": {
        "id": 99203313,
        "ktp_id": "voluptas",
        "password": "voluptas",
        "birth_date": "2010-08-16",
        "phone": "voluptas",
        "mobile": "voluptas",
        "mobile2": "voluptas",
        "pin_bb": "voluptas",
        "notes": "voluptas",
        "photo": "voluptas",
        "fullname": "voluptas",
        "address": "voluptas",
        "sex": "P",
        "zipcode": "voluptas",
        "fb": "voluptas",
        "tw": "voluptas",
        "ig": "voluptas",
        "website": "voluptas",
        "status": "voluptas",
        "hobby": "voluptas",
        "about": "voluptas",
        "skill": "voluptas",
        "hope": "voluptas",
        "reason": "voluptas",
        "is_patnership": true,
        "role_id": 99203313,
        "point_id": 99203313,
        "branch_id": 99203313,
        "tenant_id": 99203313
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
"{\n      \"id\": 1,\n      \"ktp_id\": null,\n      \"username\": \"nurcahyo0113234\",\n      \"email\": \"lawlawliet9882@gmail.com\",\n      \"birth_date\": null,\n      \"phone\": null,\n      \"mobile\": null,\n      \"mobile2\": null,\n      \"pin_bb\": null,\n      \"notes\": null,\n      \"photo\": \"1523624295.sudah-kuduga-1.jpg\",\n      \"fullname\": \"Nur Cahyo Putro\",\n      \"address\": null,\n      \"sex\": \"L\",\n      \"zipcode\": null,\n      \"fb\": null,\n      \"tw\": null,\n      \"ig\": null,\n      \"website\": null,\n      \"status\": null,\n      \"hobby\": null,\n      \"about\": null,\n      \"skill\": null,\n      \"hope\": null,\n      \"reason\": null,\n      \"is_patnership\": 1,\n      \"role_id\": 1,\n      \"point_id\": 0,\n      \"branch_id\": 0,\n      \"tenant_id\": 1,\n      \"created_at\": \"2018-04-13 11:42:46\",\n      \"updated_at\": \"2018-04-13 19:44:19\"\n  }"
```

### HTTP Request
`POST api/v1/my-profile/update-profile`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    id | numeric |  optional  | 
    ktp_id | string |  optional  | 
    password | string |  optional  | 
    birth_date | date |  optional  | 
    phone | string |  optional  | 
    mobile | string |  optional  | 
    mobile2 | string |  optional  | 
    pin_bb | string |  optional  | 
    notes | string |  optional  | 
    photo | string |  optional  | 
    fullname | string |  optional  | 
    address | string |  optional  | 
    sex | string |  optional  | `L` or `P`
    zipcode | string |  optional  | 
    fb | string |  optional  | 
    tw | string |  optional  | 
    ig | string |  optional  | 
    website | string |  optional  | 
    status | string |  optional  | 
    hobby | string |  optional  | 
    about | string |  optional  | 
    skill | string |  optional  | 
    hope | string |  optional  | 
    reason | string |  optional  | 
    is_patnership | boolean |  optional  | 
    role_id | numeric |  optional  | 
    point_id | numeric |  optional  | 
    branch_id | numeric |  optional  | 
    tenant_id | numeric |  optional  | 

<!-- END_c59fe4ed86324bcaa6a9931589a815dd -->

<!-- START_5f8b7a2a352cba95ff4e0d2dc103278a -->
## Change Photo Profile

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/my-profile/change-photo" \
-H "Accept: application/json" \
    -d "photo"="est" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/my-profile/change-photo",
    "method": "POST",
    "data": {
        "photo": "est"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
"{\n 'photo' : 'STRING_BASE64'\n}"
```

### HTTP Request
`POST api/v1/my-profile/change-photo`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    photo | string |  required  | 

<!-- END_5f8b7a2a352cba95ff4e0d2dc103278a -->

#My badge Social Learning

Longer description
<!-- START_5925cc8e5a2788d58bb474cfbcf1bed5 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-my-badge" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-my-badge",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-my-badge`

`HEAD api/v1/social-learning-my-badge`


<!-- END_5925cc8e5a2788d58bb474cfbcf1bed5 -->

<!-- START_2d0b0ce83cc7a60100408e1e53121798 -->
## Show the form for creating a new resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-my-badge/create" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-my-badge/create",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-my-badge/create`

`HEAD api/v1/social-learning-my-badge/create`


<!-- END_2d0b0ce83cc7a60100408e1e53121798 -->

<!-- START_624e7be91fa6e78e0b168370326f325e -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/social-learning-my-badge" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-my-badge",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/social-learning-my-badge`


<!-- END_624e7be91fa6e78e0b168370326f325e -->

<!-- START_84b06f6dc0737a308d7fa28a6308f834 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-my-badge/{social_learning_my_badge}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-my-badge/{social_learning_my_badge}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-my-badge/{social_learning_my_badge}`

`HEAD api/v1/social-learning-my-badge/{social_learning_my_badge}`


<!-- END_84b06f6dc0737a308d7fa28a6308f834 -->

<!-- START_01b35ef58c40713199765796f7748110 -->
## Show the form for editing the specified resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-my-badge/{social_learning_my_badge}/edit" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-my-badge/{social_learning_my_badge}/edit",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-my-badge/{social_learning_my_badge}/edit`

`HEAD api/v1/social-learning-my-badge/{social_learning_my_badge}/edit`


<!-- END_01b35ef58c40713199765796f7748110 -->

<!-- START_a4c8dd37ed65fa39779e4308513f70a9 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT "http://babastudio.test/api/v1/social-learning-my-badge/{social_learning_my_badge}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-my-badge/{social_learning_my_badge}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/social-learning-my-badge/{social_learning_my_badge}`

`PATCH api/v1/social-learning-my-badge/{social_learning_my_badge}`


<!-- END_a4c8dd37ed65fa39779e4308513f70a9 -->

<!-- START_20e33250a27f580067142891f25cc441 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE "http://babastudio.test/api/v1/social-learning-my-badge/{social_learning_my_badge}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-my-badge/{social_learning_my_badge}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/v1/social-learning-my-badge/{social_learning_my_badge}`


<!-- END_20e33250a27f580067142891f25cc441 -->

#My course Social Learning

Longer description
<!-- START_71d0dfe046cc4fc8151c4f86df4a6b5e -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-my-course" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-my-course",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-my-course`

`HEAD api/v1/social-learning-my-course`


<!-- END_71d0dfe046cc4fc8151c4f86df4a6b5e -->

<!-- START_fe215f59687de05d2fb5caa8c3714bb6 -->
## Show the form for creating a new resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-my-course/create" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-my-course/create",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-my-course/create`

`HEAD api/v1/social-learning-my-course/create`


<!-- END_fe215f59687de05d2fb5caa8c3714bb6 -->

<!-- START_53a555d0643ef6fec0922eac26a56474 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/social-learning-my-course" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-my-course",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/social-learning-my-course`


<!-- END_53a555d0643ef6fec0922eac26a56474 -->

<!-- START_b8532260c40bcef9263a1a3cbe5c570d -->
## Display the specified resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-my-course/{social_learning_my_course}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-my-course/{social_learning_my_course}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-my-course/{social_learning_my_course}`

`HEAD api/v1/social-learning-my-course/{social_learning_my_course}`


<!-- END_b8532260c40bcef9263a1a3cbe5c570d -->

<!-- START_d7a44cab735b2525ce0ec2eefe765fe0 -->
## Show the form for editing the specified resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-my-course/{social_learning_my_course}/edit" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-my-course/{social_learning_my_course}/edit",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-my-course/{social_learning_my_course}/edit`

`HEAD api/v1/social-learning-my-course/{social_learning_my_course}/edit`


<!-- END_d7a44cab735b2525ce0ec2eefe765fe0 -->

<!-- START_a287c5455991fe7f21d2529cd0c6bf3d -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT "http://babastudio.test/api/v1/social-learning-my-course/{social_learning_my_course}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-my-course/{social_learning_my_course}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/social-learning-my-course/{social_learning_my_course}`

`PATCH api/v1/social-learning-my-course/{social_learning_my_course}`


<!-- END_a287c5455991fe7f21d2529cd0c6bf3d -->

<!-- START_edf0e25aacad5c9b828f5e67e6f02341 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE "http://babastudio.test/api/v1/social-learning-my-course/{social_learning_my_course}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-my-course/{social_learning_my_course}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/v1/social-learning-my-course/{social_learning_my_course}`


<!-- END_edf0e25aacad5c9b828f5e67e6f02341 -->

<!-- START_fc2fcd88a8561224498dccf1f2011b60 -->
## api/v1/social-learning-my-course/{id}/add-to-cart

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/social-learning-my-course/{id}/add-to-cart" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-my-course/{id}/add-to-cart",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/social-learning-my-course/{id}/add-to-cart`


<!-- END_fc2fcd88a8561224498dccf1f2011b60 -->

#Order History Social Learning

Longer description
<!-- START_177998e2b0d64cbece3d3c6bc2da0bf9 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-order-history" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-order-history",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-order-history`

`HEAD api/v1/social-learning-order-history`


<!-- END_177998e2b0d64cbece3d3c6bc2da0bf9 -->

<!-- START_84d54511cea7d767fce45036a4fe4c49 -->
## Show the form for creating a new resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-order-history/create" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-order-history/create",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-order-history/create`

`HEAD api/v1/social-learning-order-history/create`


<!-- END_84d54511cea7d767fce45036a4fe4c49 -->

<!-- START_a46566d278c34036aeb38fac95bc6b9d -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/social-learning-order-history" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-order-history",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/social-learning-order-history`


<!-- END_a46566d278c34036aeb38fac95bc6b9d -->

<!-- START_eb6dd236fcbfedc6637c248202573367 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-order-history/{social_learning_order_history}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-order-history/{social_learning_order_history}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-order-history/{social_learning_order_history}`

`HEAD api/v1/social-learning-order-history/{social_learning_order_history}`


<!-- END_eb6dd236fcbfedc6637c248202573367 -->

<!-- START_f889ffb79918d0474cf73dfdf1d16980 -->
## Show the form for editing the specified resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-order-history/{social_learning_order_history}/edit" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-order-history/{social_learning_order_history}/edit",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-order-history/{social_learning_order_history}/edit`

`HEAD api/v1/social-learning-order-history/{social_learning_order_history}/edit`


<!-- END_f889ffb79918d0474cf73dfdf1d16980 -->

<!-- START_9b9d9ee2048e3e24c1a2616f0f735c40 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT "http://babastudio.test/api/v1/social-learning-order-history/{social_learning_order_history}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-order-history/{social_learning_order_history}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/social-learning-order-history/{social_learning_order_history}`

`PATCH api/v1/social-learning-order-history/{social_learning_order_history}`


<!-- END_9b9d9ee2048e3e24c1a2616f0f735c40 -->

<!-- START_40a70eba77f7c83602244d49774cfc81 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE "http://babastudio.test/api/v1/social-learning-order-history/{social_learning_order_history}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-order-history/{social_learning_order_history}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/v1/social-learning-order-history/{social_learning_order_history}`


<!-- END_40a70eba77f7c83602244d49774cfc81 -->

#Post Social Learning

This module requires full authentication
 Headers : 
   - Authorization : &quot;Bearer ACCESS_TOKEN&quot;
<!-- START_3a2624800023d9dde1245764c2c80724 -->
## api/v1/social-learning-posts

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-posts" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-posts",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-posts`

`HEAD api/v1/social-learning-posts`


<!-- END_3a2624800023d9dde1245764c2c80724 -->

<!-- START_711c101d656836af1b9d2bb05fa2a96f -->
## api/v1/social-learning-posts/create

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-posts/create" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-posts/create",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-posts/create`

`HEAD api/v1/social-learning-posts/create`


<!-- END_711c101d656836af1b9d2bb05fa2a96f -->

<!-- START_8614a37b010a69ebaab32bf236b5b8e5 -->
## api/v1/social-learning-posts

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/social-learning-posts" \
-H "Accept: application/json" \
    -d "photo"="ut" \
    -d "content"="ut" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-posts",
    "method": "POST",
    "data": {
        "photo": "ut",
        "content": "ut"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
"{\n data: {\n    \"photo\": null,\n    \"content\": \"New Content\",\n    \"user_id\": 1,\n    \"tenant_id\": 1,\n    \"updated_at\": \"2018-04-11 09:35:46\",\n    \"created_at\": \"2018-04-11 09:35:46\",\n    \"id\": 1\n  },\n}"
```

### HTTP Request
`POST api/v1/social-learning-posts`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    photo | string |  optional  | 
    content | string |  required  | 

<!-- END_8614a37b010a69ebaab32bf236b5b8e5 -->

<!-- START_83d193fd49e76c5ff2255608ce5b9fd9 -->
## api/v1/social-learning-posts/{social_learning_post}

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-posts/{social_learning_post}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-posts/{social_learning_post}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-posts/{social_learning_post}`

`HEAD api/v1/social-learning-posts/{social_learning_post}`


<!-- END_83d193fd49e76c5ff2255608ce5b9fd9 -->

<!-- START_41c6443acf98828cdce5a95945b5539c -->
## api/v1/social-learning-posts/{social_learning_post}/edit

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-posts/{social_learning_post}/edit" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-posts/{social_learning_post}/edit",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-posts/{social_learning_post}/edit`

`HEAD api/v1/social-learning-posts/{social_learning_post}/edit`


<!-- END_41c6443acf98828cdce5a95945b5539c -->

<!-- START_ddac22946cd03f14e38f712658cd47b1 -->
## api/v1/social-learning-posts/{social_learning_post}

> Example request:

```bash
curl -X PUT "http://babastudio.test/api/v1/social-learning-posts/{social_learning_post}" \
-H "Accept: application/json" \
    -d "photo"="debitis" \
    -d "content"="debitis" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-posts/{social_learning_post}",
    "method": "PUT",
    "data": {
        "photo": "debitis",
        "content": "debitis"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/social-learning-posts/{social_learning_post}`

`PATCH api/v1/social-learning-posts/{social_learning_post}`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    photo | string |  optional  | 
    content | string |  required  | 

<!-- END_ddac22946cd03f14e38f712658cd47b1 -->

<!-- START_f7cc885d3455559e9fc52e433ac0bf14 -->
## api/v1/social-learning-posts/{post_id}

> Example request:

```bash
curl -X DELETE "http://babastudio.test/api/v1/social-learning-posts/{social_learning_post}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-posts/{social_learning_post}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/v1/social-learning-posts/{social_learning_post}`


<!-- END_f7cc885d3455559e9fc52e433ac0bf14 -->

<!-- START_66f6cb4b8a695642b2608e50e0d54567 -->
## api/v1/posts/{id}/likes

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/posts/{id}/likes" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/posts/{id}/likes",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/posts/{id}/likes`


<!-- END_66f6cb4b8a695642b2608e50e0d54567 -->

<!-- START_0489583d0829994e46dbe12456f54bd1 -->
## api/v1/posts/{id}/dislikes

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/posts/{id}/dislikes" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/posts/{id}/dislikes",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/posts/{id}/dislikes`


<!-- END_0489583d0829994e46dbe12456f54bd1 -->

<!-- START_3255d465b980ccc318da0f3fd71d34a6 -->
## api/v1/posts/{id}/comments

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/posts/{id}/comments" \
-H "Accept: application/json" \
    -d "content"="voluptas" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/posts/{id}/comments",
    "method": "POST",
    "data": {
        "content": "voluptas"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/posts/{id}/comments`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    content | string |  required  | 

<!-- END_3255d465b980ccc318da0f3fd71d34a6 -->

<!-- START_029d7a41638d80a922dba3a8627d0021 -->
## api/v1/comments/{id}/likes

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/comments/{id}/likes" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/comments/{id}/likes",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/comments/{id}/likes`


<!-- END_029d7a41638d80a922dba3a8627d0021 -->

<!-- START_31b0579c584df848f6c8f60dda2f1692 -->
## api/v1/comments/{id}/dislikes

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/comments/{id}/dislikes" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/comments/{id}/dislikes",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/comments/{id}/dislikes`


<!-- END_31b0579c584df848f6c8f60dda2f1692 -->

<!-- START_0cded5ca8cf79ca935b9b560d4646787 -->
## api/v1/social-learning-posts/{post_id}/image

This request will return image as string Base64

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-posts/{id}/image" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-posts/{id}/image",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-posts/{id}/image`

`HEAD api/v1/social-learning-posts/{id}/image`


<!-- END_0cded5ca8cf79ca935b9b560d4646787 -->

#Profile Social Learning

Longer description
<!-- START_a3d79fa72c0a81970a47643feb6ed710 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-profile" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-profile",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-profile`

`HEAD api/v1/social-learning-profile`


<!-- END_a3d79fa72c0a81970a47643feb6ed710 -->

<!-- START_9e0a3ef90e009f305bb578f2a4ab105e -->
## Show the form for creating a new resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-profile/create" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-profile/create",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-profile/create`

`HEAD api/v1/social-learning-profile/create`


<!-- END_9e0a3ef90e009f305bb578f2a4ab105e -->

<!-- START_39288ea5d89398ab485bd04c6d5cb128 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/social-learning-profile" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-profile",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/social-learning-profile`


<!-- END_39288ea5d89398ab485bd04c6d5cb128 -->

<!-- START_c4e59fd562a610152dac9c60d4f0de95 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-profile/{social_learning_profile}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-profile/{social_learning_profile}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-profile/{social_learning_profile}`

`HEAD api/v1/social-learning-profile/{social_learning_profile}`


<!-- END_c4e59fd562a610152dac9c60d4f0de95 -->

<!-- START_26d056303b785204d4ac281c0a57ab11 -->
## Show the form for editing the specified resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-profile/{social_learning_profile}/edit" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-profile/{social_learning_profile}/edit",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-profile/{social_learning_profile}/edit`

`HEAD api/v1/social-learning-profile/{social_learning_profile}/edit`


<!-- END_26d056303b785204d4ac281c0a57ab11 -->

<!-- START_b837720d61cbf82a08146b0eda71b607 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT "http://babastudio.test/api/v1/social-learning-profile/{social_learning_profile}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-profile/{social_learning_profile}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/social-learning-profile/{social_learning_profile}`

`PATCH api/v1/social-learning-profile/{social_learning_profile}`


<!-- END_b837720d61cbf82a08146b0eda71b607 -->

<!-- START_f86c6ee47d1619667172c19ec10361e2 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE "http://babastudio.test/api/v1/social-learning-profile/{social_learning_profile}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-profile/{social_learning_profile}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/v1/social-learning-profile/{social_learning_profile}`


<!-- END_f86c6ee47d1619667172c19ec10361e2 -->

#Question

This module requires full authentication
 Headers : 
   - Authorization : &quot;Bearer ACCESS_TOKEN&quot;
<!-- START_9e468fc004f35a3d92e87c3ed003cea9 -->
## Get All.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/questions" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/questions",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/questions`

`HEAD api/v1/questions`


<!-- END_9e468fc004f35a3d92e87c3ed003cea9 -->

<!-- START_5e0d3d3e18ece1f385154962c8361fbe -->
## Show the form for creating a new resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/questions/create" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/questions/create",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/questions/create`

`HEAD api/v1/questions/create`


<!-- END_5e0d3d3e18ece1f385154962c8361fbe -->

<!-- START_fbd033d4038578cf755c7758d540c071 -->
## Insert.

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/questions" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/questions",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/questions`


<!-- END_fbd033d4038578cf755c7758d540c071 -->

<!-- START_486d51887e71e3da442b940dae81ab4d -->
## Get One.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/questions/{question}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/questions/{question}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/questions/{question}`

`HEAD api/v1/questions/{question}`


<!-- END_486d51887e71e3da442b940dae81ab4d -->

<!-- START_078918c5adf058ed9dbd04cc6bb7207f -->
## Show the form for editing the specified resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/questions/{question}/edit" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/questions/{question}/edit",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/questions/{question}/edit`

`HEAD api/v1/questions/{question}/edit`


<!-- END_078918c5adf058ed9dbd04cc6bb7207f -->

<!-- START_2d806ae864a0f561ddc506a08c49c811 -->
## Update.

> Example request:

```bash
curl -X PUT "http://babastudio.test/api/v1/questions/{question}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/questions/{question}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/questions/{question}`

`PATCH api/v1/questions/{question}`


<!-- END_2d806ae864a0f561ddc506a08c49c811 -->

<!-- START_1d32db85c9b6da1172fc7c9a15d7e885 -->
## Delete.

> Example request:

```bash
curl -X DELETE "http://babastudio.test/api/v1/questions/{question}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/questions/{question}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/v1/questions/{question}`


<!-- END_1d32db85c9b6da1172fc7c9a15d7e885 -->

<!-- START_ffe6afd38b9af9e9e5d792f3cafed808 -->
## Like.

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/questions/{id}/likes" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/questions/{id}/likes",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/questions/{id}/likes`


<!-- END_ffe6afd38b9af9e9e5d792f3cafed808 -->

<!-- START_1fc93a16c435449ca03476fa30ff1c8e -->
## Dislike.

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/questions/{id}/dislikes" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/questions/{id}/dislikes",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/questions/{id}/dislikes`


<!-- END_1fc93a16c435449ca03476fa30ff1c8e -->

<!-- START_df0dac59fc94cb94a0dbfd143ecc861b -->
## Comment.

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/questions/{id}/comments" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/questions/{id}/comments",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/questions/{id}/comments`


<!-- END_df0dac59fc94cb94a0dbfd143ecc861b -->

<!-- START_acb910d5358d0d27964177b310c97739 -->
## Like Comment.

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/questions/comments/{id}/likes" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/questions/comments/{id}/likes",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/questions/comments/{id}/likes`


<!-- END_acb910d5358d0d27964177b310c97739 -->

<!-- START_d5e410645170521550488958a9fb8f56 -->
## Dislike Comment.

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/questions/comments/{id}/dislikes" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/questions/comments/{id}/dislikes",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/questions/comments/{id}/dislikes`


<!-- END_d5e410645170521550488958a9fb8f56 -->

#Quiz

Longer description
<!-- START_a7c93e92ef138a98656743e9720da410 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/quizzes" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/quizzes",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/quizzes`

`HEAD api/v1/quizzes`


<!-- END_a7c93e92ef138a98656743e9720da410 -->

<!-- START_6afecff9ae99413a7be71393b4cc929d -->
## Show the form for creating a new resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/quizzes/create" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/quizzes/create",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/quizzes/create`

`HEAD api/v1/quizzes/create`


<!-- END_6afecff9ae99413a7be71393b4cc929d -->

<!-- START_2ddfa9e9ab268cf8cf2fe44d13ba1c08 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/quizzes" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/quizzes",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/quizzes`


<!-- END_2ddfa9e9ab268cf8cf2fe44d13ba1c08 -->

<!-- START_3bc770160a06512c22d8ef47c7127157 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/quizzes/{quiz}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/quizzes/{quiz}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/quizzes/{quiz}`

`HEAD api/v1/quizzes/{quiz}`


<!-- END_3bc770160a06512c22d8ef47c7127157 -->

<!-- START_621de4b319296a368ba5503cda23e811 -->
## Show the form for editing the specified resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/quizzes/{quiz}/edit" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/quizzes/{quiz}/edit",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/quizzes/{quiz}/edit`

`HEAD api/v1/quizzes/{quiz}/edit`


<!-- END_621de4b319296a368ba5503cda23e811 -->

<!-- START_25f3cd9470eac3b762cae2d8cb3ea5b4 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT "http://babastudio.test/api/v1/quizzes/{quiz}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/quizzes/{quiz}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/quizzes/{quiz}`

`PATCH api/v1/quizzes/{quiz}`


<!-- END_25f3cd9470eac3b762cae2d8cb3ea5b4 -->

<!-- START_8cb0b4c756715f4467d7a8b6a8c61fb5 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE "http://babastudio.test/api/v1/quizzes/{quiz}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/quizzes/{quiz}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/v1/quizzes/{quiz}`


<!-- END_8cb0b4c756715f4467d7a8b6a8c61fb5 -->

#Section Social Learning

Longer description
<!-- START_915e5ece5cd5531968056a2ea9a5b00c -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/courses/{id}/section" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/courses/{id}/section",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/courses/{id}/section`


<!-- END_915e5ece5cd5531968056a2ea9a5b00c -->

<!-- START_82db79bd578441f42b091c5db5840bbc -->
## api/v1/sections/delete-all/{id}

> Example request:

```bash
curl -X DELETE "http://babastudio.test/api/v1/sections/delete-all/{id}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/sections/delete-all/{id}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/v1/sections/delete-all/{id}`


<!-- END_82db79bd578441f42b091c5db5840bbc -->

#Setting Social Learning

Longer description
<!-- START_ee33a4454ddd6fbecb5499a991eed58d -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-setting" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-setting",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-setting`

`HEAD api/v1/social-learning-setting`


<!-- END_ee33a4454ddd6fbecb5499a991eed58d -->

<!-- START_1fe7a88edbf483008da7a3089d40f2cf -->
## Show the form for creating a new resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-setting/create" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-setting/create",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-setting/create`

`HEAD api/v1/social-learning-setting/create`


<!-- END_1fe7a88edbf483008da7a3089d40f2cf -->

<!-- START_6a45eff5a4e61831063da9dd77fb63e4 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/social-learning-setting" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-setting",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/social-learning-setting`


<!-- END_6a45eff5a4e61831063da9dd77fb63e4 -->

<!-- START_3c20c181d3f55ce400dcbcf9ba61e56d -->
## Display the specified resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-setting/{social_learning_setting}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-setting/{social_learning_setting}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-setting/{social_learning_setting}`

`HEAD api/v1/social-learning-setting/{social_learning_setting}`


<!-- END_3c20c181d3f55ce400dcbcf9ba61e56d -->

<!-- START_23500abe36eb29e8db3ce14bf32fc8bd -->
## Show the form for editing the specified resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-setting/{social_learning_setting}/edit" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-setting/{social_learning_setting}/edit",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-setting/{social_learning_setting}/edit`

`HEAD api/v1/social-learning-setting/{social_learning_setting}/edit`


<!-- END_23500abe36eb29e8db3ce14bf32fc8bd -->

<!-- START_231206a8b7d7f8ab035abf85f6b8d163 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT "http://babastudio.test/api/v1/social-learning-setting/{social_learning_setting}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-setting/{social_learning_setting}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/social-learning-setting/{social_learning_setting}`

`PATCH api/v1/social-learning-setting/{social_learning_setting}`


<!-- END_231206a8b7d7f8ab035abf85f6b8d163 -->

<!-- START_dc8320161700e8784d0e2762946589fe -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE "http://babastudio.test/api/v1/social-learning-setting/{social_learning_setting}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-setting/{social_learning_setting}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/v1/social-learning-setting/{social_learning_setting}`


<!-- END_dc8320161700e8784d0e2762946589fe -->

#User Social Learning

Longer description
<!-- START_8499058e06d5d2523bf69d440e124483 -->
## api/v1/users/{id}/follow

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/users/{id}/follow" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/users/{id}/follow",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/users/{id}/follow`


<!-- END_8499058e06d5d2523bf69d440e124483 -->

<!-- START_bea52021f2f1a49a60e189c80f0c91de -->
## api/v1/users/{id}/unfollow

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/users/{id}/unfollow" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/users/{id}/unfollow",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/users/{id}/unfollow`


<!-- END_bea52021f2f1a49a60e189c80f0c91de -->

<!-- START_080f3ecebb7bcc2f93284b8f5ae1ac3b -->
## api/v1/users

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/users" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/users",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/users`

`HEAD api/v1/users`


<!-- END_080f3ecebb7bcc2f93284b8f5ae1ac3b -->

<!-- START_516efe68800340987a961f28f13fffbd -->
## Show the form for creating a new resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/users/create" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/users/create",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/users/create`

`HEAD api/v1/users/create`


<!-- END_516efe68800340987a961f28f13fffbd -->

<!-- START_4194ceb9a20b7f80b61d14d44df366b4 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/users" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/users",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/users`


<!-- END_4194ceb9a20b7f80b61d14d44df366b4 -->

<!-- START_b4ea58dd963da91362c51d4088d0d4f4 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/users/{user}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/users/{user}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/users/{user}`

`HEAD api/v1/users/{user}`


<!-- END_b4ea58dd963da91362c51d4088d0d4f4 -->

<!-- START_2960955e5812bbc17bfc941ae06fad43 -->
## Show the form for editing the specified resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/users/{user}/edit" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/users/{user}/edit",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/users/{user}/edit`

`HEAD api/v1/users/{user}/edit`


<!-- END_2960955e5812bbc17bfc941ae06fad43 -->

<!-- START_296fac4bf818c99f6dd42a4a0eb56b58 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT "http://babastudio.test/api/v1/users/{user}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/users/{user}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/users/{user}`

`PATCH api/v1/users/{user}`


<!-- END_296fac4bf818c99f6dd42a4a0eb56b58 -->

<!-- START_22354fc95c42d81a744eece68f5b9b9a -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE "http://babastudio.test/api/v1/users/{user}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/users/{user}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/v1/users/{user}`


<!-- END_22354fc95c42d81a744eece68f5b9b9a -->

<!-- START_b141b6cedcf6287a82afb19d16ede928 -->
## api/v1/users/{name}/search

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/users/{name}/search" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/users/{name}/search",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/v1/users/{name}/search`

`HEAD api/v1/users/{name}/search`


<!-- END_b141b6cedcf6287a82afb19d16ede928 -->

<!-- START_44b173697ae183ffb0d0c7ed521de1c2 -->
## api/v1/users/role/{id}

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/users/role/{id}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/users/role/{id}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/users/role/{id}`

`HEAD api/v1/users/role/{id}`


<!-- END_44b173697ae183ffb0d0c7ed521de1c2 -->

<!-- START_dfb3d7037db610e35d167b24729b8080 -->
## api/v1/user/update-role/{id}

> Example request:

```bash
curl -X PUT "http://babastudio.test/api/v1/user/update-role/{id}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/user/update-role/{id}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/user/update-role/{id}`


<!-- END_dfb3d7037db610e35d167b24729b8080 -->

#general
<!-- START_86eb9ac1bae1048977186921f7283685 -->
## api/v1/material/store

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/material/store" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/material/store",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/material/store`


<!-- END_86eb9ac1bae1048977186921f7283685 -->

<!-- START_50cf937a551933a8015157eac3775c00 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-course" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-course",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-course`

`HEAD api/v1/social-learning-course`


<!-- END_50cf937a551933a8015157eac3775c00 -->

<!-- START_571a4429b7673381bef8af53b36b264c -->
## Show the form for creating a new resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-course/create" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-course/create",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-course/create`

`HEAD api/v1/social-learning-course/create`


<!-- END_571a4429b7673381bef8af53b36b264c -->

<!-- START_42e249fb9e3364f1154d642b2fa4902d -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/social-learning-course" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-course",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/social-learning-course`


<!-- END_42e249fb9e3364f1154d642b2fa4902d -->

<!-- START_9d9ce6484ac22e01de0ee86924148971 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-course/{social_learning_course}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-course/{social_learning_course}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-course/{social_learning_course}`

`HEAD api/v1/social-learning-course/{social_learning_course}`


<!-- END_9d9ce6484ac22e01de0ee86924148971 -->

<!-- START_276987e64ceca852753cb177a9e6fbb8 -->
## Show the form for editing the specified resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/social-learning-course/{social_learning_course}/edit" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-course/{social_learning_course}/edit",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/social-learning-course/{social_learning_course}/edit`

`HEAD api/v1/social-learning-course/{social_learning_course}/edit`


<!-- END_276987e64ceca852753cb177a9e6fbb8 -->

<!-- START_75fbf3a9edd0a071f4600d5179dbf4f0 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT "http://babastudio.test/api/v1/social-learning-course/{social_learning_course}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-course/{social_learning_course}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/social-learning-course/{social_learning_course}`

`PATCH api/v1/social-learning-course/{social_learning_course}`


<!-- END_75fbf3a9edd0a071f4600d5179dbf4f0 -->

<!-- START_825165d562ba00cb667e796b03975d97 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE "http://babastudio.test/api/v1/social-learning-course/{social_learning_course}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/social-learning-course/{social_learning_course}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/v1/social-learning-course/{social_learning_course}`


<!-- END_825165d562ba00cb667e796b03975d97 -->

<!-- START_80b78bb5213d82cf8fedc59eec885310 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/courses" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/courses",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/courses`

`HEAD api/v1/courses`


<!-- END_80b78bb5213d82cf8fedc59eec885310 -->

<!-- START_1db061fd40df6a6ab423456db8812db2 -->
## Show the form for creating a new resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/courses/create" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/courses/create",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/courses/create`

`HEAD api/v1/courses/create`


<!-- END_1db061fd40df6a6ab423456db8812db2 -->

<!-- START_15245ea1278895ac4c88a2967d66b62d -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/courses" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/courses",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/courses`


<!-- END_15245ea1278895ac4c88a2967d66b62d -->

<!-- START_3e9bac20f710ad63b29e6258d84d2035 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/courses/{course}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/courses/{course}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/courses/{course}`

`HEAD api/v1/courses/{course}`


<!-- END_3e9bac20f710ad63b29e6258d84d2035 -->

<!-- START_3c216f4511e23c8ef807d7a15a1179a3 -->
## Show the form for editing the specified resource.

> Example request:

```bash
curl -X GET "http://babastudio.test/api/v1/courses/{course}/edit" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/courses/{course}/edit",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/courses/{course}/edit`

`HEAD api/v1/courses/{course}/edit`


<!-- END_3c216f4511e23c8ef807d7a15a1179a3 -->

<!-- START_1a067bbc74e95f961ad6bc806b898b73 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT "http://babastudio.test/api/v1/courses/{course}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/courses/{course}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/courses/{course}`

`PATCH api/v1/courses/{course}`


<!-- END_1a067bbc74e95f961ad6bc806b898b73 -->

<!-- START_04c095825d47b1ac994d43c4d7b5da6b -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE "http://babastudio.test/api/v1/courses/{course}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/courses/{course}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/v1/courses/{course}`


<!-- END_04c095825d47b1ac994d43c4d7b5da6b -->

<!-- START_e645c6e853b6b43b3d0e44e6baafad26 -->
## api/v1/users/select-superadmin

> Example request:

```bash
curl -X POST "http://babastudio.test/api/v1/users/select-superadmin" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/users/select-superadmin",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/users/select-superadmin`


<!-- END_e645c6e853b6b43b3d0e44e6baafad26 -->

<!-- START_f8e547ca61eef68a5e2891043284a491 -->
## api/v1/material/delete/{id}

> Example request:

```bash
curl -X DELETE "http://babastudio.test/api/v1/material/delete/{id}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://babastudio.test/api/v1/material/delete/{id}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/v1/material/delete/{id}`


<!-- END_f8e547ca61eef68a5e2891043284a491 -->

