<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>NBLS - Babastudio</title>
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
      <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="css/custom.css" rel="stylesheet">
      <link href="lib/animate/animate.min.css" rel="stylesheet">
      <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
      <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
      <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">
      <link href="css/style.css" rel="stylesheet">
   </head>
   <body>
     <!-- topbar -->
      <div class="container-fluid bg-blue top-bar f-white pl-5 pb-2">
        <div class="row">
          <div class="col-md-8 col-sm-12">
            <div class="ml-4"><i class="fa fa-phone ml-5 float-sm-left mt-1"></i> + Call Us : 021 - 5366 4008</div>
          </div>
          <div class="col-md-4 col-sm-12">
            <div class="ml-2 tentang">
              <a href="#" class="f-white mr-2">Tentang Kami</a><span>|<span>
              <i class="fa fa-user ml-2"></i>
              <a href="#" class="mr-1 link">Login</a> or <a href="#" class="mr-1 link">Register</a>
              <span><i class="fa fa-shopping-cart"></i></span>
            </div>
          </div>
        </div>
      </div>
      <!-- end topbar -->
      <!-- navbar -->
      <nav class="navbar  navbar-expand-lg navbar-dark bg-black navbar-bootbites" data-toggle="sticky-onscroll">
         <div class="container">
            <a class="navbar-brand" href="#"><img src="images/babastudio.png" width="200em" class="img-fluid"></a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
               <ul class="navbar-nav ml-auto">
                  <li class="nav-item">
                     <a class="nav-link" href="#">BERANDA</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="#">PAKET KURSUS ONLINE</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="#">CARA PENGGUNAAN</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="#">KUPON DISKON</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="#">MULAI BELAJAR</a>
                  </li>
               </ul>
            </div>
         </div>
      </nav>
      <!-- end navbar -->
      <!-- section 1 -->
      <section id="intro">
         <div class="intro-container">
            <div id="introCarousel" class="carousel  slide carousel-slide" data-ride="carousel">
               <div class="carousel-inner" role="listbox">
                  <div class="carousel-item active">
                     <div class="carousel-background"><img src="images/1.jpg" alt=""></div>
                     <div class="carousel-container">
                        <div class="carousel-content">
                           <h2><span class="f-orange f-slim mt-2">KURSUS</span> <br/><span class="f-orange f-slim">ONLINE HYBRID</span><span class="f-white"> NBLS</span></h2>
                           <h4 class="f-white mb-5 f-slim">Kursus Online dengan Live</h4>
                           <div class="col-md-6 float-left">
                              <img src="images/ditemani.jpg" class="img-fluid img-caption">
                              <h5 class="f-white f-slim t-slide">Belajar</h5>
                              <h5 class="f-white">DITEMANI & DIBIMBING</h5>
                           </div>
                           <div class="col-md-6 float-left">
                              <img src="images/kur.jpg" class="img-fluid img-caption">
                              <h5 class="f-white f-slim t-slide">Pertanyaan</h5>
                              <h5 class="f-white">DIJAWAB REALTIME</h5>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="carousel-item">
                     <div class="carousel-background"><img src="images/1.jpg" alt=""></div>
                     <div class="carousel-container">
                        <div class="carousel-content">
                           <h2><span class="f-orange f-slim">KURSUS</span> <br/><span class="f-orange f-slim">ONLINE HYBRID</span><span class="f-white"> NBLS</span></h2>
                           <h4 class="f-white mb-5 f-slim">Kursus Online dengan Live</h4>
                           <div class="col-md-6 float-left">
                              <img src="images/ditemani.jpg" class="img-fluid img-caption">
                              <h5 class="f-white f-slim t-slide">Belajar</h5>
                              <h4 class="f-white">ditemani & dibimbing</h4>
                           </div>
                           <div class="col-md-6 float-left">
                              <img src="images/kur.jpg" class="img-fluid img-caption">
                              <h5 class="f-white f-slim t-slide">Belajar</h5>
                              <h4 class="f-white">ditemani & dibimbing</h4>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <a class="carousel-control-prev" href="#introCarousel" role="button" data-slide="prev">
               <span class="carousel-control-prev-icon ion-chevron-left" aria-hidden="true"></span>
               <span class="sr-only">Previous</span>
               </a>
               <a class="carousel-control-next" href="#introCarousel" role="button" data-slide="next">
               <span class="carousel-control-next-icon ion-chevron-right" aria-hidden="true"></span>
               <span class="sr-only">Next</span>
               </a>
            </div>
         </div>
      </section>
      <!-- end section 1 -->
      <!-- main -->
      <main id="main">
        <!-- section 2 -->
         <section id="clients" class="bg-grey">
            <div class="container media row offset-md-1">
               <div class="col-md-4 mt-3">
                  <h2 style="margin-bottom:-1em;">TELAH DILIPUT</h2>
                  <br/>
                  <h2 class="f-blue f-bold">DI MEDIA TELEVISI</h2>
               </div>
               <div class="col-md-2 col-sm-12 mb-2">
                  <div class="card">
                     <div class="card-body"><img src="images/metrologo.png"></div>
                  </div>
               </div>
               <div class="col-md-2 col-sm-12 mb-2">
                  <div class="card">
                     <div class="card-body"><img src="images/kompaslogo.png"></div>
                  </div>
               </div>
               <div class="col-md-2 col-sm-12 mb-2">
                  <div class="card">
                     <div class="card-body"><img src="images/mnclogo.png"></div>
                  </div>
               </div>
               <div class="col-md-2 col-sm-12 mb-2">
                  <div class="card">
                     <div class="card-body"><img src="images/tvonelogo.png"></div>
                  </div>
               </div>
            </div>
         </section>
         <!-- end section 2 -->
         <!-- section 3 -->
         <section id="services" class="bg-white">
            <div class="container">
               <div class="row">
                  <div class="col-lg-6 col-md-6 bg-blue f-white">
                     <h2 class="title mb-5"><span class="f-orange f-slim">LIPUTAN</span>
                        <br/><span>MEDIA</span>
                     </h2>
                     <h4 class="mb-3" >Liputan Babastudio E-learning di kompas TV bersama Alumni</h4>
                     <p class="description mb-5">Kompas TV meliput Babastudio E-learning karena, memberikan kursus gratis pada lebih dari 500 orang setiap harinya. </p>
                     <div class="video-container mb-5">
                        <iframe src="//www.youtube.com/embed/zgbFOxI4ROw" frameborder="0" width="560" height="315"></iframe>
                     </div>
                  </div>
                  <div class="col-lg-6 col-md-6 pt-5" >
                     <div class="video-container mt-5">
                        <iframe src="//www.youtube.com/embed/zgbFOxI4ROw" frameborder="0" width="560" height="315"></iframe>
                     </div>
                     <h4 class="mt-5 mb-4" >Membantu Pusta Intel AD & AU Menguasai SEO & Digital Marketing</h4>
                     <p class="description mt-2">Kompas TV meliput Babastudio E-learning karena, memberikan kursus gratis pada lebih dari 500 orang setiap harinya.</p>
                  </div>
               </div>
            </div>
         </section>
         <!-- end section 3 -->
         <!-- section 4 -->
         <section id="services" class="bg-image">
            <div class="container">
            <div class="row">
               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 bg-grey">
                  <h2 class="title2"> <br/> <span class="">KENAPA</span>
                     <span class="f-blue f-slim">HYBRID LEARNING</span><br/><span>Dapat membantu Anda</span>
                  </h2>
                  <!-- Vertical Timeline -->
                  <section id="conference-timeline">
                     <div class="conference-center-line"></div>
                     <div class="conference-timeline-content">
                        <div class="timeline-article mb-4">
                           <div class="content-left-container">
                              <div class="content-left">
                                 <img src="images/kur.jpg" class="img-fluid">
                              </div>
                           </div>
                           <div class="content-right-container">
                              <div class="content-right">
                                 <h5>INSTRUKTUR SAMA</h5>
                                 <p>Instruktur SAMA PERSIS dengan kelas Reguler tatap muka di babastudio</p>
                              </div>
                           </div>
                           <div class="meta-date">
                           </div>
                        </div>
                        <div class="timeline-article mb-4">
                           <div class="content-left-container">
                              <div class="content-left">
                                 <img src="images/materi.jpg" class="img-fluid">
                              </div>
                           </div>
                           <div class="content-right-container">
                              <div class="content-right">
                                 <h5>MATERI SAMA</h5>
                                 <p>Materi SAMA PERSIS dengan kelas Reguler tatap muka di babastudio</p>
                              </div>
                           </div>
                           <div class="meta-date">
                           </div>
                        </div>
                        <div class="timeline-article">
                           <div class="content-left-container">
                              <div class="content-left">
                                 <img src="images/ditemani.jpg" class="img-fluid">
                              </div>
                           </div>
                           <div class="content-right-container">
                              <div class="content-right">
                                 <h5>SERTIFIKAT SAMA</h5>
                                 <p>Sertifikat SAMA PESIS dengan kelas Regular tatap muka di babastudio</p>
                              </div>
                           </div>
                           <div class="meta-date">
                           </div>
                        </div>
                     </div>
                  </section>
               </div>
            </div>
         </section>
         <!-- end section 4 -->
         <!-- section 5 -->
         <section id="services" class="bg-white">
            <div class="container">
               <div class="row">
                  <div class="col-lg-6 col-md-6 bg-white f-white">
                     <!-- <div class="icon"><i class="ion-ios-analytics-outline"></i></div> -->
                     <div class="video-container mb-5 mt-5">
                        <iframe src="//www.youtube.com/embed/zgbFOxI4ROw" frameborder="0" width="560" height="315"></iframe>
                     </div>
                  </div>
                  <div class="col-lg-6 col-md-6 pt-5" >
                     <div class="services-top">
                        <div class="container bootstrap snippet">
                           <div class="row text-left">
                              <div class="col-sm-12 col-md-12 col-md-12">
                                 <h2>APA ITU</h2>
                                 <h2><b>LIVE INSTRUKTUR</b></h2>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-offset-1 col-sm-12 col-md-12 col-md-10">
                                 <div class="services-list">
                                    <div class="row">
                                       <div class="col-sm-12 col-md-12 col-md-12">
                                          <div class="service-block" style="visibility: visible;">
                                             <div class="ico highlight mr-2 mb-2"><img src="images/icon1.png" class="img-fluid"></div>
                                             <div class="text-block">
                                                <div class="text">Akan memastikan Anda mudah menguasai Skill yang Anda pelajari kapanpun Anda bertanya pada masa belajar, Instruktur akan menjawab secara realtime.</div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-sm-12 col-md-12 col-md-12">
                                          <div class="service-block" style="visibility: visible;">
                                             <div class="ico highlight mr-2 mb-2"><img src="images/icon2.png" class="img-fluid"></div>
                                             <div class="text-block">
                                                <div class="text">Jika ada masalah dalam bahasa pemrograman Anda, Instruktur akan LIVE mngkoreksi kesalahan Anda. Progress Anda akan di Bimbing secara menyeluruh.</div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-sm-12 col-md-12 col-md-12">
                                          <div class="service-block" style="visibility: visible;">
                                             <div class="ico highlight mr-2 mb-2"><img src="images/icon3.png" class="img-fluid"></div>
                                             <div class="text-block">
                                                <div class="text">Anda pun dapat berkomunitas dan Berkomunikasi dengan para Murid dan Alumni babastudio.</div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- end section 5 -->
         <!-- section 6 -->
         <section id="about" class="bg-grey">
            <div class="container">
               <header class="section-header">
                  <h3 class="text-center mb-5">Hanya 1 Harga dapatkan Seluruh paket Kursus <br/>babastudio ini senilai 27 Juta</h3>
               </header>
               <div class="row about-cols">
                  <div class="col-md-4">
                     <div class="about-col">
                        <div class="card h-100">
                           <div class="img">
                              <img src="images/game.jpg" alt="" class="img-fluid">
                              <div class="icon"><img src="images/logo-b.png"></div>
                           </div>
                           <div class="card-body mt-3">
                              <small class="text-muted ml-3"><i class="fa fa-clock-o ico mr-2" aria-hidden="true"></i><span class="mr-2">72 jam</span> <i class="fa fa-calendar ico" aria-hidden="true"></i> 10 Juli 2017</small>
                              <h2 class="title ml-3"><a href="#" class="f-blue f-slim">KURSUS GAMES ONLINE</a></h2>
                              <p>
                                 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                              </p>
                           </div>
                           <div class="card-footer">
                              <h5 class="float-left mt-1 ml-2">Nama Instruktur</h5>
                              <a href="" class="btn btn-primary ml-2">Lihat Detail</a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="about-col">
                        <div class="card h-100">
                           <div class="img">
                              <img src="images/game.jpg" alt="" class="img-fluid">
                              <div class="icon"><img src="images/logo-b.png"></div>
                           </div>
                           <div class="card-body mt-3">
                              <small class="text-muted ml-3"><i class="fa fa-clock-o ico mr-2" aria-hidden="true"></i><span class="mr-2">72 jam</span> <i class="fa fa-calendar ico" aria-hidden="true"></i> 10 Juli 2017</small>
                              <h2 class="title ml-3"><a href="#" class="f-blue f-slim">KURSUS GAMES ONLINE</a></h2>
                              <p>
                                 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                              </p>
                           </div>
                           <div class="card-footer">
                              <h5 class="float-left mt-1 ml-2">Nama Instruktur</h5>
                              <a href="" class="btn btn-primary ml-2">Lihat Detail</a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="about-col">
                        <div class="card h-100">
                           <div class="img">
                              <img src="images/game.jpg" alt="" class="img-fluid">
                              <div class="icon"><img src="images/logo-b.png"></div>
                           </div>
                           <div class="card-body mt-3">
                              <small class="text-muted ml-3"><i class="fa fa-clock-o ico mr-2" aria-hidden="true"></i><span class="mr-2">72 jam</span> <i class="fa fa-calendar ico" aria-hidden="true"></i> 10 Juli 2017</small>
                              <h2 class="title ml-3"><a href="#" class="f-blue f-slim">KURSUS GAMES ONLINE</a></h2>
                              <p>
                                 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                              </p>
                           </div>
                           <div class="card-footer">
                              <h5 class="float-left mt-1 ml-2">Nama Instruktur</h5>
                              <a href="" class="btn btn-primary ml-2">Lihat Detail</a>
                           </div>
                        </div>
                     </div>
                  </div>

               </div>
               <div class="container mt-3">
                 <div class="col-md-3 mx-auto">
                   <button type="button" class="btn btn-primary btn-lg ">LIHAT SELENGKAPNYA</button>
                 </div>
               </div>
            </div>
         </section>
         <!-- end section 6 -->
         <!-- section 7 -->
         <section id="services" class="bg-white">
            <div class="container">
               <header class="section-header mt-5">
                  <h3 class="text-center mb-4">Kami telah membantu lebih dari 21000  <br/>alumni untuk mengupgrade skill nya</h3>
                  <h3 class="text-center mb-4 f-blue f-bold">Apa Kata Mereka ?</h3>
               </header>
               <div class="row">
                  <div class="col-lg-6 col-md-6" >
                     <div class="services-top">
                        <div class="container bootstrap snippet">
                           <div class="row text-left">
                              <div class="col-sm-12 col-md-12 col-md-12">
                                 <h3>21 Ribu Murid dan Bertambah terus</h3>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-offset-1 col-sm-12 col-md-12 col-md-10">
                                 <div class="services-list murid scrollbar" id="style-1">
                                    <div class="row">
                                       <div class="col-sm-12 col-md-12 col-md-12">
                                          <div class="service-block" style="visibility: visible;">
                                             <div class="mr-2 foto"><img src="images/pp.jpg" class="img-fluid" width="70em"></div>
                                             <div class="text-block">
                                                <div class="name">M. Saepul Bahri</div>
                                                <div class="info pl-1 ">Programmer & Graphic Designer</div>
                                                <div class="text desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-sm-12 col-md-12 col-md-12">
                                          <div class="service-block" style="visibility: visible;">
                                             <div class="mr-2 foto"><img src="images/pp.jpg" class="img-fluid" width="70em"></div>
                                             <div class="text-block">
                                                <div class="name">M. Saepul Bahri</div>
                                                <div class="info pl-1">Programmer & Graphic Designer</div>
                                                <div class="text desc" >Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-sm-12 col-md-12 col-md-12">
                                          <div class="service-block" style="visibility: visible;">
                                             <div class="mr-2 foto"><img src="images/pp.jpg" class="img-fluid" width="70em"></div>
                                             <div class="text-block">
                                                <div class="name">M. Saepul Bahri</div>
                                                <div class="info pl-1">Programmer & Graphic Designer</div>
                                                <div class="text desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-sm-12 col-md-12 col-md-12">
                                          <div class="service-block" style="visibility: visible;">
                                             <div class="mr-2 foto"><img src="images/pp.jpg" class="img-fluid" width="70em"></div>
                                             <div class="text-block">
                                                <div class="name">M. Saepul Bahri</div>
                                                <div class="info pl-1">Programmer & Graphic Designer</div>
                                                <div class="text desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-6 col-md-6 bg-white">
                     <h3>Apa Kata Murid Babastudio ?</h3>
                     <div class="video-container mb-5">
                        <iframe src="//www.youtube.com/embed/uP67y-RCtwM" frameborder="0" width="560" height="315"></iframe>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- end section 7 -->
      </main>
      <!-- end main -->
      <!-- Footer -->
      <section class="section-footer" id="footer">
         <div class="container">
            <div class="row text-center text-xs-center text-sm-left text-md-left">
               <div class="col-xs-12 col-sm-4 col-md-3">
                  <img src="images/logo.jpg" class="img-fluid">
                  <p class="f-white">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                     Lorem Ipsum has been the industry's
                     standard dummy text ever since the 1500s,
                     when an unknown printer took a galley of type and scrambled
                     it to make a type specimen book.
                  </p>
                  <ul class="list-unstyled quick-links">
                     <li><a href="javascript:void();"><i class="fa fa-phone"></i>(021) 5366 4008</a></li>
                     <li><a href="javascript:void();"><i class="fa fa-envelope"></i>info@babastudio.com</a></li>
                  </ul>
               </div>
               <div class="col-xs-12 col-sm-4 col-md-3">
                  <h5>LINK MAP</h5>
                  <ul class="list-unstyled quick-links">
                     <li><a href="javascript:void();"><i class="fa fa-circle"></i>Home</a></li>
                     <li><a href="javascript:void();"><i class="fa fa-circle"></i>About</a></li>
                     <li><a href="javascript:void();"><i class="fa fa-circle" ></i>FAQ</a></li>
                     <li><a href="javascript:void();"><i class="fa fa-circle"></i>Get Started</a></li>
                     <li><a href="javascript:void();"><i class="fa fa-circle"></i>Videos</a></li>
                  </ul>
               </div>
               <div class="col-xs-12 col-sm-4 col-md-3">
                  <h5>GET NEWSLETTER</h5>
                  <p class="f-white">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                     Lorem Ipsum has been the industry's
                  </p>
                  <div class="input-group">
                     <input type="text" class="form-control" placeholder="" aria-describedby="basic-addon2">
                     <span class="input-group-addon" id="basic-addon2"><i class="fa fa-paper-plane" aria-hidden="true"></i></span>
                     <br/>
                     <!-- <h6 class="f-white f-slim">Yout Information are safe with us </h6> -->
                  </div>
                  <p class="f-white">Yout Information are safe with us
                  </p>
                  <div id="social">
                     <a class="facebookBtn smGlobalBtn" href="#" ></a>
                     <a class="twitterBtn smGlobalBtn" href="#" ></a>
                     <a class="googleplusBtn smGlobalBtn" href="#" ></a>
                  </div>
               </div>
               <div class="col-xs-12 col-sm-4 col-md-3">
                  <h5>OUR INSTRUCTOR</h5>
                  <div class="col-xs-3 float-left mr-1 mt-1"><img src="images/pp.jpg" width="70em" ></div>
                  <div class="col-xs-3 float-left mr-1 mt-1"><img src="images/pp.jpg" width="70em" ></div>
                  <div class="col-xs-3 float-left mr-1 mt-1"><img src="images/pp.jpg" width="70em" ></div>
                  <div class="col-xs-3 float-left mr-1 mt-1"><img src="images/pp.jpg" width="70em" ></div>
                  <div class="col-xs-3 float-left mr-1 mt-1"><img src="images/pp.jpg" width="70em" ></div>
                  <div class="col-xs-3 float-left mr-1 mt-1"><img src="images/pp.jpg" width="70em" ></div>
                  <div class="col-xs-3 float-left mr-1 mt-1"><img src="images/pp.jpg" width="70em" ></div>
                  <div class="col-xs-3 float-left mr-1 mt-1"><img src="images/pp.jpg" width="70em" ></div>
                  <div class="col-xs-3 float-left mr-1 mt-1"><img src="images/pp.jpg" width="70em" ></div>
               </div>
            </div>
            <hr/ style="border:1px solid #152e48;">
            <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
               <div class="col-md-6 col-sm-12 col-xs-12">
                  <p class="h6 float-left">&copy All right Reversed.<a class="text-green ml-2" href="" target="_blank">BABASTUDIO</a></p>
               </div>
               <div class="credits" style="font-size:5px; color:#ccc; opacity: 0.2; float:right;">
                  Best <a href="https://bootstrapmade.com/">Bootstrap Templates</a> by BootstrapMade
               </div>
            </div>
            </hr>
         </div>
      </section>
      <!-- end Footer -->
      <script src="vendor/jquery/jquery.min.js"></script>
      <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      <script src="js/custom.js"></script>
      <script src="lib/jquery/jquery-migrate.min.js"></script>
      <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
      <script src="lib/easing/easing.min.js"></script>
      <script src="lib/superfish/hoverIntent.js"></script>
      <script src="lib/superfish/superfish.min.js"></script>
      <script src="lib/wow/wow.min.js"></script>
      <script src="lib/waypoints/waypoints.min.js"></script>
      <script src="lib/counterup/counterup.min.js"></script>
      <script src="lib/owlcarousel/owl.carousel.min.js"></script>
      <script src="lib/isotope/isotope.pkgd.min.js"></script>
      <script src="lib/lightbox/js/lightbox.min.js"></script>
      <script src="lib/touchSwipe/jquery.touchSwipe.min.js"></script>
      <script src="js/main.js"></script>
   </body>
</html>
