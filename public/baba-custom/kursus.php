<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <meta name="description" content="">
   <meta name="author" content="">
   <title>NBLS - Babastudio</title>
   <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
   <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
   <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
   <link href="css/custom.css" rel="stylesheet">
   <link href="lib/animate/animate.min.css" rel="stylesheet">
   <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
   <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
   <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">
   <link href="css/style.css" rel="stylesheet">
</head>
   <body>
     <!-- topbar -->
      <div class="container-fluid bg-dark top-bar f-white pl-5 pb-2">
        <div class="row">
          <div class="col-md-8 col-sm-12">
            <div class="ml-4"><i class="fa fa-phone ml-5 float-sm-left mt-1"></i> + Call Us : 021 - 5366 4008</div>
          </div>
          <div class="col-md-4 col-sm-12">
            <div class="ml-2 tentang">
              <a href="#" class="f-white mr-2">Tentang Kami</a><span>|<span>
              <i class="fa fa-user ml-2"></i>
              <a href="#" class="mr-1 link">Login</a> or <a href="#" class="mr-1 link">Register</a>
              <span><i class="fa fa-shopping-cart"></i></span>
            </div>
          </div>
        </div>
      </div>
      <!-- end topbar -->
      <!-- navbar -->
      <nav class="navbar  navbar-expand-lg navbar-dark bg-blue navbar-bootbites" data-toggle="sticky-onscroll">
         <div class="container">
            <a class="navbar-brand" href="index.html">babastudio.com</a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
               <ul class="navbar-nav ml-auto">
                  <li class="nav-item">
                     <a class="nav-link" href="about.html">BERANDA</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="services.html">TENTANG KAMI</a>
                  </li>
                  <li class="nav-item dropdown">
                     <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     KURSUS
                     </a>
                     <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                        <a class="dropdown-item" href="portfolio-1-col.html">1 Column Portfolio</a>
                        <a class="dropdown-item" href="portfolio-2-col.html">2 Column Portfolio</a>
                        <a class="dropdown-item" href="portfolio-3-col.html">3 Column Portfolio</a>
                        <a class="dropdown-item" href="portfolio-4-col.html">4 Column Portfolio</a>
                        <a class="dropdown-item" href="portfolio-item.html">Single Portfolio Item</a>
                     </div>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="about.html">REGISTER</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="services.html">LOGIN</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="#">
                     <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                     </a>
                  </li>
               </ul>
            </div>
         </div>
      </nav>
      <!-- end navbar -->
      <!-- header -->
      <header>
         <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <!-- <ol class="carousel-indicators">
               <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
               <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
               <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol> -->
            <div class="carousel-inner" role="listbox">
              <div class="carousel-background"><img src="images/banner.jpg" alt=""></div>
                  <div class="carousel-caption d-none d-md-block">
                     <h3><span class="f-orange">BABASTUDIO</span> <br/><span class="f-orange f-slim">HYBRID</span><span class="f-white"> LEARNING</span></h3>
                     <p>Lebih dari 3000 orang telah menggunakan sistem
                        <br/>Babastudio E-learning untuk kurusu online
                     </p>
                  </div>
               </div>
               <div class="carousel-item" style="background-image: url('images/banner.jpg')">
                  <div class="carousel-caption d-none d-md-block">
                     <h3><span class="f-orange">BABASTUDIO</span> <br/><span class="f-orange f-slim">HYBRID</span><span class="f-white"> LEARNING</span></h3>
                     <p>Lebih dari 3000 orang telah menggunakan sistem
                        <br/>Babastudio E-learning untuk kurusu online
                     </p>
                  </div>
               </div>
               <div class="carousel-item" style="background-image: url('images/banner.jpg')">
                  <div class="carousel-caption d-none d-md-block">
                     <h3><span class="f-orange">BABASTUDIO</span> <br/><span class="f-orange f-slim">HYBRID</span><span class="f-white"> LEARNING</span></h3>
                     <p>Lebih dari 3000 orang telah menggunakan sistem
                        <br/>Babastudio E-learning untuk kurusu online
                     </p>
                  </div>
               </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
            </a>
         </div>
      </header>
      <!-- end header -->
      <!-- filter kursus -->
      <div class="container mt-5">
         <div class="col-md-12 no-padding message">
            <h3>Kursus</h3>
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
               <strong>Holy guacamole!</strong> You should check in on some of those fields below.
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <div class="card">
               <div class="card-body">
                  <form class="form-inline">
                     <div class="col-md-1 no-padding">
                        <h4>Filter By</h4>
                     </div>
                     <div class="col-md-10 offset-md-1">
                        <select class="form-control mb-2 mr-sm-2 mr-md-3">
                           <option>-- Select One --</option>
                           <option>Kategori</option>
                           <option>Harga</option>
                           <option>Populer</option>
                        </select>
                        <select class="form-control  mb-2 mr-sm-2 mr-md-3">
                           <option>-- Select One --</option>
                           <option>Kategori</option>
                           <option>Harga</option>
                           <option>Populer</option>
                        </select>
                        <input type="text" class="form-control mb-2 mr-sm-2 w-50" placeholder="Search for...">
                        <span class="input-group-btn">
                        <button class="btn btn-defaul mb-2 mr-sm-2" type="button">
                        <span class="fa fa-search" ></span>
                        </button>
                        </span>
                     </div>
                  </form>
               </div>
            </div>
         </div>
         <!-- end filter kursus -->
         <!-- daftar kursus -->
         <div class="row mt-5">
            <div class="col-lg-4 mb-4">
               <div class="card h-100">
                  <a href="#"><img class="card-img-top" src="images/html-css.jpg" alt=""></a>
                  <div class="card-body">
                     <small class="text-muted">By Babastudio | <i class="fa fa-clock-o" aria-hidden="true"></i> 10 Juli 2017</small>
                     <h3 class="card-title">HTML & CSS</h3>
                     <div class="row">
                        <div class="col-md-12">
                           <small class="text-muted"><i class="fa fa-users" aria-hidden="true"></i> 10 Siswa</small>
                           <h4 class="text-warning float-right">IDR 100K</h4>
                        </div>
                     </div>
                  </div>
                  <div class="card-footer">
                     <a href="#" >View Details <i class="fa fa-long-arrow-right"></i></a>
                  </div>
               </div>
            </div>
            <div class="col-lg-4 mb-4">
               <div class="card h-100">
                  <a href="#"><img class="card-img-top" src="images/html-css.jpg" alt=""></a>
                  <div class="card-body">
                     <small class="text-muted">By Babastudio | <i class="fa fa-clock-o" aria-hidden="true"></i> 10 Juli 2017</small>
                     <h3 class="card-title">HTML & CSS</h3>
                     <div class="row">
                        <div class="col-md-12">
                           <small class="text-muted"><i class="fa fa-users" aria-hidden="true"></i> 10 Siswa</small>
                           <h4 class="text-warning float-right">IDR 100K</h4>
                        </div>
                     </div>
                  </div>
                  <div class="card-footer">
                     <a href="#" >View Details <i class="fa fa-long-arrow-right"></i></a>
                  </div>
               </div>
            </div>
            <div class="col-lg-4 mb-4">
               <div class="card h-100">
                  <a href="#"><img class="card-img-top" src="images/html-css.jpg" alt=""></a>
                  <div class="card-body">
                     <small class="text-muted">By Babastudio | <i class="fa fa-clock-o" aria-hidden="true"></i> 10 Juli 2017</small>
                     <h3 class="card-title">HTML & CSS</h3>
                     <div class="row">
                        <div class="col-md-12">
                           <small class="text-muted"><i class="fa fa-users" aria-hidden="true"></i> 10 Siswa</small>
                           <h4 class="text-warning float-right">IDR 100K</h4>
                        </div>
                     </div>
                  </div>
                  <div class="card-footer">
                     <a href="#" >View Details <i class="fa fa-long-arrow-right"></i></a>
                  </div>
               </div>
            </div>
            <div class="col-lg-4 mb-4">
               <div class="card h-100">
                  <a href="#"><img class="card-img-top" src="images/html-css.jpg" alt=""></a>
                  <div class="card-body">
                     <small class="text-muted">By Babastudio | <i class="fa fa-clock-o" aria-hidden="true"></i> 10 Juli 2017</small>
                     <h3 class="card-title">HTML & CSS</h3>
                     <div class="row">
                        <div class="col-md-12">
                           <small class="text-muted"><i class="fa fa-users" aria-hidden="true"></i> 10 Siswa</small>
                           <h4 class="text-warning float-right">IDR 100K</h4>
                        </div>
                     </div>
                  </div>
                  <div class="card-footer">
                     <a href="#" >View Details <i class="fa fa-long-arrow-right"></i></a>
                  </div>
               </div>
            </div>
            <div class="col-lg-4 mb-4">
               <div class="card h-100">
                  <a href="#"><img class="card-img-top" src="images/html-css.jpg" alt=""></a>
                  <div class="card-body">
                     <small class="text-muted">By Babastudio | <i class="fa fa-clock-o" aria-hidden="true"></i> 10 Juli 2017</small>
                     <h3 class="card-title">HTML & CSS</h3>
                     <div class="row">
                        <div class="col-md-12">
                           <small class="text-muted"><i class="fa fa-users" aria-hidden="true"></i> 10 Siswa</small>
                           <h4 class="text-warning float-right">IDR 100K</h4>
                        </div>
                     </div>
                  </div>
                  <div class="card-footer">
                     <a href="#" >View Details <i class="fa fa-long-arrow-right"></i></a>
                  </div>
               </div>
            </div>
            <div class="col-lg-4 mb-4">
               <div class="card h-100">
                  <a href="#"><img class="card-img-top" src="images/html-css.jpg" alt=""></a>
                  <div class="card-body">
                     <small class="text-muted">By Babastudio | <i class="fa fa-clock-o" aria-hidden="true"></i> 10 Juli 2017</small>
                     <h3 class="card-title">HTML & CSS</h3>
                     <div class="row">
                        <div class="col-md-12">
                           <small class="text-muted"><i class="fa fa-users" aria-hidden="true"></i> 10 Siswa</small>
                           <h4 class="text-warning float-right">IDR 100K</h4>
                        </div>
                     </div>
                  </div>
                  <div class="card-footer">
                     <a href="#" >View Details <i class="fa fa-long-arrow-right"></i></a>
                  </div>
               </div>
            </div>
            <div class="col-lg-4 mb-4">
               <div class="card h-100">
                  <a href="#"><img class="card-img-top" src="images/html-css.jpg" alt=""></a>
                  <div class="card-body">
                     <small class="text-muted">By Babastudio | <i class="fa fa-clock-o" aria-hidden="true"></i> 10 Juli 2017</small>
                     <h3 class="card-title">HTML & CSS</h3>
                     <div class="row">
                        <div class="col-md-12">
                           <small class="text-muted"><i class="fa fa-users" aria-hidden="true"></i> 10 Siswa</small>
                           <h4 class="text-warning float-right">IDR 100K</h4>
                        </div>
                     </div>
                  </div>
                  <div class="card-footer">
                     <a href="#" >View Details <i class="fa fa-long-arrow-right"></i></a>
                  </div>
               </div>
            </div>
            <div class="col-lg-4 mb-4">
               <div class="card h-100">
                  <a href="#"><img class="card-img-top" src="images/html-css.jpg" alt=""></a>
                  <div class="card-body">
                     <small class="text-muted">By Babastudio | <i class="fa fa-clock-o" aria-hidden="true"></i> 10 Juli 2017</small>
                     <h3 class="card-title">HTML & CSS</h3>
                     <div class="row">
                        <div class="col-md-12">
                           <small class="text-muted"><i class="fa fa-users" aria-hidden="true"></i> 10 Siswa</small>
                           <h4 class="text-warning float-right">IDR 100K</h4>
                        </div>
                     </div>
                  </div>
                  <div class="card-footer">
                     <a href="#" >View Details <i class="fa fa-long-arrow-right"></i></a>
                  </div>
               </div>
            </div>
            <div class="col-lg-4 mb-4">
               <div class="card h-100">
                  <a href="#"><img class="card-img-top" src="images/html-css.jpg" alt=""></a>
                  <div class="card-body">
                     <small class="text-muted">By Babastudio | <i class="fa fa-clock-o" aria-hidden="true"></i> 10 Juli 2017</small>
                     <h3 class="card-title">HTML & CSS</h3>
                     <div class="row">
                        <div class="col-md-12">
                           <small class="text-muted"><i class="fa fa-users" aria-hidden="true"></i> 10 Siswa</small>
                           <h4 class="text-warning float-right">IDR 100K</h4>
                        </div>
                     </div>
                  </div>
                  <div class="card-footer">
                     <a href="#" >View Details <i class="fa fa-long-arrow-right"></i></a>
                  </div>
               </div>
            </div>
         </div>
         <div class="container">
            <div class="offset-5">
               <nav aria-label="Page navigation example">
                  <ul class="pagination">
                     <li class="page-item">
                        <a class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                        </a>
                     </li>
                     <li class="page-item"><a class="page-link" href="#">1</a></li>
                     <li class="page-item"><a class="page-link" href="#">2</a></li>
                     <li class="page-item"><a class="page-link" href="#">3</a></li>
                     <li class="page-item">
                        <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                        </a>
                     </li>
                  </ul>
               </nav>
            </div>
         </div>
      </div>
      <!-- end daftar kursus -->
      <!-- Footer -->
      <section class="section-footer" id="footer">
         <div class="container">
            <div class="row text-center text-xs-center text-sm-left text-md-left">
               <div class="col-xs-12 col-sm-4 col-md-3">
                  <img src="images/logo.jpg" class="img-fluid">
                  <p class="f-white">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                     Lorem Ipsum has been the industry's
                     standard dummy text ever since the 1500s,
                     when an unknown printer took a galley of type and scrambled
                     it to make a type specimen book.
                  </p>
                  <ul class="list-unstyled quick-links">
                     <li><a href="javascript:void();"><i class="fa fa-phone"></i>(021) 5366 4008</a></li>
                     <li><a href="javascript:void();"><i class="fa fa-envelope"></i>info@babastudio.com</a></li>
                  </ul>
               </div>
               <div class="col-xs-12 col-sm-4 col-md-3">
                  <h5>LINK MAP</h5>
                  <ul class="list-unstyled quick-links">
                     <li><a href="javascript:void();"><i class="fa fa-circle"></i>Home</a></li>
                     <li><a href="javascript:void();"><i class="fa fa-circle"></i>About</a></li>
                     <li><a href="javascript:void();"><i class="fa fa-circle" ></i>FAQ</a></li>
                     <li><a href="javascript:void();"><i class="fa fa-circle"></i>Get Started</a></li>
                     <li><a href="javascript:void();"><i class="fa fa-circle"></i>Videos</a></li>
                  </ul>
               </div>
               <div class="col-xs-12 col-sm-4 col-md-3">
                  <h5>GET NEWSLETTER</h5>
                  <p class="f-white">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                     Lorem Ipsum has been the industry's
                  </p>
                  <div class="input-group">
                     <input type="text" class="form-control" placeholder="" aria-describedby="basic-addon2">
                     <span class="input-group-addon" id="basic-addon2"><i class="fa fa-paper-plane" aria-hidden="true"></i></span>
                     <br/>
                     <!-- <h6 class="f-white f-slim">Yout Information are safe with us </h6> -->
                  </div>
                  <p class="f-white">Yout Information are safe with us
                  </p>
                  <div id="social">
                     <a class="facebookBtn smGlobalBtn" href="#" ></a>
                     <a class="twitterBtn smGlobalBtn" href="#" ></a>
                     <a class="googleplusBtn smGlobalBtn" href="#" ></a>
                  </div>
               </div>
               <div class="col-xs-12 col-sm-4 col-md-3">
                  <h5>OUR INSTRUCTOR</h5>
                  <div class="col-xs-3 float-left mr-1 mt-1"><img src="images/pp.jpg" width="70em" ></div>
                  <div class="col-xs-3 float-left mr-1 mt-1"><img src="images/pp.jpg" width="70em" ></div>
                  <div class="col-xs-3 float-left mr-1 mt-1"><img src="images/pp.jpg" width="70em" ></div>
                  <div class="col-xs-3 float-left mr-1 mt-1"><img src="images/pp.jpg" width="70em" ></div>
                  <div class="col-xs-3 float-left mr-1 mt-1"><img src="images/pp.jpg" width="70em" ></div>
                  <div class="col-xs-3 float-left mr-1 mt-1"><img src="images/pp.jpg" width="70em" ></div>
                  <div class="col-xs-3 float-left mr-1 mt-1"><img src="images/pp.jpg" width="70em" ></div>
                  <div class="col-xs-3 float-left mr-1 mt-1"><img src="images/pp.jpg" width="70em" ></div>
                  <div class="col-xs-3 float-left mr-1 mt-1"><img src="images/pp.jpg" width="70em" ></div>
               </div>
            </div>
            <hr/ style="border:1px solid #152e48;">
            <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
               <div class="col-md-6 col-sm-12 col-xs-12">
                  <p class="h6 float-left">&copy All right Reversed.<a class="text-green ml-2" href="" target="_blank">BABASTUDIO</a></p>
               </div>
               <div class="credits" style="font-size:5px; color:#ccc; opacity: 0.2; float:right;">
                  Best <a href="https://bootstrapmade.com/">Bootstrap Templates</a> by BootstrapMade
               </div>
            </div>
            </hr>
         </div>
      </section>
      <!-- end Footer -->
      <script src="vendor/jquery/jquery.min.js"></script>
      <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      <script src="js/custom.js"></script>
      <script src="lib/jquery/jquery-migrate.min.js"></script>
      <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
      <script src="lib/easing/easing.min.js"></script>
      <script src="lib/superfish/hoverIntent.js"></script>
      <script src="lib/superfish/superfish.min.js"></script>
      <script src="lib/wow/wow.min.js"></script>
      <script src="lib/waypoints/waypoints.min.js"></script>
      <script src="lib/counterup/counterup.min.js"></script>
      <script src="lib/owlcarousel/owl.carousel.min.js"></script>
      <script src="lib/isotope/isotope.pkgd.min.js"></script>
      <script src="lib/lightbox/js/lightbox.min.js"></script>
      <script src="lib/touchSwipe/jquery.touchSwipe.min.js"></script>
      <script src="js/main.js"></script>
   </body>
</html>
